using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

/// 
/// A BaseEditor class.
/// 
public class BaseEditor<T> : Editor where T : class
{	
	protected FieldProperty[] databaseFields;
	protected FieldProperty[] dataFields;
	
	protected List<FieldProperty[]> pInfoList = new List<FieldProperty[]>();
		
	public virtual void OnEnable()
	{
        T data = target as T;
        databaseFields = ExposeMembers.GetFieldsAndProperties(data);
    }

    public override void OnInspectorGUI()
    { 				
		if (target == null)
			return;

        base.OnInspectorGUI();

        //this.DrawDefaultInspector();
        ExposeMembers.Expose(databaseFields);
 
		foreach(FieldProperty[] p in pInfoList)
		{
            ExposeMembers.Expose( p );	
		}
    }

}
