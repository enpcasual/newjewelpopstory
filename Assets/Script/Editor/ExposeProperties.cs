using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

#region InfoClass
public class FieldProperty
{
    System.Object m_Instance;
    MemberInfo m_Info;
    SerializedPropertyType m_Type;

    MethodInfo m_Getter;
    MethodInfo m_Setter;

    public SerializedPropertyType Type
    {
        get
        {
            return m_Type;
        }
    }

    public String Name
    {
        get
        {
            return ObjectNames.NicifyVariableName(m_Info.Name);
        }
    }

    public FieldProperty(System.Object instance, MemberInfo info)
    {

        m_Instance = instance;
        m_Info = info;


        switch (m_Info.MemberType)
        {
            case MemberTypes.Field:
                {
                    FieldInfo _info = (FieldInfo)m_Info;
                    GetPropertyType(_info.FieldType, out m_Type);
                }
                break;
            case MemberTypes.Property:
                {
                    PropertyInfo _info = (PropertyInfo)m_Info;
                    GetPropertyType(_info.PropertyType, out m_Type);
                    m_Getter = _info.GetGetMethod();
                    m_Setter = _info.GetSetMethod();
                }
                break;
            default:
                //다른 타입들은 패스!!
                break;
        }
    }

    public System.Object GetValue()
    {
        switch (m_Info.MemberType)
        {
            case MemberTypes.Field:
                {
                    FieldInfo _info = (FieldInfo)m_Info;
                    return _info.GetValue(m_Instance);
                }
                break;
            case MemberTypes.Property:
                {
                    return m_Getter.Invoke(m_Instance, null);
                }
                break;
            default:
                //다른 타입들은 패스!!
                break;
        }

       return null;
    }

    public void SetValue(System.Object value)
    {
        switch (m_Info.MemberType)
        {
            case MemberTypes.Field:
                {
                    FieldInfo _info = (FieldInfo)m_Info;
                    _info.SetValue(m_Instance, value);
                }
                break;
            case MemberTypes.Property:
                {
                    m_Setter.Invoke(m_Instance, new System.Object[] { value });
                }
                break;
            default:
                //다른 타입들은 패스!!
                break;
        }      
    }

    public static bool GetPropertyType(Type type, out SerializedPropertyType propertyType)
    {
        propertyType = SerializedPropertyType.Generic;

        if (type == typeof(int))
        {
            propertyType = SerializedPropertyType.Integer;
            return true;
        }

        if (type == typeof(float))
        {
            propertyType = SerializedPropertyType.Float;
            return true;
        }

        if (type == typeof(bool))
        {
            propertyType = SerializedPropertyType.Boolean;
            return true;
        }

        if (type == typeof(string))
        {
            propertyType = SerializedPropertyType.String;
            return true;
        }

        if (type == typeof(Vector2))
        {
            propertyType = SerializedPropertyType.Vector2;
            return true;
        }

        if (type == typeof(Vector3))
        {
            propertyType = SerializedPropertyType.Vector3;
            return true;
        }

        if (type.IsEnum)
        {
            propertyType = SerializedPropertyType.Enum;
            return true;
        }

        return false;

    }

}
#endregion
public static class ExposeMembers
{
	public static void Expose(FieldProperty[] arrayinfo )
	{
 
		GUILayoutOption[] emptyOptions = new GUILayoutOption[0];
 
		EditorGUILayout.BeginVertical( emptyOptions );
 
		foreach (FieldProperty info in arrayinfo)
		{
 
			EditorGUILayout.BeginHorizontal( emptyOptions );
 
			switch (info.Type )
			{
			case SerializedPropertyType.Integer:
                    info.SetValue( EditorGUILayout.IntField(info.Name, (int)info.GetValue(), emptyOptions ) ); 
				break;
 
			case SerializedPropertyType.Float:
                    info.SetValue( EditorGUILayout.FloatField(info.Name, (float)info.GetValue(), emptyOptions ) );
				break;
 
			case SerializedPropertyType.Boolean:
                    info.SetValue( EditorGUILayout.Toggle(info.Name, (bool)info.GetValue(), emptyOptions ) );
				break;
 
			case SerializedPropertyType.String:
                    info.SetValue( EditorGUILayout.TextField(info.Name, (String)info.GetValue(), emptyOptions ) );
				break;
 
			case SerializedPropertyType.Vector2:
                    info.SetValue( EditorGUILayout.Vector2Field(info.Name, (Vector2)info.GetValue(), emptyOptions ) );
				break;
 
			case SerializedPropertyType.Vector3:
                    info.SetValue( EditorGUILayout.Vector3Field(info.Name, (Vector3)info.GetValue(), emptyOptions ) );
				break;
 
			case SerializedPropertyType.Enum:
                    info.SetValue(EditorGUILayout.EnumPopup(info.Name, (Enum)info.GetValue(), emptyOptions));
				break;
 
			default:
 
				break;
 
			}
 
			EditorGUILayout.EndHorizontal();
 
		}
 
		EditorGUILayout.EndVertical();
 
	}
 
    //Reflection의 property, Field 개념. Field는 변수. property는 getter,setter
	public static FieldProperty[] GetFieldsAndProperties( System.Object obj )
	{
 
		List<FieldProperty> fields = new List<FieldProperty>();

        MemberInfo[] infos = obj.GetType().GetFields();
    
        foreach ( MemberInfo info in infos )
		{
            //property일때 체크. Field랑 같이 체크할려니까 ..
			//if ( ! (info.CanRead && info.CanWrite) )
			//	continue;
 
			object[] attributes = info.GetCustomAttributes( true );
 
			bool isExposed = false;
 
			foreach( object o in attributes )
			{
				if ( o.GetType() == typeof( ExposePropertyAttribute ) )
				{
					isExposed = true;
					break;
				}
			}

            if (!isExposed)
                continue;


            FieldProperty field = new FieldProperty(obj, info);
            fields.Add(field);

        }

        return fields.ToArray();
 
	}

    //여기에 추가 하는게 맞는건가? 일단 추가 해놓고..
    //******************************************************
    //Property를 보여주기 위해 Editor쪽에서 사용하는 함수
    //*******************************************************
    public static SerializedProperty DrawProperty(string label, SerializedObject serializedObject, string property, bool padding, params GUILayoutOption[] options)
    {
        SerializedProperty sp = serializedObject.FindProperty(property);

        if (sp != null)
        {
            if (padding) EditorGUILayout.BeginHorizontal();

            if (label != null) EditorGUILayout.PropertyField(sp, new GUIContent(label), options);
            else EditorGUILayout.PropertyField(sp, options);

            if (padding)
            {
                GUILayout.Space(18f);
                EditorGUILayout.EndHorizontal();
            }
        }
        else
        {
            Debug.LogError("SpriteTextInspector : Property Not Find : " + property);
        }
        return sp;
    }

}