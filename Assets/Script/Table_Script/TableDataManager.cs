﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

#region Class_Struct


public class LocalizingT
{
    public int index;
    public string kor;//한국
    public string eng;//영어
    public string ja;//일본
    public string zh_TW;//중국번체
    public string zh_CN;//중국간체
    public string hi;//인도
    public string ar;//중동
    public string ru;//러시아
    public string de;//독어
    public string pl;//폴란드
    public string fr;//프랑스
}

public class GateWay
{
    public int index;
    public int stage;
    public int needstar;
    public int theme;
    //public int startstage;
   // public int endstage;
}

public class GoldBoxStage
{
    public int index;
    public int stage;
    public int gold;
}


public class ShopList
{
    public int index;
    public int saletype;
    public string price;
    public int gold;
    public int salegold;
}

public class DailyReward
{
    public int index;
    public int type;
    public int reward1;
    public int reward2;
}

public class FoodTruckinfo
{
    public int index;
    public int grade;
    public int openlevel;
    public int moveplus;
    public int scoreplus;
    public int goldplus;
    public int bonusplus;
    public int butterflyplus;
    public int buyprice;
}

public class MissionGold
{
    public int index;
    public int theme;
    public int rewardgold;
    public int rewardtruck;
}

public class BuyPrice
{
    public int index;
    public string type;
    public int typeindex;
    public int buyprice;
}

public class GPGSAchivement
{
    public int index;
    public int point;
    public int type;
    public int value;

}

public class CpInfo
{
    public string num;
    public int value;
}

public class FreeReward
{
    public int index;
    public int cooltime;
    public int type;
    public int kind;
    public int reward;
}

public class Packageitem
{
    public int index;
    public int itemcnt;
    public int glodcnt;
    public string price;
}

public class FreeBox
{
    public int index;
    public int type;
    public string kind;
    public int reward;
    public int rate1;
    public int accumulateRate1;
    public int rate2;
    public int accumulateRate2;
}

#endregion

public class TableDataManager : Singleton<TableDataManager>
{
    public enum Language  
    {
        KO = 0,
        EN,
        JA,
        ZH_TW,
        ZH_CN,
        HI,
        AR,
        RU,
        DE,
        PL,
        FR,
    }

    public Language currentLanguage = Language.EN;


    public Language changeLanguage = Language.EN;
    //****************************
    //Table Data
    //****************************
  
    public List<LocalizingT> m_LocalizingData = new List<LocalizingT>();

    public List<GateWay> m_GateWay = new List<GateWay>();

    public List<GoldBoxStage> m_GoldBoxStage = new List<GoldBoxStage>();

    public List<ShopList> m_ShopList = new List<ShopList>();

    public List<DailyReward> m_DailyReward = new List<DailyReward>();

    public List<FoodTruckinfo> m_Foodtruck = new List<FoodTruckinfo>();

    public List<MissionGold> m_missiongold = new List<MissionGold>();

    public List<BuyPrice> m_buyprice = new List<BuyPrice>();

    public List<GPGSAchivement> m_achivement = new List<GPGSAchivement>();

    public List<CpInfo> m_cp = new List<CpInfo>();

    public List<FreeReward> m_FreeRewardlist = new List<FreeReward>();

    public List<Packageitem> m_packageitem = new List<Packageitem>();

    public List<FreeBox> m_FreeBoxlist = new List<FreeBox>();
    //****************************
    //Table Load Function
    //****************************
    //로컬라이징 테이블 데이터 
    #region Localizing
    public void Load_LocalizingTable()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Text", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                LocalizingT item = new LocalizingT();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); cnt++;
                item.kor = stral[cnt]; cnt++;
                item.eng = stral[cnt]; cnt++;
                item.ja = stral[cnt]; cnt++;
                item.zh_TW = stral[cnt]; cnt++;
                item.zh_CN = stral[cnt]; cnt++;
                item.hi = stral[cnt]; cnt++;
                item.ar = stral[cnt]; cnt++;
                item.ru = stral[cnt]; cnt++;
                item.de = stral[cnt]; cnt++;
                item.pl = stral[cnt]; cnt++;
                item.fr = stral[cnt]; cnt++;
                /*
                if (stral.Length > 1)
                {
                    item.kor = stral[cnt]; ++cnt;
                }
                if (stral.Length > 2)
                {
                    item.eng = stral[cnt]; ++cnt;
                }
                 */

                m_LocalizingData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }
    }

    public void LocalTextSetting(SystemLanguage getlanguagetype)
    {
        SystemLanguage[] languagekind =
            {
                SystemLanguage.Korean,//"ko",//한국어
                SystemLanguage.English,// "en",//영어
                SystemLanguage.Japanese,//"ja",//일어
                SystemLanguage.ChineseTraditional,//"zh_TW",//중국(번체)
                SystemLanguage.ChineseSimplified,//"zh_CN",//중국(간체)
                SystemLanguage.Indonesian,//"hi",//인도
                SystemLanguage.Arabic,//"ar",//아랍
                SystemLanguage.Russian,//"ru",//러시아
                SystemLanguage.German,//"de",//독일
                SystemLanguage.Polish,//"pl",//폴란드
                SystemLanguage.French,//"fr"//프랑스

            };

        for (int i = 0; i < languagekind.Length; i++)
        {
            if (languagekind[i] == getlanguagetype)
            {
                
                  currentLanguage = (Language)i;

                  
                break;
            }
        }

        //저장..
        SecurityPlayerPrefs.SetInt("CurrenLanguage", (int)TableDataManager.Instance.currentLanguage);
    }
    public string GetLocalizeText(int nIndex)
    {


        //리스트에 해당 인덱스가 없다면
        if (m_LocalizingData.Exists(r => r.index == nIndex) == false)
        {
            Debug.LogError(nIndex + " : Localizing Data NULL");
            return "NULL Data";
        }
        else
        {

          
            switch (currentLanguage)
            {
                case Language.KO:
                    return m_LocalizingData.Find(r => r.index == nIndex).kor;
                case Language.EN:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
                case Language.JA:
                    return m_LocalizingData.Find(r => r.index == nIndex).ja;
                case Language.ZH_TW:
                    return m_LocalizingData.Find(r => r.index == nIndex).zh_TW;
                case Language.ZH_CN:
                    return m_LocalizingData.Find(r => r.index == nIndex).zh_CN;
                case Language.HI:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
                case Language.AR:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
                case Language.RU:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
                case Language.DE:
                    return m_LocalizingData.Find(r => r.index == nIndex).de;
                case Language.PL:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
                case Language.FR:
                    return m_LocalizingData.Find(r => r.index == nIndex).fr;
                default:
                    return m_LocalizingData.Find(r => r.index == nIndex).eng;
            }
        }
    }
    #endregion

    //gateway  데이타 
    #region GateWay
    public void Load_GateWay()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/1_gateway", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                GateWay item = new GateWay();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.stage = int.Parse(stral[cnt]); ++cnt;
                item.needstar = int.Parse(stral[cnt]); ++cnt;
                item.theme = int.Parse(stral[cnt]); ++cnt;
                /*
                if (item.index == 0)
                {
                    item.startstage = 0;

                }
                else
                {
                    item.startstage = m_GateWay[i - 1].stage;

                }
                item.endstage = item.stage - 1;
               
                */
                m_GateWay.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_GateWay");
        }
    }
    #endregion

    //보물상자  데이타 
    #region GoldBox
    public void Load_GoldBox()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/2_goldbox", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                GoldBoxStage item = new GoldBoxStage();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.stage = int.Parse(stral[cnt]); ++cnt;
                item.gold = int.Parse(stral[cnt]); ++cnt;


                m_GoldBoxStage.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_GoldBox");
        }
    }
    #endregion

    #region Shop
    //상점리스트 데이타
    public void Load_Shop()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/5_store", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                ShopList item = new ShopList();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.saletype = int.Parse(stral[cnt]); ++cnt;
                item.price = stral[cnt]; ++cnt;
                item.gold = int.Parse(stral[cnt]); ++cnt;
                item.salegold = int.Parse(stral[cnt]); ++cnt;

                m_ShopList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_Shop");
        }
    }
    #endregion

    #region DailyReward
    //데일리 보상
    public void Load_DailyReward()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/4_daily_reward", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                DailyReward item = new DailyReward();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.type = int.Parse(stral[cnt]); ++cnt;

                item.reward1 = int.Parse(stral[cnt]); ++cnt;
                item.reward2 = int.Parse(stral[cnt]); ++cnt;

                m_DailyReward.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_DailyReward");
        }
    }
    #endregion

    #region FoodTruck
    //데일리 보상
    public void Load_FoodTruck()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/6_foodcar", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                FoodTruckinfo item = new FoodTruckinfo();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.grade = int.Parse(stral[cnt]); ++cnt;

                item.openlevel = int.Parse(stral[cnt]); ++cnt;
                item.moveplus = int.Parse(stral[cnt]); ++cnt;
                item.scoreplus = int.Parse(stral[cnt]); ++cnt;
                item.goldplus = int.Parse(stral[cnt]); ++cnt;
                item.bonusplus = int.Parse(stral[cnt]); ++cnt;
                item.butterflyplus = int.Parse(stral[cnt]); ++cnt;
                item.buyprice = int.Parse(stral[cnt]); ++cnt;

                m_Foodtruck.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_DailyReward");
        }
    }
    #endregion

    #region MissionGold

    //미션보상
    public void Load_MissionGold()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/7_mission", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                MissionGold item = new MissionGold();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;
                item.theme = int.Parse(stral[cnt]); ++cnt;

                item.rewardgold = int.Parse(stral[cnt]); ++cnt;

                item.rewardtruck = int.Parse(stral[cnt]); ++cnt;

                m_missiongold.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_DailyReward");
        }
    }
    #endregion

    #region BuyGold

    //미션보상
    public void Load_BuyGold()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/8_buyprice", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                BuyPrice item = new BuyPrice();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;

                item.type = stral[cnt]; ++cnt;

                item.typeindex = int.Parse(stral[cnt]); ++cnt;

                item.buyprice = int.Parse(stral[cnt]); ++cnt;

                m_buyprice.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_BuyGold error");
        }
    }
    #endregion

    #region GPGS_Achivement

    //업적
    public void Load_Achivement()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/9_gpgs", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                GPGSAchivement item = new GPGSAchivement();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;

                item.point = int.Parse(stral[cnt]); ++cnt;

                item.type = int.Parse(stral[cnt]); ++cnt;

                item.value = int.Parse(stral[cnt]); ++cnt;

                m_achivement.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_Achivement error");
        }
    }
    #endregion

    #region CP
    //업적
    public void Load_Cp()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/10_cp", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');


                CpInfo item = new CpInfo();

                int cnt = 0;

                item.num = stral[cnt]; ++cnt;

                item.value = int.Parse(stral[cnt]); ++cnt;

                m_cp.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_cp error");
        }
    }
    #endregion



    #region FreeCoinReward
    //무료시청
    public void Load_FreeCoinReward()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/11_freecoin", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');


                FreeReward item = new FreeReward();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;

                item.cooltime = int.Parse(stral[cnt]); ++cnt;

                item.type = int.Parse(stral[cnt]); ++cnt;

                item.kind = int.Parse(stral[cnt]); ++cnt;

                item.reward = int.Parse(stral[cnt]); ++cnt;


                m_FreeRewardlist.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_cp error");
        }
    }
    #endregion

    #region Packageitem
    //패키지 아이템
    public void Load_PackageItem()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/12_specialitem", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');


                Packageitem item = new Packageitem();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;

                item.itemcnt = int.Parse(stral[cnt]); ++cnt;

                item.glodcnt = int.Parse(stral[cnt]); ++cnt;

                item.price = stral[cnt]; ++cnt;

                 m_packageitem.Add(item);
                
            }
        }
        catch
        {
            Debug.LogError("Load_cp error");
        }
    }
    #endregion

    #region FreeBoxReward
    //무료시청
    public void Load_FreeBoxReward()
    {
        try
        {
            TextAsset txObj = Resources.Load("Table/13_freeBox", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');


                FreeBox item = new FreeBox();

                int cnt = 0;

                item.index = int.Parse(stral[cnt]); ++cnt;

                item.type = int.Parse(stral[cnt]); ++cnt;

                item.kind = stral[cnt]; ++cnt;

                item.reward = int.Parse(stral[cnt]); ++cnt;

                item.rate1 = int.Parse(stral[cnt]); ++cnt;

                item.rate2 = int.Parse(stral[cnt]); ++cnt;


                m_FreeBoxlist.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Load_cp error");
        }
    }
    #endregion
}
