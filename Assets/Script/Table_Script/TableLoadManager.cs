﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TableLoadManager : MonoBehaviour
{
    public static TableLoadManager instans = null;

    int m_MaxProcess = 10;
    int m_Process = 0;

 
    public UISprite Progress_bar;

    public UILabel lb_Version;
    public UILabel lb_LoginState;

    public GameObject GRB_ALL;

    // Use this for initialization
    void Awake()
    {
        instans = this;

        Progress_bar.fillAmount = 0.0f;
 
       // sp_Fill.gameObject.SetActive(true);

        
        /*
        switch(Const_AppInfo.APP_MarKet)
        {
            case MarKet.Google:
                GRB_ALL.SetActive(false);
                break;
            case MarKet.OneStore:
                GRB_ALL.SetActive(true);
                break;
            default:
                GRB_ALL.SetActive(false);
                break;
        }
         * */
    }

    public void Init()
    {
        StartCoroutine(Process());
    }
    void Start()
    {
        
    }

    IEnumerator Process()
    {
        TableDataManager.Instance.Load_LocalizingTable();

        
        //4. 테이블 로드 
        yield return StartCoroutine(TableDataLoad());

       
    }

    bool isStateEnd = false;
    bool isCheckEnd = false;
    void CheckState()
    {
        isStateEnd = true;
    }

    void CheckVersion()
    {

#if UNITY_EDITOR
        isCheckEnd = true;
        return;
#elif UNITY_ANDROID
        //int ManifestVersionCode = Native_Brige.GetVersionCode();
        //Debug.Log("Version :"+ ManifestVersionCode);
        //if (ManifestVersionCode == CompanyManager.Instance.m_MyCompanyInfo.Server_AppCode)
        //{
        //    isCheckEnd = true;
        //}
        //else if(ManifestVersionCode > CompanyManager.Instance.m_MyCompanyInfo.Server_AppCode)
        //{
        //    //클라이언트 버전이 더 클때는 서버점검이라고 표시 해준다.
        //    PopupManager.Instance.pop_Check.Show(TableDataManager.Instance.GetLocalizeText(482), Application.Quit );
        //}
        //else
        //{
        //    //서버 버전이 더 클때는 업데이트 받으라고 표시해준다.
        //    PopupManager.Instance.pop_Check.Show(TableDataManager.Instance.GetLocalizeText(483), OpenUpdate);
        //}

#endif

    }

    float Delaytime = 0f;
    IEnumerator TableDataLoad()
    {

        TableDataManager.Instance.Load_GateWay();
        TableLoadPlus();
        TableDataManager.Instance.Load_GoldBox();
        TableLoadPlus();
        TableDataManager.Instance.Load_Shop();
        TableLoadPlus();
        TableDataManager.Instance.Load_DailyReward();
        TableLoadPlus();
        TableDataManager.Instance.Load_FoodTruck();
        TableDataManager.Instance.Load_MissionGold();
        TableDataManager.Instance.Load_BuyGold();
        TableDataManager.Instance.Load_Achivement();
        yield return null;
        //스테이지 세팅
        
       

       
    
              

       
#if UNITY_EDITOR

#elif UNITY_ANDROID
       //구글 로그인을 기다린다..
        //while (Social.localUser.authenticated == false)
        //{
        //    if(Delaytime < 10)
        //    {
        //        Delaytime += Time.deltaTime;
        //        if (Delaytime > 10)
        //            go_Login.GetComponent<UILabel>().text = TableDataManager.Instance.GetLocalizeText(484);
        //    }       
        //    yield return null;
        //}       
#endif
      
    }


    public void TableLoadPlus()
    {
        m_Process++;
        Progress_bar.fillAmount = (float)m_Process / (float)m_MaxProcess;
    }

}
