﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WorldObjectAnimation : MonoBehaviour {
    
    [SerializeField]
    public abstract bool IsPlay { get; }
    protected bool m_IsPlay = false;

    public bool IsCantLoop = false;

    public bool IsLoop = false;
    public void SetLoop(bool isLoop)
    {
        if (!IsCantLoop)
        {
            IsLoop = isLoop;
        }
    }

    public bool Active()
    {
        return this.enabled;
    }

    public void Enable()
    {
        this.enabled = true;
    }

    public void Disable()
    {
        this.enabled = false;
    }

    public abstract void Actor();


    // 클릭 시 해당 오브젝트가 임팩트가 있으면 발생 시켜주려고 했는데...
    // 문제가 조금 있어서 패스.
    //public virtual void Awake()
    //{
    //    UIButton button = gameObject.AddComponent<UIButton>();
    //    BoxCollider collider = gameObject.AddComponent<BoxCollider>();
    //    UISprite uiSprite = GetComponent<UISprite>();
    //    if (uiSprite != null)
    //    {
    //        collider.size = uiSprite.localSize;
    //    }
    //    else
    //    {
    //        Sprite sprite = GetComponent<Sprite>();
    //        if (sprite == null)
    //        {
    //            return;
    //        }

    //        collider.size = sprite.texture.texelSize;
    //    }

    //    button.onClick.Add(new EventDelegate(this, "Actor"));
    //}
}
