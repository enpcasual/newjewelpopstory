﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ListExtension;

public class TestScript {

    static public int TestCnt = 10000; //반복 횟수
    static public int LoopCnt = 0;
    static public int WorkCnt = 0;
    static public int SetColorCnt = 0; //전체 중복된 갯수
    static public float StartTime = 0f;
    static public float EndTime = 0f;

    static public void Init()
    {
        LoopCnt = 0;
        SetColorCnt = 0;
    }


    static public void ShowTestLog()
    {
        Debug.Log("시뮬레이션횟수 : " + TestCnt);
        Debug.Log("총루프반복횟수 : " + LoopCnt);
        Debug.Log("블럭단위작업횟수 : " + WorkCnt);
        Debug.Log("총타입바꾼횟수 : " + SetColorCnt);
        float delta = EndTime - StartTime;
        Debug.Log("걸린 시간 : " + delta.ToString());
        
        Debug.Log("1회당루프횟수 : " + (float)LoopCnt / TestCnt);
    }
    public void Temp()
    {
        var list = new List<GameObject>();
        var item = list.GetRandom();
    }
}



namespace ListExtension
{
    public static class ListExtension
    {
        public static T GetRandom<T>(this List<T> target)
        {
            return target[Random.Range(0, target.Count)];
        }
    }
}
