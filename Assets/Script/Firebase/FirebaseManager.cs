﻿using UnityEngine;
//using Firebase.Analytics;

public enum ABTestType
{
	A,
	B,
}
public partial class FirebaseManager : MonoBehaviour
{
	public static FirebaseManager Instance;

	int isLogScreen = 0;

	private void Awake()
	{
		if (Instance == null)
			Instance = this;

		isLogScreen = SecurityPlayerPrefs.GetInt("LogScreen", 0);
	}

	private void Start()
	{
		//FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
		//{
		//	var dependencyStatus = task.Result;
		//	if (dependencyStatus == Firebase.DependencyStatus.Available)
		//	{
		//		// Create and hold a reference to your FirebaseApp, i.e.
		//		//   app = Firebase.FirebaseApp.DefaultInstance;
		//		// where app is a Firebase.FirebaseApp property of your application class.

		//		// Set a flag here indicating that Firebase is ready to use by your
		//		// application.
		//	}
		//	else
		//	{
		//		UnityEngine.Debug.LogError(System.String.Format(
		//		  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
		//		// Firebase Unity SDK is not safe to use here.
		//	}
		//});
		SetType();
	}

	private void OnApplicationQuit()
	{
		if (isLogScreen == 0)
		{
			Debug.Log("더이상 안날린다.");
			SecurityPlayerPrefs.SetInt("LogScreen", 1);
		}
	}

	//****************************
	//Stage 정렬을위한 String변경
	//****************************
	public string GetCategoryStage(int stage)
	{
        string str = "Stage_";
        if (stage >= 10000)
        {
            str += stage.ToString();
        }
        else if (stage >= 1000)
        {
            str += "0" + stage.ToString();
        }
        else if (stage >= 100)
        {
            str += "00" + stage.ToString();
        }
        else if (stage >= 10)
        {
            str += "000" + stage.ToString();
        }
        else
        {
            str += "0000" + stage.ToString();
        }

        return str;
	}

	//*********************
	//신규 유저 1섹션 로그
	//*********************

	public ABTestType TestType = ABTestType.A;
	public void SetType()
	{
        string uid = SystemInfo.deviceUniqueIdentifier; //Native_Brige.GetUUID();
	}

	public void LogEvent_NewUser(int stage, int clearcnt)
	{
		if (GameManager.Instance.m_GameData.GamePlayFirst && clearcnt == 1)
		{
			//LogEvent("NewUser", TestType.ToString(), GetCategoryStage(stage), 1); // 190117_필요없는값이라고 하셔서 제거
		}
	}

	//*********************
	//추가 정보
	//*********************
	public void LogEvent_LoadingTime(long time)
	{
		LogEvent("Info", "LoadingTime", "LoadingTime", time);
    }

    public void LogEvent_PlayTime(int stage, int playtime)
    {
        string category = GetCategoryStage(stage);
        string action = "Time";
        string label = "PlayTime";
        int value = playtime;

        LogEvent(category, action, label, value);
    }

    //*****************************
    //카테고리, 액션으로 함수 추가
    //*****************************
    public void LogEvent_Play(int stage, int playcnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Play";
		string label = "PlayCount_" + playcnt.ToString();
		int value = playcnt == 1 ? 1 : 2; // 처음은 1, 그다음 클리어는 2
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_Clear(int stage, int clearcnt, int playcnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Clear";
		string label = "ClearCount_" + clearcnt.ToString();
		int value = clearcnt * 100 / playcnt;  // clearcnt / playcnt * 100
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_Fail(int stage, int failcnt, int playcnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Fail";
		string label = "FailCount_" + failcnt.ToString();
		int value = failcnt * 100 / playcnt;  // failcnt / playcnt * 100
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_Score(int stage, int score)
	{
		string category = GetCategoryStage(stage);
		string action = "Score";
		string label = "StageScore";
		int value = score;
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_Star(int stage, int starcnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Star";
		string label = "StarCount";
		int value = starcnt;
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_RemainMove(int stage, int movecnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Move";
		string label = "MoveRemain";
		int value = movecnt;
        
		LogEvent(category, action, label, value);
	}

	#region 캐쉬아이템 정보
	const string Action_CashItem = "CashItem";
	public void LogEvent_CashItem_UseClear(int stage)
	{
		string category = GetCategoryStage(stage);
		string action = "CashItem";
		string label = "CashItem_UseClear";
        
		LogEvent(category, Action_CashItem, label, 1);
	}
	public void LogEvent_CashItem_NotUseClear(int stage)
	{
		string category = GetCategoryStage(stage);
		string action = "CashItem";
		string label = "CashItem_NotUseClear";
        
		LogEvent(category, Action_CashItem, label, 1);
	}

	public void LogEvent_CashItem_H(int stage, int usecnt)
	{
		string category = GetCategoryStage(stage);
		string action = "CashItem";
		string label = "CashItem_H";
		int value = usecnt;
        
		LogEvent(category, Action_CashItem, label, value);
	}

	public void LogEvent_CashItem_B(int stage, int usecnt)
	{
		string category = GetCategoryStage(stage);
		string action = "CashItem";
		string label = "CashItem_B";
		int value = usecnt;
        
		LogEvent(category, Action_CashItem, label, value);
	}

	public void LogEvent_CashItem_L(int stage, int usecnt)
	{
		string category = GetCategoryStage(stage);
		string label = "CashItem_L";
		int value = usecnt;
        
		LogEvent(category, Action_CashItem, label, value);
	}

	public void LogEvent_CashItem_RandomBox(int stage, int kind)
	{
		string category = GetCategoryStage(stage);
		string action = "RandomBox";
		string label = "RandomBox_" + kind.ToString();
        
		LogEvent(category, action, label, 1);
	}
	#endregion
	public void LogEvent_Continue(int stage, int usecnt)
	{
		string category = GetCategoryStage(stage);
		string action = "Continue";
		string label = "CountineCount";
		int value = usecnt;
        
		LogEvent(category, action, label, value);
	}

	public void LogEvent_AdLocation(int stage, string location)
	{
		string category = GetCategoryStage(stage);
		string action = "ADLocation";
		string label = string.Format("ADLocation_{0}", location);
        
		LogEvent(category, action, label, 1);
	}

	public void LogEvent_ADChoice(string eventName, string eventAction, int stage = -1)
	{
		string category = "ADInfo" + (stage != -1 ? ("_" + GetCategoryStage(stage)) : string.Empty);
		string action = "ChoiceEvent";
		string label = string.Format("{0}_{1}", eventName, eventAction);
		int value = 1;
        
		LogEvent(category, action, label, value);
	}

    public void LogEvent_Collection(string type, int level)
    {
        string category = "collection";
        string action = type + "_level";
        string label = type + "_" + level.ToString();        
                
        LogEvent(category, action, label, 1);        
    }

    public void LogEvent_RegistMap(string type)
    {
        // type = Auto(자동플레이), Direct(직접플레이)
        string category = "EditorMode";
        string action = "RegistMap";
        string label = type;

        LogEvent(category, action, label, 1);
    }
    
    public void LogEvent_PlayMultiMode(string type)
    {
        // type = Popular(인기모드), Challenge(챌린지모드)
        string category = "MultiMode";
        string action = "Play";
        string label = type;

        LogEvent(category, action, label, 1);
    }

    public void LogEvent_UseGold(int stage, string type, int gold)
    {
        string category = GetCategoryStage(stage);
        string action = "UseGold";
        string label = string.Format("UseGold_{0} : {1}", type, gold);

        LogEvent(category, action, label, gold);
    }

    public void LogEvent_GDPR(int stage, string countryCode)
    {
        string category = GetCategoryStage(stage);
        string action = "GDPR_Click";
        string label = string.Format("GDPR_Click_{0}", countryCode);

        LogEvent(category, action, countryCode, 1);
    }

    //**************
    //LogScreen
    //**************
    public void LogScreen(string log, int stage = -1)
	{
		//처음일때는 모든 씬정보를 날리고 그다음부턴 스테이지 정보만 날린다.
		if (isLogScreen == 1 && stage == -1)
			return;

		string category = "";
		if (stage != -1)
			category = GetCategoryStage(stage);
#if !UNITY_EDITOR && !TEST_MODE
                Firebase.Analytics.FirebaseAnalytics.SetCurrentScreen(log, category);
				Debug.Log("LogScreen() : " + log + category);
#endif
    }
    /// <summary>
    /// 애널리틱스 로그이벤트 기록
    /// </summary>
    /// <param name="name">로그네임</param>
    /// <param name="parameters">파라메터배열</param>
    void LogEvent(string eventCategory, string eventAction, string eventLabel, long value)
    {
#if !UNITY_EDITOR && !TEST_MODE
        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventCategory, eventLabel, value);
#endif
    }
}
