﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CheatStage : MonoBehaviour
{
    public enum CommandID
    {
        Left = 0,
        Right = 1,
    }

    [SerializeField]
    private Transform LeftButton;

    [SerializeField]
    private Transform RightButton;

    [SerializeField]
    private Transform PasswordObject;

    [SerializeField]
    private UIInput StageInput;

    [SerializeField]
    private UIInput TimeScaleInput;
    


    [SerializeField]
    private List<CommandID> Command = new List<CommandID>();

    private List<CommandID> CommandInput = new List<CommandID>();

    private Coroutine myRoutine;

    private void OnEnable()
    {
        CommandInput.Clear();
    }

    public bool isCanUseCheat
    {
        get
        {
            bool value = false;
#if UNITY_EDITOR || ENABLE_CHEAT || DEVELOPMENT_BUILD
            value = true;
#endif
            return value;
        }
    }

    public void PushCommand(Transform item)
    {
        if (!isCanUseCheat)
            return;

        if (item == RightButton)
        {
            CommandInput.Add(CommandID.Right);
        }
        else if (item == LeftButton)
        {
            CommandInput.Add(CommandID.Left);
        }

        if (CommandInput.SequenceEqual(Command))
        {
            gameObject.SetActive(true);
            return;
        }

        Invoke("RunResetCommand",2f);
    }

    private void RunResetCommand()
    {
        CommandInput.Clear();
    }

    public void SubmitPassword(UILabel value)
    {
        if (value.text != null && value.text.Equals("20180919"))
        {
            gameObject.SetActive(true);
            return;
        }
    }

    public void SubmitCheat()
    {
        if (!isCanUseCheat)
            return;

        int stagecount = StageItemManager.Instance.LastStage;

        int inputStage = 0;

        int c_part = 0;         //0부터 시작 // 입력은 1부터시작처럼 받음
        int c_stage = 0;         //0부터 시작

        if (!int.TryParse(StageInput.value, out inputStage))
            Debug.LogError("Parse Fail : SubmitCheat");

        inputStage -= 1;
        
            c_stage = stagecount < inputStage ? stagecount : inputStage;

        SSD stage_data;
        int mystar = 0;

        for (int i = 1; i <= inputStage; i++)
        {
            stage_data = GameManager.Instance.m_GameData.SSD_GetData(i);
            stage_data.BestScore = 9999;
            stage_data.BestStartCnt = 3;
            GameManager.Instance.m_GameData.SetBestStarCount(i, 3);
            GameManager.Instance.m_GameData.SSD_Save(i, stage_data);

            mystar += 3;
        }

        SecurityPlayerPrefs.SetInt("CurrenPart", c_part);
        SecurityPlayerPrefs.SetInt("CurrentStage", c_stage);
        SecurityPlayerPrefs.SetInt("MyStar", mystar);

        SecurityPlayerPrefs.SetInt("MyGold", 100000);

        int[] coverUnlockStages = { 6, 11, 22 };
        
        for (int i = 0; i < coverUnlockStages.Length; i++)
        {
            if(coverUnlockStages[i] <= inputStage)
                SecurityPlayerPrefs.SetBool("ItemCovers" + i, false);
        }
    }

    public void SubmitCheat_TimeScale()
    {
        if (!isCanUseCheat)
            return;

        float value = 0;
        if(!float.TryParse(TimeScaleInput.value, out value))
        {
            Debug.LogError("Parse Fail : SubmitCheat_TimeScale");
        }

        Time.timeScale = value;
    }

    public void OnClickReSet()
    {
        for (int i = 0; i < 600; i++)
        {
            SSD _ssd = GameManager.Instance. m_GameData.SSD_GetData(i + 1);
            _ssd.Init();
            GameManager.Instance.m_GameData.SSD_Save(i + 1, _ssd);
        }

        SecurityPlayerPrefs.SetInt("GamePalyFirst", 1);

        SecurityPlayerPrefs.SetInt("Login", 0);

        SecurityPlayerPrefs.SetInt("CurrenPart", 0);

        SecurityPlayerPrefs.SetInt("CurrentStage", 0);


        DataManager.SaveToPlayerPrefs<myfoodtruckdata>("myfoodtruckdata", null);

        SecurityPlayerPrefs.SetInt("MyGold", 0);

        SecurityPlayerPrefs.SetInt("MyStar", 0);

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 0);


        SecurityPlayerPrefs.SetInt("GPGSclearstageindex", 0);
        SecurityPlayerPrefs.SetInt("GPGSmybesttruckindex", 0);
        SecurityPlayerPrefs.SetInt("GPGSmystarindex", 0);
        SecurityPlayerPrefs.SetInt("GPGScomboindex", 0);
        SecurityPlayerPrefs.SetInt("GPGSclearpoint", 0);
        SecurityPlayerPrefs.SetInt("addfreechk", 0);
        SecurityPlayerPrefs.SetInt("raterecodechk", 0);
        SecurityPlayerPrefs.SetInt("ratestageindex", 24);

        SecurityPlayerPrefs.SetLong("FreeCoinTime", 0);

        SecurityPlayerPrefs.SetInt("FreeRewardnowindex", 0);

        SecurityPlayerPrefs.SetInt("SpecialPopchk", 0);

        for (int i = 0; i < GameManager.Instance.m_GameData.MyItemCnt.Count; i++)
        {
            SecurityPlayerPrefs.SetInt("MyItem" + i, 0);
        }

        SecurityPlayerPrefs.SetInt("SpecialPopchk", 0);
        SecurityPlayerPrefs.SetInt("Packageitem1buychk", 0);
        SecurityPlayerPrefs.SetInt("Packageitem2buychk", 0);
        SecurityPlayerPrefs.SetInt("Packageitem3buychk", 0);
        SecurityPlayerPrefs.SetInt("Packageitem4buychk", 0);

        SecurityPlayerPrefs.SetInt("FreeUsecnt", 0);
        SecurityPlayerPrefs.SetInt("FreeUseTruckGrade", 0);

        SecurityPlayerPrefs.SetInt("sharechk", 0);

        int cashItemCount = UI_ThreeMatch.Instance != null ? UI_ThreeMatch.Instance.CashItemCovers.Count : 3;
        for (int i = 0; i < cashItemCount; i++)
        {
            SecurityPlayerPrefs.SetBool("ItemCovers" + i, true);
        }
    }

    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }
}