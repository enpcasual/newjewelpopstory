﻿using UnityEngine;
using System.Collections;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Data : ScriptableObject
{
    static Data _Instance;

    public static Data Instance
    {
        get
        {
            _Instance = LoadScriptableObject<Data>("Data");

            return _Instance;
        }
    }

    //public Game m_Game = new Game();

    #region ScriptObject 사용 안함.

    //private Stage GetStage(int part, int chapter, int stage)
    //{
    //    part--;
    //    chapter--;
    //    stage--;
    //    if (part >= 0 && chapter >= 0 && stage >= 0)
    //        return m_Game.m_ListPart[part].m_ListChapter[chapter].m_ListStage[stage];
    //    else
    //    {
    //        Debug.LogError("잘못된 데이타 접근!");
    //        return null;
    //    }
    //}

    //public Stage GetStageCopy(int part, int chapter, int stage)
    //{
    //    return Cloner.DeepCopy<Stage>(GetStage(part, chapter, stage));
    //}


    //함수
#if UNITY_EDITOR
    // [MenuItem("V2R/Data/DataCreate")]
    public static void DataCreate()
    {
        //s_Instance = (Data)AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/Data.asset", typeof(Data));

        _Instance = LoadScriptableObject<Data>("Data");

        if (_Instance == null)
        {
            _Instance = CreateInstance<Data>();

            AssetDatabase.CreateAsset(_Instance, "Assets/Resources/Data/Data.asset");

            Selection.activeObject = _Instance;

            EditorUtility.DisplayDialog(
                "Create Complete",
                "It was created in \"Assets/Resources/Data/Data.asset\".",
                "ok"
            );
        }
        else
        {
            Selection.activeObject = _Instance;

            EditorUtility.DisplayDialog(
              "Exist",
              "It already exists in \"Assets/Resources/Data/Data.asset\".",
              "ok"
          );
        }
    }

    //[MenuItem("V2R/Data/ExposeForApk")]
    public static void DataSet(int aa)
    {


    }
#endif


    //로드할때 이 함수로 공용으로 사용하자!
    public static T LoadScriptableObject<T>(string path) where T : ScriptableObject
    {
        System.Type type = typeof(T);
        return Resources.Load<T>(path + "/" + type.ToString());
    }

    #endregion
}
