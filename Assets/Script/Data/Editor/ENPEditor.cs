﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ENPEditor : Editor
{

    [MenuItem("ENP/DataCreate/MissionData")]
    public static void DataModifyMissionData()
    {
        WorldData _world = new WorldData();

        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));

        int changecnt = 0;
        int index = 0;
        foreach (FileInfo file in List_File)
        {
            Stage _preStage = DataManager.LoadFromFile<Stage>(file.FullName);

            string num = file.Name.Replace(".txt", "");
            _world.m_DicMissionType[num] = _preStage.missionType;
            index++;

            changecnt++;
        }

        DataManager.SaveToFile<WorldData>("Assets/Data/Stage/A/World/WorldData.txt", _world);

        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "MissionSave",
               "Assets/Data/Stage/A/World/WorldData.txt  MissionCnt:" + changecnt,
               "OK"
           );
    }

    [MenuItem("ENP/DataCopyForApk/A_SET")]
    public static void DataCopyForApk_ASet()
    {
        DataModifyMissionData();
        DataCopyForApk("A");
    }

    [MenuItem("ENP/DataCopyForApk/B_SET")]
    public static void DataCopyForApk_BSet()
    {
        DataCopyForApk("B");
    }

    public static void DataCopyForApk(string set)
    {
        List<FileInfo> List_File = new List<FileInfo>();

        //TXT파일만 복사해야 되서 디렉토리 통짜로 복사 못함.

        //Stage txt
        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/" + set);
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));
        var enumerator = List_File.GetEnumerator();
        while (enumerator.MoveNext())
        {
            File.Copy(enumerator.Current.ToString(), Directory.GetCurrentDirectory() + "/Assets/Resources/Data/Stage/" + enumerator.Current.Name, true);
        }

        //GameData txt
        File.Copy("Assets/Data/Stage/" + set + "/World/WorldData.txt", "Assets/Resources/Data/Stage/World/WorldData.txt", true);

        //MysteryData txt
        File.Copy("Assets/Data/Stage/" + set + "/Item/MysteryData.txt", "Assets/Resources/Data/Stage/Item/MysteryData.txt", true);

        //S_TreeData txt
        File.Copy("Assets/Data/Stage/" + set + "/Item/S_TreeData.txt", "Assets/Resources/Data/Stage/Item/S_TreeData.txt", true);

        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
                "Copy Complete",
                "/Assets/Resources/Data/Stage/" + set,
                "OK"
            );
    }

    [MenuItem("ENP/DataConvertFromLlp/A_SET")]
    public static void DataConvertFromLlp_ASet()
    {
        DataConvertFromLlp("A");
    }

    [MenuItem("ENP/DataConvertFromLlp/B_SET")]
    public static void DataConvertFromLlp_BSet()
    {
        DataConvertFromLlp("B");
    }

    public static void DataConvertFromLlp(string set)
    {
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Llp/split");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));

        foreach (FileInfo file in List_File)
        {
            Level _level = DataManager.LoadFromFile<Level>(file.FullName);
            Stage _stage = ConvertFromLlp(_level);

            //*****************************
            //여기서 ReSave 한번 한다.
            //******************************
            //패널정보가 없으면 Empty 추가
            PanelSetEmpty(_stage);

            //중복 패널 제거
            PanelRepeatRemove(_stage);
            //패널 순서를 변경.
            PanelOrdering(_stage);
            //*********************************

            DataManager.SaveToFile<Stage>("Assets/Data/Stage/" + set + "/" + file.Name, _stage);
        }


        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "Covert Complete",
               "Assets/Data/Llp/split => Assets/Data/Stage/" + set,
               "OK"
           );
    }

    public static Stage ConvertFromLlp(Level _level)
    {
        Stage _stage = new Stage();

        #region 패널
        //**********
        //패널 
        //**********
        for (int i = 0, len = _level.panels.Length; i < len; i++)
        {
            switch (_level.panels[i])
            {
                case 0://BasicPanel 
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 1://EmptyPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Empty;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 2://FrostPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Ice_Cage;

                        //케이지처럼 -1값이 오네.(설명은 케이지참고) 
                        info.defence = Mathf.Clamp(_level.strengths[i], 1, 5);
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 3://LockedPanel
                    break;
                case 4://SnowPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Bread_Block;

                        //케이지처럼 -1값이 오네.(설명은 케이지참고) 
                        info.defence = Mathf.Clamp(_level.strengths[i], 1, 5);
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 5://PortalA
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Warp_In;
                        info.defence = -1;//_level.strengths[i];
                        info.value = _level.portalIndices[i];
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 6://PortalB
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Warp_Out;
                        info.defence = -1;//_level.strengths[i];
                        info.value = _level.portalIndices[i];
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 7://SnowmanPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.IceCream_Block;
                        info.defence = _level.strengths[i];
                        if (info.defence <= 0) //-1값일때가 있다..
                            info.defence = 1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 8://CagePanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Lolly_Cage;
                        //이부분 결론은 롤리팝 데이타가 개거지같다.
                        //데이타는 -1인데 소스상에서 다음처럼 처리하는 부분이 있다.
                        //_durability = Mathf.Clamp(_durability, 0, _pnd.skin.Length - 1); 
                        //그래서 데이타가 -1이여도 1단계 케이지가 만들어진다.
                        info.defence = Mathf.Clamp(_level.strengths[i], 1, 3);
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 9://FactoryPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.IceCream_Creator;
                        info.defence = _level.strengths[i];
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 10://CavePanel
                    break;
                case 11://CreatorBombPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_TimeBomb;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 12://CreatorSpiralPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_Sprial;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 13://CreatorIcecreamPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_Food;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 14://CreatorSpiralBombPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_Sprial_TimeBomb;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 15://CreatorIcecreamBombPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_Food_TimeBomb;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 16://CreatorIcecreamSpiralPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Creator_Food_Sprial;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 17://CreatorPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Creator_Empty;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 18://CakePanel A
                    {//c
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Cake_C;
                        info.defence = 3;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 19://CakePanel B
                    {//d
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Cake_D;
                        info.defence = 3;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 20://CakePanel C
                    {//a
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Cake_A;
                        info.defence = 3;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 21://CakePanel D
                    {//b
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Cake_B;
                        info.defence = 3;
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 22://ConveyorPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.ConveyerBelt;
                        info.defence = -1;//_level.strengths[i];
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 23://InvisibleObstructionPanel
                    {
                        PanelData info = new PanelData();
                        info.paneltype = PanelType.Default_Full;
                        info.defence = -1;
                        _stage.panels[i].listinfo.Add(info);

                        info = new PanelData();
                        info.paneltype = PanelType.Fixed_Block;
                        info.defence = -1;//_level.strengths[i];
                        _stage.panels[i].listinfo.Add(info);
                    }
                    break;
                case 24://ColorIcingPanel
                    break;
                case 25://ColorChangerPanel
                    break;
                //26,27 is empty.
                case 28://ColorPie5
                    break;
                case 29://ColorPie6
                    break;
                case 30://LavaCakePlatePanel
                    break;
                default:
                    Debug.LogError("패널에서 체크하지 못한 값이 존재한다.:" + _level.panels[i]);
                    break;
            }

            //미구현으로 해당 보드에 패널이 한개도 없을경우에 아이스크림생성기를 넣어준다.(작업자 확인용)
            if (_stage.panels[i].listinfo.Count == 0)
            {
                PanelData info = new PanelData();
                info.paneltype = PanelType.Default_Full;
                info.defence = -1;
                _stage.panels[i].listinfo.Add(info);

                info = new PanelData();
                info.paneltype = PanelType.IceCream_Creator;
                info.defence = -1;//_level.strengths[i];
                _stage.panels[i].listinfo.Add(info);
            }


            //포탈이 패널말고 따로 구현되어 있네. 여기서 추가 작업!
            if (((_level.portalTypes != null) && (_level.portalTypes[i] > 0)) &&
                      ((_level.portalIndices != null) && (_level.portalIndices[i] > 0)))
            {
                PanelData info = new PanelData();

                if (_level.portalTypes[i] == 1) //IN
                {
                    info.paneltype = PanelType.Warp_In;
                    info.defence = -1;//_level.strengths[i];
                    info.value = _level.portalIndices[i];
                    _stage.panels[i].listinfo.Add(info);
                }
                else if (_level.portalTypes[i] == 2) //OUT
                {
                    info.paneltype = PanelType.Warp_Out;
                    info.defence = -1;//_level.strengths[i];
                    info.value = _level.portalIndices[i];
                    _stage.panels[i].listinfo.Add(info);
                }
            }


        }

        //미션패널로 넣자.
        for (int i = 0, len = _level.shaded.Length; i < len; i++)
        {
            if (_level.shaded[i] >= 0)
            {
                PanelData info = new PanelData();
                info.paneltype = PanelType.Wafer_floor;
                info.defence = _level.shaded[i] + 1;//_level.strengths[i]; //0부터 한칸이네..아놔..진짜.
                _stage.panels[i].listinfo.Add(info);
            }
        }
        //음식 도착 패널 추가 //값은 받아오는데 Editor에 보이는 작업을 따로 해야한다.
        for (int i = 0, len = _level.treasureGoal.Length; i < len; i += 2)
        {
            int x = _level.treasureGoal[i];
            int y = (MatchDefine.MaxY - 1) - _level.treasureGoal[i + 1]; //좌표가 Y축이 반대!! ㅅㅂ
            int index = x + (y * MatchDefine.MaxX);
            PanelData info = new PanelData();
            info.paneltype = PanelType.FoodArrive;
            info.defence = -1;
            _stage.panels[index].listinfo.Add(info);
        }

        //펭귄출발 패널 추가 //값은 받아오는데 Editor에 보이는 작업을 따로 해야한다.
        for (int i = 0, len = _level.penguinSpawn.Length; i < len; i += 2)
        {
            int x = _level.penguinSpawn[i];
            int y = (MatchDefine.MaxY - 1) - _level.penguinSpawn[i + 1]; //좌표가 Y축이 반대!! ㅅㅂ
            int index = x + (y * MatchDefine.MaxX);
            PanelData info = new PanelData();
            info.paneltype = PanelType.JellyBearStart;
            info.defence = -1;
            _stage.panels[index].listinfo.Add(info);
        }

        #endregion

        #region 아이템
        //**********
        //아이템
        //**********
        //Llp는 컬러가 None과 Rnd의 구분이 없다.
        for (int i = 0, len = _level.pieces.Length; i < len; i++)
        {
            //아이템이 없는 곳처리를 startPieces로 한줄 알았으나..(정말 데이타..욕나온다.)
            //결국 패널쪽 조건 먼저 체크해서 문제가 발생하지 않는거고 데이타는 엉망이다.
            //간혹 startPieces[i]값이 false일때가 있는데.. 이값을 늦게 추가했나보다..
            //그러면 결국 답이 없네.. 개짜증나네.. 게임상 문제는 없겠지만 데이타가 깔끔하지 않은데;;
            if (_level.startPieces[i] == false)
            {
                _stage.items[i] = ItemType.None;
                _stage.colors[i] = ColorType.None;
            }
            else
            {
                //컬러가 0이면 기본값은 random처리 하고 None인것들은 아래 케이스문에서 개별처리.
                _stage.colors[i] = _level.colors[i] == 0 ? ColorType.Rnd : (ColorType)_level.colors[i];

                switch (_level.pieces[i])
                {
                    case 0: //Normal Piece
                        _stage.items[i] = ItemType.Normal;
                        break;
                    case 1: //Horizontal Piece
                        _stage.items[i] = ItemType.Line_X;
                        break;
                    case 2: //Vertical Piece
                        _stage.items[i] = ItemType.Line_Y;
                        break;
                    case 3: //Bomb Piece
                        _stage.items[i] = ItemType.Bomb;
                        break;
                    case 4: //Special Five
                        _stage.items[i] = ItemType.Rainbow;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 5: //Icecream Pop Piece
                        _stage.items[i] = ItemType.Misson_Food1;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 6: //Icecream Cone Piece
                        _stage.items[i] = ItemType.Misson_Food2;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 7: //Time Bomb Piece
                        _stage.items[i] = ItemType.TimeBomb;
                        break;
                    case 8: //SpiralSnow Piece
                        _stage.items[i] = ItemType.Donut;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 9: //Penguin Piece
                        _stage.items[i] = ItemType.JellyBear;
                        break;
                    case 10: //Mystery Piece
                        _stage.items[i] = ItemType.Mystery;
                        break;
                    case 11: //Chameleon Piece
                        _stage.items[i] = ItemType.Chameleon;
                        break;
                    case 12: //YetiBall Piece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 13: //Ghost Piece
                        _stage.items[i] = ItemType.Ghost;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 14: //Frog Piece
                        _stage.items[i] = ItemType.JellyMon;      //미구현 아이템은 스파이럴
                        break;
                    case 15: //XPiece
                        _stage.items[i] = ItemType.Line_C;
                        break;
                    case 16: //CollectPenuin Piece
                        _stage.items[i] = ItemType.Misson_Food3;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 17: //Jollyfish Piece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 18: //Rainbow Bomb Piece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 19: //Rainbow XPiece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 20: //Collect04Piece
                        _stage.items[i] = ItemType.Misson_Food4;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 21: //Collect05Piece
                        _stage.items[i] = ItemType.Misson_Food5;
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 22: //Jollyfish 2Piece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                    case 23: //Chocolate Cramel Piece
                        _stage.items[i] = ItemType.Spiral;      //미구현 아이템은 스파이럴
                        _stage.colors[i] = ColorType.None;
                        break;
                }
            }
        }
        #endregion

        #region stage 세팅
        //*******************
        //스테이지 기본 세팅
        //*******************
        //기본스폰라인
        for (int i = 0; i < _stage.defaultSpawnLine.Length; i++)
        {
            _stage.defaultSpawnLine[i] = false;
        }
        for (int i = 0, len = _level.defaultSpawnColumn.Length; i < len; i++)
        {
            _stage.defaultSpawnLine[_level.defaultSpawnColumn[i]] = true;
        }
        //푸드스폰라인
        for (int i = 0; i < _stage.foodSpawnLine.Length; i++)
        {
            _stage.foodSpawnLine[i] = false;
        }
        for (int i = 0, len = _level.treasureSpawnColumn.Length; i < len; i++)
        {
            _stage.foodSpawnLine[_level.treasureSpawnColumn[i]] = true;
        }

        //출현 컬러
        for (int i = 0; i < 6; i++)
        {
            _stage.appearColor[i] = true;
        }
        for (int i = 0, len = _level.normalProbability.Length; i < len; i++)
        {
            if (_level.normalProbability[i] > 0)
                _stage.appearColor[i] = true;
            else
                _stage.appearColor[i] = false;
        }
        //클리어 점수 목표
        _stage.scoreStar1 = _level.scoreToReach[0];
        _stage.scoreStar2 = _level.scoreToReach[1];
        _stage.scoreStar3 = _level.scoreToReach[2];


        //클리어 제한, 조건
        _stage.limit_Move = _level.allowedMoves;

        #endregion

        #region 미션세팅
        //******************
        //미션 정보
        //*******************
        //데이타는 게임이 다 만들어지고난 다음에 컨버팅하는게 맞는거 같다..
        //어쨌든 없는 미션들은 ORDER로 넣어준다.
        if (_level.isClearShadedGame)                //1.Mission : Eliminate Shaded
        {
            _stage.missionType = MissionType.Wafer;
            _stage.isWaferMission = true;

            MissionInfo info = new MissionInfo();
            info.type = MissionType.Wafer;
            info.kind = MissionKind.Wafer;
            info.count = 0;
            _stage.missionInfo.Add(info);
        }
        if (_level.isClearChocolateGame)        //2.Mission : Eliminate Snowman
        {
            _stage.missionType = MissionType.IceCream;
            _stage.isIceCreamMission = true;

            MissionInfo info = new MissionInfo();
            info.type = MissionType.IceCream;
            info.kind = MissionKind.IceCream;
            info.count = 0;
            _stage.missionInfo.Add(info);
        }
        if (_level.isClearBlockerGame)         //3.Mission : Eliminate Around Blocker
        {
            if (_level.blockers[0] > 0)  //도넛
            {
                _stage.missionType = MissionType.OrderN;
                _stage.isOrderNMission = true;              //일반오더미션이다.
                MissionInfo info = new MissionInfo();
                info.type = MissionType.OrderN;             //일반오더미션이다.
                info.kind = MissionKind.Donut;
                info.count = _level.blockers[0];
                _stage.missionInfo.Add(info);

                _stage.Donut_MinExist = _level.blockerInfos[0].minCount;
                _stage.Donut_MaxExist = _level.blockerInfos[0].maxCount;
                _stage.Donut_Interval = _level.blockerInfos[0].interval;
                _stage.Donut_SpawnCnt = _level.blockerInfos[0].spawnCount;
            }
        }
        if (_level.isClearCageGame)            //4.Mission : Eliminate Cage
        {
            _stage.missionType = MissionType.OrderN;
            _stage.isOrderNMission = true;               //일반오더미션이다.
            MissionInfo info = new MissionInfo();
            info.type = MissionType.OrderN;              //일반오더미션이다.
            info.kind = MissionKind.LollyCage;
            info.count = 0;
            _stage.missionInfo.Add(info);
        }
        if (_level.isClearSnowGame)            //5.Mission : Eliminate Snow
        {
            _stage.missionType = MissionType.OrderN;
            _stage.isOrderNMission = true;               //일반오더미션이다.
            MissionInfo info = new MissionInfo();
            info.type = MissionType.OrderN;              //일반오더미션이다.
            info.kind = MissionKind.Bread;
            info.count = 0;
            _stage.missionInfo.Add(info);
        }
        if (_level.isClearIcingGame)           //6.Mission : liminate Icing
        { }
        if (_level.isClearTimeBombGame)        //7.Mission : Eliminate TimeBomb
        {
            _stage.missionType = MissionType.OrderS;
            _stage.isOrderSMission = true;               //스폐셜오더미션이다.
            MissionInfo info = new MissionInfo();
            info.type = MissionType.OrderS;              //스폐셜오더미션이다.
            info.kind = MissionKind.TimeBomb;
            info.count = 0;
            _stage.missionInfo.Add(info);
        }
        if (_level.isClearMysteryGame)         //8.Mission : Eliminate Mystery
        { }
        if (_level.isClearCakeGame)            //9.Mission : Eliminate Cake
        { }
        if (_level.isClearChameleonGame)       //10.Mission : Eliminate Chameleon
        { }
        if (_level.isClearPieGame)             //11.Mission : Eliminate Pie
        { }
        if (_level.isGetTypesGame)             //12.Mission : Collect Jewel
        {
            _stage.missionType = MissionType.OrderN;
            _stage.isOrderNMission = true;
            for (int i = 0, len = _level.numToGet.Length; i < len; i++)
            {
                //기본보석은 index가 동일하다..
                if (_level.numToGet[i] > 0)
                {
                    MissionInfo info = new MissionInfo();
                    info.type = MissionType.OrderN;
                    info.kind = MissionKind.Red + i;
                    info.count = _level.numToGet[i];
                    _stage.missionInfo.Add(info);
                }
            }
        }
        if (_level.isTreasureGame)             //13.Mission : Collect Ingredient
        {
            _stage.missionType = MissionType.Food;
            _stage.isFoodMission = true;
            for (int i = 0, len = _level.treasures.Length; i < len; i++)
            {
                //기본보석은 index가 동일하다..
                if (_level.treasures[i] > 0)
                {
                    MissionInfo info = new MissionInfo();
                    info.type = MissionType.Food;
                    info.kind = MissionKind.StrawberryCake + i;
                    info.count = _level.treasures[i];
                    _stage.missionInfo.Add(info);
                }
            }
            //_stage.isTopCreateFood = _level.enableTreasure;
            _stage.food_MaxExist = _level.maxOnScreen;
            _stage.food_Interval = _level.movePerTreasure;
            _stage.food_SpawnCnt = _level.treasureSpawnCountPerMove;
        }
        if (_level.isSpecialJewelGame)         //14.Mission : Collect Special Jewel
        {
            _stage.missionType = MissionType.OrderS;
            _stage.isOrderSMission = true;
            for (int i = 0, len = _level.specialJewels.Length; i < len; i++)
            {
                if (_level.specialJewels[i] > 0)
                {
                    MissionInfo info = new MissionInfo();
                    info.type = MissionType.OrderS;

                    switch (i)
                    {
                        case 0://line
                            info.kind = MissionKind.Line;
                            break;
                        case 1://line+line
                            info.kind = MissionKind.Line_Line;
                            break;
                        case 2://line+bomb
                            info.kind = MissionKind.Bomb_Line;
                            break;
                        case 3://line+rainbow
                            info.kind = MissionKind.Rainbow_Line;
                            break;
                        case 4://bomb
                            info.kind = MissionKind.Bomb;
                            break;
                        case 5://bomb+bomb
                            info.kind = MissionKind.Bomb_Bomb;
                            break;
                        case 6://bomb+rainbow
                            info.kind = MissionKind.Rainbow_Bomb;
                            break;
                        case 7://rainbow
                            info.kind = MissionKind.Rainbow;
                            break;
                        case 8://rainbow+rainbow
                            info.kind = MissionKind.Rainbow_Rainbow;
                            break;
                        case 9://x
                            info.kind = MissionKind.Cross;
                            break;
                        case 10://line+x
                            info.kind = MissionKind.Cross_Line;
                            break;
                        case 11://bomb+x
                            info.kind = MissionKind.Bomb_Cross;
                            break;
                        case 12://rainbow+x
                            info.kind = MissionKind.Rainbow_Cross;
                            break;
                        case 13://x+x
                            info.kind = MissionKind.Cross_Cross;
                            break;
                    }
                    info.count = _level.specialJewels[i];
                    _stage.missionInfo.Add(info);
                }
            }
        }
        if (_level.isPenguinGame)              //15.Mission : Eliminate Penguin
        {
            _stage.missionType = MissionType.Bear;
            _stage.isBearMission = true;

            MissionInfo info = new MissionInfo();
            info.type = MissionType.Bear;
            info.kind = MissionKind.Bear;
            info.count = _level.numberOfPenguin;
            _stage.missionInfo.Add(info);

            _stage.Bear_MaxExist = _level.maxPenguinOnScreen;
            _stage.Bear_Interval = _level.movePerPenguin;
        }
        if (_level.isYetiGame)                 //16.Mission : Eliminate Yeti
        { }

        //만약 미션 처리가 다 됐다면 이부분은 지우면된다.!!
        //아직 구현안된 미션이면
        if (_stage.missionInfo.Count == 0)
        {
            _stage.missionType = MissionType.OrderN;
            _stage.isOrderNMission = true;
            for (int i = 0; i < 3; i++)
            {
                MissionInfo info = new MissionInfo();
                info.type = MissionType.OrderN;
                info.kind = MissionKind.Red + i;
                info.count = 10;
                _stage.missionInfo.Add(info);
            }
        }
        #endregion

        #region 추가 세팅
        //********************8
        //시한 폭탄 추가 세팅
        //**********************
        //_stage.isTopCreateTimeBomb = _level.enableTimeBomb;
        _stage.TimeBomb_MinExist = _level.minTimeBombCount;
        _stage.TimeBomb_MaxExist = _level.maxTimeBombCount;
        _stage.TimeBomb_Interval = _level.movePerTimeBomb;
        _stage.TimeBomb_SpawnCnt = _level.timeBombSpawnCountPerMove;
        _stage.TimeBomb_FirstCount = _level.defaultFallBackTime > 0 ? _level.defaultFallBackTime : 15;
        if (_level.enableTimeBomb)
        {
            for (int i = 0; i < _stage.TimeBombSpawnLine.Length; i++)
            {
                _stage.TimeBombSpawnLine[i] = true;
            }
        }

        //********************8
        //미스테리 추가 세팅
        //**********************
        _stage.Mystery_MinExist = _level.minMysteryPieceCount;
        _stage.Mystery_MaxExist = _level.maxMysteryPieceCount;
        _stage.Mystery_Interval = _level.movePerMysteryPiece;
        _stage.Mystery_SpawnCnt = _level.mysteryPieceSpawnCountPerMove;
        if (_level.enableMysteryPiece)
        {
            for (int i = 0; i < _stage.MysterySpawnLine.Length; i++)
            {
                _stage.MysterySpawnLine[i] = true;
            }
        }

        //********************8
        //카멜레온 추가 세팅
        //**********************
        _stage.Chameleon_MinExist = _level.minChameleonPieceCount;
        _stage.Chameleon_MaxExist = _level.maxChameleonPieceCount;
        _stage.Chameleon_Interval = _level.movePerChameleon;
        _stage.Chameleon_SpawnCnt = _level.chameleonSpawnCountPerMove;
        if (_level.enableChameleon)
        {
            for (int i = 0; i < _stage.ChameleonSpawnLine.Length; i++)
            {
                _stage.ChameleonSpawnLine[i] = true;
            }
        }
        #endregion

        return _stage;
    }

    //개발자전용 사용할때만 주석해제
    [MenuItem("ENP/DataModify_OnlyJS/VariableName")]
    public static void DataModifyVariableName()
    {
        string fromString = "Orange";
        string toString = "bbbb";
        int changecnt = 0;
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.AllDirectories));

        foreach (FileInfo file in List_File)
        {
            StreamReader sr = file.OpenText();

            string ori_data = sr.ReadToEnd();
            string chg_data = ori_data.Replace(fromString, toString);

            sr.Close();
            if (ori_data.Equals(chg_data) == false)
            {
                changecnt++;
                using (StreamWriter sw = file.CreateText())
                {
                    sw.Write(chg_data);

                    sw.Close();
                }
            }
        }


        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "VariableName Change",
               fromString + " => " + toString + " : " + changecnt.ToString(),
               "OK"
           );
    }

    //개발자전용 사용할때만 주석해제
    [MenuItem("ENP/DataModify_OnlyJS/ReSave")]
    public static void DataModifyReSave()
    {
        //데이타클래스에 변경됐었을때 데이타를 전부 다시 저장하기 위해 사용
        //삭제된 변수는 읽으면서 무시될테고 다시 저장하면서 추가할 내용이 있으면 소스가 추가되어야함.
        //일단은 데이타 구조 및 변수가 변경되어서 기존 데이타중 일부를 날리는 용도로 사용함..
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));

        int changecnt = 0;
        foreach (FileInfo file in List_File)
        {
            Stage _preStage = DataManager.LoadFromFile<Stage>(file.FullName);
            //------------ 만약 추가할께 있으면 여기서 변경!! ---------------
            //패널정보가 없으면 Empty 추가
            PanelSetEmpty(_preStage);

            //중복 패널 제거
            PanelRepeatRemove(_preStage);
            //패널 순서를 변경.
            PanelOrdering(_preStage);

            //타임폭탄라인체크추가로인해 기존 타임폭탄쓰는레벨에서 세팅.
            //if(_preStage.TimeBomb_MinExist >0 || _preStage.TimeBomb_MaxExist > 0)
            //{
            //    for(int i = 0; i<_preStage.TimeBombSpawnLine.Length; i++)
            //        _preStage.TimeBombSpawnLine[i] = true;
            //}

            //---------------------------------------------------------------
            DataManager.SaveToFile<Stage>(file.FullName, _preStage);

            changecnt++;
        }

        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "ReSave",
               "Assets/Data/Stage/A  cnt:" + changecnt,
               "OK"
           );
    }

    public static void PanelSetEmpty(Stage stage)
    {
        for (int i = 0; i < stage.panels.Length; i++)
        {
            List<PanelData> list = new List<PanelData>();

            if (stage.panels[i].listinfo.Count == 0)
            {
                PanelData info = new PanelData();
                info.paneltype = PanelType.Default_Empty;
                info.defence = -1;
                stage.panels[i].listinfo.Add(info);
            }

        }
    }

    public static void PanelRepeatRemove(Stage stage)
    {
        for (int i = 0; i < stage.panels.Length; i++)
        {
            List<PanelData> list = new List<PanelData>();

            for (int k = 0; k < stage.panels[i].listinfo.Count; k++)
            {
                if (list.Exists(find => find.paneltype == stage.panels[i].listinfo[k].paneltype))
                {
                    continue;
                }

                list.Add(stage.panels[i].listinfo[k]);
            }

            stage.panels[i].listinfo = list;
        }
    }

    public static void PanelOrdering(Stage stage)
    {
        for (int i = 0; i < stage.panels.Length; i++)
        {
            stage.panels[i].listinfo.Sort(delegate (PanelData a, PanelData b)
            {
                return a.paneltype.CompareTo(b.paneltype);
            });

            stage.panels[i].listinfo.Reverse();
        }
    }


    [MenuItem("ENP/FindStage/TimeBomb")]
    public static void FindStage_TimeBomb()
    {
        FindStage("\"" + ItemType.TimeBomb.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/Donut")]
    public static void FindStage_Donut()
    {
        FindStage("\"" + ItemType.Donut.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/Spiral")]
    public static void FindStage_Spiral()
    {
        FindStage("\"" + ItemType.Spiral.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/Bread")]
    public static void FindStage_Bread_Block()
    {
        FindStage("\"" + PanelType.Bread_Block.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/LollyCage")]
    public static void FindStage_LollyCage()
    {
        FindStage("\"" + PanelType.Lolly_Cage.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/IceCage")]
    public static void FindStage_IceCage()
    {
        FindStage("\"" + PanelType.Ice_Cage.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/IceCream")]
    public static void FindStage_IceCream()
    {
        FindStage("\"" + PanelType.IceCream_Block.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/IceCreamCreator")]
    public static void FindStage_IceCreamCreator()
    {
        FindStage("\"" + PanelType.IceCream_Creator.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/Warp")]
    public static void FindStage_Warp()
    {
        FindStage("\"" + PanelType.Warp_In.ToString() + "\"");
    }
    [MenuItem("ENP/FindStage/ConveyerBelt")]
    public static void FindStage_ConveyerBelt()
    {
        FindStage("\"" + PanelType.ConveyerBelt.ToString() + "\"");
    }

    //[MenuItem("ENP/FindStage/Mystery_Problem")]
    //public static void FindStage_Mystery()
    //{
    //    FindStage("Mystery_Normal");
    //}


    public static void FindStage(string find)
    {
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.AllDirectories));

        List<int> List_Stage = new List<int>();
        foreach (FileInfo file in List_File)
        {
            StreamReader sr = file.OpenText();

            string ori_data = sr.ReadToEnd();
            sr.Close();
            if (ori_data.Contains(find))
            {
                string num = file.Name.Replace(".txt", "");
                List_Stage.Add(int.Parse(num));
            }
        }
        //순서
        List_Stage.Sort(delegate (int a, int b)
              {
                  return a.CompareTo(b);
              });

        string stage = "";
        for (int i = 0; i < List_Stage.Count; i++)
        {
            if (i % 10 == 0)
                stage += "\n";

            stage += (List_Stage[i].ToString() + " ");
        }

        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "Find_" + find,
               "블럭, 미션(블럭과동일한미션)  갯수 :" + List_Stage.Count + stage,
               "OK"
           );
    }

    [MenuItem("ENP/FindStage/MissionInfo")]
    public static void FindStage_MissionInifo()
    {
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.AllDirectories));

        List<int> List_Stage = new List<int>();
        foreach (FileInfo file in List_File)
        {
            Stage _preStage = DataManager.LoadFromFile<Stage>(file.FullName);
            foreach (MissionInfo info in _preStage.missionInfo)
            {
                if (info.kind == MissionKind.Purple || info.kind == MissionKind.Orange)
                {
                    string num = file.Name.Replace(".txt", "");
                    List_Stage.Add(int.Parse(num));
                }
            }
        }
        //순서
        List_Stage.Sort(delegate (int a, int b)
        {
            return a.CompareTo(b);
        });

        string stage = "";
        for (int i = 0; i < List_Stage.Count; i++)
        {
            if (i % 10 == 0)
                stage += "\n";

            stage += (List_Stage[i].ToString() + " ");
        }

        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
               "찾기 완료",
               "소스에서 세팅한 미션조건인 스테이지 갯수 :" + List_Stage.Count + stage,
               "OK"
           );

    }

    [MenuItem("ENP/ShortCut/ThreematchScene #F1")]
    public static void GotoThreematchScene()
    {
        string initScenePath = Application.dataPath + "/Scene/Threematch.unity";
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene(initScenePath);
    }

    [MenuItem("ENP/ShortCut/EditorModeScene #F2")]
    public static void GotoEditorModeScene()
    {
        string initScenePath = Application.dataPath + "/Scene/EditorMode.unity";
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene(initScenePath);
    }

    [MenuItem("ENP/ShortCut/TutorialScene #F3")]
    public static void GotoTutorialScene()
    {
        string initScenePath = Application.dataPath + "/Scene/Tutorial.unity";
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene(initScenePath);
    }

    [MenuItem("ENP/MyShortcutsKey/SetActive %e")]
    public static void SetActive()
    {
        foreach (GameObject obj in Selection.objects)
        {
            obj.SetActive(!obj.activeSelf);
        }
    }

    [MenuItem("ENP/MyShortcutsKey/SavePrefab %w")]
    public static void SavePrefab()
    {
        foreach (GameObject obj in Selection.objects)
        {
            var instanceRoot = PrefabUtility.FindRootGameObjectWithSameParentPrefab(obj);
            var targetPrefab = UnityEditor.PrefabUtility.GetPrefabParent(instanceRoot);

            PrefabUtility.ReplacePrefab(
                    instanceRoot,
                    targetPrefab,
                    ReplacePrefabOptions.ConnectToPrefab
                    );
        }
    }

    [MenuItem("ENP/MyShortcutsKey/TimeScaleSpeedUp %t")]
    public static void TimeScale()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 5;
        }
    }
}
