﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelDataEditor : Editor
{
public enum ValueType
    {
        String,
        Number,
        True,
        False,
        Array,
        Null,
    }

    //실제 변수명.
    static string[] array_Valiable = {
        "Stage",        //검색은 안함. 
        "scoreStar1",
        "scoreStar2",
        "scoreStar3",
        "limit_Move",
        "missionType",
        "isOrderNMission",
        "isOrderSMission",
        "isWaferMission",
        "isFoodMission",
        "isBearMission",
        "isIceCreamMission",
        "food_MaxExist",
        "food_Interval",
        "Bear_MaxExist",
        "Bear_Interval",
        "TimeBomb_FirstCount",
        "IceCream_Interval",
        "IceCreamCreator_Interval",
    };

    //엑셀 첫번째줄에 보여질 명칭
    static string[] array_ExcelStr = {
        "Stage",        //검색은 안함. 
        "Star1",
        "Star2",
        "Star3",
        "Move",
        "mission",
        "M_OrderN",
        "M_OrderS",
        "M_Wafer",
        "M_Food",
        "M_Bear",
        "M_IceCream",
        "food_MaxExist",
        "food_Interval",
        "Bear_MaxExist",
        "Bear_Interval",
        "TimeBomb_T",
        "IceCream_I",
        "IceCreamC_I",
    };

    static HSSFWorkbook hssfwb;
    static ISheet sheet;

    #region Txt데이타에서 엑셀데이타 생성
    [MenuItem("ENP/ExcelData/Create")]
    public static void ExcelDataCreate()
    {
        //엑셀데이타 초기화
        hssfwb = new HSSFWorkbook();

        sheet = hssfwb.CreateSheet("FoodTruckMania");

        //첫번째 Row 이름 세팅..
        for (int i = 0; i < array_ExcelStr.Length; i++)
        {
            SetExcelData(0, i, array_ExcelStr[i]);
        }

        //***********************
        //Txt 파싱 -> Excel 추가
        //***********************
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));

        List_File.Sort(delegate (FileInfo a, FileInfo b)
        {
            int ia = int.Parse(a.Name.Replace(".txt", ""));
            int ib = int.Parse(b.Name.Replace(".txt", ""));
            return ia.CompareTo(ib);
        });

        List<int> List_Stage = new List<int>();
        int rowindex = 0;
        foreach (FileInfo file in List_File)
        {
            rowindex++;
            StreamReader sr = file.OpenText();

            string ori_data = sr.ReadToEnd();
            sr.Close();

            string varname = "";
            for (int i = 0; i < array_Valiable.Length; i++)
            {
                if(i == 0)
                {
                    //스테이지 번호 적는 곳
                    string num = file.Name.Replace(".txt", "");
                    SetExcelData(rowindex, i, int.Parse(num));
                    continue;
                }

                varname = "\"" + array_Valiable[i] + "\"";
                int index = ori_data.IndexOf(varname);
                index += array_Valiable[i].Length + 2 + 2; //':' +  한칸 띄어쓰기

                ValueType type = GetValueType(ori_data, index);

                switch (type)
                {
                    case ValueType.String:
                        {
                            string var = parseString(ori_data, index);
                            SetExcelData(rowindex, i, var);
                        }
                        break;
                    case ValueType.Number:
                        {

                            int var = parseNumber(ori_data, index);
                            SetExcelData(rowindex, i, var);
                        }
                        break;
                    case ValueType.True:
                    case ValueType.False:
                        {
                            string var = parseBool(ori_data, index);
                            SetExcelData(rowindex, i, var);
                        }
                        break;
                    case ValueType.Null:
                        break;
                    default:
                        break;
                }
            }

        }


        try
        {
            //************
            //엑셀 저장
            //************
            string path = string.Format("{0}{1}", Application.dataPath, "/Data/Stage/StageData.xls");
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                hssfwb.Write(file);
                file.Close();
            }
        }
        catch (System.Exception)
        {
            EditorUtility.DisplayDialog(
            "ExcelData",
            "세이브를 실패했습니다!! 엑셀이 열려있는지 확인해주세요.",
            "Create Fail"
        );
            return;
            throw;
        }



        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
            "ExcelData",
            "엑셀데이타가 생성되었습니다.\n Assets/Data/Stage/StageData.xls",
            "Create OK"
        );
    }

    static void SetExcelData<T>(int stage, int valIndex, T var)
    {
        if (sheet.GetRow(stage) == null)
            sheet.CreateRow(stage);
        IRow row = sheet.GetRow(stage);

        if (row.GetCell(valIndex) == null)
            row.CreateCell(valIndex);
        ICell cell = row.GetCell(valIndex);

        if (typeof(T) == typeof(string))
            cell.SetCellValue((string)(object)var);
        else if (typeof(T) == typeof(int))
            cell.SetCellValue((int)(object)var);

    }

    static ValueType GetValueType(string json, int index)
    {
        if (index == json.Length)
        {
            Debug.LogError("이런경우는 없을꺼다!");
            return ValueType.String;
        }

        char c = json[index];
        while(true)
        {
            switch (c)
            {
                case '"':
                    return ValueType.String;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                    return ValueType.Number;
            }


            int remainingLength = json.Length - index;

            // false
            if (remainingLength >= 5)
            {
                if (json[index] == 'f' &&
                    json[index + 1] == 'a' &&
                    json[index + 2] == 'l' &&
                    json[index + 3] == 's' &&
                    json[index + 4] == 'e')
                {
                    return ValueType.False;
                }
            }

            // true
            if (remainingLength >= 4)
            {
                if (json[index] == 't' &&
                    json[index + 1] == 'r' &&
                    json[index + 2] == 'u' &&
                    json[index + 3] == 'e')
                {
                    return ValueType.True;
                }
            }

            // null
            if (remainingLength >= 4)
            {
                if (json[index] == 'n' &&
                    json[index + 1] == 'u' &&
                    json[index + 2] == 'l' &&
                    json[index + 3] == 'l')
                {
                    return ValueType.Null;
                }
            }

            index++;

            if (index == json.Length)
            {
                Debug.LogError("이런경우는 없을꺼다!");
                return ValueType.String;
            }
        }
    }

    static string parseString(string json, int index)
    {
        string s = "";
        char c = json[index];

        //첫번째 "를 찾는다.
        while (true)
        {
            if (c == '"')
                break;
            index++;
            c = json[index];
        }

        while (true)
        {   
            index++;         //첫번째 "로 들어왔으니 그건 바로 넘긴다.
            c = json[index];     

            if (index == json.Length)
                break;
   
            if (c == '"')
                return s;

            s += c;
        }

        return null;
    }

    static int parseNumber(string json, int index)
    {
        int firstIndex = getFirstIndexOfNumber(json, index);
        int lastIndex = getLastIndexOfNumber(json, firstIndex);

        int charLength = (lastIndex - index) + 1;

        string number = json.Substring(firstIndex, charLength);

        return int.Parse(number);
    }

    //첫번째 숫자 위치를 찾는다.
    static int getFirstIndexOfNumber(string json, int index)
    {
        char c = json[index];
        while (true)
        {
            if ("0123456789+-.eE".IndexOf(c) != -1)
                break;
            index++;
            c = json[index];
        }
        return index;
    }

    static string parseBool(string json, int index)
    {
        while (true)
        {
            // false
            if (json[index] == 'f' &&
                json[index + 1] == 'a' &&
                json[index + 2] == 'l' &&
                json[index + 3] == 's' &&
                json[index + 4] == 'e')
            {
                break;
            }

            // true

            if (json[index] == 't' &&
                json[index + 1] == 'r' &&
                json[index + 2] == 'u' &&
                json[index + 3] == 'e')
            {
                break;
            }

            index++;
        }


        char c = json[index];
        string s = "";

        s += c;

        //첫번째 "를 찾는다.
        while (true)
        {
            index++;
            c = json[index];

            s += c;

            if (c == 'e')
                return s;
        }
    }

    static int getLastIndexOfNumber(string json, int index)
    {
        int lastIndex;
        for (lastIndex = index; lastIndex < json.Length; lastIndex++)
            if ("0123456789+-.eE".IndexOf(json[lastIndex]) == -1)
            {
                break;
            }
        return lastIndex - 1;
    }
    #endregion

    #region 엑셀 데이타 -> 게임 적용
    [MenuItem("ENP/ExcelData/Apply")]
    public static void ExcelDataApply()
    {     
        try
        {
            //*****************
            //엑셀 데이타 로드
            //*****************
            string path = string.Format("{0}{1}", Application.dataPath, "/Data/Stage/StageData.xls");

            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new HSSFWorkbook(file);
                file.Close();
            }

            sheet = hssfwb.GetSheet("FoodTruckMania");
        }
        catch (System.Exception)
        {
            EditorUtility.DisplayDialog(
          "ExcelData",
          "데이타 적용에 실패했습니다!! 엑셀이 열려있는지 확인해주세요.",
          "Apply Fail"
        );

            return;
            throw;
        }


        //*******************
        // TXT 파싱 및 수정
        //*******************
        List<FileInfo> List_File = new List<FileInfo>();

        DirectoryInfo di = new DirectoryInfo("Assets/Data/Stage/A");
        List_File.AddRange(di.GetFiles("*.txt", SearchOption.TopDirectoryOnly));
        List_File.Sort(delegate (FileInfo a, FileInfo b)
        {
            int ia = int.Parse(a.Name.Replace(".txt", ""));
            int ib = int.Parse(b.Name.Replace(".txt", ""));
            return ia.CompareTo(ib);
        });

        List<int> List_Stage = new List<int>();
        int rowindex = 0;
        foreach (FileInfo file in List_File)
        {
            rowindex++;
            StreamReader sr = file.OpenText();

            string ori_data = sr.ReadToEnd();
            sr.Close();

            string chg_data = string.Copy(ori_data);


            string varname = "";
            for (int i = 0; i < array_Valiable.Length; i++)
            {
                if (i == 0)
                {
                    //스테이지 번호 적는 곳
                    //string num = file.Name.Replace(".txt", "");
                    //SetExcelData(rowindex, i, int.Parse(num));
                    continue;
                }

                IRow row;
                try
                {
                    row = sheet.GetRow(rowindex);
                }
                catch (System.Exception)
                {

                    throw;
                }
                
                ICell cell = row.GetCell(i);


                varname = "\"" + array_Valiable[i] + "\"";
                int index = chg_data.IndexOf(varname);
                index += array_Valiable[i].Length + 2 + 2; //':' +  한칸 띄어쓰기


                ValueType type = GetValueType(chg_data, index);

                switch (type)
                {
                    case ValueType.String:
                        {
                            SetTxtDataString(ref chg_data, index, cell.StringCellValue);
                        }
                        break;
                    case ValueType.Number:
                        {
                            SetTxtDataInt(ref chg_data, index, (int)cell.NumericCellValue);
                        }
                        break;
                    case ValueType.True:
                    case ValueType.False:
                        {
                            SetTxtDataBool(ref chg_data, index, cell.StringCellValue);
                        }
                        break;
                    case ValueType.Null:
                        break;
                    default:
                        break;
                }
            }

            //***********
            // TXT 저장
            //***********
            if (ori_data.Equals(chg_data) == false)
            {
                using (StreamWriter sw = file.CreateText())
                {
                    sw.Write(chg_data);

                    sw.Close();
                }
            }
        }


        AssetDatabase.Refresh();

        EditorUtility.DisplayDialog(
            "ExcelData",
            "엑셀데이타가 적용되었습니다.",
            "Apply OK"
        );
    }

    static void SetTxtDataString(ref string data, int index, string str)
    {
        int firstindex = getIndexOfString(data, index) + 1;     //쌍따음표 빼고
        int lastindex = getIndexOfString(data, firstindex) -1;  //쌍따음표 빼고

        int charLength = (lastindex - firstindex) + 1;

        data = data.Remove(firstindex, charLength);
        data = data.Insert(firstindex, str);
    }

    static void SetTxtDataInt(ref string data, int index, int var)
    {
        int firstindex = getFirstIndexOfNumber(data, index);
        int lastindex = getLastIndexOfNumber(data, index);

        int charLength = (lastindex - firstindex) + 1;

        data = data.Remove(firstindex, charLength);
        data = data.Insert(firstindex, var.ToString());
    }

    static void SetTxtDataBool(ref string data, int index, string str)
    {
        int firstindex = getFirstIndexOfBool(data, index); 
        int lastindex = getLastIndexOfBool(data, firstindex); 

        int charLength = (lastindex - firstindex) + 1;

        data = data.Remove(firstindex, charLength);
        data = data.Insert(firstindex, str);
    }

    static int getIndexOfString(string json, int index)
    {
        //첫번째 "를 찾는다.
        while (true)
        {
            char c = json[index];
            if (c == '"')
                break;
            index++;
        }
        return index;
    }

    static int getFirstIndexOfBool(string json, int index)
    {
        // false
        while (true)
        {
            if (json[index] == 'f' &&
                json[index + 1] == 'a' &&
                json[index + 2] == 'l' &&
                json[index + 3] == 's' &&
                json[index + 4] == 'e')
            {
                return index;
            }

            // true

            if (json[index] == 't' &&
                json[index + 1] == 'r' &&
                json[index + 2] == 'u' &&
                json[index + 3] == 'e')
            {
                return index;
            }

            index++;
        }
    }

    static int getLastIndexOfBool(string json, int index)
    {
        //첫번째 "를 찾는다.
        while (true)
        {
            char c = json[index];
            if (c == 'e')
                break;
            index++;
        }
        return index;
    }
    #endregion
}