﻿using UnityEngine;
using System.Collections;
using JsonFx.Json;
using System;
using System.Text;
using System.IO;

public static class DataManager {

    #region Tutorial_Data
    static string Path_Tutorial_Save = "Assets/Resources/";
    static string Path_Tutorial = "Data/Tutorial/Tutorial_";

//    public static TutorialSaveInfo LoadTutorial(int stage) 
//    {
//#if UNITY_EDITOR
//        TutorialSaveInfo _info = LoadFromResource<TutorialSaveInfo>(Path_Tutorial + stage);
//        //TutorialSaveInfo _info = LoadFromFile<TutorialSaveInfo>(Path_Tutorial_Save + Path_Tutorial + stage + ".txt");
//#else
//        //apk에서 읽을때
//        TutorialSaveInfo _info = LoadFromResource<TutorialSaveInfo>(Path_Tutorial + stage);
//#endif
//        return _info;
//    }
//    public static void SaveTutorial(int stage, TutorialSaveInfo info, int set = 0)
//    {
//        SaveToFile<TutorialSaveInfo>(Path_Tutorial_Save + Path_Tutorial + stage + ".txt", info);
//    }
    #endregion

    #region Stage_Data
    //파일 Path
    static string Path_Stage = "Assets/Data/Stage/";

    public static Stage LoadStage(int stage, int set = 0)
    {
#if UNITY_EDITOR
        //파일에서 읽을때
        string dataSet = (char)(set + 65) + "/";
        Stage _stage = LoadFromFile<Stage>(Path_Stage + dataSet + stage + ".txt");
#else
        //apk에서 읽을때
        Stage _stage = LoadFromResource<Stage>("Data/Stage/" + stage);
#endif
        return _stage;
    }
    public static void SaveStage(int stage, Stage csd, int set = 0)
    {
        string dataSet = (char)(set + 65) + "/";
        SaveToFile<Stage>(Path_Stage + dataSet + stage + ".txt", csd);
    }
    #endregion

    #region Word_Data(MissionData)
    public static void SaveGame_Mission(int stage, MissionType type)
    {
        //string dataSet = (char)(set + 65) + "/";
        WorldData _data = LoadWorld();

        int index = stage -1;
        _data.m_DicMissionType[stage.ToString()] = type;

        SaveGame(_data);
    }

    public static WorldData LoadWorld(int set = 0)
    {
#if UNITY_EDITOR
        //파일에서 읽을때
        string dataSet = (char)(set + 65) + "/";
        WorldData _data = LoadFromFile<WorldData>(Path_Stage + dataSet + "World/WorldData.txt");
#else
        //apk에서 읽을때
        WorldData _data = LoadFromResource<WorldData>("Data/Stage/" + "World/WorldData");
#endif
        return _data;
    }
    public static void SaveGame(WorldData cgd, int set = 0)
    {
        string dataSet = (char)(set + 65) + "/";
        SaveToFile<WorldData>(Path_Stage + dataSet + "World/WorldData.txt", cgd);
    }

    #endregion

    #region Mystery_Data(Item Prob)
    public static MysteryData LoadMysteryData(int set = 0)
    {
#if UNITY_EDITOR
        //파일에서 읽을때
        string dataSet = (char)(set + 65) + "/";
        MysteryData _data = LoadFromFile<MysteryData>(Path_Stage + dataSet + "Item/MysteryData.txt");
#else
        //apk에서 읽을때
        MysteryData _data = LoadFromResource<MysteryData>("Data/Stage/" + "Item/MysteryData");
#endif
        return _data;
    }
    public static void SaveMysteryData(MysteryData info, int set = 0)
    {
        string dataSet = (char)(set + 65) + "/";
        SaveToFile<MysteryData>(Path_Stage + dataSet + "Item/MysteryData.txt", info);
    }
    #endregion

    #region S_Tree_Data(Item Prob)
    public static S_TreeData LoadSTreeData(int set = 0)
    {
        S_TreeData _data = null;
        try
        {
#if UNITY_EDITOR
            //파일에서 읽을때
            string dataSet = (char)(set + 65) + "/";
            _data = LoadFromFile<S_TreeData>(Path_Stage + dataSet + "Item/S_TreeData.txt");
#else
            //apk에서 읽을때
            _data = LoadFromResource<S_TreeData>("Data/Stage/" + "Item/S_TreeData");
#endif
        }
        catch
        {
            Debug.LogError("S_TreeData.txt 파일이 존재하지 않습니다.");
        }

        return _data;
    }
    public static void SaveSTreeData(S_TreeData info, int set = 0)
    {
        string dataSet = (char)(set + 65) + "/";
        SaveToFile<S_TreeData>(Path_Stage + dataSet + "Item/S_TreeData.txt", info);
    }
    #endregion

    #region 기본 관리 함수
    //Resource에서 읽어온다.
    public static T LoadFromResource<T>(string path)
    {
        TextAsset data = Resources.Load(path) as TextAsset;
        if (data != null)
        {
            T _obj = JsonReader.Deserialize<T>(data.text);
            return _obj;
        }

        return default(T);
    }


    //파일로 로드, 세이브
    public static T LoadFromFile<T>(string path) 
    {
        string data = JFileMgr.File_Read_String(path);

        T _obj = default(T);
        try
        {
            _obj = JsonReader.Deserialize<T>(data);
        }
        catch(Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("[ERROR:DataManager.LoadFromFile]{0} in FilePath({1})", e.GetType(), path);

            throw(e);
#endif
        }
        return _obj;

        //결국 자식클래스로 Deserialize가 안됨.
        //JsonReaderSettings settings = JsonDataReader.CreateSettings(true);
        //settings.TypeHintName = "__type";
        //JsonReader reader = new JsonReader(data, settings);
        //object obj = new object();
        //T _obj = JsonReader.Deserialize<T>(data);
    }

    public static void SaveToFile<T>(string path, T _obj)
    {
        string data = JsonWriter.Serialize(_obj);
        JFileMgr.File_Create_String(path, data.ToString());

        //JsonWriterSettings settings = JsonDataWriter.CreateSettings(true);
        //settings.TypeHintName = "__type";
        //StringBuilder data = new StringBuilder(string.Empty);
        //JsonWriter writer = new JsonWriter(data, settings);
        //writer.Write(_obj);
        //JFileMgr.File_Create_String(path, data.ToString());
    }


    public static T LoadFromPlayerPrefs<T>(string key)
    {
        //  string data = JFileMgr.File_Read_String(path);
        string data = SecurityPlayerPrefs.GetString(key, "-1");

        T _obj = default(T);
        if (data.Equals("-1"))
        {

        }
        else
        {
            try
            {
                _obj = JsonReader.Deserialize<T>(data);
            }
            catch (Exception e)
            {
                Debug.Log("error Load Data!!!!!!!!!!!!!!" + key + e);
            }
        }

        return _obj;
    }

    public static void SaveToPlayerPrefs<T>(string key, T _obj)
    {
        string data = JsonWriter.Serialize(_obj);

        SecurityPlayerPrefs.SetString(key, data);

        //SecurityPlayerPrefs.Save(); //for문에서 이것때문에 문제가 생긴다.

        //JFileMgr.File_Create_String(path, data);
    }

    public static string Serialize<T>(T _obj)
    {
        //string _data = JsonWriter.Serialize(_obj);

        StringBuilder sb = new StringBuilder();
        JsonWriter wr = new JsonWriter(sb);
        wr.PrettyPrint = false;
        wr.Write(_obj);

        return sb.ToString();
    }
    public static T Deserialize<T>(string data)
    {
        T _obj = JsonReader.Deserialize<T>(data);

        //StringBuilder sb = new StringBuilder();
        //JsonReader rd = new JsonReader(sb);
        //T _obj = rd.ReadObject<T>(data);

        return _obj;
    }
    #endregion

}

 