﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

public static class JFileMgr
{
//#if UNITY_ANDROID
//    public static string ROOT_DATA_PATH = Application.persistentDataPath + "/";                  //패치파일은 상대주소를 가지고 있으므로 최상위 폴더까지만 구한다.
//#elif UNITY_IPHONE
//	public static string ROOT_DATA_PATH = Application.persistentDataPath + "/";
//#endif

    //파일 유무, 생성(쓰기), 읽기, 삭제
    public static bool File_Exist(string r_path)
    {
        r_path = r_path.Replace('\\', '/');
        bool ret = File.Exists(r_path);
        return ret;
    }

    public static void File_Create(string r_path, byte[] data)
    {
        r_path = r_path.Replace('\\', '/');
        System.IO.FileStream _fs = new System.IO.FileStream(r_path, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
        _fs.Write(data, 0, data.Length);
        _fs.Flush();
        _fs.Close();
        _fs.Dispose();

        //File.WriteAllBytes(ROOT_DATA_PATH + r_path, data); //이렇게 하면 되는건가;;;
    }

    public static void File_Create_String(string r_path, string data)
    {
        r_path = r_path.Replace('\\', '/');
        File.WriteAllText(r_path, data, Encoding.UTF8);
    }

    public static string File_Read_String(string r_path)
    {
        r_path = r_path.Replace('\\', '/');
        string ret = File.ReadAllText(r_path, Encoding.UTF8);

        return ret;
    }

    public static void File_Delete(string r_path)
    {
        r_path = r_path.Replace('\\', '/');
        File.Delete(r_path);
    }


    //파일 이동
    public static void File_Move(string form_path, string to_path)
    {
        form_path = form_path.Replace('\\', '/');
        to_path = to_path.Replace('\\', '/');
        File.Move(form_path, to_path);
    }


    //디렉토리 유무, 생성
    public static bool Directory_Exist(string r_path)
    {
        r_path = r_path.Replace('\\', '/');
        bool ret = Directory.Exists(r_path);
        return ret;
    }

    public static void Directory_Create(string r_path)
    {
        r_path = r_path.Replace('\\', '/');
        Directory.CreateDirectory(r_path);
    }
}
