﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChapterSD
{
    int imsi;

    public ChapterSD()
    {
        imsi = 0;
    }
}

[System.Serializable]
public class Chapter
{
    #region SD
    public string sd_Key;

    public ChapterSD _sd;
    public ChapterSD m_SD
    {
        get
        {
            if (_sd == null)
                _sd = DataManager.LoadFromPlayerPrefs<ChapterSD>(sd_Key);

            if (_sd == null)
                _sd = new ChapterSD();

            return _sd;
        }
        set
        {
            _sd = value;
        }
    }

    public void SaveData()
    {
        DataManager.SaveToPlayerPrefs<ChapterSD>(sd_Key, m_SD);
    }
    #endregion SD

    //Stage정보
    public List<Stage> m_ListStage = new List<Stage>();
}


