﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JsonFx.Json;
using System.Text;
using System;

[System.Serializable]
public class SSD //Stage Save Data
{
    public int BestScore;
    public int BestStartCnt;
    public int FailCount; //실패를 카운터 하여 난이도 변경.

    //Analytics
    public int TotalPlayCnt;
    public int TotalClearCnt;
    public int TotalFailCnt;
    public SSD()
    {
        Init();
    }

    public void Init()
    {
        BestScore = 0;
        BestStartCnt = 0;
        FailCount = 0;
        //TotalPlayCnt = 0;
        //TotalClearCnt = 0;
        //TotalFailCnt = 0;
    }

    public void Copy(SSD s)
    {
        BestScore = s.BestScore;
        BestStartCnt = s.BestStartCnt;
        FailCount = s.FailCount; //


        TotalPlayCnt = s.TotalPlayCnt;
        TotalClearCnt = s.TotalClearCnt;
        TotalFailCnt = s.TotalFailCnt;
    }
}

[System.Serializable]
public class GameData
{
    //**************
    //StageSaveDate
    //**************
    [JsonIgnore]
    public Dictionary<int, int> BestStars = new Dictionary<int, int>(); // //SSD_GetData()로 접근하세요.
    //*******************
    //Dic_SSD 서버 저장용
    //*******************
    public string BestStartData;

    //********
    //SaveData
    //********

    //public int MyLevel;
    public int MyGold;
    public int MyStar;

    [JsonIgnore]
    public myfoodtruckdata MyFoodTruckData;
    //*******************
    //Dic_SSD 서버 저장용
    //*******************
    public int MyTruckGrade;
    public List<int> TruckIndex = new List<int>();
    public List<int> TruckGrade = new List<int>();


    public bool GamePlayFirst;//게임 처음실행시 구글플레이 자동로그인 유도
    public List<int> MyItemCnt = new List<int>();
    public bool LoginChk = false;
 
    public int HotTimeDay;
    public bool HotTimeHour1;
    public bool HotTimeHour2;
    public bool HotTimeHour3;

    public int AdsFreeDisplayCnt;//애드프리 노출 횟수
    public bool AdsFreeChk;//광고 제거를 하였는가?
    public bool RateRecodeChk;// 리뷰를 달았는가?
    public int RateStageIndex; //리뷰를 볼 스테이지 인덱스
    public bool BeforeSuperSale;//5일전에 슈퍼세일을 했나??

    public int NowMissionLevel;
    public bool TruckTutorialChk;
    public int NowClearStageIndex;

    public int NowMyBestTruckIndex;
    public int NowMyStarIndex;
    public int NowComboIndex;
    public int NowClearPointIndex;
    public int MyBestScore;

    public bool Bg_Snd;
    public bool Effect_Snd;
    public int Notification;


    public int CurrentPart;
    public int CurrentStage;


    public int FreeRewardnowindex;

    public bool Packageitem1buychk;
    public bool Packageitem2buychk;
    public bool Packageitem3buychk;
    public bool Packageitem4buychk;

    public bool sharechk;

    //Tutorial 진행 정도.
    public bool[] CashItemCover = new bool[3] { true, true, true }; // 캐쉬 아이템의 장금 상태

    //사용안함..(버그때문에 추가 했었음)
    //public void Copy(GameData data)
    //{
    //    //**************
    //    //StageSaveDate
    //    //**************
    //    //Dic_SSD.Clear();
    //    //foreach(KeyValuePair<int, SSD> dic in data.Dic_SSD)
    //    //{
    //    //    SSD _ssd = new SSD();
    //    //    _ssd.Copy(dic.Value);
    //    //    Dic_SSD[dic.Key] = _ssd;
    //    //}

    //    //*******************
    //    //Dic_SSD 서버 저장용
    //    //*******************
    //    BestStartData = data.BestStartData;

    //    //********
    //    //SaveData
    //    //********

    //    //public int MyLevel;
    //    MyGold = data.MyGold;
    //    MyStar = data.MyStar;

    //    //MyFoodTruckData;
    //    //*******************
    //    //Dic_SSD 서버 저장용
    //    //*******************
    //    MyTruckGrade = data.MyTruckGrade;
    //    for (int i = 0; i < data.TruckIndex.Count; i++)
    //    {
    //        TruckIndex.Add(data.TruckIndex[i]);
    //        TruckGrade.Add(data.TruckGrade[i]);
    //    }


    //    GamePlayFirst = data.GamePlayFirst;
    //    for (int i = 0; i < data.MyItemCnt.Count; i++)
    //    {
    //        MyItemCnt.Add(data.MyItemCnt[i]);
    //    }

    //    LoginChk = data.LoginChk;

    //    HotTimeDay = data.HotTimeDay;
    //    HotTimeHour1 = data.HotTimeHour1;
    //    HotTimeHour2 = data.HotTimeHour2;
    //    HotTimeHour3 = data.HotTimeHour3;

    //    AdsFreeDisplayCnt = data.AdsFreeDisplayCnt;
    //    AdsFreeChk = data.AdsFreeChk;
    //    RateRecodeChk = data.RateRecodeChk;
    //    BeforeSuperSale = data.BeforeSuperSale;

    //    NowMissionLevel = data.NowMissionLevel;
    //    TruckTutorialChk = data.TruckTutorialChk;
    //    NowClearStageIndex = data.NowClearStageIndex;

    //    NowMyBestTruckIndex = data.NowMyBestTruckIndex;
    //    NowMyStarIndex = data.NowMyStarIndex;
    //    NowComboIndex = data.NowComboIndex;
    //    NowClearPointIndex = data.NowClearPointIndex;
    //    MyBestScore = data.MyBestScore;

    //    Bg_Snd = data.Bg_Snd;
    //    Effect_Snd = data.Effect_Snd;
    //    Notification = data.Notification;

    //    CurrentPart = data.CurrentPart;
    //    CurrentStage = data.CurrentStage;

    //    FreeRewardnowindex = data.FreeRewardnowindex;
    //}

    #region SSD 관리 함수
    [JsonIgnore]
    string sd_key = "SSD_";
    public SSD SSD_GetData(int stage)
    {
        if (stage > 10000)
            Debug.LogError("GetData : 미션스테이지는 세이브데이타에 접근하지 않도록 해주세요!");
        SSD ssd = SSD_Load(stage);
        if (ssd == null)
            ssd = new SSD();
        return ssd;
    }

    public void SSD_Save(int stage, SSD sd)
    {
        if (stage > 10000)
            Debug.LogError("Save : 미션스테이지는 세이브데이타에 접근하지 않도록 해주세요!");
        DataManager.SaveToPlayerPrefs<SSD>(sd_key + stage.ToString(), sd);
    }

    public SSD SSD_Load(int stage)
    {
        if (stage > 10000)
            Debug.LogError("Load : 미션스테이지는 세이브데이타에 접근하지 않도록 해주세요!");
        return DataManager.LoadFromPlayerPrefs<SSD>(sd_key + stage.ToString());
    }

    //********편의 함수***********/
    public int SSD_TotalPlayCnt_Plus(int stage)
    {
        SSD ssd = SSD_GetData(stage);
        ssd.TotalPlayCnt++;
        DataManager.SaveToPlayerPrefs<SSD>(sd_key + stage, ssd);

        return ssd.TotalPlayCnt;
    }

    public int SSD_TotalClearCnt_Plus(int stage)
    {
        SSD ssd = SSD_GetData(stage);
        ssd.TotalClearCnt++;
        DataManager.SaveToPlayerPrefs<SSD>(sd_key + stage, ssd);

        return ssd.TotalClearCnt;
    }

    public int SSD_TotalFailCnt_Plus(int stage)
    {
        SSD ssd = SSD_GetData(stage);
        ssd.TotalFailCnt++;
        DataManager.SaveToPlayerPrefs<SSD>(sd_key + stage, ssd);

        return ssd.TotalFailCnt;
    }

    public int GetBestStarCount(int stage)
    {
        if (!BestStars.ContainsKey(stage))
        {
            BestStars.Add(stage, SSD_GetData(stage).BestStartCnt);
        }

        return BestStars[stage];
    }

    public void SetBestStarCount(int stage, int starCount, bool isGoogleLoad = false)
    {
        if (!BestStars.ContainsKey(stage))
        {
            BestStars.Add(stage, starCount);
        }
        else
        {
            if (BestStars[stage] < starCount)
            {
                BestStars[stage] = starCount;
            }
        }

        if (!isGoogleLoad)
        {
            BestStartData = string.Empty;
            StringBuilder strb = new StringBuilder();
            for (int i = 0; i <= StageItemManager.Instance.CurrentStage; i++)
            {
                if (!BestStars.ContainsKey(i + 1))
                {
                    BestStars.Add(i + 1, SSD_GetData(i + 1).BestStartCnt);
                }

                strb.Append(BestStars[i + 1].ToString());
                //if (CurrentStage == 0)
                //{
                //    break;
                //}
            }

            BestStartData = strb.ToString();
            SecurityPlayerPrefs.SetString("BestStartData", BestStartData);
        }
    }
    #endregion


    #region SaveData
    public void SaveData_PlayerPrefs()
    {
        //**************
        //StageSaveDate
        //**************
        //세이브 데이타 최적화.
        //Dic_SSD.Clear();
        for (int i = 1; i <= BestStartData.Length; i++)
        {
            SSD ssd = new SSD();
            ssd.BestStartCnt = BestStartData[i - 1] - '0'; //= (int)char.GetNumericValue(BestStartData[i - 1]);
            SetBestStarCount(i, ssd.BestStartCnt, true);
            SSD_Save(i, ssd);
        }

        SecurityPlayerPrefs.SetString("BestStartData", BestStartData);

        //********
        //SaveData
        //********

        //SecurityPlayerPrefs.SetInt("MyLevel", m_MyLevel);
        SecurityPlayerPrefs.SetInt("MyGold", MyGold);
        SecurityPlayerPrefs.SetInt("MyStar", MyStar);

        MyFoodTruckData = new myfoodtruckdata();
        MyFoodTruckData.m_MyFoodTruckGrade = MyTruckGrade;
        for (int i = 0; i < TruckIndex.Count; i++)
        {
            Mytruck truck = new Mytruck();
            truck.truckindex = TruckIndex[i];
            truck.truckgrade = TruckGrade[i];

            MyFoodTruckData.gradelist.Add(truck);
        }
        DataManager.SaveToPlayerPrefs("myfoodtruckdata", MyFoodTruckData);

        SecurityPlayerPrefs.SetInt("GamePalyFirst", GamePlayFirst == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("Login", LoginChk == false ? 1 : 0);

        SecurityPlayerPrefs.SetInt("HotTimeDay", HotTimeDay);
        SecurityPlayerPrefs.SetInt("HotTimeHour1", HotTimeHour1 == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("HotTimeHour2", HotTimeHour2 == true ? 1: 0);
        SecurityPlayerPrefs.SetInt("HotTimeHour3", HotTimeHour3 == true ? 1: 0);

        SecurityPlayerPrefs.SetInt("Addfreepopupcnt", AdsFreeDisplayCnt);
        SecurityPlayerPrefs.SetInt("addfreechk", AdsFreeChk == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("raterecodechk", RateRecodeChk == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("ratestageindex", RateStageIndex);
        
        SecurityPlayerPrefs.SetInt("beforesupersale", BeforeSuperSale == true ? 1 : 0);

        SecurityPlayerPrefs.SetInt("nowmissionlevel", NowMissionLevel);
        SecurityPlayerPrefs.SetInt("trucktutorialchk", TruckTutorialChk == true ? 1 : 0);

        SecurityPlayerPrefs.SetInt("GPGSclearstageindex", NowClearStageIndex);
        SecurityPlayerPrefs.SetInt("GPGSmybesttruckindex", NowMyBestTruckIndex);
        SecurityPlayerPrefs.SetInt("GPGSmystarindex", NowMyStarIndex);
        SecurityPlayerPrefs.SetInt("GPGScomboindex", NowComboIndex);
        SecurityPlayerPrefs.SetInt("GPGSclearpoint", NowClearPointIndex);
        SecurityPlayerPrefs.SetInt("GPGSmybestscore", MyBestScore);

        SecurityPlayerPrefs.SetInt("isBGM", Bg_Snd == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("notification", Notification);
        SecurityPlayerPrefs.SetInt("isEffect", Effect_Snd == true ? 1 : 0);

        Debug.Log("CurrentPart : " + CurrentPart);
        Debug.Log("CurrentStage : " + CurrentStage);
        SecurityPlayerPrefs.SetInt("CurrenPart", CurrentPart);
        SecurityPlayerPrefs.SetInt("CurrentStage", CurrentStage);
        SecurityPlayerPrefs.SetInt("FreeRewardnowindex", FreeRewardnowindex);


        SecurityPlayerPrefs.SetInt("Packageitem1buychk", Packageitem1buychk == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("Packageitem2buychk", Packageitem2buychk == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("Packageitem3buychk", Packageitem3buychk == true ? 1 : 0);
        SecurityPlayerPrefs.SetInt("Packageitem4buychk", Packageitem4buychk == true ? 1 : 0);

        
        SecurityPlayerPrefs.SetInt("sharechk", sharechk == true ? 1 : 0);


        for (int i = 0; i < 3; i++)
        {
            SecurityPlayerPrefs.SetInt("MyItem" + i, MyItemCnt[i]);
        }

        //튜토리얼 정보 
        int cashItemCount = UI_ThreeMatch.Instance != null ? UI_ThreeMatch.Instance.CashItemCovers.Count : 3;
        for (int i = 0; i < cashItemCount; i++)
        {
            SecurityPlayerPrefs.SetBool("ItemCovers" + i, CashItemCover[i]);
        }
        



        SecurityPlayerPrefs.Save();
    }

    public void LoadData_PlayerPrefs(bool isGoogleSave = false)
    {
        //**************
        //StageSaveDate
        //**************

        if (isGoogleSave)
        {
            int stageIndex = 0;
            SSD _sd = null;
            BestStartData = string.Empty;
            StringBuilder strb = new StringBuilder();
            foreach (KeyValuePair<string, MissionType> dic in GameManager.Instance.m_WorldData.m_DicMissionType)
            {
                //일반 레벨 데이타 로드!
                stageIndex++;
                _sd = SSD_GetData(stageIndex);

                //서버에 저장될 데이타
                strb.Append(_sd.BestStartCnt.ToString());
            }

            BestStartData = strb.ToString();
        }
        else
        {
            BestStartData = SecurityPlayerPrefs.GetString("BestStartData", string.Empty);
            for (int i = 1; i <= BestStartData.Length; i++)
            {
                int BestStartCnt = BestStartData[i - 1] - '0'; //= (int)char.GetNumericValue(BestStartData[i - 1]);
                SetBestStarCount(i, BestStartCnt, true);
            }
        }

        //********
        //SaveData
        //********

        //m_MyLevel = SecurityPlayerPrefs.GetInt("MyLevel", 0);
        MyGold = SecurityPlayerPrefs.GetInt("MyGold", 0);
        MyStar = SecurityPlayerPrefs.GetInt("MyStar", 0);
        MyFoodTruckData = DataManager.LoadFromPlayerPrefs<myfoodtruckdata>("myfoodtruckdata");
        if (isGoogleSave)
        {
            MyTruckGrade = MyFoodTruckData.m_MyFoodTruckGrade;

            TruckIndex.Clear();
            TruckGrade.Clear();          
            for (int i = 0; i<MyFoodTruckData.gradelist.Count; i++)
            {
                TruckIndex.Add(MyFoodTruckData.gradelist[i].truckindex);
                TruckGrade.Add(MyFoodTruckData.gradelist[i].truckgrade);
            }
        }

        GamePlayFirst = SecurityPlayerPrefs.GetInt("GamePalyFirst", 1) == 1 ? true : false;
        LoginChk = SecurityPlayerPrefs.GetInt("Login", 0) == 1 ? true : false;
        //LoginChk = Social.localUser.authenticated;

        HotTimeDay = SecurityPlayerPrefs.GetInt("HotTimeDay", 0);
        HotTimeHour1 = SecurityPlayerPrefs.GetInt("HotTimeHour1", 0) == 1 ? true : false;
        HotTimeHour2 = SecurityPlayerPrefs.GetInt("HotTimeHour2", 0) == 1 ? true : false;
        HotTimeHour3 = SecurityPlayerPrefs.GetInt("HotTimeHour3", 0) == 1 ? true : false;

        AdsFreeDisplayCnt = SecurityPlayerPrefs.GetInt("Addfreepopupcnt", 0);
        AdsFreeChk = SecurityPlayerPrefs.GetInt("addfreechk", 0) == 1 ? true : false;
        RateRecodeChk = SecurityPlayerPrefs.GetInt("raterecodechk", 0) == 1 ? true : false;
        RateStageIndex = SecurityPlayerPrefs.GetInt("ratestageindex", 24);
        BeforeSuperSale = SecurityPlayerPrefs.GetInt("beforesupersale", 0) == 1 ? true : false;

        NowMissionLevel = SecurityPlayerPrefs.GetInt("nowmissionlevel", 0);
        TruckTutorialChk = SecurityPlayerPrefs.GetInt("trucktutorialchk", 0) == 1 ? true : false;

        NowClearStageIndex = SecurityPlayerPrefs.GetInt("GPGSclearstageindex", 0);
        NowMyBestTruckIndex = SecurityPlayerPrefs.GetInt("GPGSmybesttruckindex", 0);
        NowMyStarIndex = SecurityPlayerPrefs.GetInt("GPGSmystarindex", 0);
        NowComboIndex = SecurityPlayerPrefs.GetInt("GPGScomboindex", 0);
        NowClearPointIndex = SecurityPlayerPrefs.GetInt("GPGSclearpoint", 0);
        MyBestScore = SecurityPlayerPrefs.GetInt("GPGSmybestscore", 0);

        Bg_Snd = SecurityPlayerPrefs.GetInt("isBGM", 1) == 1 ? true : false;
        Notification = SecurityPlayerPrefs.GetInt("notification", 1);
        Effect_Snd = SecurityPlayerPrefs.GetInt("isEffect", 1) == 1 ? true : false;

        CurrentPart = SecurityPlayerPrefs.GetInt("CurrenPart", 0);
        CurrentStage = SecurityPlayerPrefs.GetInt("CurrentStage", 0);
        
        
        
        //StageItemManager.Instance.CurrentPart = CurrentPart;
        //StageItemManager.Instance.CurrentStage = CurrentStage;
        FreeRewardnowindex = SecurityPlayerPrefs.GetInt("FreeRewardnowindex", 0);

        Packageitem1buychk = SecurityPlayerPrefs.GetInt("Packageitem1buychk", 0) == 1 ? true : false;
        Packageitem2buychk = SecurityPlayerPrefs.GetInt("Packageitem2buychk", 0) == 1 ? true : false;
        Packageitem3buychk = SecurityPlayerPrefs.GetInt("Packageitem3buychk", 0) == 1 ? true : false;
        Packageitem4buychk = SecurityPlayerPrefs.GetInt("Packageitem4buychk", 0) == 1 ? true : false;

        sharechk = SecurityPlayerPrefs.GetInt("sharechk", 0) == 1 ? true : false;


        if (GamePlayFirst)
        {
            for (int i = 0; i < 3; i++)
            {
                SecurityPlayerPrefs.SetInt("MyItem" + i, 2);
            }
        }

        MyItemCnt.Clear();
        for (int i = 0; i < 3; i++)
        {
            int itemcnt = SecurityPlayerPrefs.GetInt("MyItem" + i, 0);
            MyItemCnt.Add(itemcnt);
        }

        //튜토리얼 정보 
        CashItemCover = new bool[3];
        int cashItemCount = UI_ThreeMatch.Instance != null ? UI_ThreeMatch.Instance.CashItemCovers.Count : 3;
        for (int i = 0; i < cashItemCount; i++)
        {
            CashItemCover[i] = (SecurityPlayerPrefs.GetBool("ItemCovers" + i, true));
        }

    }
    #endregion

  
}
