﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonFx.Json;

public class WorldData{

    //public List<MissionType> m_ListMissionType;
    public Dictionary<string, MissionType> m_DicMissionType; //키값이 int이면 Serialize 할수 없다

    public WorldData()
    {
        //m_ListMissionType = new List<MissionType>();
        m_DicMissionType = new Dictionary<string, MissionType>();
    }
}
