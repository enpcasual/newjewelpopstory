﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonFx.Json;

public enum S_TreeSettingType
{
    Basic = 0,
    Easy,
    Normal,
    Hard,
    //추가자리
    Last_Test,
}

public enum S_TreeItemType
{
    //순서 변동 불가!!
    //추가만 가능!!

    Normal = 0,
    Line_X,
    Line_Y,
    Line_C,
    Bomb,
    Rainbow,
    Donut,
    Spiral,
    JellyBear,
    TimeBomb,
    Mystery,
    Chameleon,
    JellyMon,
    Ghost,
    Key,
    //추가~~


    Last,
}

public class S_TreeData {

    public List<int[]> List_Prob;

    [JsonIgnore]
    public int[] total;

    public S_TreeData()
    {
        List_Prob = new List<int[]>();

        for (int i = 0; i <= (int)S_TreeSettingType.Last_Test; i++)
        {
            List_Prob.Add(new int[(int)(S_TreeItemType.Last)]);
        }
    }

    public void InitTotal()
    {
        total = new int[List_Prob.Count];

        int[] _prob = null;
        int _sum = 0;
        for(int i = 0; i < List_Prob.Count; i++)
        {
            _prob = List_Prob[i];
            _sum = 0;
            for (int k = 0; k < _prob.Length; k++)
            {
                //전체 합산
                _sum += _prob[k];
            }

            total[i] = _sum;
        }

    }
}
