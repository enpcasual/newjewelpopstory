﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonFx.Json;

public enum MysterySettingType
{
    Mystery_Basic,
    Mystery_Easy,
    Mystery_Normal,
    Mystery_Hard,
    Mystery_Candy,
    Mystery_Special,
    Mystery_Icecream,
    Mystery_IceCreamCreator,
    Mystery_Bear,
    //추가자리
    Mystery_Last_Test,
}

public enum MysteryItemType
{
    //순서 변동 불가!!
    //추가만 가능!!

    Normal = 0,
    Line,
    Bomb,
    XLine,
    Rainbow,
    Normal_Ice,
    Line_Ice,
    Bomb_Ice,
    XLine_Ice,
    Rainbow_Ice,
    Bread1,
    Bread2,
    Bread3,
    Bread4,
    Bread5,
    IceCream,
    JellyBear,
    Spiral,
    TimeBomb,
    Normal_Lolly,
    Chameleon,
    //추가~~


    Last,
}

public class MysteryData {

    public List<int[]> List_Prob;

    [JsonIgnore]
    public int[] total;
    [JsonIgnore]
    public int[] totalonlyItem; //컨베이어 벨트위일때는 이걸 써야함.

    public MysteryData()
    {
        List_Prob = new List<int[]>();

        for (int i = 0; i <= (int)MysterySettingType.Mystery_Last_Test; i++)
        {
            List_Prob.Add(new int[(int)(MysteryItemType.Last)]);
        }
    }

    public void InitTotal()
    {
        total = new int[List_Prob.Count];
        totalonlyItem = new int[List_Prob.Count];

        int[] _prob = null;
        int _sum = 0;
        int _sumofcvb = 0;
        for(int i = 0; i < List_Prob.Count; i++)
        {
            _prob = List_Prob[i];
            _sum = 0;
            _sumofcvb = 0;
            for (int k = 0; k < _prob.Length; k++)
            {
                //전체 합산
                _sum += _prob[k];

                //아이템만 합산
                switch ((MysteryItemType)k)
                {
                    case MysteryItemType.Normal:
                    case MysteryItemType.Line:
                    case MysteryItemType.Bomb:
                    case MysteryItemType.XLine:
                    case MysteryItemType.Rainbow:
                    case MysteryItemType.JellyBear:
                    case MysteryItemType.Spiral:
                    case MysteryItemType.TimeBomb:
                    case MysteryItemType.Chameleon:
                        _sumofcvb += _prob[k];
                        break;
                    default:
                        break;
                }
            }

            total[i] = _sum;
            totalonlyItem[i] = _sumofcvb;
        }

    }
}
