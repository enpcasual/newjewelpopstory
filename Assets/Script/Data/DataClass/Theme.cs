﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ThemeData
{
    int imsi;

    public ThemeData()
    {
        imsi = 0;
    }
}

[System.Serializable]
public class Theme
{
    #region SD
    public string sd_Key;

    public ThemeData _sd;
    public ThemeData m_SD
    {
        get
        {
            if (_sd == null)
                _sd = DataManager.LoadFromPlayerPrefs<ThemeData>(sd_Key);

            if (_sd == null)
                _sd = new ThemeData();

            return _sd;
        }
        set
        {
            _sd = value;
        }
    }

    public void SaveData()
    {
        DataManager.SaveToPlayerPrefs<ThemeData>(sd_Key, m_SD);
    }
    #endregion SD

    public Theme()
    {
        listMissionType = new List<MissionType>();
    }

    //챕터는 추후작업
    //public List<Chapter> m_ListChapter = new List<Chapter>();


    public List<MissionType> listMissionType;


}