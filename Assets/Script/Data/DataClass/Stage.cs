﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JsonFx.Json;


[System.Serializable]
public class Stage
{
    //*******************
    //보드 세팅
    //*******************
    public List<DROP_DIR[]> DropDirs = new List<DROP_DIR[]>();
    public bool isUseGravity = false;
    public Pannels[] panels;

    public ItemType[] items;
    public ColorType[] colors;

    public List<int> focus;

    //*******************
    //스테이지 기본 세팅
    //*******************
    public bool[] defaultSpawnLine; //아이템기본드랍라인 //dropSpawnLine
    public bool[] foodSpawnLine;    //푸드 드랍라인
    public bool[] SpiralSpawnLine;  //스파이럴 드랍라인
    public bool[] DonutSpawnLine;   //도넛 드랍라인
    public bool[] TimeBombSpawnLine;//시한폭탄 드랍라인
    public bool[] MysterySpawnLine; //미스테리 드랍라인
    public bool[] ChameleonSpawnLine; //미스테리 드랍라인
    public bool[] KeySpawnLine;     //열쇠 드랍라인

    //Y값을 배열로 안만들고 새로 만든 이유는 기존의 세이브 데이터를 로드하지 못하기 때문에
    //나중에 컴버터 기능을 추가하여 배열로 수정하여야함.
    public bool[] defaultSpawnLineY; //아이템기본드랍라인 //dropSpawnLine
    public bool[] foodSpawnLineY;    //푸드 드랍라인
    public bool[] SpiralSpawnLineY;  //스파이럴 드랍라인
    public bool[] DonutSpawnLineY;   //도넛 드랍라인
    public bool[] TimeBombSpawnLineY;//시한폭탄 드랍라인
    public bool[] MysterySpawnLineY; //미스테리 드랍라인
    public bool[] ChameleonSpawnLineY; //미스테리 드랍라인
    public bool[] KeySpawnLineY;     //열쇠 드랍라인

    public bool[] appearColor;      //출현컬러
    public long scoreStar1;        //클리어 점수 목표
    public long scoreStar2;
    public long scoreStar3;
    public int limit_Move;          //클리어 제한, 조건

    //************
    //미션 정보
    //************
    public MissionType missionType; //단순히 월드맵에서 보여주는 부분으로 추후에 월드맵쪽으로 정보빼야함.

    //미션 체크
    public bool isOrderNMission; //>>모으기N
    public bool isOrderSMission; //>>모으기S
    public bool isWaferMission; //>>와퍼   
    public bool isFoodMission; //>>음식(Food)
    public bool isBearMission; //>>곰 모으기
    public bool isIceCreamMission;  //>>아이스크림전멸
    public bool isJamMission;   //>>쨈으로 덮기

    //미션 정보
    public List<MissionInfo> missionInfo;
  
    //음식 추가 변수
    //public bool isTopCreateFood;
    public int food_MaxExist;//maxExistFood;  //최대 음식 갯수
    public int food_Interval;//spawnInterval; //음식 출현 인터벌
    public int food_SpawnCnt;//onceSpawnCnt;  //현재 게임상에 max보다 갯수가 작을때 한번에 출현할수 있는 수. 1 ~ maxcnt

    //스파이럴 드랍조건
    //public bool isTopCreateSpiral;
    public int Spiral_MinExist;
    public int Spiral_MaxExist;
    public int Spiral_Interval;
    public int Spiral_SpawnCnt;

    //도넛 드랍조건
    //public bool isTopCreateDonut;
    public int Donut_MinExist;
    public int Donut_MaxExist;
    public int Donut_Interval;
    public int Donut_SpawnCnt;

    //시한폭탄 드랍조건
    //public bool isTopCreateTimeBomb;
    public int TimeBomb_MinExist;
    public int TimeBomb_MaxExist;
    public int TimeBomb_Interval;
    public int TimeBomb_SpawnCnt;
    public int TimeBomb_FirstCount;

    //젤리베어 생성조건
    public int Bear_MaxExist;
    public int Bear_Interval;

    //아이스크림 증식,생성조건
    public int IceCream_Interval;
    public int IceCreamCreator_Interval;

    //미스테리 생성조건
    public MysterySettingType Mystery_SettingType;
    public int Mystery_MinExist;
    public int Mystery_MaxExist;
    public int Mystery_Interval;
    public int Mystery_SpawnCnt;

    //카멜레온 생성조건
    public int Chameleon_MinExist;
    public int Chameleon_MaxExist;
    public int Chameleon_Interval;
    public int Chameleon_SpawnCnt;

    //열쇠 생성조건
    public int Key_MinExist;
    public int Key_MaxExist;
    public int Key_Interval;
    public int Key_SpawnCnt;

    //스페셜트리아이템생성확률
    public S_TreeSettingType S_Tree_SettingType;

    //보석트리아이템생성
    public ItemType[] JewelTreeItem;
    public ColorType[] JewelTreeItemColor;

    public Stage()
    {
        panels = new Pannels[81];
        for (int i = 0; i < 81; i++)
        {
            panels[i] = new Pannels();
            panels[i].listinfo = new List<PanelData>();
        }
        items = new ItemType[81];
        colors = new ColorType[81];

        focus = new List<int>();

        defaultSpawnLine = new bool[9];
        foodSpawnLine = new bool[9];
        SpiralSpawnLine = new bool[9];
        DonutSpawnLine = new bool[9];
        TimeBombSpawnLine = new bool[9];
        MysterySpawnLine = new bool[9];
        ChameleonSpawnLine = new bool[9];
        KeySpawnLine = new bool[9];

        defaultSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        foodSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        SpiralSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        DonutSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        TimeBombSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        MysterySpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        ChameleonSpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };
        KeySpawnLineY = new bool[9] { true, true, true, true, true, true, true, true, true, };

        appearColor = new bool[6];

        missionInfo = new List<MissionInfo>();

        //기본값
        IceCream_Interval = 1;
        IceCreamCreator_Interval = 1;
        TimeBomb_FirstCount = 15;

        //보석트리아이템생성
        JewelTreeItem = new ItemType[4];
        JewelTreeItemColor = new ColorType[4];
        for (int i = 0; i < 4; i++)
        {
            JewelTreeItem[i] = ItemType.Line_X;
            JewelTreeItemColor[i] = ColorType.Rnd;
        }
    }

    public void CreateGravity()
    {
        for (int i = 0; i < 81; i++)
        {
            DropDirs.Add(new DROP_DIR[1]);
            DropDirs[i][0] = DROP_DIR.U;
        }
    }
}

[System.Serializable]
public class Pannels
{
    public List<PanelData> listinfo;
}

[System.Serializable]
public class PanelData
{
    public PanelType paneltype;
    public int defence; //Defence는 이미지가 달라지므로 함부로 용도 변경 불가!!
    public int value;   //각 패널마다 추가 값이 필요한 경우 사용 가능!
    public string addData;
}

//public enum ConveyerType
public class ConveyerData
{

    public int conveyertype;
}

public class Items
{
    public List<ItemData> listinfo;
}

public class ItemData
{
    public ItemType items;
    public ColorType colors;
    public int value;
}

[System.Serializable]
public class MissionInfo
{
    public MissionType type;
    public MissionKind kind;
    public int count;

    public void Copy(MissionInfo info)
    {
        type = info.type;
        kind = info.kind;
        count = info.count;
    }
}