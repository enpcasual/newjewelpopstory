﻿using UnityEngine;
using System.Collections;

public enum LoadingType
{
    Circle,
    Icon,
}

public class Loading : MonoBehaviour
{
    public static Loading Instance;

    public GameObject root;
    public bool m_isShow = false;

    public GameObject loading1;
    public GameObject loading2;

    public void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        if (m_isShow == false)
            root.SetActive(m_isShow);
    }

    public void SetShow(bool v, LoadingType type = LoadingType.Circle)
    {
        Debug.Log("Loading Set : " + v.ToString());
        root.SetActive(v);

        m_isShow = v;

        if (v == true)
        {
            if (type == LoadingType.Circle)
            {
                loading1.SetActive(true);
                loading2.SetActive(false);
            }
            else if (type == LoadingType.Icon)
            {
                loading1.SetActive(false);
                loading2.SetActive(true);
            }
        }
    }

}
