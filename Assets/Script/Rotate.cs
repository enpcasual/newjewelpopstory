﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Transform m_myTransform;
    private Transform myTransform
    {
        get
        {
            if (m_myTransform == null)
                m_myTransform = transform;

            return m_myTransform;
        }
    }


    void Update()
    {
        myTransform.Rotate(Vector3.forward, speed * Time.deltaTime);
    }
}