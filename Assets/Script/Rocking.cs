﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//Terget을 까딱까닥하게 회전시키는 애니메이션
//frog2(개구리) 오브잭트와, 케릭터 오브젝트에서 사용.
public class Rocking : WorldObjectAnimation
{
    public GameObject Terget = null;
    public override bool IsPlay { get { return m_IsPlay; } }

    private Vector3 m_StartPos = Vector3.zero;
    public int RotateZ_Max = 10;
    public int RotateZ_Min = -10;

    public float DelayTime = 0.1f;
    public float RepeatTime = 1f;

    private void Awake()
    {
        m_StartPos = transform.localPosition;
    }

    private void Start()
    {
        Init();
        if (IsLoop)
        {
            Actor();
        }
    }

    private void OnEnable()
    {
        if (IsLoop)
        {
            Actor();
        }
    }

    public void Init()
    {
        if(Terget == null)
        {
            Terget = this.gameObject;
        }
    }

    private void OnDisable()
    {
        if (m_StartPos != Vector3.zero)
        {
            transform.localPosition = m_StartPos;
        }

        m_IsPlay = false;
    }

    public override void Actor()
    {
        if (gameObject.activeSelf && !m_IsPlay)
        {
            m_IsPlay = true;
            StartCoroutine(RotateRight());
        }
    }

    private IEnumerator RotateLeft()
    {
        yield return new WaitForSeconds(RepeatTime);

        float x = transform.localRotation.eulerAngles.x;
        float y = transform.localRotation.eulerAngles.y;

        Terget.transform.DOLocalRotate(new Vector3(x, y, RotateZ_Max), RepeatTime);

        if (IsLoop)
        {
            StartCoroutine(RotateRight());
        }
        else
        {
            m_IsPlay = false;
        }
    }

    private IEnumerator RotateRight()
    {
        yield return new WaitForSeconds(DelayTime);
        yield return new WaitForSeconds(RepeatTime);

        float x = transform.localRotation.eulerAngles.x;
        float y = transform.localRotation.eulerAngles.y;

        Terget.transform.DOLocalRotate(new Vector3(x, y, RotateZ_Min), RepeatTime);
        StartCoroutine(RotateLeft());
    }
}
