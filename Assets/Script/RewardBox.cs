﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RewardBox : MonoBehaviour {

    [SerializeField]
    private GameObject m_RewardBoxObj = null;
    [SerializeField]
    private GameObject m_ShaftObj = null;

    [SerializeField]
    private int m_StartDelayTiemMin = 30;
    [SerializeField]
    private int m_StartDelayTiemMax = 120;
    private int m_StartDelayTiem = 30;
    [SerializeField]
    private int m_ExtensionDelayTiem = 3;

    [SerializeField]
    private float m_Time = 0;

    public bool isPaly = false;

    [SerializeField]
    private int MaxCount = 1;
    [SerializeField]
    private int Count = 0;
    private FreeBox m_CurrentReward = null;

    [SerializeField]
    private GameObject[] ChshItemEffects = new GameObject[3];

    [SerializeField]
    private ParticleSystem Shine = null;

    [SerializeField]
    private int Percentage = 7;

    [SerializeField]
    private int VisibleTime = 7;


    private void OnEnable()
    {
        Init();
    }

    public void ReStart()
    {
        Init();
    }

    public void Init()
    {
        if (Percentage > Random.Range(0, 100) && MatchManager.Instance != null && MatchManager.Instance.m_StageIndex > 22)
        {
            Count = 0;
            m_Time = 0;
        }
        else
        {
            Count = MaxCount;
        }

        if (m_Sequence != null)
        {
            m_Sequence.Kill();
        }

        m_StartDelayTiem = Random.Range(m_StartDelayTiemMin, m_StartDelayTiemMax);
        m_ShaftObj.transform.localPosition = new Vector3(650, 0);
        isPaly = false;
        m_RewardBoxObj.SetActive(false);
        CashItemEffectdisable();
    }

    public void ChooseCurrentReward(bool isMoveChoose)
    {
        int total = 0;
        m_CurrentReward = null;
        for (int i = 0; i < TableDataManager.Instance.m_FreeBoxlist.Count; i++)
        {
            if (isMoveChoose)
            {
                total += TableDataManager.Instance.m_FreeBoxlist[i].rate2;
                TableDataManager.Instance.m_FreeBoxlist[i].accumulateRate2 = total;
            }
            else
            {
                total += TableDataManager.Instance.m_FreeBoxlist[i].rate1;
                TableDataManager.Instance.m_FreeBoxlist[i].accumulateRate1 = total;
            }
        }
        int range =  Random.Range(0, total);
        for (int i = 0; i < TableDataManager.Instance.m_FreeBoxlist.Count; i++)
        {
            if (isMoveChoose && range < TableDataManager.Instance.m_FreeBoxlist[i].accumulateRate2)
                m_CurrentReward = TableDataManager.Instance.m_FreeBoxlist[i];
            else if(range < TableDataManager.Instance.m_FreeBoxlist[i].accumulateRate1)
                m_CurrentReward = TableDataManager.Instance.m_FreeBoxlist[i];

            if (m_CurrentReward != null)
                return;
        }

        if (m_CurrentReward == null) m_CurrentReward = TableDataManager.Instance.m_FreeBoxlist[TableDataManager.Instance.m_FreeBoxlist.Count - 1];
    }

    private void Update()
    {
        if (!isPaly && Count < MaxCount)// && !TutorialManager.Instance.isTutorial)
        {
            m_Time += Time.deltaTime;

            if (m_StartDelayTiem < m_Time && MatchManager.Instance.m_StepType == StepType.Wait && MatchManager.Instance.m_MatchState == MatchState.Playing)
            {
                PlayRewardBox(false);
            }
        }
    }

    private Sequence m_Sequence = null;
    public void PlayRewardBox(bool isMoveChoose)
    {
        m_Time = 0;
        
        if(m_Sequence != null)
        {
            m_Sequence.Kill();
        }

        m_StartDelayTiem += m_ExtensionDelayTiem;

        m_RewardBoxObj.SetActive(true);

        Count++;
        isPaly = true;

        ChooseCurrentReward(isMoveChoose);

        float random = (Random.value - 0.5f) * 200f;
        m_ShaftObj.transform.localPosition = new Vector3(650, random);

        random = (Random.value - 0.5f) * 200f;
        //tween = m_RewardBox.transform.DOLocalMove(, 15);
        Vector3 pos1 = new Vector3(-650, random);
        Vector3 pos2 = (pos1 + m_ShaftObj.transform.localPosition) * 0.5f;

        //Moveto는 목표점으로 이동, Moveby는 내가 얼마만큼 이동
        m_Sequence = DOTween.Sequence();
        //seq.Append(bf.transform.DOScale(2f, 1f));
        m_Sequence.Append(m_ShaftObj.transform.DOLocalMove(pos2, VisibleTime));
        m_Sequence.Insert(VisibleTime * 2, m_ShaftObj.transform.DOLocalMove(pos1, VisibleTime));
        m_Sequence.OnComplete(() =>
        {
            m_RewardBoxObj.SetActive(false);
            isPaly = false;
        });
        
        FirebaseManager.Instance.LogEvent_AdLocation(MatchManager.Instance.m_StageIndex, "RewardBox_Validate");
    }

    public void OnRewardBoxClick()
    {
        Popup_Manager.INSTANCE.popup_index = 42;
        TwoButtonPopup.TitleIndex = 288;
        TwoButtonPopup.NoticeIndex = 289;
        TwoButtonPopup.Count = m_CurrentReward.reward;
        TwoButtonPopup.ImageName = m_CurrentReward.kind;
        TwoButtonPopup.OKButtonAction = ShowAd;
        TwoButtonPopup.CloseButtonAction = Close;
        Popup_Manager.INSTANCE.Popup_Push();

        AdManager.AdsRewardResult = RewardResult;

        Shine.Stop();
        m_RewardBoxObj.SetActive(false);
        m_Time = 0;
    }

    public void ShowAd()
    {
        AdManager.Instance.Ad_Show("RewardBox");
        isPaly = false;
    }

    public void Close()
    {
        isPaly = false;

        Shine.Stop();
        m_RewardBoxObj.SetActive(false);
        m_Time = 0;
    }

    public void ResetTime()
    {
        if (m_Sequence != null)
        {
            m_Sequence.Kill();
        }
        
        m_Time = 0;
        isPaly = false;
        m_RewardBoxObj.SetActive(false);
    }

    public void RewardResult()
    {
        switch (m_CurrentReward.type)
        {
            case 0://골드
                {
                    Popup_Manager.INSTANCE.popup_index = 41;
                    OneButtonPopup.TitleIndex = 18;
                    OneButtonPopup.NoticeIndex = -1;
                    OneButtonPopup.Count = m_CurrentReward.reward;
                    OneButtonPopup.ImageName = m_CurrentReward.kind;
                    Popup_Manager.INSTANCE.Popup_Push();

                    GoldSetting(m_CurrentReward.reward);

                    GameManager.Instance.m_adsRewardchk = true;
                }
                break;
            case 1://아이템
                {
                    GameManager.Instance.m_adsRewardchk = true;
                    string strTmp = System.Text.RegularExpressions.Regex.Replace(m_CurrentReward.kind, @"\D", "");


                    int itemindex = int.Parse(strTmp) - 1;
                    GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                    ChshItemEffects[itemindex].SetActive(true);
                    SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                    UI_ThreeMatch.Instance.CashItemUpdate(itemindex);

                    Invoke("CashItemEffectdisable", 2f);
                }

                break;
            case 2:
                MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.None, m_CurrentReward.reward);
                break;
        }
    }

    private void CashItemEffectdisable()
    {
        for (int i = 0; ChshItemEffects.Length > i; i++)
        {
            ChshItemEffects[i].SetActive(false);
        }
    }

    public void GoldSetting(int rewardmoney)
    {
        GameManager.Instance.m_GameData.MyGold += rewardmoney;

        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);
    }
}
