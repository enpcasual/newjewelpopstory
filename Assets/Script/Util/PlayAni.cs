﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAni : MonoBehaviour {

    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    private bool WinPlay = true;

	// Use this for initialization
	void OnEnable () {
        animator.SetBool("Win", WinPlay);
    }
}
