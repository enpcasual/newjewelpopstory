﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonFx.Json;

//using UnityEngine.SocialPlatforms.GameCenter;

//GameCenter + iCloud
public class GCIC_Manager : MonoBehaviour
{

    public static GCIC_Manager Instance;

    void Awake()
    {
        Instance = this;
    }

    public bool isLogin()
    {
        return Social.localUser.authenticated;
    }

    public void LoginGC()
    {
        // 로그인이 안되어 있으면
        if (!Social.localUser.authenticated)
            Social.localUser.Authenticate(LoginCallBackGPGS);
    }

    public void LoginCallBackGPGS(bool result)
    {
        if (result == true || Social.localUser.authenticated) //성공했는데 false를 준다..
        {
            Debug.Log("로그인!");

            GameManager.Instance.m_GameData.LoginChk = true;

            SecurityPlayerPrefs.SetInt("Login", 1);

            if (GameManager.Instance.m_Achivementloginchk)
            {
                ShowAchievementsUI();
            }
        }
        else {

            GameManager.Instance.m_GameData.LoginChk = false;
            Debug.Log("로그인 실패!!!!");
        }
    }

    public void LogoutGPGS()
    {

    }

    //업적
    public void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }

    //업적 업로드
    public void ReportProgress(string _id, float _value = 100f)
    {
        Social.ReportProgress(_id, _value, (bool success) =>
        {
            if (success)
                Debug.Log("AchieveProgress Success");
            else
                Debug.Log("AchieveProgress Fail");
        });
    }

    //리더보드
    public void ShowLeaderboardUI()
    {
        Social.ShowLeaderboardUI();
    }

    //리더보드 업로드
    public void ReportScore(string _id, long _value)
    {
        Social.ReportScore(_value, _id, (bool success) =>
        {
            if (success)
                Debug.Log("ReportScore Success : " + _id);
            else
                Debug.Log("ReportScore Fail : " + _id);
        });
    }


    #region iCloud

    public enum SaveGameType
    {
        None = 0,
        Save,
        Load,
    }

    SaveGameType m_SaveGameChk = SaveGameType.None;

    public void OpenSavedGame(SaveGameType type, LoadingType loading = LoadingType.Circle)
    {
        StartCoroutine(OpenSavedGame_iOS(type, loading));
    }

    IEnumerator OpenSavedGame_iOS(SaveGameType type, LoadingType loading = LoadingType.Circle)
    {

        //팝업이 내려가는 애니메이션 때문에 시간이 걸린다.
        //이 시간을 기다리지 않으면 iOS에서는 한프레임에 모든걸 처리하기때문에
        //세이브,로딩이 끝났을때 다른팝업이 뜨지 않는다.
        Loading.Instance.SetShow(true, LoadingType.Icon);
        yield return new WaitForSeconds(0.4f);

#if UNITY_IOS
        //iOS는 콜백함수 없다. 여기서 다 처리 한다.
        //1.Native_Brige를 통해 uuid를 iCloud에 세이브, 로드
        //2.NetWorkManager를 통해 Daerisoft서버 데이타 세이브, 로드
        if (type == SaveGameType.Save)
		{
			GameManager.Instance.m_GameData.LoadData_PlayerPrefs(true);
			string data = JsonWriter.Serialize(GameManager.Instance.m_GameData);
			string[] datas = data.Split(',');
			Native_Brige.SetCloudData("Length", datas.Length.ToString());
			for(int i =0; i < datas.Length; i++) // datas.Length = 33
            {
                bool save_ok = Native_Brige.SetCloudData(i.ToString(), datas[i]);
                Debug.Log("Now Saving " + i + "/" + data.Length + ", " + save_ok);
                if (!save_ok){ 
					if(Popup_Manager.INSTANCE.popup_index != 2 && Popup_Manager.INSTANCE.popup_index != 35 && Popup_Manager.INSTANCE.popup_index != 7)
					{
						//오류메시지 출력
						Loading.Instance.SetShow(false); //Loading_Save

						Popup_Manager.INSTANCE.popup_index = 33;
						Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(193) + "2001");
						Popup_Manager.INSTANCE.Popup_Push();
					}
                    else
                    {
                        Debug.Log("PopupConfiguration is Incorrect");
                        if(Loading.Instance.m_isShow)
                            Loading.Instance.SetShow(false); //Loading_Save
                    }
				}
				else 
				{
					if(Popup_Manager.INSTANCE.popup_index != 2 && Popup_Manager.INSTANCE.popup_index != 35 && Popup_Manager.INSTANCE.popup_index != 7)
					{
						Loading.Instance.SetShow(false); //Loading_Save

						Popup_Manager.INSTANCE.popup_index = 33;
						Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(192));
						Popup_Manager.INSTANCE.Popup_Push();
                    }
                    else
                    {
                        Debug.Log("PopupConfiguration is Incorrect");
                        if (Loading.Instance.m_isShow)
                            Loading.Instance.SetShow(false); //Loading_Save
                    }
                }
			}
            //string uuid = Native_Brige.GetUUID();
            //bool uuid_ok = Native_Brige.SetCloudData(uuid);
            //if (uuid_ok)
            //{
                //***************************
                //데이타는 다에리서버에 저장
                //****************************
            //    GameManager.Instance.m_GameData.LoadData_PlayerPrefs(true);
            //    string data = JsonWriter.Serialize(GameManager.Instance.m_GameData);

            //    NetWorkManager.instance.Req_userDataBackup(data);
            //}
			//else
        }
        else
		{
			string data = Native_Brige.GetCloudData("0");
		/*
            string uuid = Native_Brige.GetCloudData();
            if (uuid.Equals("None") == false)
            {
                //***************************
                //클라우드에 있는 키를 가지고
                //다에리서버에서 데이타 로드
                //****************************
                NetWorkManager.instance.Req_userDataRestore(uuid);
            }
		else*/	if(data.Equals("None"))
          	{
                //오류메시지 출력
                Loading.Instance.SetShow(false); //Loading_Load

                Popup_Manager.INSTANCE.popup_index = 33;
                Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(196) + "2001");
				Popup_Manager.INSTANCE.Popup_Push();
		}
			else
			{
				Loading.Instance.SetShow(false); //Loading_Load

				Popup_Manager.INSTANCE.popup_index = 33;
				Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(195));
				Popup_Manager.INSTANCE.Popup_Push();
				
				int Length = int.Parse(Native_Brige.GetCloudData("Length"));
				for(int i = 1; i < Length; i++)
				{
					string LoadData = Native_Brige.GetCloudData(i.ToString());
					data = string.Format("{0},{1}",data,LoadData);
				}

				GameManager.Instance.m_GameData = JsonReader.Deserialize<GameData>(data);
				GameManager.Instance.m_GameData.SaveData_PlayerPrefs(); //GameData입장에서는 PlayerPrefs에 저장.   

				//*****************
				//월드맵, UI, 차고
				//*****************
				if(GameManager.Instance.m_GameData.AdsFreeChk)
					AdMob_Manager.Instance.AdMob_Banner_Destory();
				StageItemManager.Instance.Init();
				UI_WorldMap.Instance.MyDataSetting();
				CarPortManager.Instance.MyFoodTruckSetting();
			}

		}
#endif
    }

    #endregion
}
