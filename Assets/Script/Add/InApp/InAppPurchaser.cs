﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


public enum ProductKind
{
    gold_1 = 1,
    gold_2 = 2,
    gold_3 = 3,
    gold_4 = 4,
    gold_5 = 5,
    gold_6 = 6,
    gold_21 = 21,
    free_ads = 100,

    package_1 = 201,
    package_2 = 202,
    package_3 = 203,
    package_4 = 204,
}



//IStoreListener에서 Purchaser 클래스를 파생하면 Unity Purchasing에서 메시지를받을 수 있습니다.
public class InAppPurchaser : MonoBehaviour, IStoreListener
{
    public static InAppPurchaser Instance;

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

#if UNITY_ANDROID

    // 구입할 수있는 모든 제품의 제품 식별자: 
    public const string kProductIDConsumable_1 = "coin_3000"; //0.99
    public const string kProductIDConsumable_2 = "coin_15000"; //3.99
    public const string kProductIDConsumable_3 = "coin_42000"; //9.99
    public const string kProductIDConsumable_4 = "coin_90000"; //19.99
    public const string kProductIDConsumable_5 = "coin_240000"; //49.99
    public const string kProductIDConsumable_6 = "coin_600000"; //99.99
    //public const string kProductIDConsumable_11 = "foodcrush_gold_11"; //0.99
    public const string kProductIDConsumable_21 = "supersale"; //4.99
    public const string kProductIDConsumable_free = "ads_free_30000";

    public const string kProductIDConsumable_p1 = "package_2200"; // 
    public const string kProductIDConsumable_p2 = "package2_11000"; //
    public const string kProductIDConsumable_p3 = "package3_33000"; //
    public const string kProductIDConsumable_p4 = "package4_110000"; //
    //public static string kProductIDSubscription = "subscription";

#elif UNITY_IOS
    // 구입할 수있는 모든 제품의 제품 식별자: 
    public const string kProductIDConsumable_1 = "gold_2000_pop"; //0.99
    public const string kProductIDConsumable_2 = "gold_10000_pop"; //3.99
    public const string kProductIDConsumable_3 = "gold_28000_pop"; //9.99
    public const string kProductIDConsumable_4 = "gold_60000_pop"; //19.99
    public const string kProductIDConsumable_5 = "gold_160000_pop"; //49.99
    public const string kProductIDConsumable_6 = "gold_400000_pop"; //99.99
    //public const string kProductIDConsumable_11 = "foodcrush_gold_11"; //0.99
    public const string kProductIDConsumable_21 = "SuperSale_pop"; //4.99
    public const string kProductIDConsumable_free = "Removeads_2200_pop";

    public const string kProductIDConsumable_p1 = "package_2200_pop"; // 
    public const string kProductIDConsumable_p2 = "package2_11000_pop"; //
    public const string kProductIDConsumable_p3 = "package3_33000_POP"; //
    public const string kProductIDConsumable_p4 = "package4_110000_POP"; //
    //public static string kProductIDSubscription = "subscription";

#endif
    //Apple App App Store 제품 별 Subscription 제품 식별자.
    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

    //Google Play 스토어 전용 제품 식별자 가입 제품입니다.
    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    public ProductKind GetProductKind(string productid)
    {
        ProductKind pk = ProductKind.gold_1;
        switch (productid)
        {
            case kProductIDConsumable_1:
                pk = ProductKind.gold_1;
                break;
            case kProductIDConsumable_2:
                pk = ProductKind.gold_2;
                break;
            case kProductIDConsumable_3:
                pk = ProductKind.gold_3;
                break;
            case kProductIDConsumable_4:
                pk = ProductKind.gold_4;
                break;
            case kProductIDConsumable_5:
                pk = ProductKind.gold_5;
                break;
            case kProductIDConsumable_6:
                pk = ProductKind.gold_6;
                break;
            //case kProductIDConsumable_11:
            //    pk = ProductKind.gold_11;
            //    break;
            case kProductIDConsumable_21:
                pk = ProductKind.gold_21;
                break;
            case kProductIDConsumable_free:
                pk = ProductKind.free_ads;
                break;
            case kProductIDConsumable_p1:
                pk = ProductKind.package_1;
                break;
            case kProductIDConsumable_p2:
                pk = ProductKind.package_2;
                break;
            case kProductIDConsumable_p3:
                pk = ProductKind.package_3;
                break;
            case kProductIDConsumable_p4:
                pk = ProductKind.package_4;
                break;
            default:
                break;
        }
        return pk;
    }

    public string GetProductId(ProductKind kind)
    {
        string id = "";
        switch (kind)
        {
            case ProductKind.gold_1:
                id = kProductIDConsumable_1;
                break;
            case ProductKind.gold_2:
                id = kProductIDConsumable_2;
                break;
            case ProductKind.gold_3:
                id = kProductIDConsumable_3;
                break;
            case ProductKind.gold_4:
                id = kProductIDConsumable_4;
                break;
            case ProductKind.gold_5:
                id = kProductIDConsumable_5;
                break;
            case ProductKind.gold_6:
                id = kProductIDConsumable_6;
                break;
            //case ProductKind.gold_11:
            //    id = kProductIDConsumable_11;
            //    break;
            case ProductKind.gold_21:
                id = kProductIDConsumable_21;
                break;
            case ProductKind.free_ads:
                id = kProductIDConsumable_free;
                break;
            case ProductKind.package_1:
                id = kProductIDConsumable_p1;
                break;
            case ProductKind.package_2:
                id = kProductIDConsumable_p2;
                break;
            case ProductKind.package_3:
                id = kProductIDConsumable_p3;
                break;
            case ProductKind.package_4:
                id = kProductIDConsumable_p4;
                break;
            default:
                break;
        }
        return id;
    }

    void Awake()
    {
        Instance = this;
    }

    IEnumerator Start()
    {
        yield return null;
        if (m_StoreController == null)
        {
            InitializePurchasing();
            RestorePurchases();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        builder.AddProduct(kProductIDConsumable_1, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable_2, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable_3, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable_4, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable_5, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable_6, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable_11, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable_21, ProductType.Consumable);
#if UNITY_ANDROID
        builder.AddProduct(kProductIDConsumable_free, ProductType.Consumable);
#elif UNITY_IOS
		builder.AddProduct(kProductIDConsumable_free, ProductType.NonConsumable);
#endif

        //builder.AddProduct(kProductIDConsumable_p1, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable_p2, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable_p3, ProductType.Consumable);
        //builder.AddProduct(kProductIDConsumable_p4, ProductType.Consumable);
        // Continue adding the non-consumable product.
        //builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);

        /*
        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
        // if the Product ID was configured differently between Apple and Google stores. Also note that
        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
        // must only be referenced here. 
        builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
                { kProductNameAppleSubscription, AppleAppStore.Name },
                { kProductNameGooglePlaySubscription, GooglePlay.Name },
            });

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        */

        UnityPurchasing.Initialize(this, builder);
    }

    //  
    // --- IStoreListener
    //

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public string LocalePrice(ProductKind kind)
    {
        if (IsInitialized())
        {
            string product_id = GetProductId(kind);
            Product product = m_StoreController.products.WithID(product_id);
            return product.metadata.localizedPriceString;
        }

        return "null";         
    }


    public void BuyProduct(ProductKind kind)
    {
        BuyProductID(GetProductId(kind));
    }

/*
    public void BuyConsumable()
    {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDConsumable);
    }

    public void BuyNonConsumable()
    {
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDNonConsumable);
    }

    public void BuySubscription()
    {
        // Buy the subscription product using its the general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        // Notice how we use the general product identifier in spite of this ID being mapped to
        // custom store-specific identifiers above.
        BuyProductID(kProductIDSubscription);
    }
*/

    bool isProcessing = false;
    void BuyProductID(string productId)
    {
        if (isProcessing)
            return;

        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);
            isProcessing = true;

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
#if UNITY_ANDROID
                m_StoreController.InitiatePurchase(product);
#elif UNITY_IOS
				m_StoreController.InitiatePurchase(product);
#endif
            }
            // Otherwise ...
            else
            {
                isProcessing=false;
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            isProcessing = false;
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    //IOS에서만 지원하는 기능
    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public string prodect_id_ios;
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        isProcessing = false;
        //****************** 구매 성공 ******************//
        //Debug.Log("영수증: " + args.purchasedProduct.receipt);

        BuyOK(args.purchasedProduct.definition.id);

        //**********************
        //IGWorks 추적코드
        //**********************
        //메타데이타에 있는 Local를 사용하지 않고 일단 강제 입력한다.
        int price = 0;
        ProductKind kind = GetProductKind(args.purchasedProduct.definition.id);
        switch (kind)
        {
            case ProductKind.gold_1:
                price = 1100;
                break;
            case ProductKind.gold_2:
                price = 4400;
                break;
            case ProductKind.gold_3:
                price = 11000;
                break;
            case ProductKind.gold_4:
                price = 22000;
                break;
            case ProductKind.gold_5:
                price = 55000;
                break;
            case ProductKind.gold_6:
                price = 110000;
                break;
            case ProductKind.gold_21:
                price = 5500;
                break;
            case ProductKind.free_ads:
                price = 2200;
                break;
            case ProductKind.package_1:
                price = 2200;
                break;
            case ProductKind.package_2:
                price = 11000;
                break;
            case ProductKind.package_3:
                price = 33000;
                break;
            case ProductKind.package_4:
                price = 110000;
                break;
            default:
                break;
        }

        SingularSDK.Revenue("KRW", price, args.purchasedProduct.definition.id, args.purchasedProduct.transactionID, "myProductCategory", 1, price);
        //IGAWorks_Manager.instance.IGAWorks_Adbrix_Purchase(args.purchasedProduct.transactionID, args.purchasedProduct.definition.id, kind.ToString(), price);

        //Debug.Log("PGM 1 : " + args.purchasedProduct.definition.id);
        //Debug.Log("PGM 2 : " + price.ToString());
        //Debug.Log("PGM 4 : " + args.purchasedProduct.transactionID);
        //Debug.Log("PGM 5 : " + args.purchasedProduct.receipt);
        /*
   // A consumable product has been purchased by this user.
   if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
   {
       Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
   }
   // Or ... a non-consumable product has been purchased by this user.
   else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
   {
       Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
       // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
   }

   // Or ... a subscription product has been purchased by this user.
   else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
   {
       Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
       // TODO: The subscription item has been successfully purchased, grant this to the player.
   }
   // Or ... an unknown product has been purchased by this user. Fill in additional products here....
   else
   {
       Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
   }
   */
        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }


    public void BuyOK(string itemid)
    {
        int itemindex = 0;

        switch (itemid)
        {
            case kProductIDConsumable_1:
                itemindex = 1; //TableDataManager.Instance.m_ShopList[0].gold;
                break;
            case kProductIDConsumable_2:
                itemindex = 2;
                break;
            case kProductIDConsumable_3:
                itemindex = 3;
                break;
            case kProductIDConsumable_4:
                itemindex = 4;
                break;
            case kProductIDConsumable_5:
                itemindex = 5;
                break;
            case kProductIDConsumable_6:
                itemindex = 6;
                break;
            //case kProductIDConsumable_11:
            //    itemindex = 6;
            //    WorldMapUI.instance.GoldSetting(TableDataManager.Instance.m_buyprice[10].buyprice);
            //    break;
            case kProductIDConsumable_21:
                itemindex = 7;
                Popup_Manager.INSTANCE.popup_list[20].GetComponent<FoodTruckSalePop>().SaleResult();
                break;
#if UNITY_ANDROID
            case kProductIDConsumable_free:
#elif UNITY_IOS
            case kProductIDNonConsumable_free_iOS:
#endif
                itemindex = 8;

                GameManager.Instance.m_GameData.AdsFreeChk = true;
                SecurityPlayerPrefs.SetInt("addfreechk", 1);
                AdMob_Manager.Instance.AdMob_Banner_Destory();
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().RestoreSetting();

                if (Popup_Manager.INSTANCE.popup_list[7].gameObject.activeSelf)
                {
                    Popup_Manager.INSTANCE.Popup_Pop();
                }


                break;
            case kProductIDConsumable_p1:
                Popup_Manager.INSTANCE.popup_list[35].GetComponent<SpecialItemPop>().BuyResult(0);
                break;
            case kProductIDConsumable_p2:
                Popup_Manager.INSTANCE.popup_list[35].GetComponent<SpecialItemPop>().BuyResult(1);
                break;
            case kProductIDConsumable_p3:
                Popup_Manager.INSTANCE.popup_list[35].GetComponent<SpecialItemPop>().BuyResult(2);
                break;
            case kProductIDConsumable_p4:
                Popup_Manager.INSTANCE.popup_list[35].GetComponent<SpecialItemPop>().BuyResult(3);
                break;
        }


        if (Popup_Manager.INSTANCE.shoppopchk && itemindex <= 6)
        {
            if (TimeManager.Instance.supersaletimechk)
            {
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().GoldSetting(TableDataManager.Instance.m_ShopList[itemindex].salegold);
            }
            else
            {
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().GoldSetting(TableDataManager.Instance.m_ShopList[itemindex].gold);
            }

            
        }

#if UNITY_ANDROID
        	GPGS_Manager.Instance.OpenSavedGame("rf_sd", GPGS_Manager.SaveGameType.Save, false, LoadingType.Icon);
#elif UNITY_IOS
            if(itemindex != 8) //복원일때 저장을 해버리면 기존데이타를 원치 않게 날리는 문제가 발생할수 있을꺼 같다. 애드프리구매후에는 저장안함.
			    GCIC_Manager.Instance.OpenSavedGame (GCIC_Manager.SaveGameType.Save, LoadingType.Icon);
#endif
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        isProcessing = false;
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

        Loading.Instance.SetShow(false); //Loading_Purchase
    }
}