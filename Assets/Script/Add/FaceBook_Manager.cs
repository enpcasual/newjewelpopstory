﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;

public class FaceBook_Manager : MonoBehaviour
{
    static public FaceBook_Manager Instance;
    // Awake function from Unity's MonoBehavior
    public static bool isLoggedIn
    {
        get
        {
            return FB.IsInitialized && FB.IsLoggedIn;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    IEnumerator Start()
    {
        yield return null;

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();


            //딥링크
            FB.Mobile.FetchDeferredAppLinkData(DeepLinkCallback);
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }


    //로그인

    public void FBLogin(Action<bool, string> onComplete)
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, (result) => 
        {
            AuthCallback(result);
            bool isSuccess = string.IsNullOrEmpty(result.Error) && !result.Cancelled;

            if (onComplete != null)
                onComplete(isSuccess, result.Error);
        });
    }

    public void FBLogin()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
        else {
            Debug.Log("User cancelled login");
        }
    }

    public void FBLogout()
    {
        if (isLoggedIn)
        {
            FB.LogOut();
        }
    }

    //공유하기
    public void ShareLink()
    {
        //Application.OpenURL("http://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/permalink.php?story_fbid=336074726825143&id=100012678547133");

        //FB.ShareLink(new Uri("https://business.facebook.com/New-Sweet-Candy-Pop-Puzzle-World-1048076795382195/?business_id=1602412539902384"),
        FB.ShareLink(new Uri("https://www.facebook.com/New-Jewel-Pop-Story-Puzzle-World-2386715501541529/"),
        "New Jewel Pop Story",
            "Puzzle World",
            callback: ShareCallback);
    }

    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
        }
        else {
            // Share succeeded without postID
            Debug.Log("ShareLink success!");
            //어떠한 경우든 로그인을 안하면 에러 호출
            //페이스북 앱이 깔린경우. 게시와 상관없이 성공 호출
            //페이스북 앱이 안깔린경우. 게시를 눌렀을때만 성공 호출

            //ShopPop이 활성화 되어 있지 않는 문제?????
            //Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().GoldSetting(1000);

            GameManager.Instance.m_GameData.MyGold += 1000;
            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);
            WorldMapUI.instance.MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
        }
    }


    //딥링크
    void DeepLinkCallback(IAppLinkResult result)
    {
        if (!String.IsNullOrEmpty(result.Url))
        {
            Debug.Log(result.Url);
        }
    }
}
