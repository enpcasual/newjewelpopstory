﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAds_Manager : AdBase
{

    static public UnityAds_Manager Instance = null;
#if UNITY_ANDROID
    string App_Id = "3249999";
#elif UNITY_IOS
	string App_Id = "3249998";  
#endif
    bool TestMode = false;

    public static string SkipAd_ID = "video";
    public static string RewordAd_Id = "rewardedVideo";
    

    void Awake()
    {
        Instance = this;
    }

    public override void Init()
    {
        Advertisement.Initialize(App_Id, TestMode);
    }

    public override bool IsAdReady
    {
        get
        {
            return Advertisement.IsReady(RewordAd_Id);
        }
    }

    //5초후 스킵 가능한 광고
    public static void Skip_Ad_Show()
    {
        GameManager.Instance.freemovechk = true;
        ShowAdPlacement(SkipAd_ID);
    }
    public override void Ad_Show(string log)
    {
        base.Ad_Show(log);
        NotSkip_Ad_Show(log);
    }

    //스킵 불가능한 광고
    public static void NotSkip_Ad_Show(string rewardADLocation = null)
    {
        Debug.Log("Unity Ads 오리진");
        GameManager.Instance.freemovechk = true;
        RewardADLocation = rewardADLocation;
        ShowAdPlacement(RewordAd_Id);
        
    }

    public static void ShowAdPlacement(string integratonid)
    {
        if (Advertisement.IsReady(integratonid))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show(integratonid, options);
        }
    }

    private static void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("Video completed. Offer a reward to the player.");
                Debug.Log("UnityAds 보상");

                if (string.IsNullOrEmpty(RewardADLocation) == false)
                {
                    SendRewardADAnalyticsInfo();
                }

                AdManager.GameReward();
                break;
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped.");

                AdManager.AdsRewardResult = null;
                break;
            case ShowResult.Failed:
                Debug.LogError("Video failed to show.");

                AdManager.AdsRewardResult = null;
                break;
        }
        
        RewardADLocation = string.Empty;

        AdManager.Instance.IsAdPlaying = false;
    }

    //bjs
    public bool IsUnityAdsReady()
    {
        return Advertisement.IsReady(RewordAd_Id);
    }
    
}
