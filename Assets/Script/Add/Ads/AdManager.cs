﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdManager : MonoBehaviour
{
    public static AdManager Instance;
    public static System.Action AdsRewardResult = null;
    public List<AdBase> AdManagers = new List<AdBase>();

    public bool IsAdPlaying;

    public bool IsAdReady
    {
        get
        {
            for(int i = 0; i < AdManagers.Count; i++)
            {
                if (AdManagers[i].IsAdReady)
                    return AdManagers[i].IsAdReady;
            }

            return false;
        }
    }

    private void Awake()
    {
        Instance = this;
        
        for (int i = 0; i < AdManagers.Count; i++)
        {
            AdManagers[i].Init();
        }
    }

    int ShowIndex = 0;
    public bool Ad_Show(string log)
    {

        Debug.Log("IsAdPlaying : " + IsAdPlaying);
#if !UNITY_EDITOR
        if(IsAdPlaying)
            return false;
#endif

        if (AdManagers[ShowIndex].IsAdReady)
        {
            //원본 버전. 1,2,1,2,1,2,..
            //AdManagers[ShowIndex++].Ad_Show(log);
            //ShowIndex = ShowIndex % AdManagers.Count;

            //수정 버전. 1,1,1,2,2,2,...
            AdManagers[ShowIndex].Ad_Show(log);

            return true;
        }
        else
        {
            //원본 버전. 1,2,1,2,1,2,..
            //ShowIndex++;
            //ShowIndex = ShowIndex % AdManagers.Count;
            
            //수정 버전. 1,1,1,2,2,2,...
            /*Empty*/
            
            for (int i = 0; i < AdManagers.Count; i++)
            {
                if (AdManagers[i].IsAdReady)
                {
                    AdManagers[i].Ad_Show(log);
                    return true;
                }
            }
        }

        return false;
    }

    public static void GameReward()
    {
        //첫번째 이어하기 보상
        if (GameManager.Instance.gamecontinuefree)
        {

            Debug.Log("gamecontinuefree Video Reward::::::::::::::::::::::::::::::::::::");
            Popup_Manager.INSTANCE.Popup_Pop();
            MissionManager.Instance.ContinueBonus(Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().continueType);

        }
        else
        {

            GameManager.Instance.m_adsRewardchk = false;

            if (Popup_Manager.INSTANCE.popup_list[2].gameObject.activeSelf)
            {
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().VideoRewardResultSetting();
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().UnityAdsRewardResult();
            }
            else
            {
                if (AdsRewardResult != null)
                {
                    AdsRewardResult();
                    AdsRewardResult = null;
                }
                else
                {
                    WorldMapUI.instance.VideoRewardResultSetting();
                    WorldMapUI.instance.UnityAdsRewardResult();
                }
            }
        }

        GameManager.Instance.gamecontinuefree = false;
        //PopupManager.Instance.pop_AD.GiveRewardAD();
    }
}
