﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdBase : MonoBehaviour {
    protected static string RewardADLocation = string.Empty;
    private bool m_IsAdReady = false;
    public virtual bool IsAdReady
    {
        get { return m_IsAdReady; }
        set { m_IsAdReady = value; }
    }
    
    public virtual void Init() { }

    public virtual void Ad_Show(string log)
    {
        Debug.Log("[ADBASE] Ad_Show " + this.GetType().ToString() + ", In " + Time.time);
        AdManager.Instance.IsAdPlaying = true;
        RewardADLocation = log;
    }

    protected static void SendRewardADAnalyticsInfo()
    {
        int stage = 0;
        if (MatchManager.Instance && GameManager.Instance.GetGameState() == GAMESTATE.ThreeMatch)
        {
            stage = MatchManager.Instance.m_StageIndex;
        }

        SingularSDK.Event("Ad_Reward", new string[] {"type", RewardADLocation} );
        FirebaseManager.Instance.LogEvent_AdLocation(stage, RewardADLocation);
        RewardADLocation = string.Empty;
    }
}
