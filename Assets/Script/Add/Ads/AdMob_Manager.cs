﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdMob_Manager : AdBase {

    public static AdMob_Manager Instance;
    public float AdMob_Height;

#if UNITY_ANDROID
    const string App_ID = "ca-app-pub-8607653426423416~9434005926";     
    const string Banner_ID = "ca-app-pub-8607653426423416/9649628349";  
    const string Medium_ID = "ca-app-pub-8607653426423416/5574651969";  
    const string Full_ID = "ca-app-pub-8607653426423416/8289333263";    
    const string Video_ID = "ca-app-pub-8607653426423416/3686855222";   
    const string Native_ID = "ca-app-pub-8607653426423416/1756745742";
#elif UNITY_IOS
    const string App_ID = "ca-app-pub-8607653426423416~9505763278";
	const string Banner_ID = "ca-app-pub-8607653426423416/2940354925";  
	const string Medium_ID = "ca-app-pub-8607653426423416/5374946570";  
	const string Full_ID = "ca-app-pub-8607653426423416/5183374889";    
    const string Video_ID = "ca-app-pub-8607653426423416/8696272157";   
    const string Native_ID = "ca-app-pub-8607653426423416/7043251464";


#endif

    BannerView adBannerView = null; // 배너 출력
    BannerView adMediumView = null; // 중간 광고
    InterstitialAd interstitial = null; // 전면광고 


    AdRequest adRequest1;
    AdRequest adRequest2;

    //*******************
    //동영상 광고
    //*******************
    RewardBasedVideoAd rewardBasedVideo = null;
    AdRequest adRequest_V;

    public UnifiedNativeAd nativeAd;

    void Awake()
    {
        Instance = this;
        MobileAds.Initialize(App_ID);
        MobileAds.SetApplicationMuted(true);
    }
    
    public void Init_Banner()
    {
#region 배너광고 높이 구하기
        //배너타입이 SmartBanner인 경우 화면넓이에 따라 광고 높이가 다르다.
        //https://firebase.google.com/docs/admob/android/banner?hl=ko

        // - dpi(Dots Per Inch)
        //1인치(2.54 센티미터)에 들어 있는 픽셀의 수.안드로이드에서는 160을 기본으로 한다.

        // - dp 또는 dip(Density - independent Pixels)
        //화면의 크기를 기준으로 표시. 어떤 화면의 크기에서도 동일한 크기를 표시 하게 된다.
        //스크린의 픽셀과  dp가 160인 경우에는 1dp는 1pixel과 같다. 그러나 dpi가 240인 경우에는 1dp는 1.5pixel이 된다.

        //계산 공식은 아래와 같다
        //dp = pixel / (density / 160)

        //4.3버전할때는 분명 GameView의 실제 Pixel 높이를 가져왔기 때문에 dpi와 계산에 문제가 없었다.
        //5.5버전에서는 Screen.height는 설정한 해상도의 높이를 가져온다. 그런데 dpi는 모니터꺼 그대로 가져오면서 안맞게 된다...
        //Editor에서는 광고크기가 안맞는 문제가 생기게 된다.
        //그래서 일단 특정폰의 해상도와 광고 위치까지 테스트 할려면
        //GameView에서 해상도 세팅하고 dpi도 세팅해야 한다.
        float dpi = Screen.dpi; //노트5:560 갤럭시탭s2:320 아이패드에어:264
        float device_Heigh_dp = Screen.height / (dpi / 160);
        float baner_Heigh_dp = 0;
        //Debug.Log("Admob_Height : " + device_Heigh_dp);
        //dp단위로 비교.
        if (device_Heigh_dp <= 400)
        {
            baner_Heigh_dp = 32;
            AdMob_Height = baner_Heigh_dp * dpi / 160;
        }
        else if (device_Heigh_dp <= 720)
        {
            baner_Heigh_dp = 50;
            AdMob_Height = baner_Heigh_dp * dpi / 160;
        }
        else if(device_Heigh_dp <= 1200)  //ipad Air:1241
        {
            baner_Heigh_dp = 90;
            AdMob_Height = baner_Heigh_dp * dpi / 160;
        }else
        {
            //애드몹 문서상에는 720보다 크면 90으로 끝이다.
            //그런데 iad Air에서 실제 광고 높이가 좀더 크다.
            //이부분이 기기의 dpi 수치가 잘못되서 광고가 확대되었거나
            //아니면 더 큰 높이의 광고가 있다는건데. 일단은 높이를 더 주는걸로 해결한다.
            baner_Heigh_dp = 110;
            AdMob_Height = baner_Heigh_dp * dpi / 160;
        }

        //이건 픽셀단위로 바꿔서 비교하는거.. 똑같은거다.
        //if (Screen.height <= 400 * Mathf.RoundToInt(Screen.dpi / 160))
        //{
        //    AdMob_Height = 32 * Mathf.RoundToInt(Screen.dpi / 160);
        //}
        //else if (Screen.height <= 720 * Mathf.RoundToInt(Screen.dpi / 160))
        //{
        //    AdMob_Height = 50 * Mathf.RoundToInt(Screen.dpi / 160);
        //}
        //else {
        //    AdMob_Height = 90 * Mathf.RoundToInt(Screen.dpi / 160);
        //}

        //위에서 구한건 디바이스의 픽셀이므로 실제 게임의 픽셀(컨텐츠크기)로 바꿔야하낟.
        //NGUI에서 게임 사이즈의 높이가 1136이므로 디바이스의 높이와 비율로 곱한다.
        AdMob_Height = AdMob_Height * (1136f / Screen.height);

#endregion


        //adRequest1 = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice(Device_ID).Build();    //테스트
        adRequest1 = new AdRequest.Builder().Build();  //상용

        //adRequest2 = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice(Device_ID).Build();    //테스트
        adRequest2 = new AdRequest.Builder().Build();  //상용

        //Debug.Log("w" + Screen.width + "h" + Screen.height + "dpi" + Screen.dpi);
        //Debug.Log("BJS : " + SystemInfo.deviceModel);

        //**************
        //배너세팅
        //**************
        //노트4이고 배너높이가 50으로 계산됐을때(1080*1920에 dpi480)
        //string devicemodel = SystemInfo.deviceModel;
        //if ((devicemodel.Contains("SM-N910") || devicemodel.Contains("SM-N916")) && baner_Heigh_dp == 50)
        //    adBannerView = new BannerView(Banner_ID, AdSize.Banner, 20, 590);
        //else
        adBannerView = new BannerView(Banner_ID, AdSize.Banner, AdPosition.Top);
        adBannerView.LoadAd(adRequest1);
        adBannerView.Hide();

        //**************
        //중간배너 세팅
        //**************
        //if ((devicemodel.Contains("SM-N910") || devicemodel.Contains("SM-N916")) && baner_Heigh_dp == 50)
        //    adMediumView = new BannerView(Medium_ID, AdSize.MediumRectangle, 30, 390);
        //else
        adMediumView = new BannerView(Medium_ID, AdSize.MediumRectangle, AdPosition.Bottom);
        adMediumView.LoadAd(adRequest2);
        adMediumView.Hide();

        //**************
        //전면광고세팅
        //**************
        interstitial = new InterstitialAd(Full_ID);
        interstitial.OnAdClosed += OnAdClose;
        interstitial.OnAdFailedToLoad += OnAdFailedToLoad;
        interstitial.LoadAd(new AdRequest.Builder().Build());

        RequestNativeAd();
    }

    bool isBannerShow = false;

    public override bool IsAdReady
    {
        get
        {
            return rewardBasedVideo.IsLoaded();
        }
    }

    public void AdMob_Banner_Show()
    {
        //adfree일때 Init를 안타므로 null일 위험이 있음.
        if (isBannerShow || adBannerView == null)
            return;

        isBannerShow = true;
        adBannerView.Show();
    }

    public void AdMob_Banner_Hide()
    {
        //adfree일때 Init를 안타므로 null일 위험이 있음.
        if (!isBannerShow || adBannerView == null)
            return;

        isBannerShow = false;
        adBannerView.Hide();
    }

     public void AdMob_Banner_Destory()
    {
        if (adBannerView != null)
            adBannerView.Destroy();
    }


    public void AdMob_Medium_Show()
    {
        //adfree일때 Init를 안타므로 null일 위험이 있음.
        if (adMediumView == null)
            return;

        adMediumView.Show();
    }

    public void AdMob_Medium_Hide()
    {
        //adfree일때 Init를 안타므로 null일 위험이 있음.
        if (adMediumView == null)
            return;

        adMediumView.Hide();
    }

    public void AdMob_Medium_Destory()
    {
        if (adMediumView != null)
            adMediumView.Destroy();
    }

    public void AdMob_FullAd_Show()
    {
        //일본 유저가 게임 2~3판 하면 뻗는다고 함.
        //동영상을 봤는데 전면광고 나오는 타이밍에 뻗음.
        //그래서 일단 try catch문으로 감싸본다.
        try
        {
            //adfree일때 Init를 안타므로 null일 위험이 있음.
            if (interstitial == null)
                return;

            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
            else {
                Debug.Log("The interstitial wasn't loaded yet.");
            }
        }
        catch (Exception)
        {
            Debug.Log("전면광고에서 에러가 발생함.");
            throw;
        }     
    }

    //전면 광고를 닫을때 다음에 볼 새로운 광고를 미리 요청한다.
    public void OnAdClose(object sender, EventArgs args)
    {
        //Popup_Manager.INSTANCE.AddFreePopSetting();
        Popup_Manager.INSTANCE.add_displaychk = true;

        //**************
        //전면광고세팅
        //**************
        //iOS에서는 InterstitialAd를 재사용할수가 없다. 
        interstitial.Destroy();
		interstitial = new InterstitialAd(Full_ID);
		interstitial.OnAdClosed += OnAdClose;
        interstitial.OnAdFailedToLoad += OnAdFailedToLoad;
        interstitial.OnAdLoaded += Interstitial_OnAdLoaded;
        interstitial.LoadAd(new AdRequest.Builder().Build());
    }

    private void Interstitial_OnAdLoaded(object sender, EventArgs e)
    {
        Debug.Log("ADMob Manager.Interstitial Loaded + " + e);
    }

    public void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("실패한 이유 : " + args.Message);
    }

    //private void RequestNativeAd()
    //{
    //    AdLoader adLoader = new AdLoader.Builder("ca-app-pub-8607653426423416/1756745742")
    //        .ForUnifiedNativeAd()
    //        .Build();
    //}

    #region 동영상 광고 

    public override void Init()
    {
        //**************
        //동영상 광고 
        //**************
        //https://firebase.google.com/docs/admob/unity/rewarded-video
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        //adRequest_V = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice(Device_ID).Build();    //테스트
        adRequest_V = new AdRequest.Builder().Build();  //상용
        rewardBasedVideo.LoadAd(adRequest_V, Video_ID);

        // Reward based video instance is a singleton. Register handlers once to
        // avoid duplicate events.
        //if (!rewardBasedEventHandlersSet)
        {
            // Ad event fired when the rewarded video ad
            // has been received.
            rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            // has failed to load.
            //rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            // is opened.
            //rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
            // has started playing.
            //rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
            // has rewarded the user.
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            // is closed.
            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            // is leaving the application.
            //rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

            //rewardBasedEventHandlersSet = true;
            
            rewardBasedVideo.OnAdFailedToLoad += OnAdFailedToLoad2;
        }
    }

    public bool IsVideoAdLoaded()
    {
        return rewardBasedVideo.IsLoaded();
    }

    public override void Ad_Show(string log)
    {
        base.Ad_Show(log);
        AdMob_VideoAd_Show();
    }

    bool isSoundSetted = false;
    bool isBackgroundSoundOn = false;
    bool isEffectSoundOn = false;
    string BGMName = string.Empty;

    private void SaveSoundSettingAndMute()
    {
#if UNITY_IOS
        isSoundSetted = true;
        isBackgroundSoundOn = SoundManager.Instance.isBGM;
        isEffectSoundOn = SoundManager.Instance.isEffect;

        if (SoundManager.Instance.curBGM != null && SoundManager.Instance.curBGM.Clip != null)
            BGMName = SoundManager.Instance.curBGM.Clip.name;
        else
            BGMName = string.Empty;

        Debug.Log("BGM NAME : " + BGMName);

        SoundManager.Instance.isBGM = false;
        SoundManager.Instance.isEffect = false;

        SoundManager.Instance.StopBGM();
#endif
    }

    private void LoadSoundSettingAndPlay()
    {
#if UNITY_IOS
        if (!isSoundSetted)
            return;

        isSoundSetted = false;
        SoundManager.Instance.isBGM = isBackgroundSoundOn;
        SoundManager.Instance.isEffect = isEffectSoundOn;

        if (!string.IsNullOrEmpty(BGMName))
        {
            SoundManager.Instance.PlayBGM(BGMName);
        }
#endif
    }

    public void AdMob_VideoAd_Show()
    {
        Debug.Log(rewardBasedVideo.MediationAdapterClassName());
        

        if (rewardBasedVideo.IsLoaded())
        {
            GameManager.Instance.freemovechk = true;

            SaveSoundSettingAndMute();

            rewardBasedVideo.Show();

#if UNITY_EDITOR
            AdManager.Instance.IsAdPlaying = false;

            StartCoroutine(Co_Reward());
            LoadSoundSettingAndPlay();
#endif
        }
    }


    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        if (WorldMapUI.instance != null)
            WorldMapUI.instance.FreeCoinSetting();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        Debug.Log("VideoAd 닫기");
        AdManager.Instance.IsAdPlaying = false;

        rewardBasedVideo.LoadAd(adRequest_V, Video_ID);
        LoadSoundSettingAndPlay();
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        AdManager.Instance.IsAdPlaying = false;

        StartCoroutine(Co_Reward());
        LoadSoundSettingAndPlay();
        rewardBasedVideo.LoadAd(adRequest_V, Video_ID);
    }

    bool isPaused = false;
    void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;
    }

    IEnumerator Co_Reward()
    {
        while(isPaused)
        {
            yield return null;
        }

        Debug.Log("VideoAd 보상");
        // 광고 시청이 완료되었을 때 처리
        AdManager.GameReward();
        SendRewardADAnalyticsInfo();
    }

    public void OnAdFailedToLoad2(object sender, AdFailedToLoadEventArgs args)
    {
        AdManager.Instance.IsAdPlaying = false;
        AdManager.AdsRewardResult = null;

        LoadSoundSettingAndPlay();
        Debug.Log("동영상 로드 실패 : " + args.Message);
    }
    #endregion

    public void RequestNativeAd()
    {
        AdLoader adLoader = new AdLoader.Builder(Native_ID)
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded += this.HandleUnifiedNativeAdLoaded;
        adLoader.OnAdFailedToLoad += this.HandleNativeAdFailedToLoad;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("Native ad failed to load: " + args.Message);
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        MonoBehaviour.print("Unified native ad loaded.");
        this.nativeAd = args.nativeAd;
    }

}
