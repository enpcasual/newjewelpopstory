﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using JsonFx.Json;

//GPGS를 사용 하기 위한 using
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System.Text;


//매니져 클래스
public class GPGS_Manager : MonoBehaviour
{
	public static GPGS_Manager Instance;

	/// <summary> 현재 로그인 중인지 체크 </summary>
	public bool bLogin {
		get;
		set;
	}


    void Awake ()
	{
		Instance = this;
        //로그인 및 GPGS함수 접근보다 우선해야되기때문에 그냥 Awake에서 하자.
        InitializeGPGS ();
    }

	//*****************************
	//초기화, 로그인, 기본기능
	//*****************************
	/// <summary> GPGS를 초기화 합니다. </summary>
	public void InitializeGPGS ()
	{
#if UNITY_ANDROID
        bLogin = false;

        //****************************
        //특별한 기능을 사용할려면 활성
        //****************************
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames()  // enables saving game progress.                         
        //.WithInvitationDelegate(< callback method >) // registers a callback to handle game invitations received while the game is not running.
        //.WithMatchDelegate(< callback method >) // registers a callback for turn based match notifications received while the game is not running.                                                                  
        //.RequireGooglePlus() // require access to a player's Google+ social graph (usually not needed)
        .Build();
        PlayGamesPlatform.InitializeInstance(config);

        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = false;

        //PlayGamesPlatform.Instance.SetGravityForPopups(Gravity.TOP);
         /*
        //암시적으로 로그인을 해줌.
        PlayGamesPlatform.Instance.Authenticate((bool _success) =>
        {
            bLogin = _success;

            if (_success)
            {
                string username = GetNameGPGS();
                Debug.Log("username :::::::::::::::");// + username);
            }
            else
            {
                Debug.LogWarning("Authentication failed!");
            }
        });
         * */

        //Debug.Log("구글 플레이 게임 서비스 초기화!!");
#endif
    }

    public void TestResetupPopups()
    {
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.SetGravityForPopups(Gravity.TOP);
    }


	/// <summary> GPGS를 로그인 합니다. </summary>
	public void LoginGPGS ()
	{
		// 로그인이 안되어 있으면
		if (!Social.localUser.authenticated)
			Social.localUser.Authenticate (LoginCallBackGPGS);
	}

    /// <summary> GPGS Login Callback </summary>
    /// <param name="result"> 결과 </param>
    public void LoginCallBackGPGS(bool result, string message)
    {
        bLogin = result;

        if (result == true) {
			Debug.Log ("로그인!" + message);

			GameManager.Instance.m_GameData.LoginChk = true;

			SecurityPlayerPrefs.SetInt ("Login", 1);


			if (GameManager.Instance.m_Achivementloginchk) {
               
                
				Popup_Manager.INSTANCE.popup_list [10].GetComponent<Game_Center> ().PopUpSetting ();

				ShowAchievementsUI ();
			} else if (GameManager.Instance.m_LeadBoardloginchk) {
                

				Popup_Manager.INSTANCE.popup_list [10].GetComponent<Game_Center> ().PopUpSetting ();

				ShowLeaderboardUI ();
			}
          

			if (m_SaveGameChk != SaveGameType.None) {
				//************************************************************************************
				//제대로 할려면 filename, bLogin, LoadingType, loading 함수 인자들도 가져와야한다.
				//하지만 지금은 과금쪽에서의 세입는 여기를 안타기때문에 결국 세이브, 로드일때만 탄다.
				//그래서 이렇게 간단하게 호출한다.
				//*************************************************************************************
				OpenSavedGame ("rf_sd", m_SaveGameChk);
			}     
		} else {
			Debug.Log ("로그인 실패!!!!" + message);
		}

		//성공이든 실패든 초기화!!
		GameManager.Instance.m_Achivementloginchk = false;
		GameManager.Instance.m_LeadBoardloginchk = false;
		m_SaveGameChk = SaveGameType.None;

		/*
        if (GameManager.Instance.m_titleloginchk)
        {

            GameManager.Instance.m_titleloginchk = false;

            UI_Main.Instance.ProgressLoadingEnd();
        }
       */
	}

	/// <summary> GPGS를 로그아웃 합니다. </summary>
	public void LogoutGPGS ()
	{
#if UNITY_ANDROID
		// 로그인이 되어 있으면
		if (Social.localUser.authenticated) {
			((PlayGamesPlatform)Social.Active).SignOut ();
			Debug.Log ("로그 아웃!!!");
			bLogin = false;

            GameManager.Instance.m_GameData.LoginChk = false;

            SecurityPlayerPrefs.SetInt("Login", 0);
        }
#endif
	}

	#region GetInfo

	public string GetIDGPGS ()
	{
		if (Social.localUser.authenticated) {
			//이메일을 사용하면 문제가 있다..
			String email = Social.localUser.id;
			//string email = PlayGamesPlatform.Instance.GetUserEmail();
			//string email = ((PlayGamesLocalUser)(Social.localUser)).Email;
			return email;
		} else
			return null;
	}

	/// <summary> GPGS에서 자신의 프로필 이미지를 가져옵니다.</summary>
	/// <returns> Texture 2D 이미지 </returns>
	public Texture2D GetImageGPGS ()
	{
		if (Social.localUser.authenticated)
			return Social.localUser.image;
		else
			return null;
	}

	/// <summary> GPGS 에서 사용자 이름을 가져옵니다. </summary>
	/// <returns> 이름 </returns>
	public string GetNameGPGS ()
	{
		if (Social.localUser.authenticated)
			return Social.localUser.userName;
		else
			return null;
	}

    #endregion

    #region Achievement
    //업적
    public void ShowAchievementsUI ()
	{
#if UNITY_ANDROID
		PlayGamesPlatform.Instance.ShowAchievementsUI ();
#endif
	}

	//업적 점수 업로드
	public void ReportProgress (string _id, float _value = 100f)
	{
#if UNITY_ANDROID
#if !TEST_MODE
        PlayGamesPlatform.Instance.ReportProgress (_id, _value, (bool success) => {
			if (success)
				Debug.Log ("AchieveProgress Success");
			else
				Debug.Log ("AchieveProgress Fail");
		});
#endif
#endif
	}

	//업적 스텝 증가
	public void IncrementAchievement (string _id, int _step)
	{
#if UNITY_ANDROID
		PlayGamesPlatform.Instance.IncrementAchievement (_id, _step, (bool success) => {
			if (success)
				Debug.Log ("IncrementAchieve Success");
			else
				Debug.Log ("IncrementAchieve Fail");
		});
#endif
	}

	#endregion

	#region LeaderBoard
	//리더보드
	public void ShowLeaderboardUI ()
	{
#if UNITY_ANDROID
		PlayGamesPlatform.Instance.ShowLeaderboardUI ();
#endif
	}

	//리더보드 - 특정 리더보드 만 보여줌
	public void ShowLeaderBoardUI_One (string lb_id)
	{
#if UNITY_ANDROID
		PlayGamesPlatform.Instance.ShowLeaderboardUI (lb_id);
#endif
	}

	//리더보드 점수 업로드
	public void ReportScore (string _id, long _value)
	{
#if UNITY_ANDROID
#if !TEST_MODE
		PlayGamesPlatform.Instance.ReportScore (_value, _id, (bool success) => {
			if (success)
				Debug.Log ("ReportScore Success : " + _id);
			else
				Debug.Log ("ReportScore Fail : " + _id);
		});
#endif
#endif
	}

	//public static bool IsLoadScore = false;
	//public static long MyHighScore = 0;
	//public static ILeaderboard HighScoreLeaderboard;
	// Call this function to get things started.
	public void loadLeaderboards ()
	{
		// Do not get leaderboards if the app is running on Unity editor.
		if (Application.isEditor)
			return;

		// Check if user is logged in.
		if (Social.localUser.authenticated) {
			getHighScoreFromLeaderboard ();
		}
	}

	private void getHighScoreFromLeaderboard ()
	{
		//string[] UserFilter = { Social.localUser.id }; // Get the local player username
		//HighScoreLeaderboard = PlayGamesPlatform.Instance.CreateLeaderboard(); // initiate 
		//HighScoreLeaderboard.id = GPGS.GPGS_Const.leaderboard__; // The string ID from Google console
		//HighScoreLeaderboard.SetUserFilter(UserFilter); // Filter out other players
		//HighScoreLeaderboard.LoadScores(RWResult =>
		//{
		//    IsLoadScore = true;

		//    if (RWResult)
		//    {            
		//        MyHighScore = (int)HighScoreLeaderboard.localUserScore.value;
		//        Debug.Log("LoadScores Result111 : " + MyHighScore);
		//    }
		//});
	}

	#endregion


	#region Cloud

	public enum SaveGameType
	{
		None = 0,
		Save,
		Load,
	}

	SaveGameType m_SaveGameChk = SaveGameType.None;

	public void OpenSavedGame (string filename, SaveGameType type, bool bLogin = true, LoadingType loading = LoadingType.Circle)
	{

#if UNITY_EDITOR //이제 PC에서는 저장 불가.
        //if (type == SaveGameType.Save)
        //{
        //    GameManager.Instance.m_GameData.LoadData_PlayerPrefs(true);
        //    //DataManger.SaveToPlayerPrefs("FoodCrush_SaveData", GameManager.Instance.m_GameData);

        //    string data = DataManager.Serialize(GameManager.Instance.m_GameData);
        //    Debug.Log(data);
        //    NetWorkManager.instance.Req_userDataBackup(data);
        //}
        //else {
        //    GameManager.Instance.m_GameData = DataManager.LoadFromPlayerPrefs<GameData>("FoodCrush_SaveData");
        //    //GameManager.Instance.m_GameData.SaveData_PlayerPrefs();

        //    //***************************
        //    //데이타는 다에리서버에 로드
        //    //****************************
        //    NetWorkManager.instance.Req_userDataRestore(Native_Brige.GetUUID());
        //}
#elif UNITY_ANDROID
        if (Social.localUser.authenticated)
        {
            Loading.Instance.SetShow(true, loading); //Loading_Save, Loading_Load

            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            if (type == SaveGameType.Save)
                savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToSave); //저장루틴진행
            else
                savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToRead); //로딩루틴 진행          
        }
        else if (bLogin)
        {
            m_SaveGameChk = type;
            LoginGPGS();
        }
#endif
    }

    //클라우드 파일 저장
    void OnSavedGameOpenedToSave (SavedGameRequestStatus status, ISavedGameMetadata game)
	{
		if (status == SavedGameRequestStatus.Success) {
            // handle reading or writing of saved game.
            GameManager.Instance.m_GameData.LoadData_PlayerPrefs(true);
            string data = DataManager.Serialize(GameManager.Instance.m_GameData);
            //Debug.Log("세이브" + data);

            SaveGameData(game, Encoding.UTF8.GetBytes(data), DateTime.Now.TimeOfDay);
        } else {
			//파일열기에 실패 했습니다. 오류메시지를 출력하든지 합니다.
			Loading.Instance.SetShow (false); //Loading_Save

			Popup_Manager.INSTANCE.popup_index = 33;
			Popup_Manager.INSTANCE.popup_list [33].GetComponent<NoticePop> ().notice_setting (TableDataManager.Instance.GetLocalizeText (193) + "1011");
			Popup_Manager.INSTANCE.Popup_Push ();
		}
	}


	void SaveGameData (ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
	{
#if UNITY_ANDROID
		ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;


		SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder ();

		builder = builder

            .WithUpdatedPlayedTime (totalPlaytime)

            .WithUpdatedDescription ("Saved game at " + DateTime.Now);


		/*

        if (savedImage != null)

        {

            // This assumes that savedImage is an instance of Texture2D

            // and that you have already called a function equivalent to

            // getScreenshot() to set savedImage

            // NOTE: see sample definition of getScreenshot() method below

            byte[] pngData = savedImage.EncodeToPNG();

            builder = builder.WithUpdatedPngCoverImage(pngData);

        }*/


		SavedGameMetadataUpdate updatedMetadata = builder.Build ();

		savedGameClient.CommitUpdate (game, updatedMetadata, savedData, OnSavedGameWritten);
#endif
	}


	void OnSavedGameWritten (SavedGameRequestStatus status, ISavedGameMetadata game)
	{
        Loading.Instance.SetShow(false); //Loading_Save

        if (status == SavedGameRequestStatus.Success)
        {
            if (Popup_Manager.INSTANCE.popup_index != 2 && Popup_Manager.INSTANCE.popup_index != 35 && Popup_Manager.INSTANCE.popup_index != 7) //shop, 패키지팝업,ad_free 일때는 팝업 안띄운다.
            {
                Popup_Manager.INSTANCE.popup_index = 33;
                Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(192));
                Popup_Manager.INSTANCE.Popup_Push();
            }
        }
        else
        {
            if (Popup_Manager.INSTANCE.popup_index != 2 && Popup_Manager.INSTANCE.popup_index != 35 && Popup_Manager.INSTANCE.popup_index != 7) //shop, 패키지팝업,ad_free 일때는 팝업 안띄운다.
            {
                Popup_Manager.INSTANCE.popup_index = 33;
                Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(193) + "1012");
                Popup_Manager.INSTANCE.Popup_Push();
            }
        }

    }



	//클라우드 파일 로드

	public void OnSavedGameOpenedToRead (SavedGameRequestStatus status, ISavedGameMetadata game)
	{
		if (status == SavedGameRequestStatus.Success) {
			// handle reading or writing of saved game.
			LoadGameData (game);
		} else {
			//파일열기에 실패 한경우, 오류메시지를 출력하던지 합니다.
			Loading.Instance.SetShow (false); //Loading_Load

			Popup_Manager.INSTANCE.popup_index = 33;
			Popup_Manager.INSTANCE.popup_list [33].GetComponent<NoticePop> ().notice_setting (TableDataManager.Instance.GetLocalizeText (196) + "1021");
			Popup_Manager.INSTANCE.Popup_Push ();
		}
	}

	void LoadGameData (ISavedGameMetadata game)
	{
#if UNITY_ANDROID
		ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

		savedGameClient.ReadBinaryData (game, OnSavedGameDataRead);
#endif
	}


	void OnSavedGameDataRead (SavedGameRequestStatus status, byte[] data)
	{
        Loading.Instance.SetShow(false); //Loading_Save

        if (status == SavedGameRequestStatus.Success && data.Length > 0)
        {
            GameManager.Instance.m_GameData = JsonReader.Deserialize<GameData>(Encoding.UTF8.GetString(data));
            GameManager.Instance.m_GameData.SaveData_PlayerPrefs(); //GameData입장에서는 PlayerPrefs에 저장.
            //GameManager.Instance.GameSetting();

            //*****************
            //월드맵, UI, 차고
            //*****************
            if (GameManager.Instance.m_GameData.AdsFreeChk)
                AdMob_Manager.Instance.AdMob_Banner_Destory();
            StageItemManager.Instance.Init();
            UI_WorldMap.Instance.MyDataSetting();
            CarPortManager.Instance.MyFoodTruckSetting();

            //Debug.Log("다에리 Restore : 성공");// :" + data);
            Popup_Manager.INSTANCE.popup_index = 33;
            Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(195));//, GameQuit);
            Popup_Manager.INSTANCE.Popup_Push();
        }
        else
        {
            //Debug.Log("다에리 Restore : 실패");
            Popup_Manager.INSTANCE.popup_index = 33;
            Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(196) + "1022");
            Popup_Manager.INSTANCE.Popup_Push();
        }
    }

#endregion

}
