﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

public class Native_Brige : MonoBehaviour
{
#if UNITY_IOS
	[DllImport ("__Internal")]
	private static extern System.IntPtr GetLanguage_iOS();

        [DllImport ("__Internal")]
        private static extern System.IntPtr GetCountry_iOS_ISO_3166_1_alpha_2();

    [DllImport ("__Internal")]
	private static extern System.IntPtr GetCloudData_iOS(string key);

	[DllImport ("__Internal")]
	private static extern bool SetCloudData_iOS(string key, string uuid);
#endif

    public static bool isOnline()
    {
        bool isOnline = true;
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        isOnline = _adrjO.Call<bool>("isOnline");
#elif UNITY_IOS

#endif
        return isOnline;
    }

    public void onResume(string text)
    {
        Debug.Log("onResume : ");

        /*
        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
        {
            if (Popup_Manager.INSTANCE.popup_stack.Count <= 0)
            {
                Popup_Manager.INSTANCE.popup_index = 17;

                Popup_Manager.INSTANCE.Popup_Push();

               GameManager.Instance.OnResumechk = true;
            }
        }
        */

        Popup_Manager.INSTANCE.OnResumePauseChk();
    }

    //언어선택
    public static string GetLanguage()
    {
        string lan = "";
#if UNITY_EDITOR
        lan = "ko";
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID



        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        lan = _adrjO.Call<string>("GetLanguage");
#elif UNITY_IOS
		//ios 작업
		lan = Marshal.PtrToStringAnsi(GetLanguage_iOS());
#endif

        return lan;
    }
    
    /// <summary>AOS : alpha 3, IOS : alpha 2</summary>
    public static string GetCountry_ISO_3166_1()
    {
        string str = string.Empty;
#if UNITY_EDITOR
        str = "KOR";
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        str = _adrjO.Call<string>("GetCountry_ISO_3166_1_alpha_3");
#elif UNITY_IOS
        str = Marshal.PtrToStringAnsi(GetCountry_iOS_ISO_3166_1_alpha_2());
#endif
        return str;

    }

    //데이타 로드할때 사용한다. 서버껄로 교체한다.
    public static void SetUUID(string uuid)
    {
        SecurityPlayerPrefs.SetString("uuid", uuid);
    }

    //만약 다른기기에서 데이타로드를 하면 uuid가 바뀐다.
    //즉, uuid는 항상 이 함수를 호출해서 가져와야한다. 변수 할당해서 들고 있지 말것!
    public static string GetUUID()
    {
        string uuid = "";
#if UNITY_EDITOR
#elif UNITY_ANDROID
        //1회 가져오면 그다음부터는 PlayerPrefs에서 가져온다.
        //한번 할당된 id는 변경되지 않도록 구글드라이브에 저장한다.(세이브를 눌렀을때)
        //만약 기기가 변동 되더라도 세이브데이타를 로드하면 구글드라이브에 저장되어 있는 id를 가져와 넣어준다. 
        uuid = SecurityPlayerPrefs.GetString("uuid", "null");
        if (uuid.Equals("null"))
        {
            AndroidJNI.AttachCurrentThread();

            AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

            uuid = _adrjO.Call<string>("getDeviceUuid");
        }
#elif UNITY_IOS
		//ios 작업
		uuid = SecurityPlayerPrefs.GetString("uuid", "null");
		if (uuid.Equals("null"))
		{
			uuid = SystemInfo.deviceUniqueIdentifier;
		}
#endif

        return uuid;
    }

    public static void Application_Quit()
    {
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID



        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        _adrjO.Call("Application_MoveTaskToBack");
        _adrjO.Call("Application_Finish");
        _adrjO.Call("Application_KillProcess");
#elif UNITY_IOS
		//ios 작업
		
#endif

    }
    
    #region webview
    public static void CallMoreGame()
    {
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID

        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        _adrjO.Call("pgp_CallMoreGame");
#elif UNITY_IOS
		//ios 작업
		
#endif

    }

    public void CancelMoreGame()
    {
        //WorldMapUI.instance.CancelMoreGame();
    }
    #endregion

#if UNITY_IOS
	//iOS 클라우드 구
	public static string GetCloudData(string key)
	{
		Debug.Log("Unity GetCloudData");
		return Marshal.PtrToStringAnsi(GetCloudData_iOS(key));
	}

	public static bool SetCloudData(string key, string uuid)
	{
		Debug.Log("Unity SetCloudData");
		return SetCloudData_iOS(key, uuid);
	}
#endif

    #region 알람_사용안함.
    public void GamePlaychk(string str)
    {
        Debug.Log("GamePlaychk:::::::::::::::::::::");

#if UNITY_EDITOR

#elif UNITY_ANDROID
        //mPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //mCurrentActivity = mPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        //mCurrentActivity.Call("GamePlayChk",str);
#endif

    }

    public void AlarmSetting(string str)
    {

        Debug.Log("AlarmSetting:::::::::::::::::::::");
#if UNITY_EDITOR

#elif UNITY_ANDROID
        //mPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //mCurrentActivity = mPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        //mCurrentActivity.Call("Alarm",str);
#endif

    }

    public void releaseAlarm(string str)
    {

        Debug.Log("releaseAlarm:::::::::::::::::::::");
#if UNITY_EDITOR

#elif UNITY_ANDROID
        //mPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //mCurrentActivity = mPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        //mCurrentActivity.Call("releaseAlarm",str);
#endif

    }
    #endregion

    public void PluginLog(string text)
    {
        Debug.Log("Plugin : " + text);
    }

    #region 접근하는 법 볼려고 남겨놓음
    public static void SaveImageFile(byte[] data)
    {
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        _adrjO.Call("Save_Image", data);
#endif
    }


    public static void ShareFuntion()
    {
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        _adrjO.Call("Share_Function");
#endif
    }

    public static string GetPhoneNumber()
    {
        string pn = "";
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        pn = _adrjO.Call<string>("GetPhoneNumber");
#endif
        return pn;
    }

    public static int GetVersionCode()
    {
#if UNITY_EDITOR
        return 0;
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJavaClass contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
        string packageName = context.Call<string>("getPackageName");
        AndroidJavaObject packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
        return packageInfo.Get<int>("versionCode");
#elif UNITY_IOS
		return 1;
#endif
    }

    //int versionName =  context().getPackageManager().getPackageInfo(context().getPackageName(), 0).versionName;
    public static string GetVersionName()
    {
#if UNITY_EDITOR
        return "";
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJavaClass contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
        string packageName = context.Call<string>("getPackageName");
        AndroidJavaObject packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
        return packageInfo.Get<string>("versionName");
#elif UNITY_IOS
		return "";
#endif
    }

    public static bool CheckLootingPhone()
    {
        bool chk = false;;
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");

        chk = _adrjO.Call<bool>("execCmd");
#elif UNITY_IOS

#endif
        return chk;
    }

    public static void Firebase_FCM()
    {
#if UNITY_EDITOR
        //에디터이면서 안드로이드 세팅되어 있기 때문에 1차 검색이 EDITOR여야 하는군.
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();
        
        AndroidJavaClass _adrJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        AndroidJavaObject _adrjO = _adrJC.GetStatic<AndroidJavaObject>("currentActivity");
        _adrjO.CallStatic("FirebaseMessing");
#endif
    }
    #endregion
}
