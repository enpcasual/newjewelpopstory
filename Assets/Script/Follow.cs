﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {
    public GameObject FollowObj = null;

    // Update is called once per frame
    void Update() {
        if (FollowObj != null)
        {
            transform.position = FollowObj.transform.position;
        }

    }
}
