﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Board))]
public class GravityDisplayer : MonoBehaviour
{
    public static bool useDefaultGravity { get;private set;}


    [SerializeField]
    private List<Sprite> DropDirections;

    [SerializeField]
    private SpriteRenderer target;
    private Transform m_targetTransform;
    private Transform targetTransform { get { if (m_targetTransform == null) m_targetTransform = target.transform; return m_targetTransform; } }

    [SerializeField]
    private AnimationCurve alphaCurve;

    [SerializeField]
    private AnimationCurve positionCurve;

    private Vector3 offset ;

    private Board m_Board = null;
    public Board Board
    {
        get
        {
            if (m_Board == null)
            {
                m_Board = GetComponent<Board>();
                m_Board.m_GravityDisplayer = this;
            }
            return m_Board;
        }
    }

    void Awake()
    {
        var board = Board;
        offset = Vector3.back * 10f;
    }

    void OnEnable()
    {
        Item.OnClickItem += OnClickItem;
    }

    public static void ClearData()
    {
        useDefaultGravity = true;
    }

    private void OnClickItem(Item item)
    {
        Disable();
    }

    public void Play()
    {
        if (AnimationRoutine != null)
        {
            StopCoroutine(AnimationRoutine);
        }

        if (Board.Drop_Dir != DROP_DIR.U)
        {
            useDefaultGravity = false;
        }

        target.sprite = DropDirections[(int)Board.Drop_Dir];


        AnimationRoutine = StartCoroutine(RunAnimation(0f, 0.8f, GetDropPosition(Board.Drop_Dir)));
    }

    private void Disable()
    {
        if (AnimationRoutine != null)
        {
            StopCoroutine(AnimationRoutine);
        }
        
        StartCoroutine(DisableAnimation(0.3f));
    }

    private IEnumerator DisableAnimation(float time)
    {
        float t = 0;

        var defaultAlpha = target.color.a;

        while (t < time)
        {
            if (!Board.IsPanelFixed)
                target.color = new Color(1, 1, 1, Mathf.Lerp(defaultAlpha, 0, t / time));

            yield return null;

            t += Time.deltaTime;
        }

        target.color = new Color(1, 1, 1, 0);
    }

    private Coroutine AnimationRoutine;
    private IEnumerator RunAnimation(float to_alpha, float time, Vector3 to_pos)
    {
        float t = 0;
        bool nextPlayed = false;
        var defaultAlpha = 1f;

        targetTransform.localPosition = Vector3.zero + offset;
        
        while (t < time)
        {
            if (!Board.IsPanelFixed)
            {
                target.color = new Color(1, 1, 1,
                    alphaCurve.Evaluate(t / time));
                //Mathf.Lerp(defaultAlpha, to, t / time));
                targetTransform.localPosition = Vector3.Lerp(Vector3.zero, to_pos, positionCurve.Evaluate(t / time)) + offset;
            }

            if (t > time * 0.15f && nextPlayed == false)
            {
                var e = Board.GravityReference().GetEnumerator();
                while (e.MoveNext())
                {
                    e.Current.m_GravityDisplayer.Play();
                }
                nextPlayed = true;
            }

            yield return null;

            t += Time.deltaTime;
        }

        target.color = new Color(1, 1, 1, Mathf.Lerp(defaultAlpha, to_alpha, 1));
        

        AnimationRoutine = null;
    }

    private Vector3 GetDropPosition(DROP_DIR dir)
    {
        switch (dir)
        {
            case DROP_DIR.U: return new Vector3(0, -0.52f, 0);
            case DROP_DIR.D: return new Vector3(0, 0.52f, 0);
            case DROP_DIR.L: return new Vector3(0.52f, 0, 0);
            case DROP_DIR.R: return new Vector3(-0.52f, 0, 0);
            default:return Vector3.zero;
        }
    }


    void OnDisable()
    {
        Item.OnClickItem -= OnClickItem;
    }

    //void OnDestory()
    //{
    //    Item.OnClickItem -= OnClickItem;
    //}


    //private Dictionary<GravityDisplayer,Coroutine> 
    //public void Play_flow(GameObject targetobject)
    //{
    //    var references = Board.GravityReference();

    //    var objectList = new List<GameObject>(references.Count);

    //    objectList.Add(targetobject);

    //    for (int i = 1; i < references.Count; ++i)
    //    {
    //        var obj = ObjectPool.Instance.GetObject(MatchManager.Instance.Board_Prefab, MatchManager.Instance.GameField.transform);
    //        objectList.Add(obj);
    //    }

    //    for (int i = 0; i < references.Count; ++i)
    //    {
    //        StartCoroutine(RunFlowAnimation(objectList[i], references[i].m_GravityDisplayer, 1f));
    //    }

    //    if (references.Count > 1)
    //    {
    //        for (int i = 0; i < references.Count - 1; ++i)
    //        {

    //        }
    //    }

    //}

    //private Coroutine FlowAnimationRoutine;
    //private IEnumerator RunFlowAnimation(GameObject target,GravityDisplayer next, float time)
    //{
    //    float t = 0;

    //    Vector3 startPosition = target.transform.position;

    //    while (t < time)
    //    {
    //        target.transform.position = Vector3.Lerp(startPosition, this.transform.position, t / time);
    //        target.transform.LookAt(this.transform);

    //        yield return null;

    //        t += Time.deltaTime;
    //    }

    //    target.transform.position = Vector3.Lerp(startPosition, this.transform.position, 1);
    //    FlowAnimationRoutine = null;
    //}
}