﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodArvPanel : Panel {

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(def);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);
        //Defence--;
        //SetSprite(Defence);

        ////이펙트
        //PanelEffect panelEffect = EffectManager.Instance.GetPanelEffect();
        //panelEffect.BreadEffect(m_Board.transform.position);

        ////사운드
        //SoundManager.Instance.PlayEffect("defense_destroy");

        Complete(false, this);
    }
}
