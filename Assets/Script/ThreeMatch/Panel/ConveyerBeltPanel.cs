﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using JsonFx.Json;

public enum ConveyerBeltType
{
    Start,
    Mid,
    End,
}

//순서가 있어서 수정하면 안됨. 그래서 그냥 따로 만듦.
public enum ConveyerBeltDir 
{
    Left = 0,
    Top,
    Right,
    Bottom,
}

public class ConveyerBeltInfo
{
    public ConveyerBeltType Type;
    public ConveyerBeltDir Dir_In;
    public ConveyerBeltDir Dir_Out;
    public int WaropIndex;
}



public class ConveyerBeltPanel : Panel {

    //object링크
    public GameObject m_Mask; 
    public SpriteRenderer m_Active_1_1;
    public SpriteRenderer m_Active_1_2;
    public SpriteRenderer m_Active_2_1;
    public GameObject m_Particle;

    //const 값
    const float ConveyerBeltSpeed = 1.2f; //속도맞음. 올리면 빨라짐. 
    const Ease ConveyerBeltEase = Ease.InOutQuad;


    //컨베이어벨트 정보
    public ConveyerBeltInfo m_ConveyerBeltInfo;
    [HideInInspector] public Board EndBoard;   //캐싱용


    //variable
    public bool isStraight = true;
    //public bool FirstTiming = true; //깜빡이는 타이밍. 스테이지 로딩시 세팅.
    //public float deltaTime = 0f;
    //public bool isMoveAnim = false;
   
  public void SetArrow(bool onoff)
    {
        if(isStraight)
        {
            m_Active_1_1.gameObject.SetActive(onoff);
            m_Active_1_2.gameObject.SetActive(onoff);
        }
        else
        {
            m_Active_2_1.gameObject.SetActive(onoff);
        }
    }

    #region 컨베이어 벨트 화살표 순서대로 켜지는 시스템.
    //public void SetArrow(bool fistTiming)
    //{
    //    if(isStraight)
    //    {
    //        m_Active_1_1.gameObject.SetActive(fistTiming);
    //        m_Active_1_2.gameObject.SetActive(!fistTiming);
    //    }
    //    else
    //    {
    //        m_Active_2_1.gameObject.SetActive(fistTiming);
    //    }
    //}

    //public void Update()
    //{
    //    if (isMoveAnim)
    //    {
    //        deltaTime += Time.deltaTime;
    //        if (deltaTime > 0.2f)
    //        {
    //            deltaTime = 0f;
    //            SetArrow(FirstTiming);
    //            FirstTiming = !FirstTiming;
    //        }
    //    }
    //}
    #endregion

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        m_Active_1_1.gameObject.SetActive(false);
        m_Active_1_2.gameObject.SetActive(false);
        m_Active_2_1.gameObject.SetActive(false);
        //deltaTime = 9999; //시작할때 바로 켜지도록

        m_ConveyerBeltInfo = JsonReader.Deserialize<ConveyerBeltInfo>(addInfo);

        //**************************
        //컨베이어벨트 이미지 세팅
        //**************************
        int gap = Mathf.Abs(m_ConveyerBeltInfo.Dir_In - m_ConveyerBeltInfo.Dir_Out);
        if (gap == 2)
        {
            isStraight = true;
            //직선일때
            m_SpriteRender.sprite = List_Sprite[0];
            m_SpriteRender.transform.localEulerAngles = new Vector3(0, 0, (int)m_ConveyerBeltInfo.Dir_In * -90);
        }
        else
        {
            isStraight = false;
            //곡선일때
            m_SpriteRender.sprite = List_Sprite[1];
            if (m_ConveyerBeltInfo.Dir_Out.Next() == m_ConveyerBeltInfo.Dir_In)
                m_SpriteRender.transform.localEulerAngles = new Vector3(0, 0, (int)m_ConveyerBeltInfo.Dir_In * -90);
            else
                m_SpriteRender.transform.localEulerAngles = new Vector3(180, 0, (int)m_ConveyerBeltInfo.Dir_In * 90);
        }

        //***********
        //포탈마스크
        //***********
        Vector3 maks_zero = new Vector3(-0.36f, -0.36f, 0);
        if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start || m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
        {
            m_Mask.SetActive(true);

            ConveyerBeltDir chekc_dir;
            if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
                chekc_dir = m_ConveyerBeltInfo.Dir_In;
            else
                chekc_dir = m_ConveyerBeltInfo.Dir_Out;

            if (chekc_dir == ConveyerBeltDir.Left)
                m_Mask.transform.position = m_Board.GetBoardPos() + maks_zero + new Vector3(-0.72f, 0, 0.1f);  //Mask를 왼쪽과 오른쪽, 탑과 바텀이 겹치지 않도록 
            else if (chekc_dir == ConveyerBeltDir.Top)
                m_Mask.transform.position = m_Board.GetBoardPos() + maks_zero + new Vector3(0, 0.72f, 0.1f);   //0.1와 0.15로 위치를 다르게 적용.
            else if (chekc_dir == ConveyerBeltDir.Right)
                m_Mask.transform.position = m_Board.GetBoardPos() + maks_zero + new Vector3(0.72f, 0, 0.15f);    
            else if (chekc_dir == ConveyerBeltDir.Bottom)
                m_Mask.transform.position = m_Board.GetBoardPos() + maks_zero + new Vector3(0, -0.72f, 0.15f);
        }
        else
        {
            m_Mask.SetActive(false);
        }

        //***********
        //워프 이펙트
        //***********
        if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start || m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
        {
            m_Particle.SetActive(true);

            ConveyerBeltDir chekc_dir;
            if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
                chekc_dir = m_ConveyerBeltInfo.Dir_In; 
            else
                chekc_dir = m_ConveyerBeltInfo.Dir_Out;

            if (chekc_dir == ConveyerBeltDir.Left)
            {
                m_Particle.transform.position = m_Board.GetBoardPos() + new Vector3(-0.36f, 0, -0.2f);
                m_Particle.transform.eulerAngles = new Vector3(0, 0, 0);
            }             
            else if (chekc_dir == ConveyerBeltDir.Right)
            {
                m_Particle.transform.position = m_Board.GetBoardPos() + new Vector3(0.36f, 0, -0.2f);
                m_Particle.transform.eulerAngles = new Vector3(0, 0, 180);
            }        
            else if (chekc_dir == ConveyerBeltDir.Top)
            {
                m_Particle.transform.position = m_Board.GetBoardPos() + new Vector3(0, 0.36f, -0.2f);
                m_Particle.transform.eulerAngles = new Vector3(0, 0, 270);
            }
            else if (chekc_dir == ConveyerBeltDir.Bottom)
            {
                m_Particle.transform.position = m_Board.GetBoardPos() + new Vector3(0, -0.36f, -0.2f);
                m_Particle.transform.eulerAngles = new Vector3(0, 0, 90);
            }               
        }
        else
        {
            m_Particle.SetActive(false);
        }
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);
        //이펙트

        //사운드
       
        Complete(false, this);
    }


    public void ConveyerBeltInItSetting()
    {
        m_Board.CoveyerItem = m_Board.m_Item;
        m_Board.m_Item = null;
        //isMoveAnim = true;
        //deltaTime = 9999;
        SetArrow(true);
    }

    public void ConveyerBeltStop()
    {
        //isMoveAnim = false;
        SetArrow(false);
    }

    public void ConveyerBeltMove(System.Action Complete)
    {
        switch(m_ConveyerBeltInfo.Type)
        {
            case ConveyerBeltType.Start:
                ConveyerBeltMove_Start(Complete);
                break;
            case ConveyerBeltType.Mid:
                ConveyerBeltMove_Mid(Complete);
                break;
            case ConveyerBeltType.End:
                ConveyerBeltMove_End(Complete);
                break;
        }
    }

    public void ConveyerBeltMove_Start(System.Action Complete)
    {
        if(EndBoard.CoveyerItem == null)
        {
            Complete();
            return;
        }

        m_Board.GenItem(EndBoard.CoveyerItem.m_ItemType, EndBoard.CoveyerItem.m_Color, EndBoard.CoveyerItem.m_Value, false, false, true);
       
        switch(EndBoard.CoveyerItem.m_ItemType) //특수한 값을 따로 유지 해야하는 경우
        {
            case ItemType.JellyMon:
                {
                    ((JellyMon)m_Board.m_Item).CopyJellyMon(((JellyMon)EndBoard.CoveyerItem).m_EatCnt);
                }
                break;
        }

        float _z = 0f;
        switch (m_ConveyerBeltInfo.Dir_In)
        {
            case ConveyerBeltDir.Left:
                _z = 0.06f;
                m_Board.m_Item.transform.position = m_Board.GetBoardPos() + new Vector3(-MatchDefine.BoardHeight, 0, _z);
                break;
            case ConveyerBeltDir.Top:
                _z = 0.06f;
                m_Board.m_Item.transform.position = m_Board.GetBoardPos() + new Vector3(0, MatchDefine.BoardHeight, _z);
                break;
            case ConveyerBeltDir.Right:
                _z = 0.11f;
                m_Board.m_Item.transform.position = m_Board.GetBoardPos() + new Vector3(MatchDefine.BoardHeight, 0, _z);
                break;
            case ConveyerBeltDir.Bottom:
                _z = 0.11f;
                m_Board.m_Item.transform.position = m_Board.GetBoardPos() + new Vector3(0, -MatchDefine.BoardHeight, _z);
                break;
            default:
                break;
        }


        Vector3 des = m_Board.GetBoardPos() + new Vector3(0, 0, _z);
        Tween tween = m_Board.m_Item.transform.DOMove(des, ConveyerBeltSpeed).SetSpeedBased();
        tween.SetEase(ConveyerBeltEase);
        tween.OnComplete(() =>
        {
            m_Board.m_Item.transform.position = m_Board.GetBoardPos();
            m_Board.m_MatchingCheck = true;
            Complete();
        });
    }
    public void ConveyerBeltMove_Mid(System.Action Complete)
    {     
        //들어오는 아이템
        Board front_Board = null;
        if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Left)
            front_Board = m_Board[SQR_DIR.LEFT];
        else if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Right)
            front_Board = m_Board[SQR_DIR.RIGHT];
        else if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Top)
            front_Board = m_Board[SQR_DIR.TOP];
        else
            front_Board = m_Board[SQR_DIR.BOTTOM];


        if (front_Board.CoveyerItem == null)
        {
            Complete();
            return;
        }
            

        m_Board.m_Item = front_Board.CoveyerItem;
        m_Board.m_Item.m_Board = m_Board;

        Vector3 des = m_Board.GetBoardPos();
        Tween tween = m_Board.m_Item.transform.DOMove(des, ConveyerBeltSpeed).SetSpeedBased();
        tween.SetEase(ConveyerBeltEase);
        tween.OnComplete(() =>
        {
            m_Board.m_Item.transform.position = m_Board.GetBoardPos();
            m_Board.m_MatchingCheck = true;
            Complete();
        });
    }

    public void ConveyerBeltMove_End(System.Action Complete)
    {
        float _z = 0;
        if(m_ConveyerBeltInfo.Dir_Out == ConveyerBeltDir.Left || m_ConveyerBeltInfo.Dir_Out == ConveyerBeltDir.Top)
            _z = 0.06f;
        else
            _z = 0.11f;
        

        //들어오는 아이템 처리
        Board front_Board = null;
        if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Left)
            front_Board = m_Board[SQR_DIR.LEFT];
        else if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Right)
            front_Board = m_Board[SQR_DIR.RIGHT];
        else if (m_ConveyerBeltInfo.Dir_In == ConveyerBeltDir.Top)
            front_Board = m_Board[SQR_DIR.TOP];
        else
            front_Board = m_Board[SQR_DIR.BOTTOM];

        if (front_Board.CoveyerItem != null)
        {
            m_Board.m_Item = front_Board.CoveyerItem;
            m_Board.m_Item.m_Board = m_Board;

            m_Board.m_Item.transform.position += new Vector3(0, 0, _z);
            Vector3 des = m_Board.GetBoardPos() + new Vector3(0, 0, _z);
            Tween tween = m_Board.m_Item.transform.DOMove(des, ConveyerBeltSpeed).SetSpeedBased();
            tween.SetEase(ConveyerBeltEase);
            tween.OnComplete(() =>
            {
                m_Board.m_Item.transform.position = m_Board.GetBoardPos();
                m_Board.m_MatchingCheck = true;
                //Complete();
            });
        }

        //나가는 아이템 처리
        if (m_Board.CoveyerItem != null)
        {
            Vector3 out_des = Vector3.zero;
            switch (m_ConveyerBeltInfo.Dir_Out)
            {
                case ConveyerBeltDir.Left:
                    out_des = m_Board.GetBoardPos() + new Vector3(-MatchDefine.BoardHeight, 0, _z);
                    break;
                case ConveyerBeltDir.Top:
                    out_des = m_Board.GetBoardPos() + new Vector3(0, MatchDefine.BoardHeight, _z);
                    break;
                case ConveyerBeltDir.Right:
                    out_des = m_Board.GetBoardPos() + new Vector3(MatchDefine.BoardHeight, 0, _z); //Mask Size가 0.05이고 0.1를 이동함.
                    break;
                case ConveyerBeltDir.Bottom:
                    out_des = m_Board.GetBoardPos() + new Vector3(0, -MatchDefine.BoardHeight, _z); //Mask Size가 0.05이고 0.1를 이동함.
                    break;
            }
            m_Board.CoveyerItem.transform.position += new Vector3(0, 0, _z);
            Tween tween = m_Board.CoveyerItem.transform.DOMove(out_des, ConveyerBeltSpeed).SetSpeedBased();
            tween.SetEase(ConveyerBeltEase);
            tween.OnComplete(() =>
            {             
                m_Board.CoveyerItem.Destroy();
                Complete();
            });
        }
        else
        {
            Complete();
        }
    }


}
