﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class CreatorPanel : Panel {

    public SpriteRenderer sr_handle;

    //UIMask m_Mask;
    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);

        //m_Mask = transform.GetComponentInChildren<UIMask>();
        //m_Mask.transform.localPosition = new Vector3(-MatchDefine.BoardWidth/2, -MatchDefine.BoardHeight / 2, -transform.localPosition.z + m_Mask.depth/2);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Complete(false, this);
    }

    override public void AddAction()
    {
        if (sr_handle != null)
        {
            //아이템이 연속으로 생성되면 0.5초 사이에 2~3개는 중첩될텐데.. 음 근데 일단 플레이상 문제 없음..
            Tweener tw = sr_handle.transform.DOLocalRotate(new Vector3(0, 0, -360), 0.5f, RotateMode.FastBeyond360);
            tw.SetEase(Ease.OutQuart);
            tw.OnComplete(() =>
            {
                sr_handle.transform.localEulerAngles = Vector3.zero;
            });
        }
    }
}
