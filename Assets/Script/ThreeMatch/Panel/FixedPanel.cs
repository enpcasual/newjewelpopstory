﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FixedPanel : Panel {

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);
        //이펙트

        //사운드

        Complete(false, this);
    }

}
