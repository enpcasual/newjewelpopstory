﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreadPanel : Panel {

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Defence--;
        SetSprite(Defence);      

        //이펙트
        EffectManager.Instance.BreadEffect(m_Board.transform.position, Defence);

        //사운드
        SoundManager.Instance.PlayEffect("bread_destroy");

        Complete(true, this);
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Bread);
    }

}
