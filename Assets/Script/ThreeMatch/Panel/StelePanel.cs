﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SteleType
{
    Stele_2X1 = 1,
    Stele_1X2 = 2,
    Stele_2X2 = 3,
    Stele_3X2 = 4,
    Stele_2X3 = 5,
}

public class StelePanel : Panel {

    List<Board> m_ListSteleBoard = null;

    static public Vector3 GetCorrectPos(int steletype)
    {
        Vector3 _correctpos = Vector3.zero;
        switch (steletype)
        {
            case 1:
                {
                    _correctpos = new Vector3(0.36f, 0, 0);
                }
                break;
            case 2:
                {
                    _correctpos = new Vector3(0, -0.36f, 0);
                }
                break;
            case 3:
                {
                    _correctpos = new Vector3(0.36f, -0.36f, 0);
                }
                break;
            case 4:
                {
                    _correctpos = new Vector3(0.72f, -0.36f, 0);
                }
                break;
            case 5:
                {
                    _correctpos = new Vector3(0.36f, -0.72f, 0);
                }
                break;
        }

        return _correctpos;
    }

    //이 함수는 각 석판중에 첫번째Board에서만 들어온다.
    public void SetSteleBoards(SteleType type)
    {
        if(m_ListSteleBoard == null)
            m_ListSteleBoard = new List<Board>();

        m_ListSteleBoard.Clear();

        switch (type)
        {
            case SteleType.Stele_2X1:
                m_ListSteleBoard.Add(m_Board);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.RIGHT]);
                break;
            case SteleType.Stele_1X2:
                m_ListSteleBoard.Add(m_Board);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOM]);
                break;
            case SteleType.Stele_2X2:
                m_ListSteleBoard.Add(m_Board);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.RIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOM]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOMRIGHT]);
                break;
            case SteleType.Stele_3X2:
                m_ListSteleBoard.Add(m_Board);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.RIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.RIGHT][SQR_DIR.RIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOM]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOMRIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOMRIGHT][SQR_DIR.RIGHT]);
                break;
            case SteleType.Stele_2X3:
                m_ListSteleBoard.Add(m_Board);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOM]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOM][SQR_DIR.BOTTOM]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.RIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOMRIGHT]);
                m_ListSteleBoard.Add(m_Board[SQR_DIR.BOTTOMRIGHT][SQR_DIR.BOTTOM]);
                break;
        }
    }

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);

        m_SpriteRender.transform.localPosition = GetCorrectPos(Defence);
    }

    //이 함수는 SteleHidePanel에서만 호출한다.!!
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        bool _clear = true;
        for(int i = 0; i < m_ListSteleBoard.Count; i++)
        {
            //일단 겹수가 0인지로 체크 할껀데 만약 타이밍이 너무 빠르면
            // 코루틴같이 시간 좀 기달려도 된다.
            if(m_ListSteleBoard[i].GetPanel(PanelType.Stele_Hide) != null && m_ListSteleBoard[i].GetPanel(PanelType.Stele_Hide).Defence > 0)
            {
                _clear = false;
                break;
            }
        }

        if(_clear)
        {
            //이펙트 수정해야함.
            //EffectManager.Instance.BreadEffect(m_Board.transform.position, 0);

            //클리어 사운드
            SoundManager.Instance.PlayEffect("slate_find");


            for (int i = 0; i < m_ListSteleBoard.Count; i++)
            {
                m_ListSteleBoard[i].PanelDestroy(PanelType.Stele);
            }

            MissionApply();

            //여기는 예외적으로 PanelBrust에서 탄게 아니므로 Complete를 타지 않는다.
            //Complete(true, this); 

            //기본 Brust 구조를 타지않기 때문에 여기에서 점수를 보여준다.
            MatchManager.Instance.ScoreTextShow(DestoryScore, m_Board, ColorType.None);
        }
    }

    override public void MissionApply()
    {
        //이미지 위치가 이미 이동되어 있기 때문에 Sprite객체를 넘겨준다.
        MissionManager.Instance.MissionApply_Stele(m_SpriteRender.gameObject, MissionType.Stele, MissionKind.Stele, Defence);
    }

}
