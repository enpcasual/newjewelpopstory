﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IceCreamPanel : Panel {

    public static int m_Count = 0;
    public static bool m_IceCreamBrust = false;

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Defence--;
        SetSprite(Defence);

        //이펙트
        EffectManager.Instance.IceCreamEffect(m_Board.transform.position, Defence);

        //사운드
        SoundManager.Instance.PlayEffect("icecream_removed");

        m_IceCreamBrust = true;

        Complete(true, this);
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.IceCream, MissionKind.IceCream);
    }

    void OnEnable()
    {
        m_Count++;
    }

    void OnDisable()
    {
        m_Count--;
    }

}
