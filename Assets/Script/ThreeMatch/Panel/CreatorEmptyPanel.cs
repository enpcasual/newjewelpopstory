﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatorEmptyPanel : Panel {

    //UIMask m_Mask;
    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);

        //m_Mask = transform.GetComponentInChildren<UIMask>();
        //m_Mask.transform.localPosition = new Vector3(-MatchDefine.BoardWidth/2, -MatchDefine.BoardHeight / 2, -transform.localPosition.z + m_Mask.depth/2);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Complete(false, this);
    }

}
