﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LollyCagePanel : Panel {

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Defence--;
        SetSprite(Defence);    

        //이펙트
        EffectManager.Instance.LollyCageEffect(m_Board.transform.position, Defence == 0 ? 1 : 2);

        SoundManager.Instance.PlayEffect("cage_destroy", false, 0.5f);
        Complete(true, this);
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.LollyCage);
    }
}
