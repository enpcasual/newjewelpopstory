﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PanelType
{
    //*****************주의 사항*****************
    //값이 0보다 크면 아이템보다 뒤에 생성 된다.
    //Default값이 제일 크지만 순성상 맨 처음에 
    //놓았으므로 enum의 자동증감으로 값이
    //들어가지 않도록 모든 값을 지정해야한다.
    //*******************************************
    //enum값 수정시 
    // 1. PanelManager의 Inspector에서 해당 항목 PanelType 수정
    // 2. Editor모드에서 패널 항목들의 Extension목록의 PanelComp.PanelType 재 설정
    Default_Empty = 101,
    Default_Full = 100,

    //*********************************
    //기본 생성기_이미지 없음
    //*********************************
    Creator_Empty = 90, //Default 패널들과 동급

    //*********************************
    // 2.Bottom - 패널 아래(석판)
    //*********************************
    //석판_혹시라도 나중에 와퍼나 잼이 석판 위에 존재할경우 대비해서 여기에 추가
    //(enum값은 게임상에서 그리 순서일뿐이고 에디터에서 같이 찍을수 없다.)
    Stele = 82,                 //석판은 미션패널(B_Botto)과 동급


    //*********************************
    // 3.Floor - 패널 바닥(와퍼)
    //*********************************
    Stele_Hide = 81,

    //*********************************
    //미션관련 판넬. 
    //기본판넬과 특수판낼 사이에 위치
    //*********************************
    Wafer_floor = 80,
    Jam = 79,

    //*********************************
    //아이템보다 뒤로 가는 판넬.
    //*********************************
    JewelTree_D = 35,
    JewelTree_C = 34,
    JewelTree_B = 33,
    JewelTree_A = 32,
    Ring = 31,
    MagicColor = 30,
    S_Tree = 29,
    Cracker = 28,
    Cake_D = 27,
    Cake_C = 26,
    Cake_B = 25,
    Cake_A = 24,
    ConveyerBelt = 23,                    
    IceCream_Block = 22,
    IceCream_Creator = 21,
    Bread_Block = 20,
    Fixed_Block = 19,

    //*********************************
    //0은 아이템이 존재하는 위치
    //*********************************

    //*********************************
    //아이템보다 앞에 보이는 판넬
    //*********************************
    Lolly_Cage = -10,
    Ice_Cage = -11,
    Bottle_Cage = -12,

    //*********************************
    //추가 생성기_이미지 있음
    //*********************************
    Creator_Food = -20,
    Creator_Sprial = -21,
    Creator_TimeBomb = -22,
    Creator_Food_Sprial = -23,
    Creator_Food_TimeBomb = -24,
    Creator_Sprial_TimeBomb = -25,
    Creator_Key = -26,
    Creator_Key_Food = -27,
    Creator_Key_TimeBomb = -28,

    //*********************************
    //패널 이동장치
    //********************************
    Warp_In = -30,
    Warp_Out = -31,


    FoodArrive = -32,
    JellyBearStart = -33,

}

[System.Serializable]
public class PanelObj
{
    public PanelType m_PanelType;
    public GameObject m_PanelObj;
}

public class PanelManager : MonoBehaviour {
    static public PanelManager Instance;

    public List<PanelObj> List_Panel;

    //Editor모드에서 패널의 정보에 접근하기 위해..
    public Dictionary<PanelType, Panel> Dic_PanelInfo = new Dictionary<PanelType, Panel>();

    void Awake()
    {
        Instance = this;
#if UNITY_EDITOR
        foreach(PanelObj info in List_Panel)
        {
            Dic_PanelInfo[info.m_PanelType] = info.m_PanelObj.GetComponent<Panel>();
        }
#endif
    }
    public GameObject CreatePanel(PanelType type , Transform parentBoard)
    {
        GameObject ItemObj = null;
        for (int i = 0; i < List_Panel.Count; i++)
        {
            if (List_Panel[i].m_PanelType == type)
            {
                ItemObj = ObjectPool.Instance.GetObject(List_Panel[i].m_PanelObj, parentBoard);
                break;
            }
        }
        return ItemObj;
    }

    public Panel GetPanelInfo(PanelType type)
    {
        for (int i = 0; i < List_Panel.Count; i++)
        {
            if (List_Panel[i].m_PanelType == type)
            {
                return List_Panel[i].m_PanelObj.GetComponent<Panel>();
            }
        }
        return null;
    }
}
