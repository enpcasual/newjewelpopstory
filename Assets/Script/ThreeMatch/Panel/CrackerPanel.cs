﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrackerPanel : Panel {

    ColorType m_Color = ColorType.None;
    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);

        //SetSprite(def);
        SetColorSprite(def);
    }

    public void SetColorSprite(int def)
    {
        m_Color = (ColorType)def;
        if (m_Color == ColorType.Rnd)
            m_Color = MatchManager.Instance.m_AppearColor[UnityEngine.Random.Range(0, MatchManager.Instance.m_AppearColor.Count)];

        m_SpriteRender.sprite = List_Sprite[(int)m_Color - 1];

    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    bool _ok = false;
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);
        if (arroundRoot == null)
        {
            //스폐셜 아이템으로 인한 폭발
            _ok = true;
        }
        else
        {
            if (arroundRoot.m_Item == null)
            {
                Debug.LogError("아이템은 무조껀 있을꺼라고 예상했는데 어떻게 타는거지??");
                _ok = false;
            }
            else
            {
                if (arroundRoot.m_Item.m_Color == m_Color)
                    _ok = true;
                else
                    _ok = false;
            }
        }


        if(_ok)
        {
            Defence = 0;
            // SetSprite(Defence);

            //이펙트
            EffectManager.Instance.CrackerEffect(m_Board.transform.position, (int)m_Color - 1);

            //사운드
            SoundManager.Instance.PlayEffect("cracker_destroy");     
        }

        Complete(_ok, this);
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Cracker);
    }
}
