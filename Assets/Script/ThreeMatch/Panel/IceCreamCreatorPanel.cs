﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IceCreamCreatorPanel : Panel {

    public static int m_Count = 0;

    const float rotate_speed = 150f;
    void Update()
    {
        m_SpriteRender.transform.Rotate(Vector3.back * rotate_speed * Time.deltaTime);
    }

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);
        //이펙트       

        //사운드
        //SoundManager.Instance.PlayEffect("defense_destroy");

        Complete(false, this);
    }

    void OnEnable()
    {
        m_Count++;
    }

    void OnDisable()
    {
        m_Count--;
    }

}
