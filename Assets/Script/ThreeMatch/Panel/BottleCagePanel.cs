﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BottleCagePanel : Panel {
    private void OnDisable()
    {
        isAnim = false;
        if (m_Board != null)
        {
            m_Board.m_PanelBrusting_Ctn = 0;
        }
    } 

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //특별한 패널
    //Board에서 타지 않고 KeyItem쪽에서 탄다.
    public void BottleBrust(KeyItem key)
    {
        //base.Brust(isAroundBrust, Complete);

        StartCoroutine(Co_BottleBrust(key.m_Color, key.m_Board));
    }

    bool isAnim = false;
    IEnumerator Co_BottleBrust(ColorType color, Board board)
    {
        isAnim = true;

        //키가 날라가서 보물상자를 깨땔까지 매칭스텝을 넘어가지 않아야한다. 
        //(미션스텝으로 먼저 가버려서 미션클리어 체크가 안되는 문제발생)
        m_Board.m_PanelBrusting_Ctn++;

        KeyItem animkey = (KeyItem)board.CreateItem(ItemType.Key, color);

        //이펙트
        ParticleManager shine = EffectManager.Instance.BottleShine(board.transform.position);
        shine.transform.parent = animkey.transform;

        animkey.transform.position += new Vector3(0, 0, -1f);
        float duration = 0.35f;
        Vector3 pos = m_Board.transform.position + new Vector3(0, 0, -1f);
        Sequence seq = DOTween.Sequence();
        seq.Insert(0f, animkey.transform.DOScale(1.5f, duration).SetEase(Ease.OutCubic));
        seq.Insert(duration, animkey.transform.DOScale(0.9f, duration).SetEase(Ease.InCubic));
        seq.Insert(0f, animkey.transform.DOMove(pos, duration*2).SetEase(Ease.Linear));
        seq.OnComplete(() =>
        {               
            isAnim = false;
            shine.transform.parent = MatchManager.Instance.GameField.transform;
            ObjectPool.Instance.Restore(animkey.gameObject);
        });

        while (isAnim)
        {
            yield return null;
        }

        Defence--;
        SetSprite(Defence);

        //이펙트
        EffectManager.Instance.BottleEffect(m_Board.transform.position, Defence);

        //사운드
        SoundManager.Instance.PlayEffect("bottle_destroy");

        //점수를 보여준다.
        MatchManager.Instance.ScoreTextShow(DestoryScore, m_Board, ColorType.None);

        MissionApply();

        //미션Apply하면 해제한다.(그전까지 미션스텝으로 넘어가면 안된다)
        m_Board.m_PanelBrusting_Ctn--;

        //Defece가 0이면 삭제
        if (Defence <= 0)
        {
            //유리병이 사라지면 매칭 체크 해야한다.
            //하지만 아이템 Brusting 상태니까 당연히 매칭스텝으로 가겠지.
            m_Board.m_MatchingCheck = true;

            Board.m_ListBottlePanel.Remove(this);

            //보물상자가 없으면 열쇠 삭제 및 드랍 안함.
            if (Board.m_ListBottlePanel.Count <= 0 && KeyItem.m_Count > 0)
            {
                for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
                {
                    board = MatchManager.Instance.m_ListBoard[i];

                    if (board.m_Item is KeyItem)
                    {
                        ColorType colorIndex = board.m_Item.m_Color;
                        board.GenItem(ItemType.Normal, colorIndex); //체인지

                        //사운드
                        //SoundManager.Instance.PlayEffect("bear_goal");
                    }
                }
            }

            m_Board.PanelDestroy(this);        
        }
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Bottle);
    }
}
