﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class JewelTreePanel : Panel {

    int JewelCnt = 0;

    public List<SpriteRenderer> m_ListJewel;
    //아이템이 날라가도록 변경되서 사용안함.(원래 사탕이 날라가는거라 기존좌표 백업용)
    //public List<Vector3> m_ListStartPoint = new List<Vector3>(); 

    //이 패널은 4개의 보드가 하나의 패널을 들고 있다.

    public override void Awake()
    {
        base.Awake();

        //시작 포인트를 가져온다.
        //즉, 보석위치는 프리펩에서 잡아준다.
        //for (int i = 0; i < m_ListJewel.Count; i++)
        //{
        //    m_ListStartPoint.Add(m_ListJewel[i].transform.localPosition);
        //}
    }

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);

        //보석초기화
        Jewel_Init();
    }

    public void Jewel_Init()
    {
        JewelCnt = 0;

        for (int i = 0; i < m_ListJewel.Count; i++)
        {
            //m_ListJewel[i].transform.localPosition = m_ListStartPoint[i];
            m_ListJewel[i].gameObject.SetActive(false);           
        }
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        if (isCo_Jewel)
        {
            //쥬얼리 생성중에 다른 보드에서 중복으로 타면 여기를 탄다.
            Complete(false, this);
        }
        else
        {
            StartCoroutine(Co_JewelProcess(Complete));
        }

    }

    bool isCo_Jewel = false;
    public IEnumerator Co_JewelProcess(System.Action<bool, Panel> Complete)
    {
        isCo_Jewel = true;   
        
        //하나씩 생성 
        m_ListJewel[JewelCnt].gameObject.SetActive(true);
        m_ListJewel[JewelCnt].transform.localScale = Vector3.zero;
        switch(JewelCnt)
        {
            case 0:
                m_ListJewel[JewelCnt].transform.DOScale(1f, 0.3f).SetEase(Ease.Linear);
                break;
            case 1:
                m_ListJewel[JewelCnt].transform.DOScale(1f, 0.3f).SetEase(Ease.Linear);
                break;
            case 2:
                m_ListJewel[JewelCnt].transform.DOScale(1f, 0.3f).SetEase(Ease.Linear);
                break;
            case 3:
                m_ListJewel[JewelCnt].transform.DOScale(1f, 0.3f).SetEase(Ease.Linear);
                break;
        }

        //이펙트
        EffectManager.Instance.JewelTreeEffect(m_ListJewel[JewelCnt].transform.position);

        //cnt 증가
        JewelCnt++;

        yield return new WaitForSeconds(0.2f);

        //사운드
        SoundManager.Instance.PlayEffect("cre_candy2");

        yield return new WaitForSeconds(0.3f);

        if (JewelCnt == 4)
        {
            //*****************************
            //1.캔디 자리에 아이템으로 교체
            //*****************************
            //보석초기화
            Jewel_Init();

            //아이템 생성(애니메이션 용)
            List<Item> li_Item = new List<Item>();
            for (int i = 0; i < 4; i++)
            {
                //생성
                li_Item.Add(m_Board.CreateItem(MatchManager.Instance.m_CSD.JewelTreeItem[i], MatchManager.Instance.m_CSD.JewelTreeItemColor[i]));

                //좌표, 스케일
                li_Item[i].transform.position = m_ListJewel[i].transform.position + new Vector3(0, -0.36f, -1); //패널들보다 위로 날라가야한다.
                li_Item[i].transform.localScale = m_ListJewel[i].transform.localScale;

                //이펙트
                EffectManager.Instance.ItemChangeEffect(li_Item[i].transform.position);
            }
            //생성 사운드
            SoundManager.Instance.PlayEffect("tm");
           

            yield return new WaitForSeconds(0.5f);


            //*****************************
            //2.아이템 날라가는 애니메이션
            //*****************************
            //날라갈수 있는 보드 체크
            List<Board> list_bd = MatchManager.Instance.GetNormalItemBoards(4);

            //날라갈때 사운드
            SoundManager.Instance.PlayEffect("blockchange");

            int okcnt = 0;
            float duration = 0.4f;
            for (int i = 0; i < list_bd.Count; i++)
            {
                //선택된 보드는 아이템이 날라가는 사이에 Brust나 drop 되면 안된다.
                list_bd[i].m_ItemBrusting = true;
                list_bd[i].m_DropAnim = true; 

                Sequence seq = DOTween.Sequence();
                seq.Insert(0f, li_Item[i].transform.DOScale(2f, duration).SetEase(Ease.OutCubic));
                seq.Insert(duration, li_Item[i].transform.DOScale(1f, duration).SetEase(Ease.InCubic));
                seq.Insert(0f, li_Item[i].transform.DOMove(list_bd[i].transform.position + new Vector3(0, 0, -1f), duration*2).SetEase(Ease.Linear)); //패널들보다 위로 날라가야한다.
                seq.OnComplete(() =>
                {              
                    okcnt++;                  
                });
            }

            while(true)
            {
                if(okcnt == list_bd.Count)
                    break;

                yield return null;
            }

            //*****************************
            //3. 아이템 교체
            //*****************************
            //아이템생성
            for (int i = 0; i < list_bd.Count; i++)
            {
                //그자리에 아이템 생성
                list_bd[i].GenItem(li_Item[i].m_ItemType, li_Item[i].m_Color); //컬러세팅값이 랜덤일때 날라가는블럭과 색이 같도록 li_Item에서 가져온다

                //이펙트
                EffectManager.Instance.ItemChangeEffect(list_bd[i].transform.position);

                //위에서 true해준걸 정상 복구
                list_bd[i].m_ItemBrusting = false;
                list_bd[i].m_DropAnim = false;

                list_bd[i].m_MatchingCheck = true;
            }
            //생성 사운드
            SoundManager.Instance.PlayEffect("tm");

            //날라가는 아이템 삭제 ☆☆날라가는 블럭과 실제 생성되는 블럭의 컬러가 같도록 생성후에 삭제☆☆
            for (int i = 0; i < 4; i++)
            {
                ObjectPool.Instance.Restore(li_Item[i].gameObject);
            }
            li_Item.Clear();
        }

        isCo_Jewel = false;

        //실제로 터지지 않으므로 false
        Complete(false, this);
    }
}
