﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DefaultPanel : Panel
{
    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(def);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Complete(false, this);
    }


    public override void SetSprite(int def)
    {
        if (m_SpriteRender == null)
            return;

        if ((m_Board.Y * MatchDefine.MaxX + m_Board.X) % 2 == 0)
            m_SpriteRender.sprite = List_Sprite[0];
        else
            m_SpriteRender.sprite = List_Sprite[1];
    }
}
