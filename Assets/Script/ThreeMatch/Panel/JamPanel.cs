﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JamPanel : Panel { //MissonPanel

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);

    }

    ////1.터졌을때 보드 처리 2.이펙트 3.기타작업
    //override public void Brust(bool isAroundBrust, System.Action Complete)
    //{
    //    //base.Brust(isAroundBrust, Complete);

    //    Defence--;
    //    SetSprite(Defence);

    //    //이펙트
    //    EffectManager.Instance.WaferEffect(m_Board.transform.position, Defence);

    //    //사운드
    //    SoundManager.Instance.PlayEffect("w_crush");

    //    Complete();
    //}

    override public void Create()
    {
        //이펙트
        EffectManager.Instance.JamEffect(m_Board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("icecream_grows");

        //미션체크
        MissionApply();
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.Jam, MissionKind.Jam);
    }
}
