﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RingPanel : Panel {

    public ColorType m_Color = ColorType.None;
    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);

        //SetSprite(def);
        SetColorSprite(def);
    }

    public void SetColorSprite(int def)
    {
        m_Color = (ColorType)def;
        if (m_Color == ColorType.Rnd)
            m_Color = MatchManager.Instance.m_AppearColor[UnityEngine.Random.Range(0, MatchManager.Instance.m_AppearColor.Count)];

        m_SpriteRender.sprite = List_Sprite[(int)m_Color - 1];

    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    bool _ok = false;
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        StartCoroutine(Co_ItemCombine(arroundRoot));

        Complete(true, this);
    }

    IEnumerator Co_ItemCombine(Board arroundRoot)
    {
        //모든 처리를 여기서 해야한다.Complete()를 여기로 끌고 오면 안된다.
        //왜냐면 Complete를 바로 타지 않고 한프레임라도 늦게 타면
        //패널변수나 for문의 i값이 바뀌므로 현재 패널을 찾을수 없다.(변수를 따로 할당해야한다.)
        m_Board.m_ItemBrusting = true;
        m_Board.ItemDrop(arroundRoot, false);

        while (m_Board.m_DropAnim)
            yield return null;

        //이펙트
        EffectManager.Instance.ItemChangeEffect(m_Board.transform.position);
        //사운드
        SoundManager.Instance.PlayEffect("tm");

        //라인생성
        m_Board.GenItem(Random.Range(0, 2) == 0 ? ItemType.Line_X : ItemType.Line_Y, m_Color);

        //점수를 보여준다.
        MatchManager.Instance.ScoreTextShow(DestoryScore, m_Board, m_Color);

        m_Board.m_MatchingCheck = true;
        m_Board.m_ItemBrusting = false;

        m_Board.PanelDestroy(this);
    }
}
