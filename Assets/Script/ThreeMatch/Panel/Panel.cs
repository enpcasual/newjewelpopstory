﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class Panel : MonoBehaviour
{
    //*******************
    //Inspector Setting
    //********************
    public List<Sprite> List_Sprite;

    public int CreateScore;
    public int DestoryScore;

    public bool ItemExist = true;           //아이템이 존재할수 있는가?
    public bool ItemDrop = true;            //아이템이 드랍할수 있는가?
    public bool ItemMatch = true;           //아이템이 매칭될수 있는가?
    public bool ItemSwitch = true;          //아이템을 스위칭(터치)할수 있는가?
    public bool ItemBrust = true;           //아이템이 터질수 있는가?
    public bool PanelBrust = true;          //패널이 매칭으로 터질수 있는가?
    //원래는 PanelBrust가 true라면 그 패널은 사라지는걸 의미했다.
    //하지만 JewelTree는 Brust이면서 사라지지 않는 속성을 가지게 되었고 미션패널조건을 TogetherMission와 PanelBrust로 할수 없게 되었다.
    public bool PanelDestroy = true;        //미션패널이 사라지는가?
    public bool ArountBrust = true;         //주변매치로 인해 터지는가?
    //TogetherMission는 Wafer나 Jam이 컨베이어에서는 증식가능하고 매직컬러에서는 증식할수 없는걸 구분하기 위해 추가되었다.
    //컨베이어는 특수패널로 따로 관리되기 때문에 게임 프로세스에서는 터지지 않는걸로만 구분해도 되지만
    //패널이 생성되기전 미션초기화부분에서 데이타(Stage,CSD)로만 구분을 해야될때 필요하다.
    //즉, 관리상의 구분이 아닌 패널 자체를 구분할수있는 속성이 꼭 들어가야한다.
    public bool TogetherMission = true;      //미션패널이 있을수 있나.(와퍼가 같이 있을수 있나)


    // 8/28 보석트리 추가로 인한 조건 재 정리...(Brust_Cond 삭제, Destory 추가)
    //┌--------┬--------------┬-------------┬----------------┐
    //│        │              │ ItemExist O │ ItemExist X    │ 
    //├--------┼--------------┼-------------┼----------------┤
    //│        │ Destroy O    │ 케이지..    │ 빵..           │ 
    //│Brust O ├--------------┼-------------┼----------------┤
    //│        │ Destroy X    │             │ 보석트리..     │
    //├--------┼--------------┼-------------┼----------------┤
    //│        │ Destroy O    │ 유리병..    │ 컬러링..       │ 
    //│Brust X ├--------------┼-------------┼----------------┤
    //│        │ Destroy X    │ 쨈,컨벨트.. │ 생성기..       │ 
    //└--------┴--------------┴-------------┴----------------┘

    //*********
    //Variable
    //*********
    [HideInInspector] public SpriteRenderer m_SpriteRender;

    [HideInInspector] public Board m_Board;
    [HideInInspector] public PanelType m_PanelType;
    [HideInInspector] public int Defence = 1;                 //얼음을 부셔야 하는 단계
    [HideInInspector] public int Value = 0;                   //각 패널마다 추가로 사용하는 값.

    [HideInInspector] public float PanelDestoryTime = 0.25f;  //패널 삭제 대기 시간 == 일정시간안에 폭발은 1회폭발
    //**************
    //MonoBehaviour
    //**************
    virtual public void Awake()
    {
        m_SpriteRender = GetComponentInChildren<SpriteRenderer>();
    }

    void OnEnable()
    {
        //보여질때 좌표를 수정하자.

    }

    //*********
    //virtual
    //*********
    virtual public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        SetVisible(true);

        m_Board = board;
        m_PanelType = type;
        Defence = def;
        Value = val;

        PanelDestoryTime = 0.25f;
    }

    public void SetVisible(bool visible)
    {
        if (m_SpriteRender != null) //EmptyPanel처럼 null인게 있네..
            m_SpriteRender.gameObject.SetActive(visible);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    virtual public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {

    }

    //게임중에 생성되는 보드
    virtual public void Create()
    {

    }

    virtual public void MissionApply()
    { }

    virtual public void SetSprite(int def)
    {
        if (def > 0 && List_Sprite.Count >= def)
            m_SpriteRender.sprite = List_Sprite[def - 1];
    }

    //추가 액션
    //평소상태나 어떤 상태에서 특별한 액션이 필요할때.
    virtual public void AddAction()
    {

    }

    //**********
    //Fuction
    //**********

    #region 터치 관리 함수
    void OnMouseDown()
    {
        //클릭
        //Debug.Log("터치 클릭 : " + m_Board.X.ToString() + ", " + m_Board.X.ToString());

        if (MatchManager.Instance.m_StepType != StepType.Wait)
            return;

        switch (MatchManager.Instance.m_TouchState)
        {
            case TouchState.Switching:
                {
                   
                }
                break;
            case TouchState.CashItemUse:
                {
                    UI_ThreeMatch.Instance.CashItemUse(m_Board);
                }
                break;
            case TouchState.JellyMonDrop:
                {

                }
                break;
            default:
                break;
        }

    }
    //void OnMouseEnter()
    //{
    //    //GUIElement 또는 Collider에 마우스가 들어가면 1회 호출
    //}
    //void OnMouseUp()
    //{
    //    //버튼을 때면 호출
    //}
    //void OnMoseUpAsButton()
    //{
    //    //같은 GUIElement 또는 Collider 에서 버튼을 때면 호출
    //}

    //void OnMouseDrag()
    //{
    //    //드래그
    //} 
    //void OnMouseExit()
    //{
    //    //더 이상 어떤 GUIElement 또는 Collider에도 마우스가 올라가 있지 않게 되면 호출
    //}
    //void OnMouseOver()
    //{
    //    //GUIElement 또는 Collider에 마우스가 올라가 있으면 매 프레임마다 호출
    //}
    #endregion
}
