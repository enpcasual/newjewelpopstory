﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CakePanel : Panel {

    public CakePanel CakePanel_A = null; //케이크의 갯수를 관리할 메인패널
    int CakeBrustCnt = 0;

    //케이크가 폭발할때 다른 케이크가 영향받지 않도록!!
    public static bool IsCakeAbility = false;

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);

        CakeBrustCnt = 0;
        IsCakeAbility = false;

        switch (type)
        {
            case PanelType.Cake_D:
                CakePanel_A = (CakePanel)m_Board[SQR_DIR.TOPLEFT].GetPanel(PanelType.Cake_A);
                break;
            case PanelType.Cake_C:
                CakePanel_A = (CakePanel)m_Board[SQR_DIR.TOP].GetPanel(PanelType.Cake_A);
                break;
            case PanelType.Cake_B:
                CakePanel_A = (CakePanel)m_Board[SQR_DIR.LEFT].GetPanel(PanelType.Cake_A);
                break;
            case PanelType.Cake_A:
                CakePanel_A = this;
                break;
        }
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        if(IsCakeAbility && arroundRoot != null)
        {
            //케이크가 여러개일때 하나가 터져서 효과가 발동될때 다른케이크는 영향을 받지 않는다.
            Complete(false, this);
            return;
        }

        //base.Brust(isAroundBrust, Complete);

        Defence--;
        SetSprite(Defence);

        //이펙트
        EffectManager.Instance.CakeEffect(m_Board.transform.position, 0);

        //사운드
        SoundManager.Instance.PlayEffect("defense_destroy");

        Complete(true, this);
        
        //1이면 -1로 바꿔서 더이상 터지지 않는다..
        if (Defence == 1) Defence = -1;

        if(CakePanel_A.CakeBrustCount())
        {
            //무조껀 TopLeft보드로 호출해서 이펙트 좌표 쉽게 잡자.
            AbilityManager.Instance.Ability_CakePanel(CakePanel_A.m_Board, () =>
            {
                CakePanel_A.m_Board[SQR_DIR.RIGHT].PanelDestroy(PanelType.Cake_B);
                CakePanel_A.m_Board[SQR_DIR.RIGHT].m_IsCakeBoard = false;

                CakePanel_A.m_Board[SQR_DIR.BOTTOM].PanelDestroy(PanelType.Cake_C);
                CakePanel_A.m_Board[SQR_DIR.BOTTOM].m_IsCakeBoard = false;

                CakePanel_A.m_Board[SQR_DIR.BOTTOMRIGHT].PanelDestroy(PanelType.Cake_D);
                CakePanel_A.m_Board[SQR_DIR.BOTTOMRIGHT].m_IsCakeBoard = false;

                CakePanel_A.m_Board.PanelDestroy(PanelType.Cake_A);
                CakePanel_A.m_Board.m_IsCakeBoard = false;
            });
        }
    }

    public bool CakeBrustCount()
    {
        CakeBrustCnt++;

        if (CakeBrustCnt >= 8)
            return true;

        return false;
    }

}
