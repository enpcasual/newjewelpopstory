﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class S_TreePanel : Panel {

    override public void Init(Board board, PanelType type, int def, int val, string addInfo)
    {
        base.Init(board, type, def, val, null);
        SetSprite(Defence);
    }

    //1.터졌을때 보드 처리 2.이펙트 3.기타작업
    override public void Brust(Board arroundRoot, System.Action<bool, Panel> Complete)
    {
        //base.Brust(isAroundBrust, Complete);

        Defence--;
        SetSprite(Defence);      

        //이펙트
        EffectManager.Instance.S_TreeEffect(m_Board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("defense_destroy");

        //터질때 아이템 생성
        if (Defence == 0)
            S_TreeItemCreate();

        Complete(true, this);
    }

    override public void MissionApply()
    {
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.S_Tree);
    }

    public void S_TreeItemCreate()
    {
        //현재 스테이지의 세팅타입은 스테이지마다 들고 있고
        //세팅타입에 따른 확률 정보는 ItemManager가 로드해서 들고 있다.
        int[] minfo = ItemManager.Instance.m_S_TreeData.List_Prob[(int)MatchManager.Instance.m_CSD.S_Tree_SettingType];

        int random = 0;
        S_TreeItemType itemtype = S_TreeItemType.Normal;
        int _sum = 0;

        random = Random.Range(0, ItemManager.Instance.m_S_TreeData.total[(int)MatchManager.Instance.m_CSD.S_Tree_SettingType]);

        for (int i = 0; i < (int)S_TreeItemType.Last; i++)
        {
            _sum += minfo[i];
            if (random < _sum)
            {
                itemtype = (S_TreeItemType)i;
                break;
            }
        }

        switch (itemtype)
        {
            case S_TreeItemType.Normal:
                m_Board.GenItem(ItemType.Normal, ColorType.Rnd);
                break;
            case S_TreeItemType.Line_X:
                m_Board.GenItem(ItemType.Line_X, ColorType.Rnd);
                break;
            case S_TreeItemType.Line_Y:
                m_Board.GenItem(ItemType.Line_Y, ColorType.Rnd);
                break;
            case S_TreeItemType.Line_C:
                m_Board.GenItem(ItemType.Line_C, ColorType.Rnd);
                break;
            case S_TreeItemType.Bomb:
                m_Board.GenItem(ItemType.Bomb, ColorType.Rnd);
                break;
            case S_TreeItemType.Rainbow:
                m_Board.GenItem(ItemType.Rainbow, ColorType.None);
                break;
            case S_TreeItemType.Donut:
                m_Board.GenItem(ItemType.Donut, ColorType.None);
                break;
            case S_TreeItemType.Spiral:
                m_Board.GenItem(ItemType.Spiral, ColorType.None);
                break;
            case S_TreeItemType.JellyBear:
                m_Board.GenItem(ItemType.JellyBear, ColorType.Rnd);
                break;
            case S_TreeItemType.TimeBomb:
                m_Board.GenItem(ItemType.TimeBomb, ColorType.Rnd);
                break;
            case S_TreeItemType.Mystery:
                m_Board.GenItem(ItemType.Mystery, ColorType.Rnd);
                break;
            case S_TreeItemType.Chameleon:
                m_Board.GenItem(ItemType.Chameleon, ColorType.Rnd);
                break;
            case S_TreeItemType.JellyMon:
                m_Board.GenItem(ItemType.JellyMon, ColorType.Rnd);
                break;
            case S_TreeItemType.Ghost:
                m_Board.GenItem(ItemType.Ghost, ColorType.None);
                break;
            case S_TreeItemType.Key:
                m_Board.GenItem(ItemType.Key, ColorType.Rnd);
                break;
            case S_TreeItemType.Last:
                break;
            default:
                break;
        }
    }
}
