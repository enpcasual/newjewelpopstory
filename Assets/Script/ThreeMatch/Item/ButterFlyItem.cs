﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class ButterFlyItem : Item
{
    public List<Sprite> List_IdleAnimSprite_1;
    public List<Sprite> List_IdleAnimSprite_2;
    public List<Sprite> List_IdleAnimSprite_3;
    public List<Sprite> List_IdleAnimSprite_4;
    public List<Sprite> List_IdleAnimSprite_5;
    public List<Sprite> List_IdleAnimSprite_6;

    public List<Sprite> List_FlyAnimSprite_1;
    public List<Sprite> List_FlyAnimSprite_2;
    public List<Sprite> List_FlyAnimSprite_3;
    public List<Sprite> List_FlyAnimSprite_4;
    public List<Sprite> List_FlyAnimSprite_5;
    public List<Sprite> List_FlyAnimSprite_6;

    static List<List<Sprite>> List_IdleFrameInfo = new List<List<Sprite>>();
    static List<List<Sprite>> List_FlyFrameInfo = new List<List<Sprite>>();
    UI2DSpriteAnimation SpriteAnim;

    public override void Awake()
    {
        base.Awake();

        if (List_IdleFrameInfo.Count == 0)
        {
            List_IdleFrameInfo.Add(List_IdleAnimSprite_1);
            List_IdleFrameInfo.Add(List_IdleAnimSprite_2);
            List_IdleFrameInfo.Add(List_IdleAnimSprite_3);
            List_IdleFrameInfo.Add(List_IdleAnimSprite_4);
            List_IdleFrameInfo.Add(List_IdleAnimSprite_5);
            List_IdleFrameInfo.Add(List_IdleAnimSprite_6);
        }

        if (List_FlyFrameInfo.Count == 0)
        {
            List_FlyFrameInfo.Add(List_FlyAnimSprite_1);
            List_FlyFrameInfo.Add(List_FlyAnimSprite_2);
            List_FlyFrameInfo.Add(List_FlyAnimSprite_3);
            List_FlyFrameInfo.Add(List_FlyAnimSprite_4);
            List_FlyFrameInfo.Add(List_FlyAnimSprite_5);
            List_FlyFrameInfo.Add(List_FlyAnimSprite_6);
        }

        SpriteAnim = GetComponentInChildren<UI2DSpriteAnimation>();
    }

    public override void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        base.Init(board, type, color, val);

        m_SpriteRender.transform.localPosition = Vector3.zero;
        m_SpriteRender.transform.eulerAngles = Vector3.zero;

        //혹시나 다시 한번 스톱.
        Animation_Stop();

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(Animation_Idle());
        }
    }

    //void OnEnable()
    //{
    //    m_Count++;
    //}

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));

      
    }
    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
        //EffectManager.Instance.BrustEffet_NotPiece(transform.position, m_Color);
        m_AbilityMgr.Ability_Butterfly(m_Board, m_Color, ItemType.None);

        Complete();
        yield return null;
    }
    #endregion

    #region Combine Brust 
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, Action Complete)
    {
        switch (combinetype)
        {
            //CheckCombine()에도 같이 추가 해줘야 한다!!
            case ItemType.Butterfly:
                {
                    m_AbilityMgr.Ability_ButterflyButterfly(m_Board, m_Color);
                }
                break;
            default:
                break;
        }

        //완료
        Complete();
        yield return null;
    }

    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Butterfly:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }

    #endregion

    //public override void MissionApply()
    //{
    //    MissionManager.Instance.MissionApply(gameObject, MissionType.Bear, MissionKind.Bear);
    //}

    //아이템 변경없이 색만 바뀔때가 있다. 즉, init을 다시 타지 않기 때문에 애니메이션이미지를 바꿔줘야한다.
    //ex)처음 세팅할때 매칭되지 않도록 SetColorRandomOther을 사용해 색을 바꿔준다.

    public override void SetSprite(int index)
    {
        base.SetSprite(index);
        SpriteAnim.frames = List_IdleFrameInfo[(int)m_Color - 1].ToArray();
    }

    public IEnumerator Animation_Idle()
    {
        if (m_Color == ColorType.None)
        {
            Debug.LogError("ButterFlySprite Color is NONE");

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPaused = true;
#endif
            yield break;
        }

        SpriteAnim.frames = List_IdleFrameInfo[(int)m_Color - 1].ToArray();
        SpriteAnim.framesPerSecond = 20;
        SpriteAnim.loop = false;

        while (true)
        {
            if (m_Color != ColorType.None)
            {
                SpriteAnim.ResetToBeginning();
                SpriteAnim.Play();
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(5f, 20f));
        }
        //SoundManager.Instance.PlayEffect("jellymon_eat");
    }

    public void Animation_Fly()
    {
        //Idle코루틴 스톱
        Animation_Stop();

        SpriteAnim.frames = List_FlyFrameInfo[(int)m_Color - 1].ToArray();
        SpriteAnim.framesPerSecond = 20;
        SpriteAnim.loop = true;
        SpriteAnim.ResetToBeginning();      
        SpriteAnim.Play();

        //SoundManager.Instance.PlayEffect("jellymon_eat");
    }

    public void Animation_Stop()
    {
        SpriteAnim.Pause();

        StopAllCoroutines();
    }
}
