﻿using UnityEngine;
using System;
using System.Collections;
using DG.Tweening;

public class RainbowItem : Item
{
    //public SpriteRenderer Aura_Foggy;
    public ParticleSystem Aura_Light;

    //const float rotate_speed = 150f;
    //void Update()
    //{
    //    m_SpriteRender.transform.Rotate(Vector3.back * -rotate_speed * Time.deltaTime);
    //}


    //public bool IsAccel = false;
    //const int Accel = 1;
    //const int AccelMax = 30;
    ////const float AccelTime= 0.1f; //적을수록 금방 빨라짐.

    //int _accelvalue;
    //float _acceltimevalue;

    //void Update()
    //{
    //    if (IsAccel)
    //    {
    //        if (AccelMax >= _accelvalue)
    //        {
    //            _accelvalue += Accel;
    //            //_acceltimevalue += Time.deltaTime;

    //           // if (AccelTime >= _acceltimevalue)
    //            {
    //                Aura_Light.transform.eulerAngles = Aura_Light.transform.eulerAngles + new Vector3(0, 0, _accelvalue);
    //                //Aura_Light.transform.Rotate(new Vector3(0, 0, _accelvalue));
    //            }
    //        }
    //    }
    //}

    public override void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        base.Init(board, type, color, val, copy);

        //IsAccel = false;
        //_accelvalue = 0;
        //_acceltimevalue = 0;

        transform.transform.localScale = Vector3.one;

        m_SpriteRender.transform.DOKill();
        m_SpriteRender.transform.eulerAngles = Vector3.zero;

        ParticleSystem.RotationOverLifetimeModule rol = Aura_Light.rotationOverLifetime;
        rol.z = new ParticleSystem.MinMaxCurve(40f/360f);

        Aura_Light.gameObject.SetActive(true);
        Aura_Light.transform.DOKill();      
        Aura_Light.transform.localScale = Vector3.one;
        Aura_Light.transform.localPosition = new Vector3(0, 0, 0.1f);
    }

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        m_Board.m_DropAnim = true;
        Complete += () =>
        {
            m_Board.m_DropAnim = false;
        };

        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }
    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_Rainbow(m_Board, Complete);

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(1);

        //완료
        //Complete();
        yield return null;
    }
    #endregion

    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, Action Complete)
    {
        switch (combinetype)
        {
            case ItemType.Normal:
            case ItemType.JellyBear:
            case ItemType.TimeBomb:
            case ItemType.Mystery:
            case ItemType.Chameleon:
            case ItemType.JellyMon:
            case ItemType.Key:
                {
                    m_AbilityMgr.Ability_RainbowNormal(m_Board, Complete);
                    //yield return new WaitForSeconds(1f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!

                    //완료
                    //Complete();
                }
                break;
            case ItemType.Butterfly:
                {
                    m_AbilityMgr.Ability_RainbowButterfly(m_Board);
                    yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!

                    //완료
                    Complete();
                }
                break;
            case ItemType.Line_X:
            case ItemType.Line_Y:
                {
                    m_AbilityMgr.Ability_RainbowLine(m_Board);
                    yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!

                    //완료
                    Complete();
                }
                break;
            case ItemType.Line_C:
                {
                    m_AbilityMgr.Ability_RainbowCross(m_Board);
                    yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!

                    //완료
                    Complete();
                }
                break;
            case ItemType.Bomb:
                {
                    m_AbilityMgr.Ability_RainbowBomb(m_Board);
                    yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!

                    //완료
                    Complete();
                }
                break;    
            case ItemType.Rainbow:
                {
                    m_AbilityMgr.Ability_RainbowRainbow(m_Board, ()=>
                    {
                        Complete();
                    });
                }
                break;
            default:
                {
                    Complete();
                }
                break;
        }

      
    }

    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Normal:
            case ItemType.Butterfly:
            case ItemType.JellyBear:
            case ItemType.TimeBomb:
            case ItemType.Line_X:
            case ItemType.Line_Y:
            case ItemType.Bomb:
            case ItemType.Line_C:
            case ItemType.Rainbow:
            case ItemType.Mystery:
            case ItemType.Chameleon:
            case ItemType.JellyMon:
            case ItemType.Key:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }
    #endregion

    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Rainbow);
        switch (m_Conbinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Rainbow_Line);
                break;
            case ItemType.Line_C:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Rainbow_Cross);
                break;
            case ItemType.Bomb:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Rainbow_Bomb);
                break;
            case ItemType.Rainbow:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Rainbow_Rainbow);
                break;
        }
    }
}
