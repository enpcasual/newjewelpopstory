﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public interface IRainbowBurst
{

    //죄송합니다. 마땅히 방법이 생각이 안났어요
    bool isRainbowBurst { get; set; }

    void RainbowBurstEffect();
}

abstract public class Item : MonoBehaviour
{
    public const float SerialBrustDelay = 0.05f;

    //*******************
    //Inspector Setting
    //********************
    public List<Sprite> List_KindSprite;
    public static event Action<Item> OnClickItem = null;

    public int CreateScore;
    public int DestoryScore; //이값은 사용하지말고 함수 사용할것! 인스펙터기재용

    public bool Match = true;
    public bool Switch = true;
    public bool Drop = true;
    public bool m_Brust = true;
    public bool ArountBrust = false;        //주변매치로 인해 같이 터지는가?
    public bool HaveNextItem = false;       //터지면 다른 아이템으로 변경되는가?


    //*********
    //Variable
    //*********
    [HideInInspector]
    public MatchManager m_MatchMgr;
    [HideInInspector]
    public UI_ThreeMatch m_UIMgr;
    [HideInInspector]
    public EffectManager m_EffectMgr;
    [HideInInspector]
    public AbilityManager m_AbilityMgr;

    [HideInInspector]
    public SpriteRenderer m_SpriteRender;


    [HideInInspector]
    public Board m_Board;
    public ItemType m_ItemType;
    public ColorType m_Color;
    [HideInInspector]
    public int m_Value = 0;                         //각 아이템마다 추가로 사용하는 값.                                                             
    [HideInInspector]
    public ItemType m_Conbinetype = ItemType.None;  //콤바인했을때 미션 처리용.

    public ItemDrop m_ItemDrop = new ItemDrop();

    [HideInInspector]
    public bool m_BrustEffect = true;
    [HideInInspector]
    public bool m_BrustScore = true;
    [HideInInspector]
    public bool m_BrustArround = true; //스위칭일때는 안터진다.
    [HideInInspector]
    public bool m_BrustSound = true;
    [HideInInspector]
    public bool m_CreateAnim = false;
    [HideInInspector]
    public int m_WarpPassCount = 0;  //포탈을 통과했을때.(무한포탈 처리용)

    [HideInInspector]
    public float ItemDestoryTime = 0.25f;  //아이템 삭제 대기 시간 == 위에아이템이 바로 떨어지지 않도록 대기하는시간

    [HideInInspector]
    public bool m_MissionApplied = false;

    virtual public int GetDestoryScore(bool combo)
    {
        return DestoryScore;
    }

    //**************
    //MonoBehaviour
    //**************
    virtual public void Awake()
    {
        m_SpriteRender = GetComponentInChildren<SpriteRenderer>();
    }

    //*********
    //virtual
    //*********
    virtual public void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        //Start보다 init를 먼저 타므로 여기에서 세팅.
        m_MatchMgr = MatchManager.Instance;
        m_UIMgr = UI_ThreeMatch.Instance;
        m_EffectMgr = EffectManager.Instance;
        m_AbilityMgr = AbilityManager.Instance;


        SetVisible(true);

        m_Board = board;
        m_ItemType = type;
        m_Value = val;
        m_Conbinetype = ItemType.None;

        m_BrustEffect = true;
        m_BrustScore = true;
        m_BrustSound = true;
        m_BrustArround = true;
        m_MissionApplied = false;
        m_WarpPassCount = 0;

        ItemDestoryTime = 0.25f;

        m_ItemDrop.Init();

        transform.localScale = Vector3.one;
        m_SpriteRender.transform.localPosition = Vector3.zero; //폭탄으로 흔드는중에 사라진놈들 초기화.

        StopAllCoroutines();

        //컬러 세팅
        if (color == ColorType.Rnd)
            SetColorRandom();
        else
            SetColor(color);
    }

    virtual public void Brust(Action Complete)
    {
    }

    virtual public void Take(Action Complete)
    {

    }

    virtual public bool CheckCombine(ItemType combinetype) { return false; }
    virtual public void CombineBrust(ItemType combinetype, Action Complete)
    {
        m_Conbinetype = combinetype;
    }

    virtual public void MissionApply()
    { m_MissionApplied = true; }
    virtual public void Destroy()
    {
        ObjectPool.Instance.Restore(gameObject);
    }



    //**********
    //Fuction
    //**********
    public void SetBrustSound(bool snd)
    {
        //터지는 소리가 연속으로 터질때만..
        m_BrustSound = snd;
    }

    public void SetVisible(bool visible)
    {
        m_SpriteRender.gameObject.SetActive(visible);
    }

    virtual public void SetSprite(int index)
    {
        if (List_KindSprite.Count > index)
            m_SpriteRender.sprite = List_KindSprite[index];
        else
            Debug.LogError("이미지 갯수를 넘어가는 세팅 : " + m_ItemType);
    }

    public void SetColor(ColorType color)
    {
        m_Color = color; //None타입이 오면 이미지는 없더라도 타입은 저장하자.

        if (m_Color == ColorType.None)
            return;

        SetSprite((int)m_Color - 1);
    }

    public ColorType GetColorRandom(bool includecrazycolor)
    {
        //확률컬러 포함 && 확률컬러 있음
        if (m_MatchMgr.m_CrazyColorProb > 0)
        {
            //확률을 포함할꺼고 확률에 당첨이 됐으면 전체 랜덤
            if (includecrazycolor && UnityEngine.Random.Range(0, 10) < m_MatchMgr.m_CrazyColorProb)
                return m_MatchMgr.m_AppearColor[UnityEngine.Random.Range(0, m_MatchMgr.m_AppearColor.Count)];
            else //컬러를 포함하고 싶지 않거나 당첨이 안됐으면 빼고 구함
                return m_MatchMgr.m_AppearColor[UnityEngine.Random.Range(0, m_MatchMgr.m_AppearColor.Count - 1)];
        }
        else//확률컬러가 없으면 어떤상황이든 그냥 랜덤으로 구하면된다.
        {
            return m_MatchMgr.m_AppearColor[UnityEngine.Random.Range(0, m_MatchMgr.m_AppearColor.Count)];
        }
    }

    public ColorType GetColorRandomOther(bool includecrazycolor)
    {
        ColorType _tempColor = m_Color;
        while(m_Color == _tempColor)
        {
            _tempColor = GetColorRandom(includecrazycolor);
        }
        return _tempColor;
    }

   
    public void SetColorRandom()
    {
        //Crazy확률이 적용되는 컬러는 마지막 인덱스에 있다.
        //확률값이 0보다 크고 확률에 걸리지 않으면 Crazy컬러는 나오지 않는다.
        SetColor(GetColorRandom(true));
    }

    public void SetColorRandomOther()
    {
        //Crazy확률이 적용되는 컬러는 마지막 인덱스에 있다.
        //확률값이 0보다 크고 확률에 걸리지 않으면 Crazy컬러는 나오지 않는다.
        SetColor(GetColorRandomOther(true));
    }


    #region 터치 관리 함수
    //*********
    //Touch
    //*********
    static protected bool SwitchingTouch = false;
    static public bool SwitchStart = false;
    static public Item Swap_A;
    static public Item Swap_B;
    void OnMouseDown()
    {
        if (m_MatchMgr.m_MatchState != MatchState.Playing)
            return;

        if (m_MatchMgr.m_StepType != StepType.Wait)
            return;

        if (OnClickItem != null)
            OnClickItem.Invoke(this);

        switch (m_MatchMgr.m_TouchState)
        {
            case TouchState.Switching:
                {
                    Swap_A = this;
                    SwitchStart = true;
                    SwitchingTouch = false;
                }
                break;
            case TouchState.CashItemUse:
                {
                    m_UIMgr.CashItemUse(m_Board);
                }
                break;
            case TouchState.JellyMonDrop:
                {
                    Swap_A = this;
                    SwitchingTouch = false;
                }
                break;
            default:
                break;
        }

        //클릭
        //Debug.Log("터치 클릭 : " + m_Board.X.ToString() + ", " + m_Board.X.ToString());


        //이펙트 
        //ItemEffect effect = m_EffectMgr.GetItemEffect();
        //effect.TouchEffect(Swap_A.gameObject);

    }
    void OnMouseEnter()
    {
        if (m_MatchMgr.m_MatchState != MatchState.Playing)
            return;

        if (m_MatchMgr.m_StepType != StepType.Wait)
            return;

        switch (m_MatchMgr.m_TouchState)
        {
            case TouchState.Switching:
                {
                    if (SwitchStart && Swap_A != this)
                    {
                        if (Swap_A.CheackNeighbor(this))
                        {
                            Swap_B = this;
                            SwitchStart = false;
                            SwitchingTouch = true;
                            m_MatchMgr.Switching(Swap_A, Swap_B);
                        }
                    }
                }
                break;
            case TouchState.CashItemUse:
                {
                  
                }
                break;
            case TouchState.JellyMonDrop:
                {
                    if (Swap_A != this)
                    {
                        Swap_B = this;
                        SwitchingTouch = true;
                    }
                }
                break;
            default:
                break;
        }

        //GUIElement 또는 Collider에 마우스가 들어가면 1회 호출
        //Debug.Log("터치 진입 1회 : " + m_Board.X.ToString() + ", " + m_Board.X.ToString());   

    }
    virtual public void OnMouseUp()
    {
        if (m_MatchMgr.m_MatchState != MatchState.Playing)
            return;

        if (m_MatchMgr.m_StepType != StepType.Wait)
            return;

        switch (m_MatchMgr.m_TouchState)
        {
            case TouchState.Switching:
                {
                    //만약 Switching을 못하면 초기화
                    //블럭을 벗어나도 OnMouseUP은 무조껀 탄다.
                    SwitchStart = false;
                }
                break;
            case TouchState.CashItemUse:
                {

                }
                break;
            case TouchState.JellyMonDrop:
                {
                    //1.처음 배부른 JellyMon을 클릭하면 오버라이드 된 OnMouseUp()를 탄다.
                    //2.그다음 다른 모든 아이템들중 하나를 누르면 아래 if문을 탄다.
                    JellyMon.JellyMonDrop(m_Board);
                }
                break;
            default:
                break;
        }      

        //버튼을 때면 호출
        //Debug.Log("터치 해제 : " + m_Board.X.ToString() + ", " + m_Board.X.ToString()); 

    }

    //이거 안타는데 일단 사용하지 않는거 같으니 주석처리
    //void OnMoseUpAsButton()
    //{    
    //    if (m_MatchMgr.m_MatchState != MatchState.Playing)
    //        return;

    //    if (m_MatchMgr.m_StepType != StepType.Wait)
    //        return;
    //    //같은 GUIElement 또는 Collider 에서 버튼을 때면 호출
    //    //Debug.Log("터치 해제 Same : " + m_Board.X.ToString() + ", " + m_Board.X.ToString());

    //}
    //void OnMouseDrag()
    //{
    //    //드래그
    //} 
    //void OnMouseExit()
    //{
    //    //더 이상 어떤 GUIElement 또는 Collider에도 마우스가 올라가 있지 않게 되면 호출
    //}
    //void OnMouseOver()
    //{
    //    //GUIElement 또는 Collider에 마우스가 올라가 있으면 매 프레임마다 호출
    //}
    #endregion

    protected virtual void OnDisable()
    {
        StopDropAnimCheck();
    }

    #region 아이템드랍애니메이션
    public bool CheackNeighbor(Item _item)
    {
        if (m_Board[SQR_DIR.TOP] != null)
            if (m_Board[SQR_DIR.TOP].m_Item == _item)
                return true;

        if (m_Board[SQR_DIR.BOTTOM] != null)
            if (m_Board[SQR_DIR.BOTTOM].m_Item == _item)
                return true;

        if (m_Board[SQR_DIR.LEFT] != null)
            if (m_Board[SQR_DIR.LEFT].m_Item == _item)
                return true;

        if (m_Board[SQR_DIR.RIGHT] != null)
            if (m_Board[SQR_DIR.RIGHT].m_Item == _item)
                return true;

        return false;
    }

    public IEnumerator DropAnim(Vector3 des, bool Create, bool side, Board bd, System.Action onComplete, DROP_DIR drop_Dir)
    {
        while (true)
        {
            if (m_CreateAnim == false)
                break;
            yield return null;
        }

        //사이드만 연속 체크 한다.
        if (side)
            StartDropAnimCheck();
        else
            StopDropAnimCheck();

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(m_ItemDrop.Co_ItemDrop(this, des, Create, side, bd, onComplete, drop_Dir));
        }
        else
        {
            Debug.Log(transform.position);
        }
        //StartCoroutine(m_ItemDrop.Co_ItemDrop(this, des, Create, side, bd, onComplete));
    }

    //아이템이 연속으로 이동중인지 아닌지를 판단하는 부분이 필요하다.
    //LastDropCheck함수로 체크하는건 대각선이나 충돌느낌을 주는부분에서 부족하다.
    bool IsDropAnimCheck = false;
    int permissibleFrame = 2;

    bool StartDropAnimCheck()
    {
        m_ItemDrop.consecutively++;

        permissibleFrame = 2;
        if (IsDropAnimCheck)
        {       
            return false;
        }
        else
        {
            StartCoroutine(DropAnimCheck());
            return true;
        }
    }

    IEnumerator DropAnimCheck()
    {
        IsDropAnimCheck = true;

        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (m_Board.m_DropAnim == false)
            {
                permissibleFrame--;
                if (permissibleFrame < 0)
                    break;
            }
        }

        IsDropAnimCheck = false;
        m_ItemDrop.consecutively = 0;
    }

    public void StopDropAnimCheck()
    {
        if(IsDropAnimCheck)
        {
            StopAllCoroutines();
            IsDropAnimCheck = false;
            m_ItemDrop.consecutively = 0;
        }  
    }

    public void ListDropAnima(DROP_DIR drop_Dir)
    {
        Sequence seq = DOTween.Sequence();
        if (drop_Dir == DROP_DIR.U || drop_Dir == DROP_DIR.D)
        {
            seq.Insert(0f, transform.DOScale(new Vector3(1.1f, 0.8f), 0.1f).SetEase(Ease.OutCubic));
            seq.Insert(0.1f, transform.DOScale(new Vector3(0.9f, 1.1f), 0.1f).SetEase(Ease.OutCubic));
            seq.Insert(0.2f, transform.DOScale(new Vector3(1.0f, 1.0f), 0.1f).SetEase(Ease.OutCubic));
        }
        else
        {
            seq.Insert(0f, transform.DOScale(new Vector3(0.8f, 1.1f), 0.1f).SetEase(Ease.OutCubic));
            seq.Insert(0.1f, transform.DOScale(new Vector3(1.1f, 0.9f), 0.1f).SetEase(Ease.OutCubic));
            seq.Insert(0.2f, transform.DOScale(new Vector3(1.0f, 1.0f), 0.1f).SetEase(Ease.OutCubic));
        }
    }


    #endregion

}
public class ItemDrop
{
    const float C_StartSpeed = 8.8f; //생성되는 아이템의 스피드
    const float E_StartSpeed = 0.5f; //존재하는 아이템의 스피드

    public float C_Speed = 0f;
    public float E_Speed = 0f;
    public float Accel = 0f;

    //연속이동횟수.(대각선가속도 사용)
    public int consecutively = 0;

    //***************************************************************************
    //1. 생성아이템과 기존아이템은 드랍속도가 다르다. CreateDrop
    //2. 기존아이템드랍일 경우 첫번째 블럭과 그이후블럭의 이동 가속도가 다르다 FirstBoardDrop
    //****************************************************************************
    bool CreateDrop = false;
    bool FirstBoardDrop = true;

    public void Copy(ItemDrop ida)
    {
        C_Speed = ida.C_Speed;
        E_Speed = ida.E_Speed;
        Accel = ida.Accel;

        consecutively = ida.consecutively;

        CreateDrop = ida.CreateDrop;
        FirstBoardDrop = ida.FirstBoardDrop;
    }

    public void Init()
    {
        //완전 초기화
        C_Speed = C_StartSpeed;
        E_Speed = E_StartSpeed;
        Accel = 0f;
        CreateDrop = false;
        FirstBoardDrop = true;
        //consecutively = 0;
    }

    public void Init_Side()
    {
        //사이드에서 직선으로 떨어질때 
        //E_Speed = E_Speed / 2;
        Accel = 0f;
        CreateDrop = false;
        FirstBoardDrop = false; //드랍처음이 아니므로 flase
    }

    public IEnumerator Co_ItemDrop(Item item, Vector3 des, bool Create, bool side, Board bd, System.Action onComplete, DROP_DIR drop_Dir)
    {
        if (Create)
            CreateDrop = true;

        if (side)
        {
            E_Speed = 2.4f + consecutively * 1f;    //연속으로 사이드로 이동할때는 초기값 증가.
        }

        Vector3 vector = (des - item.transform.position).normalized;         //이동 방향

        while (true)
        {
            if (CreateDrop)
            {
                Accel += 0.03f;
                C_Speed += Accel;

                if (C_Speed  > 20)
                    C_Speed = 20;

                item.transform.position += vector * Time.deltaTime * C_Speed;
            }
            else
            {
                //움직이기(드랍) 시작할때
                if (FirstBoardDrop) //side일때는 로직상 무조껀 true. 
                {
                    Accel += 0.08f;
                    E_Speed += Accel;
                }
                else
                {
                    E_Speed += 0.08f;
                }

                if (E_Speed > 20)
                    E_Speed = 20;

                item.transform.position += vector * Time.deltaTime * E_Speed;
            }

            float distance = Vector3.Distance(item.transform.position, des);

            if ((Arrive(drop_Dir, item, des) || bd == null && distance <= 0.2f) || distance > 10) //distance <= 0.2f)
            {
                if (distance > 10)
                    Debug.LogError("블록이 위로 이동 중이네...? " + drop_Dir);

                if (item.m_ItemType == ItemType.Rainbow)
                {
                    des.z = item.transform.position.z;
                }

                item.transform.position = des;
                break;
            }

            yield return null;
        }

        if(bd == null)
        {
            //보드가 널인경우는 워프에서 In를 타고 Destory되는 놈.
            Init();

            if (onComplete != null) onComplete();   //애니메이션 종료.

            yield break;
        }

        if (bd.CheckLastDrop())
        {
            //사운드
            SoundManager.Instance.PlayEffect("drop", false, 0.7f, pitch : 0.75f);
            var maxDropSpeed = Mathf.Max(C_Speed, E_Speed);
            //item.ListDropAnima(drop_Dir);

            Init();
            if(side)
            {
                if (onComplete != null) onComplete();   //애니메이션 종료.
            }
            else
            {
                //Sequence sq = DOTween.Sequence();
                //sq.Append(item.transform.DOMove(bd.GetBoardPos() + (bd.GetDropVector() * (0.025f * maxDropSpeed * 0.1f)), 0.05f).SetLoops(Mathf.FloorToInt(maxDropSpeed * 0.07f) + 1, LoopType.Yoyo)).OnComplete(() =>
                //{
                //    item.transform.DOMove(des, 0.05f).SetEase(Ease.InQuad);
                    if (onComplete != null) onComplete();   //애니메이션 종료.
                //});
            }
        }
        else if (side)
        {
            Init_Side();

            if (onComplete != null) onComplete();   //애니메이션 종료.
        }
        else
        {
            FirstBoardDrop = false;

            //만약 다음보드에 아이템이 있다면 
            if (CreateDrop)
            {
                List<Board> boards = bd.GravityReference();
                for (int i = 0; i < boards.Count; i++)
                {
                    if (boards[i] != null && boards[i].IsItemExist)
                    {
                        Init();
                    }
                }
            }
            if (onComplete != null) onComplete();   //애니메이션 종료.
        }
    }

    private bool Arrive(DROP_DIR drop_Dir, Item item, Vector3 des)
    {
        switch (drop_Dir)
        {
            case DROP_DIR.U:
                return item.transform.position.y <= des.y;
            case DROP_DIR.D:
                return item.transform.position.y >= des.y;
            case DROP_DIR.L:
                return item.transform.position.x >= des.x;
            case DROP_DIR.R:
                return item.transform.position.x <= des.x;
        }

        return false;
    }

    /*
      //*************************
    //Tween 사용해서 애니메이션
    //*************************
    public IEnumerator DropAnim1(bool creatdrop, bool lastdrop, System.Action onComplete)//, bool isDropFromSide, System.Action onComplete = null)
    {
        while (true)
        {
            if (CreateAnim == false)
                break;

            yield return null;
        }
    //Debugger.Assert(GO != null, "GamePiece.Drop GO is null.");

    //velocity += Time.deltaTime * GM.gravity;
    //velocity = Mathf.Min(GM.maxVelocity, velocity);

    //State = STATE.DROP;

    //DOTween.Complete(GetInstanceID());
    //transform.DOKill();

    //**********************
    //떨어지는 애니메이션
    //**********************
    Tweener tweener;
        if (creatdrop)
        {
            //이부분이 코루틴이 아닌데.. 결국 속도는 한칸(한블럭)단위로 올라간다는건데..
            //그러면 creatdrop은 top한블럭일때만이니까 의미가 없네.. 그냥 처음속도를 높여주자.
            //C_Speed += Time.deltaTime * 70f;
            //C_Speed = Mathf.Min(10f, C_Speed);
            tweener = transform.DOMove(m_Board.GetBoardPos(), C_Speed).SetSpeedBased();         
        }
        else
        {            
            E_Speed = Mathf.Min(12f, E_Speed);
            tweener = transform.DOMove(m_Board.GetBoardPos(), E_Speed).SetSpeedBased();
            E_Speed += 3; //프레임단위가 아닌데 DeltaTime이 무슨의미가 있니..     
        }

        tweener.SetEase(Ease.Linear);
        tweener.SetUpdate(UpdateType.Normal);

        //**********************
        //튕기는 애니메이션
        //**********************
        if (lastdrop)
        {
            tweener.OnComplete(() =>
            {
                //사운드
                SoundManager.Instance.PlayEffect("drop");

                C_Speed = C_StartSpeed;
                 E_Speed = E_StartSpeed;

                onComplete();   //애니메이션 종료.
                //Debug.Log("X : " + m_Board.X + ", Y : " + m_Board.Y);
                //if (MatchManager.Instance.CheckDropEnd(m_Board.X, m_Board.Y))
                //{
                //    Sequence sq = DOTween.Sequence();
                //    sq.Append(transform.DOMove(m_Board.GetItemPos() + (Vector3.up * 0.1f), 0.1f).SetLoops(2, LoopType.Yoyo)).OnComplete(() =>
                //    {
                //        m_Action = false; //모든 애니메이션 종료.
                //    });
                //}
            });
        }
        else
        {
            tweener.OnComplete(() =>
            {
                onComplete();   //애니메이션 종료.
            });
        }
        //Tween dropTween = transform.DOMove(m_Board.GetItemPos(), 1).SetSpeedBased();
        //dropTween.SetEase(Ease.Linear);
        //dropTween.SetId(GO.GetInstanceID());

        //    Vector3 scaleAmount = Vector3.zero;


        //    if (GM.useJellyBounce && (isDropFromSide == false))
        //    {
        //        float ratio = Mathf.Min(GM.sloshingRatio,
        //            (velocity - GM.startVelocity) / (GM.maxVelocity - GM.startVelocity));
        //        scaleAmount = orgScale * ratio;
        //        GO.transform.localScale = new Vector3(
        //            orgScale.x - scaleAmount.x,
        //            orgScale.y + scaleAmount.y,
        //            orgScale.z);
        //    }

        //    if (isEndOfDrop && (isDropFromSide == false))
        //    {
        //        dropTween.OnComplete(() =>
        //        {
        //            velocity = GM.startVelocity;

        //            Sequence seq = DOTween.Sequence();

        //            Vector3 bouncePos = targetPos + (Vector3.up * GM.Size * GM.bounceRatio);

        //            Tween bounceTween = GO.transform.DOMove(bouncePos, GM.bounceDuration);
        //            bounceTween.SetEase(Ease.OutQuad);
        //            bounceTween.SetLoops(2, LoopType.Yoyo);

        //            seq.Append(bounceTween);

        //            if (GM.useJellyBounce)
        //            {
        //                Vector3 targetScale = new Vector3(
        //                    orgScale.x * (1F + GM.sloshingRatio),
        //                    orgScale.y * (1F - GM.sloshingRatio),
        //                    orgScale.z);

        //                seq.Insert(0F, GO.transform.DOScale(targetScale, GM.bounceDuration)
        //                    .SetEase(Ease.OutQuad));
        //                seq.Insert(GM.bounceDuration, GO.transform.DOScale(orgScale, GM.bounceDuration)
        //                    .SetEase(Ease.InQuad));
        //            }

        //            seq.OnComplete(() =>
        //            {
        //                State = STATE.STABLE;

        //                if ((GO != null) && (Owner != null))
        //                {
        //                    GO.transform.position = Owner.Position;
        //                }

        //                if (onComplete != null) onComplete();
        //            });
        //        });
        //    }
        //    else {
        //        dropTween.OnComplete(() =>
        //        {
        //            State = STATE.STABLE;

        //            if ((GO != null) && (Owner != null))
        //            {
        //                GO.transform.position = Owner.Position;
        //            }

        //            if (onComplete != null) onComplete();
        //        });
        //    }
    }
*/
}
