﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ItemType
{
    None = -1,
    Normal = 0,
    Butterfly = 1,  //사각형으로 4개모이면 만들어지는 특수 아이템
    Line_X = 2,
    Line_Y = 3,
    Line_C = 4,     //T 5개 모으면 X 방향 라인
    Bomb = 5,       //ㄱ,ㄴ 5개 모으면 주변이 터지는 폭탄
    Rainbow = 6,    //한줄로 5개 모으면 같은색을 없애는 폭탄


    CondItem_Last,  //매칭에서 합쳐질 조건이 있는 아이템들까지만.

    Donut = 20,      //도넛
    Spiral = 21,     //스파이럴
    JellyBear = 22,  //젤리곰
    TimeBomb = 23,   //시간폭탄
    

    Mystery = 30,   //미스테리
    Chameleon = 31, //카멜레온

    JellyMon = 35, //젤리몬
    Ghost = 36,    //유령

    Key = 40,       //열쇠

    //보너스타임때 나오는 아이템
    BonusCross = 90,
    BonusBomb = 91,

    //미션관련 아이템
    Misson_Food1 = 100,
    Misson_Food2 = 101,
    Misson_Food3 = 102,
    Misson_Food4 = 103,
    Misson_Food5 = 104,
    Misson_Food6 = 105,

    //Last, //item에 번호를 매겨버리면 이건 사용 못할듯!!
}

public enum ColorType
{
    None = 0,
    RED = 1,
    YELLOW = 2,
    GREEN = 3,
    BLUE = 4,
    PURPLE = 5,
    ORANGE = 6,
    
    Rnd = 7,
}

[System.Serializable]
public class ItemObj
{
    public ItemType m_ItemType;
    public GameObject m_ItemObj;
}

//*********************
//아이템 추가 방법
//*********************
//1. enum ItemType에 타입추가
//2. 기존아이템의 스크립트, 오브젝트 복사해서 이름변경.
//   - 오브젝트에 새로만든 스크립트로 교체
//   - Sprite 이미지 교체 (컬러이미지리스트 체크)
//   - 인스펙터에 이미지 및 기본 값들 세팅.
//3. ItemManager(obj)에 리스트목록에 추가
//4. SpecailItemCondition()에 조건추가
//5. 추가한 스크립트 아이템에 맞게 수정.

public class ItemManager : MonoBehaviour {
    static public ItemManager Instance;

    MatchManager MatchMgr;

    public List<ItemObj> List_Item;

    //스파이럴 드랍조건
    int Spiral_Interval = 0;
    int Spiral_SpawnCnt = 0;
    bool Spiral_Create = false;

    //도넛 드랍조건
    int Donut_Interval = 0;
    int Donut_SpawnCnt = 0;
    bool Donut_Create = false;

    //시한폭탄 드랍조건
    int TimeBomb_Interval = 0;
    int TimeBomb_SpawnCnt = 0;
    bool TimeBomb_Create = false;

    //미스테리 드랍조건
    public MysteryData m_MysteryData = null;
    int Mystery_Interval = 0;
    int Mystery_SpawnCnt = 0;
    bool Mystery_Create = false;

    //카멜레온 드랍조건
    int Chameleon_Interval = 0;
    int Chameleon_SpawnCnt = 0;
    bool Chameleon_Create = false;

    //열쇠 드랍조건
    int Key_Interval = 0;
    int Key_SpawnCnt = 0;
    bool Key_Create = false;

    //스페셜트리 아이템생성
    public S_TreeData m_S_TreeData = null;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MatchMgr = MatchManager.Instance;
    }
    public void Init()
    {
        Spiral_Interval = 0;
        Spiral_SpawnCnt = 0;

        Donut_Interval = 0;
        Donut_SpawnCnt = 0;

        TimeBomb_Interval = 0;
        TimeBomb_SpawnCnt = 0;

        if (m_MysteryData == null)
        {
            m_MysteryData = DataManager.LoadMysteryData();
            m_MysteryData.InitTotal();
        }

        if(m_S_TreeData == null)
        {
            m_S_TreeData = DataManager.LoadSTreeData();
            m_S_TreeData.InitTotal();
        }
    }

    #region 아이템 생성
    public GameObject CreateItem(ItemType type)
    {
        GameObject ItemObj = null;
        for (int i = 0; i < List_Item.Count; i++)
        {
            if (List_Item[i].m_ItemType == type)
            {
                ItemObj = ObjectPool.Instance.GetObject(List_Item[i].m_ItemObj, MatchManager.Instance.GameField.transform);
                break;
            }
        }
        return ItemObj;
    }
    #endregion

    #region 아이템 오브젝트 풀 초기화
    public void Init_ObjectPool_Item()
    {
        //일단 일반블럭에서 문제가 발생하므로 일반 블럭만 초기화 한다.
        for (int i = 0; i < List_Item.Count; i++)
        {
            if (List_Item[i].m_ItemType == ItemType.Normal)
            {
                ObjectPool.Instance.Restore_Obj(List_Item[i].m_ItemObj);
                break;
            }
        }
    }
    #endregion

    #region 특수 아이템 드랍 조건(푸드만 미션쪽에)
    public void Spiral_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if (Spiral_Create)
        {
            Spiral_Create = false;
            Spiral_Interval = 0;
            Spiral_SpawnCnt = 0;
        }

        Spiral_Interval++;       
    }
    public void Spiral_ItemCreateApply()
    {
        //생성될때 인터벌은 초기화 SpawnCnt는 증가
        Spiral_Create = true;
        Spiral_SpawnCnt++;
    }
    public bool IsCreateSpiral(Board bd, bool istop = true)
    {
        if ((MatchMgr.m_CSD.SpiralSpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.SpiralSpawnLineY[bd.Y] == false)) && istop)
            return false;

        if (MatchMgr.m_CSD.Spiral_MaxExist <= SpiralItem.m_Count)
            return false;

        if(MatchMgr.m_CSD.Spiral_Interval > Spiral_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.Spiral_MinExist <= SpiralItem.m_Count && MatchMgr.m_CSD.Spiral_SpawnCnt <= Spiral_SpawnCnt)
            return false;


        Spiral_ItemCreateApply();

        return true;
    }

    public void Donut_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if(Donut_Create)
        {
            Donut_Create = false;
            Donut_Interval = 0;
            Donut_SpawnCnt = 0;
        }
        //무브를 사용하면 인터벌은 증가 SpawnCnt는 초기화
        Donut_Interval++;
    }
    public void Donut_ItemCreateApply()
    {
        Donut_Create = true;
        Donut_SpawnCnt++;        
    }
    public bool IsCreateDonut(Board bd)
    {
        if (MatchMgr.m_CSD.DonutSpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.DonutSpawnLineY[bd.Y] == false))
            return false;

        if (MatchMgr.m_CSD.Donut_MaxExist <= DonutItem.m_Count)
            return false;

        if (MatchMgr.m_CSD.Donut_Interval > Donut_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.Donut_MinExist <= DonutItem.m_Count && MatchMgr.m_CSD.Donut_SpawnCnt <= Donut_SpawnCnt)
            return false;


        Donut_ItemCreateApply();

        return true;
    }

    public void TimeBomb_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if (TimeBomb_Create)
        {
            TimeBomb_Create = false;
            TimeBomb_Interval = 0;
            TimeBomb_SpawnCnt = 0;
        }

        TimeBomb_Interval++;
    }
    public void TimeBomb_ItemCreateApply()
    {
        //생성될때 인터벌은 초기화 SpawnCnt는 증가
        TimeBomb_Create = true;
        TimeBomb_SpawnCnt++;
    }
    public bool IsCreateTimeBomb(Board bd, bool istop = true)
    {
        if ((MatchMgr.m_CSD.TimeBombSpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.TimeBombSpawnLineY[bd.Y] == false)) && istop) //top 드랍일때만(맨위 or empty 생성기)
            return false;

        if (MatchMgr.m_CSD.TimeBomb_MaxExist <= TimeBombItem.m_Count)
            return false;

        if (MatchMgr.m_CSD.TimeBomb_Interval > TimeBomb_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.TimeBomb_MinExist <= TimeBombItem.m_Count && MatchMgr.m_CSD.TimeBomb_SpawnCnt <= TimeBomb_SpawnCnt)
            return false;

        if (MatchMgr.m_MatchState == MatchState.BonusTime)
            return false;

        TimeBomb_ItemCreateApply();

        return true;
    }

    public void Mystery_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if (Mystery_Create)
        {
            Mystery_Create = false;
            Mystery_Interval = 0;
            Mystery_SpawnCnt = 0;
        }
        //무브를 사용하면 인터벌은 증가 SpawnCnt는 초기화
        Mystery_Interval++;
    }
    public void Mystery_ItemCreateApply()
    {
        Mystery_Create = true;
        Mystery_SpawnCnt++;
    }
    public bool IsCreateMystery(Board bd)
    {
        if (MatchMgr.m_CSD.MysterySpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.MysterySpawnLineY[bd.Y] == false))
            return false;

        if (MatchMgr.m_CSD.Mystery_MaxExist <= MysteryItem.m_Count)
            return false;

        if (MatchMgr.m_CSD.Mystery_Interval > Mystery_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.Mystery_MinExist <= MysteryItem.m_Count && MatchMgr.m_CSD.Mystery_SpawnCnt <= Mystery_SpawnCnt)
            return false;

        if (MatchMgr.m_MatchState == MatchState.BonusTime)
            return false;

        Mystery_ItemCreateApply();

        return true;
    }

    public void Chameleon_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if (Chameleon_Create)
        {
            Chameleon_Create = false;
            Chameleon_Interval = 0;
            Chameleon_SpawnCnt = 0;
        }
        //무브를 사용하면 인터벌은 증가 SpawnCnt는 초기화
        Chameleon_Interval++;
    }
    public void Chameleon_ItemCreateApply()
    {
        Chameleon_Create = true;
        Chameleon_SpawnCnt++;
    }
    public bool IsCreateChameleon(Board bd)
    {
        if (MatchMgr.m_CSD.ChameleonSpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.ChameleonSpawnLineY[bd.Y] == false))
            return false;

        if (MatchMgr.m_CSD.Chameleon_MaxExist <= ChameleonItem.m_Count)
            return false;

        if (MatchMgr.m_CSD.Chameleon_Interval > Chameleon_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.Chameleon_MinExist <= ChameleonItem.m_Count && MatchMgr.m_CSD.Chameleon_SpawnCnt <= Chameleon_SpawnCnt)
            return false;

        if (MatchMgr.m_MatchState == MatchState.BonusTime)
            return false;

        Chameleon_ItemCreateApply();

        return true;
    }

    public void Key_IntervalApply()
    {
        //인터벌은 생성중에는 초기화 하면 안된다.
        if (Key_Create)
        {
            Key_Create = false;
            Key_Interval = 0;
            Key_SpawnCnt = 0;
        }
        //무브를 사용하면 인터벌은 증가 SpawnCnt는 초기화
        Key_Interval++;
    }
    public void Key_ItemCreateApply()
    {
        Key_Create = true;
        Key_SpawnCnt++;
    }
    public bool IsCreateKey(Board bd, bool istop = true)
    {
        if ((MatchMgr.m_CSD.KeySpawnLine[bd.X] == false || (MatchMgr.m_CSD.isUseGravity && MatchMgr.m_CSD.KeySpawnLineY[bd.Y] == false)) && istop)
            return false;

        //보물상자가 없으면 열쇠 드랍 안함.
        if(Board.m_ListBottlePanel.Count <= 0)
            return false;

        if (MatchMgr.m_CSD.Key_MaxExist <= KeyItem.m_Count)
            return false;

        if (MatchMgr.m_CSD.Key_Interval > Key_Interval)
            return false;

        //최저수보다 크고 출현갯수를 넘었으면 안나온다.(최저갯수보다 적음, 출현갯수가 남음. 둘중에 하나만 충족되면 생성)
        if (MatchMgr.m_CSD.Key_MinExist <= KeyItem.m_Count && MatchMgr.m_CSD.Key_SpawnCnt <= Key_SpawnCnt)
            return false;

        if (MatchMgr.m_MatchState == MatchState.BonusTime)
            return false;

        Key_ItemCreateApply();

        return true;
    }
    #endregion

}
