﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class JellyMon : Item
{
    //인스펙터 세팅
    public List<Sprite> List_EatSprite_1;
    public List<Sprite> List_EatSprite_2;
    public List<Sprite> List_EatSprite_3;
    public List<Sprite> List_EatSprite_4;
    public List<Sprite> List_EatSprite_5;
    public List<Sprite> List_EatSprite_6;

    public List<Sprite> List_FullEatSprite;

    public List<Sprite> List_FlySprite_1;
    public List<Sprite> List_FlySprite_2;
    public List<Sprite> List_FlySprite_3;
    public List<Sprite> List_FlySprite_4;
    public List<Sprite> List_FlySprite_5;
    public List<Sprite> List_FlySprite_6;

    //Awake 세팅
    List<List<Sprite>> List_EatAnim = new List<List<Sprite>>();
    List<List<Sprite>> List_FlyAnim = new List<List<Sprite>>();
    public UI2DSpriteAnimation SpriteAnim = null;
    public ParticleSystem TouchEffect = null;
    public ParticleSystem MatchEffect = null;
    public GameObject TutorialEffect = null;

    //const
    const int MaxEatCnt = 11;

    //구현 변수
    public static JellyMon m_BrustJellyMon;   //폭발할려고 선택된 JellyMon
    public int m_EatCnt = 0;

    public bool m_IsFull
    {
        get
        {
            if (m_EatCnt >= MaxEatCnt)
                return true;
            return false;
        }
    }

    public override void Awake()
    {
        base.Awake();

        List_EatAnim.Add(List_EatSprite_1);
        List_EatAnim.Add(List_EatSprite_2);
        List_EatAnim.Add(List_EatSprite_3);
        List_EatAnim.Add(List_EatSprite_4);
        List_EatAnim.Add(List_EatSprite_5);
        List_EatAnim.Add(List_EatSprite_6);

        List_FlyAnim.Add(List_FlySprite_1);
        List_FlyAnim.Add(List_FlySprite_2);
        List_FlyAnim.Add(List_FlySprite_3);
        List_FlyAnim.Add(List_FlySprite_4);
        List_FlyAnim.Add(List_FlySprite_5);
        List_FlyAnim.Add(List_FlySprite_6);
    }

    public override void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        m_EatCnt = 0; //SetSprite에서 이 값을 사용하여 이미지를 정한다. 전판에 배가 부른놈은 여기서 초기화필요함.

        base.Init(board, type, color, val);

        m_BrustJellyMon = null;
        TouchEffect.gameObject.SetActive(false);
        TutorialEffect.SetActive(false);
    }

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);
        StartCoroutine(CoroutineBrust(Complete));
    }

    override public void SetSprite(int index)
    {
        if (m_IsFull)
            //base.SetSprite(index + 6);
            m_SpriteRender.sprite = List_FullEatSprite[index];
        else
            base.SetSprite(index);

        //매직컬러 위에 젤리몬이 있을때 간혹 실제 컬러와 젤리몬 이미지가 안맞는경우가 있다.
        //젤리몬이 Eat애님을 플레이 중인데 매직컬러가 중간에 작동해서 결국 Eat애님의 마지막이미지로 변경되는걸로 추측된다.
        //그래서 컬러를 바꿀때 애님정보도 같이 바꿔준다.
        SpriteAnim.frames = List_EatAnim[index].ToArray();
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //젤리몬은 Brust되지 않는다 m_Brust가 false
        yield return null;
    }

    bool EatAnim = true;
    public void Animation_Eat()
    {
        EatAnim = true;
        SpriteAnim.frames = List_EatAnim[(int)m_Color - 1].ToArray();
        SpriteAnim.loop = false;
        SpriteAnim.ResetToBeginning();
        SpriteAnim.Play();

        MatchEffect.Stop();
        MatchEffect.Play();

        SoundManager.Instance.PlayEffect("jellymon_eat");
    }

    public void Animation_Fly()
    {
        SpriteAnim.frames = List_FlyAnim[(int)m_Color - 1].ToArray();
        SpriteAnim.loop = true;
        SpriteAnim.ResetToBeginning();
        SpriteAnim.Play();
        //SoundManager.Instance.PlayEffect("jellymon_eat");
    }

    public void Animation_Stop()
    {
        //SpriteAnim.enabled = false;
        SpriteAnim.Pause();
    }

    public void Update()
    {
        if (EatAnim && SpriteAnim.enabled == false)
        {
            EatAnim = false;
            FullCheck();
        }
    }

    public void EatCount()
    {
        m_EatCnt++;
    }

    public void FullCheck()
    {
        if (m_EatCnt >= MaxEatCnt)
            SetFullSprite();
    }

    public void CopyJellyMon(int eatcnt)
    {
        //아이템을 생성할때 기본적인 아이템 값들은 복사가 되는데
        //젤리몬에만 해당되는 추가적인 값을 복사하고 적용한다.
        m_EatCnt = eatcnt;
        FullCheck();
    }


    //tutorial
    static public bool firstFullJellyMon = true;
    public void SetFullSprite()
    {
        //m_SpriteRender.sprite = List_FullEatSprite[(int)m_Color-1];
        SetColor(m_Color); //원래 여기에서 배부른 이미지로 바꿔줬지만 매직컬러 같은곳에서도 이미지를 바꾸는 경우가 있어서 SetSprite를 오버라이드 해놨다. 그래서 여기서는 함수한번 호출해주면 된다.
        TouchEffect.gameObject.SetActive(true);

        //튜토리얼..
        if (firstFullJellyMon && m_MatchMgr.m_StageIndex <= 150)
        {
            firstFullJellyMon = false;
            TutorialEffect.SetActive(true);
        }
    }

    IEnumerator Co_Tutorial_323_11()
    {
        /*
        while (m_MatchMgr.m_StepType != StepType.Wait)
        {
            //만약 젤리몬 튜토리얼이 나오기 전에 클리어하면 튜토리얼 종료
            if (m_MatchMgr.m_StepType == StepType.Clear || m_MatchMgr.m_StepType == StepType.Fail)
                yield break;

            yield return null;
        }
        TutorialInfo tInfo = TutorialManager.Instance.GetTutorialInfo(323, 11);
        if (tInfo != null)
        {
            tInfo.List_MaskBoard[0].coordinate = new Vector2(m_Board.X, m_Board.Y);

            TutorialManager.Instance.Tutorial_Start(323, 11, 11);
        }   
        */
        yield return null;
    }

    #region 터치 override
    public static bool JellyMon_Fly = false;
    override public void OnMouseUp()
    {
        if (m_MatchMgr.m_MatchState != MatchState.Playing)
            return;

        if (m_MatchMgr.m_StepType != StepType.Wait)
            return;

        base.OnMouseUp();

        //*****************************************************
        //여기는 젤리몬일때만 무조건 호출된다.(다른 아이템 X)
        //(젤리몬 선택한 상태에서 다시 선택할때도 호출된다는점을 기억하자)
        //*****************************************************

        if (SwitchingTouch) //스위칭같은 애매한 터치 방지..
            return;

        if (m_IsFull == false) //배부른놈이 아니면 터치할 일 없음.
            return;

        if (JellyMon_Fly) //이미 젤리몬이 날라가고 있다면
            return;

        if (m_BrustJellyMon == null)
        {
            //UI 세팅
            m_UIMgr.JellyMon_Select(true);
            //Touch 세팅
            m_MatchMgr.SetTouchState(TouchState.JellyMonDrop);
            //젤리몬 세팅
            m_BrustJellyMon = this;
        }
        else
        {
            //***************************************************
            //여기는 선택된 상태에서 젤리몬을 다시 누른거다!
            //(물런 배부르고 젤리몬이 날라가는중이 아니여야한다.)
            //JellyMonDrop()에서 터지지 않고 여기를 탄다.
            //****************************************************

            //UI 세팅
            m_UIMgr.JellyMon_Select(false);
            //Touch 세팅
            m_MatchMgr.SetTouchState(TouchState.Switching);
            //젤리몬 세팅
            m_BrustJellyMon = null;
        }
    }
    public static void JellyMonDrop(Board bd)
    {
        //**********************************************************
        //젤리몬이 선택된 상태에서 어떤 아이템을 선택하던 호출된다.
        //**********************************************************

        //1. 스위칭같은 애매한 터치 방지..
        if (SwitchingTouch)
            return;

        if (JellyMon_Fly) //이미 젤리몬이 날라가고 있다면
            return;

        //젤리몬이면 리턴
        if (bd.m_Item is JellyMon)
            return;


        //젤리몬Fly 
        JellyMon_Fly = true;
        //Fly 애니메이션
        m_BrustJellyMon.Animation_Fly();

        //이펙트 꺼줘야한다.
        m_BrustJellyMon.TouchEffect.gameObject.SetActive(false);

        //사운드
        SoundManager.Instance.PlayEffect("jellymon_jump");

        //UI 세팅_일단 먼저 해제
        UI_ThreeMatch.Instance.JellyMon_Select(false);

        //이동 애니메이션
        m_BrustJellyMon.transform.localScale = Vector3.zero;
        m_BrustJellyMon.transform.position += new Vector3(0, 0, -1f);//다른 패널들보다 위에서 움직이도록 -1뺀다.
        Vector3 pos = bd.transform.position + new Vector3(0, 0, -1f);
        EffectManager.Instance.JellyMonJumpEffect(m_BrustJellyMon.transform.position, 0.2f);
        Sequence seq = DOTween.Sequence();
        seq.Insert(0f, m_BrustJellyMon.transform.DOScale(0f, 0.1f).SetEase(Ease.OutCubic));
        seq.Insert(0.9f, m_BrustJellyMon.transform.DOScale(1f, 0.1f).SetEase(Ease.InCubic));
        seq.Insert(0f, m_BrustJellyMon.transform.DOMove(pos, 0.1f).SetEase(Ease.Linear));
        seq.OnComplete(() =>
        {
            JellyMon_Fly = false;

            //Touch 세팅
            MatchManager.Instance.SetTouchState(TouchState.Switching);

            //딸기쨈적용.
            if (m_BrustJellyMon.m_Board.m_IsJamBoard) bd.JamPanelCreate(0); //여기서 날라갈 보드에 적용시켜줘야 효과(Ability_JellyMon)에 적용된다.

            //날라가는 애니메이션 종료
            m_BrustJellyMon.Animation_Stop();

            //컬러만 먼저 구해서 바꾼다.
            //이유는 떨어지는곳의 기존 아이템이 효과발동하고 사라질때까지는 시간이 좀 걸리는데
            //그동안 젤리몬도 예전색으로 보이기 때문이다.
            ColorType _color = m_BrustJellyMon.GetColorRandom(false); //확률컬러는 빼고 랜덤.

            m_BrustJellyMon.m_EatCnt = 0;       //값을 초기화 해준다. 그래야 다시 홀쭉한 젤리몬이 된다.
            m_BrustJellyMon.SetColor(_color);

            //효과 발동!
            AbilityManager.Instance.Ability_JellyMon(bd, 1, () =>
            {
                //기존 보드에서 젤리몬 삭제             
                m_BrustJellyMon.m_Board.ItemDestroy();
                m_BrustJellyMon = null;

                //젤리몬 생성(현재보드의 아이템타입을 사용하므로 늦게 생성)
                bd.GenItem(ItemType.JellyMon, _color);
            });
        });
    }
    #endregion
}
