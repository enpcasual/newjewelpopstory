﻿using UnityEngine;
using System;
using System.Collections;

public class DonutItem : Item
{
    public static int m_Count = 0;

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        //이펙트
        EffectManager.Instance.DonutEffect(m_Board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("defense_destroy");

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(0);

        //완료
        Complete();
        yield return null;
    }

    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }

    static int Cnt = 0;
    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        Cnt++;
        //Debug.LogError("Donut : "+Cnt);
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Donut);
    }

}
