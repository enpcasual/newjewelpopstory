﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ChameleonItem : Item
{
    public static int m_Count = 0;



    public List<Sprite> List_ChangeSprite_1;
    public List<Sprite> List_ChangeSprite_2;
    public List<Sprite> List_ChangeSprite_3;
    public List<Sprite> List_ChangeSprite_4;
    public List<Sprite> List_ChangeSprite_5;
    public List<Sprite> List_ChangeSprite_6;
    public UI2DSpriteAnimation SpriteAnim;

    //Awake 세팅
    List<List<Sprite>> List_ChangeAnim = new List<List<Sprite>>();

    public override void Awake()
    {
        base.Awake();

        List_ChangeAnim.Add(List_ChangeSprite_1);
        List_ChangeAnim.Add(List_ChangeSprite_2);
        List_ChangeAnim.Add(List_ChangeSprite_3);
        List_ChangeAnim.Add(List_ChangeSprite_4);
        List_ChangeAnim.Add(List_ChangeSprite_5);
        List_ChangeAnim.Add(List_ChangeSprite_6);
    }

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //이펙트&효과&사운드
        //******************
        if (m_BrustEffect)
        {
            //******************
            //기본 Brust 이펙트
            //******************
            //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
            ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
            _effect.DefaultBrustEffect(m_Board.transform.position, m_Color);
        }

        //사운드
        if (m_BrustSound)
            SoundManager.Instance.PlayEffect("combo1");


        //완료
        Complete();
        yield return null;
    }

    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }

    public void ChangeImage()
    {
        SpriteAnim.frames = List_ChangeAnim[(int)m_Color - 1].ToArray();
        SpriteAnim.loop = false;
        SpriteAnim.ResetToBeginning();
        SpriteAnim.Play();
    }
}
