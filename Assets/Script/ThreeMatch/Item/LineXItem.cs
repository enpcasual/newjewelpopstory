﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LineXItem : Item
{
    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    public override void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_LineX(m_Board,m_Color);

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(0);

        //완료
        Complete();
        yield return null;
    }
    #endregion

    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, Complete));
    }
    public IEnumerator CoroutineCombineBrust(ItemType combinetype, Action Complete)
    {

        switch (combinetype)
        {
            //CheckCombine()에도 같이 추가 해줘야 한다!!
            case ItemType.Butterfly:
                {
                    m_AbilityMgr.Ability_LineXButterfly();
                }
                break;
            case ItemType.Line_Y:
            case ItemType.Line_X:
                {
                    m_AbilityMgr.Ability_LineLine(m_Color);
                    yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!
                }
                break;
            default:
                break;
        }

        //완료
        Complete();
        yield return null;
    }
    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Butterfly:
            case ItemType.Line_X:
            case ItemType.Line_Y:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }

    #endregion

    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Line);
        switch (m_Conbinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Line_Line);
                break;
        }

    }
}
