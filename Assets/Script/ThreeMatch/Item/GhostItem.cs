﻿using UnityEngine;
using System;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class GhostItem : Item
{
    public List<Sprite> List_IdleAnimSprite;
    public List<Sprite> List_PlayAnimSprite;

    public List<GameObject> ParticleObjs = new List<GameObject>();


    UI2DSpriteAnimation SpriteAnim;

    public override void Awake()
    {
        base.Awake();

        SpriteAnim = GetComponentInChildren<UI2DSpriteAnimation>();
    }

    public override void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        base.Init(board, type, color, val);

        for(int i = 0; i < ParticleObjs.Count; i++)
        {
            ParticleObjs[i].transform.localPosition = Vector3.zero;
        }

        InitSprite();      
    }

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }
    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_Ghost(m_Board, Complete, false);

        yield return null;
    }
    #endregion

    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        switch (combinetype)
        {
            case ItemType.Normal:
            case ItemType.Butterfly:
            case ItemType.Line_X:
            case ItemType.Line_Y:
            case ItemType.Line_C:
            case ItemType.Bomb:
            case ItemType.Rainbow:
            case ItemType.Donut:
            case ItemType.Spiral:
            case ItemType.JellyBear:
            case ItemType.TimeBomb:
            case ItemType.Mystery:
            case ItemType.Chameleon:
            case ItemType.JellyMon:
            case ItemType.Ghost:
            case ItemType.Key:
                //case ItemType.BonusCross:
                //case ItemType.BonusBomb:
                //case ItemType.Misson_Food1:
                //case ItemType.Misson_Food2:
                //case ItemType.Misson_Food3:
                //case ItemType.Misson_Food4:
                //case ItemType.Misson_Food5:
                //case ItemType.Misson_Food6:    
                m_AbilityMgr.Ability_Ghost(m_Board, Complete, true);
                break;
            default:
                break;
        }
    }

    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Normal:
            case ItemType.Butterfly:
            case ItemType.Line_X:
            case ItemType.Line_Y:
            case ItemType.Line_C:
            case ItemType.Bomb:
            case ItemType.Rainbow:
            case ItemType.Donut:
            case ItemType.Spiral:
            case ItemType.JellyBear:
            case ItemType.TimeBomb:
            case ItemType.Mystery:
            case ItemType.Chameleon:
            case ItemType.JellyMon:
            case ItemType.Ghost:
            case ItemType.Key:
                //case ItemType.BonusCross:
                //case ItemType.BonusBomb:
                //case ItemType.Misson_Food1:
                //case ItemType.Misson_Food2:
                //case ItemType.Misson_Food3:
                //case ItemType.Misson_Food4:
                //case ItemType.Misson_Food5:
                //case ItemType.Misson_Food6:    
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }
    #endregion

    #region 유령 Brust 이미지
    public void InitSprite()
    {
        //Board.cs의 GenItme()를 호출할때 Item의 SetColor()컬러를 호출한다. 
        //하지만 컬러가 None이면 아무행동도 하지 않는다.
        //컬러가 None이여도 이미지를 넣어줘야 하는거 아닌가??
        //일단 모든 아이템을 체크해서 작업하기전까지 유령은 이 함수 호출한다.

        //Brust 애니메이션 끔.
        Animation_Stop();

        //기본 이미지 세팅
        SetSprite(0);
       
        //기본 애니메이션
        //DOTween.Kill(m_SpriteRender.transform);
        //m_SpriteRender.transform.localPosition = new Vector3(0, -0.04f, 0);
        //m_SpriteRender.transform.DOLocalMoveY(0.07f, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);

        StartCoroutine(Animation_Idle());
    }

    public void SetBrustSprite()
    {
        //터질때 이미지를 바꿔줄꺼다.
        //하지만 애니메이션을 넣을수 있으므로 함수를 하나 만든다.

        SetSprite(1);

        DOTween.Kill(m_SpriteRender.transform);
    }

    public IEnumerator Animation_Idle()
    {
        SpriteAnim.frames = List_IdleAnimSprite.ToArray();
        SpriteAnim.framesPerSecond = 20;
        SpriteAnim.loop = false;

        while (true)
        {
            SpriteAnim.ResetToBeginning();
            SpriteAnim.Play();
            yield return new WaitForSeconds(4f);
        }

        //SoundManager.Instance.PlayEffect("jellymon_eat");
    }


    public void Animation_play()
    {
        //Idle코루틴 스톱
        Animation_Stop();

        SpriteAnim.frames = List_PlayAnimSprite.ToArray();
        SpriteAnim.framesPerSecond = 20;
        SpriteAnim.loop = true;
        SpriteAnim.ResetToBeginning();
        SpriteAnim.Play();
    }

    public void Animation_Stop()
    {
        SpriteAnim.Pause();
    }
    #endregion
}
