﻿using UnityEngine;
using System;
using System.Collections;
using DG.Tweening;
using TMPro;

public class TimeBombItem : Item
{
    public TextMeshPro m_TextMesh;


    public static int m_Count = 0;

    bool firstset = true;
    public int m_TimeBombCnt
    {
        get
        {
            return m_Value;
        }
        set
        {
            m_Value = value;
            m_TextMesh.text = m_Value.ToString();
            if (m_Value <= 5 && firstset == false) //처음 생성시 5이하이면 효과 없음.(생성효과랑 겹침)
            {
                transform.DOScale(1.5f, 0.3f).SetLoops(2, LoopType.Yoyo);
                //사운드
                SoundManager.Instance.PlayEffect("timebomb_ticking");
            }

            if (firstset)  firstset = false;
        }
    }

    public override void Init(Board board, ItemType type, ColorType color, int val = 0, bool copy = false)
    {
        base.Init(board, type, color, val);

        firstset = true;

        if (copy == false)            
            m_TimeBombCnt = m_MatchMgr.m_CSD.TimeBomb_FirstCount;
        else
            m_TimeBombCnt = val;
    }


    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //기본 Brust 이펙트
        //******************
        //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
        ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
        _effect.DefaultBrustEffect(m_Board.transform.position, m_Color);


        //이펙트
        EffectManager.Instance.TimeBombEffect(m_Board.transform.position, (int)m_Color-1);

        //사운드
        SoundManager.Instance.PlayEffect("timebomb_destroy");

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(0);

        //완료
        Complete();
        yield return null;
    }


    void OnEnable()
    {
        m_Count++;

        //Init보다 먼저 타기때문에.. static
        MatchManager.SwitchingApply += SwitchingApply;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;

        //Init보다 먼저 타기때문에.. static
        MatchManager.SwitchingApply -= SwitchingApply;
    }

    public void SwitchingApply()
    {
        m_TimeBombCnt--;
    }


    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.TimeBomb);
    }
}
