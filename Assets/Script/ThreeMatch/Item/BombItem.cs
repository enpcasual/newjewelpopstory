﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BombItem : Item
{
    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    public override void Brust(Action Complete)
    {
        base.Brust(Complete);

        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_Bomb(m_Board,this, 1, Complete);
    }
    #endregion

    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        switch (combinetype)
        {
            case ItemType.Butterfly:
                {
                    m_AbilityMgr.Ability_BombButterfly(m_Board);
                    Complete(); //나비와의 콤바인은 나비가 날아가서 다시 구현되므로 이폭탄은 그냥 없앤다!
                }
                break;
            case ItemType.Line_Y:
            case ItemType.Line_X:
                {
                    m_AbilityMgr.Ability_BombLine(m_Color, Complete);
                }
                break;
            case ItemType.Line_C:
                {
                    m_AbilityMgr.Ability_BombCross(m_Color, Complete);         
                }
                break;
            case ItemType.Bomb:
                {
                    m_AbilityMgr.Ability_BombBomb(m_Board, 3, Complete);      
                }
                break;
            default:
                {
                    Complete(); //아이템이 효과 없이 지워질때 필요하다!!
                }
                break;
        }    
    }

    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Butterfly:
            case ItemType.Line_Y:
            case ItemType.Line_X:
            case ItemType.Line_C:
                break;
            case ItemType.Bomb:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }
    #endregion

    public override void MissionApply()
    {
        if(m_MissionApplied)
            return;

        base.MissionApply();

        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Bomb);
        switch (m_Conbinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Bomb_Line);
                break;
            case ItemType.Line_C:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Bomb_Cross);
                break;
            case ItemType.Bomb:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderS, MissionKind.Bomb_Bomb);
                break;
        }
    }
}

