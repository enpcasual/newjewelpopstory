﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class KeyItem : Item
{
    public static int m_Count = 0;

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //이펙트&효과&사운드
        //******************
        if (m_BrustEffect)
        {
            //******************
            //기본 Brust 이펙트
            //******************
            //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
            ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
            _effect.DefaultBrustEffect(m_Board.transform.position, m_Color);
        }

        //사운드
        if (m_BrustSound)
            SoundManager.Instance.PlayEffect("combo1");

        //효과 발동
        BottleBrust();

        //완료
        Complete();
        yield return null;
    }
    #endregion

    public void BottleBrust()
    {
        List<BottleCagePanel>_ListBottlePanel = Board.m_ListBottlePanel;
        for (int i = _ListBottlePanel.Count - 1 ; i >= 0; i--)
        {
            //Board의 Brust시스템과 연계할수 없다. 
            //그리고 여기는 item이다. 코루틴 호출하면 안된다.! 
            _ListBottlePanel[i].BottleBrust(this);
        }

        //Debug.LogError("보물상자 남은 갯수 : " + Board.m_ListBottlePanel.Count);
    }

    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }
}
