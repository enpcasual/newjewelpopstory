﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FoodItem : Item
{
    //*********
    //Variable
    //*********
    public static int m_Count = 0;

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        //*******************************************************
        //푸드는 m_Brust값이 false로 세팅해서 여기를 탈수 없다!!
        //Take함수를 이용한다.
        //********************************************************

        base.Brust(Complete);

        Complete();        
    }

    //음식은 특별하다! 코루틴은 Item스크립트에서 돌리면 안된다.
    override public void Take(Action Complete)
    {
        StartCoroutine(Co_Take(Complete));
    }

    public IEnumerator Co_Take(Action Complete)
    {
        yield return new WaitForSeconds(0.5f); 
        //완료   
        SoundManager.Instance.PlayEffect("object_down");

        Complete();
    }


    void OnEnable()
   // public override void Init(Board board, Stage stage)
    {
        //base.Init(board, stage);
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }

    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        switch (m_ItemType)
        {
            case ItemType.Misson_Food1:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.StrawberryCake);
                break;
            case ItemType.Misson_Food2:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.ChocolatePiece);
                break;
            case ItemType.Misson_Food3:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.MintCake);
                break;
            case ItemType.Misson_Food4:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.Parfait);
                break;
            case ItemType.Misson_Food5:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.WhiteCake);
                break;
            case ItemType.Misson_Food6:
                MissionManager.Instance.MissionApply(gameObject, MissionType.Food, MissionKind.Hamburger);
                break;
        }
    }


    //일반 관리용 삭제
    public override void Destroy()
    {
        base.Destroy();
    }
  
}
