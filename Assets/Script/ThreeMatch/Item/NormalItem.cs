﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NormalItem : Item, IRainbowBurst
{
    private bool m_isRainbowBurst;
    public bool isRainbowBurst
    {
        get
        {
            return m_isRainbowBurst;
        }
        set
        {
            m_isRainbowBurst = value;
        }
    }

    public override int GetDestoryScore(bool combo)
    {
        if (m_BrustScore)
        {
            if (combo)
                return m_MatchMgr.ComboCnt * DestoryScore;
            else
                return DestoryScore;
        }
        else
            return 0;
    }

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //이펙트&효과&사운드
        //******************
        if (m_BrustEffect)
        {
            if (isRainbowBurst)
            {
                isRainbowBurst = false;
                this.RainbowBurstEffect();
            }
            else
            {
                //******************
                //기본 Brust 이펙트
                //******************
                //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
                EffectManager.Instance.PieceEffect(m_Board.transform.position, m_Color);
                //ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
                //_effect.DefaultBrustEffect(m_Board.transform.position, m_Color);
            }
        }

        //사운드
        if (m_BrustSound)
            SoundManager.Instance.PlayEffect("combo1");


        //완료
        Complete();
        yield return null;
    }
    #endregion

    #region Combine Brust
    //지우면 안됨. 치트키에서 여기를 타는구나..
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, Action Complete)
    {
        //완료
        Complete();
        yield return null;
    }


    public override bool CheckCombine(ItemType combinetype)
    {
        return base.CheckCombine(combinetype);
    }

    #endregion

    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        switch (m_Color)
        {
            case ColorType.None:
                break;
            case ColorType.ORANGE:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Orange);
                break;
            case ColorType.RED:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Red);
                break;
            case ColorType.YELLOW:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Yellow);
                break;
            case ColorType.PURPLE:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Purple);
                break;
            case ColorType.BLUE:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Blue);
                break;
            case ColorType.GREEN:
                MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Green);
                break;
            case ColorType.Rnd:
                break;
            default:
                break;
        }
    }


    public override void Destroy()
    {
        base.Destroy();
    }

    public void RainbowBurstEffect()
    {
        EffectManager.Instance.PieceEffect(m_Board.transform.position, m_Color);
        //.레인보우 버스트일 경우 해야할 무언갈 다 하시면 됩니다.
        //ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
        //_effect.RainbowBrustEffect(m_Board.transform.position, m_Color);
    }
}
