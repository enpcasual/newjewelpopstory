﻿using UnityEngine;
using System;
using System.Collections;

public class SpiralItem : Item
{
    public static int m_Count = 0;

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);


        //이펙트
        EffectManager.Instance.SpiralEffect(m_Board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("spiral_destroy");

        //효과 발동    

        //Action 호출
        Complete();
    }

    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }

    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        MissionManager.Instance.MissionApply(gameObject, MissionType.OrderN, MissionKind.Spiral);
    }

}
