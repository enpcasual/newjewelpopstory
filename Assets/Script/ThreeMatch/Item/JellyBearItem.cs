﻿using UnityEngine;
using System;
using System.Collections;

public class JellyBearItem : Item
{

    public static int m_Count = 0;

    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //기본 Brust 이펙트
        //******************
        //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
        ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
        _effect.DefaultBrustEffect(m_Board.transform.position, m_Color);

        //사운드
        SoundManager.Instance.PlayEffect("bear_destroy");

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(0);

        //완료
        Complete();
        yield return null;
    }



    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }


    public override void MissionApply()
    {
        if (m_MissionApplied)
            return;

        base.MissionApply();
        MissionManager.Instance.MissionApply(gameObject, MissionType.Bear, MissionKind.Bear);
    }
}
