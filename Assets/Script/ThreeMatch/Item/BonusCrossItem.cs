﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BonusCrossItem : Item
{
    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_BonusCross(m_Board);

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        //yield return new WaitForSeconds(0);

        //완료
        Complete();
        yield return null;
    }

    #endregion

    /*
    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, bool abillity, System.Action Complete)
    {
        base.CombineBrust(combinetype, abillity, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, abillity, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, bool abillity, System.Action Complete)
    {
        if (abillity)
        {
            switch (combinetype)
            {
                case ItemType.Line_Y:
                case ItemType.Line_X:
                case ItemType.Bomb_X:
                    {
                        m_AbilityMgr.Ability_BombXLine(m_Color);
                        yield return new WaitForSeconds(0f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!
                    }
                    break;
                case ItemType.Bomb_O:
                    {
                        m_AbilityMgr.Ability_BombXBombO(m_Color);
                        yield return new WaitForSeconds(1f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!
                    }
                    break;
                default:
                    break;
            }
        }

        //아이템 삭제
        SetVisible(false);
        yield return new WaitForSeconds(DestoryTime);
        //완료
        Complete();
        yield return null;
    }

    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
            case ItemType.Bomb_X:
                break;
            case ItemType.Bomb_O:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }

    #endregion
    */
}
