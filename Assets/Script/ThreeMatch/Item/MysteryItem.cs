﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using JsonFx.Json;

public class MysteryItem : Item
{
    public static int m_Count = 0;

    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    override public void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //******************
        //이펙트&효과&사운드
        //******************
        if (m_BrustEffect)
        {
            //******************
            //기본 Brust 이펙트
            //******************
            //해당 보드아이템이 터지는데 드랍되는 중이라도 이펙트를 보드에 준다.
            ItemEffect _effect = EffectManager.Instance.GetDefaultEffect();
            _effect.DefaultBrustEffect(m_Board.transform.position, m_Color);
        }

        //사운드
        if (m_BrustSound)
            SoundManager.Instance.PlayEffect("combo1");


        //완료
        Complete();
        yield return null;
    }
    #endregion

    #region Combine Brust
    //지우면 안됨. 치트키에서 여기를 타는구나..
    public override void CombineBrust(ItemType combinetype, Action Complete)
    {
        base.CombineBrust(combinetype, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, Action Complete)
    {
        //완료
        Complete();
        yield return null;
    }


    public override bool CheckCombine(ItemType combinetype)
    {
        return base.CheckCombine(combinetype);
    }

    #endregion


    void OnEnable()
    {
        m_Count++;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        m_Count--;
    }
}
