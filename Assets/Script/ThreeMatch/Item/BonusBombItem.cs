﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BonusBombItem : Item
{
    #region Match Brust
    //터졌을때 이펙트 및 효과 발동
    public override void Brust(Action Complete)
    {
        base.Brust(Complete);

        StartCoroutine(CoroutineBrust(Complete));
    }

    public IEnumerator CoroutineBrust(Action Complete)
    {
        //*******************
        //이펙트 & 효과 발동
        //*******************
        m_AbilityMgr.Ability_BonusBomb(m_Board, 1);

        //****************************************
        //아이템 삭제 딜레이. 연출에 따라 다르다. 
        //*****************************************
        yield return new WaitForSeconds(0.3f);

        //완료
        Complete();
        yield return null;
    }
    #endregion

    /*
    #region Combine Brust
    public override void CombineBrust(ItemType combinetype, bool abillity, System.Action Complete)
    {
        base.CombineBrust(combinetype, abillity, Complete);

        StartCoroutine(CoroutineCombineBrust(combinetype, abillity, Complete));
    }

    public IEnumerator CoroutineCombineBrust(ItemType combinetype, bool abillity, System.Action Complete)
    {
        switch (combinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
                {
                    m_AbilityMgr.Ability_BombOLine(m_Color);

                    //아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!
                    //폭탄의 애니메이션1초 대기. 먼저 터지면 waitStep으로 넘어가버린다
                    yield return new WaitForSeconds(1f);
                }
                break;
            case ItemType.Bomb_O:
                {
                    m_AbilityMgr.Ability_BombOBombO(m_Board, 2); //만약 이펙트가 커야하면 수정 필요.
                    yield return new WaitForSeconds(0.5f);//아이템 삭제 딜레이. 연출마다 다를수밖에 없다.!
                }
                break;

            default:
                break;
        }

        //아이템 삭제
        SetVisible(false);
        yield return new WaitForSeconds(DestoryTime);
        //완료
        Complete();
        yield return null;
    }
    public override bool CheckCombine(ItemType combinetype)
    {
        base.CheckCombine(combinetype);
        bool isConbine = true;
        switch (combinetype)
        {
            case ItemType.Line_Y:
            case ItemType.Line_X:
                break;
            case ItemType.Bomb_O:
                break;
            default:
                isConbine = false;
                break;
        }

        return isConbine;
    }
    #endregion
    */
}

