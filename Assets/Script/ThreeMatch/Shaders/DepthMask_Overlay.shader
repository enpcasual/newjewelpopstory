Shader "Mask/Depth Mask_Overlay" {
    SubShader {
		Tags{ "Queue" = "Overlay" } //Transparent
        Lighting Off
        ZTest LEqual
        ZWrite On
        ColorMask 0
        Pass {}
    }
}