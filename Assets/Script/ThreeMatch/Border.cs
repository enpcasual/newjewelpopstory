﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Point
{
    public int x;
    public int y;
}

public class MeshInfo
{
    public int x;
    public int y;

    public bool Exist;

    //Labels
    public int Label;

    public MeshInfo(int _x, int _y)
    {
        x = _x;
        y = _y;
        Exist = false;
        Label = 0;
    }
}

public class ListMeshInfo : List<MeshInfo>
{

    public MeshInfo this[int _x, int _y]
    {
        get
        {
            return this.Find(find => find.x == _x && find.y == _y);
        }
    }
}

public class Border : MonoBehaviour
{

    Mesh mesh;

    public float BlockPixel = 1; //1이면 100pixel
    public float round = 7;
    public float lineWidth = 15;
    public float lineUVScale = 3; //이미지의 반복횟수?

    public void SetBorderMesh(List<Point> data)
    {
        float _blockPixel = BlockPixel * 0.01f;
        float _round = round * 0.01f;
        float _lineWidth = lineWidth * 0.01f;
        float _lineUVScale = lineUVScale;

        if (mesh != null)
        {
            DestroyImmediate(mesh, true);
            mesh = null;
        }

        mesh = CreateBorderMesh(data, _blockPixel, _round, _lineWidth, _lineUVScale);
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public static Mesh CreateBorderMesh(List<Point> data, float blockSize, float round, float lineWidth, float lineUVScale)
    {
        int minX = data.Min(r => r.x);
        int maxX = data.Max(r => r.x);
        int minY = data.Min(r => r.y);
        int maxY = data.Max(r => r.y);

        ListMeshInfo listMeshInfo = new ListMeshInfo(); 
        for (int y = minY; y <= maxY; y++)
            for (int x = minX = 0; x <= maxX; x++)
                listMeshInfo.Add(new MeshInfo(x, y));



        //외각선
        foreach (Point po in data)
            listMeshInfo[po.x, po.y].Exist = true;

        int LabelCnt = ConnectedComponentLabeling(listMeshInfo);
        List<Mesh> listmesh = new List<Mesh>();

        Vector3 offset = new Vector3(blockSize * 0.5f, blockSize * 0.5f, 0);

        for (int i = 1; i <= LabelCnt; ++i)
        {
            Point[] points = DetectingMarchingSquares(listMeshInfo, i);

            if (points == null)
                continue;

            List<Vector3> pos = new List<Vector3>();
            for (int k = 0; k < points.Length; k++)
            {
                pos.Add(new Vector3(points[k].x * blockSize, points[k].y * blockSize, 0) + offset);
            }

            pos = MakeRound(pos, round);
            listmesh.Add(CreateLineMesh(pos.ToArray(), lineWidth, lineUVScale, false, true));
        }

        //내각선
        foreach (MeshInfo info in listMeshInfo)
        {
            info.Label = 0;
            info.Exist = true;
        }

        foreach (Point po in data)
            listMeshInfo[po.x, po.y].Exist = false;

        LabelCnt = ConnectedComponentLabeling(listMeshInfo, false);

        //실제 내각 찾기
        List<int> holes = new List<int>();

        for (int i = 1; i <= LabelCnt; ++i)
            holes.Add(i);

        for (int y = minY; y <= maxY; ++y)
        {
            if (holes.Contains(listMeshInfo[minX, y].Label))
                holes.Remove(listMeshInfo[minX, y].Label);

            if (holes.Contains(listMeshInfo[maxX, y].Label))
                holes.Remove(listMeshInfo[maxX, y].Label);
        }

        for (int x = minX; x <= maxX; ++x)
        {
            if (holes.Contains(listMeshInfo[x, minY].Label))
                holes.Remove(listMeshInfo[x, minY].Label);

            if (holes.Contains(listMeshInfo[x, maxY].Label))
                holes.Remove(listMeshInfo[x, maxY].Label);
        }

        foreach (int h in holes)
        {
            Point[] points = DetectingMarchingSquares(listMeshInfo, h);

            if (points == null)
                continue;

            List<Vector3> pos = new List<Vector3>();
            for (int k = 0; k < points.Length; k++)
            {
                pos.Add(new Vector3(points[k].x * blockSize, points[k].y * blockSize, 0) + offset);
            }

            pos = MakeRound(pos, round);
            listmesh.Add(CreateLineMesh(pos.ToArray(), lineWidth, lineUVScale, false, true));
        }

        //Combine mesh
        Mesh mesh = new Mesh();
        CombineInstance[] combine = new CombineInstance[listmesh.Count];

        for (int i = 0; i < listmesh.Count; ++i)
        {
            combine[i].mesh = listmesh[i];
            combine[i].subMeshIndex = 0;
        }

        mesh.CombineMeshes(combine, true, false);
        mesh.RecalculateBounds();
        ;
        return mesh;
    }

    #region Connected-component labeling
    //connected component labeling에 대한 설명블러그
    //http://m.blog.daum.net/shksjy/198
    static int ConnectedComponentLabeling(ListMeshInfo data, bool dir8 = true)
    {
        Dictionary<int, List<int>> linked = new Dictionary<int, List<int>>(); //등가표!!
        int nextLabel = 1;

        //First pass
        foreach (MeshInfo meshinfo in data) //생성할때 순서대로 만들어놨다.
        {
            if (meshinfo.Exist == false) continue;

            int currentLabel;
            List<int> neighbors = new List<int>();

            //TOP
            MeshInfo _neightbor = data[meshinfo.x, meshinfo.y - 1];
            if (_neightbor != null)
                if (_neightbor.Label != 0)
                    neighbors.Add(_neightbor.Label);

            //LEFT
            _neightbor = data[meshinfo.x - 1, meshinfo.y];
            if (_neightbor != null)
                if (_neightbor.Label != 0)
                    neighbors.Add(_neightbor.Label);

            //TOPRIGHT
            _neightbor = data[meshinfo.x + 1, meshinfo.y - 1];
            if (_neightbor != null && dir8 == true)
                if (_neightbor.Label != 0)
                    neighbors.Add(_neightbor.Label);

            //TOPLEFT
            _neightbor = data[meshinfo.x - 1, meshinfo.y - 1];
            if (_neightbor != null && dir8 == true)
                if (_neightbor.Label != 0)
                    neighbors.Add(_neightbor.Label);


            if (neighbors.Count == 0)
            {
                currentLabel = nextLabel;
                linked.Add(currentLabel, new List<int>() { currentLabel }); //라벨과 
                nextLabel++;
            }
            else
            {
                //Find the smallest label
                //neighbors.Sort((int x, int y) => x.CompareTo(y));  //Sort로 최소값 구할려다가 Linq로 min값을 바로 구할수 있다.
                currentLabel = neighbors.Min();

                foreach (int labels in neighbors)
                {
                    //1. neighbors가 한개만 있거나 동일한 라벨들만 있다면 있다면 변경되는건 없다.
                    //2. 주변의 여러개의 다른 라벨들이 있었다면 해당 라벨에 추가한다.
                    //(즉, 같은라벨 안에 여러값이 있다면 서로 연결되어 있는거다.)
                    linked[labels] = linked[labels].Union(neighbors).ToList();
                }

            }

            meshinfo.Label = currentLabel; //최종적으로 라벨을 넣어준다.
        }


        //라벨마다 들어있는 값중에 다른 라벨값이 있으면 그 라벨하고는 연결된것이므로 현재 값들을 넣어준다.
        //계속 작업이 진행되면 라벨의 최소값이 연결된 끝라벨까지 추가된다.
        foreach (KeyValuePair<int, List<int>> dic in linked)
        {
            foreach (int val in linked[dic.Key])
                if (dic.Key != val)
                {
                    List<int> temp = linked[val].Union(linked[dic.Key]).ToList();
                    linked[val].Clear();
                    linked[val].AddRange(temp);
                }
        }

        //Second pass
        int LabelCnt = 0;
        foreach (MeshInfo meshinfo in data)
        {

            if (meshinfo.Label == 0) continue;

            meshinfo.Label = linked[meshinfo.Label].Min();

            if (meshinfo.Label > LabelCnt)
                LabelCnt = meshinfo.Label;
        }

        return LabelCnt;
    }
    #endregion

    #region MarchingSquares
    //http://kksbee.blogspot.kr/2014/01/marching-squares.html
    //http://blog.naver.com/youibelieve/110140107131
    static int CheckMarchingSquare(ListMeshInfo data, int x, int y, int label)
    {
        int ret = 0;

        if (data[x, y] != null)
            if (data[x, y].Label == label) ret += 1;

        if (data[x + 1, y] != null)
            if (data[x + 1, y].Label == label) ret += 2;

        if (data[x + 1, y + 1] != null)
            if (data[x + 1, y + 1].Label == label) ret += 4;

        if (data[x, y + 1] != null)
            if (data[x, y + 1].Label == label) ret += 8;

        return ret;
    }

    static Point[] DetectingMarchingSquares(ListMeshInfo data, int label)
    {
        MeshInfo info = data.Find(r => r.Label == label);
        if (info == null) return null;

        Point pt = new Point();
        pt.x = info.x;
        pt.y = info.y;
        pt.x -= 1;
        pt.y -= 1;
        int ix = pt.x;
        int iy = pt.y;
        int initialVal = CheckMarchingSquare(data, pt.x, pt.y, label);

        if (initialVal == 0 || initialVal == 15)
            return null;

        List<Point> pts = new List<Point>();
        pts.Add(pt);
        int prevDir = -1;

        do
        {
            Point point = new Point();
            point.x = pt.x;
            point.y = pt.y;
            pt = point;

            int d = -1;
            int v = CheckMarchingSquare(data, pt.x, pt.y, label);

            switch (v)
            {
                case 8:
                case 9:
                case 11:
                    d = 0;
                    break;

                case 4:
                case 12:
                case 13:
                    d = 1;
                    break;

                case 2:
                case 6:
                case 14:
                    d = 2;
                    break;

                case 1:
                case 3:
                case 7:
                    d = 3;
                    break;

                case 5:
                    d = (prevDir == 0) ? 1 : 3;
                    break;

                case 10:
                    d = (prevDir == 3) ? 0 : 2;
                    break;

                default:
                    return null;
            }

            switch (d)
            {
                case 0:
                    pt.y += 1;
                    break;

                case 1:
                    pt.x += 1;
                    break;

                case 2:
                    pt.y -= 1;
                    break;

                case 3:
                    pt.x -= 1;
                    break;
            }
            pts.Add(pt);
            prevDir = d;
        } while (ix != pt.x || iy != pt.y);

        return pts.ToArray();
    }
    #endregion

    #region CreateLineMesh
    public enum LineOffset
    {
        Centered,
        Inner,
        Outer
    }

    static List<Vector3> MakeRound(List<Vector3> vec, float radius)
    {
        Vector3 v1;
        Vector3 v2;
        List<Vector3> nVec = new List<Vector3>();

        for (int i = 0; i < vec.Count; ++i)
        {
            int i1, i2;

            if (i == 0)
            {
                i1 = vec.Count - 2;
                i2 = 1;
            }
            else if (i == vec.Count - 1)
            {
                i1 = i - 1;
                i2 = 1;
                nVec.Add(nVec[0]);
                //원래 데이타가 처음점과 끝점을 같은 좌표를 넘겨준다.
                //그런데 여기에서 라운드때문에 3점이 중복되므로 원본데이타처럼 한점만 중복되도록한다.
                continue;
            }
            else
            {
                i1 = i - 1;
                i2 = i + 1;
            }

            v1 = (vec[i1] - vec[i]).normalized;
            v2 = (vec[i2] - vec[i]).normalized;
            float curvature = Vector3.Dot(v1, v2);

            if (Mathf.Abs(curvature) < 0.1f)
            {
                nVec.Add(vec[i] + v1 * radius);
                nVec.Add(vec[i] + (v1 + v2) * radius * 0.3f);
                nVec.Add(vec[i] + v2 * radius);
            }
            else
            {
                nVec.Add(vec[i]);
            }
        }

        return nVec;
    }

    public static Mesh CreateLineMesh(Vector3[] pos, float lineWidth, float uvScale, bool flipY, bool closed, LineOffset Loffset = LineOffset.Centered)
    {
        int vcnt = pos.Length;

        int[] m_Indices = new int[(vcnt * 2 - 2) * 3];
        for (int i = 0, k = 0; i < vcnt * 2 - 3; i += 2, k++)
        {
            m_Indices[i * 3] = k * 2;
            m_Indices[i * 3 + 1] = k * 2 + 1;
            m_Indices[i * 3 + 2] = k * 2 + 2;

            m_Indices[i * 3 + 3] = k * 2 + 1;
            m_Indices[i * 3 + 4] = k * 2 + 3;
            m_Indices[i * 3 + 5] = k * 2 + 2;

        }

        //setup dist for uv
        float[] dist = new float[vcnt];
        float totalDist = 0;

        for (int i = 1; i < pos.Length; ++i)
        {
            totalDist += (pos[i - 1] - pos[i]).magnitude;
            dist[i] = totalDist;
        }

        float scaleAdj = (int)(totalDist + 0.5f) * uvScale;

        for (int i = 0; i < pos.Length; ++i)
            dist[i] = dist[i] * scaleAdj / totalDist;

        //setup pos & uv
        Vector3[] vertices = new Vector3[vcnt * 2];
        Vector2[] uv = new Vector2[vcnt * 2];
        Vector3 oldTangent = Vector3.zero;
        Vector3 faceNormal = Vector3.back;

        if (closed)
        {
            Vector3 dir = (pos[pos.Length - 1] - pos[pos.Length - 2]);
            oldTangent = Vector3.Cross(dir, faceNormal).normalized;
        }

        int j = 0;

        for (int i = 0, len = pos.Length; i < len; i++)
        {
            Vector3 cur = pos[i];
            Vector3 next = i != len - 1 ? pos[i + 1] : pos[1];
            Vector3 dir = (next - cur);
            Vector3 tangent = Vector3.Cross(dir, faceNormal).normalized;
            Vector3 offset = Vector3.zero;


            //*BJS**************************************************************************
            //각도가 꺽이면서 동일한 거리에 verties를 만들면 곡선이 되버린다.
            //vertices가 있을 거리를 각도에 맞게 늘려줘서 반듯한 사각형이 되도록 보정한다.
            //Round가 적용되도 자세히 보면 차이점이 보인다. 꺽였을때 두께가 유지 된다.
            //******************************************************************************
            Vector3 temp = (oldTangent + tangent).normalized; //현재 vertices가 놓여질 방향
            float deg = Vector3.Angle(dir, temp);            //즉, 현재 vertices의 각도를 구한다.
            float sinv = Mathf.Sin(Mathf.Deg2Rad * deg);     //각도로 sin값을 구한다.
            float rev = 1 / sinv;                            //비율만 필요하기때문에 y는 1이라고 하고 사선의 길이를 구한다.

            switch (Loffset)
            {
                case LineOffset.Centered:
                    offset = (oldTangent + tangent).normalized * lineWidth * 0.5f * rev;
                    vertices[j * 2] = cur - offset;
                    vertices[j * 2 + 1] = cur + offset;
                    break;

                case LineOffset.Inner:
                    offset = (oldTangent + tangent).normalized * lineWidth;
                    vertices[j * 2] = cur - offset;
                    vertices[j * 2 + 1] = cur;
                    break;

                case LineOffset.Outer:
                    offset = (oldTangent + tangent).normalized * lineWidth;
                    vertices[j * 2] = cur;
                    vertices[j * 2 + 1] = cur + offset;
                    break;
            }

            if (!flipY)
            {
                uv[j * 2] = new Vector2(dist[i], 0);
                uv[j * 2 + 1] = new Vector2(dist[i], 1);
            }
            else
            {
                uv[j * 2] = new Vector2(dist[i], 1);
                uv[j * 2 + 1] = new Vector2(dist[i], 0);
            }

            oldTangent = tangent;
            j++;
        }

        //build mesh
        var mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.SetTriangles(m_Indices, 0);
        mesh.RecalculateBounds();
        ;
        return mesh;
    }
    #endregion
}
