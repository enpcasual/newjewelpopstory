﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum LineType { LineX, LineY, CrossLT, CrossRT }


public class LineEffect : MonoBehaviour {

    public List<Sprite> m_ListLineSpr = new List<Sprite>();
    public List<Sprite> m_ListLineShotSpr = new List<Sprite>();
    public List<Sprite> m_ListLineBigSpr = new List<Sprite>();

    //Liine Brust
    public SpriteRenderer LineX;
    public SpriteRenderer LineY;
    public SpriteRenderer LineCL;
    public SpriteRenderer LineCR;

    public SpriteRenderer LineShot1;
    public SpriteRenderer LineShot2;
    public SpriteRenderer LineShot3;
    public SpriteRenderer LineShot4;

    //CombineBrust
    public GameObject LineBig_Sprite; //SpriteRenderer
    public SpriteRenderer LineShotBig1;
    public SpriteRenderer LineShotBig2;
    public SpriteRenderer LineShotBig3;
    public SpriteRenderer LineShotBig4;


    #region 라인 이펙트
    //라인 기본이펙트
    float scale_y = 30f;
    float scale_time = 1.2f;
    float scale_y_bonus = 7f;
    float scale_time_bonus = 0.6f;

    public void Reset(Vector3 pos)
    {
        transform.position = pos;
    }

    public void LineXEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                _spr = m_ListLineSpr[Random.Range(0,6)];
                break;
            case ColorType.RED:   
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:  
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }
        LineX.sprite = _spr;
        LineX.transform.localScale = Vector3.one;
        LineX.color = Color.white;
        LineX.gameObject.SetActive(true);
        Sequence seq = DOTween.Sequence();
        seq.Append(LineX.transform.DOScaleY(scale_y, scale_time).SetEase(Ease.Linear));                        //스케일
        seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineX.color, _color => LineX.color = _color, 0f, scale_time / 2));  //알파값
        seq.OnComplete(() =>
        {
            LineX.transform.localScale = Vector3.zero;

            LineX.gameObject.SetActive(false);
        });

        //미사일..
        //LineShotEffect(color, LineType.LineX);

        StartCoroutine(ReStore(1f));
    }

    public void LineYEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                _spr = m_ListLineSpr[Random.Range(0, 6)];
                break;
            case ColorType.RED:
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:         
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }
        LineY.sprite = _spr;
        LineY.transform.localScale = Vector3.one;
        LineY.color = Color.white;
        LineY.gameObject.SetActive(true);
        Sequence seq = DOTween.Sequence();
        seq.Append(LineY.transform.DOScaleY(scale_y, scale_time).SetEase(Ease.Linear));                        //스케일
        seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineY.color, _color => LineY.color = _color, 0f, scale_time / 2));  //알파값
        seq.OnComplete(() =>
        {
            LineY.transform.localScale = Vector3.zero;

            LineY.gameObject.SetActive(false);
        });

        //미사일..
        //LineShotEffect(color, LineType.LineY);

        StartCoroutine(ReStore(1f));
    }

    public void LineCEffect(Vector3 pos, ColorType color, bool bonus = false)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                _spr = m_ListLineSpr[Random.Range(0, 6)];
                break;
            case ColorType.RED:
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }
        LineCL.sprite = _spr;
        LineCL.transform.localScale = Vector3.one;
        LineCL.color = Color.white;
        LineCL.gameObject.SetActive(true);

        LineCR.sprite = _spr;
        LineCR.transform.localScale = Vector3.one;
        LineCR.color = Color.white;
        LineCR.gameObject.SetActive(true);


        Sequence seq = DOTween.Sequence();

        if (bonus) //보너스 타임때 나오는 라인크로스는 범위가 작다.
        {
            seq.Append(LineCL.transform.DOScaleY(scale_y_bonus * 1.4f, scale_time_bonus).SetEase(Ease.Linear));                        //스케일
            seq.Insert(0, LineCR.transform.DOScaleY(scale_y_bonus * 1.4f, scale_time_bonus).SetEase(Ease.Linear));                        //스케일
            seq.Insert(scale_time_bonus / 2, DOTween.ToAlpha(() => LineCL.color, _color => LineCL.color = _color, 0f, scale_time_bonus / 2));  //알파값     
            seq.Insert(scale_time_bonus / 2, DOTween.ToAlpha(() => LineCR.color, _color => LineCR.color = _color, 0f, scale_time_bonus / 2));  //알파값
        }
        else
        {
            seq.Append(LineCL.transform.DOScaleY(scale_y * 1.4f, scale_time).SetEase(Ease.Linear));                        //스케일
            seq.Insert(0, LineCR.transform.DOScaleY(scale_y * 1.4f, scale_time).SetEase(Ease.Linear));                        //스케일
            seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineCL.color, _color => LineCL.color = _color, 0f, scale_time / 2));  //알파값     
            seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineCR.color, _color => LineCR.color = _color, 0f, scale_time / 2));  //알파값
        }
        seq.OnComplete(() =>
        {
            LineCL.transform.localScale = Vector3.zero;
            LineCR.transform.localScale = Vector3.zero;
            LineCL.gameObject.SetActive(false);
            LineCR.gameObject.SetActive(false);
        });

        //미사일..
        //LineShotEffect(color, LineType.CrossLT, bonus);

        StartCoroutine(ReStore(1f));
    }
    #endregion

    #region 라인 미사일
    //라인 마시일 이펙트
    float shotmove_small = 1.4f;//1.4f;
    float shottime_small = 0.20f;//0.156f; //낮을수록 빠름.. 걸리는 시간..
    float shotmove = 7f;
    float shottime = 0.1f; //낮을수록 빠름.. 걸리는 시간..
    float shotmove_big = 7f;
    float shottime_big = 0.8f; //낮을수록 빠름.. 걸리는 시간..
  

    public void LineShotEffect(ColorType color, LineType line, bool bonus = false)
    {
        Sprite _spr = null;
        switch (color)
        {
            case ColorType.RED:         
                _spr = m_ListLineShotSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineShotSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineShotSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineShotSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineShotSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineShotSpr[5];
                break;
            default:
                break;
        }
        LineShot1.sprite = _spr;
        LineShot2.sprite = _spr;
        LineShot3.sprite = _spr;
        LineShot4.sprite = _spr;

        //Shot이펙트
        switch (line)
        {
            case LineType.LineX:
                {
                    //left
                    LineShot1.gameObject.SetActive(true);
                    LineShot1.transform.localPosition = new Vector3(-0.15f, 0, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot1.transform.eulerAngles = new Vector3(0, 0, 90);
                    //right
                    LineShot2.gameObject.SetActive(true);
                    LineShot2.transform.localPosition = new Vector3(0.15f, 0, -0.5f);
                    LineShot2.transform.eulerAngles = new Vector3(0, 0, -90);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShot1.transform.DOLocalMoveX(-shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShot2.transform.DOLocalMoveX(shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShot1.gameObject.SetActive(false);
                        LineShot2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.LineY:
                {
                    //top
                    LineShot3.gameObject.SetActive(true);
                    LineShot3.transform.localPosition = new Vector3(0, 0.15f, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot3.transform.eulerAngles = new Vector3(0, 0, 0);
                    //bottom
                    LineShot4.gameObject.SetActive(true);
                    LineShot4.transform.localPosition = new Vector3(0, -0.15f, -0.5f);
                    LineShot4.transform.eulerAngles = new Vector3(0, 0, 180);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShot3.transform.DOLocalMoveY(shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShot4.transform.DOLocalMoveY(-shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShot3.gameObject.SetActive(false);
                        LineShot4.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossLT:
            case LineType.CrossRT:
                {
                    //TL
                    LineShot1.gameObject.SetActive(true);
                    LineShot1.transform.localPosition = new Vector3(-0.15f, 0.15f, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot1.transform.eulerAngles = new Vector3(0, 0, 45);
                    //TR
                    LineShot2.gameObject.SetActive(true);
                    LineShot2.transform.localPosition = new Vector3(0.15f, 0.15f, -0.5f);
                    LineShot2.transform.eulerAngles = new Vector3(0, 0, -45);
                    //BL
                    LineShot3.gameObject.SetActive(true);
                    LineShot3.transform.localPosition = new Vector3(-0.15f, -0.15f, -0.5f);
                    LineShot3.transform.eulerAngles = new Vector3(0, 0, 135);
                    //BR
                    LineShot4.gameObject.SetActive(true);
                    LineShot4.transform.localPosition = new Vector3(0.15f, -0.15f, -0.5f);
                    LineShot4.transform.eulerAngles = new Vector3(0, 0, -135);

                    Sequence seq = DOTween.Sequence();
                    if (bonus)
                    {
                        seq.Append(LineShot1.transform.DOLocalMove(new Vector3(-shotmove_small, shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot2.transform.DOLocalMove(new Vector3(shotmove_small, shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot3.transform.DOLocalMove(new Vector3(-shotmove_small, -shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot4.transform.DOLocalMove(new Vector3(shotmove_small, -shotmove_small, -1), shottime_small).SetEase(Ease.Linear));

                    }
                    else
                    {
                        seq.Append(LineShot1.transform.DOLocalMove(new Vector3(-shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot2.transform.DOLocalMove(new Vector3(shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot3.transform.DOLocalMove(new Vector3(-shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot4.transform.DOLocalMove(new Vector3(shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                    }
                    seq.OnComplete(() =>
                    {
                        LineShot1.gameObject.SetActive(false);
                        LineShot2.gameObject.SetActive(false);
                        LineShot3.gameObject.SetActive(false);
                        LineShot4.gameObject.SetActive(false);
                    });
                }
                break;
        }
    }
    #endregion

    #region 라인 큰 미사일
    public void CombineBrust_Sprite(Vector3 pos, ColorType color, bool cross = false)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        //Sprite _spr = null;
        //switch (color)
        //{        
        //    case ColorType.RED:
        //        _spr = m_ListLineBigSpr[0];
        //        break;
        //    case ColorType.YELLOW:
        //        _spr = m_ListLineBigSpr[1];
        //        break;
        //    case ColorType.GREEN:
        //        _spr = m_ListLineBigSpr[2];
        //        break;
        //    case ColorType.BLUE:         
        //        _spr = m_ListLineBigSpr[3];
        //        break;
        //    case ColorType.PURPLE:
        //        _spr = m_ListLineBigSpr[4];
        //        break;
        //    case ColorType.ORANGE:
        //        _spr = m_ListLineBigSpr[5];
        //        break;
        //    default:
        //        break;
        //}

        //LineBig_Sprite1.sprite = _spr;
        LineBig_Sprite.gameObject.SetActive(true);

        if (cross == false)
            LineBig_Sprite.transform.localEulerAngles = new Vector3(0, 0, 0);
        else
            LineBig_Sprite.transform.localEulerAngles = new Vector3(0, 0, 45);


        //스케일 애니메이션
        LineBig_Sprite.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.14f);
        seq.Append(LineBig_Sprite.transform.DOScale(1f, 0.6f).SetEase(Ease.Linear));
        seq.AppendInterval(0.3f);
        seq.OnComplete(() =>
        {
            LineBig_Sprite.gameObject.SetActive(false);
        });
    }

    float shotBig_gap = 0;
    public void LineShotBigEffect(Vector3 pos, ColorType color, LineType line)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        //Sprite _spr = null;
        //switch (color)
        //{
        //    case ColorType.RED:       
        //        _spr = m_ListLineShotSpr[0];
        //        break;
        //    case ColorType.YELLOW:
        //        _spr = m_ListLineShotSpr[1];
        //        break;
        //    case ColorType.GREEN:
        //        _spr = m_ListLineShotSpr[2];
        //        break;
        //    case ColorType.BLUE:
        //        _spr = m_ListLineShotSpr[3];
        //        break;
        //    case ColorType.PURPLE:
        //        _spr = m_ListLineShotSpr[4];
        //        break;
        //    case ColorType.ORANGE:
        //        _spr = m_ListLineShotSpr[5];
        //        break;
        //    default:
        //        break;
        //}

        //LineShotBig1.sprite = _spr;
        //LineShotBig2.sprite = _spr;
        //LineShotBig3.sprite = _spr;
        //LineShotBig4.sprite = _spr;

        //Shot이펙트
        /*
        switch (line)
        {
            case LineType.LineX:
                {
                    LineShotBig1.gameObject.SetActive(true);
                    LineShotBig1.transform.localPosition = new Vector3(shotBig_gap, 0, 0);
                    LineShotBig1.transform.eulerAngles = new Vector3(0, 0, 90);

                    LineShotBig2.gameObject.SetActive(true);
                    LineShotBig2.transform.localPosition = new Vector3(-shotBig_gap, 0, 0);
                    LineShotBig2.transform.eulerAngles = new Vector3(0, 0, -90);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig1.transform.DOLocalMoveX(-shotmove, shottime_big).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShotBig2.transform.DOLocalMoveX(shotmove, shottime_big).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShotBig1.gameObject.SetActive(false);
                        LineShotBig2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.LineY:
                {
                    LineShotBig3.gameObject.SetActive(true);
                    LineShotBig3.transform.localPosition = new Vector3(0, -shotBig_gap, 0);
                    LineShotBig3.transform.eulerAngles = new Vector3(0, 0, 0);

                    LineShotBig4.gameObject.SetActive(true);
                    LineShotBig4.transform.localPosition = new Vector3(0, shotBig_gap, 0);
                    LineShotBig4.transform.eulerAngles = new Vector3(0, 0, 180);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig3.transform.DOLocalMoveY(shotmove, shottime_big).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShotBig4.transform.DOLocalMoveY(-shotmove, shottime_big).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShotBig3.gameObject.SetActive(false);
                        LineShotBig4.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossLT:
                {
                    LineShotBig1.gameObject.SetActive(true);
                    LineShotBig1.transform.localPosition = new Vector3(shotBig_gap, -shotBig_gap, 0);
                    LineShotBig1.transform.eulerAngles = new Vector3(0, 0, 45);

                    LineShotBig2.gameObject.SetActive(true);
                    LineShotBig2.transform.localPosition = new Vector3(-shotBig_gap, shotBig_gap, 0);
                    LineShotBig2.transform.eulerAngles = new Vector3(0, 0, -135);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig1.transform.DOLocalMove(new Vector3(-shotmove, shotmove, -1), shottime_big).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig2.transform.DOLocalMove(new Vector3(shotmove, -shotmove, -1), shottime_big).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig1.gameObject.SetActive(false);
                        LineShotBig2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossRT:
                {
                    LineShotBig3.gameObject.SetActive(true);
                    LineShotBig3.transform.localPosition = new Vector3(-shotBig_gap, -shotBig_gap, 0);
                    LineShotBig3.transform.eulerAngles = new Vector3(0, 0, -45);

                    LineShotBig4.gameObject.SetActive(true);
                    LineShotBig4.transform.localPosition = new Vector3(shotBig_gap, shotBig_gap, 0);
                    LineShotBig4.transform.eulerAngles = new Vector3(0, 0, 135);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig3.transform.DOLocalMove(new Vector3(shotmove, shotmove, -1), shottime_big).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig4.transform.DOLocalMove(new Vector3(-shotmove, -shotmove, -1), shottime_big).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig3.gameObject.SetActive(false);
                        LineShotBig4.gameObject.SetActive(false);
                    });
                }
                break;
        }
        */

        StartCoroutine(ReStore(3f));
    }
    #endregion

    IEnumerator ReStore(float time)
    {
        yield return new WaitForSeconds(time);
        ObjectPool.Instance.Restore(this.gameObject);
    }

    public void Restore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }
}
