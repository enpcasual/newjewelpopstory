﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RainbowMoveEffect : MonoBehaviour
{

    public float VibrationTime = 0.8f;
    [SerializeField]
    private GameObject m_StartItem = null;
    [SerializeField]
    private GameObject m_EndItem = null;
    [SerializeField]
    private float m_Time = 0;

    [SerializeField]
    private TweenScale tweenScale;
    
    private Vector3 m_EndPos = Vector3.zero;
    private Board m_Board;
    
    public void SetInfo(GameObject start, GameObject end, float time = 0.9f) //전기이펙트가 블럭보다 앞이여서 기본값이 -2였는데 이제 뒤로 간다..
    {
        start.transform.position += new Vector3(0, 0, -0.15f);

        m_StartItem = start;
        m_EndItem = end;
        m_Time = time;
        m_EndPos = m_EndItem.transform.position;
        transform.position = m_StartItem.transform.position;
        //m_LightningBolt.Trigger(); //처음에는 호출해줘야 바로 위치 찾아간다. Duration을 0.1 줘서 update가 그만큼 기다린다.

        tweenScale.duration = m_Time;

        Invoke("ReStore", m_Time + 1.4f); // 0.4초 동안 폭발 임팩트가 있음.
        Invoke("RePosition", m_Time); // enditem 포지션x값을 초기화.
        Invoke("DoMove", 0.1f); // 0.1초 후에 무브를 시작해야 정확한 위치로 이동이 가능

        if (VibrationTime > time)
            VibrationTime = time;

        Invoke("OnVibration", m_Time + 0.1f); //진동을 시작해야하는 시간.
    }

    bool vibration = false;
    bool on = true;
    //타겟이 된 오브젝트를 좌우로 흔듬.
    private void Update()
    {
        if (m_StartItem != null && m_EndItem != null)
        {
            if (vibration)
            {
                if (on)
                {
                    m_EndItem.transform.position += new Vector3(0.05f, 0, 0);
                }
                else
                {
                    m_EndItem.transform.position += new Vector3(-0.05f, 0, 0);
                }
            }

            on = !on;
        }
    }

    private void OnVibration()
    {
        m_EndItem.transform.DOScale(m_EndItem.transform.localScale.x * 1.2f, VibrationTime);
        vibration = true;
    }

    private void DoMove()
    {
        Vector3 pos = m_EndItem.transform.position;
        pos -= new Vector3(0, 0, 2f);
        transform.DOMove(pos, m_Time);

        tweenScale.ResetToBeginning();
        tweenScale.PlayForward();
    }

    private void ReStore()
    {
        vibration = false;
        m_StartItem = null;
        m_EndItem = null;
        ObjectPool.Instance.Restore(this.gameObject);
    }

    private void RePosition()
    {
        Vector3 pos = m_EndItem.transform.position;
        pos.x = m_EndPos.x;
        m_EndItem.transform.position = pos;
        m_EndItem.transform.localScale = Vector3.one;
        m_EndItem.SetActive(true);
    }
}