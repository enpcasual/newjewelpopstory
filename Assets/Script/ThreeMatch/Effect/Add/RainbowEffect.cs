﻿using UnityEngine;
using System.Collections;
using DigitalRuby.LightningBolt;
using DG.Tweening;

public class RainbowEffect : MonoBehaviour {

    public LightningBoltScript m_LightningBolt;
    public LineRenderer m_LineRenderer;
    public ParticleSystem m_Point;

    GameObject m_Start;
    GameObject m_End;

    public void SetInfo(GameObject start, GameObject end, float dis, float start_z = -2, float end_z = -2)
    {
        m_Start = start;
        m_End = end;

        m_LightningBolt.StartObject = m_Start;
        m_LightningBolt.EndObject = m_End;
        m_LightningBolt.StartPosition = new Vector3(0, 0, start_z);
        m_LightningBolt.EndPosition = new Vector3(0, 0, end_z);

        ////columns값은 이미지을 세로로 나눌갯수를 의미한다.
        ////거리가 가까우면 이미지의 잘게 나눠서 사용하도록 한다.
        if (dis < 2)
            m_LightningBolt.Columns = 7;
        else if (dis < 4)
            m_LightningBolt.Columns = 5;
        else if (dis < 6)
            m_LightningBolt.Columns = 3;
        else if (dis < 8)
            m_LightningBolt.Columns = 1;

        m_LightningBolt.Trigger(); //처음에는 호출해줘야 바로 위치 찾아간다. Duration을 0.1 줘서 update가 그만큼 기다린다.

        //point
        m_Point.transform.position = m_End.transform.position + new Vector3(0, 0, -2); 
        m_Point.Play();

        Invoke("ReStore", 1f);

        //
        Line_Alpha = 0;
        m_LineRenderer.startColor = new Color(1, 1, 1, 1f);
        m_LineRenderer.endColor = new Color(1, 1, 1, 0);
    }

    float Line_Alpha = 0;

    void Update()
    {
        if (m_End != null)
        {
            m_Point.transform.position = m_End.transform.position + new Vector3(0, 0, -2);

            if (Line_Alpha < 1)
            {
                Line_Alpha += Time.deltaTime * 3; //변수 따로 만들필요 없이 시간을 알파값을 사용해버리자.
                m_LineRenderer.startColor = new Color(1, 1, 1, 1 - Line_Alpha);
                m_LineRenderer.endColor = new Color(1, 1, 1, Line_Alpha);
            }
        }
    }

    public void ReStore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }	
}
