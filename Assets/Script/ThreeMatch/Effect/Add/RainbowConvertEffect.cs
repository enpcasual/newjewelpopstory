﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainbowConvertEffect : MonoBehaviour
{

    [Flags]
    public enum EffectOptions
    {
        None = 0,
        Vibration_Start = 1,
        Vibration_End = 2,
        Scale = 4,
    }

    public float VibrationTime = 0.9f;

    [SerializeField]
    private GameObject Start = null;

    [SerializeField]
    private GameObject End = null;

    [SerializeField]
    private float RunTime = 0;

    [SerializeField]
    private ParticleSystem WorldModule;



    private Vector3 StartPosition = Vector3.zero;
    private Vector3 EndPosition = Vector3.zero;

    private Board m_Board;

    private Action CompleteAction = null;

    public void SetInfo(GameObject start, GameObject end, float time, ColorType colorType = ColorType.Rnd,List<EffectOptions> options = null, Action onComplete = null)
    {
        CompleteAction = onComplete;
        start.transform.position += new Vector3(0, 0, -0.15f);

        Start = start;
        End = end;
        RunTime = time;

        StartPosition = Start.transform.position;
        EndPosition = End.transform.position;

        transform.position = Start.transform.position;
        //m_LightningBolt.Trigger(); //처음에는 호출해줘야 바로 위치 찾아간다. Duration을 0.1 줘서 update가 그만큼 기다린다.

        if (colorType != ColorType.Rnd && WorldModule.textureSheetAnimation.enabled)
        {
            var curve = new ParticleSystem.MinMaxCurve(((int)colorType - 1) / 6f);

            var module = WorldModule.textureSheetAnimation;
            module.startFrame = curve;
        }
        else if (WorldModule.textureSheetAnimation.enabled)
        {
            var curve = new ParticleSystem.MinMaxCurve(0, 1);

            var module = WorldModule.textureSheetAnimation;
            module.startFrame = curve;
        }

        Invoke("ReStore", RunTime + 0.3f); // 0.4초 동안 폭발 임팩트가 있음.
        Invoke("RePosition", RunTime); // enditem 포지션x값을 초기화.
        //Invoke("DoMove", 0.1f); // 0.1초 후에 무브를 시작해야 정확한 위치로 이동이 가능
        if (VibrationTime > time)
            VibrationTime = time;

        if (options != null && options.Contains(EffectOptions.Vibration_Start))
        {
            Invoke("OnVibration_Start", VibrationTime); //진동을 시작해야하는 시간.
        }

        if (options != null && options.Contains(EffectOptions.Vibration_End))
        {
            Invoke("OnVibration_End", VibrationTime); //진동을 시작해야하는 시간.
        }

        DoMove();
        WorldModule.Play();
    }

    bool vibration = false;
    bool on = true;
    bool vibration_End = false;
    bool vibration_Start = false;
    //타겟이 된 오브젝트를 좌우로 흔듬.
    private void Update()
    {
        if (Start != null && End != null)
        {
            if (vibration)
            {
                if (vibration_End)
                {
                    End.transform.position += new Vector3(on ? 0.05f : -0.05f, 0, 0);
                }
                if (vibration_Start)
                {
                    Start.transform.position += new Vector3(on ? 0.05f : -0.05f, 0, 0);
                }
            }

            on = !on;
        }
    }

    private void OnVibration_Start()
    {
        Start.transform.DOScale(Start.transform.localScale.x * 1.2f, VibrationTime);
        vibration_Start = true;
        vibration = true;
    }

    private void OnVibration_End()
    {
        End.transform.DOScale(End.transform.localScale.x * 1.2f, VibrationTime);
        vibration_End = true;
        vibration = true;
    }


    private void DoMove()
    {
        Vector3 pos = End.transform.position;
        pos -= new Vector3(0, 0, 0.5f);
        transform.DOMove(pos, RunTime).OnComplete(() => { if (CompleteAction != null) CompleteAction.Invoke(); });
    }

    private void ReStore()
    {
        vibration = false;
        Start = null;
        End = null;
        ObjectPool.Instance.Restore(this.gameObject);
    }

    private void RePosition()
    {
        Vector3 pos_e = End.transform.position;
        pos_e.x = EndPosition.x;

        End.transform.position = pos_e;
        if (vibration)
            End.transform.localScale = Vector3.one;
        //End.SetActive(true);

        Vector3 pos_s = Start.transform.position;
        pos_s.x = StartPosition.x;

        Start.transform.position = pos_s;
        if (vibration)
            Start.transform.localScale = Vector3.one;
        //Start.SetActive(true);

    }
}