﻿using UnityEngine;
using System.Collections;
using DigitalRuby.LightningBolt;
using DG.Tweening;

public class GhostEffect : MonoBehaviour {
    
    public void SetInfo(GameObject start, GameObject end, float dis, float start_z = -2, float end_z = -2)
    {
        transform.position = start.transform.position;
        Vector3 endpos = end.transform.position + new Vector3(0, 0, -2);
        transform.DOMove(endpos, 1.0f);
        Invoke("ReStore", 1f);
    }

    void Update()
    {
        //m_Point.transform.position = m_End.transform.position + new Vector3(0, 0, -2);
    }

    public void ReStore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }	
}
