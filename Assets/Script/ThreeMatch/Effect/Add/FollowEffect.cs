﻿using UnityEngine;
using System.Collections;

public class FollowEffect : MonoBehaviour {

    public GameObject m_Target;	
    public Vector3 m_Offset = Vector3.zero;
	// Update is called once per frame
    public void Init(GameObject target, Vector3 offset)
    {
        m_Target = target;
        m_Offset = offset;
    }

	void Update () {
        //아 말도 안되는 버그네.. 이것 때문에 화병나겠네.. 
        //문제가 일단 아이템에 Brust될때 화면에 안보이도록 좌표를 저 멀리 보내버리는게 있는데(99,99,99)
        //이펙트가 아이템 자식으로 붙어있던 이렇게 좌표를 옮겨주던 일단 그렇게 멀리가버리면
        //그다음부터 파티클시스템이 멍통이 되버린다..욕나오네. 이것때문에 개고생했네..
        //근데 이유를 모르겠고 일단 99로 보내기 때문에 대충 20이하일때만 따라가도록 조건 붙여놓음.
        if (m_Target != null && m_Target.transform.position.y < 20) 
            transform.position = m_Target.transform.position + m_Offset;           
	}
}
