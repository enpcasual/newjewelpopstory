﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ParticleManager : MonoBehaviour
{

    public List<ParticleSystem> m_ListParticle;
    [HideInInspector]
    public bool m_One_MatSystem = true;

    [HideInInspector]
    public int m_ImageTotalCnt = 0;
    [HideInInspector]
    public int m_ImageBundleCnt = 0;    //낱개로 사용하려면 값을 1주면된다.
    [HideInInspector]
    public bool m_ImageRandom = false;  //여러이미지를 출력할것인가?(파티클을 한개만 사용하기 위해)
    [HideInInspector]
    public int m_ImageRandomCnt = 0;


    float DestroyItem = 0;
    float particletime = 0;
    ParticleSystem.TextureSheetAnimationModule tex;
    ParticleSystem.MainModule Main;
    float constant = 0;


    //texindex : 이미지 인덱스. 단m_ImageBundleCnt값(묶음단위)에 따라 곱하기로 계산되어 적용된다.
    //texRandom : 묵음단위의 이미지를 랜덤하게 출력하고자 할때 사용한다. 즉, 파티클 한개로 여러이미지를 출려할때 사용한다.
    public void Play(Vector3 pos, int tex_index = 0, float z = -2)
    {
        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if(m_ListParticle[i] == null) continue;
            m_ListParticle[i].Stop();
        }

        transform.position = pos + new Vector3(0, 0, z);

        if (m_One_MatSystem)
        {
            if (m_ListParticle.Count == 0 || m_ImageTotalCnt <= 0 || m_ImageBundleCnt <= 0)
            {
                Debug.LogError("EffectManager가 세팅되지 않았습니다. : " + gameObject.name);
            }
            else
            {
                float uv = 1f / m_ImageTotalCnt;  //이미지의 간격? 이미지의 위치값을 구하기 위해서. 0~1사이

                int frameStart = tex_index * m_ImageBundleCnt; //이미지 시작 인덱스 설정

                for (int i = 0; i < m_ListParticle.Count; i++)
                {
                    if (m_ListParticle[i] == null) continue;

                    tex = m_ListParticle[i].textureSheetAnimation;

                    if (m_ImageRandom)
                    {
                        constant = uv * (frameStart + m_ImageRandomCnt);
                        if (m_ImageTotalCnt < frameStart + m_ImageRandomCnt)
                        {
                            constant = uv * frameStart;
                            Debug.LogError("이펙트 이미지의 갯수를 벗어난 범위가 입력됨." + gameObject.name);
                        }

                        tex.frameOverTime = new ParticleSystem.MinMaxCurve(uv * frameStart, constant); //랜덤 최소값(포함됨), 랜덤 최대값(포함안됨)
                    }
                    else
                    {
                        constant = uv * (frameStart + i);
                        if (m_ImageTotalCnt < frameStart + i)
                        {
                            constant = i;
                            Debug.LogError("이펙트 index보다 이미지가 적다!" + gameObject.name);
                        }

                        tex.frameOverTime = new ParticleSystem.MinMaxCurve(constant);
                    }
                }
            }
        }

        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if (m_ListParticle[i] == null) continue;

            m_ListParticle[i].Play();
        }

        DestroyItem = 0;
        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if (m_ListParticle[i] == null) continue;

            particletime = m_ListParticle[i].duration + m_ListParticle[i].startLifetime;
            if (DestroyItem < particletime)
                DestroyItem = particletime;
        }

        Invoke("Restore", DestroyItem);
    }

    public void Play(Vector3 pos, Color color, float z = -2)
    {
        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if (m_ListParticle[i] == null) continue;
            m_ListParticle[i].Stop();
        }

        transform.position = pos + new Vector3(0, 0, z);


        if (m_ListParticle.Count == 0)
        {
            Debug.LogError("EffectManager가 세팅되지 않았습니다. : " + gameObject.name);
        }
        else
        {
            for (int i = 0; i < m_ListParticle.Count; i++)
            {
                if (m_ListParticle[i] == null) continue;

                //SheetAnim
                tex = m_ListParticle[i].textureSheetAnimation;
                tex.enabled = false;    //만약 켜져있으면 꺼주자. 이미지는 한장일테니까.

                //MainModule
                Main = m_ListParticle[i].main;
                color.a = Main.startColor.color.a;
                Main.startColor = color;    //효과마다 컬러가 동일하게 적용될수 없을꺼 같다. 그냥 컬러는 넘겨받는다.             
            }
        }

        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if (m_ListParticle[i] == null) continue;

            m_ListParticle[i].Play();
        }

        DestroyItem = 0;
        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            if (m_ListParticle[i] == null) continue;

            particletime = m_ListParticle[i].duration + m_ListParticle[i].startLifetime;
            if (DestroyItem < particletime)
                DestroyItem = particletime;
        }

        Invoke("Restore", DestroyItem);
    }


    #region 속성 변경용 추가 함수
    public void SetShapeBox(Vector3 box)
    {
        for (int i = 0; i < m_ListParticle.Count; i++)
        {
            var sh = m_ListParticle[i].shape;
            sh.scale = box;
        }
    }
    #endregion

    public void Restore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }
}
