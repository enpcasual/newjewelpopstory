﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class EffectManager : MonoBehaviour
{
    static public EffectManager Instance;

    MatchManager matchMgr;

    //*****************************
    //이펙트도 순서가 있다.
    //z값 -1까지는 Panel공간이다.
    //*****************************

    //기본 이펙트(예전구현방식_정리안됨)
    public GameObject PF_DefaultEffect;     //1

    public GameObject PF_DefaultPiece;      //2 기본 보석 이펙트

    //아이템 이펙트
    public GameObject PF_BombEffect;        //2
    public GameObject PF_BombBigEffect;     //2
    public GameObject PF_SpiralEffect;      //2
    public GameObject PF_DonutEffect;       //2
    public GameObject PF_TimeBombEffect;    //2
    public GameObject PF_CashItemEffect;    //2
    public GameObject PF_RainbowEffect;     //2
    public GameObject PF_RainbowMoveEffect;   //2
    public GameObject PF_RainbowConvertEffect;   //2
    public GameObject PF_GhostEffect;     //2
    public GameObject PF_SmokeEffect;       //2
    public GameObject PF_ButterflyEffect;   //나비와 같은 Z값. 자식으로 붙여서 LocalPosion을 zero줌


    //패널 이펙트
    public GameObject PF_WaferEffect;       //2
    public GameObject PF_BreadEffect;       //2
    public GameObject PF_LollyCageEffect;   //2
    public GameObject PF_IceCageEffect;     //2
    public GameObject PF_IceCreamEffect;    //2
    public GameObject PF_CakeEffect;        //2
    public GameObject PF_JamEffect;         //2
    public GameObject PF_CrackerEffect;     //2
    public GameObject PF_BottleEffect;      //2
    public GameObject PF_BottleShine;       //2
    public GameObject PF_MagicColorEffect;  //2
    public GameObject PF_S_TreeEffect;      //2
    public GameObject PF_JewelTreeEffect;   //2
    public GameObject PF_SteleHideEffect;   //2


    //Line 이펙트
    public GameObject PF_LineEffect;        //※
    public GameObject PF_TwinLineEffect;        //※
    public GameObject PF_BigShotEffect;     //2 큰 미사일 커져서 깨질때 나오는이펙트

    //기타 이펙트
    public GameObject PF_ItemChangeEffect;  //2
    public GameObject PF_BonusItemChangeEffect;  //2
    public GameObject PF_SignEffect;        //2 Bomb, Rainbow 전조현상 이펙트
    public GameObject PF_BigBrustEffect;    //2 타임폭탄이 터져서 게임 끝날때
    public GameObject PF_RainbowBigBrustEffect;    //2 레인보우X2 터질때
    public GameObject PF_RainbowBurstEffect;       //2 레인보우 버스트

    public GameObject PF_CashItem1;       //케쉬 아이템1 사용
    public GameObject PF_CashItem1_Sun;       //케쉬 아이템1 사용
    public GameObject PF_CashItem2;       //케쉬 아이템2 사용

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        matchMgr = MatchManager.Instance;

        //*****************************
        //UI쪽에서 사용하는 ObjectPool
        //*****************************
        ObjectPool.Instance.CreatePool(PF_DefaultEffect, 50);
    }

    #region DefaultEffect
    public ItemEffect GetDefaultEffect()
    {
        return ObjectPool.Instance.GetObject<ItemEffect>(PF_DefaultEffect, matchMgr.GameField.transform);
    }

    public void PieceEffect(Vector3 pos, ColorType color)
    {
        //조각
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_DefaultPiece, matchMgr.GameField.transform);
        obj.Play(pos, (int)color - 1);
    }
    #endregion

    #region ItemEffect
    public void BombEffect(Vector3 pos, ColorType color)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BombEffect, matchMgr.GameField.transform);
        Color _color = Color.white;
        switch (color)
        {
            case ColorType.None:
                //보너스 폭탄, 타임폭탄에서 여기를 탄다.
                _color = Color.white;
                break;
            case ColorType.RED:
                _color = new Color32(250, 100, 100, 255);
                break;
            case ColorType.YELLOW:
                _color = new Color32(200, 200, 50, 255);
                break;
            case ColorType.GREEN:
                _color = new Color32(100, 250, 100, 255);
                break;
            case ColorType.BLUE:
                _color = new Color32(100, 100, 250, 255);
                break;
            case ColorType.PURPLE:
                _color = new Color32(150, 100, 200, 255);
                break;
            case ColorType.ORANGE:
                _color = new Color32(250, 150, 50, 255);
                break;
            default:
                Debug.LogError("여기를 타는건 문제 있음!!");
                break;
        }
        obj.Play(pos, 0);
    }
    public void BombBigEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BombBigEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void SpiralEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_SpiralEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void DonutEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_DonutEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void TimeBombEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_TimeBombEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public RainbowEffect GetRainbowEffect()
    {
        return ObjectPool.Instance.GetObject<RainbowEffect>(PF_RainbowEffect, matchMgr.GameField.transform);
    }

    public RainbowMoveEffect GetRainbowMoveEffect()
    {
        return ObjectPool.Instance.GetObject<RainbowMoveEffect>(PF_RainbowMoveEffect, matchMgr.GameField.transform);
    }
    public RainbowConvertEffect GetRainbowConvertEffect()
    {
        RainbowConvertEffect efc = ObjectPool.Instance.GetObject<RainbowConvertEffect>(PF_RainbowConvertEffect, matchMgr.GameField.transform);

        return efc;
    }
    public GhostEffect GetGhostEffect()
    {
        return ObjectPool.Instance.GetObject<GhostEffect>(PF_GhostEffect, matchMgr.GameField.transform);
    }
    public void CashItemEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_CashItemEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void SmokeEffect(Vector3 pos)
    {
        ParticleManager efc = ObjectPool.Instance.GetObject<ParticleManager>(PF_SmokeEffect, matchMgr.GameField.transform);
        efc.Play(pos, 0);
    }
    public void ButterflyEffect(Transform parent, ColorType color)
    {
        ParticleManager efc = ObjectPool.Instance.GetObject<ParticleManager>(PF_ButterflyEffect, matchMgr.GameField.transform);

        efc.Play(Vector3.zero, GetColor(color));
        efc.transform.parent = parent;
        efc.transform.localPosition = Vector3.zero;
    }
    #endregion

    #region PanelEffect
    public void WaferEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_WaferEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }  
    public void BreadEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BreadEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void LollyCageEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_LollyCageEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void IceCageEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_IceCageEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void IceCreamEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_IceCreamEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void CakeEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_CakeEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void JamEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_JamEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void CrackerEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_CrackerEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    public void BottleEffect(Vector3 pos, int index)
    {
        ParticleManager obj_top;
        ParticleManager obj_bottom;

        //top.상단2장은 겹마다 바뀐
        //bottom.하단2장은 한겹일때만 다름
        obj_top = ObjectPool.Instance.GetObject<ParticleManager>(PF_BottleEffect, matchMgr.GameField.transform);
        obj_top.Play(pos, index);
        obj_bottom = ObjectPool.Instance.GetObject<ParticleManager>(PF_BottleEffect, matchMgr.GameField.transform);
        if (index == 0)
            obj_bottom.Play(pos, 2);
        else
            obj_bottom.Play(pos, 3);

    }
    public ParticleManager BottleShine(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BottleShine, matchMgr.GameField.transform);
        obj.Play(pos);

        return obj;
    }
    public void MagicColorEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_MagicColorEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void S_TreeEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_S_TreeEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void JewelTreeEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_JewelTreeEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    public void SteleHideEffect(Vector3 pos, int index)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_SteleHideEffect, matchMgr.GameField.transform);
        obj.Play(pos, index);
    }
    #endregion


    #region LineEffect
    public LineEffect GetLineEffect()
    {
        return ObjectPool.Instance.GetObject<LineEffect>(PF_LineEffect, matchMgr.GameField.transform);
    }

    public TwinLineEffect GetTwinLineEffect()
    {
        return ObjectPool.Instance.GetObject<TwinLineEffect>(PF_TwinLineEffect, matchMgr.GameField.transform);
    }

    public void BigShotEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BigShotEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }
    #endregion

    #region Etceffect
    public void ItemChangeEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_ItemChangeEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }

    public void BonusItemChangeEffect(Vector3 pos)
    {
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BonusItemChangeEffect, matchMgr.GameField.transform);
        obj.Play(pos, 0);
    }

    public void SignEffect(SpriteRenderer spr, float time)
    {
        SignEffect efc = ObjectPool.Instance.GetObject<SignEffect>(PF_SignEffect, matchMgr.GameField.transform);
        efc.Play(spr, time);
    }
    public void BigBrustEffect(Vector3 pos)
    {
        ParticleManager efc = ObjectPool.Instance.GetObject<ParticleManager>(PF_BigBrustEffect, matchMgr.GameField.transform);
        efc.Play(pos, 0);
    }
    public void RainbowBigBrustEffect(Vector3 pos)
    {
        ParticleManager efc = ObjectPool.Instance.GetObject<ParticleManager>(PF_RainbowBigBrustEffect, matchMgr.GameField.transform);
        efc.Play(pos, 0);
    }
    public void RainbowBurstEffect(Vector3 pos)
    {
        ParticleManager efc = ObjectPool.Instance.GetObject<ParticleManager>(PF_RainbowBurstEffect, matchMgr.GameField.transform);
        efc.Play(pos, 0);
    }

    public void JellyMonJumpEffect(Vector3 pos, float time = 0.1f)
    {
        DelayPooling efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem1, matchMgr.GameField.transform);
        Vector3 endPos = pos - new Vector3(0, -7, 0);
        efc.transform.position = pos - new Vector3(0, 1, 0);
        efc.transform.DOMove(endPos, time);
    }

    public void CashItem1Effect(Vector3 pos, float time = 0.1f)
    {
        DelayPooling efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem1, matchMgr.GameField.transform);
        Vector3 endPos = pos;
        efc.transform.position = endPos - new Vector3(0, -7, 0);
        efc.transform.DOMove(endPos, time);
        efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem1_Sun, matchMgr.GameField.transform);
        pos.y = 2;
        efc.transform.position = pos;
    }

    public void CashItem2Effect(Vector3 pos)
    {
        DelayPooling efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem2, matchMgr.GameField.transform);
        Vector3 endPos = pos - new Vector3(-7, 0, 0);
        efc.transform.position = pos;
        efc.transform.DOMove(endPos, 1.0f);

        efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem2, matchMgr.GameField.transform);
        efc.transform.position = pos;
        endPos = pos - new Vector3(7, 0, 0);
        efc.transform.DOMove(endPos, 1.0f);

        efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem2, matchMgr.GameField.transform);
        efc.transform.position = pos;
        endPos = pos - new Vector3(0, 7, 0);
        efc.transform.DOMove(endPos, 1.0f);

        efc = ObjectPool.Instance.GetObject<DelayPooling>(PF_CashItem2, matchMgr.GameField.transform);
        efc.transform.position = pos;
        endPos = pos - new Vector3(0, -7, 0);
        efc.transform.DOMove(endPos, 1.0f);
    }
    #endregion




    public static Color GetColor(ColorType color)
    {
        Color _color = Color.white;
        switch (color)
        {
            case ColorType.None:
                //사용하지 않음. 나중에 흰색으로 사용할일이 있을때,.
                break;
            case ColorType.RED:
                if (!ColorUtility.TryParseHtmlString("#FF3737FF", out _color))
                {
                    _color = new Color32(253, 98, 101, 255);
                }
                break;
            case ColorType.YELLOW:
                if (!ColorUtility.TryParseHtmlString("#FFD939FF", out _color))
                {
                    _color = new Color32(250, 209, 36, 255);
                }
                break;
            case ColorType.GREEN:
                if (!ColorUtility.TryParseHtmlString("#6DFF37FF", out _color))
                {
                    _color = new Color32(89, 223, 40, 255);
                }
                break;
            case ColorType.BLUE:
                if (!ColorUtility.TryParseHtmlString("#1EBEFFFF", out _color))
                {
                    _color = new Color32(0, 185, 227, 255);
                }
                break;
            case ColorType.PURPLE:
                if (!ColorUtility.TryParseHtmlString("#BD37FFFF", out _color))
                {
                    _color = new Color32(243, 97, 239, 255);
                }
                break;
            case ColorType.ORANGE:
                if (!ColorUtility.TryParseHtmlString("#FF9F26FF", out _color))
                {
                    _color = new Color32(255, 167, 9, 255);
                }
                break;
            default:
                Debug.LogError("여기를 타는건 문제 있음!!");
                break;
        }
        return _color;
    }

    //#region UGUIEffect
    //public void ScoreTextEffect(int score, ColorType type, Vector3 pos)
    //{
    //    Text text = ObjectPool.Instance.GetObject<Text>(PF_ScoreText, matchMgr.m_Canvas.transform);

    //    text.text = score.ToString();
    //    //switch (type)
    //    //{
    //    //    case ColorType.ORANGE:
    //    //        text.color = new Color32(255, 124, 31, 255);
    //    //        break;
    //    //    case ColorType.RED:
    //    //        text.color = new Color32(255, 40, 31, 255);
    //    //        break;
    //    //    case ColorType.YELLOW:
    //    //        text.color = new Color32(255, 228, 29, 255);
    //    //        break;
    //    //    case ColorType.PURPLE:
    //    //        text.color = new Color32(225, 31, 253, 255);
    //    //        break;
    //    //    case ColorType.BLUE:
    //    //        text.color = new Color32(31, 159, 255, 255);
    //    //        break;
    //    //    case ColorType.GREEN:
    //    //        text.color = new Color32(74, 250, 33, 255);
    //    //        break;
    //    //    default:
    //    //        text.color = Color.white;
    //    //        break;
    //    //}
    //    text.color = Color.white;

    //    text.transform.position = pos + new Vector3(0, 0, -0.1f);
    //    text.transform.localScale = Vector3.zero; //아래 트윈에서 스케일준다.

    //    Sequence seq = DOTween.Sequence();
    //    seq.Append(text.transform.DOScale(1.2f, 0.5f).SetEase(Ease.OutCubic));
    //    seq.Append(text.transform.DOScale(1f, 0.3f).SetEase(Ease.Linear));
    //    seq.AppendInterval(0.2f);
    //    seq.Insert(0.7F, DOTween.ToAlpha(() => text.color, _color => text.color = _color, 0f, 0.3F));
    //    seq.OnComplete(() =>
    //    {
    //        ObjectPool.Instance.Restore(text.gameObject);
    //    });
    //}
    //#endregion
}

public class Bezier
{

    public Vector3 p0;
    public Vector3 p1;
    public Vector3 p2;
    public Vector3 p3;

    public float ti = 0f;

    private Vector3 b0 = Vector3.zero;
    private Vector3 b1 = Vector3.zero;
    private Vector3 b2 = Vector3.zero;
    private Vector3 b3 = Vector3.zero;

    private float Ax;
    private float Ay;
    private float Az;

    private float Bx;
    private float By;
    private float Bz;

    private float Cx;
    private float Cy;
    private float Cz;

    // Init function v0 = 1st point, v1 = handle of the 1st point , v2 = handle of the 2nd point, v3 = 2nd point
    // handle1 = v0 + v1
    // handle2 = v3 + v2
    public Bezier(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
    {
        this.p0 = v0;
        this.p1 = v1;
        this.p2 = v2;
        this.p3 = v3;
    }
    public Bezier()
    {
    }

    // 0.0 >= t <= 1.0
    public Vector3 GetPointAtTime(float t)
    {
        this.CheckConstant();
        float t2 = t * t;
        float t3 = t * t * t;
        float x = this.Ax * t3 + this.Bx * t2 + this.Cx * t + p0.x;
        float y = this.Ay * t3 + this.By * t2 + this.Cy * t + p0.y;
        float z = this.Az * t3 + this.Bz * t2 + this.Cz * t + p0.z;
        return new Vector3(x, y, z);

    }

    private void SetConstant()
    {
        this.Cx = 3f * ((this.p0.x + this.p1.x) - this.p0.x);
        this.Bx = 3f * ((this.p3.x + this.p2.x) - (this.p0.x + this.p1.x)) - this.Cx;
        this.Ax = this.p3.x - this.p0.x - this.Cx - this.Bx;

        this.Cy = 3f * ((this.p0.y + this.p1.y) - this.p0.y);
        this.By = 3f * ((this.p3.y + this.p2.y) - (this.p0.y + this.p1.y)) - this.Cy;
        this.Ay = this.p3.y - this.p0.y - this.Cy - this.By;

        this.Cz = 3f * ((this.p0.z + this.p1.z) - this.p0.z);
        this.Bz = 3f * ((this.p3.z + this.p2.z) - (this.p0.z + this.p1.z)) - this.Cz;
        this.Az = this.p3.z - this.p0.z - this.Cz - this.Bz;

    }

    // Check if p0, p1, p2 or p3 have changed
    private void CheckConstant()
    {
        if (this.p0 != this.b0 || this.p1 != this.b1 || this.p2 != this.b2 || this.p3 != this.b3)
        {
            this.SetConstant();
            this.b0 = this.p0;
            this.b1 = this.p1;
            this.b2 = this.p2;
            this.b3 = this.p3;
        }
    }
    static public Vector2 Bezier2(Vector2 Start, Vector2 Control, Vector2 End, float t)
    {
        return (((1 - t) * (1 - t)) * Start) + (2 * t * (1 - t) * Control) + ((t * t) * End);
    }
    static public Vector3 Bezier2(Vector3 Start, Vector3 Control, Vector3 End, float t)
    {
        return (((1 - t) * (1 - t)) * Start) + (2 * t * (1 - t) * Control) + ((t * t) * End);
    }
    static public Vector2 Bezier3(Vector2 s, Vector2 st, Vector2 et, Vector2 e, float t)
    {
        return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
    }
    static public Vector3 Bezier3(Vector3 s, Vector3 st, Vector3 et, Vector3 e, float t)
    {
        return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
    }
}

