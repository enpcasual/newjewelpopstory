﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemEffect : MonoBehaviour {

    //터치
    public ParticleSystem touch;

    //스위칭
    public ParticleSystem Switching_Circle;
    public ParticleSystem Switching_Shine;


    //보너스타임//아 그러네.. 이부분 일단 Upper카메라로 보도록 세팅.
    //public SpriteRenderer BonusMove_Star;
    //public ParticleSystem BonusMove_Circle;

    //NormalItem Brust
    public ParticleSystem brustRing;
    public ParticleSystem Piece;
    public ParticleSystem Powder;
    public ParticleSystem Symbol;
    public ParticleSystem Flash;


    //리소스
    public List<Material> m_ListPieceMat = new List<Material>();
    public List<Color> m_ListColor = new List<Color>();

    public void Reset(Vector3 pos)
    {
        transform.position = pos;

        //터치
        touch.Stop();

        //스위칭
        Switching_Circle.Stop();
        Switching_Shine.Stop();

        //Normal Brust
        brustRing.Stop();
        Piece.Stop();
        Powder.Stop();
        Flash.Stop();
    }

    public void TouchEffect(GameObject target)
    {
        Reset(target.transform.position + new Vector3(0, 0, -1f));

        touch.Play();
        Invoke("Restore", 1f);
    }

    public void SwitchEffect(GameObject target)
    {
        Reset(target.transform.position + new Vector3(0, 0, -1f));

        //원이펙트
        //switching.transform.localPosition = Vector3.zero;
        //switching.GetComponent<FollowEffect>().Init(target, new Vector3(0,0, 0.5f));
        //switching.Play();

        //별이펙트
        Switching_Shine.Play();

        Invoke("Restore", 1f);
    }

    public void DefaultBrustEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1f));

        //1.링
        var ring_main = brustRing.main;
        ring_main.startSize = 1.2f;
        SetColor(ring_main, EffectManager.GetColor(color));
        brustRing.Play();

        //2.조각
        //3.별
        //4.스모크
        //switch (color)
        //{
        //    case ColorType.ORANGE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[0];
        //        Powder.startColor = m_ListColor[0];
        //        break;
        //    case ColorType.RED:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[1];
        //        Powder.startColor = m_ListColor[1];
        //        break;
        //    case ColorType.YELLOW:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[2];
        //        Powder.startColor = m_ListColor[2];
        //        break;
        //    case ColorType.PURPLE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[3];
        //        Powder.startColor = m_ListColor[3];
        //        break;
        //    case ColorType.BLUE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[4];
        //        Powder.startColor = m_ListColor[4];
        //        break;
        //    case ColorType.GREEN:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[5];
        //        Powder.startColor = m_ListColor[5];
        //        break;
        //    default:
        //        break;
        //}

        //Piece.Play();

        SetColor(Powder.main, EffectManager.GetColor(color));
        Powder.Play();

        //5. 스파크
        SetColor(Flash.main, EffectManager.GetColor(color));
        Flash.Play();

        Invoke("Restore", 1.5f);
    }



    private void SetColor(ParticleSystem.MainModule target, Color targetColor)
    {
        targetColor.a = target.startColor.color.a;
        target.startColor = targetColor;
    }
    public void RainbowBrustEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1f));

        //1.링
        var ring_main = brustRing.main;
        ring_main.startSize = 1.5f;
        SetColor(ring_main, EffectManager.GetColor(color));
        brustRing.Play();

        //2.조각
        //3.별
        //4.스모크
        //switch (color)
        //{
        //    case ColorType.ORANGE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[0];
        //        Powder.startColor = m_ListColor[0];
        //        break;
        //    case ColorType.RED:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[1];
        //        Powder.startColor = m_ListColor[1];
        //        break;
        //    case ColorType.YELLOW:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[2];
        //        Powder.startColor = m_ListColor[2];
        //        break;
        //    case ColorType.PURPLE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[3];
        //        Powder.startColor = m_ListColor[3];
        //        break;
        //    case ColorType.BLUE:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[4];
        //        Powder.startColor = m_ListColor[4];
        //        break;
        //    case ColorType.GREEN:
        //        Piece.GetComponent<Renderer>().material = m_ListPieceMat[5];
        //        Powder.startColor = m_ListColor[5];
        //        break;
        //    default:
        //        break;
        //}

        //Piece.Play();

        //SetColor(Powder.main, EffectManager.GetColor(color));
        //Powder.Play();

        //.심볼
        var curve = new ParticleSystem.MinMaxCurve(((int)color - 1) / 6f);

        var module = Symbol.textureSheetAnimation;
        module.startFrame = curve;

        Symbol.Play();

        //5. 스파크
        SetColor(Flash.main, EffectManager.GetColor(color));
        Flash.Play();

        Invoke("Restore", 1.5f);
    }

    public void Restore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }
}
