﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class TwinLineEffect : MonoBehaviour
{

    public List<Sprite> m_ListLineSpr = new List<Sprite>();
    public List<Sprite> m_ListLineShotSpr = new List<Sprite>();
    public List<Sprite> m_ListLineBigSpr = new List<Sprite>();

    //Liine Brust
    public SpriteRenderer LineX;
    private SpriteRenderer LineX_Alpha;
    private SpriteRenderer LineX_Beta;

    public SpriteRenderer LineY;
    private SpriteRenderer LineY_Alpha;
    private SpriteRenderer LineY_Beta;

    public SpriteRenderer LineCL;
    private SpriteRenderer LineCL_Alpha;
    private SpriteRenderer LineCL_Beta;

    public SpriteRenderer LineCR;
    private SpriteRenderer LineCR_Alpha;
    private SpriteRenderer LineCR_Beta;

    public SpriteRenderer LineShot1;
    public SpriteRenderer LineShot2;
    public SpriteRenderer LineShot3;
    public SpriteRenderer LineShot4;

    public ParticleSystem LineFollow1;
    public ParticleSystem LineFollow2;
    public ParticleSystem LineFollow3;
    public ParticleSystem LineFollow4;

    //CombineBrust
    public GameObject LineBig_Sprite;
    public SpriteRenderer LineBig_Sprite1;
    public SpriteRenderer LineBig_Sprite2;
    public SpriteRenderer LineShotBig1;
    public SpriteRenderer LineShotBig2;
    public SpriteRenderer LineShotBig3;
    public SpriteRenderer LineShotBig4;

    [SerializeField]
    private Color InitializedColor;

    #region 라인 이펙트
    //라인 기본이펙트
    float scale_x = 1.5f;
    float scale_y = 30f;
    float scale_time = 1.1f;
    float scale_y_bonus = 5f;
    float scale_time_bonus = 0.45f;
    float scale_up_time = 0.45f;

    float move_amount = 0.3f;

    Dictionary<SpriteRenderer, List<SpriteRenderer>> SameSpritesDict = new Dictionary<SpriteRenderer, List<SpriteRenderer>>();

    private void Awake()
    {
        LineX_Alpha = LineX.transform.GetChild(0).GetComponent<SpriteRenderer>();
        LineX_Beta = LineX.transform.GetChild(1).GetComponent<SpriteRenderer>();
        SameSpritesDict.Add(LineX, new List<SpriteRenderer>() { LineX, LineX_Alpha, LineX_Beta });

        LineY_Alpha = LineY.transform.GetChild(0).GetComponent<SpriteRenderer>();
        LineY_Beta = LineY.transform.GetChild(1).GetComponent<SpriteRenderer>();
        SameSpritesDict.Add(LineY, new List<SpriteRenderer>() { LineY, LineY_Alpha, LineY_Beta });

        LineCL_Alpha = LineCL.transform.GetChild(0).GetComponent<SpriteRenderer>();
        LineCL_Beta = LineCL.transform.GetChild(1).GetComponent<SpriteRenderer>();
        SameSpritesDict.Add(LineCL, new List<SpriteRenderer>() { LineCL, LineCL_Alpha, LineCL_Beta });

        LineCR_Alpha = LineCR.transform.GetChild(0).GetComponent<SpriteRenderer>();
        LineCR_Beta = LineCR.transform.GetChild(1).GetComponent<SpriteRenderer>();
        SameSpritesDict.Add(LineCR, new List<SpriteRenderer>() { LineCR, LineCR_Alpha, LineCR_Beta });
    }

    private void SetSprite(SpriteRenderer target, Sprite sprite)
    {
        var e = SameSpritesDict[target].GetEnumerator();
        while (e.MoveNext())
        {
            e.Current.sprite = sprite;
        }
    }

    private void SetColor(SpriteRenderer target, Color color)
    {
        var e = SameSpritesDict[target].GetEnumerator();
        while (e.MoveNext())
        {
            e.Current.color = color;
        }
    }

    public void Reset(Vector3 pos)
    {
        transform.position = pos;
    }

    public void LineXEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                return;
                _spr = m_ListLineSpr[Random.Range(0, 6)];
                break;
            case ColorType.RED:
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }

        SetSprite(LineX, _spr);
        SetColor(LineX, InitializedColor);
        LineX.transform.localScale = Vector3.one;
        LineX.gameObject.SetActive(true);

        LineX_Alpha.transform.localPosition = Vector3.zero;
        LineX_Beta.transform.localPosition = Vector3.zero;

        Sequence seq = DOTween.Sequence();
        seq.Append(LineX.transform.DOScaleY(scale_y, scale_time).SetEase(Ease.Linear));                        //스케일

        //width
        seq.Insert(0, LineX.transform.DOScaleX(scale_x, scale_up_time).SetEase(Ease.InSine));                        //스케일
        seq.Insert(scale_up_time, LineX.transform.DOScaleX(0, scale_time - scale_up_time).SetEase(Ease.Linear));

        //position
        seq.Insert(0, LineX_Alpha.transform.DOLocalMoveY(-move_amount, scale_time).SetEase(Ease.Linear));
        seq.Insert(0, LineX_Beta.transform.DOLocalMoveY(move_amount, scale_time).SetEase(Ease.Linear));

        //color
        seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineX.color, _color => SetColor(LineX, _color), 0f, scale_time / 2));  //알파값
        seq.OnComplete(() =>
        {
            LineX.transform.localScale = Vector3.zero;

            LineX.gameObject.SetActive(false);
        });

        //미사일..
        LineShotEffect(color, LineType.LineX);

        Invoke("Restore", 1f);
    }

    public void LineYEffect(Vector3 pos, ColorType color)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                return;
                _spr = m_ListLineSpr[Random.Range(0, 6)];
                break;
            case ColorType.RED:
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }

        SetSprite(LineY, _spr);
        SetColor(LineY, InitializedColor);
        LineY.transform.localScale = Vector3.one;
        LineY.gameObject.SetActive(true);

        LineY_Alpha.transform.localPosition = Vector3.zero;
        LineY_Beta.transform.localPosition = Vector3.zero;

        Sequence seq = DOTween.Sequence();
        seq.Append(LineY.transform.DOScaleY(scale_y, scale_time).SetEase(Ease.Linear));                        //스케일

        seq.Insert(0, LineY.transform.DOScaleX(scale_x, scale_up_time).SetEase(Ease.InSine));                        //스케일
        seq.Insert(scale_up_time, LineY.transform.DOScaleX(0, scale_time - scale_up_time).SetEase(Ease.Linear));

        //position
        seq.Insert(0, LineY_Alpha.transform.DOLocalMoveY(-move_amount, scale_time).SetEase(Ease.Linear));
        seq.Insert(0, LineY_Beta.transform.DOLocalMoveY(move_amount, scale_time).SetEase(Ease.Linear));

        seq.Insert(scale_time / 2, DOTween.ToAlpha(() => LineY.color, _color => SetColor(LineY, _color), 0f, scale_time / 2));  //알파값
        seq.OnComplete(() =>
        {
            LineY.transform.localScale = Vector3.zero;

            LineY.gameObject.SetActive(false);
        });

        //미사일..
        LineShotEffect(color, LineType.LineY);

        Invoke("Restore", 1f);
    }

    public void LineCEffect(Vector3 pos, ColorType color, bool bonus = false)
    {
        Reset(new Vector3(pos.x, pos.y, -1.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.None:
                if (!bonus)
                    return;
                _spr = m_ListLineSpr[Random.Range(0, 6)];
                break;
            case ColorType.RED:
                _spr = m_ListLineSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineSpr[5];
                break;
            default:
                break;
        }
        SetSprite(LineCL, _spr);
        SetColor(LineCL, InitializedColor);

        LineCL.transform.localScale = Vector3.one;
        LineCL.gameObject.SetActive(true);

        SetSprite(LineCR, _spr);
        SetColor(LineCR, InitializedColor);
        LineCR.transform.localScale = Vector3.one;
        LineCR.gameObject.SetActive(true);

        LineCR_Alpha.transform.localPosition = Vector3.zero;
        LineCR_Beta.transform.localPosition = Vector3.zero;
        LineCL_Alpha.transform.localPosition = Vector3.zero;
        LineCL_Beta.transform.localPosition = Vector3.zero;

        var scale = bonus ? scale_y_bonus : scale_y;
        var scaleTime = bonus ? scale_time_bonus : scale_time;
        var moveAmount = bonus ? move_amount * 0.5f : move_amount;

        Sequence seq = DOTween.Sequence();
        seq.Append(LineCL.transform.DOScaleY(scale * 1.4f, scaleTime).SetEase(Ease.Linear));                        //스케일
        seq.Insert(0, LineCR.transform.DOScaleY(scale * 1.4f, scaleTime).SetEase(Ease.Linear));                        //스케일

        seq.Insert(0, LineCL.transform.DOScaleX(scale_x, scale_up_time).SetEase(Ease.InSine));                        //스케일
        seq.Insert(scale_up_time, LineCL.transform.DOScaleX(0, scaleTime - scale_up_time).SetEase(Ease.Linear));
        seq.Insert(0, LineCR.transform.DOScaleX(scale_x, scale_up_time).SetEase(Ease.InSine));                        //스케일
        seq.Insert(scale_up_time, LineCR.transform.DOScaleX(0, scaleTime - scale_up_time).SetEase(Ease.Linear));


        seq.Insert(0, LineCR_Alpha.transform.DOLocalMoveY(-moveAmount, scaleTime).SetEase(Ease.Linear));
        seq.Insert(0, LineCR_Beta.transform.DOLocalMoveY(moveAmount, scaleTime).SetEase(Ease.Linear));
        seq.Insert(0, LineCL_Alpha.transform.DOLocalMoveY(-moveAmount, scaleTime).SetEase(Ease.Linear));
        seq.Insert(0, LineCL_Beta.transform.DOLocalMoveY(moveAmount, scaleTime).SetEase(Ease.Linear));


        seq.Insert(scaleTime / 2, DOTween.ToAlpha(() => LineCL.color, _color => SetColor(LineCL, _color), 0f, scaleTime / 2));  //알파값     
        seq.Insert(scaleTime / 2, DOTween.ToAlpha(() => LineCR.color, _color => SetColor(LineCR, _color), 0f, scaleTime / 2));  //알파값

        seq.OnComplete(() =>
        {
            LineCL.transform.localScale = Vector3.zero;
            LineCR.transform.localScale = Vector3.zero;
            LineCL.gameObject.SetActive(false);
            LineCR.gameObject.SetActive(false);
        });

        //미사일..
        LineShotEffect(color, LineType.CrossLT, bonus);

        Invoke("Restore", 1f);
    }
    #endregion

    #region 라인 미사일
    //라인 마시일 이펙트
    float shotmove = 7f;
    float shottime = 0.6f; //낮을수록 빠름.. 걸리는 시간..
    float shotmove_small = 1.4f;//1.4f;
    float shottime_small = 0.15f;//0.156f; //낮을수록 빠름.. 걸리는 시간..


    private void ParticleColorSet(ParticleSystem.MainModule module, Color _targetColor)
    {
        var gradient = new Gradient();

        gradient.SetKeys(new GradientColorKey[] { new GradientColorKey(_targetColor, 0) }, module.startColor.gradient.alphaKeys);

        module.startColor = gradient;
    }
    private void ParticleVelocitySet(ParticleSystem.VelocityOverLifetimeModule module, Vector2 dir)
    {
        //module.space
    }

    private Color ParticleColorConvert(Color color)
    {
        Vector3 hsv = Vector3.zero;
        Color.RGBToHSV(color, out hsv.x, out hsv.y, out hsv.z);

        return Color.HSVToRGB(hsv.x, 92f / 256, hsv.z);
    }

    public void LineShotEffect(ColorType color, LineType line, bool bonus = false)
    {
        Sprite _spr = null;
        switch (color)
        {
            case ColorType.RED:
                _spr = m_ListLineShotSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineShotSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineShotSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineShotSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineShotSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineShotSpr[5];
                break;
            default:
                break;
        }
        LineShot1.sprite = _spr;
        LineShot2.sprite = _spr;
        LineShot3.sprite = _spr;
        LineShot4.sprite = _spr;

        ParticleColorSet(LineFollow1.main, ParticleColorConvert(EffectManager.GetColor(color)));
        ParticleColorSet(LineFollow2.main, ParticleColorConvert(EffectManager.GetColor(color)));
        ParticleColorSet(LineFollow3.main, ParticleColorConvert(EffectManager.GetColor(color)));
        ParticleColorSet(LineFollow4.main, ParticleColorConvert(EffectManager.GetColor(color)));
        //LineFollow1.startColor

        //Shot이펙트
        switch (line)
        {
            case LineType.LineX:
                {
                    //left
                    LineShot1.gameObject.SetActive(true);
                    LineShot1.transform.localPosition = new Vector3(-0.15f, 0, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot1.transform.eulerAngles = new Vector3(0, 0, 90);
                    ParticleVelocitySet(LineFollow1.velocityOverLifetime, Vector2.left);

                    //right
                    LineShot2.gameObject.SetActive(true);
                    LineShot2.transform.localPosition = new Vector3(0.15f, 0, -0.5f);
                    LineShot2.transform.eulerAngles = new Vector3(0, 0, -90);
                    ParticleVelocitySet(LineFollow1.velocityOverLifetime, Vector2.right);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShot1.transform.DOLocalMoveX(-shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShot2.transform.DOLocalMoveX(shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShot1.gameObject.SetActive(false);
                        LineShot2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.LineY:
                {
                    //top
                    LineShot3.gameObject.SetActive(true);
                    LineShot3.transform.localPosition = new Vector3(0, 0.15f, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot3.transform.eulerAngles = new Vector3(0, 0, 0);
                    //bottom
                    LineShot4.gameObject.SetActive(true);
                    LineShot4.transform.localPosition = new Vector3(0, -0.15f, -0.5f);
                    LineShot4.transform.eulerAngles = new Vector3(0, 0, 180);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShot3.transform.DOLocalMoveY(shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.Insert(0, LineShot4.transform.DOLocalMoveY(-shotmove, shottime).SetEase(Ease.Linear));                        //스케일
                    seq.OnComplete(() =>
                    {
                        LineShot3.gameObject.SetActive(false);
                        LineShot4.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossLT:
            case LineType.CrossRT:
                {
                    //TL
                    LineShot1.gameObject.SetActive(true);
                    LineShot1.transform.localPosition = new Vector3(-0.15f, 0.15f, -0.5f); //Shot이 라인이펙트보다 위로 오도록.
                    LineShot1.transform.eulerAngles = new Vector3(0, 0, 45);
                    //TR
                    LineShot2.gameObject.SetActive(true);
                    LineShot2.transform.localPosition = new Vector3(0.15f, 0.15f, -0.5f);
                    LineShot2.transform.eulerAngles = new Vector3(0, 0, -45);
                    //BL
                    LineShot3.gameObject.SetActive(true);
                    LineShot3.transform.localPosition = new Vector3(-0.15f, -0.15f, -0.5f);
                    LineShot3.transform.eulerAngles = new Vector3(0, 0, 135);
                    //BR
                    LineShot4.gameObject.SetActive(true);
                    LineShot4.transform.localPosition = new Vector3(0.15f, -0.15f, -0.5f);
                    LineShot4.transform.eulerAngles = new Vector3(0, 0, -135);

                    Sequence seq = DOTween.Sequence();
                    if (bonus)
                    {
                        seq.Append(LineShot1.transform.DOLocalMove(new Vector3(-shotmove_small, shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot2.transform.DOLocalMove(new Vector3(shotmove_small, shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot3.transform.DOLocalMove(new Vector3(-shotmove_small, -shotmove_small, -1), shottime_small).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot4.transform.DOLocalMove(new Vector3(shotmove_small, -shotmove_small, -1), shottime_small).SetEase(Ease.Linear));

                    }
                    else
                    {
                        seq.Append(LineShot1.transform.DOLocalMove(new Vector3(-shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot2.transform.DOLocalMove(new Vector3(shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot3.transform.DOLocalMove(new Vector3(-shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                        seq.Insert(0, LineShot4.transform.DOLocalMove(new Vector3(shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                    }
                    seq.OnComplete(() =>
                    {
                        LineShot1.gameObject.SetActive(false);
                        LineShot2.gameObject.SetActive(false);
                        LineShot3.gameObject.SetActive(false);
                        LineShot4.gameObject.SetActive(false);
                    });
                }
                break;
        }
    }
    #endregion

    #region 라인 큰 미사일
    public void CombineBrust_Sprite(Vector3 pos, ColorType color, bool cross = false)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        //Sprite _spr = null;
        //switch (color)
        //{        
        //    case ColorType.RED:
        //        _spr = m_ListLineBigSpr[0];
        //        break;
        //    case ColorType.YELLOW:
        //        _spr = m_ListLineBigSpr[1];
        //        break;
        //    case ColorType.GREEN:
        //        _spr = m_ListLineBigSpr[2];
        //        break;
        //    case ColorType.BLUE:         
        //        _spr = m_ListLineBigSpr[3];
        //        break;
        //    case ColorType.PURPLE:
        //        _spr = m_ListLineBigSpr[4];
        //        break;
        //    case ColorType.ORANGE:
        //        _spr = m_ListLineBigSpr[5];
        //        break;
        //    default:
        //        break;
        //}

        //LineBig_Sprite1.sprite = _spr;
        LineBig_Sprite.gameObject.SetActive(true);

        if (cross == false)
            LineBig_Sprite.transform.localEulerAngles = new Vector3(0, 0, 0);
        else
            LineBig_Sprite.transform.localEulerAngles = new Vector3(0, 0, 45);


        //스케일 애니메이션
        LineBig_Sprite.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.14f);
        seq.Append(LineBig_Sprite.transform.DOScale(1f, 0.6f).SetEase(Ease.Linear));
        seq.Insert(0.14f, LineBig_Sprite.transform.DORotate(LineBig_Sprite.transform.localEulerAngles + new Vector3(0, 0, 180), 0.6f).SetEase(Ease.Linear));
        seq.AppendInterval(0.3f);
        seq.OnComplete(() =>
        {
            LineBig_Sprite.gameObject.SetActive(false);
            EffectManager.Instance.BigShotEffect(pos + Vector3.back);
        });
    }

    public void CombineBrust_Sprite1(Vector3 pos, ColorType color, bool cross = false)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.RED:
                _spr = m_ListLineBigSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineBigSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineBigSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineBigSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineBigSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineBigSpr[5];
                break;
            default:
                break;
        }

        LineBig_Sprite1.sprite = _spr;
        LineBig_Sprite1.transform.localScale = Vector3.one;
        LineBig_Sprite1.gameObject.SetActive(true);

        if (cross == false)
            LineBig_Sprite1.transform.localEulerAngles = new Vector3(0, 0, 0);
        else
            LineBig_Sprite1.transform.localEulerAngles = new Vector3(0, 0, 45);

        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.14f);
        seq.Append(LineBig_Sprite1.transform.DOScale(new Vector3(3.0f, 3.5f), 0.6f).SetEase(Ease.InQuad));
        //seq.Insert(0.14f, LineBig_Sprite1.transform.DOLocalRotate(new Vector3(0, 0, 180), 0.6f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear));
        seq.AppendInterval(0.3f);
        seq.OnComplete(() =>
        {
            LineBig_Sprite1.gameObject.SetActive(false);
        });
    }

    public void CombineBrust_Sprite2(Vector3 pos, ColorType color, bool cross = false)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.RED:
                _spr = m_ListLineBigSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineBigSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineBigSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineBigSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineBigSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineBigSpr[5];
                break;
            default:
                break;
        }

        LineBig_Sprite2.sprite = _spr;
        LineBig_Sprite2.gameObject.SetActive(true);
        LineBig_Sprite2.transform.localScale = Vector3.one;

        if (cross == false)
            LineBig_Sprite2.transform.localEulerAngles = new Vector3(0, 0, 90);
        else
            LineBig_Sprite2.transform.localEulerAngles = new Vector3(0, 0, 135);

        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.25f);
        seq.Append(LineBig_Sprite2.transform.DOScale(new Vector3(3.0f, 3.5f), 0.6f).SetEase(Ease.InQuad));
        //seq.Insert(0.25f, LineBig_Sprite2.transform.DOLocalRotate(new Vector3(0, 0, -180), 0.6f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear));
        seq.AppendInterval(0.3f);
        seq.OnComplete(() =>
        {
            LineBig_Sprite2.gameObject.SetActive(false);
        });
    }

    float shotBig_gap = 0f;
    public void LineShotBigEffect(Vector3 pos, ColorType color, LineType line)
    {
        Reset(new Vector3(pos.x, pos.y, -2.5f));

        Sprite _spr = null;
        switch (color)
        {
            case ColorType.RED:
                _spr = m_ListLineBigSpr[0];
                break;
            case ColorType.YELLOW:
                _spr = m_ListLineBigSpr[1];
                break;
            case ColorType.GREEN:
                _spr = m_ListLineBigSpr[2];
                break;
            case ColorType.BLUE:
                _spr = m_ListLineBigSpr[3];
                break;
            case ColorType.PURPLE:
                _spr = m_ListLineBigSpr[4];
                break;
            case ColorType.ORANGE:
                _spr = m_ListLineBigSpr[5];
                break;
            default:
                break;
        }

        LineShotBig1.sprite = _spr;
        LineShotBig2.sprite = _spr;
        LineShotBig3.sprite = _spr;
        LineShotBig4.sprite = _spr;

        //Shot이펙트
        switch (line)
        {
            case LineType.LineX:
                {
                    LineShotBig1.gameObject.SetActive(true);
                    LineShotBig1.transform.localPosition = new Vector3(shotBig_gap, 0, 0);
                    LineShotBig1.transform.eulerAngles = new Vector3(0, 0, 90);

                    LineShotBig2.gameObject.SetActive(true);
                    LineShotBig2.transform.localPosition = new Vector3(-shotBig_gap, 0, 0);
                    LineShotBig2.transform.eulerAngles = new Vector3(0, 0, -90);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig1.transform.DOLocalMoveX(-shotmove, shottime).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig2.transform.DOLocalMoveX(shotmove, shottime).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig1.gameObject.SetActive(false);
                        LineShotBig2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.LineY:
                {
                    LineShotBig3.gameObject.SetActive(true);
                    LineShotBig3.transform.localPosition = new Vector3(0, -shotBig_gap, 0);
                    LineShotBig3.transform.eulerAngles = new Vector3(0, 0, 0);

                    LineShotBig4.gameObject.SetActive(true);
                    LineShotBig4.transform.localPosition = new Vector3(0, shotBig_gap, 0);
                    LineShotBig4.transform.eulerAngles = new Vector3(0, 0, 180);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig3.transform.DOLocalMoveY(shotmove, shottime).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig4.transform.DOLocalMoveY(-shotmove, shottime).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig3.gameObject.SetActive(false);
                        LineShotBig4.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossLT:
                {
                    LineShotBig1.gameObject.SetActive(true);
                    LineShotBig1.transform.localPosition = new Vector3(shotBig_gap, -shotBig_gap, 0);
                    LineShotBig1.transform.eulerAngles = new Vector3(0, 0, 45);

                    LineShotBig2.gameObject.SetActive(true);
                    LineShotBig2.transform.localPosition = new Vector3(-shotBig_gap, shotBig_gap, 0);
                    LineShotBig2.transform.eulerAngles = new Vector3(0, 0, -135);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig1.transform.DOLocalMove(new Vector3(-shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig2.transform.DOLocalMove(new Vector3(shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig1.gameObject.SetActive(false);
                        LineShotBig2.gameObject.SetActive(false);
                    });
                }
                break;
            case LineType.CrossRT:
                {
                    LineShotBig3.gameObject.SetActive(true);
                    LineShotBig3.transform.localPosition = new Vector3(-shotBig_gap, -shotBig_gap, 0);
                    LineShotBig3.transform.eulerAngles = new Vector3(0, 0, -45);

                    LineShotBig4.gameObject.SetActive(true);
                    LineShotBig4.transform.localPosition = new Vector3(shotBig_gap, shotBig_gap, 0);
                    LineShotBig4.transform.eulerAngles = new Vector3(0, 0, 135);

                    Sequence seq = DOTween.Sequence();
                    seq.Append(LineShotBig3.transform.DOLocalMove(new Vector3(shotmove, shotmove, -1), shottime).SetEase(Ease.Linear));
                    seq.Insert(0, LineShotBig4.transform.DOLocalMove(new Vector3(-shotmove, -shotmove, -1), shottime).SetEase(Ease.Linear));
                    seq.OnComplete(() =>
                    {
                        LineShotBig3.gameObject.SetActive(false);
                        LineShotBig4.gameObject.SetActive(false);
                    });
                }
                break;
        }

        Invoke("Restore", 3f);
    }
    #endregion

    public void Restore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }
}
