﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SignEffect : MonoBehaviour {

    public SpriteRenderer m_Sr;

    float m_Time;
    public void Play(SpriteRenderer render, float time)
    {
        transform.parent = render.transform;
        transform.position = render.transform.position + new Vector3(0, 0, -0.01f);
        transform.localScale = Vector3.one;
        transform.localEulerAngles = Vector3.zero;

        m_Sr.sprite = render.sprite;
        m_Time = time;

        StartCoroutine(Co_AdditiveEffect());
    }

    IEnumerator Co_AdditiveEffect()
    {
        m_Sr.color = Color.black;
        m_Sr.DOColor(Color.white, 0.1f).SetLoops(-1, LoopType.Yoyo);

        float temp = 0;

        while (true)
        {
            yield return null;
            temp += Time.deltaTime;
            if (m_Time <= temp )
                break;
        }

        m_Sr.DOKill();

        ObjectPool.Instance.Restore(gameObject);
    }

}
