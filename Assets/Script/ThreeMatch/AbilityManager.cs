﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

// 원래는 각아이템별 클래스에서 함수를 만들었으나 사라지는 아이템에서는 
// 코루틴을 돌릴수 없는 문제로 인해 AbilityManager를 만들어 통합하게 되었다.
public class AbilityManager : MonoBehaviour {

    //***********************************
    //Item의 효과를 통합 관리
    //1. 효과는 코루틴으로 동작중인데 item이 먼저 사라질때.
    //2. 기존 효과 함수 재사용이 용이함.
    //그래서 일단 아이템과는 별도의 오브젝트에서 효과를 돌려줘야 한다.

    public static AbilityManager Instance;


    public MatchManager m_MatchMgr;
    public EffectManager m_EffectMgr;
    //public UI_ThreeMatch m_UITreeMatch;

    const float SerialBrustDelay = 0.01f;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_MatchMgr = MatchManager.Instance;
        m_EffectMgr = EffectManager.Instance;
        //m_UITreeMatch = UI_ThreeMatch.Instance;
    }

    #region Butterfly
    public void Ability_Butterfly(Board bd, ColorType color, ItemType combine)
    {
        StartCoroutine(Co_Ability_Butterfly(bd, color, combine));
    }
    IEnumerator Co_Ability_Butterfly(Board bd, ColorType color, ItemType combine) //bd는 위치정보, color는 나비 및 이펙트의 컬러
    {
        //날라갈 보드 선택
        Board FlyTargetBoard = GetFlyTargetBoard(bd.m_IsJamBoard);

        //말도 안되게 나비가 날아갈 타겟이 없다면 진행 불가!
        if (FlyTargetBoard == null)
            yield break;

        //나비 객체 생성
        ButterFlyItem bf = bd.CreateItem(ItemType.Butterfly, color) as ButterFlyItem;
        bf.Animation_Fly();

        //날라가는 나비의 반짝이는 꼬리 파티클
        EffectManager.Instance.ButterflyEffect(bf.transform, bf.m_Color);

        //사운드
        SoundManager.Instance.PlayEffect("butterfly_ready");

        //선택된 보드는 아이템이 날라가는 사이에 Brust나 drop 되면 안된다.

        //여기서 m_ItemBrusting값을 True주는 이유.(2번의 이유때문에 다른 변수를 추가하여 작업할수 없음)
        //1. 다른 나비가 해당 블럭을 중복 선택하지 않도록.(이말은 즉, 나비가 날아갈 보드를 선택할때는 값이 false인것만 찾는다)
        //2. 드랍이나 매칭등으로 나비가 날아가는 동안 다른 이슈가 생기지 않도록.
        FlyTargetBoard.m_ItemBrusting = true;
        FlyTargetBoard.m_PanelBrusting = true;
        FlyTargetBoard.m_DropAnim = true;

        //딸기잼 정보를 이동시작시점에서 체크해야한다.
        bool isJam = bd.m_IsJamBoard;

        //이동 애니메이션
        bf.transform.position += new Vector3(0, 0, -1f);//다른 패널들보다 위에서 움직이도록 -1뺀다.
        Vector3 pos = FlyTargetBoard.transform.position + new Vector3(0, 0, -1f);

        Vector3 _dir = (pos - bf.transform.position).normalized;
        // 방향을 바라보는 Quaternion을 구한다.
        Quaternion _rot = Quaternion.LookRotation(_dir);
        bf.transform.rotation = _rot;
        var angle = Mathf.Atan2(_dir.y, _dir.x) * Mathf.Rad2Deg;                                                                                     //주석처리 : 박쥐는 머리를 위로 해서 날라간다.
        bf.transform.eulerAngles = new Vector3(0, 0, angle - 90); //이미지가 X축으로 되어 있으면 0도 인데 y축으로 보고 있기 때문에 -90를 해준다.     //주석처리 : 박쥐는 머리를 위로 해서 날라간다.

        //추가. 박쥐가 x+로 날라가는지 x-로 날라가는지에따라 flip horizontal을 해줘야한다
        //.그래서 x스케일값에 -1을 곱해준다

        //이미지가 왼쪽을 보고있다.
        var look_factor = (_dir.x < 0) ? 1 : -1;

        var getConvertedScale = new Func<float, Vector3>(((value) => new Vector3(look_factor * value, value, value)));

        bf.transform.localScale = getConvertedScale(2);
        //곡선 그리며 이동 하기
        Vector3 OrthoVector = Vector3.zero;
        Vector3.OrthoNormalize(ref _dir, ref OrthoVector);          //목표방향에 수직인 벡터를 구함.

        //Moveto는 목표점으로 이동, Moveby는 내가 얼마만큼 이동
        Sequence seq = DOTween.Sequence();
        //seq.Append(bf.transform.DOScale(2f, 1f));
        seq.Append(bf.transform.DOScale(getConvertedScale(1f), 1f));
        seq.Insert(0, bf.transform.DOScale(getConvertedScale(1.5f), 0.5f).SetEase(Ease.OutCubic));
        seq.Insert(0.5f, bf.transform.DOScale(getConvertedScale(1f), 0.5f).SetEase(Ease.InCubic));
        seq.Insert(0f, bf.transform.DOBlendableMoveBy(pos - bf.transform.position, 1f).SetEase(Ease.Linear));
        seq.Insert(0f, bf.transform.DOBlendableMoveBy(OrthoVector / 2, 0.25f));
        seq.Insert(0.25f, bf.transform.DOBlendableMoveBy(-OrthoVector, 0.5f)); //moveby이므로 항상 내가 이동하는 거리이다.*2해준다.
        seq.Insert(0.75f, bf.transform.DOBlendableMoveBy(OrthoVector / 2, 0.25f));
        seq.OnComplete(() =>
        {
            //AbilityManager.Instance.Ability_Butterfly(FlyTargetBoard);
            //위에서 true해준걸 정상 복구
            FlyTargetBoard.m_ItemBrusting = false;
            FlyTargetBoard.m_PanelBrusting = false;
            FlyTargetBoard.m_DropAnim = false;

            //딸기쨈적용.
            if (isJam) FlyTargetBoard.JamPanelCreate(0);

            switch (combine)
            {
                case ItemType.None:
                    {
                        FlyTargetBoard.Brust(false, 0);
                        //나비아이템이 사라져야할 시점.
                        ObjectPool.Instance.Restore(bf.gameObject);
                    }
                    break;
                case ItemType.Butterfly:
                    {
                        FlyTargetBoard.Brust(false, 0);
                        //나비아이템이 사라져야할 시점.
                        ObjectPool.Instance.Restore(bf.gameObject);
                    }
                    break;
                case ItemType.Line_X:
                    {
                        FlyTargetBoard.Brust(false, 0);
                        Ability_LineX(FlyTargetBoard, color);
                        //나비아이템이 사라져야할 시점.
                        ObjectPool.Instance.Restore(bf.gameObject);
                    }
                    break;
                case ItemType.Line_Y:
                    {
                        FlyTargetBoard.Brust(false, 0);
                        Ability_LineY(FlyTargetBoard, color);
                        //나비아이템이 사라져야할 시점.
                        ObjectPool.Instance.Restore(bf.gameObject);
                    }
                    break;
                case ItemType.Line_C:
                    {
                        FlyTargetBoard.Brust(false, 0);
                        Ability_Cross(FlyTargetBoard, color);
                        //나비아이템이 사라져야할 시점.
                        ObjectPool.Instance.Restore(bf.gameObject);
                    }
                    break;
                case ItemType.Bomb:
                    {
                        //전조이펙트중에 wait상태로 가지 않도록..
                        FlyTargetBoard.m_ItemBrusting = true;
                        FlyTargetBoard.m_PanelBrusting = true;
                        FlyTargetBoard.m_DropAnim = true;
                        Ability_Bomb(FlyTargetBoard, bf, 1, () =>
                        {
                            FlyTargetBoard.m_ItemBrusting = false;
                            FlyTargetBoard.m_PanelBrusting = false;
                            FlyTargetBoard.m_DropAnim = false;

                            //폭탄자리의 보드는 여기서 터트려준다.
                            FlyTargetBoard.Brust(false, 0);
                            //나비아이템이 사라져야할 시점.
                            ObjectPool.Instance.Restore(bf.gameObject);
                        });
                    }
                    break;
            }


        });

        yield return null;
    }
    public Board GetFlyTargetBoard(bool isJam)
    {
        List<MissionInfo> List_MsInfo = MissionManager.Instance.List_MsInfo_PC; //미션매니저에서 실제 미션 진행상황.
        List<Board> FlyPossibleBoard = new List<Board>();

        //*****************************************
        //1. 미션 중에 최우선 미션있으면 먼저 찾음.
        //*****************************************
        if (MissionManager.Instance.List_MsInfo_PC.Exists(find => find.kind == MissionKind.Stele))
        {
            GetMissionTargetBoards(MissionKind.Stele, ref FlyPossibleBoard);
        }
        else if (MissionManager.Instance.List_MsInfo_PC.Exists(find => find.count > 0 && find.kind == MissionKind.Wafer))
        {
            GetMissionTargetBoards(MissionKind.Wafer, ref FlyPossibleBoard);
        }
        else if (isJam)//나비아래 잼이 있는가? 잼이 있으면 미션이 있는거기 때문에 미션체크는 생략..
        {
            GetMissionTargetBoards(MissionKind.Jam, ref FlyPossibleBoard);
        }
        else if (MissionManager.Instance.List_MsInfo_PC.Exists(find => find.count > 0 && (int)find.kind >= (int)MissionKind.StrawberryCake && (int)find.kind <= (int)MissionKind.Hamburger))
        {
            GetMissionTargetBoards(MissionKind.StrawberryCake, ref FlyPossibleBoard);
        }
        //찾았으면
        if (FlyPossibleBoard.Count > 0)
            return FlyPossibleBoard[UnityEngine.Random.Range(0, FlyPossibleBoard.Count)];

        //*******************************
        //2. 남은 미션은 순차적으로 찾음.
        //*******************************
        for (int i = 0; i < List_MsInfo.Count; i++)
        {
            //여기서 구지 1순위미션들을 예외처리 하는 이유는 
            //나비가 동시에 여러개 터지면 날아가는 중에는  count가 0이 아닐테고 남은 나비들이 불필요한 체크를 하게 된다.
            if (List_MsInfo[i].kind == MissionKind.Stele)
                continue;
            if (List_MsInfo[i].kind == MissionKind.Wafer)
                continue;
            if (List_MsInfo[i].kind == MissionKind.Jam)
                continue;
            if ((int)List_MsInfo[i].kind >= (int)MissionKind.StrawberryCake && (int)List_MsInfo[i].kind <= (int)MissionKind.Hamburger)
                continue;

            if (List_MsInfo[i].count > 0)
            {
                GetMissionTargetBoards(List_MsInfo[i].kind, ref FlyPossibleBoard);
            }
        }
        //찾았으면
        if (FlyPossibleBoard.Count > 0)
            return FlyPossibleBoard[UnityEngine.Random.Range(0, FlyPossibleBoard.Count)];

        //********************************
        //3. 미션은 아니지만 우선 순위
        //********************************
        if (MissionManager.Instance.List_MsInfo_PC.Exists(find => find.kind == MissionKind.Bottle) == false) //미션에 없었다면 여기서 찾는다.
        {
            GetMissionTargetBoards(MissionKind.Bottle, ref FlyPossibleBoard);
        }
        //찾았으면
        if (FlyPossibleBoard.Count > 0)
            return FlyPossibleBoard[UnityEngine.Random.Range(0, FlyPossibleBoard.Count)];

        //*******************************
        //4. 패널, 아이템 랜덤으로 선택
        //********************************
        //패널
        for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
        {
            if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].IsNowPanelBrustDestroy)
            {
                if (m_MatchMgr.m_ListBoard[i].m_IsCakeBoard)
                {
                    if (m_MatchMgr.m_ListBoard[i].GetPanel<CakePanel>() == m_MatchMgr.m_ListBoard[i].GetPanel<CakePanel>().CakePanel_A)
                        GetRemainCakeBoard(m_MatchMgr.m_ListBoard[i].GetPanel<CakePanel>(), ref FlyPossibleBoard);
                }
                else
                    FlyPossibleBoard.Add(m_MatchMgr.m_ListBoard[i]);
            }
        }
        //찾았으면
        if (FlyPossibleBoard.Count > 0)
            return FlyPossibleBoard[UnityEngine.Random.Range(0, FlyPossibleBoard.Count)];


        //아이템
        for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
        {
            if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
            {
                FlyPossibleBoard.Add(m_MatchMgr.m_ListBoard[i]);
            }
        }
        if (FlyPossibleBoard.Count > 0)
            return FlyPossibleBoard[UnityEngine.Random.Range(0, FlyPossibleBoard.Count)];

        //끝
        return null;
    }

    public void GetMissionTargetBoards(MissionKind mskind, ref List<Board> list)
    {
        //**************************************
        //작업시 문제가 있던 부분에 대한 정리
        //**************************************
        //아이템은 m_ItemBrusting값을 참조하여 찾으면 문제가 없다.(IsNowItemBrust값에 포함하여 체크되도록 수정됨)
        //하지만 패널을 m_PanelBrusting값으로만 찾으면 문제가 생긴다.
        //m_ItemBrusting은 아이템이 터지고 있느냐는 단순한 체크 기능뿐 아니라
        //드랍 및 매칭 등 다양한 곳에서 해당 보드의 Brust체크 및 가능여부를 제공한다.
        //그래서 선택한 Board의 m_ItemBrusting값을 true로 줘서 드랍 및 다른 진행이 되지 않도록 막아야하는데
        //패널 찾을때 m_PanelBrusting으로만 보드를 찾으면 m_ItemBrusing값이 언제 flase로 바뀔지 알수 없다.
        //(예를 들어 첨에 원래 true였는데 해당 보드가 선택됐고 내가 다시 true로 줬지만 Brust가 끝나가 나비가 도착하기전에 false로 바뀔수 있다.)
        //그러면 나비가 도착하기전에 터진다거나 나비가 중복으로 선택되어 날아게 되어 Brust를 두번 태워버린다.
        //나비가 도착하면 m_ItemBrusting값을 false로 바꾸도록 되어 있으니 기존에 중복체크소스가 무의미하게 되고 함수가 중복 실행될때 에러가 난다.

        //결론은 보드를 선택할때는 무조껀 m_ItemBrusting값을 참조해야 한다는것이다! 아이템을 찾을때는 m_ItemBrusting만 체크해도 문제가 없다.
        //하지만 패널은 찾을때는 m_ItemBrusting을 기본으로 깔고 m_PanelBrusting을 더해서 찾아야 한다. 
        //왜냐면 드랍 및 매칭 관련 된 부분들이 m_ItemBrusting변수로 채크하기 때문이다.
        //즉, 게임 로직이 아이템관련 위주로 되어 있고 패널은 부가적인것이다.

        switch (mskind)
        {
            case MissionKind.None:
                break;
            case MissionKind.Red:
            case MissionKind.Yellow:
            case MissionKind.Green:
            case MissionKind.Blue:
            case MissionKind.Purple:
            case MissionKind.Orange:
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.Normal
                                && m_MatchMgr.m_ListBoard[i].m_Item.m_Color == (ColorType)mskind) //일반블럭컬러값 == 미션종류값 이기 때문에 가능.
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Donut:
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.Donut)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Spiral:
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.Spiral)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Bread://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Bread_Block) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.LollyCage://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Lolly_Cage) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.IceCage://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Ice_Cage) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Cracker://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Cracker) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Bottle: //보물상자는 키를 찾아야한다.
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.Key)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.S_Tree://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.S_Tree) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;

            //콤비네이션 미션은 나비가 수행 불가능하고
            //라인, 폭탄, 레인보우는 유저가 콤비네이션을 하기 위해 아끼고 있을수 있는데 나비가 깨버리면 안될꺼 같다.
            //case MissionKind.Line:
            //    break;
            //case MissionKind.Line_Line:
            //    break;
            //case MissionKind.Cross:
            //    break;
            //case MissionKind.Cross_Line:
            //    break;
            //case MissionKind.Cross_Cross:
            //    break;
            //case MissionKind.Bomb:
            //    break;
            //case MissionKind.Bomb_Line:
            //    break;
            //case MissionKind.Bomb_Cross:
            //    break;
            //case MissionKind.Bomb_Bomb:
            //    break;
            //case MissionKind.Rainbow:
            //    break;
            //case MissionKind.Rainbow_Line:
            //    break;
            //case MissionKind.Rainbow_Cross:
            //    break;
            //case MissionKind.Rainbow_Bomb:
            //    break;
            //case MissionKind.Rainbow_Rainbow:
            //    break;
            case MissionKind.TimeBomb:
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.TimeBomb)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Wafer: //패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Wafer_floor) != null && m_MatchMgr.m_ListBoard[i].IsNowPanelBrustDestroy) //패널이고 맨위패널이 터질수 있어야한다.
                            {
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                            }
                        }
                    }
                }
                break;
            case MissionKind.StrawberryCake:
            case MissionKind.ChocolatePiece:
            case MissionKind.MintCake:
            case MissionKind.Parfait:
            case MissionKind.WhiteCake:
            case MissionKind.Hamburger:
                {
                    //StrawberryCake값으로만 넘어온다. 하지만 모든 음식을 체크해서 넘겨주도록하자.
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsItemExist)
                        {
                            if ((int)m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType >= (int)ItemType.Misson_Food1
                            && (int)m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType <= (int)ItemType.Misson_Food6)
                            {
                                List<Board> boards = m_MatchMgr.m_ListBoard[i].DropReference();
                                for (int j = 0; j < boards.Count; j++)
                                {
                                    if (boards[j] != null
                                        && boards[j].IsNowBoardBrust)
                                    {
                                        if (boards[j].m_IsCakeBoard)
                                        {
                                            GetRemainCakeBoard(boards[j].GetPanel<CakePanel>().CakePanel_A, ref list);
                                        }
                                        else
                                        {
                                            list.Add(boards[j]);
                                        }

                                    }

                                }
                            }
                        }
                    }
                }
                break;
            case MissionKind.Bear:
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].IsNowItemBrust)
                        {
                            if (m_MatchMgr.m_ListBoard[i].m_Item.m_ItemType == ItemType.JellyBear)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.IceCream://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.IceCream_Block) != null)
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                        }
                    }
                }
                break;
            case MissionKind.Jam://패널
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].IsPanelNeverMission == false && !m_MatchMgr.m_ListBoard[i].m_IsJamBoard)
                            {
                                if (m_MatchMgr.m_ListBoard[i].m_ListPanel.Count > 0)
                                {
                                    if (m_MatchMgr.m_ListBoard[i].m_ListPanel[0].PanelBrust && m_MatchMgr.m_ListBoard[i].m_ListPanel[0].Defence > 0)
                                    {
                                        //조건으로 터지는 Bottle 제외(PanelBrust)
                                        //조각이 없는 케이크 판은 제외(PanelBrust & Defence)
                                        list.Add(m_MatchMgr.m_ListBoard[i]);
                                    }
                                }
                                else
                                    list.Add(m_MatchMgr.m_ListBoard[i]);
                            }
                        }
                    }
                }
                break;
            case MissionKind.Stele://석판
                {
                    for (int i = 0; i < m_MatchMgr.m_ListBoard.Count; i++)
                    {
                        if (m_MatchMgr.m_ListBoard[i].m_ItemBrusting == false && m_MatchMgr.m_ListBoard[i].m_PanelBrusting == false)
                        {
                            if (m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Stele) != null && m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Stele_Hide) != null && m_MatchMgr.m_ListBoard[i].IsNowPanelBrustDestroy
                                && m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Bottle_Cage) == null
                                && m_MatchMgr.m_ListBoard[i].GetPanel(PanelType.Ring) == null)
                            {
                                list.Add(m_MatchMgr.m_ListBoard[i]);
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    public void Ability_ButterflyButterfly(Board bd, ColorType color)
    {
        //**********
        //콤바인처리
        //**********
        Item.Swap_A.m_Board.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Item.Swap_B.m_Board.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //콤바인 효과
        //보드는 위치용으로 사용하기 때문에 Swap_B를 사용. 하지만 컬러는 기존 나비에서 가져온다.
        Ability_Butterfly(Item.Swap_B.m_Board, Item.Swap_A.m_Color, ItemType.Butterfly);
        Ability_Butterfly(Item.Swap_B.m_Board, Item.Swap_A.m_Color, ItemType.Butterfly);
        Ability_Butterfly(Item.Swap_B.m_Board, Item.Swap_B.m_Color, ItemType.Butterfly);
    }

    public void GetRemainCakeBoard(CakePanel CakePanel_A, ref List<Board> list)
    {
        if (CakePanel_A.Defence != -1)
            list.Add(CakePanel_A.m_Board);
        if (CakePanel_A.m_Board[SQR_DIR.RIGHT].GetPanel<CakePanel>().Defence != -1)
            list.Add(CakePanel_A.m_Board[SQR_DIR.RIGHT]);
        if (CakePanel_A.m_Board[SQR_DIR.BOTTOM].GetPanel<CakePanel>().Defence != -1)
            list.Add(CakePanel_A.m_Board[SQR_DIR.BOTTOM]);
        if (CakePanel_A.m_Board[SQR_DIR.BOTTOMRIGHT].GetPanel<CakePanel>().Defence != -1)
            list.Add(CakePanel_A.m_Board[SQR_DIR.BOTTOMRIGHT]);
    }
    #endregion


    #region LineX,Y
    public void Ability_LineX(Board board, ColorType color)
    {
        StartCoroutine(Co_Ability_LineX(board,color));
    }
    IEnumerator Co_Ability_LineX(Board board, ColorType color)
    {
        //이펙트
        //LineEffect effect = m_EffectMgr.GetLineEffect();
        //effect.LineXEffect(board.transform.position, color);
        //added
        //effect.LineShotEffect(board.IsItemExist ? board.m_Item.m_Color : ColorType.None, LineType.LineX);

        var effect = m_EffectMgr.GetTwinLineEffect();
        effect.LineXEffect(board.transform.position, color);


        //나비 적용 전 코드
        //effect.LineXEffect(board.transform.position, board.IsItemExist ? board.m_Item.m_Color : ColorType.None);
        //effect.LineShotEffect(board.IsItemExist ? board.m_Item.m_Color : ColorType.None, LineType.LineX);

        //점수
        //m_UITreeMatch.ScoreText(60, board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("line");

        //효과
        StartCoroutine(Shot_Dir(board[SQR_DIR.LEFT], SQR_DIR.LEFT, board.m_IsJamBoard));
        StartCoroutine(Shot_Dir(board[SQR_DIR.RIGHT], SQR_DIR.RIGHT, board.m_IsJamBoard));

        yield return null;
    }
    public void Ability_LineY(Board board, ColorType color)
    {
        StartCoroutine(Co_Ability_LineY(board, color));
    }
    IEnumerator Co_Ability_LineY(Board board, ColorType color)
    {
        //이펙트
        //LineEffect effect = m_EffectMgr.GetLineEffect();

        //effect.LineYEffect(board.transform.position, color);

        //add
        //effect.LineShotEffect(board.IsItemExist ? board.m_Item.m_Color : ColorType.None, LineType.LineY);

        var effect = m_EffectMgr.GetTwinLineEffect();
        effect.LineYEffect(board.transform.position, color);

        //effect.LineYEffect(board.transform.position, board.IsItemExist ? board.m_Item.m_Color : ColorType.None);
        //effect.LineShotEffect(board.IsItemExist ? board.m_Item.m_Color : ColorType.None, LineType.LineY);

        //점수
        //m_UITreeMatch.ScoreText(60, board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("line");

        //효과
        StartCoroutine(Shot_Dir(board[SQR_DIR.TOP], SQR_DIR.TOP, board.m_IsJamBoard));
        StartCoroutine(Shot_Dir(board[SQR_DIR.BOTTOM], SQR_DIR.BOTTOM, board.m_IsJamBoard));

        yield return null;
    }

    IEnumerator Shot_Dir(Board board, SQR_DIR dir, bool jam)
    {
        //여기서 코루틴을 타는 이유는 결국 스파이럴이나 잼의 증식을 방해하는 방해물,케이지등을 실시간으로 체크하기 위해서이다. 
        //기존에는 효과가 발생할 모든 보드를 한번에 조건 체크하고 각보드에서 코루틴으로 대기했었다.
        //하지만 실제 터질때까지 딜레이가 있으므로 그사이 해당 보드에 변화가 생겼다면 반영되지 않는 문제가 있다.
        //이제는 여기에서 코루틴을 돌면서 딜레이를 체크한후 터질시점에서 조건들을 체크한다.

        if (board == null)
            yield break;

        List<Board> list_dir = board.GetBoardDir(dir);

        for (int i = 0; i < list_dir.Count; i++)
        {
            list_dir[i].Brust(true, 0, true, false);

            if (list_dir[i].m_Item is SpiralItem) break;    //스파이럴
            if (list_dir[i].GetPanel<S_TreePanel>() != null) break;          //스페셜트리

            //연속적으로 잼 증식
            jam = list_dir[i].JamPanelCreateCountinue(jam);

            yield return new WaitForSeconds(SerialBrustDelay);   
        }
    }


    public void Ability_LineXButterfly()
    {
        Board Board_LineX = Item.Swap_A.m_Board;
        Board Board_Butterfly = Item.Swap_B.m_Board;
        if (Board_LineX.m_Item.m_ItemType != ItemType.Line_X)
        {
            Board_LineX = Item.Swap_B.m_Board;
            Board_Butterfly = Item.Swap_A.m_Board;
        }

        //**********
        //콤바인처리
        //**********
        Board_LineX.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_Butterfly.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //콤바인 효과 
        //보드는 위치용으로 사용하기 때문에 Swap_B를 사용. 하지만 컬러는 기존 나비에서 가져온다.
        Ability_Butterfly(Item.Swap_B.m_Board, Board_Butterfly.m_Item.m_Color, ItemType.Line_X);
    }
    public void Ability_LineYButterfly()
    {
        Board Board_LineY = Item.Swap_A.m_Board;
        Board Board_Butterfly = Item.Swap_B.m_Board;
        if (Board_LineY.m_Item.m_ItemType != ItemType.Line_X)
        {
            Board_LineY = Item.Swap_B.m_Board;
            Board_Butterfly = Item.Swap_A.m_Board;
        }

        //**********
        //콤바인처리
        //**********
        Board_LineY.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_Butterfly.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //콤바인 효과
        //보드는 위치용으로 사용하기 때문에 Swap_B를 사용. 하지만 컬러는 기존 나비에서 가져온다.
        Ability_Butterfly(Item.Swap_B.m_Board, Board_Butterfly.m_Item.m_Color, ItemType.Line_Y);
    }
    public void Ability_LineLine(ColorType color)
    {
        StartCoroutine(Co_Ability_LineLine(color));
    }
    IEnumerator Co_Ability_LineLine(ColorType color)
    {
        //터치한 Swap_A자리에서 터지는게아니고 이동한 Swap_B자리에서 터진다
        Board Board_A = Item.Swap_A.m_Board;
        Board Board_B = Item.Swap_B.m_Board;

        //이펙트 
        //Ability함수 재사용으로 인해 추가 이펙트만 필요..

        //사운드
        //Ability함수 재사용으로 인해 추가 사운드 있을때만

        //**********
        //콤바인처리
        //**********
        Board_A.CombineBrust(ItemType.None);  //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_B.CombineBrust(ItemType.None);  //ItemType을 None을 주면 효과없이 그냥 Brust.

        //*********
        //효과 발동
        //*********
        Ability_LineX(Board_B, Board_B.m_Item.m_Color);
        Ability_LineY(Board_B, Board_B.m_Item.m_Color);

        yield return null;
    }

    #endregion

    #region LineCross
    public void Ability_Cross(Board board, ColorType color)
    {
        StartCoroutine(Co_Ability_Cross(board, color));
    }
    IEnumerator Co_Ability_Cross(Board board, ColorType color)
    {
        //이펙트 
        //LineEffect effect = m_EffectMgr.GetLineEffect();

        var effect = m_EffectMgr.GetTwinLineEffect();
        effect.LineCEffect(board.transform.position, color);

        //effect.LineCEffect(board.transform.position, board.IsItemExist ? board.m_Item.m_Color : ColorType.None);
        //effect.LineShotEffect(board.IsItemExist ? board.m_Item.m_Color : ColorType.None, LineType.CrossLT);

        //점수
        //m_UITreeMatch.ScoreText(60, board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("line");

        List<Board> list_TL = board.GetBoardDir(SQR_DIR.TOPLEFT);
        List<Board> list_TR = board.GetBoardDir(SQR_DIR.TOPRIGHT);
        List<Board> list_BL = board.GetBoardDir(SQR_DIR.BOTTOMLEFT);
        List<Board> list_BR = board.GetBoardDir(SQR_DIR.BOTTOMRIGHT);

        StartCoroutine(Shot_Dir(board[SQR_DIR.TOPLEFT], SQR_DIR.TOPLEFT, board.m_IsJamBoard));
        StartCoroutine(Shot_Dir(board[SQR_DIR.TOPRIGHT], SQR_DIR.TOPRIGHT, board.m_IsJamBoard));
        StartCoroutine(Shot_Dir(board[SQR_DIR.BOTTOMLEFT], SQR_DIR.BOTTOMLEFT, board.m_IsJamBoard));
        StartCoroutine(Shot_Dir(board[SQR_DIR.BOTTOMRIGHT], SQR_DIR.BOTTOMRIGHT, board.m_IsJamBoard));

        yield return null;
    }


    public void Ability_CrossButterfly()
    {
        Board Board_LineC = Item.Swap_A.m_Board;
        Board Board_Butterfly = Item.Swap_B.m_Board;
        if (Board_LineC.m_Item.m_ItemType != ItemType.Line_C)
        {
            Board_LineC = Item.Swap_B.m_Board;
            Board_Butterfly = Item.Swap_A.m_Board;
        }

        //**********
        //콤바인처리
        //**********
        Board_LineC.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_Butterfly.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //콤바인 효과
        //보드는 위치용으로 사용하기 때문에 Swap_B를 사용. 하지만 컬러는 기존 나비에서 가져온다.
        Ability_Butterfly(Item.Swap_B.m_Board, Board_Butterfly.m_Item.m_Color, ItemType.Line_C);
    }
    public void Ability_CrossLine(ColorType color)
    {
        StartCoroutine(Co_Ability_CrossLine(color));
    }
    IEnumerator Co_Ability_CrossLine(ColorType color)
    {
        Board Board_A = Item.Swap_A.m_Board;
        Board Board_B = Item.Swap_B.m_Board;

        //이펙트 
        //다른Ability함수를 사용했으므로 기본적인 이펙트는 자동 출력된다.
        //추가적으로 필요하면 추가한다.

        //점수
        //m_UITreeMatch.ScoreText(60, Board_B.transform.position);

        //**********
        //콤바인처리
        //**********
        Board_A.CombineBrust(ItemType.None);  //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_B.CombineBrust(ItemType.None);  //ItemType을 None을 주면 효과없이 그냥 Brust.

        //*********
        //효과 발동
        //*********
        Ability_LineX(Board_B, Board_B.m_Item.m_Color);
        Ability_LineY(Board_B, Board_B.m_Item.m_Color);

        Ability_Cross(Board_B, Board_B.m_Item.m_Color);

        yield return null;
    }

    #endregion

    #region Bomb
    //Brust
    public void Ability_Bomb(Board board,Item item, int range, Action complete)
    {
        StartCoroutine(Co_Ability_Bomb(board, item, range, complete));
    }

    IEnumerator Co_Ability_Bomb(Board board, Item item, int range, Action complete)
    {
        //사운드, 이펙트 순서는 바뀔수 있다..

        //*********
        //이펙트 1
        //*********
        m_EffectMgr.SignEffect(item.m_SpriteRender, 0.5f);


        yield return new WaitForSeconds(0.3f);

        //**********
        //이펙트 2
        //**********
        m_EffectMgr.BombEffect(board.transform.position, item.m_Color);

        yield return new WaitForSeconds(0.3f);

        //점수
        //m_UITreeMatch.ScoreText(60, board.transform.position);

        //*********
        //폭탄흔듦
        //*********
        StartCoroutine(Co_Shake());

        //*********
        //사운드.. 
        //*********
        SoundManager.Instance.PlayEffect("bomb");

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        complete();

        //**********
        //효과
        //**********
        List<List<Board>> list_boundary = new List<List<Board>>();
        for (int i = 0; i < range; i++)
        {
            List<Board> list_around = board.GetBoardAround(i + 1, true);
            list_boundary.Add(list_around);
        }

        List<Board> last_boards = new List<Board>(); 
        for (int i = 0; i < (int)DROP_DIR.List; i++)
        {
            Board lastboard = board;
            for(int j = 0; j <= range; j++)
            {
                if(lastboard != null)
                    lastboard = lastboard[(DROP_DIR)i];
            }

            if(lastboard != null)
                last_boards.Add(lastboard);
        }

        if (last_boards.Count > 0)
            list_boundary.Add(last_boards);

        //안쪽부터 밖으로 터트리자!
        for (int i = 0; i < list_boundary.Count; i++)
        {
            for (int k = 0; k < list_boundary[i].Count; k++)
            {
                list_boundary[i][k].Brust(true, 0, true, false);

                //폭탄은 조건으로 그 뒤를 막는게 없다.
                if (board.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
            }

            yield return new WaitForSeconds(SerialBrustDelay);
        }
    }

    IEnumerator Co_Shake(float power = 0.05f)
    {
        int cnt = 0;
        while (true)
        {
            if (cnt == 6)
            {
                for (int i = 0, len = MatchManager.Instance.m_ListBoard.Count; i < len; i++)
                {
                    if (MatchManager.Instance.m_ListBoard[i].m_Item != null)
                    {
                        //겜중에서 아이템이 앞으로 나와야 하는 상황이 있는데 z값을 0을 주면 안된다.
                        //ex)레인보우 Brust연출
                        //MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition = Vector3.zero;
                        MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition = new Vector3(0, 0, MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition.z);
                    }
                }
                break;
            }

            float x = UnityEngine.Random.Range(0, 2) == 0 ? UnityEngine.Random.Range(0.02f, power) : -UnityEngine.Random.Range(0.02f, power);
            float y = UnityEngine.Random.Range(0, 2) == 0 ? UnityEngine.Random.Range(0.02f, power) : -UnityEngine.Random.Range(0.02f, power);

            for (int i = 0, len = MatchManager.Instance.m_ListBoard.Count; i < len; i++)
            {
                if (MatchManager.Instance.m_ListBoard[i].m_Item != null)
                {
                    //겜중에서 아이템이 앞으로 나와야 하는 상황이 있는데 z값을 0을 주면 안된다.
                    //ex)레인보우 Brust연출
                    //MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition = new Vector3(x, y, 0);
                    MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition = new Vector3(x, y, MatchManager.Instance.m_ListBoard[i].m_Item.m_SpriteRender.transform.localPosition.z);
                }
            }

            cnt++;
            yield return null;
        }

    }
    public void Ability_BombButterfly(Board board)
    {
        Board Board_Bomb = Item.Swap_A.m_Board;
        Board Board_Butterfly = Item.Swap_B.m_Board;
        if (Board_Bomb.m_Item.m_ItemType != ItemType.Bomb)
        {
            Board_Bomb = Item.Swap_B.m_Board;
            Board_Butterfly = Item.Swap_A.m_Board;
        }

        //**********
        //콤바인처리
        //**********
        //Board_Bomb.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_Butterfly.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //콤바인 효과
        //보드는 위치용으로 사용하기 때문에 Swap_B를 사용. 하지만 컬러는 기존 나비에서 가져온다.
        Ability_Butterfly(Item.Swap_B.m_Board, Board_Butterfly.m_Item.m_Color, ItemType.Bomb);
    }


    public void Ability_BombLine(ColorType color, Action complete)
    {
        StartCoroutine(Co_Ability_BombLine(color, complete));
    }
    IEnumerator Co_Ability_BombLine(ColorType color, Action complete)
    {
        m_MatchMgr.SpecialEffect = true;
        Board Board_A = Item.Swap_A.m_Board;
        Board Board_B = Item.Swap_B.m_Board;


        EffectManager.Instance.ItemChangeEffect(Board_B.transform.position);

        yield return new WaitForSeconds(0.4f);

        //사운드
        SoundManager.Instance.PlayEffect("crossbomb");

        //이펙트 
        LineEffect effect1 = m_EffectMgr.GetLineEffect();
        effect1.CombineBrust_Sprite(Board_B.transform.position, color, false);

        //yield return new WaitForSeconds(1.04f);

        //라인이펙트 추가
        if (Board_B[SQR_DIR.TOP] != null)
            m_EffectMgr.GetTwinLineEffect().LineXEffect(Board_B[SQR_DIR.TOP].transform.position, ColorType.BLUE);
        if (Board_B != null)
            m_EffectMgr.GetTwinLineEffect().LineXEffect(Board_B.transform.position, ColorType.BLUE);
        if (Board_B[SQR_DIR.BOTTOM] != null)
            m_EffectMgr.GetTwinLineEffect().LineXEffect(Board_B[SQR_DIR.BOTTOM].transform.position, ColorType.BLUE);
        if (Board_B[SQR_DIR.LEFT] != null)
            m_EffectMgr.GetTwinLineEffect().LineYEffect(Board_B[SQR_DIR.LEFT].transform.position, ColorType.BLUE);
        if (Board_B != null)
            m_EffectMgr.GetTwinLineEffect().LineYEffect(Board_B.transform.position, ColorType.BLUE);
        if (Board_B[SQR_DIR.RIGHT] != null)
            m_EffectMgr.GetTwinLineEffect().LineYEffect(Board_B[SQR_DIR.RIGHT].transform.position, ColorType.BLUE);


        //날라가는 보석
        effect1.LineShotBigEffect(Board_B.transform.position, color, LineType.LineX);
        effect1.LineShotBigEffect(Board_B.transform.position, color, LineType.LineY);

        //가운데조각이펙트
        //EffectManager.Instance.BigShotEffect(Board_B.transform.position);

        //점수
        //m_UITreeMatch.ScoreText(60, Board_B.transform.position);

        //**********
        //콤바인처리
        //**********
        Board_A.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_B.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        complete();

        //top
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.LEFT], SQR_DIR.TOP, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOP], SQR_DIR.TOP, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.RIGHT], SQR_DIR.TOP, Board_B.m_IsJamBoard));

        //Bottom
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.LEFT], SQR_DIR.BOTTOM, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOM], SQR_DIR.BOTTOM, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.RIGHT], SQR_DIR.BOTTOM, Board_B.m_IsJamBoard));

        //Right
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOP], SQR_DIR.RIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.RIGHT], SQR_DIR.RIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOM], SQR_DIR.RIGHT, Board_B.m_IsJamBoard));

        //Left
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOP], SQR_DIR.LEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.LEFT], SQR_DIR.LEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOM], SQR_DIR.LEFT, Board_B.m_IsJamBoard));

        yield return new WaitForSeconds(0.5f);
        m_MatchMgr.SpecialEffect = false;
        yield return null;
    }
    IEnumerator BigShot_Dir(Board board, SQR_DIR dir, bool jam)
    {
        if (board == null)
            yield break;

        List<Board> list_dir = board.GetBoardDir(dir);

        switch (dir)
        {
            case SQR_DIR.TOP:
            case SQR_DIR.BOTTOM:
            case SQR_DIR.LEFT:
            case SQR_DIR.RIGHT:
                {
                    if(list_dir.Count > 0)
                    {
                        for (int i = 0; i < list_dir.Count; i++)
                        {
                            list_dir[i].Brust(true, 0, true, false);

                            if (list_dir[i].m_Item is SpiralItem) break;    //스파이럴
                            if (list_dir[i].GetPanel<S_TreePanel>() != null) break;          //스페셜트리

                            //연속적으로 잼 증식
                            jam = list_dir[i].JamPanelCreateCountinue(jam);

                            //시작하자마자 그 다음 칸까지 닿아있기 때문에 바로 터트린다.
                            yield return new WaitForSeconds(i == 0 ? 0 : SerialBrustDelay);
                        }
                    }
                }
                break;
            case SQR_DIR.TOPRIGHT:
            case SQR_DIR.TOPLEFT:
            case SQR_DIR.BOTTOMLEFT:
            case SQR_DIR.BOTTOMRIGHT:
                {
                    if (list_dir.Count > 0)
                    {
                        for (int i = 0; i < list_dir.Count; i++)
                        {
                            list_dir[i].Brust(true, 0, true, false);

                            if (list_dir[i].m_Item is SpiralItem) break;    //스파이럴
                            if (list_dir[i].GetPanel<S_TreePanel>() != null) break;          //스페셜트리

                            //연속적으로 잼 증식
                            jam = list_dir[i].JamPanelCreateCountinue(jam);

                            //대각선일때는 길이는 길어졌지만 미사일은 길어지지 않았기 때문에 바로 닿아있지 않아서 바로 터지지 않는다.
                            yield return new WaitForSeconds(i == 0 ? 0.06f : SerialBrustDelay);
                        }
                    }
                }
                break;
            default:
                break;
        }


    }

    public void Ability_BombCross(ColorType color, Action complete)
    {
        StartCoroutine(Co_Ability_BombCross(color, complete));
    }
    IEnumerator Co_Ability_BombCross(ColorType color, Action complete)
    {
        m_MatchMgr.SpecialEffect = true;

        Board Board_A = Item.Swap_A.m_Board;
        Board Board_B = Item.Swap_B.m_Board;

        EffectManager.Instance.ItemChangeEffect(Board_B.transform.position);

        yield return new WaitForSeconds(0.4f);

        //*****************
        //왼쪽 대각선 적용
        //*****************
        //사운드
        SoundManager.Instance.PlayEffect("crossbomb");

        //이펙트 
        LineEffect effect1 = m_EffectMgr.GetLineEffect();
        effect1.CombineBrust_Sprite(Board_B.transform.position, color, true);

        //yield return new WaitForSeconds(1.04f);

        //라인이펙트 추가
        if (Board_B[SQR_DIR.TOP] != null)
            m_EffectMgr.GetTwinLineEffect().LineCEffect(Board_B[SQR_DIR.TOP].transform.position, ColorType.BLUE);
        if (Board_B != null)
            m_EffectMgr.GetTwinLineEffect().LineCEffect(Board_B.transform.position, ColorType.BLUE);
        if (Board_B[SQR_DIR.BOTTOM] != null)
            m_EffectMgr.GetTwinLineEffect().LineCEffect(Board_B[SQR_DIR.BOTTOM].transform.position, ColorType.BLUE);



        //날라가는 보석 
        effect1.LineShotBigEffect(Board_B.transform.position, color, LineType.CrossLT);
        effect1.LineShotBigEffect(Board_B.transform.position, color, LineType.CrossRT);

        //가운데조각이펙트
        //EffectManager.Instance.BigShotEffect(Board_B.transform.position);

        //점수
        //m_UITreeMatch.ScoreText(60, Board_B.transform.position);

       
        //**********
        //콤바인처리
        //**********
        Board_A.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_B.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        complete();

        //*********
        //효과 발동
        //*********
        //왼쪽 대각선
        //TL
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOP], SQR_DIR.TOPLEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.LEFT], SQR_DIR.TOPLEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOPLEFT], SQR_DIR.TOPLEFT, Board_B.m_IsJamBoard));

        //BR
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOM], SQR_DIR.BOTTOMRIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.RIGHT], SQR_DIR.BOTTOMRIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOMRIGHT], SQR_DIR.BOTTOMRIGHT, Board_B.m_IsJamBoard));

        //TR
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOP], SQR_DIR.TOPRIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.RIGHT], SQR_DIR.TOPRIGHT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.TOPRIGHT], SQR_DIR.TOPRIGHT, Board_B.m_IsJamBoard));


        //BL
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOM], SQR_DIR.BOTTOMLEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.LEFT], SQR_DIR.BOTTOMLEFT, Board_B.m_IsJamBoard));
        StartCoroutine(BigShot_Dir(Board_B[SQR_DIR.BOTTOMLEFT], SQR_DIR.BOTTOMLEFT, Board_B.m_IsJamBoard));

 
        //사운드
        //SoundManager.Instance.PlayEffect("bomb");


        yield return new WaitForSeconds(0.5f);
        m_MatchMgr.SpecialEffect = false;
        yield return null;
    }

    public void Ability_BombBomb(Board board, int range, Action complete)
    {
        StartCoroutine(Co_Ability_BombBomb(board, range, complete));
    }
    IEnumerator Co_Ability_BombBomb(Board board, int range, Action complete)
    {
        //사운드, 이펙트 순서는 바뀔수 있다..
        Board Board_A = Item.Swap_A.m_Board;
        Board Board_B = Item.Swap_B.m_Board;

        //****************
        //1차 효과 스케일
        //****************
        Item.Swap_A.transform.position += new Vector3(0,0, -1f); //케이지보다 앞에 나와야 한다.
        Item.Swap_A.transform.DOScale(2.5f, 0.3f);
        yield return new WaitForSeconds(0.3f);

        //*********************
        //2차 전조 이펙트
        //*********************
        m_EffectMgr.SignEffect(Item.Swap_A.m_SpriteRender, 0.5f);
        yield return new WaitForSeconds(0.3f);

        //***************
        //3차 폭발이펙트
        //***************
        m_EffectMgr.BombBigEffect(Board_B.transform.position);

        yield return new WaitForSeconds(0.3f);

        //점수
        //m_UITreeMatch.ScoreText(60, Board_B.transform.position);

        //*********
        //사운드.. 
        //*********
        SoundManager.Instance.PlayEffect("bomb");

        //**********
        //콤바인처리
        //**********
        Board_B.CombineBrust(ItemType.None);//효과 없이 먼저 터져야 중복적용 안됨.

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        complete();

        //**********
        //효과
        //**********
        List<List<Board>> list_boundary = new List<List<Board>>();
        for (int i = 0; i < range; i++)
        {
            List<Board> list_around = Board_B.GetBoardAround(i + 1, true);
            list_boundary.Add(list_around);
        }

        //안쪽부터 밖으로 터트리자!
        for (int i = 0; i < list_boundary.Count; i++)
        {
            for (int k = 0; k < list_boundary[i].Count; k++)
            {
                list_boundary[i][k].Brust(true, 0, true, false);

                if (Board_B.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
            }

            yield return new WaitForSeconds(0.1f);
        }
    }
    #endregion

    #region Rainbow
    public void Ability_Rainbow(Board board, Action complete)
    {
        ColorType color = m_MatchMgr.m_AppearColor[UnityEngine.Random.Range(0, m_MatchMgr.m_AppearColor.Count)];

        //*******************
        //효과 발동 & 이펙트
        //*******************
        StartCoroutine(Co_Ability_RainBowColorBrust(board, color, complete));
    }
    //IEnumerator Co_Ability_Rainbow(Board board)
    //{
    //    //사운드
    //    SoundManager.Instance.PlayEffect("lightning");

    //    //이펙트
    //    //효과적용되는 부분에서 각각적용.

    //    //점수
    //    //m_UITreeMatch.ScoreText(60, board.transform.position);

    //    ColorType color = m_MatchMgr.m_AppearColor[UnityEngine.Random.Range(0, m_MatchMgr.m_AppearColor.Count)];

    //    //*******************
    //    //효과 발동 & 이펙트
    //    //*******************
    //    //레인보우 위에도 동그란이펙트하나 놓을려고.
    //    RainbowEffect eff = EffectManager.Instance.GetRainbowEffect();
    //    eff.SetInfo(board.m_Item.gameObject, board.m_Item.gameObject, Vector3.Distance(board.transform.position, board.transform.position));

    //    Board _bd = null;
    //    for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
    //    {
    //        _bd = MatchManager.Instance.m_ListBoard[i];
    //        if (_bd.m_Item != null)
    //        {
    //            if (_bd.m_Item.m_Color == color)
    //            {
    //                _bd.Brust(false, 1f, true, false);

    //                //이펙트
    //                RainbowEffect rbe = EffectManager.Instance.GetRainbowEffect();
    //                rbe.SetInfo(board.m_Item.gameObject, _bd.m_Item.gameObject, Vector3.Distance(board.transform.position, _bd.transform.position));
    //                //Debug.Log("t_board : " + _bd.X + ":" + _bd.Y + "   " + "dis : " + Vector3.Distance(_bd.transform.position, m_Board.transform.position));

    //                if (board.m_IsJamBoard) _bd.JamPanelCreate(0.1f);
    //            }
    //        }
    //    }

    //    yield return null;
    //}

    public void Ability_RainbowNormal(Board board, Action complete)
    {
        Board Board_R = Item.Swap_A.m_Board;
        Board Board_N = Item.Swap_B.m_Board;

        if (board.Equals(Board_R) == false)
        {
            Board_R = Item.Swap_B.m_Board;
            Board_N = Item.Swap_A.m_Board;
        }

        //*******************
        //효과 발동 & 이펙트
        //*******************
        StartCoroutine(Co_Ability_RainBowColorBrust(board, Board_N.m_Item.m_Color, complete));
    }

    IEnumerator Co_Ability_RainBowColorBrust(Board board, ColorType color, Action complete)
    {
        //사운드
        SoundManager.Instance.PlayEffect("line_ready");

        RainbowItem Rainbow = (RainbowItem)board.m_Item;
        //ParticleSystem Rainbow_Aura_Particle = Rainbow.Aura_Light.GetComponent<ParticleSystem>();
        //TweenRotation Rainbow_Aura_TweenRotation = Rainbow.Aura_Light.GetComponent<TweenRotation>();
        //레인보우 이미지 회전.
        //Rainbow.m_SpriteRender.transform.DORotate(new Vector3(0,0,360), 1, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).SetLoops(-1);// Rotate(Vector3.back * -rotate_speed * Time.deltaTime);

        //레인보우 이미지 확대&앞으로이동
        //Rainbow.transform.DOScale(1.5f, 1);
        Rainbow.m_SpriteRender.transform.localPosition = new Vector3(0, 0, -2.5f);

        //아우라 이펙트 회전 의미가 없는듯.
        //ParticleSystem.RotationOverLifetimeModule rol = Rainbow.Aura_Light.rotationOverLifetime;
        //rol.z = new ParticleSystem.MinMaxCurve(2f);

        //아우라 이펙트를 더 크게 확대
        Rainbow.Aura_Light.transform.DOScale(1.5f, 1);
        Rainbow.Aura_Light.transform.localPosition = new Vector3(0, 0, -2.4f);
        Rainbow.transform.localRotation = Quaternion.identity;
        Rainbow.transform.DOKill();

        Sequence r_seq = DOTween.Sequence();
        r_seq.Append(Rainbow.transform.DOScale(2f, 0.2f).SetEase(Ease.OutQuad));
        r_seq.SetDelay(0.1f);
        r_seq.Append(Rainbow.transform.DORotate(new Vector3(0, 0, 360), 0.3f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(5, LoopType.Incremental));
        r_seq.Append(Rainbow.transform.DOScale(0,0.2f).SetEase(Ease.OutQuad));
        r_seq.AppendCallback(() =>
        {
            Rainbow.Aura_Light.transform.DOKill();
            //Rainbow.Aura_Light.gameObject.SetActive(false);
            //Rainbow.m_SpriteRender.enabled = false;
            //Rainbow.gameObject.SetActive(false);
            //EffectManager.Instance.RainbowBurstEffect(Rainbow.transform.position);
        });
        r_seq.Play();

        yield return new WaitForSeconds(0.5f);

        //사운드
        //SoundManager.Instance.PlayEffect("lightning");

        //*******************
        //효과 발동 & 이펙트
        //*******************
        Board _bd = null;
        float _effecttime = 0.75f;
        float _delaytime = 0.04f;
        float playdeley = 0.00f;

        var targetItems = new List<Item>();
        var targetSequences = new List<Sequence>();
        var waitForNext = new WaitForSeconds(playdeley);

        for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
        {
            _bd = MatchManager.Instance.m_ListBoard[i];
            if (_bd.m_Item != null)
            {
                if (_bd.m_Item.m_Color == color && _bd.m_ItemBrusting == false )
                {
                    //아이템 스케일.
                    //if (_bd.IsNowItemBrust)
                    //{
                    //    _bd.m_Item.transform.DOScale(1.3f, 0.8f);
                    //    _bd.m_Item.transform.DOShakePosition(0.8f, 0.05f);
                    //}
                    var effect = EffectManager.Instance.GetRainbowConvertEffect();
                    var copiedItem = _bd.m_Item;
                    effect.SetInfo(Rainbow.gameObject, copiedItem.gameObject, 0.3f, color, onComplete: () =>
                    {
                        copiedItem.transform.DOKill();

                        Sequence seq = DOTween.Sequence();
                        seq.Append(copiedItem.transform.DOScale(1.25f, 0.2f).SetEase(Ease.OutBack));
                        seq.SetDelay(0.1f);
                        seq.Append(copiedItem.transform.DORotate(new Vector3(0, 0, 360), _effecttime, RotateMode.FastBeyond360).SetEase(Ease.OutQuart));
                        seq.Append(copiedItem.transform.DOLocalMoveX(copiedItem.transform.localPosition.x - 0.02f, 0.03f));
                        seq.Append(copiedItem.transform.DOLocalMoveX(copiedItem.transform.localPosition.x + 0.04f, 0.03f).SetLoops(20, LoopType.Yoyo));
                        seq.OnComplete(() => { copiedItem.transform.localScale = Vector3.one; });
                        seq.Play();

                        targetSequences.Add(seq);
                    });
                    SoundManager.Instance.PlayEffect("swish");

                    targetItems.Add(copiedItem);
                    
                    //yield return waitForNext;

                    //_bd.Brust(false, 0.2f + _effecttime, true, false);


                    //이펙트
                    //RainbowMoveEffect rbe = EffectManager.Instance.GetRainbowMoveEffect();
                    //rbe.SetInfo(board.m_Item.gameObject, _bd.m_Item.gameObject, _effecttime - 0.2f);

                    //RainbowEffect rbe = EffectManager.Instance.GetRainbowEffect();
                    //rbe.SetInfo(board.m_Item.gameObject, _bd.m_Item.gameObject, Vector3.Distance(board.transform.position, _bd.transform.position));
                    //Debug.Log("t_board : " + _bd.X + ":" + _bd.Y + "   " + "dis : " + Vector3.Distance(_bd.transform.position, m_Board.transform.position));

                    //if (board.m_IsJamBoard) _bd.JamPanelCreate(0.2f + 0.1f + _effecttime + _effecttime + playdeley * );

                }
            }
        }

        yield return new WaitForSeconds(_effecttime + 0.25f);

        var se = targetSequences.GetEnumerator();
        while (se.MoveNext())
        {
            se.Current.Complete();
            se.Current.Kill(true);
        }

        var ie = targetItems.GetEnumerator();
        while (ie.MoveNext())
        {
            if(ie.Current is RainbowItem)
            {
                continue;
            }
            if (ie.Current is IRainbowBurst)
            {
                var i_burst = ie.Current as IRainbowBurst;
                i_burst.isRainbowBurst = true;
            }

            ie.Current.m_Board.Brust(false,0f,true,false);
            if (board.m_IsJamBoard) ie.Current.m_Board.JamPanelCreate(0f);
        }
        
        
        //사운드
        SoundManager.Instance.PlayEffect("bomb");

        StartCoroutine(Co_Shake());

        //Rainbow 회전때문에 아우라 이펙트가 자식으로 들어가지 못했다. 따로 관리해줘야 한다.
        //Rainbow.m_SpriteRender.enabled = true;
        yield return null;
        complete();
    }

    public void Ability_RainbowButterfly(Board board)
    {
        StartCoroutine(Co_Ability_RainBowChangeItem(board, ItemType.Butterfly, 0.8f));
    }
    public void Ability_RainbowLine(Board board)
    {
        StartCoroutine(Co_Ability_RainBowChangeItem(board, ItemType.Line_X, 0.8f));
    }
    public void Ability_RainbowCross(Board board)
    {
        StartCoroutine(Co_Ability_RainBowChangeItem(board, ItemType.Line_C, 0.8f));
    }
    public void Ability_RainbowBomb(Board board)
    {
        StartCoroutine(Co_Ability_RainBowChangeItem(board, ItemType.Bomb, 0.5f));
    }
    IEnumerator Co_Ability_RainBowChangeItem(Board board, ItemType type, float delay)
    {
        m_MatchMgr.SpecialEffect = true;

        Board Board_R = Item.Swap_A.m_Board;
        Board Board_L = Item.Swap_B.m_Board;
        if (board.Equals(Board_R) == false)
        {
            Board_R = Item.Swap_B.m_Board;
            Board_L = Item.Swap_A.m_Board;
        }

        //점수
        //m_UITreeMatch.ScoreText(60, Item.Swap_B.m_Board.transform.position);

        //**********
        //콤바인처리
        //**********
        //Board_R.ItemCombineBrust(SerialBrustDelay, ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        //Board_L.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //이펙트
        EffectManager.Instance.BonusItemChangeEffect(Item.Swap_B.transform.position);
        SoundManager.Instance.PlayEffect("ch_2");

        yield return new WaitForSeconds(0.2f);

        //*********
        //효과 발동
        //*********
        Board _bd;
        List<Board> trackingBoards = new List<Board>();
        List<Item> listBrustItem = new List<Item>();
        for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
        {
            _bd = MatchManager.Instance.m_ListBoard[i];
            if (_bd.m_Item != null && !_bd.m_ItemBrusting)
            {
                //if(_bd.m_Item is JellyMon)
                //    continue;

                if (_bd.m_Item.m_Color == Board_L.m_Item.m_Color)
                {
                    //1. 일반블럭 2. 같은 타입 3. 라인으로 들어왔을때 세로타입도 추가
                    if (_bd.m_Item is NormalItem || _bd.m_Item.m_ItemType == type || (type == ItemType.Line_X && _bd.m_Item.m_ItemType == ItemType.Line_Y))
                    {
                        trackingBoards.Add(_bd);
                    }
                }
            }
        }

        //해당 블록에는 튕기지 않기 위해서 
        //추적 리스트에서는 제거, 파괴할 리스트에 미리 추가해둠.
        listBrustItem.Add(Board_L.m_Item);
        trackingBoards.Remove(Board_L);

        trackingBoards.Sort((t, b) =>
        {
            return UnityEngine.Random.Range(-1, 2);
        });

        var count = Mathf.RoundToInt(Mathf.Clamp(trackingBoards.Count / 10f, 1, 5));
        var interval = trackingBoards.Count / count;

        for (int i = 0; i < count - 1; ++i)
        {
            //n-1개는 딜레이없이 진행하기위해 일반 startCoroutine
            StartCoroutine(RainbowChangeEffectVisit(trackingBoards.GetRange(interval * i, interval), Board_L, type));
        }
        //나머지하나는 딜레이를 맞추기 위해 yield return문을 추가.
        yield return StartCoroutine(RainbowChangeEffectVisit(trackingBoards.GetRange(interval * (count - 1), trackingBoards.Count - (interval * (count - 1))), Board_L, type));

        //for (int i = 0; i < trackingBoards.Count; i++)
        //{
        //    //이펙트
        //    var effect = EffectManager.Instance.GetRainbowConvertEffect();
        //    var copiedBoard = trackingBoards[i];
        //    effect.SetInfo(board.gameObject, copiedBoard.gameObject, 0.4f, copiedBoard.m_Item.m_Color, onComplete: () =>
        //    {
        //        RainbowChangeEffectArrived(copiedBoard, type);
        //    });
        //}

        var e = trackingBoards.GetEnumerator();
        while (e.MoveNext())
        {
            listBrustItem.Add(e.Current.m_Item);
        }

        yield return new WaitForSeconds(0.7f);

        for (int i = 0; i < listBrustItem.Count; i++)
        {
            listBrustItem[i].m_Board.Brust(false, i * 0.05f, true, false);
        }


        m_MatchMgr.SpecialEffect = false;

        yield return null;
    }

    private IEnumerator RainbowChangeEffectVisit(List<Board> targetBoards, Board startBoard, ItemType type)
    {
        Board lastBoard = null;

        bool running = true;
        var waitWhileEffectRunning = new WaitWhile(() => running);

        var e = targetBoards.GetEnumerator();
        while (e.MoveNext())
        {
            running = true;
            if (lastBoard != null)
            {
                //이펙트
                var effect = EffectManager.Instance.GetRainbowConvertEffect();
                var copiedBoard = e.Current;
                effect.SetInfo(lastBoard.gameObject, e.Current.gameObject, 0.16f, copiedBoard.m_Item.m_Color, onComplete: () =>
                {
                    running = false;
                    RainbowChangeEffectArrived(copiedBoard, type);
                });
            }
            else
            {
                var effect = EffectManager.Instance.GetRainbowConvertEffect();
                var copiedBoard = e.Current;
                effect.SetInfo(startBoard.gameObject, e.Current.gameObject, 0.16f, copiedBoard.m_Item.m_Color, onComplete: () =>
                {
                    running = false;
                    RainbowChangeEffectArrived(copiedBoard, type);
                });
            }

            yield return waitWhileEffectRunning;

            //listBrustItem.Add(e.Current.m_Item);

            lastBoard = e.Current;
        }
    }

    private void RainbowChangeEffectArrived(Board targetBoard, ItemType type)
    {
        if (type == ItemType.Line_X)
        {
            targetBoard.GenItem(UnityEngine.Random.Range(0, 2) == 0 ? ItemType.Line_Y : ItemType.Line_X, targetBoard.m_Item.m_Color);
        }
        else
        {
            targetBoard.GenItem(type, targetBoard.m_Item.m_Color);
        }

        SoundManager.Instance.PlayEffect("tm");
        EffectManager.Instance.ItemChangeEffect(targetBoard.transform.position);


        //잼증식
        if (Item.Swap_B.m_Board.m_IsJamBoard)
        {
            targetBoard.JamPanelCreate(0);
        }

    }

    IEnumerator Co_Ability_RainBowChangeItem_Food(Board board, ItemType type, float delay)
    {
        m_MatchMgr.SpecialEffect = true;

        Board Board_R = Item.Swap_A.m_Board;
        Board Board_L = Item.Swap_B.m_Board;
        if (board.Equals(Board_R) == false)
        {
            Board_R = Item.Swap_B.m_Board;
            Board_L = Item.Swap_A.m_Board;
        }

        //점수
        //m_UITreeMatch.ScoreText(60, Item.Swap_B.m_Board.transform.position);

        //**********
        //콤바인처리
        //**********
        //Board_R.ItemCombineBrust(SerialBrustDelay, ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        //Board_L.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //*********
        //효과 발동
        //*********
        float burstDelay = 0f;
        Board _bd;
        List<Item> listBrustItem = new List<Item>();
        List<Board> trackingBoards = new List<Board>();

        for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
        {
            _bd = MatchManager.Instance.m_ListBoard[i];
            if (_bd.m_Item != null && !_bd.m_ItemBrusting)
            {
                //if(_bd.m_Item is JellyMon)
                //    continue;

                if (_bd.m_Item.m_Color == Board_L.m_Item.m_Color)
                {
                    //1. 일반블럭 2. 같은 타입 3. 라인으로 들어왔을때 세로타입도 추가
                    if (_bd.m_Item is NormalItem || _bd.m_Item.m_ItemType == type || (type == ItemType.Line_X && _bd.m_Item.m_ItemType == ItemType.Line_Y))
                    {
                        SoundManager.Instance.PlayEffect("tm");
                        //_bd.m_Item.transform.DOScale(1.5f, 0.45f).SetEase(Ease.OutBounce);
                        //이펙트
                        var _copiedBoard = _bd;
                        var i_seq = DOTween.Sequence();
                        i_seq.Append(_bd.m_Item.transform.DOScale(1.6f, 0.1f).SetEase(Ease.InQuad));        //to
                        i_seq.Append(_bd.m_Item.transform.DOScale(1.2f, 0.1f).SetEase(Ease.OutQuad));      //back
                        i_seq.Append(_bd.m_Item.transform.DOScale(1.6f, 0.1f).SetEase(Ease.InQuad));      //to
                        i_seq.Append(_bd.m_Item.transform.DOScale(1.4f, 0.1f).SetEase(Ease.OutQuad));      //back
                        i_seq.Append(_bd.m_Item.transform.DOScale(1.6f, 0.1f).SetEase(Ease.Linear));      //to
                        i_seq.Play();

                        trackingBoards.Add(_bd);
                    }
                }
            }
        }
        var isJamBoard = Item.Swap_B.m_Board.m_IsJamBoard;
        yield return new WaitForSeconds(0.375f);

        var e = trackingBoards.GetEnumerator();
        while (e.MoveNext())
        {
            var _copiedBoard = e.Current;
            Sequence seq = DOTween.Sequence();
            seq.Append(_copiedBoard.m_Item.transform.DOScale(1f, 0.3f).SetEase(Ease.InQuart));
            seq.InsertCallback(0.2f, () =>
            {
                EffectManager.Instance.ItemChangeEffect(_copiedBoard.transform.position);

                SoundManager.Instance.PlayEffect("ch_2");

                if (type == ItemType.Line_X)
                    _copiedBoard.GenItem(UnityEngine.Random.Range(0, 2) == 0 ? ItemType.Line_Y : ItemType.Line_X, _copiedBoard.m_Item.m_Color);
                else
                    _copiedBoard.GenItem(type, _copiedBoard.m_Item.m_Color);

                //잼증식
                if (isJamBoard) _copiedBoard.JamPanelCreate(0);

            });
            seq.Play();

            listBrustItem.Add(e.Current.m_Item);

            burstDelay += 0.07f;

            yield return new WaitForSeconds(0.07f);
        }


        yield return new WaitForSeconds(0.4f);


        for (int i = 0; i < listBrustItem.Count; i++)
        {
            listBrustItem[i].m_Board.Brust(false, i * 0.07f, true, false);
        }

        m_MatchMgr.SpecialEffect = false;

        yield return null;
    }

    public void Ability_RainbowRainbow(Board board, Action complete)
    {
        StartCoroutine(Co_Ability_RainbowRainbow(board, complete));
    }
    IEnumerator Co_Ability_RainbowRainbow(Board board, Action complete)
    {
        m_MatchMgr.SpecialEffect = true;

        Board Board_R1 = Item.Swap_A.m_Board;
        Board Board_R2 = Item.Swap_B.m_Board;
        if (board.Equals(Board_R1) == false)
        {
            Board_R1 = Item.Swap_B.m_Board;
            Board_R2 = Item.Swap_A.m_Board;
        }

        //****************
        //1차 효과 스케일
        //****************
        Item.Swap_A.transform.position += new Vector3(0, 0, -1f); //케이지보다 앞에 나와야 한다.
        Item.Swap_A.transform.DOScale(2.5f, 0.3f);
        yield return new WaitForSeconds(0.3f);

        //*********************
        //2차 전조 이펙트
        //*********************
        m_EffectMgr.SignEffect(Item.Swap_A.m_SpriteRender, 0.5f);
        yield return new WaitForSeconds(0.3f);

        //***************
        //3차 폭발이펙트
        //***************
        m_EffectMgr.BombBigEffect(Board_R2.transform.position);
        yield return new WaitForSeconds(0.3f);

        //큰 폭발 이펙트
        EffectManager.Instance.RainbowBigBrustEffect(Board_R2.transform.position);

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        if (complete != null) complete();

        //**********
        //콤바인처리
        //**********
        Board_R1.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.
        Board_R2.CombineBrust(ItemType.None); //ItemType을 None을 주면 효과없이 그냥 Brust.

        //*********
        //효과 발동
        //*********
        List<List<Board>> list_boundary = new List<List<Board>>();
        for (int i = 0; i < 8; i++)
        {
            List<Board> list_around = Item.Swap_B.m_Board.GetBoardAround(i + 1, true);
            list_boundary.Add(list_around);
        }

        //안쪽부터 밖으로 터트리자!
        for (int i = 0; i < list_boundary.Count; i++)
        {
            for (int k = 0; k < list_boundary[i].Count; k++)
            {
                list_boundary[i][k].Brust(true,  0, true, false);

                if (Item.Swap_B.m_Board.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
            }

            yield return new WaitForSeconds(0.1f);
        }

        m_MatchMgr.SpecialEffect = false;
        yield return null;
    }
    #endregion

    #region Bonus Cross
    float BC_adddelay = 0.03f;
    public void Ability_BonusCross(Board board)
    {
        StartCoroutine(Co_Ability_BonusCross(board));
    }
    IEnumerator Co_Ability_BonusCross(Board board)
    {
        //이펙트 
        var effect = m_EffectMgr.GetTwinLineEffect();
        effect.LineCEffect(board.transform.position, board.m_Item.m_Color, true);
        //effect.LineShotEffect(board.m_Item.m_Color, LineType.CrossLT, true);

        //사운드
        SoundManager.Instance.PlayEffect("line");

        List<Board> list_TL = board.GetBoardDir(SQR_DIR.TOPLEFT, 2);
        List<Board> list_TR = board.GetBoardDir(SQR_DIR.TOPRIGHT, 2);
        List<Board> list_BL = board.GetBoardDir(SQR_DIR.BOTTOMLEFT, 2);
        List<Board> list_BR = board.GetBoardDir(SQR_DIR.BOTTOMRIGHT, 2);
        for (int i = 0; i < list_TL.Count; i++)
        {
            list_TL[i].Brust(true, (SerialBrustDelay+ BC_adddelay) * i, true, false);
        }
        for (int i = 0; i < list_TR.Count; i++)
        {
            list_TR[i].Brust(true, (SerialBrustDelay + BC_adddelay) * i, true, false);
        }
        for (int i = 0; i < list_BL.Count; i++)
        {
            list_BL[i].Brust(true, (SerialBrustDelay + BC_adddelay) * i, true, false);
        }
        for (int i = 0; i < list_BR.Count; i++)
        {
            list_BR[i].Brust(true, (SerialBrustDelay + BC_adddelay) * i, true, false);
        }

        yield return null;
    }
    #endregion

    #region Bonus Bomb
    public void Ability_BonusBomb(Board board, int range)
    {
        StartCoroutine(Co_Ability_BonusBomb(board, range));
    }
    IEnumerator Co_Ability_BonusBomb(Board board, int range)
    {
        //사운드, 이펙트 순서는 바뀔수 있다..

        //*********
        //이펙트 1
        //*********
        //이부분은 이펙트로 바꿔야 한다!!
        //board.m_Item.m_SpriteRender.color = new Color32(255, 255, 255, 100);
        //yield return new WaitForSeconds(0.1f);

        //board.m_Item.m_SpriteRender.color = new Color32(255, 255, 255, 255);
        //yield return new WaitForSeconds(0.1f);

        //board.m_Item.m_SpriteRender.color = new Color32(255, 255, 255, 100);
        //yield return new WaitForSeconds(0.1f);

        //board.m_Item.m_SpriteRender.color = new Color32(255, 255, 255, 255);

        //**********
        //이펙트 2
        //**********
        m_EffectMgr.BombEffect(board.transform.position, ColorType.None);

        yield return new WaitForSeconds(0.3f);


        //*********
        //폭탄흔듦
        //*********
        StartCoroutine(Co_Shake(0.03f));

        //*********
        //사운드.. 
        //*********
        SoundManager.Instance.PlayEffect("bomb");

        //**********
        //효과
        //**********
        if (board[SQR_DIR.TOP] != null)
            board[SQR_DIR.TOP].Brust(true, 0, true, false);
        if (board[SQR_DIR.BOTTOM] != null)
            board[SQR_DIR.BOTTOM].Brust(true, 0, true, false);
        if (board[SQR_DIR.LEFT] != null)
            board[SQR_DIR.LEFT].Brust(true, 0, true, false);
        if (board[SQR_DIR.RIGHT] != null)
            board[SQR_DIR.RIGHT].Brust(true, 0, true, false);

        //List<List<Board>> list_boundary = new List<List<Board>>();
        //for (int i = 0; i < range; i++)
        //{
        //    List<Board> list_around = board.GetBoardAround(i + 1, true);
        //    list_boundary.Add(list_around);
        //}

        ////안쪽부터 밖으로 터트리자!
        ////bool sound = true; //bomb사운드가 있어서 일단 주석.
        //for (int i = 0; i < list_boundary.Count; i++)
        //{
        //    for (int k = 0; k < list_boundary[i].Count; k++)
        //    {
        //        StartCoroutine(Co_Brust(list_boundary[i][k], SerialBrustDelay * i/*, sound*/));
        //        //sound = false;
        //    }
        //    //sound = true;
        //}
    }
    #endregion

    #region CashItem
    public IEnumerator Hammer(Board board, System.Action complete)
    {
        //이펙트
        m_EffectMgr.CashItemEffect(board.transform.position);
        m_EffectMgr.CashItem1Effect(board.transform.position);

        //사운드
        SoundManager.Instance.PlayEffect("bomb");

        StartCoroutine(Co_Shake(0.02f));

        board.Brust(false, 0, true, false);

        //콤보
        m_MatchMgr.ComboPlus(false);

        //complete();

        yield return null;
    }
    public IEnumerator CrossBomb(Board board, System.Action complete)
    {
        //이펙트
        m_EffectMgr.CashItemEffect(board.transform.position);
        m_EffectMgr.CashItem2Effect(board.transform.position);


        //사운드
        SoundManager.Instance.PlayEffect("bomb");

        //흔듦
        StartCoroutine(Co_Shake());

        //*********
        //효과 발동
        //*********
        board.Brust(false, 0, true, false);

        Ability_LineX(board, ColorType.None);
        Ability_LineY(board, ColorType.None);

        //board.ItemBrust(false, 0);

        //List<Board> list_left = board.GetBoardDir(SQR_DIR.LEFT);
        //List<Board> list_right = board.GetBoardDir(SQR_DIR.RIGHT);
        //List<Board> list_top = board.GetBoardDir(SQR_DIR.TOP);
        //List<Board> list_bottom = board.GetBoardDir(SQR_DIR.BOTTOM);

        //Shot_Dir(list_left);
        //Shot_Dir(list_right);

        //Shot_Dir(list_top);
        //Shot_Dir(list_bottom);

        //콤보
        m_MatchMgr.ComboPlus(false);

        //complete();

        yield return null;
    }

    public IEnumerator ColorThunder(Board board, System.Action complete)
    {
        //이펙트
        m_EffectMgr.CashItemEffect(board.transform.position);


        //사운드
        SoundManager.Instance.PlayEffect("lightning");

        //*******************
        //효과 발동 & 이펙트
        //*******************
        Board _bd = null;
        for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
        {
            _bd = MatchManager.Instance.m_ListBoard[i];
            if (_bd.m_Item != null)
            {
                if (_bd.m_Item.m_Color == board.m_Item.m_Color)
                {
                    _bd.Brust(false, 1f, true, false);

                    //이펙트
                    //var effect = EffectManager.Instance.GetRainbowConvertEffect();
                    //effect.SetInfo(board.gameObject, _bd.gameObject, 0.4f, _bd.m_Item.m_Color);

                    //이펙트
                    RainbowEffect rbe = EffectManager.Instance.GetRainbowEffect();
                    rbe.SetInfo(board.m_Item.gameObject, _bd.m_Item.gameObject, Vector3.Distance(board.transform.position, _bd.transform.position));
                    //Debug.Log("t_board : " + _bd.X + ":" + _bd.Y + "   " + "dis : " + Vector3.Distance(_bd.transform.position, m_Board.transform.position));

                    if (board.m_IsJamBoard) _bd.JamPanelCreate(0.1f);
                }
            }
        }

        //콤보
        m_MatchMgr.ComboPlus(false);

        //*********
        //폭탄흔듦
        //*********
        StartCoroutine(Co_Shake(0.03f));
        //complete();

        yield return null;
    }
    #endregion

    #region CakePanel
    public void Ability_CakePanel(Board board, Action complete)
    {
        StartCoroutine(Co_Ability_CakePanel(board, complete));
    }
    IEnumerator Co_Ability_CakePanel(Board board, Action complete)
    {
        m_MatchMgr.SpecialEffect = true;     

        //***************
        //폭발이펙트
        //***************
        m_EffectMgr.BombBigEffect(board.transform.position + new Vector3(MatchDefine.BoardWidth / 2, -MatchDefine.BoardHeight / 2));

        yield return new WaitForSeconds(0.3f);

        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        if (complete != null) complete();

        //*********
        //효과 발동
        //*********
        CakePanel.IsCakeAbility = true;
        List<List<Board>> list_boundary = new List<List<Board>>();
        for (int i = 0; i < 7; i++)
        {
            List<Board> list_around = board.GetBoardAround2X2(i + 1, true);
            list_boundary.Add(list_around);
        }

        //안쪽부터 밖으로 터트리자!
        for (int i = 0; i < list_boundary.Count; i++)
        {
            for (int k = 0; k < list_boundary[i].Count; k++)
            {
                //1. 다른케이크는 Brust되지 않는다. 
                //2. IsCakeAbility와 isAroundBrust 값으로 주변폭발도 막는다.
                if (list_boundary[i][k].m_IsCakeBoard) 
                    continue;

                list_boundary[i][k].Brust(true, 0, true, false);

                if (board.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
            }

            yield return new WaitForSeconds(0.15f);
        }

        m_MatchMgr.SpecialEffect = false;
        CakePanel.IsCakeAbility = false;
        yield return null;
    }
    #endregion

    #region JellyMon
    public void Ability_JellyMon(Board board, int range, Action complete)
    {
        StartCoroutine(Co_Ability_JellyMon(board, range, complete));
    }
    IEnumerator Co_Ability_JellyMon(Board board, int range, Action complete)
    {
        //교체될 위치의 아이템이 효과가 발동된 후 젤리몬으로 교체되기전에 이동되는 문제 때문에 드랍멈춤.
        m_MatchMgr.SpecialEffect = true;

        //**********
        //이펙트 2
        //**********
        m_EffectMgr.CashItem1Effect(board.transform.position);
        //m_EffectMgr.SmokeEffect(board.transform.position);

        //yield return new WaitForSeconds(0.3f);

        //점수
        //m_UITreeMatch.ScoreText(60, board.transform.position);

        //*********
        //폭탄흔듦
        //*********
        StartCoroutine(Co_Shake());

        //*********
        //사운드.. 
        //*********
        SoundManager.Instance.PlayEffect("bomb");

        //**********
        //효과
        //**********
        List<List<Board>> list_boundary = new List<List<Board>>();
        for (int i = 0; i < range; i++)
        {
            List<Board> list_around = board.GetBoardAround(i + 1, true);
            list_boundary.Add(list_around);
        }

        //★★중요★★
        //젤리몬 같은 그자리에 다시 무언가를 생성하는 타입은 젤리몬과 겹친다.
        //그래서 폭발시키지않고 바로 교체해야한다.
        //주변에 스폐셜아이템으로 터지지 않도록 최우선 순위로 교체한다.
        //if (board.m_Item.m_ItemType == ItemType.Mystery)
        if (board.IsItemHaveNextItem)
        {
            //1.젤리몬 교체
            complete();

            //2.주변 터트린다! 폭탄들처럼 딜레이 주지 않는다. 젤리몬이 도착한 보드도 바로 터져야한다.
            for (int i = 0; i < list_boundary.Count; i++)
            {
                for (int k = 0; k < list_boundary[i].Count; k++)
                {
                    list_boundary[i][k].Brust(true, 0, true, false);

                    if (board.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
                }
            }
        }
        else
        {
            //1. 이동자리 블럭 폭발
            board.m_NextItemType = ItemType.JellyMon;
            board.Brust(true, 0, true, false);

            //2. 주변 터트린다! 폭탄들처럼 딜레이 주지 않는다. 젤리몬이 도착한 보드도 바로 터져야한다.
            for (int i = 0; i < list_boundary.Count; i++)
            {
                for (int k = 0; k < list_boundary[i].Count; k++)
                {
                    list_boundary[i][k].Brust(true, 0, true, false);

                    if (board.m_IsJamBoard) list_boundary[i][k].JamPanelCreate(0);
                }
            }

            //3. 현재 위치의 아이템 효과가 끝날때까지 대기.

            while (board.m_ItemBrusting)
                yield return null;

            //4. 젤리몬 교체
            complete();
        }


        //만약 젤리몬이 떨어진곳이 주변에 아무것도 터질수 없는 곳이라면
        //이전에 있던곳이 빈칸이 된다. 무조껀 매칭스텝으로 보내야한다.
        if ( m_MatchMgr.m_StepType != StepType.Matching)
            m_MatchMgr.SetStep(StepType.Matching);

        m_MatchMgr.SpecialEffect = false;
    }
    #endregion

    #region Ghost
    public void Ability_Ghost(Board board, Action complete, bool iscombine)
    {
        StartCoroutine(Co_Ability_Ghost(board, complete, iscombine));
    }
    IEnumerator Co_Ability_Ghost(Board board, Action complete, bool iscombine)
    {
        m_MatchMgr.SpecialEffect = true;

        if (iscombine)
        {
            Board Board_Other = Item.Swap_B.m_Board;
            if (Item.Swap_B.m_Board.Equals(board))
            {
                Board_Other = Item.Swap_A.m_Board;
            }

            //********************
            //매칭된 아이템 폭발
            //*********************
            Board_Other.Brust(false, 0);
        }

        //**********************
        //고스트 1차 이미지 변경
        //**********************
        GhostItem ghost = (GhostItem)board.m_Item;
        ghost.SetBrustSprite();

        //**********************
        //고스트 2차 효과 스케일
        //**********************

        //추가. 포커스초기화가 아래트윈을 삭제하므로 그전에 포커스초기화을 한번 해준다.
        if (MatchManager.Instance.FocusContainsItem<GhostItem>())
            MatchManager.Instance.FocusInit();

        ghost.transform.position += new Vector3(0, 0, -2.5f); //케이지보다 앞에 나와야 한다.
        //ghost.transform.DOScale(2f, 0.3f);
        yield return new WaitForSeconds(0.3f);

        //고스트 애니메이션 및 사운드
        SoundManager.Instance.PlayEffect("ghost_sound");

        ghost.Animation_play();
        //******************************
        //고스트 3차 효과 발동 & 이펙트
        //******************************
        //폭탄 생상 할수 있는 NormalItem 자리만 전부 찾음
        Board _bd = null;
        List<Board> list_normal_bd = new List<Board>();
        List<Board> list_bomb_bd = new List<Board>();
        for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
        {
            _bd = MatchManager.Instance.m_ListBoard[i];
            if (_bd.m_Item is NormalItem)
            {
                list_normal_bd.Add(_bd);
            }
        }

        //폭탄 생성할 3군데 랜덤으로  선택
        int rnd = 0;
        if (list_normal_bd.Count > 3)
        {
            //무한 루프에 걸릴 여지가 있다. 3보다 작을때는 여기를 타지 않는다.
            while (list_bomb_bd.Count < 3)
            {
                rnd = UnityEngine.Random.Range(0, list_normal_bd.Count);
                if (list_bomb_bd.Contains(list_normal_bd[rnd]) == false)
                {
                    list_bomb_bd.Add(list_normal_bd[rnd]);
                }
            }
        }
        else
        {
            list_bomb_bd = list_normal_bd;
        }

        //폭탄 생성하자 + 이펙트      
        for(int i =0; i< list_bomb_bd.Count; i++)
        {
            list_bomb_bd[i].GenItem(ItemType.Bomb, ColorType.Rnd);

            //딸기잼 증식
            if (board.m_IsJamBoard) list_bomb_bd[i].JamPanelCreate(0.9f);

            //이펙트
            ghost.ParticleObjs[i].transform.DOMove(list_bomb_bd[i].m_Item.gameObject.transform.position, 0.3f);

            //GhostEffect rbe = EffectManager.Instance.GetGhostEffect();
            //rbe.SetInfo(ghost.gameObject, list_bomb_bd[i].m_Item.gameObject, Vector3.Distance(ghost.transform.position, list_bomb_bd[i].m_Item.transform.position), 1f);
            //Debug.Log("t_board : " + _bd.X + ":" + _bd.Y + "   " + "dis : " + Vector3.Distance(_bd.transform.position, m_Board.transform.position));
        }

        //터트린다.
        for (int i = 0; i < list_bomb_bd.Count; i++)
        {
            list_bomb_bd[i].Brust(false, 0.3f, true, false);
        }

        yield return new WaitForSeconds(0.4f);
        //**************************
        //현재아이템 완료
        //아이템이 사라져야할 시점.
        //**************************
        if (complete != null) complete();       

        //**********
        //콤바인처리
        //**********
        //각각 다 터지고 있으므로 없음.

        m_MatchMgr.SpecialEffect = false;
    }
    #endregion
}
