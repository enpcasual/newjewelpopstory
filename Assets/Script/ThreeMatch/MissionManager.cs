﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MissionType
{
    OrderN=0,     //특정블럭 수치만큼 모으기
    OrderS,
    Wafer,      //바닥의 얼음 없애기
    Food,       //요리 모으기
    Bear,       //젤리베어 모으기
    IceCream,   //아이스크림 모두 없애기
    Jam,        //바닥을 전부 쨈으로 덮기
    Stele,
}

public enum MissionKind
{
    None = 0, //Colortype과 비교하는곳이 있으니 Orange까지는 값 수정불가.

    //OrderN
    Red,
    Yellow,
    Green,
    Blue,
    Purple,
    Orange,
    Donut,
    Spiral,
    Bread,
    LollyCage,
    IceCage,
    Cracker,
    Bottle,
    S_Tree,

    //OrderS
    Line,
    Line_Line,
    Cross,
    Cross_Line,
    Cross_Cross,  
    Bomb,
    Bomb_Line,
    Bomb_Cross,
    Bomb_Bomb,  
    Rainbow,
    Rainbow_Line,
    Rainbow_Cross,
    Rainbow_Bomb,
    Rainbow_Rainbow,
    TimeBomb,

    //Wafer 미션
    Wafer,

    //Food 미션
    StrawberryCake,//RedCake,
    ChocolatePiece,
    MintCake,
    Parfait,//YellowCake,
    WhiteCake,//BlueCake,
    Hamburger,

    //Bear 미션
    Bear,

    //아이스크림 미션
    IceCream,

    //Jam 미션
    Jam,

    //석판 미션
    Stele,

}

public enum FailType
{
    None,
    Limit_Move,
    TimeBombOver,
    ShufflingOver,
}

public class MissionManager : MonoBehaviour {

    static public MissionManager Instance;

    MatchManager Matchmgr;
    UI_ThreeMatch MatchUI;

    Stage CSD;

    public List<MissionInfo> List_MsInfo_PC = new List<MissionInfo>();
    public List<MissionInfo> List_MsInfo_UI = new List<MissionInfo>();

    public List<MissionInfo> List_MsInfo_Food = new List<MissionInfo>(); //푸드쪽에서는 하나 더 필요하다.

    public int[] oder_speed = new int[4];


    public bool IsTimeBombOver = false;
    public Item TimeBombOver;

    public FailType m_FailType = FailType.None;

    public int Jam_RemainCnt = 0;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Matchmgr = MatchManager.Instance;
        MatchUI = UI_ThreeMatch.Instance;
    }

    #region Mission
    public void MissionSetting(Stage stage)
    {
        m_FailType = FailType.None;
        IsTimeBombOver = false;


        CSD = stage;   

        //무브세팅
        MoveLimitInit();

        //점수세팅
        MatchUI.ScoreUIInit();

        #region 석판 미션- 여기에서 자동으로 세팅됨.
        //석판 미션 세팅하지 않고 석판이 하나라도 있으면 자동 세팅 된다.
        if (CSD.missionInfo.Exists(find => find.type == MissionType.Stele) == false) //중복으로 추가 되지 않도록..(미션이 아예없을수도 있다.)
        {
            int stele_cnt = 0;
            for (int i = 0; i < CSD.panels.Length; i++)
            {
                for (int k = 0; k < CSD.panels[i].listinfo.Count; k++)
                {
                    if (CSD.panels[i].listinfo[k].paneltype == PanelType.Stele)
                    {
                        if (CSD.panels[i].listinfo[k].value == i) //index와 i가 같으면 각 석판중 첫번째
                        {
                            stele_cnt++;
                        }
                    }
                }
            }
            //석판 관련 추가!!
            if (stele_cnt > 0)
            {
                MissionInfo SteleMission = new MissionInfo();
                SteleMission.type = MissionType.Stele;
                SteleMission.kind = MissionKind.Stele;
                SteleMission.count = stele_cnt;
                CSD.missionInfo.Insert(0, SteleMission);
            }
        }
        #endregion

        #region 오더N 미션
        if (CSD.isOrderNMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.Bread)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.Bread_Block);
                }
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.LollyCage)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.Lolly_Cage);
                }
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.IceCage)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.Ice_Cage);
                }
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.Cracker)
                {
                    //방어력이 칼라를 구분하는 용도로 사용됨.
                    CSD.missionInfo[i].count = CheckPanelCnt_NotDef(PanelType.Cracker);
                }
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.Bottle)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.Bottle_Cage);
                }
                if (CSD.missionInfo[i].type == MissionType.OrderN && CSD.missionInfo[i].kind == MissionKind.S_Tree)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.S_Tree);
                }
            }
        }
        #endregion
        #region 오더S 미션
        if (CSD.isOrderSMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.OrderS && CSD.missionInfo[i].kind == MissionKind.TimeBomb)
                {
                    CSD.missionInfo[i].count = CheckItemCnt(ItemType.TimeBomb);
                }
            }
        }
        #endregion

        #region 와퍼 미션
        if (CSD.isWaferMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.Wafer)
                {
                    CSD.missionInfo[i].count = CheckPanelCnt(PanelType.Wafer_floor);  //이부분은 와퍼 갯수를 체크해서 넣어준다.
                }               
            }
        }
        #endregion

        #region 음식 미션
        List_MsInfo_Food.Clear();
        if (CSD.isFoodMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.Food)
                {
                    MissionInfo temp = new MissionInfo();
                    temp.Copy(CSD.missionInfo[i]);
                    List_MsInfo_Food.Add(temp); //푸드 생성할때 사용할 추가 정보.
                }
            }

            food_Spawn = false;
            food_Interval = 0;
            food_SpawnCnt = 0;

            //**********세팅된 푸드에서 미션 감소**************
            //푸드가 갯수가 안맞으면 Llp데이타가 거지같아서 일수도 있다.
            //Llp데이타는 블럭이 존재 하지 않는곳에도 블럭데이타가 있을수 있다.
            //즉, 패널은 블럭이 존재할수 없는데 블럭데이타에는 푸드가 있으면 꼬이겠지.
            for (int i = 0; i < CSD.items.Length; i++)
            {
                if (CSD.items[i] == ItemType.Misson_Food1 || CSD.items[i] == ItemType.Misson_Food2 || CSD.items[i] == ItemType.Misson_Food3
                    || CSD.items[i] == ItemType.Misson_Food4 || CSD.items[i] == ItemType.Misson_Food5 || CSD.items[i] == ItemType.Misson_Food6)
                {
                    CreatFoodItemApply(CSD.items[i]);
                }
            }
        }

        if(CSD.isBearMission)
        {

        }

        #endregion

        #region 아이스크림 미션
        if (CSD.isIceCreamMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.IceCream)
                {
                    CSD.missionInfo[i].count = CheckIceCreamCnt();  //이부분은 갯수체크해서 넣어준다.
                }
            }
        }
        #endregion

        #region 쨈 미션
        if (CSD.isJamMission)
        {
            for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
            {
                if (CSD.missionInfo[i].type == MissionType.Jam)
                {
                    Jam_RemainCnt = CheckNotJamBoard();
                    CSD.missionInfo[i].count = Jam_RemainCnt;  //이부분은 와퍼 갯수를 체크해서 넣어준다.
                }
            }
        }
        #endregion


        #region 미션정보 복사. PC,UI
        List_MsInfo_PC.Clear();
        List_MsInfo_UI.Clear();
        //어짜피 미션별로 체크해서 복사할 필요가 없구나. 와퍼의 갯수같이 예외사항만 선 처리후 그냥 복사.
        for (int i = 0, len = CSD.missionInfo.Count; i < len; i++)
        {
            if (List_MsInfo_PC.Count > 4)
            {
                Debug.LogError("미션갯수가 4개 초과!!!!");
                break;
            }

            MissionInfo temp1 = new MissionInfo();
            temp1.Copy(CSD.missionInfo[i]);
            List_MsInfo_PC.Add(temp1);

            MissionInfo temp2 = new MissionInfo();
            temp2.Copy(CSD.missionInfo[i]);
            List_MsInfo_UI.Add(temp2);
        }
        #endregion

        MatchUI.MissionInit(List_MsInfo_UI);

        SequenceInit();
    }

    public void MissionPlusCount(MissionType type, MissionKind kind, int cnt)
    {
        if (type == MissionType.OrderN && CSD.isOrderNMission == false)
            return;

        if (type == MissionType.OrderS && CSD.isOrderSMission == false)
            return;

        if (type == MissionType.Wafer && CSD.isWaferMission == false)
            return;

        if (type == MissionType.Food && CSD.isFoodMission == false)
            return;

        if (type == MissionType.Bear && CSD.isBearMission == false)
            return;

        if (type == MissionType.IceCream && CSD.isIceCreamMission == false)
            return;

        if (type == MissionType.Jam && CSD.isJamMission == false)
            return;

        //미션 적용
        for (int i = 0; i < List_MsInfo_PC.Count; i++)
        {
            if (List_MsInfo_PC[i].kind == kind)
            {
                List_MsInfo_PC[i].count += cnt;
                List_MsInfo_UI[i].count += cnt;
                MatchUI.MissionUpdate(i, List_MsInfo_UI[i]);

                break;
            }
        }
    }
    
    public int GetMissionLeftCnt(MissionType type, MissionKind kind)
    {
        //미션 적용
        for (int i = 0; i < List_MsInfo_PC.Count; i++)
        {
            if (List_MsInfo_PC[i].kind == kind)
            {
                return List_MsInfo_PC[i].count;
            }
        }
        return 0;
    }

    public void MissionApply(GameObject target, MissionType type, MissionKind kind)
    {
        if (type == MissionType.OrderN && CSD.isOrderNMission == false)
            return;

        if (type == MissionType.OrderS && CSD.isOrderSMission == false)
            return;

        if (type == MissionType.Wafer && CSD.isWaferMission == false)
            return;

        if (type == MissionType.Food && CSD.isFoodMission == false)
            return;

        if (type == MissionType.Bear && CSD.isBearMission == false)
            return;

        if (type == MissionType.IceCream && CSD.isIceCreamMission == false)
            return;

        if (type == MissionType.Jam && CSD.isJamMission == false)
            return;

        //스케일 애니메이션
        bool scale = false;
        if(type == MissionType.Food)
            scale = true;


        //미션 적용
        for (int i = 0; i < List_MsInfo_PC.Count; i++)
        {
            if (List_MsInfo_PC[i].kind == kind)
            {
                if (List_MsInfo_PC[i].count > 0)
                {
                    List_MsInfo_PC[i].count--;

                    if (List_MsInfo_PC[i].kind == MissionKind.Jam)
                    {
                        List_MsInfo_UI[i].count--;
                        MatchUI.MissionUpdate(i, List_MsInfo_UI[i]);
                        //Debug.Log(List_MsInfo_UI[i].count);
                    }
                    else
                    {
                        StartCoroutine(MatchUI.GetMissionItemAnim(target, i, kind, oder_speed[i], scale, () =>
                        {
                            List_MsInfo_UI[i].count--;
                            MatchUI.MissionUpdate(i, List_MsInfo_UI[i]);
                        }));

                        oder_speed[i]++;

                        if (oder_speed[i] > 5)
                            oder_speed[i] = 0;
                    }
                    break;
                }
            }
        }
    }

    //석판은 미션은 한개인데(MissionKind 1개) 석판이미지가 5개로 각각의 이미지가 애니메이션이 되어야 한다.
    //그래서 MissionKind로 모든걸 처리하는 기존 함수를 사용할수 없다. 
    //수정도 고려했으나 함수만 복잡해질꺼 같아서 새로 추가한다.
    public void MissionApply_Stele(GameObject target, MissionType type, MissionKind kind, int steletype)
    {
        //UI카메라뎁스가 상위이기 때문에 UI쪽에서 처리해야 한다.
        //미션 적용
        for (int i = 0; i < List_MsInfo_PC.Count; i++)
        {
            if (List_MsInfo_PC[i].kind == kind)
            {
                if (List_MsInfo_PC[i].count > 0)
                {
                    List_MsInfo_PC[i].count--;


                    StartCoroutine(MatchUI.GetMissionItemAnim_Stele(target, i, kind, oder_speed[i], true, steletype, () =>
                    {
                        List_MsInfo_UI[i].count--;
                        MatchUI.MissionUpdate(i, List_MsInfo_UI[i]);
                    }));

                    oder_speed[i]++;

                    if (oder_speed[i] > 5)
                        oder_speed[i] = 0;
                    break;
                }
            }
        }
    }

    public bool CheckMissionClear()
    {

        for (int i = 0; i < List_MsInfo_PC.Count; i++)
        {
            if (List_MsInfo_PC[i].count > 0)
            {
                return false;
            }
        }

        return true;
    }

    public bool CheckMissionFail()
    {
        if (IsTimeBombOver)
        {
            m_FailType = FailType.TimeBombOver;
            return true;
        }

        if (CSD.limit_Move <= 0)
        {
            m_FailType = FailType.Limit_Move;
            return true;
        }  

        return false;
    }

    public void MissionFailAnimation(System.Action complete)
    {
        StartCoroutine(Co_MissionFailAnimation(complete));
    }

    IEnumerator Co_MissionFailAnimation(System.Action complete)
    {
        if (CSD.limit_Move <= 0)
        {


        }
       else if (IsTimeBombOver)
        {
            if (TimeBombOver != null)
            {
                EffectManager.Instance.BombEffect(TimeBombOver.transform.position, ColorType.None);
                yield return new WaitForSeconds(0.3f);
                EffectManager.Instance.BigBrustEffect(TimeBombOver.transform.position);
                //사운드
                SoundManager.Instance.PlayEffect("timebomb_explode");
                yield return new WaitForSeconds(1f);
            }
        }

        complete();
    }

    public void ContinueBonus(int continueType)
    {
       

        switch(continueType)
        {
            case 0:              
            case 1:
            case 2:
                //이동횟수 증가
                MissionManager.Instance.MoveAddCnt(MoveAddType.Continue, 5);
                MatchUI.MoveLimitSetting(CSD.limit_Move);

                //추가 보너스 아이템 생성
                 Matchmgr.Continue_BonusItem(continueType, ()=>
                 {
                     MatchManager.Instance.SetMatchState(MatchState.Playing);
                 });

                //무브로 끝나도 폭탄횟수가 5보다 작으면 추가..
                for (int i = 0; i < Matchmgr.m_ListBoard.Count; i++)
                {
                    if (Matchmgr.m_ListBoard[i].IsItemExist == false) continue;
                    if ((Matchmgr.m_ListBoard[i].m_Item is TimeBombItem) == false) continue;

                    if(((TimeBombItem)Matchmgr.m_ListBoard[i].m_Item).m_TimeBombCnt < 5)
                        ((TimeBombItem)Matchmgr.m_ListBoard[i].m_Item).m_TimeBombCnt = 5;
                }
                break;
            //case 3:
            //case 4:
            //    break;
            case 5:
                //이동횟수 증가
                if(CSD.limit_Move < 5)
                    MoveAddCnt(MoveAddType.Continue, 5 - CSD.limit_Move);
                MatchUI.MoveLimitSetting(CSD.limit_Move);


                //타임폭탄 카운트 증가
                IsTimeBombOver = false;

                for (int i = 0; i < Matchmgr.m_ListBoard.Count; i++)
                {
                    if (Matchmgr.m_ListBoard[i].IsItemExist == false) continue;
                    if ((Matchmgr.m_ListBoard[i].m_Item is TimeBombItem) == false) continue;

                    ((TimeBombItem)Matchmgr.m_ListBoard[i].m_Item).m_TimeBombCnt += 5;
                }

                MatchManager.Instance.SetMatchState(MatchState.Playing);
                break;
            case 6:
                //이동횟수 증가
                MoveAddCnt(MoveAddType.Continue, 5);
                MatchUI.MoveLimitSetting(CSD.limit_Move);

                //타임폭탄 카운트 증가
                IsTimeBombOver = false;

                for (int i = 0; i < Matchmgr.m_ListBoard.Count; i++)
                {
                    if (Matchmgr.m_ListBoard[i].IsItemExist == false) continue;
                    if ((Matchmgr.m_ListBoard[i].m_Item is TimeBombItem) == false) continue;

                    ((TimeBombItem)Matchmgr.m_ListBoard[i].m_Item).m_TimeBombCnt += 5;
                }

                MatchManager.Instance.SetMatchState(MatchState.Playing);
                break;
        }
    }
    #endregion

    #region Limit Move
    public enum MoveAddType
    {
        None,
        Continue,
        TruckStat,
        Post
    }
    public MoveAddType m_MoveAddtype = MoveAddType.None;
    public bool TruckStat_Used = false;
    public void MoveLimitInit()
    {
        MatchUI.MoveLimitSetting(CSD.limit_Move);
        TruckStat_Used = false;
    }

    public void MoveAddCnt(MoveAddType type, int add)
    {
        m_MoveAddtype = type;
        CSD.limit_Move += add;

        MatchUI.MoveLimitSetting(CSD.limit_Move, true);
    }

    public void MoveLimitApply()
    {
        //무브횟수 감소
        if (CSD.limit_Move > 0)
        {
            CSD.limit_Move--;
            MatchUI.MoveLimitSetting(CSD.limit_Move);
            if(CSD.limit_Move == 5)
            {
                MatchUI.MoveWarning();
            }

            if (CSD.limit_Move == 1 && TruckStat_Used == false)
            {
                TruckStat_Used = true;
                StartCoroutine(MatchUI.FoodTruck_CutScene(TruckAbilityType.MovePlus));
            }
        }
    }  
    public void ApplyOfBonusTime(int remainmove)
    {
        MatchUI.MoveLimitSetting(remainmove);
    }
    #endregion

    #region Sequence
    public void SequenceInit()
    {
        for (int i = 0; i < 4; i++)
        {
            oder_speed[i] = 0;
        }
    }
    #endregion

    #region  공용 추가 함수.
    public int CheckItemCnt(ItemType type)
    {
        int _ItemCnt = 0;

        for (int i = 0; i < CSD.items.Length; i++)
        {
            if (CSD.items[i] == type)
            {
                _ItemCnt++;
            }
        }

        return _ItemCnt;
    }

    public int CheckPanelCnt(PanelType type)
    {
        int _PanelCnt = 0;

        for (int i = 0; i < CSD.panels.Length; i++)
        {
            for (int k = 0; k < CSD.panels[i].listinfo.Count; k++)
            {
                if (CSD.panels[i].listinfo[k].paneltype == type)
                {
                    if (CSD.panels[i].listinfo[k].defence > 0)
                        _PanelCnt += CSD.panels[i].listinfo[k].defence;
                }
            }
        }

        return _PanelCnt;
    }

    public int CheckPanelCnt_NotDef(PanelType type)
    {
        int _PanelCnt = 0;

        for (int i = 0; i < CSD.panels.Length; i++)
        {
            for (int k = 0; k < CSD.panels[i].listinfo.Count; k++)
            {
                if (CSD.panels[i].listinfo[k].paneltype == type)
                {
                    _PanelCnt++;
                }
            }
        }

        return _PanelCnt;
    }

    public int CheckNotJamBoard()
    {
        int _PanelCnt = 0;
        Panel _panel;
        for (int i = 0; i < CSD.panels.Length; i++)
        {
            for (int k = 0; k < CSD.panels[i].listinfo.Count; k++)
            {
                if (CSD.panels[i].listinfo[k].paneltype == PanelType.Default_Empty)
                    break;

                if (CSD.panels[i].listinfo[k].paneltype == PanelType.Jam)
                    break;

                //******************************************************************************
                //이부분은 패널이 생성되기전이므로 Board에 만들어놓은 조건 조합을 사용할수 없다.
                //주석으로 어떤조건과 같은지 표시해놓는다.
                //*******************************************************************************
                _panel = PanelManager.Instance.GetPanelInfo(CSD.panels[i].listinfo[k].paneltype);

                // == Board.IsPanelFixed  //고정장치 패스.
                if (_panel.ItemExist == false && _panel.PanelDestroy == false)
                    break;
                // == Board.IsPanelNeverMission
                if (_panel.TogetherMission == false && _panel.PanelDestroy == false)
                    break;

                //현재 보드의 모든 패널이 문제가 없을때..
                if (k == CSD.panels[i].listinfo.Count-1)
                    _PanelCnt++;
            }     
        }

        return _PanelCnt;
    }
    #endregion

    #region Food 추가 함수.
    List<int> list_index = new List<int>();
    bool food_Spawn = false;
    int food_Interval = 0;   
    int food_SpawnCnt  = 0;
    
    //푸드 생성하는 함수
    public bool CreatFoodItem(Board bd, bool istop = true)
    {
        if (CSD.isFoodMission == false)
            return false;

        if ((CSD.foodSpawnLine[bd.X] == false || (CSD.isUseGravity && CSD.foodSpawnLineY[bd.Y] == false)) && istop) //top 드랍일때만(맨위 or empty 생성기)
            return false;

        if (CSD.food_MaxExist <= FoodItem.m_Count) //게임에 존재할수 있는 갯수 체크
            return false;

        if (CSD.food_Interval > food_Interval)  //인터벌 체크
            return false;

        if (CSD.food_SpawnCnt <= food_SpawnCnt)    //한번에 나올수 있는 갯수 체크
            return false;


        list_index.Clear();
        for (int i = 0; i < List_MsInfo_Food.Count; i++)
        {
            if (List_MsInfo_Food[i].count > 0)
                list_index.Add(i);
        }

        if(list_index.Count == 0) //즉, 갯수가 남은 푸드가 없으면.
            return false;


        int ran = Random.Range(0, list_index.Count);

        MissionInfo foodInfo = List_MsInfo_Food[list_index[ran]];
        switch (foodInfo.kind)
        {
            case MissionKind.StrawberryCake:
                bd.GenItem(ItemType.Misson_Food1, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food1);
                break;
            case MissionKind.ChocolatePiece:
                bd.GenItem(ItemType.Misson_Food2, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food2);
                break;
            case MissionKind.MintCake:
                bd.GenItem(ItemType.Misson_Food3, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food3);
                break;
            case MissionKind.Parfait:
                bd.GenItem(ItemType.Misson_Food4, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food4);
                break;
            case MissionKind.WhiteCake:
                bd.GenItem(ItemType.Misson_Food5, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food5);
                break;
            case MissionKind.Hamburger:
                bd.GenItem(ItemType.Misson_Food6, ColorType.None, 0, true);
                CreatFoodItemApply(ItemType.Misson_Food6);
                break;
        }       

        return true;
    }

    //푸드 생성한걸 적용하는 함수.
    //함수를 따로 뺀이유는 처음보드에 세팅되어 있으면 Create함수를 안타기 때문에.
    public void CreatFoodItemApply(ItemType type)
    {

        food_Spawn = true;
        food_SpawnCnt++;

        MissionKind kindfood = MissionKind.StrawberryCake;
        switch (type)
        {
            case ItemType.Misson_Food1:
                kindfood = MissionKind.StrawberryCake;
                break;
            case ItemType.Misson_Food2:
                kindfood = MissionKind.ChocolatePiece;
                break;
            case ItemType.Misson_Food3:
                kindfood = MissionKind.MintCake;
                break;
            case ItemType.Misson_Food4:
                kindfood = MissionKind.Parfait;
                break;
            case ItemType.Misson_Food5:
                kindfood = MissionKind.WhiteCake;
                break;
            case ItemType.Misson_Food6:
                kindfood = MissionKind.Hamburger;
                break;
        }

        for (int k = 0; k < List_MsInfo_Food.Count; k++)
        {
            if (List_MsInfo_Food[k].kind == kindfood)
            {
                List_MsInfo_Food[k].count--;
                break;
            }
        }
    }

    public void MissionInterval()
    {
        if (CSD.isFoodMission)
        {
            if (food_Spawn)
            {
                food_Spawn = false;
                food_Interval = 0;
                food_SpawnCnt = 0;
            }

            if (CSD.food_MaxExist > FoodItem.m_Count)
            {
                food_Interval++;
            }
        }
    }

    #endregion


    #region IceCream 추가 함수.
    public int CheckIceCreamCnt()
    {
        int _IceCreamCnt = 0;

        for (int i = 0; i < CSD.panels.Length; i++)
        {
            for (int k = 0; k < CSD.panels[i].listinfo.Count; k++)
            {
                if (CSD.panels[i].listinfo[k].paneltype == PanelType.IceCream_Block)
                {
                    if (CSD.panels[i].listinfo[k].defence > 0)
                        _IceCreamCnt += CSD.panels[i].listinfo[k].defence;
                }
            }
        }

        return _IceCreamCnt;
    }
    #endregion
}
