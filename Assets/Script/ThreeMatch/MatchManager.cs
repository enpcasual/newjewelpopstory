﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum MatchState
{
    PrepareGame = 0,
    Playing,
    Shuffling,
    BonusTime,
    GameClear,
    GameFail,
}

public enum TouchState
{
    Switching,      //평소 상태
    CashItemUse,    //캐쉬아이템사용
    JellyMonDrop,   //젤리몬 드랍
    NotTouch,       //터치못하게 
}

public class MatchManager : MonoBehaviour {

    static public MatchManager Instance = null;

    public GameManager m_GameMgr;
    public ItemManager m_ItemMgr;
    public UI_ThreeMatch m_UIMgr;

    //*************
    //Scene Object
    //*************
    public GameObject GameField;
    public Border m_Border;

    //*****************
    //Inspect Variable
    //*****************
    public GameObject Board_Prefab;


    //*********************
    //Game Variable
    //*********************
    public List<Board> m_ListBoard = new List<Board>();
    public List<Board> m_ListDropStart = new List<Board>();
    public List<Board> m_ListDropHead = new List<Board>();       //중력이 아래로향할때 맨위블록
    public Dictionary<StepType, BaseStep> m_DicStep;
    public StepType m_StepType = StepType.Wait;
    public BaseStep m_Step;
    public MatchState m_MatchState = MatchState.Playing;
    public TouchState m_TouchState = TouchState.Switching;

    public int m_DataSet = 0; //Editor에서 선택한 Set가 유지된다. apk에서는 사용안됨.
    public int m_StageIndex;
    public Stage m_CSD;

    public List<ColorType> m_AppearColor;
    public int m_CrazyColorProb;    //크레이지 모드가 적용된 컬러의 출현 확률

    public int ComboCnt = 1;
    public bool SpecialEffect = false;
    public bool ClearStage = false;
    public int CountineFail = 0;
    public int GameScore = 0;

    public int TopEmptyCnt = 0;     //UIBlock에서 사용
    public int BottomEmptyCnt = 0;  //UIBlock에서 사용

    public RewardBox RewardBox = null;

    static public Action SwitchingApply;


    //playtime check
    public bool IsPlaytimeChk = false;
    public float GameStartTime;

    public void VariableInit()
    {
        ComboCnt = 1;
        CountineFail = 0;
        GameScore = 0;
        m_UIMgr.ScoreUpdate(GameScore); //이부분은 게임이 정상적으로 월드맵을 나갔다 들어오면 UI쪽에서 Text를 초기화 해주는데 개발중에는 여기서 초기화.

        FocusInit();
        HintInit();
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;  
    }

    // Use this for initialization
    void Start () {

        //Awake는 다른 Manager들도 Instance 세팅을 한다.
        //그래서 실제 프로세스는 Start부터 들어가야한다. 
        //아니면 Instance가 없는 Manager를 호출할때 에러가 난다.

        //Step 세팅
        m_DicStep = new Dictionary<StepType, BaseStep>();
        m_DicStep[StepType.Wait] = new WaitStep();
        m_DicStep[StepType.Shuffling] = new ShufflingStep();
        //m_DicStep[StepType.Drop] = new DropStep();
        m_DicStep[StepType.Matching] = new MatchingStep();

        m_DicStep[StepType.TimeBomb] = new TimeBombStep();
        m_DicStep[StepType.IceCream] = new IceCreamStep();
        m_DicStep[StepType.ConveyerBelt] = new ConveyerBeltStep();
        m_DicStep[StepType.Chameleon] = new ChameleonStep();
        m_DicStep[StepType.MagicColor] = new MagicColorStep();
        m_DicStep[StepType.BearJump] = new BearJumpStep();
        m_DicStep[StepType.BearSpawn] = new BearSpawnStep();

        //m_DicStep[StepType.Check] = new CheckStep();
        m_DicStep[StepType.Mission] = new MissionStep();
        m_DicStep[StepType.Clear] = new ClearStep();
        m_DicStep[StepType.Fail] = new FailStep();


        m_GameMgr = GameManager.Instance;
        m_ItemMgr = ItemManager.Instance;
        m_UIMgr = UI_ThreeMatch.Instance;


        //보드는 한번 생성해서 재사용하자!
        BoardCreate();

    }

    public void StepInit()
    {
        m_DicStep[StepType.Wait].Step_Init();
        m_DicStep[StepType.Shuffling].Step_Init();
        //m_DicStep[StepType.Drop].Step_Init();
        m_DicStep[StepType.Matching].Step_Init();

        m_DicStep[StepType.TimeBomb].Step_Init();
        m_DicStep[StepType.IceCream].Step_Init();
        m_DicStep[StepType.ConveyerBelt].Step_Init();
        m_DicStep[StepType.Chameleon].Step_Init();
        m_DicStep[StepType.MagicColor].Step_Init();
        m_DicStep[StepType.BearJump].Step_Init();
        m_DicStep[StepType.BearSpawn].Step_Init();

        //m_DicStep[StepType.Check].Step_Init();
        m_DicStep[StepType.Mission].Step_Init();
        m_DicStep[StepType.Clear].Step_Init();
        m_DicStep[StepType.Fail].Step_Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Step != null)
            m_Step.Step_Process();
    }

    #region PlayTimeCheck
    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            IsPlaytimeChk = false;
            //Debug.Log("TimeChk_Pause");
        }
    }
    public void PlayTimeCheck()
    {
        if (IsPlaytimeChk)
        {
            FirebaseManager.Instance.LogEvent_PlayTime(m_StageIndex, (int)(Time.realtimeSinceStartup - GameStartTime));
            //Debug.Log("TimeChk : " + (int)(Time.realtimeSinceStartup - GameStartTime));
        }
    }
    #endregion

    #region EditorMode
    public void GoToEditor()
    {
        SceneManager.LoadScene("EditorMode");
    }
    #endregion

    #region GoogleAnalytics
    public void Anlytics_BonusTime()
    {
        FirebaseManager.Instance.LogEvent_RemainMove(m_StageIndex, m_CSD.limit_Move);
    }

    public void Analytics_GameEnd(bool isClear)
    {
        int playcnt = m_GameMgr.m_GameData.SSD_TotalPlayCnt_Plus(m_StageIndex); //플레이 횟수는 Clear와 Fail 둘다 저장한다.
        FirebaseManager.Instance.LogEvent_Play(m_StageIndex, playcnt); //클리어 한 스테이지는 true

        if (isClear)
        {
            int clearcnt = m_GameMgr.m_GameData.SSD_TotalClearCnt_Plus(m_StageIndex);
            FirebaseManager.Instance.LogEvent_Clear(m_StageIndex, clearcnt, playcnt);

            //계속하기 횟수
            FirebaseManager.Instance.LogEvent_Continue(m_StageIndex, CountineFail); //실패카운터를 그대로 날리면된다.


            FirebaseManager.Instance.LogEvent_Score(MatchManager.Instance.m_StageIndex, GameScore);
            int starcnt = 1;    //클리어 했다면 무조껀 별 한개로 처리!
            if (m_CSD.scoreStar2 <= GameScore)
                starcnt++;
            if (m_CSD.scoreStar3 <= GameScore)
                starcnt++;
            FirebaseManager.Instance.LogEvent_Star(MatchManager.Instance.m_StageIndex, starcnt);
        }
        else
        {
            int failcnt = GameManager.Instance.m_GameData.SSD_TotalFailCnt_Plus(m_StageIndex);
            FirebaseManager.Instance.LogEvent_Fail(m_StageIndex, failcnt, playcnt);

            //계속하기 횟수
            FirebaseManager.Instance.LogEvent_Continue(m_StageIndex, CountineFail - 1); //컨티뉴 횟수이기 때문에 처음실패는 빼준다.
        }

        if (m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Hammer] > 0)
            FirebaseManager.Instance.LogEvent_CashItem_H(m_StageIndex, m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Hammer]);
        if (m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Bomb] > 0)
            FirebaseManager.Instance.LogEvent_CashItem_B(m_StageIndex, m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Bomb]);
        if (m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Lightning] > 0)
            FirebaseManager.Instance.LogEvent_CashItem_L(m_StageIndex, m_UIMgr.CashUseCnt[(int)UI_ThreeMatch.CashItem.Lightning]);
    }
    #endregion

    #region State 관리
    public void SetMatchState(MatchState state)
    {
        m_MatchState = state;

        switch (m_MatchState)
        {
            case MatchState.PrepareGame:
                break;
            case MatchState.Playing:
                {
                    SetStep(StepType.Matching);

                }
                break;
            case MatchState.Shuffling:
                {
                    Shuffling();
                }
                break;
            case MatchState.BonusTime:
                {
                    //Analytics
                    Anlytics_BonusTime();
                   
                    BonusTime();
                }
                break;
            case MatchState.GameClear:
                {
                    SoundManager.Instance.StopBGM(true);

                    Popup_Manager.INSTANCE.popup_index = 16;
                    Popup_Manager.INSTANCE.Popup_Push();

                    StageFailCount(false);

                    //Analytics
                    Analytics_GameEnd(true);

                    //시간 체크
                    PlayTimeCheck();
                }
                break;
            case MatchState.GameFail:
                {
                    switch (MissionManager.Instance.m_FailType)
                    {
                        case FailType.Limit_Move:
                            Popup_Manager.INSTANCE.popup_index = 4;
                            Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().continueType = CountineFail <= 2 ? CountineFail : 2;
                            Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().failCnt = CountineFail + 1;
                            Popup_Manager.INSTANCE.Popup_Push();
                            break;
                        case FailType.TimeBombOver:
                            Popup_Manager.INSTANCE.popup_index = 4;
                            Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().continueType = 5; //m_CSD.limit_Move > 4 ? 5 : 6;
                            Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().failCnt = CountineFail + 1;
                            Popup_Manager.INSTANCE.Popup_Push();
                            break;
                        case FailType.ShufflingOver:
                            Popup_Manager.INSTANCE.popup_index = 14; //바로게임오버
                            Popup_Manager.INSTANCE.Popup_Push();
                            break;
                    }            

                    CountineFail++;        
                    StageFailCount(true);   //현재 스테이지를 기존합쳐서 실패카운트                  
                }
                break;
            default:
                break;
        }
    }
    #endregion

    #region TouchState 관리
    //터치는 item과 패널, UI에서 모두 필요하므로 manager에서 관리
    public void SetTouchState(TouchState state)
    {
        m_TouchState = state;
    }
    #endregion

    #region Step 관리 함수
    public void SetStep(StepType step)
    {
        m_StepType = step;
        m_Step = m_DicStep[m_StepType];
        m_Step.Step_Play();
    }
    #endregion

    #region 게임 시작
    public void StartGame(int stage)
    {
        Popup_Manager.INSTANCE.add_displaychk = false;
        m_StageIndex = stage;
        StartGame(DataManager.LoadStage(stage));
    }

    public void StartGame()
    {

#if UNITY_EDITOR
        if (GameManager.Instance.isPlayFromEditor)
        {
            m_DataSet = UnityEditor.EditorPrefs.GetInt(EditorManager.DataSetKey, 0);
            m_StageIndex = UnityEditor.EditorPrefs.GetInt(EditorManager.SaveStageKey, 1);
        }
#endif
        StartGame(DataManager.LoadStage(m_StageIndex, m_DataSet));

    }

    public void StartGame(Stage stage)
    {
        Debug.Log("게임 시작 버튼 클릭");

        ObjectPool.Instance.Restore_All();

        if (UnityEngine.Random.Range(0, 2) == 0)
            SoundManager.Instance.PlayBGM("play_bgm1");
        else
            SoundManager.Instance.PlayBGM("play_bgm2");

        m_CSD = stage;
        if (!m_CSD.isUseGravity)
            m_CSD.CreateGravity();

        //스텝초기화
        StepInit();

        //변수 초기화
        VariableInit();
        m_ItemMgr.Init();

        m_UIMgr.UI_Init();
        //미션세팅.(미션이 아이템,패널보다 우선세팅. 푸드가 보드에 세팅되어 있으면 푸드미션정보에서 깍아줘야하기 때문)
        MissionManager.Instance.MissionSetting(m_CSD);
        MissionPopupSetting(m_CSD);

        //세팅 부분
        StageSetting(m_CSD);
        CrazyLevelSetting();
        BoardSetting(m_CSD);
        PanelSetting(m_CSD);
        ItemSetting(m_CSD);

        BoardPosionSetting();

        RewardBox.ReStart();

        GravityDisplayer.ClearData();
        SetMatchState(MatchState.PrepareGame);

        //게임 시작시간
        IsPlaytimeChk = true;
        GameStartTime = Time.realtimeSinceStartup;

    }

    public void StartFoodCarAbility()
    {
        StartCoroutine(m_UIMgr.FoodTruck_CutScene(TruckAbilityType.ButterFlyPlus));
    }

    #endregion

    #region 게임 종료
    public void EndGame()
    {
        if (TutorialManager.Instance.isTutorial)
            TutorialManager.Instance.Tutorial_End();

        GameField.SetActive(false);
        GameManager.Instance.SetGameState(GAMESTATE.WorldMap);
    }
    #endregion

    #region 스테이지 세팅
    private void StageSetting(Stage stage)
    {
        //***************
        //출현 컬러 세팅
        //***************
        m_AppearColor.Clear();
        for (int i = 0, len = stage.appearColor.Length; i < len ; i++)
        {
            if(stage.appearColor[i])
                m_AppearColor.Add((ColorType)i+1);
        }
    }

    #endregion

    #region 크레이지레벨링
    public void CrazyLevelSetting()
    {
        m_CrazyColorProb = 0;

        //미션레벨은 적용안함. 미션레벨관련 데이타는 클라우드세이브도 안한다.
        if(m_StageIndex > 10000)
            return;
        int failcnt = m_GameMgr.m_GameData.SSD_GetData(m_StageIndex).FailCount;
        if (failcnt < 3)
        {
            //아무효과없음
        }
        else if (failcnt == 3)
        {
            //3번 실패   무브 +3 증가
            MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.None, 3);
        }
        else if (failcnt >= 4 && failcnt <= 7)
        {
            //4~7번 실패   무브 +3 증가, 블록1 출현확률 70%, 50%, 30%, 0%
            MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.None, 3);

            bool _ok = false;

            for (int i = 6; i >= 1; i--)
            {
                _ok = false;
                if (m_AppearColor.Count <= 4)
                    break;

                if (m_AppearColor.Contains((ColorType)i))
                {
                    if (m_CSD.isOrderNMission)
                    {                       
                        if (m_CSD.missionInfo.Exists(find => find.kind == (MissionKind)i)) //미션컬러이면 통과
                            _ok = false;
                        else
                            _ok = true;
                    }
                    else
                    {
                        _ok = true;
                    }
                }

                if (_ok)
                {
                    if (failcnt == 7)
                        m_AppearColor.Remove((ColorType)i); //7일때는 아예 출현하지 않음.
                    else
                    {
                        m_CrazyColorProb = 7 - (failcnt - 4) * 2; //출현확률이 70%, 50%, 30% 줄어들수록 게임이 쉬워짐.

                        //현재 컬러의 인덱스를 마지막으로 이동.
                        m_AppearColor.Remove((ColorType)i);
                        m_AppearColor.Add((ColorType)i);
                    }

                    break; //무조껀 한개 깍았으면 끝!
                }
            }

        }
        else if (failcnt >= 8)
        {
            //8번 이상 실패   무브 +5 증가, 블럭1 출현확률 0%, 블럭2 출현확률 70%, 50%, 30%, 0%
            MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.None, 5);

            bool _ok = false;
            bool _scondBlock = false;

            for (int i = 6; i >= 1; i--)
            {
                _ok = false;
                if (m_AppearColor.Count <= 4)
                    break;

                if (m_AppearColor.Contains((ColorType)i))
                {
                    if (m_CSD.isOrderNMission)
                    {
                        if (m_CSD.missionInfo.Exists(find => find.kind == (MissionKind)i)) //미션컬러이면 통과
                            _ok = false;
                        else
                            _ok = true;
                    }
                    else
                    {
                        _ok = true;
                    }
                }

                if (_ok)
                {
                    if (_scondBlock == false)//첫번째 블럭효과
                    {
                        m_AppearColor.Remove((ColorType)i);
                    }
                    else //두번째 블럭효과
                    {
                        if (failcnt >= 11)
                            m_AppearColor.Remove((ColorType)i);
                        else
                        {
                            m_CrazyColorProb = 7 - (failcnt - 8) * 2; //출현확률이 70%, 50%, 30% 줄어들수록 게임이 쉬워짐.

                            //현재 컬러의 인덱스를 마지막으로 이동.
                            m_AppearColor.Remove((ColorType)i);
                            m_AppearColor.Add((ColorType)i);
                        }

                        break; //두번째 효과를 줬으면 끝.(기존에는 무조껀 Remove였기때문에 갯수에 걸렸지만 이제는 5개로 남을수 있어서 여기서 끝내줘야함.)
                    }            
                    _scondBlock = true;
                }
            }
        }
    }
    public void StageFailCount(bool isfail)
    {
        SSD _ssd = m_GameMgr.m_GameData.SSD_GetData(m_StageIndex);
        if (isfail)       
            _ssd.FailCount += 1; 
        else
            _ssd.FailCount = 0;

        m_GameMgr.m_GameData.SSD_Save(m_StageIndex, _ssd);
    }
    #endregion

    #region 미션팝업 세팅
    public void MissionPopupSetting(Stage stage)
{
    Popup_Manager.INSTANCE.popup_index = 6;
    Popup_Manager.INSTANCE.Popup_Push();
    Popup_Manager.INSTANCE.popup_list[6].GetComponent<MissionPop>().MissioninfoSetting(stage);
}
    #endregion

    #region 보드 세팅
    private void BoardCreate()
    {
        Board _board;
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                GameObject boardObj = ObjectPool.Instance.GetObject(Board_Prefab, GameField.transform);
                _board = boardObj.GetComponent<Board>();
                m_ListBoard.Add(_board);
            }
        }
    }
    private void BoardSetting(Stage stage)
    {
        int i = 0;
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                m_ListBoard[i].Init(x, y, stage);
                i++;
            }
        }

        //아이템,패널 추가관리변수 초기화
        Board.AddMgrVarClear();
        

        //특수 패널 초기화..
        Board.SpecialPanelDestory();
    }

    //패널을 세팅한 다음에 한다.
    public void BoardPosionSetting()
    {
        GameField.SetActive(true);

        //보드판 위치 세팅
        int leftCnt = 0;
        int rightCnt = 0;
        int topCnt = 0;
        int bottomCnt = 0;

        Board bd = null;
        bool EmptyBoard = false;

        //left 체크
        for (int x = 0; x < MatchDefine.MaxX; x++)
        {
            for (int y = 0; y < MatchDefine.MaxY; y++)
            {
                bd = GetBoard(x, y);
                EmptyBoard = false;
                for (int k = 0, len = bd.m_ListPanel.Count; k < len; k++)
                {
                    if (bd.m_ListPanel[k] is EmptyPanel)
                        EmptyBoard = true;
                }

                if (EmptyBoard == false)
                    break;
            }

            if (EmptyBoard == false)
            {
                leftCnt = x;
                break;
            }
        }

        //right 체크
        for (int x = MatchDefine.MaxX - 1; x >= 0; x--)
        {
            for (int y = 0; y < MatchDefine.MaxY; y++)
            {
                bd = GetBoard(x, y);
                EmptyBoard = false;
                for (int k = 0, len = bd.m_ListPanel.Count; k < len; k++)
                {
                    if (bd.m_ListPanel[k] is EmptyPanel)
                        EmptyBoard = true;
                }

                if (EmptyBoard == false)
                    break;
            }

            if (EmptyBoard == false)
            {
                rightCnt = (MatchDefine.MaxX - 1) - x;
                break;
            }
        }

        //top
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                bd = GetBoard(x, y);
                EmptyBoard = false;
                for (int k = 0, len = bd.m_ListPanel.Count; k < len; k++)
                {
                    if (bd.m_ListPanel[k] is EmptyPanel)
                        EmptyBoard = true;
                }

                if (EmptyBoard == false)
                    break;
            }

            if (EmptyBoard == false)
            {
                topCnt = y;
                break;
            }
        }

        //bottom
        for (int y = MatchDefine.MaxY - 1; y >= 0; y--)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                bd = GetBoard(x, y);
                EmptyBoard = false;
                for (int k = 0, len = bd.m_ListPanel.Count; k < len; k++)
                {
                    if (bd.m_ListPanel[k] is EmptyPanel)
                        EmptyBoard = true;
                }

                if (EmptyBoard == false)
                    break;
            }

            if (EmptyBoard == false)
            {
                bottomCnt = (MatchDefine.MaxY - 1) - y;
                break;
            }
        }

        int emptyXCnt = rightCnt - leftCnt;
        int emptyYCnt = topCnt - bottomCnt;

        //UIBlock에서 사용
        TopEmptyCnt = topCnt;
        BottomEmptyCnt = bottomCnt;

        float y_Offset = 0;
        //------------------UI 중간 위치_ Offset----------------
        Vector3 screenpos = m_GameMgr.UICamera.WorldToScreenPoint(m_UIMgr.Top_Point.position);
        Vector3 matchpos_Top = m_GameMgr.MatchCamera.ScreenToWorldPoint(screenpos);
        screenpos = m_GameMgr.UICamera.WorldToScreenPoint(m_UIMgr.Bottom_Point.position);
        Vector3 matcpos_Bottom = m_GameMgr.MatchCamera.ScreenToWorldPoint(screenpos);
        y_Offset += (matchpos_Top.y + matcpos_Bottom.y) / 2;
        //----------------------------------------------

        //DefaultPos란 9*9가 모두있을때 정 중앙을 맞춰놓은 위치.
        Vector3 DefaultPos = new Vector3(-MatchDefine.BoardWidth * 4f, MatchDefine.BoardHeight * 4f + y_Offset, 0);
        GameField.transform.position = DefaultPos + new Vector3(emptyXCnt * MatchDefine.BoardWidth / 2, emptyYCnt * MatchDefine.BoardHeight / 2, 0);

    }
    #endregion

    #region 패널 생성
    private void PanelSetting(Stage stage)
    {
        Board board;
        int index = 0;
        List<Point> Showboards = new List<Point>();
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                board = GetBoard(x, y);
                if (board != null)
                {
                    //추후 여기에서 데이타에 세팅된 패널들이 추가 되어야 한다!!
                    foreach (PanelData info in stage.panels[index].listinfo)
                    {
                        switch(info.paneltype)
                        {
                            case PanelType.Default_Full:
                                {
                                    board.DefaultPanel(info.paneltype, info.defence, info.value, info.addData);
                                    //테두리 제작용
                                    Point point = new Point();
                                    point.x = board.X;
                                    point.y = board.Y;
                                    Showboards.Add(point); 
                                }
                                break;
                            case PanelType.Warp_In:
                            case PanelType.Warp_Out:
                            case PanelType.FoodArrive:
                            case PanelType.JellyBearStart:
                            case PanelType.ConveyerBelt:
                                {
                                    //패널의 세팅과 무관하게 행동하는 패널은 따로 관리해서
                                    //조건체크하는 부분의 부하를 줄여준다.
                                    board.GenSpecialPanel(info.paneltype, info.defence, info.value, info.addData);
                                }
                                break;
                            default:
                                board.GenPanel(info.paneltype, info.defence, info.value, info.addData);
                                break;
                        }
                    }
                }

                index++;
            }
        }

        //info.addData는 모든 세팅이 끝난 후 해당 패널에서 접근되므로 전체체크는 여기에서 한다.
        ConveyerBeltAddSetting();

        m_Border.SetBorderMesh(Showboards);


        //드랍 시작 보드 세팅
        //DropStartSetting();
        m_ListDropStart.Clear();
        foreach (var ok in m_ListBoard)
        {
            if (ok.isListDrop)
            {
                m_ListDropStart.Add(ok);
            }
        }

        if (m_ListDropStart.Count == 0)
        {
            GetBoardDropStartSetting();
            //GetlineDropStartSetting();

        }

        m_ListDropHead.Clear();
        GetGravitySetting();
    }

    public void DropStartSetting()
    {
        m_ListDropStart.Clear();
        Board _board = null;
        Board _bottom = null;
        for (int i = 0; i < m_ListBoard.Count ; i++)
        {
            _board = m_ListBoard[i];
            _bottom = m_ListBoard[i][SQR_DIR.BOTTOM];

            //****************
            //현재 보드 체크
            //****************
            if (_board.IsPanelFixed || _board.m_IsWarpInBoard)
                continue;

            //****************************
            //Bottom이 Empty일때 추가처리
            //*****************************
            if (_bottom != null)
            {
                if (_bottom.GetPanel(PanelType.Default_Empty) != null) //Bottom이 Empty일때는 그 아래 아래를 체크해야한다.
                {
                    while (true)
                    {
                        _bottom = _bottom[SQR_DIR.BOTTOM];
                        if (_bottom == null) break;
                        if (_bottom.GetPanel(PanelType.Default_Empty) == null) break;
                    }
                }
            }

            //*****************
            //Start board 추가
            //******************
            if (_bottom == null)
            {
                //맨 아래줄일때는 추가.
                m_ListDropStart.Add(_board);
            }
            else
            {
                if (_bottom.IsPanelFixed || _bottom.m_IsWarpOutBoard)
                {
                    m_ListDropStart.Add(_board);
                }
            }
        }

        Debug.Log("★★★★★★★ LineCount ★★★★★★★ " + m_ListDropStart.Count);
    }

    public void GetBoardDropStartSetting()
    {
        Board _board = null;
        Board _drop_Dir = null;
        for (int i = 0; i < m_ListBoard.Count; i++)
        {
            _board = m_ListBoard[i];
            _drop_Dir = m_ListBoard[i].Drop;

            //****************
            //현재 보드 체크
            //****************
            if (_board.IsPanelFixed)// || _board.m_IsWarpInBoard)
                continue;

            if (_board.PossibleDrop_Dirs.Count > 1)
            {
                m_ListDropStart.Add(_board);
            }

            if (_drop_Dir == null || _drop_Dir.IsPanelFixed || _drop_Dir.PossibleDrop_Dirs.Count > 1)// || _drop_Dir.m_IsWarpOutBoard)
            {
                List<Board> boards = FindListDropBoards(_board);
                for (int j = 0; j < boards.Count; j++)
                {
                    if (!m_ListDropStart.Contains(boards[j]))
                    {
                        m_ListDropStart.Add(boards[j]);
                    }
                }
            }
        }

        Debug.Log("★★★★★★★ LineCount ★★★★★★★ " + m_ListDropStart.Count);
        //foreach (var temp in m_ListDropStart)
        //{
        //    Debug.Log(temp.X + " : " + temp.Y);
        //}
    }

    public void GetGravitySetting()
    {
        Board _board = null;
        for (int i = 0; i < m_ListBoard.Count; i++)
        {
            _board = m_ListBoard[i];

            bool dropExist = false;

            var e = _board.Drops.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.m_IsCreatorEmpty)
                    dropExist = true;
            }

            if (dropExist == false)
                m_ListDropHead.Add(_board);

        }

        Debug.Log("★★★★★★★ ListDropHead ★★★★★★★ " + m_ListDropHead.Count);
    }


    //item이 중력으로 갈 수 있는 마지막 위치를 찾아줌.
    private List<Board> FindListDropBoards(Board board)
    {
        List<Board> returnBoards = new List<Board>();
        for (int i = 0; i < (int)DROP_DIR.List; i++)
        {
            if (!board.IsPanelFixed)
            {
                if (board[i] != null && !board[i].IsPanelFixed)
                {
                    if (board[i].PossibleDrop_Dirs.Count > 1)
                    {
                        if (IsCheckSameDirection(board[i].PossibleDrop_Dirs, board.Drop_Dir))
                        {
                            returnBoards.Add(board);
                        }
                    }
                    else if (board[i].Drop == board)
                    {
                        //if (board[(DROP_DIR)i].m_IsWarpOutBoard || board[(DROP_DIR)i].m_IsWarpInBoard && board[(DROP_DIR)i].Drop_Dir == board.Drop_Dir)
                        //    continue;
                        if (board[i].m_IsWarpOutBoard && board[i].Drop_Dir != board.Drop_Dir)
                            continue;
                        if (board.m_IsWarpInBoard && board[i].Drop_Dir == board.Drop_Dir)
                            continue;

                        if (board[i] != null && (board[i].IsPanelFixed || board[i].m_IsWarpOutBoard))
                        {
                            if (!returnBoards.Contains(board))
                            {
                                returnBoards.Add(board);
                            }
                        }

                        List<Board> _boards = FindListDropBoards(board[i]);
                        if (_boards.Count == 0)
                        {
                            returnBoards.Add(board[i]);
                        }

                        returnBoards.AddRange(_boards);
                    }
                }
            }
        }

        if (board.m_IsWarpInBoard && board.m_WarpOutBoard != null && !board.m_WarpOutBoard.IsPanelFixed)
        {
            List<Board> _boards = FindListDropBoards(board.m_WarpOutBoard);
            if (_boards.Count == 0)
            {
                returnBoards.Add(board.m_WarpOutBoard);
            }

            returnBoards.AddRange(_boards);
        }

        if (returnBoards.Count == 0)
        {
            returnBoards.Add(board);
        }

        return returnBoards;
    }

    private bool IsCheckSameDirection(List<DROP_DIR> dirList, DROP_DIR dir)
    {
        for (int i = 0; i < dirList.Count; i++)
        {
            if (dirList[i] == dir)
                return true;
        }

        return false;
    }

    public void ConveyerBeltAddSetting()
    {
        for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
        {
            switch (Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.Type)
            {
                case ConveyerBeltType.Start:
                    {
                        for(int k =0 ; k < Board.m_ListConveyerBeltPanel.Count; k++)
                        {
                            if(Board.m_ListConveyerBeltPanel[k].m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
                            {
                                if(Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.WaropIndex == Board.m_ListConveyerBeltPanel[k].m_ConveyerBeltInfo.WaropIndex)
                                {
                                    //시작 컨베이어벨트에서 끝 컨베이어벨트 보드 캐싱
                                    Board.m_ListConveyerBeltPanel[i].EndBoard = Board.m_ListConveyerBeltPanel[k].m_Board;
                                    //끝 컨베이어벨트에서 시작 컨베이서벨트 보드 캐싱
                                    //Board.m_ListConveyerBeltPanel[k].StartBoard = Board.m_ListConveyerBeltPanel[i].m_Board;
                                }
                            }
                        }
                    }
                    break;
                case ConveyerBeltType.End:
                    {
                       //Start에서 같이 처리함.
                    }
                    break;
                case ConveyerBeltType.Mid:
                    break;
            }
        }

        #region 컨베이어 벨트 화살표 순서대로 켜지는 시스템.
        /*
        //**********************************************************************************
        //컨베이어벨트 화살표가 제대로 깜박이기 위해 순서를 체크해서 FirstTiming를 세팅한다.
        //시작점만 찾는다.(루프도 처음발견한 보드가 시작점)
        Dictionary<int, ConveyerBeltPanel> dic_panel = new Dictionary<int, ConveyerBeltPanel>();
        //스타트점이 있는것.
        for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
        {
            if (Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
                dic_panel[Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.WaropIndex] = Board.m_ListConveyerBeltPanel[i];
        }
        //루프일때 스타트.
        for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
        {
            if (dic_panel.ContainsKey(Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.WaropIndex) == false)
                dic_panel[Board.m_ListConveyerBeltPanel[i].m_ConveyerBeltInfo.WaropIndex] = Board.m_ListConveyerBeltPanel[i];
        }

        foreach (KeyValuePair<int, ConveyerBeltPanel> dic in dic_panel)
        {
            ConveyerBeltPanel Conpanel = dic.Value;
            int arrowcnt = 0;
            while (true)
            {
                Conpanel.FirstTiming = arrowcnt % 2 == 0 ? true : false;
                if (Conpanel.isStraight)
                    arrowcnt += 2;
                else
                    arrowcnt += 1;

                Conpanel = GetNextCoveyerBelt(Conpanel);
                if (Conpanel == null || dic.Value == Conpanel)
                    break;
            }
        }
        //**********************************************************************************
        */
        #endregion
    }

    public ConveyerBeltPanel GetNextCoveyerBelt(ConveyerBeltPanel panel)
    {
        if (panel.m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
            return null;

        Board NextBoard = null;
        switch (panel.m_ConveyerBeltInfo.Dir_Out)
        {
            case ConveyerBeltDir.Top:
                    NextBoard = panel.m_Board[SQR_DIR.TOP];
                break;
            case ConveyerBeltDir.Bottom:
                    NextBoard = panel.m_Board[SQR_DIR.BOTTOM];
                break;
            case ConveyerBeltDir.Left:
                NextBoard = panel.m_Board[SQR_DIR.LEFT];
                break;
            case ConveyerBeltDir.Right:
                NextBoard = panel.m_Board[SQR_DIR.RIGHT];
                break;
        }

        if(NextBoard == null)
        {
            Debug.LogError("컨테이너벨트를 따라가는중에 보드가 없네?? ");
        }
        ConveyerBeltPanel cp = NextBoard.GetConveyerBeltPanel();
        return cp;
    }
    #endregion

    #region 아이템 생성
    public void ItemSetting(Stage stage)
    {
        //ObjectPool_ItemInit(); //판마다 초기화 한다.(원인불명의 버그)

        StartCoroutine(RegenMatches(stage));

    }

    //일반아이템이 밝게 변해서 보드에 남아서 겹쳐있는 문제가 있다. 원인 모름.
    //그래서 매판마다 초기화.
    public void ObjectPool_ItemInit()
    {
        m_ItemMgr.Init_ObjectPool_Item();
    }

    IEnumerator RegenMatches(Stage stage, bool onlyFalling = false)
    {
        //TestScript.StartTime = Time.time;
        //for (int i = 0; i < TestScript.TestCnt; i++)
        {
            GenerateNewItems(stage, false);

            yield return new WaitForFixedUpdate();
            bool isMatches = true;
            while (isMatches)
            {
                isMatches = MatchsCheckAndChange(stage);

                //TestScript.LoopCnt++;
            }

            //매칭되는게 없으면 셔플링
            start_Shuffling();
        }
        //TestScript.EndTime = Time.time;
        //TestScript.ShowTestLog();
    }

    void GenerateNewItems(Stage stage, bool falling = true)
    {
        Board board;
        int index = 0;
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                board = GetBoard(x, y);
                if (board != null)
                {
                    if (board.IsNowItemEmpty)
                        GetBoard(x, y).GenItem(stage.items[index], stage.colors[index]);
                }

                index++;
            }
        }

    }

    #endregion

    #region 시작애니메이션
    void AnimateField()
    {
        Vector3 fieldPos = new Vector3(-MatchDefine.BoardWidth * 4f, MatchDefine.MaxY / 2.75f, -10);
        float yOffset = -1;
        //if (target == Target.INGREDIENT)
        //yOffset = 0.3f;
        Animation anim = GameField.GetComponent<Animation>();
        AnimationClip clip = new AnimationClip();
        AnimationCurve curveX = new AnimationCurve(new Keyframe(0, fieldPos.x + 15), new Keyframe(0.7f, fieldPos.x - 0.2f), new Keyframe(0.8f, fieldPos.x));
        AnimationCurve curveY = new AnimationCurve(new Keyframe(0, fieldPos.y + yOffset), new Keyframe(1, fieldPos.y + yOffset));
#if UNITY_5
        clip.legacy = true;
#endif
        clip.SetCurve("", typeof(Transform), "localPosition.x", curveX);
        clip.SetCurve("", typeof(Transform), "localPosition.y", curveY);
        //clip.AddEvent(new AnimationEvent() { time = 1, functionName = "EndAnimGamField" });

        ////이벤트추가..
        //AnimationEvent animationEvent = new AnimationEvent();
        //animationEvent.time = clip.length;
        //animationEvent.functionName = "AiStart";
        //clip.AddEvent(animationEvent);

        //클립추가
        anim.AddClip(clip, "appear");
        anim.Play("appear");
        GameField.transform.position = new Vector2(fieldPos.x + 15, fieldPos.y + yOffset);

    }
    #endregion

    #region Board 관리 함수
    public Board GetBoard(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < MatchDefine.MaxX && y < MatchDefine.MaxY)
        {
            //x = Mathf.Clamp(row, 0, maxRows - 1);
            //y = Mathf.Clamp(col, 0, maxCols - 1);
            return m_ListBoard[y * MatchDefine.MaxX + x];
        }
        else {
            return null;
        }
    }

    public bool CheckDropEnd(int x, int y)
    {
        bool ok = true;
        for (int i = 0; i < MatchDefine.MaxY; i++)
        {
            if (m_ListBoard[MatchDefine.MaxX * i + x].m_Item == null)
                ok = false;
        }
        return ok;
    }
    #endregion

    #region 매칭 관리 함수
    //게임 시작시 매칭되는 Item의 타입(컬러)를 수정
    public bool MatchsCheckAndChange(Stage stage, int matches = 3)
    {
        bool ret = false;
        Board _board = null;

        int index = 0;
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                _board = GetBoard(x, y);

                if (_board != null)
                {
                    if (stage.colors[index] == ColorType.Rnd)
                    {
                        //TestScript.WorkCnt++;
                        List<Board> newCombine = _board.FindMatchesAround();

                        if (newCombine.Count > 0)
                        {
                            //TestScript.SetColorCnt++;
                            _board.m_Item.SetColorRandomOther();
                            ret = true;
                        }
                    }

                    //컬러링 하고 시작부터 같지 않도록..
                    if (_board.PossibleDrop_Dirs.Count == 1 && _board.Drop != null && _board.m_IsRingBoard)
                    {
                        if (_board.Drop.m_Item != null && _board.GetPanel<RingPanel>().m_Color == _board.Drop.m_Item.m_Color)
                        {
                            _board.Drop.m_Item.SetColorRandomOther();
                            ret = true;
                        }
                    }
                    else if (_board.PossibleDrop_Dirs.Count > 1)
                    {
                        List<Board> drops = _board.Drops;
                        for (int i = 0; i < drops.Count; i++)
                        {
                            if (drops[i] != null && _board.m_IsRingBoard)
                            {
                                if (drops[i].m_Item != null && _board.GetPanel<RingPanel>().m_Color == drops[i].m_Item.m_Color)
                                {
                                    drops[i].m_Item.SetColorRandomOther();
                                    ret = true;
                                }
                            }
                        }
                    }
                }

                index++;
            }
        }

        return ret;
    }

    //매칭되서 Brust

        List<Board> List_MissionCheckBoards = new List<Board>();
    public void MatchBrust(List<Board> boards)
    {
        //매칭된 아이템들 터트리자!
        for (int i = 0; i < boards.Count; i++)
        {
            if (boards[i].m_isMatchBrust)
            {
                boards[i].m_isMatchBrust = false;

                if (boards[i].m_Item is LineXItem || boards[i].m_Item is LineYItem || boards[i].m_Item is LineCItem)
                    boards[i].Brust(false, 0.1f, false);// Item.DestoryTime - 0.1f); //Item.DestoryTime보다 크면 아래보드로 떨어져서 안터지는 상황이 발생..
                else
                    boards[i].Brust(false, 0f, false);  //콤보 사운드가 대신하기때문에 사운드 없다.
            }
        }

        
    }

    //매칭주변에서 Brust
    //public void AroundBrust(List<Board> boards)
    //{
    //    List<Board> nonDuplicated = new List<Board>();
    //    Board abd;
    //    for (int i = 0; i < boards.Count; i++)
    //    {
    //        abd = boards[i][SQR_DIR.TOP];
    //        if (abd != null)
    //            if (!boards.Contains(abd) && !nonDuplicated.Contains(abd))
    //                nonDuplicated.Add(abd);

    //        abd = boards[i][SQR_DIR.BOTTOM];
    //        if (abd != null)
    //            if (!boards.Contains(abd) && !nonDuplicated.Contains(abd))
    //                nonDuplicated.Add(abd);

    //        abd = boards[i][SQR_DIR.LEFT];
    //        if (abd != null)
    //            if (!boards.Contains(abd) && !nonDuplicated.Contains(abd))
    //                nonDuplicated.Add(abd);

    //        abd = boards[i][SQR_DIR.RIGHT];
    //        if (abd != null)
    //            if (!boards.Contains(abd) && !nonDuplicated.Contains(abd))
    //                nonDuplicated.Add(abd);
    //    }


    //    for (int i = 0; i < nonDuplicated.Count; i++)
    //    {
    //        //주변 패널 체크
    //        if (nonDuplicated[i].IsPanelArountBrust)
    //            nonDuplicated[i].PanelBrust();

    //        //주변 아이템 체크
    //        if (nonDuplicated[i].IsItemArountBrust)
    //            nonDuplicated[i].Brust(true, 0f); 
    //    }
    //}



    #endregion

    #region Switching
    //*******************************************************************
    //아이템은 언제든 없어질수 있으므로
    //게임 진행 관련 소스(특히 코루틴)을 아이템스크립트에서 돌릴수 없다.
    //*******************************************************************

    //인게임에서 스위칭 하는 동안에 백버튼이나 옵션을 눌러서 끌수가 있다. 그러면 터지는 블럭이 있는데 게임을 종료하게 되어 다음판에 문제가 발생할 수 있다.
    public bool isSwitching = false; 
    public void Switching(Item A, Item B)
    {
        StartCoroutine(Coroutine_Switching(A, B));
    }
    IEnumerator Coroutine_Switching(Item A, Item B)
    {
        if (A.m_Board.IsNowItemSwitch == false || B.m_Board.IsNowItemSwitch == false)
            yield break;

        //
        isSwitching = true;

        //사운드
        SoundManager.Instance.PlayEffect("blockchange");

        //이펙트--------------------------------------------------------------
        ItemEffect effect1 = EffectManager.Instance.GetDefaultEffect();
        effect1.SwitchEffect(A.gameObject);
        ItemEffect effect2 = EffectManager.Instance.GetDefaultEffect();
        effect2.SwitchEffect(B.gameObject);

        //--------------------------------------------------------------------

        float startTime = Time.time;
        float speed = 7f;
        float distCovered = 0;
        float scale = 1;

        Vector3 StartPos_A = A.m_Board.transform.position;
        Vector3 StartPos_B = B.m_Board.transform.position;

        //만약 스폐셜 아이템 합체인가?
        bool checkCombine_A = A.CheckCombine(B.m_ItemType);
        bool checkCombine_B = B.CheckCombine(A.m_ItemType);
        if (checkCombine_A || checkCombine_B)
        {
            //1. 튜토리얼같은곳에서 위치를 바꿔서 이펙트보다 위에 있는 문제
            //2. 레인보우가 터질때는 항상 위에 있도록..
            //A는 While문에 잡을테니 B만 여기서 최종 위치를 세팅한다.
            if (checkCombine_A)
                B.transform.position = StartPos_B + new Vector3(0, 0, 0.1f); //A보다 뒤로 이동
            else
                B.transform.position = StartPos_B + new Vector3(0, 0, -0.1f); //A보다 앞으로 이동

            while (distCovered < 1)
            {
                speed += (Time.time - startTime);
                distCovered = (Time.time - startTime) * speed;
                A.transform.position = Vector3.Lerp(StartPos_A + Vector3.back * 0.3f, StartPos_B, distCovered);

                scale = 1.5f - distCovered / 2;
                if (scale < 1)
                    scale = 1;
                A.transform.localScale = new Vector3(scale, scale, scale);
                yield return new WaitForEndOfFrame();
            }

            //터지지 않는 젤리몬은 되돌아 온다.
            if (A is JellyMon)
                A.transform.position = StartPos_A;

            if (checkCombine_A)
                A.m_Board.CombineBrust(B.m_ItemType);
            else
                B.m_Board.CombineBrust(A.m_ItemType);


            //yield return new WaitForSeconds(0.5f);

            //콤바인일때는 이동한 아이템은 기본이펙트는 안나온다.
            A.m_BrustEffect = false;
            A.m_BrustArround = false;

            //미션관련..
            MissionManager.Instance.MoveLimitApply();
            MissionManager.Instance.MissionInterval();
            m_ItemMgr.Donut_IntervalApply();
            m_ItemMgr.Spiral_IntervalApply();
            m_ItemMgr.TimeBomb_IntervalApply();
            m_ItemMgr.Mystery_IntervalApply();
            m_ItemMgr.Chameleon_IntervalApply();
            m_ItemMgr.Key_IntervalApply();
            if (SwitchingApply != null) SwitchingApply();
            BaseStep.isItemStep = true;

            //콤보_콤비네이션일때(매칭이 아닐때)도 콤보 추가.
            ComboPlus(false);

            //MatchManager.Instance.SetStep(StepType.Drop);
            MatchManager.Instance.SetStep(StepType.Matching);
        }
        else
        {
            //********************
            //스왑되는 애니메이션
            //********************
            A.m_Board.m_Item = B;
            B.m_Board.m_Item = A;
            int matchesA = A.m_Board.FindMatchesAround().Count;
            int matchesB = B.m_Board.FindMatchesAround().Count;

            while (distCovered < 1)
            {
                speed += (Time.time - startTime) * 2;
                //Debug.Log(speed);
                distCovered = (Time.time - startTime) * speed;
                A.transform.position = Vector3.Lerp(StartPos_A + Vector3.back * 0.3f, StartPos_B, distCovered);
                B.transform.position = Vector3.Lerp(StartPos_B + Vector3.back * 0.2f, StartPos_A, distCovered);

                scale = 1.5f - distCovered / 2;
                if (scale < 1)
                    scale = 1;
                A.transform.localScale = new Vector3(scale, scale, scale);
                yield return new WaitForEndOfFrame();
            }

            if (matchesA <= 0 && matchesB <= 0)
            {
                SoundManager.Instance.PlayEffect("popup");
                //*************************
                //스왑이 안될때는 돌아온다!
                //**************************
                A.m_Board.m_Item = A;
                B.m_Board.m_Item = B;

                speed = 7f;
                startTime = Time.time;
                distCovered = 0;
                while (distCovered < 1)
                {
                    speed += (Time.time - startTime) * 10;
                    distCovered = (Time.time - startTime) * speed;
                    A.transform.position = Vector3.Lerp(StartPos_B + Vector3.back * 0.3f, StartPos_A, distCovered);
                    B.transform.position = Vector3.Lerp(StartPos_A + Vector3.back * 0.2f, StartPos_B, distCovered);
                    yield return new WaitForEndOfFrame();
                }
                //SoundBase.Instance.GetComponent<AudioSource>().PlayOneShot(SoundBase.Instance.wrongMatch);
            }
            else
            {
                //매칭이 있을 경우 
                Board temp = A.m_Board;
                A.m_Board = B.m_Board;
                B.m_Board = temp;

                A.m_Board.m_MatchingCheck = true;
                B.m_Board.m_MatchingCheck = true;

                //미션관련..
                MissionManager.Instance.MoveLimitApply();
                MissionManager.Instance.MissionInterval();
                m_ItemMgr.Donut_IntervalApply();
                m_ItemMgr.Spiral_IntervalApply();
                m_ItemMgr.TimeBomb_IntervalApply();
                m_ItemMgr.Mystery_IntervalApply();
                m_ItemMgr.Chameleon_IntervalApply();
                m_ItemMgr.Key_IntervalApply();
                if (SwitchingApply != null) SwitchingApply();
                BaseStep.isItemStep = true;

                //추가되는 개념 AiStep에서 Matching!!
                MatchManager.Instance.SetStep(StepType.Matching);
            }
        }

        isSwitching = false;
    }
    #endregion

    #region 보너스 타임
    public void BonusTime()
    {
        StartCoroutine(Co_BonusTime());
    }

    IEnumerator Co_BonusTime()
    {
        if (IsShowComboText)
            yield return new WaitForSeconds(0.8F);

        ////트럭 능력 ; 점수 플러스
        //yield return StartCoroutine(m_UIMgr.FoodTruck_ScorePlus());
        ////트럭 능력 ; 보너스무브 플러스
        //yield return StartCoroutine(m_UIMgr.FoodTruck_CutScene(TruckAbilityType.BonusPlus));

        int remain_MoveCnt = m_CSD.limit_Move;
        if (remain_MoveCnt > 0)
        {
            yield return new WaitForSeconds(0.8F);

            //처음 글자 출력..
            m_UIMgr.BonusTime();
            SoundManager.Instance.PlayEffect("bonus_time");

            //사운드
            SoundManager.Instance.PlayBGM("clear_bgm");

            yield return new WaitForSeconds(0.8F);
            while (remain_MoveCnt > 0)
            {
                Board bd = GetNormalItemBoard();
                if (bd != null)
                {
                    remain_MoveCnt--;
                    MissionManager.Instance.ApplyOfBonusTime(remain_MoveCnt);

                    StartCoroutine(CreateBonusItem(bd, false));

                    yield return new WaitForSeconds(0.1F);
                }
                else {
                    yield return StartCoroutine(Co_AllBrustSpecialPiece());
                }
            }
        }
        else {
            yield return new WaitForSeconds(2F);
        }

        yield return StartCoroutine(Co_AllBrustSpecialPiece());

        SetMatchState(MatchState.GameClear);

        //while (IsFinishDropAndMatch == false) yield return new WaitForSeconds(1F);

        //if (Score < CurrentLevel.scoreToReach[0]) Score = CurrentLevel.scoreToReach[0];

        //EndGame(GAMEOVER_REASON.NONE, true, GetStarGrade());
    }

    int CreateCnt = 0;
    public IEnumerator CreateBonusItem(Board board, bool truckbonus)
    {
        CreateCnt++;
        //사운드
        SoundManager.Instance.PlayEffect("create_bonus");

        //yield return StartCoroutine(m_UIMgr.Co_BonusTimeEffect(board, truckbonus));

        //이펙트
        EffectManager.Instance.BonusItemChangeEffect(board.transform.position);

 

        yield return new WaitForSeconds(0.2f);

        int random = UnityEngine.Random.Range(0, 10);

        if (random < 5)
            board.GenItem(ItemType.BonusBomb, ColorType.None, 0, false, true);
        else
            board.GenItem(ItemType.BonusCross, ColorType.None, 0, false, true);

        CreateCnt--;

       
    }

    public Board GetNormalItemBoard()
    {
        Board target = null;

        List<Board> li_normal = new List<Board>();

        foreach (Board bd in m_ListBoard)
        {
            if (bd.IsPanelItemExist && bd.IsPanelItemBrust)
            {
                if(bd.m_Item is NormalItem)
                {
                    li_normal.Add(bd);
                }
            }
        }

        if (li_normal.Count > 0)
        {
            target = li_normal[UnityEngine.Random.Range(0, li_normal.Count)];
        }

        return target;
    }

    public List<Board> GetNormalItemBoards(int cnt)
    {
        Board target = null;
        List<Board> list_target = new List<Board>();

        List<Board> li_normal = new List<Board>();

        foreach (Board bd in m_ListBoard)
        {
            if (bd.IsPanelItemExist && bd.IsPanelItemBrust)
            {
                if (bd.m_Item is NormalItem)
                {
                    //게임 진행중에 사용하므로 두조건을 추가한다.
                    if(bd.m_ItemBrusting == false && bd.m_DropAnim == false)
                        li_normal.Add(bd);
                }
            }
        }

        if (li_normal.Count > 0)
        {
            //cnt만큼 갯수를 구했거나 전체갯수와 구한갯수가 같아지면(첨부터갯수가적어서)
            while (list_target.Count < cnt && list_target.Count != li_normal.Count)
            {
                target = li_normal[UnityEngine.Random.Range(0, li_normal.Count)];
                if (list_target.Contains(target) == false)
                    list_target.Add(target);
            }
        }

        return list_target;
    }


    public List<Board> GetItemBoards<T>()
    {
        List<Board> targetItems = new List<Board>();

        foreach (Board bd in m_ListBoard)
        {
            if (bd.IsPanelItemExist && bd.IsPanelItemBrust)
            {
                if (bd.m_Item is T)
                {
                    targetItems.Add(bd);
                }
            }
        }

        return targetItems;
    }
    public List<Board> GetPanelBoards<T>() where T : Panel
    {
        List<Board> targets = new List<Board>();

        foreach (Board bd in m_ListBoard)
        {
            if (bd.IsPanelItemExist && bd.IsPanelItemBrust)
            {
                if (bd.GetPanel<T>() != null)
                {
                    targets.Add(bd);
                }
            }
        }

        return targets;
    }



    IEnumerator Co_AllBrustSpecialPiece()
    {
        while (CreateCnt > 0)
        {
            yield return new WaitForEndOfFrame();
        }

        while (true)
        {
            Board sp = GetSpecialItem();

            if (sp == null)
                break;

            while (sp != null)
            {
                sp.Brust(false, 0);     //스폐셜 아이템
                SetStep(StepType.Matching);

                yield return new WaitForSeconds(0.2F);

                sp = GetSpecialItem();
            }

            while (m_StepType != StepType.Clear)
            {
                yield return null;
            }
        }
    }

    Board GetSpecialItem()
    {
        foreach (Board bd in m_ListBoard)
        {
            if (bd.IsPanelItemExist && bd.IsPanelItemBrust)
            {
                if(bd.m_ItemBrusting)
                    continue;

                if (bd.m_Item is LineXItem
                    || bd.m_Item is ButterFlyItem
                    || bd.m_Item is LineYItem
                    || bd.m_Item is LineCItem
                    || bd.m_Item is BombItem
                    || bd.m_Item is RainbowItem
                    || bd.m_Item is BonusCrossItem
                    || bd.m_Item is BonusBombItem)
                {
                    return bd;
                }
            }
        }
        return null;
    }

    #endregion

    #region 셔플링
   

    public bool ShufflingCheck()
    {
        //Dictionary<Board, List<Board>> Dic_MatchPossible = new Dictionary<Board, List<Board>>();

        foreach (Board bd in m_ListBoard)
        {
            List<Board> bds = GetCanSwitching(bd);

            if (bds.Count > 0)
            {
                return false;
            }
        }

        return true;
    }

    List<Board> GetCanSwitching(Board bd)
    {
        List<Board> bds = new List<Board>();

        if(bd.IsNowItemSwitch)
        {
            //매칭블럭이면 매칭체크&콤바인체크
            if (bd.IsNowItemMatch)
            {
                if (IsPossibleMatch(bd[SQR_DIR.TOP], bd.m_Item.m_Color, SQR_DIR.BOTTOM)) bds.Add(bd[SQR_DIR.TOP]);

                if (IsPossibleMatch(bd[SQR_DIR.BOTTOM], bd.m_Item.m_Color, SQR_DIR.TOP)) bds.Add(bd[SQR_DIR.BOTTOM]);

                if (IsPossibleMatch(bd[SQR_DIR.LEFT], bd.m_Item.m_Color, SQR_DIR.RIGHT)) bds.Add(bd[SQR_DIR.LEFT]);

                if (IsPossibleMatch(bd[SQR_DIR.RIGHT], bd.m_Item.m_Color, SQR_DIR.LEFT)) bds.Add(bd[SQR_DIR.RIGHT]);
            }

            //매칭여부와 상관없이 콤바인 체크를 한다
            //레인보우, 유령...  or  xy라인, 폭탄, 나비....
            if (bds.Count == 0) //일반 매칭이 없다면 콤바인 체크도 한다
            {
                if (IsPossibleCombin(bd, bd[SQR_DIR.TOP])) bds.Add(bd[SQR_DIR.TOP]);

                if (IsPossibleCombin(bd, bd[SQR_DIR.BOTTOM])) bds.Add(bd[SQR_DIR.BOTTOM]);

                if (IsPossibleCombin(bd, bd[SQR_DIR.LEFT])) bds.Add(bd[SQR_DIR.LEFT]);

                if (IsPossibleCombin(bd, bd[SQR_DIR.RIGHT])) bds.Add(bd[SQR_DIR.RIGHT]);
            }
            
        }  
        return bds;
    }

    bool IsPossibleMatch(Board bd, ColorType color, SQR_DIR exception)
    {
        if (bd == null) return false;

        //여기서는 매칭체크는 필요없다. 스위칭이 가능한지만 본다. 교체 되는놈이 Match되는놈인지는 의미 없음.
        if (bd.IsNowItemSwitch)
        {
            List<Board> li_board_x = new List<Board>();
            if (exception != SQR_DIR.LEFT) bd.FindMatches_Dir_Recursive(color, ref li_board_x, SQR_DIR.LEFT);
            if (exception != SQR_DIR.RIGHT) bd.FindMatches_Dir_Recursive(color, ref li_board_x, SQR_DIR.RIGHT);

            List<Board> li_board_y = new List<Board>();
            if (exception != SQR_DIR.TOP) bd.FindMatches_Dir_Recursive(color, ref li_board_y, SQR_DIR.TOP);
            if (exception != SQR_DIR.BOTTOM) bd.FindMatches_Dir_Recursive(color, ref li_board_y, SQR_DIR.BOTTOM);

            return (li_board_x.Count > 1) || (li_board_y.Count > 1);
        }

        return false;
    }
    bool IsPossibleCombin(Board a, Board b)
    {
        if (a == null) return false;
        if (b == null) return false;
        if (a.IsNowItemSwitch && b.IsNowItemSwitch)
        {
            return a.m_Item.CheckCombine(b.m_Item.m_ItemType) || b.m_Item.CheckCombine(a.m_Item.m_ItemType);
        }

        return false;
    }

    public void Shuffling()
    {
        StartCoroutine(Co_Shuffling());
    }

    IEnumerator Co_Shuffling()
    {
        int tryCount = 0;
       
        m_UIMgr.ShufflingText();

        for (int i = 0; i < m_ListBoard.Count; i++)
        {
            if (m_ListBoard[i].IsItemExist)
            {
                if (m_ListBoard[i].m_Item is NormalItem)
                {
                    m_ListBoard[i].m_Item.transform.DOScale(0, 0.5f);
                }
            }
        }

        SoundManager.Instance.PlayEffect("shuffle");

        yield return new WaitForSeconds(0.5f);


        bool IsShuffing = true;

        while (IsShuffing)
        {
          

            for (int i = 0; i < m_ListBoard.Count; i++)
            {
                if (m_ListBoard[i].IsItemExist)
                {
                    if (m_ListBoard[i].m_Item is NormalItem)
                    {
                        while(true)
                        {
                            m_ListBoard[i].m_Item.SetColorRandomOther();

                            List<Board> newCombine = m_ListBoard[i].FindMatchesAround();

                            if (newCombine.Count == 0)
                                break;
                        }
                    }
                }
            }

            IsShuffing = ShufflingCheck();

            if (tryCount > 999) break;

            tryCount++;
        }

        for (int i = 0; i < m_ListBoard.Count; i++)
        {
            if (m_ListBoard[i].IsItemExist)
            {
                if (m_ListBoard[i].m_Item is NormalItem)
                {
                    m_ListBoard[i].m_Item.transform.DOScale(1, 0.5f);
                }
            }
        }

        yield return new WaitForSeconds(0.5F);


        if (tryCount > 999)
        {

            //일단 셔플링으로 해결이 안되는 상황이 오면 실패로 넘긴다.
            MissionManager.Instance.m_FailType = FailType.ShufflingOver;
            SetStep(StepType.Fail);
            yield break;
        }


        SetMatchState(MatchState.Playing);
    }

    public void start_Shuffling()
    {
        //게임 시작할때 셔플링..
        int tryCount = 0;

        while (ShufflingCheck())
        {
            //Debug.LogError("셔플링에 걸림" +  tryCount);
            for (int i = 0; i < m_ListBoard.Count; i++)
            {
                if (m_ListBoard[i].IsItemExist)
                {
                    if (m_ListBoard[i].m_Item is NormalItem)
                    {
                        while (true)
                        {
                            m_ListBoard[i].m_Item.SetColorRandomOther();

                            List<Board> newCombine = m_ListBoard[i].FindMatchesAround();

                            if (newCombine.Count == 0)
                                break;
                        }
                    }
                }
            }

            if (tryCount > 999) break;

            tryCount++;
        }

        if (tryCount > 999)
        {
            //일단 셔플링으로 해결이 안되는 상황이 오면 실패로 넘긴다.
            MissionManager.Instance.m_FailType = FailType.ShufflingOver;
            SetStep(StepType.Fail);
        }
    }
    #endregion

    #region 시작포커스
    public bool m_IsFocus = false;
    List<Item> List_FocusItem = new List<Item>();
    public void FocusInit()
    {
        m_IsFocus = false;

        if (List_FocusItem == null || List_FocusItem.Count <= 0)
            return;

        foreach (Item item in List_FocusItem)
        {
            if (item == null)
                continue;

            DOTween.Kill(item.transform);
            item.transform.localScale = Vector3.one;
        }

        List_FocusItem.Clear();
    }

    public bool FocusContainsItem<T>() where T : Item
    {
        if (List_FocusItem.Count <= 0)
            return false;

        foreach (Item item in List_FocusItem)
        {
            if (item == null)
                continue;
            if (item is T)
                return true;
        }

        return false;
    }

    public void FocusShow()
    {
        if (m_CSD.focus.Count <= 0) return;

        m_IsFocus = true;

        for (int i = 0; i < m_CSD.focus.Count; i++)
        {
            if (m_ListBoard[m_CSD.focus[i]].m_Item == null)
                continue;

            List_FocusItem.Add(m_ListBoard[m_CSD.focus[i]].m_Item);
        }

        foreach (Item item in List_FocusItem)
        {
            item.transform.DOScale(1.3F, 0.7F).SetLoops(-1, LoopType.Yoyo);
        }

        StartCoroutine(Co_StopFocus());
    }

    IEnumerator Co_StopFocus()
    {
        if (List_FocusItem.Count == 0)
        {
            yield break;
        }

        while (true)
        {
            if (m_StepType != StepType.Wait)
                break;

            yield return null;
        }

        FocusInit();
    }

    #endregion

    #region 힌트
    bool m_HintOffer = false;
    List<Item> List_HintItem = new List<Item>();
    public void HintInit()
    {
        m_HintOffer = false;

        foreach (Item item in List_HintItem)
        {
            if (item == null)
                continue;

            DOTween.Kill(item.transform);
            item.transform.localScale = Vector3.one;
        }

        List_HintItem.Clear();
    }

    public void HintOffer()
    {
        if (m_HintOffer)
            return;
        List<Board> hintBoards = GetHintBoard();

        if (hintBoards.Count <= 0) return;

        foreach (Board board in hintBoards)
        {
            Item _item = board.m_Item;
            _item.transform.DOScale(1.3F, 0.7F).SetLoops(-1, LoopType.Yoyo);
            List_HintItem.Add(_item);
        }
   
        m_HintOffer = true;

        StartCoroutine(Co_StopHint());
    }

    IEnumerator Co_StopHint()
    {
        if (List_HintItem.Count == 0)
        {
            Debug.LogError("대박, 셔플링를 먼저 했는데 힌트가 없는 경우가 발생??");
            yield break;
        }

        while (true)
        {
            if(m_StepType != StepType.Wait)
                break;

            yield return null;
        }

        HintInit();
    }

    List<Board> bds = new List<Board>();
    List<Board> GetHintBoard()
    {    
        bds.Clear();

        Board bd;
        for (int i = m_ListBoard.Count - 1; i >= 0; i--)
        {
            bd = m_ListBoard[i];
            if (bd.IsNowItemSwitch)
            {
                if (bd.IsNowItemMatch)
                {
                    bds.AddRange(GetPossibleMatch(bd[SQR_DIR.TOP], bd.m_Item.m_Color, SQR_DIR.BOTTOM));
                    if (bds.Count == 0)
                        bds.AddRange(GetPossibleMatch(bd[SQR_DIR.LEFT], bd.m_Item.m_Color, SQR_DIR.RIGHT));
                    if (bds.Count == 0)
                        bds.AddRange(GetPossibleMatch(bd[SQR_DIR.BOTTOM], bd.m_Item.m_Color, SQR_DIR.TOP));
                    if (bds.Count == 0)
                        bds.AddRange(GetPossibleMatch(bd[SQR_DIR.RIGHT], bd.m_Item.m_Color, SQR_DIR.LEFT));

                    if (bds.Count > 0)
                    {
                        bds.Add(bd);
                        break;
                    }
                }
            }
        }

        if (bds.Count > 0)
            return bds;

        for (int i = m_ListBoard.Count - 1; i >= 0; i--)
        {
            bd = m_ListBoard[i];
            if (bd.IsNowItemSwitch)
            {
                //매칭여부와 상관없이 콤바인 체크를 한다
                //레인보우, 유령...  or  xy라인, 폭탄, 나비....
                //if (bd.IsNowItemMatch == false)
                //{
                if (IsPossibleCombin(bd, bd[SQR_DIR.TOP]))
                    {
                        bds.Add(bd);
                        bds.Add(bd[SQR_DIR.TOP]);
                    }

                    else if (IsPossibleCombin(bd, bd[SQR_DIR.BOTTOM]))
                    {
                        bds.Add(bd);
                        bds.Add(bd[SQR_DIR.BOTTOM]);
                    }

                    else if (IsPossibleCombin(bd, bd[SQR_DIR.LEFT]))
                    {
                        bds.Add(bd);
                        bds.Add(bd[SQR_DIR.LEFT]);
                    }

                    else if (IsPossibleCombin(bd, bd[SQR_DIR.RIGHT]))
                    {
                        bds.Add(bd);
                        bds.Add(bd[SQR_DIR.RIGHT]);
                    }
                //}
            }
            if (bds.Count > 0)
            {
                break;
            }
        }
        return bds;
    }

    List<Board> li_board_x = new List<Board>();
    List<Board> li_board_y = new List<Board>();
    List<Board> GetPossibleMatch(Board bd, ColorType color, SQR_DIR exception)
    {
        li_board_x.Clear();
        li_board_y.Clear();

        if (bd == null) return li_board_x;

        //여기서는 매칭체크는 필요없다. 스위칭이 가능한지만 본다. 교체 되는놈이 Match되는놈인지는 의미 없음.
        if (bd.IsNowItemSwitch)
        {
            if (exception != SQR_DIR.LEFT) bd.FindMatches_Dir_Recursive(color, ref li_board_x, SQR_DIR.LEFT);
            if (exception != SQR_DIR.RIGHT) bd.FindMatches_Dir_Recursive(color, ref li_board_x, SQR_DIR.RIGHT);
            if(li_board_x.Count < 2)
                li_board_x.Clear();


            if (exception != SQR_DIR.TOP) bd.FindMatches_Dir_Recursive(color, ref li_board_y, SQR_DIR.TOP);
            if (exception != SQR_DIR.BOTTOM) bd.FindMatches_Dir_Recursive(color, ref li_board_y, SQR_DIR.BOTTOM);
            if(li_board_y.Count > 1)
                li_board_x.AddRange(li_board_y);
        }

        return li_board_x;
    }

    #endregion

    #region 중력 보여주기
    public void ShowGravity()
    {
        var e = m_ListDropHead.GetEnumerator();
        while (e.MoveNext())
        {
            e.Current.m_GravityDisplayer.Play();
        }
    }
    #endregion

    #region 점수관리
    public void ScoreTextShow(int Score, Board board, ColorType color)
    {
        GameScore += Score;
        if (board != null) //트럭 능력치로 점수 올라갈때는 보드가 없다.
            m_UIMgr.ScoreText(Score, board.transform.position, color);
        m_UIMgr.ScoreUpdate(GameScore);
    }

    #endregion

    #region 콤보 관련
    public void ComboPlus(bool sound)
    {
        //콤보 이펙트 소리
        if (sound)
            SoundManager.Instance.PlayEffect("combo" + Mathf.Min(ComboCnt, 4));

        //이부분 이슈!
        //Playing중에 스폐셜로 터지는 콤보는 Brust함수의 인자로 처리하고
        //여기서는 콤보cnt를 올리지 않는다.
        if (m_MatchState == MatchState.Playing)
            ComboCnt++;   
    }

    bool IsShowComboText = false;
    public void ComboText()
    {
        //최종 콤보 연출 
        //텍스트이미지 + 소리
        IsShowComboText = false;

        if (m_MatchState == MatchState.Playing) 
        {
            if (ComboCnt > 3)
            {
                m_UIMgr.ComboText(ComboCnt);
                IsShowComboText = true;
                m_GameMgr.GPGS_AchivenmentCompare(3,ComboCnt);
            }
        }
        ComboCnt = 1;
    }
    #endregion

    #region 계속하기, 보너스아이템
    public void Continue_BonusItem(int continueType, System.Action complete)
    {
        StartCoroutine(Co_Continue_BonusItem(continueType, complete));
    }
    IEnumerator Co_Continue_BonusItem(int continueType, System.Action complete)
    {
        Board bd = null;
        if (continueType > 0)
        {
            bd = GetNormalItemBoard();
            if (bd != null)
                yield return StartCoroutine(Co_CreateSpecailItem(bd, ItemType.Bomb, ColorType.Rnd));
        }

        if (continueType > 1)
        {
            bd = GetNormalItemBoard();
            if (bd != null)
                yield return StartCoroutine(Co_CreateSpecailItem(bd, ItemType.Rainbow, ColorType.None));
        }

        complete();
    }


    IEnumerator Co_CreateSpecailItem(Board board, ItemType type, ColorType color)
    {
        //ItemEffect _ie = EffectManager.Instance.GetItemEffect();

        //Vector3 screepos = GameManager.Instance.UICamera.WorldToScreenPoint(m_UIMgr.MoveLabel.transform.position);
        //Vector3 matchpos = GameManager.Instance.MatchCamera.ScreenToWorldPoint(screepos);


        //yield return StartCoroutine(_ie.Co_BonusTimeEffect(matchpos, board.transform.position));

        //별이펙트
        EffectManager.Instance.ItemChangeEffect(board.transform.position);

        yield return new WaitForSeconds(0.1f);

        //사운드
        SoundManager.Instance.PlayEffect("tm");

        board.GenItem(type, color, 0, false, false);
        board.m_MatchingCheck = true;

    }
    #endregion

    #region 튜토리얼
    //튜토리얼매니저가 따로 있다.여기는 튜토리얼 시작점.
    public bool TutorialCheck()
    {
        if (m_DataSet == 0)
        {
            //튜토리얼
            //TutorialManager.Instance.Tutorial_Stage_Init();
            return TutorialManager.Instance.Tutorial_Start(m_StageIndex);
        }
        //
        //SetMatchState(MatchState.Playing);
        return false;
    }
    #endregion

    #region 광고보상형 아이템

    //private bool PairMatched(ItemType type, MissionKind mission)
    //{

    //}

    private List<Board> GetBoardByStep(int level)
    {
        if (level == 0)
        {
            return GetItemBoards<NormalItem>();
        }

        var missions = MissionManager.Instance.List_MsInfo_PC;

        var indexList = new List<ItemType>() { ItemType.Normal, ItemType.Chameleon, ItemType.Mystery, ItemType.TimeBomb, ItemType.Normal };

        //remove MissionItems;
        if (missions.Find((p) => p.kind == MissionKind.TimeBomb) != null)
        {
            indexList.Remove(ItemType.TimeBomb);
        }

        var index = indexList[Mathf.Clamp(level, 0, indexList.Count)];

        switch (index)
        {
            case ItemType.Normal: return GetItemBoards<NormalItem>();
            case ItemType.Chameleon: return GetItemBoards<ChameleonItem>();
            case ItemType.Mystery: return GetItemBoards<MysteryItem>();
            case ItemType.TimeBomb: return GetItemBoards<TimeBombItem>();

            default: return GetItemBoards<NormalItem>();
        }
    }

    public void Truck_BonusItem(Action<List<Board>> complete, float delay, ItemType[] types, ColorType[] colors)
    {
        StartCoroutine(Co_Truck_BonusItem(complete, delay, types, colors));
    }

    IEnumerator Co_Truck_BonusItem(Action<List<Board>> complete, float delay, ItemType[] types, ColorType[] colors)
    {
        var boards = new List<Board>();
        var missionList = MissionManager.Instance.List_MsInfo_PC;
        var focusItems = new List<Board>();

        for (int i = 0; i < m_CSD.focus.Count; i++)
        {
            if (m_ListBoard[m_CSD.focus[i]].m_Item == null)
                continue;

            focusItems.Add(m_ListBoard[m_CSD.focus[i]]);
        }

        for (int i = 0; i < types.Length;)
        {
            Board targetBoard = null;

            for (int j = 0; j < 5; ++j)
            {
                var targetBoards = GetBoardByStep(j);

                if (targetBoards == null || targetBoards.Count <= 0)
                    continue;

                targetBoards.RemoveAll((target_board) =>
                {
                    var temp = focusItems.Find((focus_board) => focus_board == target_board);
                    if (temp != null)
                        return true;

                    if (boards.Contains(target_board))
                        return true;

                    return false;
                });

                if (targetBoards.Count <= 0)
                    continue;

                targetBoard = targetBoards[UnityEngine.Random.Range(0, targetBoards.Count)];

                break;
            }


            if (targetBoard == null)
            {
                Debug.LogError("GetItemBoard is NULL. Can NOT Add AdBonus.");
            }
            else if (!boards.Contains(targetBoard))
            {
                boards.Add(targetBoard);
            }
            else
            {
                yield return null;

                continue;
            }

            ++i;
        }

        if (complete != null)
        {
            complete.Invoke(boards);
        }

        yield return new WaitForSeconds(delay);

        for (int i = 0; i < types.Length; ++i)
        {
            yield return StartCoroutine(Co_CreateSpecailItem(boards[i], types[i], colors[i]));
        }

        SetStep(StepType.Matching);

    }
    #endregion
}
