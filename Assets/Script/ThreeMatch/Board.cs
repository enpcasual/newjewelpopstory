﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum SQR_DIR{ NONE = 0, TOP, BOTTOM, LEFT, RIGHT, TOPRIGHT, TOPLEFT, BOTTOMLEFT, BOTTOMRIGHT };

public enum DROP_DIR { U, D, L, R, List } //Up, Down, Left, Right

public enum FIND_DIR { ALL = 0, HORIZONTAL, VERTICAL }

public class Board : MonoBehaviour
{
    //*****************
    //Default Variable
    //*****************
    MatchManager MatchMgr;
    ItemManager ItemMgr;

    //***************
    //Game Variable
    //***************
    public int X { set; get; }
    public int Y { set; get; }

    public int Index
    {
        get
        {
            return X + Y * MatchDefine.MaxX;
        }
    }
    public Stage m_CSD;

    public Panel m_DefaultPanel;
    //[System.NonSerialized]
    public List<Panel> m_ListPanel = new List<Panel>();

    //[System.NonSerialized]
    public Item m_Item;

    //******************
    //아이템 추가 관리
    //*******************
   

    //******************
    //패널 추가 관리
    //*******************
    static public List<Board> m_ListMagicColorBoard = new List<Board>();
    static public List<BottleCagePanel> m_ListBottlePanel = new List<BottleCagePanel>(); //케이지역할을 하기때문에 m_ListPanel에도 있어야한다. 매번 전체for문을 돌지 않도록 들고 있는다.
    static public List<RingPanel> m_ListRingPanel = new List<RingPanel>();

    //************************************************
    //별도로 동작하는 패널. Static으로 관리. (SpecialPanelDestory() 추가필수)
    //************************************************
    static public int m_WarpCount = 0;
    static public List<Panel> m_ListWarpPanel = new List<Panel>();
    static public List<Panel> m_ListFoodArvPanel = new List<Panel>();
    static public List<Panel> m_ListBearStartPanel = new List<Panel>();
    static public List<ConveyerBeltPanel> m_ListConveyerBeltPanel = new List<ConveyerBeltPanel>();

    //************************************************
    //패널체크 최적화용 변수(실시간변경되지않는패널)
    //************************************************
    public bool m_IsCreatorEmpty = false;       //기본생성보드일때 세팅
    public bool m_IsCreatorFood = false;
    public bool m_IsCreatorSprial = false;
    public bool m_IsCreatorTimeBomb = false;
    public bool m_IsCreatorFoodSprial = false;
    public bool m_IsCreatorFoodTimeBomb = false;
    public bool m_IsCreatorSprialTimeBomb = false;
    public bool m_IsCreatorKey = false;
    public bool m_IsCreatorKeyFood = false;
    public bool m_IsCreatorKeyTimeBomb = false;
    public bool m_IsWarpInBoard = false;        //WarpIn일때 세팅
    public Board m_WarpOutBoard = null;         //WarpIn일때 세팅
    public bool m_IsWarpOutBoard = false;       //WarpOut일때 세팅
    public Board m_WarpInBoard = null;          //WarpOut일때 세팅
    //public int m_WarpIndex = -1;                //warp일때 index를 세팅(워프패널이 가지고 있는 값이지만 매번 panel를 찾아야 한다. 아니면 패널을 들고 있으면 되는데..)
    public bool m_IsConveyerBeltBoard = false;  //ConveyerBelt


    //************************************************
    //패널체크 최적화용 변수(실시간변경되는패널)
    //************************************************
    public bool m_IsCakeBoard = false;
    public bool m_IsJamBoard = false;
    public bool m_IsRingBoard = false;


    [System.NonSerialized]
    public ItemType m_NextItemType = ItemType.None;

    public bool m_isMatchBrust = false;//아이템이 터져야 하는가?

    //public bool m_Brusting = false;
    public bool m_ItemBrusting = false;
    public bool m_PanelBrusting = false;
    public int m_PanelBrusting_Ctn = 0;
    public bool m_DropAnim = false;
    public bool m_NormalEffect = true;
    public bool m_SideDrop = false; //아이템이 사이드 내려가야한다는정보를 들고 있음.
    //public bool m_BearMoveing = false;  //곰이 워프를 통해 이동중일때 중복처리 안하기 위함.

    [System.NonSerialized]
    public bool m_MatchingCheck = false;

    public Item CoveyerItem; //컨베이어벨트 이동용

    public bool isListDrop = false;

    public List<DROP_DIR> PossibleDrop_Dirs = new List<DROP_DIR>();
    public int SeleteDropIndex = 0;
    public DROP_DIR Drop_Dir
    {
        get
        {
            return PossibleDrop_Dirs[SeleteDropIndex];
        }
        set
        {
            SeleteDropIndex = PossibleDrop_Dirs.IndexOf(value);
        }
    }

    public static bool isTutorialSeed = false;
    public static List<ColorType> List_Seed = new List<ColorType>();


    public GravityDisplayer m_GravityDisplayer;

    //*******************
    //Expansion Variable
    //*******************
    private Board Top { get { return MatchMgr.GetBoard(X, Y - 1); } }
    private Board Bottom { get { return MatchMgr.GetBoard(X, Y + 1); } }
    private Board Right { get { return MatchMgr.GetBoard(X + 1, Y); } }
    private Board Left { get { return MatchMgr.GetBoard(X - 1, Y); } }
    private Board TopLeft { get { return MatchMgr.GetBoard(X - 1, Y - 1); } }
    private Board TopRight { get { return MatchMgr.GetBoard(X + 1, Y - 1); } }
    private Board BottomLeft { get { return MatchMgr.GetBoard(X - 1 , Y + 1); } }
    private Board BottomRight { get { return MatchMgr.GetBoard(X + 1, Y + 1); } }

    public List<Board> Drops
    {
        get
        {
            List<Board> drops = new List<Board>();
            for (int i = 0; i < PossibleDrop_Dirs.Count; i++)
            {
                drops.Add(this[PossibleDrop_Dirs[i]]);
            }

            return drops;
        }
    }

    public Board Drop { get { return this[Drop_Dir]; } }
    public Board RealDrop { get { if (m_IsWarpOutBoard) return m_WarpInBoard; else return this[Drop_Dir]; } }
    public Board DropLeft
    {
        get
        {
            switch (Drop_Dir)
            {
                case DROP_DIR.U: return this[Drop_Dir] == null ? null : this[Drop_Dir].Left;
                case DROP_DIR.D: return this[Drop_Dir] == null ? null : this[Drop_Dir].Right;
                case DROP_DIR.R: return this[Drop_Dir] == null ? null : this[Drop_Dir].Top;
                case DROP_DIR.L: return this[Drop_Dir] == null ? null : this[Drop_Dir].Bottom;
                default: return null;
            }
        }
    }
    public Board DropRight
    {
        get
        {
            switch (Drop_Dir)
            {
                case DROP_DIR.U: return this[Drop_Dir] == null ? null : this[Drop_Dir].Right;
                case DROP_DIR.D: return this[Drop_Dir] == null ? null : this[Drop_Dir].Left;
                case DROP_DIR.R: return this[Drop_Dir] == null ? null : this[Drop_Dir].Bottom;
                case DROP_DIR.L: return this[Drop_Dir] == null ? null : this[Drop_Dir].Top;
                default: return null;
            }
        }
    }

    public Board this[SQR_DIR dir]
    {
        get
        {
            switch (dir)
            {
                case SQR_DIR.TOP: return Top;
                case SQR_DIR.BOTTOM: return Bottom;
                case SQR_DIR.LEFT: return Left;
                case SQR_DIR.RIGHT: return Right;
                case SQR_DIR.TOPRIGHT: return TopRight;
                case SQR_DIR.TOPLEFT: return TopLeft;
                case SQR_DIR.BOTTOMLEFT: return BottomLeft;
                case SQR_DIR.BOTTOMRIGHT: return BottomRight;
                default: return null;
            }
        }
    }

    public Board this[DROP_DIR dir]
    {
        get
        {
            switch (dir)
            {
                case DROP_DIR.U: return Top;
                case DROP_DIR.D: return Bottom;
                case DROP_DIR.R: return Right;
                case DROP_DIR.L: return Left;
                default: return null;
            }
        }
    }

    public Board this[int dir]
    {
        get
        {
            switch (dir)
            {
                case 0: return Top;
                case 1: return Bottom;
                case 2: return Right;
                case 3: return Left;
                default: return null;
            }
        }
    }

    //해당 블록을 땡겨가는 블록들을 반환
    public List<Board> DropReference()
    {
        List<Board> returnBoards = new List<Board>();
        for (int i = 0; i < (int)DROP_DIR.List; i++)
        {
            if (this[i] != null && (!this.IsPanelFixed))
            {
                if (!this[i].IsPanelFixed)
                {
                    if (this[i].Drop == this)
                    {
                        //드롭 방향이 1개 인경우
                        returnBoards.Add(this[i]);
                    }
                    else if (this[i].PossibleDrop_Dirs.Count > 1)
                    {
                        //드롭방향이 여러개인 경우.
                        List<Board> drops = this[i].Drops;
                        for (int j = 0; j < drops.Count; j++)
                        {
                            if (drops[j] == this)
                                returnBoards.Add(this[i]);
                        }
                    }
                }
            }
        }

        return returnBoards;
    }

    public List<Board> GravityReference()
    {
        List<Board> returnBoards = new List<Board>();
        for (int i = 0; i < (int)DROP_DIR.List; i++)
        {
            if (this[i] != null)
            {
                if (!this[i].m_IsCreatorEmpty)
                {
                    if (this[i].Drop == this)
                    {
                        //드롭 방향이 1개 인경우
                        returnBoards.Add(this[i]);
                    }
                    else if (this[i].PossibleDrop_Dirs.Count > 1)
                    {
                        //드롭방향이 여러개인 경우.
                        List<Board> drops = this[i].Drops;
                        for (int j = 0; j < drops.Count; j++)
                        {
                            if (drops[j] == this)
                                returnBoards.Add(this[i]);
                        }
                    }
                }
            }
        }

        return returnBoards;
    }

    public Vector3 GetDropVector()
    {
        switch (Drop_Dir)
        {
            case DROP_DIR.U: return Vector3.up;
            case DROP_DIR.D: return Vector3.down;
            case DROP_DIR.R: return Vector3.right;
            case DROP_DIR.L: return Vector3.left;
            default: return Vector3.up;
        }
    }

    public Vector3 GetWarpDropVector()
    {
        switch (Drop_Dir)
        {
            case DROP_DIR.U: return new Vector3(0, -MatchDefine.BoardHeight, 0.02f);  //Vector3.up;
            case DROP_DIR.D: return new Vector3(0, MatchDefine.BoardHeight, 0.02f); //Vector3.down;
            case DROP_DIR.R: return new Vector3(-MatchDefine.BoardHeight, 0, 0.02f); // Vector3.right;
            case DROP_DIR.L: return new Vector3(MatchDefine.BoardHeight, 0, 0.02f); //Vector3.left;
            default: return Vector3.up;
        }
    }


    public List<Board> GetBoardDir(SQR_DIR dir, int rimit = 0)
    {
        List<Board> list_bd = new List<Board>();

        Board bd = this;

        while (bd != null)
        {
            list_bd.Add(bd);
            bd = bd[dir];

            if(rimit > 0)
            {
                if(list_bd.Count >= rimit)
                    break;
            }
        }

        return list_bd;
    }

    public List<Board> GetBoardAround(int range = 1, bool onlyboundary = false)
    {
        List<Board> list_bd = new List<Board>();

        Board bd;

        for (int i = -range; i <= range; i++) // -1, 0, 1
        {
            for (int k = -range; k <= range; k++)
            {
                if(onlyboundary)//테두리만 가져올려면 내부는 빼고
                    if(i != -range && i != range && k != -range && k != range)
                        continue;

                bd = MatchMgr.GetBoard(X + k, Y + i);
                if(bd != null)
                    list_bd.Add(bd);
            }
        }
        return list_bd;
    }

    public List<Board> GetBoardAround2X2(int range = 1, bool onlyboundary = false)
    {
        List<Board> list_bd = new List<Board>();

        Board bd;

        //2x2를 감싸는 테두리를 구하기 위해 Right와 Bottom은 +1 한다.
        //int range_Top = range;          //y
        //int range_Bottom = range + 1;   //y
        //int range_Left = range;         //x
        //int range_Right = range + 1;    //x

        for (int i = -range; i <= range + 1; i++) // -1, 0, 1
        {
            for (int k = -range; k <= range + 1; k++)
            {
                if (onlyboundary)//테두리만 가져올려면 내부는 빼고
                    if (i != -range && i != range + 1 && k != -range && k != range + 1)
                        continue;

                bd = MatchMgr.GetBoard(X + k, Y + i);
                if (bd != null)
                    list_bd.Add(bd);
            }
        }
        return list_bd;
    }


    //*****************************************
    // 기본적인 조건
    // 객체의 하나의 특정값만을 조건으로 한다.
    //*****************************************

    #region IsItem 아이템 기본 조건
    //아이템이 존재하는가
    public bool IsItemExist
    {
        get
        {
            if (m_Item == null) return false;
            return true;
        }
    }

    private bool IsItemMatch    //private은 무조껀 조함조건을 사용
    {
        get
        {
            if (m_Item != null)
                return m_Item.Match;
            return false;
        }
    }

    private bool IsItemSwitch   //private은 무조껀 조함조건을 사용
    {
        get
        {
            if (m_Item != null)
                return m_Item.Switch;
            return false;
        }
    }
    private bool IsItemDrop   //private은 무조껀 조함조건을 사용
    {
        get
        {
            if (m_Item != null)
                return m_Item.Drop;
            return false;
        }
    }
    //아이템이 주변매칭으로 터질수 있는 조건인가
    private bool IsItemBrust
    {
        get
        {
            if (m_Item != null)
                return m_Item.m_Brust;
            return false;
        }
    }
    public bool IsItemArountBrust
    {
        get
        {
            if (m_Item != null)
                return m_Item.ArountBrust;
            return false;
        }
    }
    public bool IsItemHaveNextItem
    {
        get
        {
            if (m_Item != null)
                return m_Item.HaveNextItem;
            return false;
        }
    }
    #endregion

    #region IsPanel 패널 하나의 조건
    //아이템이 존재할수 있는 곳인가?
    public bool IsPanelItemExist
    {
        get
        {
            //기본패널 없다면 다른 패널(empty)로 인해 false될것이다.
            //데이타상의 문제를 찾을수 있도록 주석처리..
            //if(m_DefaultPanel == null) //기본패널도 없다면. 
            //    return false;

            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ItemExist == false)
                    return false;

            return true;
        }
    }
    //아이템이 드랍될수 있는가? 터치 이동 아님..

    public bool IsPanelItemDrop
    {
        //워프입구패널을 제외하고 아이템이 드랍될수 있는가?
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
            {
                //if (m_ListPanel[i] is WarpInPanel) continue;//이제 워프는 m_ListPanel에 속하지 않는다. 

                if (m_ListPanel[i].ItemDrop == false)
                    return false;
            }

            return true;
        }
    }
    //아이템이 매칭 될수 있는가?
    private bool IsPanelItemMatch   //private은 무조껀 조함조건을 사용
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ItemMatch == false)
                    return false;

            return true;
        }
    }
    //아이템이 스위칭 할수 있는가? 터치 이동.
    private bool IsPanelItemSwitch  //private은 무조껀 조함조건을 사용
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ItemSwitch == false)
                    return false;

            return true;
        }
    }
    //현재 패널에서 아이템이 터질수 있는가? ex)케이지인가?
    public bool IsPanelItemBrust
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ItemBrust == false)
                    return false;

            return true;
        }
    }
    //모든패널이 일반적인 폭발을 하는가?
    public bool IsPanelBrust
    {
        get
        {       
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
            {
                    if (m_ListPanel[i].PanelBrust == false)
                    return false;
            }
            return true;
        }
    }

    //모든패널이 사라지는가?
    public bool IsPanelDestory
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
            {
                if (m_ListPanel[i].PanelDestroy == false)
                    return false;
            }
            return true;
        }
    }

    //패널이 주변매칭으로 터질수 있는가?
    public bool IsPanelArountBrust
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ArountBrust == true)
                    return true;
            return false;
        }
    }

    public bool IsPanelTogetherMission
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].TogetherMission == true)
                    return true;
            return false;
        }
    }

    //public bool IsPanelWarpIn
    //{
    //    get
    //    {
    //        for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
    //            if (m_ListPanel[i] is WarpInPanel)
    //                return true;

    //        return false;
    //    }
    //}

    //public bool IsPanelWarpOut
    //{
    //    get
    //    {
    //        for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
    //            if (m_ListPanel[i] is WarpOutPanel)
    //                return true;

    //        return false;
    //    }
    //}


    #endregion

    #region IsPanel 패널 두개이상 조건
    //각 패널별로 체크를 해야한다.
    //위에 있는 조건을 조합하는게 아니라 패널의 조건을 조합한다.
    public bool IsPanelCage
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
                if (m_ListPanel[i].ItemExist && m_ListPanel[i].ItemDrop == false)
                    return true;

            return false;
        }
    }

    //고정물 패널인가? (드랍을 방해하는 고정장치를 체크)
    public bool IsPanelFixed        //쉽게 말하면 아이템은 있을수 없는 고정패널
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
            {
                if (m_ListPanel[i].ItemExist == false && m_ListPanel[i].PanelDestroy == false)
                    return true;
            }

            return false;
        }
    }

    public bool IsPanelNeverMission //결코 터지지 않고 미션과 같이 갈수 없으면
    {
        get
        {
            for (int i = 0, len = m_ListPanel.Count; i < len; i++)     //패널조건 체크
            {
                if (m_ListPanel[i].TogetherMission == false && m_ListPanel[i].PanelDestroy == false)
                    return true;
            }

            return false;
        }
    }
    
    public bool IsPanelBrustDestroy
    {
        get
        {
            //이제 순서대로 터지도록 수정되었으니 첫번째 패널만 체크해야한다!!
            if (m_ListPanel.Count > 0)
            {
                if (m_ListPanel[m_ListPanel.Count - 1].PanelBrust && m_ListPanel[m_ListPanel.Count - 1].PanelDestroy)
                    return true;
            }
            return false;
        }
    }

    public bool IsNowPanelBrust
    {
        get
        {
            //이제 순서대로 터지도록 수정되었으니 첫번째 패널만 체크해야한다!!
            if (m_ListPanel.Count > 0)
            {
                if (m_ListPanel[m_ListPanel.Count - 1].PanelBrust)
                    return true;
            }
            return false;
        }
    }
    #endregion

    #region IsNow 아이템조건 + 패널조건
    //현재 블럭에 아이템이 비어있는가? 아이템을 넣을 공간이 있어?
    public bool IsNowItemEmpty
    {
        get
        {
            if (!IsItemExist && IsPanelItemExist) //아이템이 없고 존재할수 있는 조건이면
                return true;
            return false;
        }
    }

    public bool IsNowItemMatch
    {
        get
        {
            if (IsItemMatch && IsPanelItemMatch)
                return true;
            return false;
        }
    }

    public bool IsNowItemBrust
    {
        get
        {
            if (IsItemExist && IsPanelItemBrust && IsItemBrust && m_ItemBrusting == false) //아이템이 존재하고 아이템이 터질수 있는 조건이면
                return true;
            return false;
        }
    }

    public bool IsNowPanelBrustDestroy
    {
        get
        {
            if (IsPanelBrustDestroy && m_PanelBrusting == false) //터지고 사라지는 패널이 존재하고 지금 터질 수 있으면.
                return true;
            return false;
        }
    }

    ////지금 당장 드랍할수 있느냐?
    //public bool IsNowItemDrop
    //{
    //    get
    //    {
    //        //터지고 있는 중일때는 드랍 안된다.
    //        if(m_ItemBrusting)
    //            return false;
    //        if (m_DropAnim)
    //            return false;

    //        //아이템이 있고 아이템이 존재할수 있고 이동할수 있으면
    //        if (IsItemExist && IsCondItemDrop) 
    //            return true;
    //        return false;
    //    }
    //}
    public bool IsNowItemDrop
    {
        get
        {
            //터지고 있는 중일때는 드랍 안된다.
            if (m_ItemBrusting)
                return false;
            if (m_DropAnim)
                return false;

            //Warp의 무한루프 방지
            if (IsItemExist && m_IsWarpInBoard)
            {
                if (m_Item.m_WarpPassCount >= m_WarpCount)
                    return false;
            }

            //아이템이 있고 아이템이 존재할수 있고 이동할수 있으면
            if (IsItemDrop && IsPanelItemDrop)
                return true;
            return false;
        }
    }

    //터치로 스위칭 가능한가?
    public bool IsNowItemSwitch
    {
        get
        {
            if (IsItemSwitch && IsPanelItemSwitch)
                return true;
            return false;
        }
    }

    //당장 매칭을 할수 있는가?
    public bool IsNowMatchSwitch
    {
        get
        {
            //아이템이 매칭과 스위칭 되는가?
            if (IsItemMatch && IsNowItemSwitch)
                return true;
            return false;
        }
    }
    public bool IsNowBoardBrust
    {
        get
        {
            if (IsNowItemBrust || IsNowPanelBrustDestroy)
                return true;
            return false;
        }
    }
    #endregion

    #region 아이템, 패널들의 조건 조합
    //현재 블럭이 아이템을 떨어트릴수 있는가? 현재블럭의 유무를 떠나서 내려올수 있는지 조건만 체크
    public bool IsCondItemDrop
    {
        get
        {           
            //아이템이 존재하더라도 드랍할수 없는 상황
            if (IsItemExist)
            {
                //젤리몬, 유령
                if(IsItemDrop == false)
                    return false;

                //Warp의 무한루프 방지
                if (m_IsWarpInBoard)
                {
                    if (m_Item.m_WarpPassCount >= m_WarpCount)
                        return false;
                }
            }

            if (IsPanelItemExist && IsPanelItemDrop) //아이템이 존재할수 있고 이동할수 있고
                return true;

            return false;
        }
    }

    #endregion


    public void Init(int x, int y, Stage stage)
    {
        //데이타 세팅
        X = x;
        Y = y;
        m_CSD = stage;
        PossibleDrop_Dirs.Clear();
        PossibleDrop_Dirs.AddRange(m_CSD.DropDirs[Index]);
        Drop_Dir = PossibleDrop_Dirs[0];

        if (PossibleDrop_Dirs.Contains(DROP_DIR.List))
        {
            PossibleDrop_Dirs.Remove(DROP_DIR.List);
            isListDrop = true;
        }
        transform.localPosition = new Vector3(x * MatchDefine.BoardWidth, -y * MatchDefine.BoardHeight, 0);

        //변수 초기화
        //m_Brusting = false;
        m_ItemBrusting = false;
        m_PanelBrusting = false;
        m_PanelBrusting_Ctn = 0;
        m_DropAnim = false;
        m_SideDrop = false;

        //아이템,패널초기화. 스폐셜패널들은 Static이기 때문에 한번만 호출하기 위해 여기서 하지 않는다.
        ItemDestroy();
        PanelDestroy();

        //*************************
        //1) 패널캐싱변수 초기화
        //*************************
        m_IsCreatorEmpty = false;
        m_IsCreatorFood = false;
        m_IsCreatorSprial = false;
        m_IsCreatorTimeBomb = false;
        m_IsCreatorFoodSprial = false;
        m_IsCreatorFoodTimeBomb = false;
        m_IsCreatorSprialTimeBomb = false;
        m_IsCreatorKey = false;
        m_IsCreatorKeyFood = false;
        m_IsCreatorKeyTimeBomb = false;

        m_IsWarpInBoard = false;
        m_WarpOutBoard = null;
        m_IsWarpOutBoard = false;
        m_WarpInBoard = null;
        //m_WarpIndex = -1;
        m_IsConveyerBeltBoard = false;

        //******************************************
        //2) 패널캐싱변수 초기화(게임중 변경되는 값)
        //******************************************
        m_IsCakeBoard = false;
        m_IsJamBoard = false;
        m_IsRingBoard = false;



        StopAllCoroutines();

        //기타
        Side_Dir = Random.Range(0,2) == 0?SQR_DIR.TOPLEFT:SQR_DIR.TOPRIGHT;
    }

 
    public bool Equals(Board bd)
    {
        if (X == bd.X && Y == bd.Y)
        return true;
        else
            return false;
    }

    void Start()
    {
        MatchMgr = MatchManager.Instance;
        ItemMgr = ItemManager.Instance;
    }
    #region Board 관련 정보
    public Vector3 GetBoardPos()
    {
        return transform.position;
    }
    #endregion

    #region DefaultPanel 생성
    public void DefaultPanel(PanelType type, int def, int val, string addInfo)
    {
        //***********************
        //Default_Full패널의 조건은 사실상 의미가 없다.
        //오히려 PanelBrust, ArroundBrust 값들이 조건체크에 방해가 된다.
        //그래서 m_ListPanel에 추가 하지 않는다.
        //하지만 반대개념인 Empty는 조건체크해야하므로 변경하지 않는다.
        GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
        panel_Obj.transform.position = GetBoardPos() + new Vector3(0, 0, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
        panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
        m_DefaultPanel = panel_Obj.GetComponent<Panel>();
    }
    #endregion

    #region Panel 생성
    public void GenPanel(PanelType type, int def, int val, string addInfo)
    {
        //********
        //주얼트리
        //********
        if (type == PanelType.JewelTree_B)
        {
            m_ListPanel.Add(this[SQR_DIR.LEFT].GetPanel(PanelType.JewelTree_A));  //만들지 않고 A의 패널을 넣어준다.
            return;
        }
        if (type == PanelType.JewelTree_C)
        {
            m_ListPanel.Add(this[SQR_DIR.TOP].GetPanel(PanelType.JewelTree_A));  //만들지 않고 A의 패널을 넣어준다.
            return;
        }
        if (type == PanelType.JewelTree_D)
        {
            m_ListPanel.Add(this[SQR_DIR.TOPLEFT].GetPanel(PanelType.JewelTree_A));  //만들지 않고 A의 패널을 넣어준다.
            return;
        }

        //*****
        //석판
        //*****
        if (type == PanelType.Stele && val != Index)
        {
            //만들지 않고 A의 패널을 넣어준다.
            m_ListPanel.Add(MatchMgr.m_ListBoard[val].GetPanel(PanelType.Stele));
            return;
        }

        GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
        panel_Obj.transform.position = GetBoardPos() + new Vector3(0, 0, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
        panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
        m_ListPanel.Add(panel_Obj.GetComponent<Panel>());

        //실시간으로 패널을 참조하는 경우는 캐싱변수를 둔다.
        switch (type)
        {
            
            case PanelType.Creator_Empty:
                {
                    m_IsCreatorEmpty = true;
                }
                break;
            case PanelType.Creator_Food:
                {
                    m_IsCreatorFood = true;
                }
                break;
            case PanelType.Creator_Sprial:
                {
                    m_IsCreatorSprial = true;
                }
                break;
            case PanelType.Creator_TimeBomb:
                {
                    m_IsCreatorTimeBomb = true;
                }
                break;
            case PanelType.Creator_Food_Sprial:
                {
                    m_IsCreatorFoodSprial = true;
                }
                break;
            case PanelType.Creator_Food_TimeBomb:
                {
                    m_IsCreatorFoodTimeBomb = true;
                }
                break;
            case PanelType.Creator_Sprial_TimeBomb:
                {
                    m_IsCreatorSprialTimeBomb = true;
                }
                break;
            case PanelType.Creator_Key:
                {
                    m_IsCreatorKey = true;
                }
                break;
            case PanelType.Creator_Key_Food:
                {
                    m_IsCreatorKeyFood = true;
                }
                break;
            case PanelType.Creator_Key_TimeBomb:
                {
                    m_IsCreatorKeyTimeBomb = true;
                }
                break;
            case PanelType.Cake_A:
            case PanelType.Cake_B:
            case PanelType.Cake_C:
            case PanelType.Cake_D:
                {
                    m_IsCakeBoard = true;
                }
                break;
            case PanelType.Jam:
                {
                    m_IsJamBoard = true;
                }
                break;
            case PanelType.MagicColor:
                {
                    m_ListMagicColorBoard.Add(this);
                }
                break;
            case PanelType.Bottle_Cage:
                {
                    m_ListBottlePanel.Add(panel_Obj.GetComponent<BottleCagePanel>());
                }
                break;
            case PanelType.Ring:
                {
                    m_IsRingBoard = true;
                    m_ListRingPanel.Add(panel_Obj.GetComponent<RingPanel>());
                }
                break;
            case PanelType.Stele:   //여기는 석판 보드중 첫번째 보드만 들어온다(index == value)
                {
                    //해당석판과 묶여있는 Board를 세팅한다.
                    panel_Obj.GetComponent<StelePanel>().SetSteleBoards((SteleType)def);
                }
                break;
        }
    }
    public Panel GetPanel(PanelType type)
    {
        for (int i = 0; i < m_ListPanel.Count; i++)
        {
            if (m_ListPanel[i].m_PanelType == type)
                return m_ListPanel[i];
        }

        return null;
    }

    public T GetPanel<T>() where T : Panel
    {
        for (int i = 0; i < m_ListPanel.Count; i++)
        {
            if (m_ListPanel[i] is T)
                return (T)m_ListPanel[i];
        }

        return null;
    }
    #endregion

    public void SetDropRotation(Transform trans)
    {
        switch (Drop_Dir)
        {
            case DROP_DIR.D:
                trans.localRotation = Quaternion.Euler(new Vector3(0, 0, 180));
                break;
            case DROP_DIR.U:
                trans.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                break;
            case DROP_DIR.L:
                trans.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));
                break;
            case DROP_DIR.R:
                trans.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
                break;
        }
    }

    #region 특수 Panel 생성
    private Vector3 GetDropPanelPostion(PanelType type)
    {
        float value = 0.36f;
        if (type == PanelType.FoodArrive)
            value = 0.18f;
        switch (Drop_Dir)
        {
            case DROP_DIR.D:
                return new Vector3(0, value, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
            case DROP_DIR.U:
                return new Vector3(0, -value, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
            case DROP_DIR.L:
                return new Vector3(value, 0, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
            case DROP_DIR.R:
                return new Vector3(-value, 0, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
        }

        return Vector3.zero;

    }
    
    public void GenSpecialPanel(PanelType type, int def, int val, string addInfo)
    {
        switch (type)
        {
            case PanelType.Warp_In:
                {
                    //워프는 드랍조건만 필요하고 다른조건들은 일반패널조건들과 충돌한다.
                    //워프는 드랍프로세스상에 처리상에서 우선순위 및 예외로 처리되기 때문에 따로 관리한다.
                    GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
                    panel_Obj.transform.position = GetBoardPos() + new Vector3(0, -0.4f, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
                    panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
                    m_ListWarpPanel.Add(panel_Obj.GetComponent<Panel>());

                    //------------------------------------------------
                    panel_Obj.transform.localPosition = GetDropPanelPostion(type);
                    SetDropRotation(panel_Obj.transform);

                    m_IsWarpInBoard = true;
                    for (int i = 0; i < m_CSD.panels.Length; i++)
                    {
                        for (int k = 0; k < m_CSD.panels[i].listinfo.Count; k++)
                        {
                            if (m_CSD.panels[i].listinfo[k].paneltype == PanelType.Warp_Out
                                && m_CSD.panels[i].listinfo[k].value == val)
                            {
                                m_WarpOutBoard = MatchMgr.m_ListBoard[i];
                                //m_WarpIndex = val;
                                break;
                            }
                        }
                        if (m_WarpOutBoard != null)
                            break;
                    }

                    //워프갯수는 In에서만 체크
                    m_WarpCount++;
                }
                break;
            case PanelType.Warp_Out:
                {
                    GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
                    panel_Obj.transform.position = GetBoardPos() + new Vector3(0, -0.4f, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
                    panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
                    m_ListWarpPanel.Add(panel_Obj.GetComponent<Panel>());

                    //--------------------------------------------------------
                    Vector3 pos = GetDropPanelPostion(type);
                    pos.z = -pos.z;
                    pos = -pos;
                    panel_Obj.transform.localPosition = pos;
                    SetDropRotation(panel_Obj.transform);

                    m_IsWarpOutBoard = true;
                    for (int i = 0; i < m_CSD.panels.Length; i++)
                    {
                        for (int k = 0; k < m_CSD.panels[i].listinfo.Count; k++)
                        {
                            if (m_CSD.panels[i].listinfo[k].paneltype == PanelType.Warp_In
                                && m_CSD.panels[i].listinfo[k].value == val)
                            {
                                m_WarpInBoard = MatchMgr.m_ListBoard[i];
                                //m_WarpIndex = val;
                                break;
                            }
                        }
                        if (m_WarpInBoard != null)
                            break;
                    }
                }
                break;
            case PanelType.FoodArrive:
                {
                    GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
                    panel_Obj.transform.localPosition = GetDropPanelPostion(type);

                    panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
                    m_ListFoodArvPanel.Add(panel_Obj.GetComponent<Panel>());
                    SetDropRotation(panel_Obj.transform);
                }
                break;
            case PanelType.JellyBearStart:
                {
                    GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
                    panel_Obj.transform.position = GetBoardPos() + new Vector3(0, -0.3f, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
                    panel_Obj.GetComponent<Panel>().Init(this, type, def, val, addInfo);
                    m_ListBearStartPanel.Add(panel_Obj.GetComponent<Panel>());
                }
                break;
            case PanelType.ConveyerBelt:
                {
                    GameObject panel_Obj = PanelManager.Instance.CreatePanel(type, transform);
                    panel_Obj.transform.position = GetBoardPos() + new Vector3(0, 0, (float)type > 0 ? (float)type / 100 : (float)type / 100 - 0.5f);
                    panel_Obj.GetComponent<ConveyerBeltPanel>().Init(this, type, def, val, addInfo);
                    m_ListConveyerBeltPanel.Add(panel_Obj.GetComponent<ConveyerBeltPanel>());

                    m_IsConveyerBeltBoard = true;
                }
                break;
            default:
                Debug.LogError("특수한 패널 추가 ..");
                break;
        }
    }
    public ConveyerBeltPanel GetConveyerBeltPanel()
    {
        for (int i = 0; i < m_ListConveyerBeltPanel.Count; i++)
        {
            if (m_ListConveyerBeltPanel[i].m_Board == this)
                return m_ListConveyerBeltPanel[i];
        }
        return null;
    }

    #endregion

    #region JamPanel 동적 생성
    //JamPanel
    public bool IsJamPanelPossible()
    {
        if (IsPanelFixed)
            return false;

        //방해물, 케이지
        if (IsPanelItemExist == false || IsPanelItemBrust == false)
            return false;

        //매직컬러..(컨베이어벨트처럼 위에 아이템이 존재하지만 가득차있어서 미션블럭이 있을수 없다.)
        if(IsPanelNeverMission)
            return false;

        return true;
    }
    public void JamPanelCreate(float delay)
    {
        //이미 잼이면 코루틴 안태운다.
        if (m_IsJamBoard)
            return;

        StartCoroutine(Co_JamPanelCreate(delay));
    }

    IEnumerator Co_JamPanelCreate(float delay)
    {
        if (delay > 0) //1 frame으로 문제가 생김..바로 생겨야되는 곳이 있음.
            yield return new WaitForSeconds(delay);

        //딜레이가 지난 다음에 패널조건을 체크하자!
        if (IsJamPanelPossible() == false)
            yield break;

        //위에서 체크하지만 delay동안 여러번 탈수도 있다.
        //그렇다고 위에서 바로 m_IsJamBoard값을 true를 주면 아직 터지지도 않은 블럭에서 딸기잼이 퍼지는현상이 나올꺼 같다..
        if (m_IsJamBoard == false)
        {
            GenPanel(PanelType.Jam, -1, 0, null);
            GetPanel(PanelType.Jam).Create();
        }
    }

    //방해물, 케이지 : 잼이 한번이라도 false조건이 되면 그 뒤로 잼번식 못함.
    public bool JamPanelCreateCountinue(bool jam)
    {
        //한번 flase가 오면 그뒤로 계속 false
        if (jam)
        {
            //패널조건을 체크해서 false값이 오면 그 뒤로 증식 못함.
            jam = IsJamPanelPossible();
            if (jam) JamPanelCreate(0);

            //만약 빈칸, 생성기..처럼 절대 미션이 불가능한 보드라면 연속성을 유지한다.
            if (IsPanelNeverMission)
                jam = true;
        }
        return jam;
    }
    #endregion

    #region Item 생성
    public Item GenItem(ItemType type, ColorType color, int val = 0, bool falling = false, bool score = false, bool copy = false)
    {
        if (type == ItemType.None) return null;

        if (m_Item != null)
        {
            //아이템모으기 미션 체크
            //현재 아이템을 다른 아이템으로 바꿀때는 사라지는 아이템은 미션 체크를 한다.
            if (m_Item.m_ItemType != ItemType.JellyBear || type != ItemType.Normal) //젤리베어는 마지막칸에서 일반블럭으로 바뀐다.예외처리..
                m_Item.MissionApply();
            ItemDestroy();
        }

        GameObject item_Obj = ItemMgr.CreateItem(type);
        m_Item = item_Obj.GetComponent<Item>();

        m_Item.Init(this, type, color, val, copy);

        if (falling)
        {
            m_Item.transform.position = GetBoardPos() + (GetDropVector() * MatchDefine.BoardHeight);// * CreateCnt[X]);
                                                                                                    //item.GetComponent<Item>().justCreatedItem = true;

            m_Item.m_CreateAnim = true;
            var cached_item = m_Item;

            m_Item.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);//Vector3.zero;
            Sequence sq = DOTween.Sequence();
            sq.Append(m_Item.transform.DOScale(Vector3.one, 0.13f)).OnComplete(() =>
            {
                m_Item.m_CreateAnim = false;
                cached_item.m_CreateAnim = false;
            });

        }
        else
            m_Item.transform.position = GetBoardPos();


        //점수 출력
        if (score)
            MatchMgr.ScoreTextShow(m_Item.CreateScore, this, color);   //_ 생성된 아이템..

        return m_Item;
    }

    public Item CreateItem(ItemType type, ColorType color)
    {
        Item item_Obj = ItemMgr.CreateItem(type).GetComponent<Item>();

        item_Obj.Init(this, type, color);

        item_Obj.transform.position = GetBoardPos();

        return item_Obj;
    }

    public void MysteryChange(ColorType color)
    {
        //현재 스테이지의 세팅타입은 스테이지마다 들고 있고
        //세팅타입에 따른 확률 정보는 ItemManager가 로드해서 들고 있다.
        int[] minfo = ItemMgr.m_MysteryData.List_Prob[(int)MatchMgr.m_CSD.Mystery_SettingType];

        int random = 0;
        MysteryItemType mysterytype = MysteryItemType.Normal;
        int _sum = 0;
        if (m_IsConveyerBeltBoard) //컨베이어 벨트 위라면..아이템류만 나온다.
        {
            random = Random.Range(0, ItemMgr.m_MysteryData.totalonlyItem[(int)MatchMgr.m_CSD.Mystery_SettingType]);

            for (int i = 0; i < (int)MysteryItemType.Last; i++)
            {
                switch ((MysteryItemType)i)
                {
                    case MysteryItemType.Normal:
                    case MysteryItemType.Line:
                    case MysteryItemType.Bomb:
                    case MysteryItemType.XLine:
                    case MysteryItemType.Rainbow:
                    case MysteryItemType.JellyBear:
                    case MysteryItemType.Spiral:
                    case MysteryItemType.TimeBomb:
                    case MysteryItemType.Chameleon:
                            _sum += minfo[i];                           
                        break;
                    default:
                        break;
                }

                if (random < _sum)
                {
                    mysterytype = (MysteryItemType)i;
                    break;
                }
            }
        }
        else
        {
            random = Random.Range(0, ItemMgr.m_MysteryData.total[(int)MatchMgr.m_CSD.Mystery_SettingType]);

            for (int i = 0; i < (int)MysteryItemType.Last; i++)
            {
                _sum += minfo[i];
                if (random < _sum)
                {
                    mysterytype = (MysteryItemType)i;
                    break;
                }
            }
        }  
      
        
        switch (mysterytype)
        {
            case MysteryItemType.Normal:
                {
                    GenItem(ItemType.Normal, color);
                }
                break;
            case MysteryItemType.Line:
                {
                    ItemType _it = Random.Range(0, 2) == 0 ? ItemType.Line_X : ItemType.Line_Y;
                    GenItem(_it, color);
                }
                break;
            case MysteryItemType.Bomb:
                {
                    GenItem(ItemType.Bomb, color);
                }
                break;
            case MysteryItemType.XLine:
                {
                    GenItem(ItemType.Line_X, color);
                }
                break;
            case MysteryItemType.Rainbow:
                {
                    GenItem(ItemType.Rainbow, ColorType.None);
                }
                break;
            case MysteryItemType.Normal_Ice:
                {
                    GenItem(ItemType.Normal, color);
                    GenPanel(PanelType.Ice_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.Line_Ice:
                {
                    ItemType _it = Random.Range(0, 2) == 0 ? ItemType.Line_X : ItemType.Line_Y;
                    GenItem(_it, color);
                    GenPanel(PanelType.Ice_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.Bomb_Ice:
                {
                    GenItem(ItemType.Bomb, color);
                    GenPanel(PanelType.Ice_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.XLine_Ice:
                {
                    GenItem(ItemType.Line_X, color);
                    GenPanel(PanelType.Ice_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.Rainbow_Ice:
                {
                    GenItem(ItemType.Rainbow, ColorType.None);
                    GenPanel(PanelType.Ice_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.Bread1:
                {
                    ItemDestroy();
                    GenPanel(PanelType.Bread_Block, 1, 0, null);
                }
                break;
            case MysteryItemType.Bread2:
                {
                    ItemDestroy();
                    GenPanel(PanelType.Bread_Block, 2, 0, null);
                }
                break;
            case MysteryItemType.Bread3:
                {
                    ItemDestroy();
                    GenPanel(PanelType.Bread_Block, 3, 0, null);
                }
                break;
            case MysteryItemType.Bread4:
                {
                    ItemDestroy();
                    GenPanel(PanelType.Bread_Block, 4, 0, null);
                }
                break;
            case MysteryItemType.Bread5:
                {
                    ItemDestroy();
                    GenPanel(PanelType.Bread_Block, 5, 0, null);
                }
                break;
            case MysteryItemType.IceCream:
                {
                    ItemDestroy();
                    GenPanel(PanelType.IceCream_Block, 1, 0, null);
                }
                break;
            case MysteryItemType.JellyBear:
                {
                    GenItem(ItemType.JellyBear, color);
                }
                break;
            case MysteryItemType.Spiral:
                {
                    GenItem(ItemType.Spiral, ColorType.None);
                }
                break;
            case MysteryItemType.TimeBomb:
                {
                    GenItem(ItemType.TimeBomb, color);
                }
                break;
            case MysteryItemType.Normal_Lolly:
                {
                    GenItem(ItemType.Normal, color);
                    GenPanel(PanelType.Lolly_Cage, 1, 0, null);
                }
                break;
            case MysteryItemType.Chameleon:
                {
                    GenItem(ItemType.Chameleon, color);
                }
                break;
            default:
                break;
        }
    }
    #endregion

    #region 매칭되는 아이템 찾기 
    List<Board> MatchItemList = new List<Board>();
    List<Board> TempList = new List<Board>();

    //현재Squre의 주변에 같은색이 3개 이상이면 리턴.
    //Horizontal과Vertical를 동시에 구하는게 Around인데 하나의 리스트로 나오기때문에 matches로 조건필요하다.
    public List<Board> FindMatchesAround(int matches = 2)
    {
        MatchItemList.Clear();

        if (m_Item == null) return MatchItemList;

        if (m_Item.Match == false) return MatchItemList;


        //** 3개 매칭 가로 **//
        //초기화
        TempList.Clear();

        //좌우로 체크해서 아이템을 countedSquares에 담은후
        //FindMatches_X_Recursive(m_Item.m_Color, ref TempList);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref TempList, SQR_DIR.LEFT);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref TempList, SQR_DIR.RIGHT);

        //3개이상 매치되면
        if (TempList.Count >= matches)
        {
            for (int i = 0; i < TempList.Count; i++)
            {
                if (MatchItemList.Contains(TempList[i]) == false)
                    MatchItemList.Add(TempList[i]);
            }
        }

        //** 3개 매칭 세로 체크 **//
        //초기화
        TempList.Clear();
        //상하로 체크해서 아이템을 countedSquares에 담은후
        //FindMatches_Y_Recursive(m_Item.m_Color, ref TempList);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref TempList, SQR_DIR.TOP);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref TempList, SQR_DIR.BOTTOM);

        //3개이상 매치되면
        if (TempList.Count >= matches)
        {
            for (int i = 0; i < TempList.Count; i++)
            {
                if (MatchItemList.Contains(TempList[i]) == false)
                    MatchItemList.Add(TempList[i]);
            }
        }

        //** 4개 사각형 매칭 체크 **?/
        //초기화
        TempList.Clear();

        FindMatchesSquare(ref TempList);
        //4개 사각형이 매칭 되면
        if (TempList.Count >= 3)
        {
            for (int i = 0; i < TempList.Count; i++)
            {
                if (MatchItemList.Contains(TempList[i]) == false)
                    MatchItemList.Add(TempList[i]);
            }
        }


        //아. 일단 이 함수를 조건 체크용으로 밖에 사용안했기 때문에 구지 현재 보드를 포함할필요없음.
        //if(MatchItemList.Count > 0)
        //    MatchItemList.Add(this);//최종적으로 현재 보드도 포함.

        return MatchItemList;
    }

    public void FindMatchesHorizontal(ref List<Board> listx)
    {
        listx.Clear();

        //FindMatches_X_Recursive(m_Item.m_Color, ref TempList);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref listx, SQR_DIR.LEFT);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref listx, SQR_DIR.RIGHT);

        if (listx.Count < 2)
            listx.Clear();
    }

    public void FindMatchesVertical(ref List<Board> listy)
    {
        listy.Clear();

        //FindMatches_Y_Recursive(m_Item.m_Color, ref TempList);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref listy, SQR_DIR.TOP);
        FindMatches_Dir_Recursive(m_Item.m_Color, ref listy, SQR_DIR.BOTTOM);

        if (listy.Count < 2)
            listy.Clear();
    }

    //동서남북 중 한쪽 방향만 체크하는 방법.
    Board NextBoard;
    public void FindMatches_Dir_Recursive(ColorType color, ref List<Board> countedItem, SQR_DIR dir)
    {
        NextBoard = this[dir];

        if (NextBoard == null) return;

        if (NextBoard.IsNowItemMatch == false) return;

        if (NextBoard.m_ItemBrusting || NextBoard.m_DropAnim) return; //터지거나 이동중이면 체크 안함.

        if (NextBoard.m_Item.m_Color == color)
        {
            //타입이 같으니까 추가
            countedItem.Add(NextBoard);

            //그다음 아이템도 같은가 계속 타고 들어간다.
            NextBoard.FindMatches_Dir_Recursive(color, ref countedItem, dir);
        }
    }

    //오토 매칭에서 사용
    public int FindMatches_Dir_Recursive(ColorType color, SQR_DIR dir, ref bool isJam, int count = 0)
    {
        NextBoard = this[dir];

        if (NextBoard == null) return count;

        if (NextBoard.IsNowItemMatch == false) return count;

        if (NextBoard.m_ItemBrusting || NextBoard.m_DropAnim) return count; //터지거나 이동중이면 체크 안함.

        if (NextBoard.m_Item.m_Color == color)
        {
            if (!isJam)
                isJam = NextBoard.m_IsJamBoard;
            //그다음 아이템도 같은가 계속 타고 들어간다.
            count = NextBoard.FindMatches_Dir_Recursive(color, dir, ref isJam, ++count);
        }

        return count;
    }

    //사용하지 않는다. 불필요하게 함수를 타는 부분이 존재한다.
    ////Left, Right을 같이 체크.
    //public void FindMatches_X_Recursive(ColorType color, ref List<Board> countedItem)
    //{
    //    if(IsNowItemMatch == false) return;

    //    if (m_ItemBrusting || m_DropAnim) return; //터지거나 이동중이면 체크 안함.

    //    if (m_Item.m_Color == color && countedItem.Contains(this) == false)
    //    {
    //        //타입이 같으니까 추가
    //        countedItem.Add(this);

    //        //그다음 아이템도 같은가 계속 타고 들어간다.
    //        if (this[SQR_DIR.LEFT] != null)
    //            this[SQR_DIR.LEFT].FindMatches_X_Recursive(color, ref countedItem);
    //        if (this[SQR_DIR.RIGHT] != null)
    //            this[SQR_DIR.RIGHT].FindMatches_X_Recursive(color, ref countedItem);
    //    }
    //}

    ////Top, Bottom을 같이 체크.
    //public void FindMatches_Y_Recursive(ColorType color, ref List<Board> countedItem)
    //{
    //    if (IsNowItemMatch == false) return;

    //    if (m_ItemBrusting || m_DropAnim) return; //터지거나 이동중이면 체크 안함.

    //    if (m_Item.m_Color == color && countedItem.Contains(this) == false)
    //    {
    //        //타입이 같으니까 추가
    //        countedItem.Add(this);

    //        //그다음 아이템도 같은가 계속 타고 들어간다.
    //        if (this[SQR_DIR.TOP] != null)
    //            this[SQR_DIR.TOP].FindMatches_Y_Recursive(color, ref countedItem);
    //        if (this[SQR_DIR.BOTTOM] != null)
    //            this[SQR_DIR.BOTTOM].FindMatches_Y_Recursive(color, ref countedItem);
    //    }
    //}
    #endregion

    #region 사각형매칭 나비 체크
    bool c_top;
    bool c_bottom;
    bool c_left;
    bool c_right;
    public void FindMatchesSquare(ref List<Board> listsquare)
    {
        listsquare.Clear();

        c_top = CheckColorMatch(m_Item.m_Color, SQR_DIR.TOP);
        c_bottom = CheckColorMatch(m_Item.m_Color, SQR_DIR.BOTTOM);
        c_left = CheckColorMatch(m_Item.m_Color, SQR_DIR.LEFT);
        c_right = CheckColorMatch(m_Item.m_Color, SQR_DIR.RIGHT);

        if (c_top && c_left)
        {
            if (CheckColorMatch(m_Item.m_Color, SQR_DIR.TOPLEFT))
            {
                listsquare.Add(this[SQR_DIR.TOP]);
                listsquare.Add(this[SQR_DIR.LEFT]);
                listsquare.Add(this[SQR_DIR.TOPLEFT]);

                return;
            }
        }

        if (c_top && c_right)
        {
            if (CheckColorMatch(m_Item.m_Color, SQR_DIR.TOPRIGHT))
            {
                listsquare.Add(this[SQR_DIR.TOP]);
                listsquare.Add(this[SQR_DIR.RIGHT]);
                listsquare.Add(this[SQR_DIR.TOPRIGHT]);

                return;
            }
        }

        if (c_bottom && c_left)
        {
            if (CheckColorMatch(m_Item.m_Color, SQR_DIR.BOTTOMLEFT))
            {
                listsquare.Add(this[SQR_DIR.BOTTOM]);
                listsquare.Add(this[SQR_DIR.LEFT]);
                listsquare.Add(this[SQR_DIR.BOTTOMLEFT]);

                return;
            }
        }

        if (c_bottom && c_right)
        {
            if (CheckColorMatch(m_Item.m_Color, SQR_DIR.BOTTOMRIGHT))
            {
                listsquare.Add(this[SQR_DIR.BOTTOM]);
                listsquare.Add(this[SQR_DIR.RIGHT]);
                listsquare.Add(this[SQR_DIR.BOTTOMRIGHT]);

                return;
            }
        }
    }

    Board checkboard;
    public bool CheckColorMatch(ColorType color, SQR_DIR dir)
    {
        checkboard = this[dir];

        if (checkboard == null)
            return false;
        if (checkboard.IsNowItemMatch == false)
            return false;
        if (checkboard.m_ItemBrusting || checkboard.m_DropAnim)  //터지거나 이동중이면 체크 안함.
            return false;
        if (checkboard.m_Item.m_Color != color)
            return false;

        return true;
    }

    #endregion
    #region 아이템 드랍
    //public bool DropItemRow()
    //{
    //    Board top = this[SQR_DIR.TOP];

    //    bool IsCondTopDrop = true;
    //    bool IsCondSideDrop = true;

    //    bool IsTopDropSpawnLine = true;

    //    if (IsNowItemEmpty)
    //    {
    //        if (top == null)
    //        {
    //            if(m_CSD.DropSpawnLine[X])
    //            {
    //                if (MissionManager.Instance.CreatFoodItem(this) == false)
    //                    GenItem(ItemType.Normal, ColorType.Rnd, true);

    //                DropAnim(true, false);

    //                IsCondTopDrop = false; //맨위인데 top없으니까 false
    //            }
    //            else
    //                IsTopDropSpawnLine = false;
    //        }
    //        else
    //        {
    //            //top에서 가져오기
    //            if (top.IsNowItemDrop && top.m_DropAnim == false)
    //            {
    //                DropItem(top, false, false);
    //            }

    //            IsCondTopDrop = top.DropItemRow();

    //            if (IsCondTopDrop == false && IsItemExist == false)  //top은 내려줄수 없는 상황이고 아이템은 없으면
    //            {
    //                //side에서 가져오기
    //                //Side는 지금 당장 드랍될아이템이 없으면 못하는걸로 가정한다.
    //                //top방향은 위에 상황을 보고 기달려줄수 있지만 side는 상황을 알수 없다.
    //                //(side에 상황을 알려면 side블럭의 top,side를 체크해야하는데..복잡해진다..)
    //                //그러면 현재 아래블럭에선 현재블럭이 drop불가능으로 보고 사이드에서 가져올려고 할것이다.
    //                Board Side = SideDrop(this);
    //                if (Side != null)
    //                {
    //                    if (Side.m_DropAnim == false)
    //                    {
    //                        if (Side.CheckLastDrop()) //즉, 좌우 라인이 채워지고 난 다음에..(로직순서상 우측라인보다 사이드가 먼저 채워지는 문제 해결)
    //                            DropItem(Side, false, true);
    //                    }
    //                }
    //                else
    //                {  
    //                    IsCondSideDrop = false; // 보드에서 사이드 드랍은 불가능..
    //                }
    //            }         
    //        }
    //    }
    //    else
    //    {
    //        if (top != null)
    //            top.DropItemRow();
    //    }

    //    //1.topDrop가능 2. sideDrop가능 3. 채워질보드인가 4. 이동가능한가?
    //    bool dropable = (IsCondTopDrop | IsCondSideDrop) & IsCondItemDrop & IsPanelItemMove;

    //    dropable = dropable & IsTopDropSpawnLine;

    //    return dropable;
    //}

    public Board GetTopBoard()
    {
        if (m_IsWarpOutBoard)
        {
            return m_WarpInBoard;
        }

        Board targetBoard = Top;
        while (true)
        {
            if(targetBoard == null)
                break;

            if (targetBoard.GetPanel(PanelType.Default_Empty) == null)
                break;

            targetBoard = targetBoard.Top;
        }
        return targetBoard;
    }

    public Board GetDropBoard()
    {
        if (m_IsWarpOutBoard)
        {
            return m_WarpInBoard;
        }

        Board targetBoard = Drop;
        while (true)
        {

            if (targetBoard == null)
                break;

            if (targetBoard.GetPanel(PanelType.Default_Empty) == null)
                break;

            if (targetBoard.Drop != null && targetBoard.Drop.PossibleDrop_Dirs.Count > 1)
                break;

            targetBoard = targetBoard.Drop;

        }
        return targetBoard;
    }

    //맨 위에서 떨어지는 드랍아이템
    public bool TopSpawnItem()
    {
        if (m_CSD.defaultSpawnLine[X] == false || (m_CSD.isUseGravity && m_CSD.defaultSpawnLineY[Y] == false))
            return false;

        if (isTutorialSeed)
        {
            GenItem(ItemType.Normal, List_Seed[0], 0, true);
            List_Seed.RemoveAt(0);

            if (List_Seed.Count == 0)
                isTutorialSeed = false;
        }
        else if (MissionManager.Instance.CreatFoodItem(this)) //요리 생성되면 리턴
        {
            //함수안에서 아이템생성함.
        }
        else if (ItemMgr.IsCreateSpiral(this))     //스파이럴 생성 조건
        {
            GenItem(ItemType.Spiral, ColorType.None, 0, true);
        }
        else if (ItemMgr.IsCreateDonut(this)) //도넛 생성 조건
        {
            GenItem(ItemType.Donut, ColorType.None, 0, true);
        }
        else if (ItemMgr.IsCreateTimeBomb(this))
        {
            GenItem(ItemType.TimeBomb, ColorType.Rnd, 0, true);
        }
        else if (ItemMgr.IsCreateMystery(this)) //미스테리 생성 조건
        {
            GenItem(ItemType.Mystery, ColorType.Rnd, 0, true);
        }
        else if (ItemMgr.IsCreateChameleon(this)) //카메렐온 생성 조건
        {
            GenItem(ItemType.Chameleon, ColorType.Rnd, 0, true);
        }
        else if (ItemMgr.IsCreateKey(this)) //열쇠 생성 조건
        {
            GenItem(ItemType.Key, ColorType.Rnd, 0, true);
        }
        else
        {
            GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
        }

        ItemDropAnim(true, false);
        return true;
    }

    //생성기에서 생성되는 아이템.
    public bool CreatorSpawnItem(Board top)
    {
        if(m_IsWarpOutBoard) //현재 보드가 워프출구이면.
        {
            if (top.IsNowItemDrop)
            {
                WarpOutItemCreate(top);
                WarpInItemDestroy(top);
                return true;
            }
        }
        else if(top.m_IsCreatorEmpty)
        {
            return TopSpawnItem();
        }
        else if (top.m_IsCreatorFood)
        {
            if (MissionManager.Instance.CreatFoodItem(this, false))
            {
                //함수안에서 아이템생성함.
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Food).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorSprial)
        {
            if (ItemMgr.IsCreateSpiral(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Spiral, ColorType.None, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Sprial).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorTimeBomb)
        {
            if (ItemMgr.IsCreateTimeBomb(this, false))
            {
                GenItem(ItemType.TimeBomb, ColorType.Rnd, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_TimeBomb).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorFoodSprial)
        {
            if (MissionManager.Instance.CreatFoodItem(this, false))
            {
                //함수안에서 아이템생성함.
            }
            else if(ItemMgr.IsCreateSpiral(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Spiral, ColorType.None, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Food_Sprial).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorFoodTimeBomb)
        {
            if (MissionManager.Instance.CreatFoodItem(this, false))
            {
                //함수안에서 아이템생성함.
            }
            else if (ItemMgr.IsCreateTimeBomb(this, false))
            {
                GenItem(ItemType.TimeBomb, ColorType.Rnd, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Food_TimeBomb).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorSprialTimeBomb)
        {
            if (ItemMgr.IsCreateSpiral(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Spiral, ColorType.None, 0, true);
            }
            else if (ItemMgr.IsCreateTimeBomb(this, false))
            {
                GenItem(ItemType.TimeBomb, ColorType.Rnd, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Sprial_TimeBomb).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorKey)
        {
            if (ItemMgr.IsCreateKey(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Key, ColorType.Rnd, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Key).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorKeyFood)
        {
            if (ItemMgr.IsCreateKey(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Key, ColorType.Rnd, 0, true);
            }
            else if (MissionManager.Instance.CreatFoodItem(this, false))
            {
                //함수안에서 아이템생성함.
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Key_Food).AddAction();
            ItemDropAnim(true, false);
            return true;
        }
        else if (top.m_IsCreatorKeyTimeBomb)
        {
            if (ItemMgr.IsCreateKey(this, false))     //스파이럴 생성 조건
            {
                GenItem(ItemType.Key, ColorType.Rnd, 0, true);
            }
            else if (ItemMgr.IsCreateTimeBomb(this, false))
            {
                GenItem(ItemType.TimeBomb, ColorType.Rnd, 0, true);
            }
            else
            {
                GenItem(ItemType.Normal, ColorType.Rnd, 0, true);
            }
            top.GetPanel(PanelType.Creator_Key_TimeBomb).AddAction();
            ItemDropAnim(true, false);
            return true;
        }

        return false;
    }

    //사이드드랍을 해야할때. 특정 생성기는 아이템을 줄수 있도록 추가 한다.
    public bool CreatorSpawnItem_Side(Board side)
    {
        //if (m_IsWarpOutBoard) //현재 보드가 워프출구이면.
        //{
        //    if (top.IsNowItemDropForWarpIn)
        //    {
        //        WarpOutItemCreate(top);
        //        WarpInItemDestroy(top);
        //        return true;
        //    }
        //}
        //else 
        if (side.m_IsCreatorEmpty)
        {
            return TopSpawnItem();
        }

        return false;
    }

    public void WarpOutItemCreate(Board top)
    {
        GenItem(top.m_Item.m_ItemType, top.m_Item.m_Color, top.m_Item.m_Value, false, false, true);
        m_Item.m_ItemDrop.Copy(top.m_Item.m_ItemDrop);

        //워프 통과 횟수
        m_Item.m_WarpPassCount = top.m_Item.m_WarpPassCount + 1; //즉 m_WarpPassCount++;

        m_Item.transform.position = GetBoardPos() - GetWarpDropVector(); //new Vector3(0, MatchDefine.BoardHeight, 0.01f);

        Vector3 des = GetBoardPos() + new Vector3(0, 0, 0.01f);
        m_DropAnim = true;
        StartCoroutine(m_Item.DropAnim(des, false, false, this, () =>
        {
            m_MatchingCheck = true; //top에 생성되는 놈들도 포함해서 움직이는 블럭들은 Check..
            m_DropAnim = false;
            m_Item.transform.position = GetBoardPos();
        }, Drop_Dir));

        //m_DropAnim = true;
        //Tween tween = m_Item.transform.DOMoveY(GetBoardPos().y, 0.2f);//.SetSpeedBased();
        //tween.SetEase(Ease.Linear);
        //tween.OnComplete(() =>
        //{
        //    m_DropAnim = false;
        //    m_Item.transform.position = GetBoardPos();
        //});
    }

    public void WarpInItemDestroy(Board top)
    {
        Item _item = top.m_Item;
        top.m_Item = null;  //현재 보드의 아이템은 비움.

        _item.transform.position = top.GetBoardPos() + new Vector3(0, 0, 0.01f);
        Vector3 des = top.GetBoardPos() + top.GetWarpDropVector(); //new Vector3(0, -MatchDefine.BoardHeight, 0.01f);
        StartCoroutine(_item.DropAnim(des, false, false,  null, () =>
        {
            _item.Destroy();
        }, Drop_Dir));

        //Tween tween = _item.transform.DOMoveY(top.GetBoardPos().y - MatchDefine.BoardHeight, 0.2f);//.SetSpeedBased();
        //tween.SetEase(Ease.Linear);
        //tween.OnComplete(() =>
        //{
        //   _item.Destroy();
        //});
    }

    Board _drop = null;
    bool IsCrtItemExist = false;
    bool IsTopDropPossible = true;
    Board Side = null;

    public bool DropItemRow(Board pb)
    {
        //*****************
        //Top의 마지막 체크
        if (IsPanelFixed)
            return false;
        if (m_IsWarpInBoard && pb != m_WarpOutBoard)
            return false;
        //******************

        _drop = GetTopBoard();

        IsCrtItemExist = false;
        IsTopDropPossible = false;

        if (IsNowItemEmpty == false)
        {
            if (_drop != null)
                _drop.DropItemRow(this);

            IsCrtItemExist = true;
        }
        else
        {
            if (_drop == null)
            {
                //아이템생성..     
                IsCrtItemExist = TopSpawnItem();
            }
            else
            {              
                if(CreatorSpawnItem(_drop)) //Warp & 생성기
                {
                    IsCrtItemExist = true;
                }
                else if (_drop.IsNowItemDrop) //top에서 가져오기 
                {
                    IsCrtItemExist = true;
                    ItemDrop(_drop, false);
                }

                //top가 재귀함수호출
                IsTopDropPossible = _drop.DropItemRow(this);

                if (IsTopDropPossible == false && IsItemExist == false)  //top은 내려줄수 없는 상황이고 아이템은 없으면
                {
                    //side에서 가져오기
                    //Side는 지금 당장 드랍될아이템이 없으면 못하는걸로 가정한다.
                    //top방향은 위에 상황을 보고 기달려줄수 있지만 side는 상황을 알수 없다.
                    //(side에 상황을 알려면 side블럭의 top,side를 체크해야하는데..복잡해진다..)
                    Side = SideDrop(this);
                    if (Side != null)
                    {
                        if (Side.CheckLastDrop()) //즉, 좌우 라인이 채워지고 난 다음에..(로직순서상 우측라인보다 사이드가 먼저 채워지는 문제 해결)
                        {
                            IsCrtItemExist = true;
                            ItemDrop(Side, true);

                            //top가 재귀함수호출
                            //Side.DropItemRow(false);
                        }
                    }
                    else if (Drop != null && Drop.GetPanel(PanelType.Default_Empty) != null && m_IsWarpInBoard == false)
                    { 
                        if (TopLeft != null && CreatorSpawnItem_Side(TopLeft)) //생성기
                        {
                            IsCrtItemExist = true;
                        }
                        else if (TopRight != null && CreatorSpawnItem_Side(TopRight)) //생성기
                        {
                            IsCrtItemExist = true;
                        }
                    }
                }
            }
        }

        //1. 현재 아이템존재 , 2. Top에서 아이템드랍가능 3. 현재 보드 드랍 조건.
        return (IsCrtItemExist | IsTopDropPossible) & IsCondItemDrop;
    }

    bool IsDropPossibleAll = true;
    int count = 0;
    public bool GravityDropItemRow(Board pb)
    {
        //*****************
        //Top의 마지막 체크
        if (IsPanelFixed)
            return false;
        if (pb != null && pb.Drop != null &&
            pb.Drop.m_DefaultPanel == null && pb.Drop_Dir != pb.Drop.Drop_Dir) // m_DefaultPanel인 곳에서 서로 상반대되는 중력이 있을 시 무한 루프에 빠지는 경우가 있어서 다음과 같이 처리
            return false;
        //if (m_IsWarpInBoard && pb != m_WarpOutBoard)
        //    return false;

        if (PossibleDrop_Dirs.Count > 1)
        {
            if (IsNowItemEmpty)
                ChangeDropDir();
            else
                return false;
        }
        //******************

        _drop = GetDropBoard();

        IsCrtItemExist = false;
        IsTopDropPossible = false;

        if (IsNowItemEmpty == false)
        {
            if (_drop != null && _drop.PossibleDrop_Dirs.Count == 1)
                _drop.GravityDropItemRow(this);

            IsCrtItemExist = true;
        }
        else
        {
            if (_drop == null)
            {
                //아이템생성..     
                IsCrtItemExist = TopSpawnItem();
            }
            else
            {
                if (CreatorSpawnItem(_drop)) //Warp & 생성기
                {
                    IsCrtItemExist = true;
                }
                else if (_drop.IsNowItemDrop) //top에서 가져오기 
                {

                    IsCrtItemExist = true;
                    ItemDrop(_drop, false);
                }

                if (_drop != null && _drop.PossibleDrop_Dirs.Count == 1)
                {
                    //top가 재귀함수호출
                    IsTopDropPossible = _drop.GravityDropItemRow(this);
                }
                else
                {
                    return (IsCrtItemExist | IsTopDropPossible) & IsCondItemDrop;
                }

                if (IsTopDropPossible == false && IsItemExist == false)  //top은 내려줄수 없는 상황이고 아이템은 없으면
                {
                    //side에서 가져오기
                    //Side는 지금 당장 드랍될아이템이 없으면 못하는걸로 가정한다.
                    //top방향은 위에 상황을 보고 기달려줄수 있지만 side는 상황을 알수 없다.
                    //(side에 상황을 알려면 side블럭의 top,side를 체크해야하는데..복잡해진다..)
                    Side = SideDrop(this);
                    if (Side != null)
                    {
                        if (Side.CheckLastDrop()) //즉, 좌우 라인이 채워지고 난 다음에..(로직순서상 우측라인보다 사이드가 먼저 채워지는 문제 해결)
                        {
                            IsCrtItemExist = true;
                            ItemDrop(Side, true);

                            //top가 재귀함수호출
                            //Side.DropItemRow(false);
                        }
                    }
                    else if (Drop != null && Drop.GetPanel(PanelType.Default_Empty) != null && m_IsWarpInBoard == false)
                    {
                        if (DropLeft != null && CreatorSpawnItem_Side(DropLeft)) //생성기
                        {
                            IsCrtItemExist = true;
                        }
                        else if (DropRight != null && CreatorSpawnItem_Side(DropRight)) //생성기
                        {
                            IsCrtItemExist = true;
                        }
                    }
                }
            }
        }

        //1. 현재 아이템존재 , 2. Top에서 아이템드랍가능 3. 현재 보드 드랍 조건.
        return (IsCrtItemExist | IsTopDropPossible) & IsCondItemDrop;
    }

    private bool IsCheckDropLine(Board side)
    {
        for (int i = 0; i < Drops.Count; i++)
        {
            if (Drops[i] != null)
            {
                for (int j = 0; j < Drops[i].Drops.Count; j++)
                {
                    if (Drops[i].Drops[j] == side)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    int ChangeDropDirCount = 0;
    public void ChangeDropDir(Board drop = null)
    {
        if (drop == null)
            drop = this;
        if (drop.PossibleDrop_Dirs.Count > 1)
        {
            drop.Drop_Dir = drop.PossibleDrop_Dirs[Random.Range(0, drop.PossibleDrop_Dirs.Count)];
            if (drop.Drop != null && !drop.Drop.IsNowItemDrop)
            {
                for (int i = 0; i < drop.PossibleDrop_Dirs.Count; i++)
                {
                    Board drop_temp = drop[drop.PossibleDrop_Dirs[i]];
                    if (drop_temp != null && drop_temp.IsNowItemDrop)
                    {
                        drop.Drop_Dir = drop.PossibleDrop_Dirs[i];
                    }
                }
            }
        }
    }


    public void ItemDrop(Board drop,bool side)
    {
        //보드쪽 세팅
        m_Item = drop.m_Item;
        drop.m_Item = null;

        //아이템쪽 세팅
        m_Item.m_Board = this;

        //드랍 애니메이션
        ItemDropAnim(false, side);
    }

    void ItemDropAnim(bool create, bool side)
    {
        m_DropAnim = true;

        StartCoroutine(m_Item.DropAnim(GetBoardPos(), create, side, this, () =>
        {
            ChangeDropDir();
            m_MatchingCheck = true; //top에 생성되는 놈들도 포함해서 움직이는 블럭들은 Check..
            m_DropAnim = false;
        }, Drop_Dir));
        //StartCoroutine(m_Item.DropAnim1(create, CheckLastDrop(), () =>
        // {
        //     //여기서 하는 이유
        //     //1. 이동중인놈들은 체크할 필요가 없기 때문에.. 최적화!!!
        //     //2. top도 내려오는 이동을 하기때문에 포함할수 있다.
        //     m_MatchingCheck = true;
        //     m_DropAnim = false;
        // }));
    }

    SQR_DIR Side_Dir = SQR_DIR.TOPLEFT;
    Board SideDrop(Board bd)
    {
        //좌우 순서만 바뀔뿐
        //무조건 하나는 넘겨준다.
        //둘다 안될때 null

        Board board_A = bd.DropLeft;
        Board board_B = bd.DropRight;

        //if (_swith)
        if (Side_Dir != SQR_DIR.TOPLEFT)
        {
            board_A = bd.DropRight;
            board_B = bd.DropLeft;
        }

        //1순위 현재 방향에서 문제가 없다.
        if (board_A != null)
        {
            if ((board_A.PossibleDrop_Dirs.Count == 1 && PossibleDrop_Dirs.Count == 1) &&
                board_A.Drop_Dir == Drop_Dir || IsCheckDropLine(board_A))
            {
                if (board_A.IsNowItemDrop)
                    return board_A;
            }
        }

        //2순위 방향이 바뀐다.
        if (board_B != null)
        {
            if (board_B.IsNowItemDrop)
            {
                if ((board_B.PossibleDrop_Dirs.Count == 1 && PossibleDrop_Dirs.Count == 1) &&
                    board_B.Drop_Dir == Drop_Dir || IsCheckDropLine(board_B))
                {
                    Side_Dir = Side_Dir == SQR_DIR.TOPLEFT ? SQR_DIR.TOPRIGHT : SQR_DIR.TOPLEFT;
                    return board_B;
                }
            }
        }

        return null;
    }

    public bool CheckLastDrop()
    {

        //여기 사이드드랍일때는 이미 체크하기때문에 의미없는데
        //아이템 드랍인데 터져버렸으면 끝난걸로 처리해줄려고 한건가??
        //하지만 터지는 중이라면 어짜피 드랍은 진행이 안될테고 Init()도 다음 생성때 호출될테고..
        //일단 막아보자..
        //if(m_ItemBrusting)
        //    return true;

        if (m_IsWarpInBoard)
        {
            return CheckLastDrop(m_WarpOutBoard);
        }


        List<Board> _dropReferences = DropReference();
        bool ret = true;

        foreach (var board in _dropReferences)
        {
            ret &= CheckLastDrop(board);
        }

        return ret;
    }

    private bool CheckLastDrop(Board _dropReference, Board _firstReference = null)
    {
        bool ret = true;

        if (_firstReference == null) _firstReference = _dropReference;

        if (_dropReference == null)
            return true;

        //if(bottom.IsCondItemDrop == false && bottom.m_IsWarpInBoard == false) //즉, 드랍이 되거나 워프인이면 마지막이 아니다.!
        //    break;

        if (_dropReference.GetPanel(PanelType.Default_Empty) == null)
        {
            if (_dropReference.IsCondItemDrop == false || _dropReference.m_ItemBrusting)
                return true;
        }


        if (_dropReference.IsNowItemEmpty)
        {
            return false;
        }

        if (_dropReference.m_IsWarpInBoard)
        {
            return _dropReference.CheckLastDrop();
        }

        foreach (var board in _dropReference.DropReference())
        {
            if (_firstReference == _dropReference) return true;
            ret &= CheckLastDrop(board);
        }

        return ret;
    }
    #endregion

    #region Brust
    //******************************************************************************************************
    //구현&이펙트딜레이: 이펙트대기, 순차폭발등을 코루틴에서 구현딜레이로 구현한다. (라인아이템..) 
    //                   레인보우로 인한 폭발이나 보너스타임일때 Line이 동시에 터지는데 Brust딜레이로 잡고 있으면 뒤에 터진 Line은 앞에 블럭이 터지지 않는 문제 발생.
    //Destroy 딜레이   : 위 아이템이 바로 떨어지지 않도록 실제로 아이템은 보이지않는 상태로 기다린다.
    //                   DestoryTime 값을 사용하고 아이템별로 고정되어 있다.
    //********************************************************************************************************

    //딜레이 구분
    //1. 범위로 폭발이면 보드가 기준 
    //2. 직접아이템 지목 폭발이면 아이템이 기준
    public void Brust(bool rangebrust, float delay, bool sound = true, bool comboscore = true) //sound, comboscore은 Normalitem만 의미 있음.
    {
        //아이템 Brust에 이슈!
        //1. Brust대기중인 아이템은 다음칸으로 드랍하지 않음.(Unstable)
        //2. Brust는 딜레이중이여도 더 빠른 Brust가 오면 터져야함.
        //3. 하지만 레인보우아이템으로 터질때처럼 아이템이 정해져있는경우도 있음.

        //해당 위치가 터져야 하는경우는 rangebrust가 true
        //특정 아이템이 터져야 하는경우는 rangebrust가 flase
        if (rangebrust && delay > 0F)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(delay);
            seq.AppendCallback(() =>
            {
                StartCoroutine(Co_Brust(0, sound, comboscore));
            });
        }
        else {
            StartCoroutine(Co_Brust(delay, sound, comboscore));
        }
    }

    IEnumerator Co_Brust(float delay, bool sound, bool comboscore)
    {
        //아이템 삭제 & 이펙트

        //if (m_Brusting) //Brust가 중복으로 걸리지 않도록.
        //    yield break;

        //m_Brusting = true;

        if (IsNowItemBrust)// && m_ItemBrusting == false)
        {
            m_ItemBrusting = true;

            m_ItemScoreDelay = true;
            ShowSore_Init(this, m_Item.m_Color); //m_ItemScoreOK 보다 다음에 호출해야 한다.

            //터질 아이템을 잡고 있어야 한다. 이동도 못하고 다른 Brust가 치고 들어올수 없다.
            //ex)레인보우가 이펙트 하는 동안
            if (delay > 0)
                yield return new WaitForSeconds(delay);

            m_Item.SetBrustSound(sound);
            m_Item.Brust(() =>
            {         
                m_BrustScore += m_Item.GetDestoryScore(comboscore);  //아이템 점수
                m_ItemScoreDelay = false;

                PanelBrust();

                StartCoroutine(Co_ItemBrustComplete());
            });   
        }
        else
        {
            if (delay > 0)
                yield return new WaitForSeconds(delay);

            PanelBrust();
        }

        //m_Brusting = false;
    }

    //ItemBrust는 터질순간에만 호출해야 되지만(딜레이를 외부에서 관리)
    //ItemCombineBrust는 딜레이가 효과를 적용하지 않는 블럭처리를 위해서 존재하므로 내부에서 관리.
    public void CombineBrust(ItemType combinetype)
    {
        //CombineBrust일때 효과를 주지 않을 블럭은 ItemType이 None으로 준다.
        StartCoroutine(Co_CombineBrust(combinetype));     
    }

    IEnumerator Co_CombineBrust(ItemType combinetype)
    {
        if (m_ItemBrusting) //Brust가 중복으로 걸리지 않도록.
            yield break;

        m_ItemBrusting = true;

        m_ItemScoreDelay = true;
        ShowSore_Init(this, m_Item.m_Color); //m_ItemBrusting 보다 다음에 호출해야 한다.

        //if (IsNowItemBrust) //애초에 터지지 않는 아이템이 들어올리 없다.
        m_Item.CombineBrust(combinetype, () =>
        {
            m_BrustScore += m_Item.GetDestoryScore(false);  //아이템 점수
            m_ItemScoreDelay = false;

            PanelBrust();
            StartCoroutine(Co_ItemBrustComplete());
        });
    }

    IEnumerator Co_ItemBrustComplete()
    {
        //주변터질수있고(콤바인이아니고) 현재 터지는놈이 ArroundBrust가 아닐때
        if (m_Item.m_BrustArround && IsItemArountBrust == false)
            AroundBrust();

        if (m_Item.m_ItemType == ItemType.Mystery && m_NextItemType == ItemType.None)
        {
            //Bread,Ice같은 패널로 바뀌면 옆아이템으로 인해 바로 터진다. 
            //그래서 m_ItemBrusting를 false로 잠시 동안 유지한다.
            yield return new WaitForSeconds(0.2f);

            MysteryChange(m_Item.m_Color);
            m_MatchingCheck = true;
        }
        else
        {
            //아이템 Destory코루틴
            m_Item.SetVisible(false);
            var copied_color = m_Item.m_Color;
            yield return new WaitForSeconds(m_Item.ItemDestoryTime);


            if(m_Item != null)
            {
                m_Item.MissionApply();
            }
            //스폐셜 아이템 생성
            switch (m_NextItemType)
            {
                case ItemType.None:
                    ItemDestroy();
                    break;
                case ItemType.Normal:
                case ItemType.Butterfly:
                case ItemType.Line_X:
                case ItemType.Line_Y:
                case ItemType.Bomb:
                case ItemType.Line_C:
                    GenItem(m_NextItemType, copied_color, 0, false, true);                  
                    m_MatchingCheck = true;
                    break;
                case ItemType.Rainbow:
                    GenItem(m_NextItemType, ColorType.None, 0, false, true);
                    break;
                default:
                    break;
            }
            m_NextItemType = ItemType.None;
        }

        if (MatchMgr.m_StepType != StepType.Matching)
            MatchMgr.SetStep(StepType.Matching);

        m_ItemBrusting = false;
    }

    #region 아이템 Take
    //***************************************************************
    //요리가 미션타이밍과 아이템만 획득하는 부분이 달라서 추가 됐다. 
    //공용으로 추후에 다른 아이템에도 사용가능하도록 명칭을 정했다.
    //쉽게 말해서 Brust2라고 보면된다.
    //***************************************************************
    public void Take()
    {
        if(m_ItemBrusting)
            return;

        m_ItemBrusting = true;

        m_Item.Take(() =>
        {
            StartCoroutine(Co_TakeComplete());
        });

    }
    IEnumerator Co_TakeComplete()
    {
        m_Item.SetVisible(false);
        m_Item.MissionApply();

        //점수 출력
        MatchMgr.ScoreTextShow(m_Item.GetDestoryScore(false), this, m_Item.m_Color); //Co_Take 아이템

        yield return new WaitForSeconds(m_Item.ItemDestoryTime);

        ItemDestroy();

        /*사용하게 되면 생기면 주석해제..
        //스폐셜 아이템 생성
        switch (m_NextItemType)
        {
            case ItemType.None:
                ItemDestroy();
                break;
            case ItemType.Normal:
            case ItemType.Line_X:
            case ItemType.Line_Y:
            case ItemType.Bomb:
            case ItemType.Line_C:
                GenItem(m_NextItemType, m_Item.m_Color, 0, false, true);
                m_MatchingCheck = true;
                break;
            case ItemType.Rainbow:
                GenItem(m_NextItemType, ColorType.None, 0, false, true);
                break;
            default:
                break;
        }
        m_NextItemType = ItemType.None;
        */

        m_ItemBrusting = false;
    }
    #endregion

    List<Board> List_Around = new List<Board>();
    public void AroundBrust()
    {
        List_Around.Clear();

        Board abd = this[SQR_DIR.TOP];
        if (abd != null)
            List_Around.Add(abd);

        abd = this[SQR_DIR.BOTTOM];
        if (abd != null)
            List_Around.Add(abd);

        abd = this[SQR_DIR.LEFT];
        if (abd != null)
            List_Around.Add(abd);

        abd = this[SQR_DIR.RIGHT];
        if (abd != null)
            List_Around.Add(abd);


        for (int i = 0; i < List_Around.Count; i++)
        {
            abd = List_Around[i];

            //주변폭발일때는 아이템과 패널이 동시에 터지지 않는다. 
            //그래서 각각 처리 한다.
            //만약 아이템이 케이지에 막혔다면 IsNowItemBrust가 되고 패널만 터질것이다.
            if (abd.IsNowItemBrust && abd.m_ItemBrusting == false && abd.IsItemArountBrust)
            {
                abd.m_ItemBrusting = true;

                abd.m_ItemScoreDelay = true;
                abd.ShowSore_Init(abd, abd.m_Item.m_Color); //m_ItemScoreDelay 보다 다음에 호출해야 한다.

                abd.m_Item.Brust(() =>
                {
                    abd.m_BrustScore += abd.m_Item.GetDestoryScore(false);  //아이템 점수
                    abd.m_ItemScoreDelay = false;

                    StartCoroutine(abd.Co_ItemBrustComplete());
                });
            }
            else
            {
                if (abd.IsPanelArountBrust)
                {
                    abd.PanelBrust(this);
                }
            }
        }
    }
    #endregion

    #region 점수표시
    bool ScoreShowing = false;
    public int m_BrustScore = 0;
    public bool m_ItemScoreDelay = false;
    public bool m_PanelScoreDelay = false;

    public void ShowSore_Init(Board db, ColorType Color = ColorType.None)
    {
        //아이템에서 시작일때 패널에서 시작일지 모르기 때문에 
        if (ScoreShowing == false)
            StartCoroutine(Co_ShowScore(db, Color));
    }

    IEnumerator Co_ShowScore(Board db, ColorType color)
    {
        ScoreShowing = true;
        while (m_ItemScoreDelay || m_PanelScoreDelay)
        {
            yield return null;
        }

        //Score Show!!
        if (m_BrustScore > 0)
            MatchMgr.ScoreTextShow(m_BrustScore, db, color);       

        m_BrustScore = 0; //먼가 막 연결되서 터져서 설사 보여줄 시간이 없으면 누적된 값을 보여주고 초기화. 즉 초기화는 여기서만,..
        ScoreShowing = false;
    }
    #endregion

    #region ItemDestroy
    public void ItemDestroy()
    {
        //Destroy(m_Item.gameObject);
        if (m_Item != null)
        {
            m_Item.Destroy();

            m_Item = null;
        }

        //**중요** 아이템이 없어졌는데 행동중인걸로 체크되서 문제가 된다.
        //아이템관련 초기화 변수!!
        m_DropAnim = false;
    }
    #endregion

    #region 패널 Brust
    public void PanelBrust(Board arroundRoot = null) //null이 아니면 주변폭발
    {
        if (m_PanelBrusting)
            return;

        if (m_ListPanel.Count <= 0)
            return;

        //int index = m_ListPanel.Count - 1;
        //패널이 가장 아래부터 순서대로 저장되어 있다.
        //그래서 역순으로 하나가 터지면 리턴..
        for (int i = m_ListPanel.Count - 1; i >= 0; i--)
        {
            //if (arroundRoot != null && m_ListPanel[i].ArountBrust == false) //주변Brust일때 주변터짐이 아닌패널은 통과.
            //    continue;

            //if(m_ListPanel[i] is JamPanel)
            //    continue;

            //터질수 있고 겹수가 1이상 남아있을때
            if (m_ListPanel[i].PanelBrust && m_ListPanel[i].Defence > 0)
            {
                //주변Brust일때 주변터짐이 아닌패널이 맨 상단이면 안터진다.(롤리케이지..)
                if (arroundRoot != null && m_ListPanel[i].ArountBrust == false)
                    break;

                //아이템이 주변터짐일경우 아이템위치보다 아래는 터지지 않는다.
                //ex)와퍼 위에 도넛이나 스파이럴이 있을때.
                if ((m_ListPanel[i].m_PanelType > 0 && IsItemArountBrust))
                    break;

                m_PanelBrusting = true;
                m_PanelBrusting_Ctn++;

                //아이템에서 먼저 호출했으면 함수 내부에서 리턴당하고 패널만터지는경우라면 여기서 코루틴 돌아간다.
                m_PanelScoreDelay = true;
                ShowSore_Init(this); //m_PanelScoreDelay 보다 다음에 호출해야 한다. 패널이라서 컬러는 기본값


                m_ListPanel[i].Brust(arroundRoot, (isbrust, panel) =>
                {
                    if(isbrust)m_BrustScore += panel.DestoryScore; //패널 점수
                    m_PanelScoreDelay = false;

                    StartCoroutine(Co_PanelBrustComplete(panel, isbrust, panel.Defence == 0 ? true : false));
                });

                break; //터지는 패널이 겹쳤을때는 하나만 터지고 리턴.. 단, 패널 순서에 주의!!
            }
            else if (m_ListPanel[i].PanelDestroy)
            {
                //케이크처럼 터질꺼 터지고 바닥이 남았을때 -1을 주면 여기로 온다.
                //와퍼는 케이크 바닥이 사라진다음에 터져야 하므로 케이크바닥이 있으면 break되어야 한다.
                //점수를 매번 줄수 없으므로 패널의 Brust함수내부에서만 처리할수 없다.
                break;
            }
        }
        return;
    }

    IEnumerator Co_PanelBrustComplete(Panel panel, bool isbrust, bool destroy)
    {
        if (isbrust) //패널이 터졌으면..
        {
            if (destroy)
                panel.SetVisible(false);

            yield return new WaitForSeconds(0.15f);
            m_PanelBrusting = false;

            yield return new WaitForSeconds(panel.PanelDestoryTime - 0.15f);

            panel.MissionApply();

            if (destroy)
                PanelDestroy(panel);

            m_PanelBrusting_Ctn--;
            m_MatchingCheck = true; //icecage같은 경우는 패널이 터지면 매칭체크가 필요하다!

            if (MatchMgr.m_StepType != StepType.Matching)
                MatchMgr.SetStep(StepType.Matching);
        }
        else //패널이 안터질수도 있다. 조건이 생겼다.(컬러잼크래커...)
        {
            m_PanelBrusting = false;
            m_PanelBrusting_Ctn--;
        }
    }


    public void PanelDestroy(PanelType type)
    {
        Panel pnl = m_ListPanel.Find(obj => obj.m_PanelType == type);
        
        if(pnl != null)
            ObjectPool.Instance.Restore(pnl.gameObject);

        m_ListPanel.Remove(pnl);
    }

    public void PanelDestroy(Panel panel)
    {
        if (panel != null)
            ObjectPool.Instance.Restore(panel.gameObject);

        m_ListPanel.Remove(panel);
    }

    public void PanelDestroy()
    {
        //디폴트 패널
        if (m_DefaultPanel != null)
        {
            ObjectPool.Instance.Restore(m_DefaultPanel.gameObject);
            m_DefaultPanel = null;
        }
         
        //조건 패널들.   
        for (int i = 0; i<m_ListPanel.Count; i++)
        {
            ObjectPool.Instance.Restore(m_ListPanel[i].gameObject);
        }

        m_ListPanel.Clear();
    }
    #endregion

    #region 추가 패널관리 초기화
    static public void AddMgrVarClear()
    {
        //***************************************
        //m_ListBoard에 있기때문에 List만 초기화.
        //***************************************

        //아이템


        //패널 
        m_ListMagicColorBoard.Clear();
        m_ListBottlePanel.Clear();  
        m_ListRingPanel.Clear();
    }
    #endregion

    #region 특수패널 Destroy
    static public void SpecialPanelDestory()
    {
        m_WarpCount = 0;

        for (int i = 0; i < m_ListWarpPanel.Count; i++)
        {
            if (m_ListWarpPanel[i] != null)
            {
                if (m_ListWarpPanel[i].gameObject != null)
                    ObjectPool.Instance.Restore(m_ListWarpPanel[i].gameObject);
            }
        }

        m_ListWarpPanel.Clear();


        for (int i = 0; i < m_ListFoodArvPanel.Count; i++)
        {
            if (m_ListFoodArvPanel[i] != null)
            {
                if (m_ListFoodArvPanel[i].gameObject != null)
                    ObjectPool.Instance.Restore(m_ListFoodArvPanel[i].gameObject);
            }
        }

        m_ListFoodArvPanel.Clear();

        for (int i = 0; i < m_ListBearStartPanel.Count; i++)
        {
            if (m_ListBearStartPanel[i] != null)
            {
                if (m_ListBearStartPanel[i].gameObject != null)
                    ObjectPool.Instance.Restore(m_ListBearStartPanel[i].gameObject);
            }
        }
        m_ListBearStartPanel.Clear();

        for (int i = 0; i < m_ListConveyerBeltPanel.Count; i++)
        {
            if (m_ListConveyerBeltPanel[i] != null)
            {
                if (m_ListConveyerBeltPanel[i].gameObject != null)
                    ObjectPool.Instance.Restore(m_ListConveyerBeltPanel[i].gameObject);
            }
        }
        m_ListConveyerBeltPanel.Clear();
    }
    #endregion
}
