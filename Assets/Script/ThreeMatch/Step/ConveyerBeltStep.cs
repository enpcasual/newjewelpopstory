﻿using UnityEngine;
using System.Collections;

public class ConveyerBeltStep : BaseStep {

    int MoveCnt;
    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (Board.m_ListConveyerBeltPanel.Count == 0)
        {
            MatchMgr.SetStep(StepType.Chameleon);       //다음 스텝으로 넘어가야한다.
            return;
        }

        for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
        {
            //컨베이어벨트가 작동하기 전에 초기작업.
            Board.m_ListConveyerBeltPanel[i].ConveyerBeltInItSetting();
        }

        MoveCnt = 0;
        for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
        {
            MoveCnt++;
            Board.m_ListConveyerBeltPanel[i].ConveyerBeltMove(()=>
            {
                MoveCnt--;
            });
        }
    }

    public override void Step_Process()
    {
        base.Step_Process();
        if (MoveCnt <= 0)
        {
            for (int i = 0, len = Board.m_ListConveyerBeltPanel.Count; i < len; i++)
            {
                //컨베이어벨트가 작동하기 전에 초기작업.
                Board.m_ListConveyerBeltPanel[i].ConveyerBeltStop();
            }

            MatchMgr.SetStep(StepType.Chameleon);
            return;
        }
    }
}
