﻿using UnityEngine;
using System.Collections;

public class TimeBombStep : BaseStep {

    Board board;

    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        //MatchingStep바로다음호출
        if (SpecialItmeStep_Before())
            return;

        if (TimeBombItem.m_Count <= 0)
        {
            MatchMgr.SetStep(StepType.IceCream);    //다음 스텝으로 넘어가야한다.
            return;
        }


        for (int i = 0; i < MatchMgr.m_ListBoard.Count; i++)
        {
            board = MatchMgr.m_ListBoard[i];

            if (board.IsItemExist == false) continue;
            if ((board.m_Item is TimeBombItem) == false) continue;

            if (((TimeBombItem)board.m_Item).m_TimeBombCnt <= 0)
            {
                MissionManager.Instance.IsTimeBombOver = true;
                MissionManager.Instance.TimeBombOver = board.m_Item;
                break;
            }
        }

        MatchMgr.SetStep(StepType.IceCream);    //다음 스텝으로 넘어가야한다.
    }

    public override void Step_Process()
    {
        base.Step_Process();
    }
}
