﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ChameleonStep : BaseStep {

    Board board;

    int AnimCnt = 0;

    public override void Step_Init()
    {
        base.Step_Init();

        AnimCnt = 0;
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (ChameleonItem.m_Count <= 0)
        {
            MatchMgr.SetStep(StepType.MagicColor);    //다음 스텝으로 넘어가야한다.
            return;
        }

        SoundManager.Instance.PlayEffect("chameleon_changed");

        for (int i = 0; i < MatchMgr.m_ListBoard.Count; i++)
        {
            board = MatchMgr.m_ListBoard[i];

            if (board.IsItemExist == false) continue;
            if ((board.m_Item is ChameleonItem) == false) continue;

            AnimCnt++;
            ChameleonAbility(board, () =>
            {
                AnimCnt--;
            });

        }
    }

    public override void Step_Process()
    {
        base.Step_Process();

        if (AnimCnt <= 0)
        {
            MatchMgr.SetStep(StepType.MagicColor);
            return;
        }
    }

    void ChameleonAbility(Board board, System.Action Complete)
    {
        ChameleonItem board_Item = board.m_Item as ChameleonItem;

        Sequence seq = DOTween.Sequence();
        //seq.Append(board.m_Item.transform.DOScale(0, 0.2f).SetEase(Ease.Linear));
        board_Item.SetSprite(((int)board_Item.m_Color - 1) + 6);
        seq.Append(board_Item.m_SpriteRender.DOColor(new Color32(255, 255, 255, 255), 0.4f));
        seq.AppendCallback(() =>
        {
            board_Item.SetColorRandomOther();
            board_Item.ChangeImage();
        });
        seq.Append(board_Item.m_SpriteRender.DOColor(new Color32(255, 255, 255, 0), 0.2f));
        seq.AppendCallback(() =>
         {
             board_Item.SetSprite(((int)board_Item.m_Color - 1) + 6);
         });
        //seq.Insert(0.2f, board.m_Item.transform.DOScale(1, 0.2f).SetEase(Ease.Linear));
        seq.Append(board_Item.m_SpriteRender.DOColor(new Color32(255, 255, 255, 255), 0.2f));  
        seq.OnComplete(() =>
        {
            board_Item.SetSprite((int)board_Item.m_Color - 1);

            //여기서 처리 해야한다.
            board.m_MatchingCheck = true;

            Complete();
        });
    }
}
