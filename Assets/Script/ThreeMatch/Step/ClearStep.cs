﻿using UnityEngine;
using System.Collections;

public class ClearStep : BaseStep
{

    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (MatchMgr.m_MatchState == MatchState.Playing)
        {
            MatchMgr.SetMatchState(MatchState.BonusTime);
        }
    }

    public override void Step_Process()
    {
        base.Step_Process();     
    }
}
