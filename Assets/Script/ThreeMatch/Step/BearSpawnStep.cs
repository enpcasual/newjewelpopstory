﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BearSpawnStep : BaseStep {

    List<int> List_PanelIndex = new List<int>();

    int SpwanCnt = 0;

    //조건 체크용
    int Bear_Interval = 0;

    public override void Step_Init()
    {
        base.Step_Init();

        SpwanCnt = 0;
        Bear_Interval = 0;
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (Board.m_ListBearStartPanel.Count == 0)
        {
            SpecialItemStep_After();       //다음 스텝으로 넘어가야한다.
            return;
        }

        if (MatchMgr.m_CSD.isBearMission) 
        {
            //미션이 베어일때 미션 달성하면 더이상 나오지 않음.
            if (MissionMgr.GetMissionLeftCnt(MissionType.Bear, MissionKind.Bear) <= 0)
            {
                SpecialItemStep_After();       //다음 스텝으로 넘어가야한다.
                return;
            }

        }

        if (MatchMgr.m_CSD.Bear_MaxExist <= JellyBearItem.m_Count)
        {
            SpecialItemStep_After();       //다음 스텝으로 넘어가야한다.
            return;
        }

        Bear_Interval++;
        if (MatchMgr.m_CSD.Bear_Interval > Bear_Interval)
        {
            SpecialItemStep_After();      //다음 스텝으로 넘어가야한다.
            return;
        }
        Bear_Interval = 0;

        //젤리곰 출현
        List_PanelIndex.Clear();

        for (int i = 0; i < Board.m_ListBearStartPanel.Count; i++)
        {
            List_PanelIndex.Add(i);
        }

        int check_index = 0;
        while (true)
        {
            if(List_PanelIndex.Count == 0)
                break;

            check_index++;
            int index = Random.Range(0, List_PanelIndex.Count);

            Panel bearStartPanel = Board.m_ListBearStartPanel[List_PanelIndex[index]];
            List_PanelIndex.RemoveAt(index);

            Board bearStartBoard = bearStartPanel.m_Board;

            if (bearStartBoard.IsItemExist == false) continue;

            if (bearStartBoard.m_Item is JellyBearItem) continue;

            if (List_PanelIndex.Count > 0) //모든아이템이 스폐셜아이템이면 마지막아이템은 곰이 된다.
            {
                if (bearStartBoard.m_Item is LineXItem) continue;

                if (bearStartBoard.m_Item is LineYItem) continue;

                if (bearStartBoard.m_Item is LineCItem) continue;

                if (bearStartBoard.m_Item is BombItem) continue;

                if (bearStartBoard.m_Item is RainbowItem) continue;
            }

            bearStartBoard.GenItem(ItemType.JellyBear, ColorType.Rnd); //체인지

            SpwanCnt++;
            JellyBearSpawn(bearStartBoard, () =>
            {
                SpwanCnt--;
            });

            //SoundManager.Instance.PlayEffect("젤리곰이 생성 사운드 넣어주세요!!");
            break; //곰은 무조껀 한마리 나오면 끝!!
        }
    }

    public override void Step_Process()
    {
        base.Step_Process();

        if (SpwanCnt <= 0)
        {
            SpecialItemStep_After();
            return;
        }
    }

    void JellyBearSpawn(Board board, System.Action Complete)
    {
        //사운드
        SoundManager.Instance.PlayEffect("bear_jump");

        board.m_Item.transform.localScale = Vector3.zero;
        board.m_Item.transform.DOScale(1f, 0.5F)
        .SetEase(Ease.OutBack)
        .OnComplete(() =>
        {
            board.m_MatchingCheck = true;

            Complete();
        });
    }

}
