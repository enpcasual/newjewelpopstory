﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class IceCreamStep : BaseStep {

    int IceCream_Interval = 0;
    int IceCreamCreator_Interval = 0;

    public override void Step_Init()
    {
        base.Step_Init();

        IceCream_Interval = 0;
        IceCreamCreator_Interval = 0;
        IceCreamPanel.m_IceCreamBrust = false;
    }

    public override void Step_Play()
    {
        base.Step_Play();
   

        if(IceCreamPanel.m_Count <= 0 && IceCreamCreatorPanel.m_Count <= 0)
        {
            MatchMgr.SetStep(StepType.ConveyerBelt);    //다음 스텝으로 넘어가야한다.
            return;
        }

        if (IceCreamPanel.m_IceCreamBrust)
        {
            //아이스크림 터트렸으면 인터벌 초기화.
            IceCream_Interval = 0;
            IceCreamCreator_Interval = 0;

            IceCreamPanel.m_IceCreamBrust = false;
            MatchMgr.SetStep(StepType.ConveyerBelt);    //다음 스텝으로 넘어가야한다.
            return;
        }

        if (IceCreamPanel.m_Count > 0)
            IceCream_Interval++;

        if (IceCreamCreatorPanel.m_Count > 0)
            IceCreamCreator_Interval++;


        //둘다 조건 충족이 안됐을때는 증식 안함.
        if (MatchMgr.m_CSD.IceCream_Interval > IceCream_Interval
            && MatchMgr.m_CSD.IceCreamCreator_Interval > IceCreamCreator_Interval)
        {
            MatchMgr.SetStep(StepType.ConveyerBelt);       //다음 스텝으로 넘어가야한다.
            return;
        }
        IceCream_Interval = 0;
        IceCreamCreator_Interval = 0;

        List<Panel> li_IceCream = new List<Panel>();
        for (int i = 0; i < MatchMgr.m_ListBoard.Count; i++)
        {
            for (int k = 0; k < MatchMgr.m_ListBoard[i].m_ListPanel.Count; k++)
            {
                if (MatchMgr.m_ListBoard[i].m_ListPanel[k] is IceCreamPanel ||
                     MatchMgr.m_ListBoard[i].m_ListPanel[k] is IceCreamCreatorPanel)
                {
                    if (MatchMgr.m_ListBoard[i].IsPanelCage) 
                        continue;
                    li_IceCream.Add(MatchMgr.m_ListBoard[i].m_ListPanel[k]);
                }
            }
        }

        List<Board> li_IceCream_Neighbor = new List<Board>();
        List<int> li_index = new List<int>();
        for (int i = 0; i < li_IceCream.Count; i++)
        {
            List<Board> li_bd = GetCopyPossibleNeighbor(li_IceCream[i].m_Board);

            li_IceCream_Neighbor.AddRange(li_bd);

            for (int k = 0; k < li_bd.Count; k++)//패널의 정보를 가져오기 위해 인덱스를 알아야한다.
                li_index.Add(i);
        }
        if(li_IceCream_Neighbor.Count > 0)
        {
            int ran_index = Random.Range(0, li_IceCream_Neighbor.Count);
            int defence = li_IceCream[li_index[ran_index]].Defence;
            if(li_IceCream[li_index[ran_index]] is IceCreamCreatorPanel) //아이스크림 생성기일때는 방어력 따로 준다.
                defence = 1;    //일단 생성기는 1짜리 아이스크림만 만든다.

            li_IceCream_Neighbor[ran_index].ItemDestroy();
            li_IceCream_Neighbor[ran_index].GenPanel(PanelType.IceCream_Block, defence, 0, null);

            if(defence <= 0)
                Debug.LogError("이게머지? defence가 0인 아이스크림을 복사???");

            Panel new_icecream = li_IceCream_Neighbor[ran_index].GetPanel(PanelType.IceCream_Block);
            new_icecream.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            new_icecream.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack);

            MissionManager.Instance.MissionPlusCount(MissionType.IceCream, MissionKind.IceCream, defence);

            SoundManager.Instance.PlayEffect("icecream_grows");
        }

        MatchMgr.SetStep(StepType.ConveyerBelt);
    }

    public override void Step_Process()
    {
        base.Step_Process();
    }

    public List<Board> GetCopyPossibleNeighbor(Board board)
    {
        List<Board> li_board = new List<Board>();

        if (isCopyPossible(board[SQR_DIR.TOP])) li_board.Add(board[SQR_DIR.TOP]);

        if (isCopyPossible(board[SQR_DIR.BOTTOM])) li_board.Add(board[SQR_DIR.BOTTOM]);

        if (isCopyPossible(board[SQR_DIR.LEFT])) li_board.Add(board[SQR_DIR.LEFT]);

        if (isCopyPossible(board[SQR_DIR.RIGHT])) li_board.Add(board[SQR_DIR.RIGHT]);

        return li_board;
    }


    bool isCopyPossible(Board board)
    {
        if (board == null) return false;

        //아이템이 존재하고 케이지로 덮혀있지 않으면
        //1. 아이템 존재
        //2. 브레드, 아이스크림생성기, 아이스크림 아님
        //3. 롤리케이지, 아이스케이지 아님
        //4. 노말 아이템(도넛, 스파이럴, 스폐셜 아이템일때 리턴)
        //5. 절대 터지지 않는 패널 위로는 못감.(매직칼라)
        //6. 특수패널중에 예외처리(컨베이어벨트)
        if (board.IsNowItemBrust == false) return false; //1,2,3

        if ((board.m_Item is NormalItem) == false) return false; //4

        if (board.IsPanelDestory == false) return false; //5

        if (board.m_IsConveyerBeltBoard) return false; //6
        return true;
    }
}
