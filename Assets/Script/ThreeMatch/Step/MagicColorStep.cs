﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MagicColorStep : BaseStep {

    int AnimCnt = 0;

    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (Board.m_ListMagicColorBoard.Count <= 0)
        {
            MatchMgr.SetStep(StepType.BearJump);    //다음 스텝으로 넘어가야한다.
            return;
        }

        for (int i = 0; i < Board.m_ListMagicColorBoard.Count; i++)
        {
            if (Board.m_ListMagicColorBoard[i].IsItemExist == false) continue;
            if (Board.m_ListMagicColorBoard[i].m_Item.m_Color == ColorType.None) continue;

            AnimCnt++;
            MagicColorAbility(Board.m_ListMagicColorBoard[i], () =>
            {
                AnimCnt--;
            });
            continue;
        }
    }

    public override void Step_Process()
    {
        base.Step_Process();

        if (AnimCnt <= 0)
        {
            MatchMgr.SetStep(StepType.BearJump);
            return;
        }
    }

    void MagicColorAbility(Board board, System.Action Complete)
    {
        Item board_Item = board.m_Item;

        //이펙트_아이템변경시점에서 출력하면 늦다. 이펙트자체에 딜레이가 0.2초 있다.
        EffectManager.Instance.MagicColorEffect(board.transform.position);

        Sequence seq = DOTween.Sequence();
        seq.Append(board.m_Item.transform.DOScale(0, 0.3f).SetEase(Ease.Linear));
        seq.Insert(0, board.m_Item.transform.DOLocalRotate(new Vector3(0,0,-360), 0.3f, RotateMode.FastBeyond360).SetEase(Ease.Linear));
        seq.AppendCallback(() =>
        {
            //사운드
            SoundManager.Instance.PlayEffect("mcolor");

            board.m_Item.SetColorRandomOther();
        });
        seq.Append(board.m_Item.transform.DOScale(1, 0.3f).SetEase(Ease.Linear));
        seq.Insert(0.3f, board.m_Item.transform.DOLocalRotate(new Vector3(0, 0, 360), 0.3f, RotateMode.FastBeyond360).SetEase(Ease.Linear));
        seq.OnComplete(() =>
        {
            board.m_Item.transform.localScale = Vector3.one;
            board.m_Item.transform.localEulerAngles = Vector3.zero;

            //여기서 처리 해야한다.
            board.m_MatchingCheck = true;

            Complete();
        });
    }
}
