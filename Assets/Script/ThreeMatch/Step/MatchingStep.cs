﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class MatchingStep : BaseStep
{

    static public bool MatchBrust = false;

    public override void Step_Init()
    {
        base.Step_Init();

        MatchBrust = false;
    }

    public override void Step_Play()
    {
        base.Step_Play();
    }

    bool AllEnd = true;
    public override void Step_Process()
    {
        base.Step_Process();

        if (MatchMgr.SpecialEffect)
            return;

        //요리 체크
        CheckFood();

        //드랍 체크
        Drop();

        //매칭 체크
        CheckMatchCondition(MatchMgr.m_ListBoard);

        //컬러 링(매칭,이동,폭발 모든것에 속하지 않는 블럭과 합체)
        CheckRing();

        AllEnd = true;

        for (int i = 0, len = MatchDefine.MaxX * MatchDefine.MaxY; i < len; i++)
        {
            if (MatchMgr.m_ListBoard[i].m_ItemBrusting || MatchMgr.m_ListBoard[i].m_PanelBrusting_Ctn > 0 || MatchMgr.m_ListBoard[i].m_DropAnim)
                AllEnd = false;
        }

        //MatchBrust = false;
        //CobineBrust = false;
        if (MatchBrust)
        {
            MatchMgr.ComboPlus(true);
            MatchBrust = false;

            MissionManager.Instance.SequenceInit();
        }

        if (AllEnd)
        {
            MatchMgr.ComboText();
            MatchMgr.SetStep(StepType.TimeBomb);

            for (int i = 0, len = MatchDefine.MaxX * MatchDefine.MaxY; i < len; i++)
            {
                if (MatchMgr.m_ListBoard[i].IsItemExist)
                    MatchMgr.m_ListBoard[i].m_Item.m_WarpPassCount = 0;
            }
        }
    }

    public void Drop()
    {
        for (int i = 0; i < MatchMgr.m_ListDropStart.Count; i++)
        {
            MatchMgr.m_ListDropStart[i].GravityDropItemRow(null);
        }
    }
    //public void Drop()
    //{
    //    for (int i = 0; i < MatchMgr.m_ListDropStart.Count; i++)
    //    {
    //        MatchMgr.m_ListDropStart[i].DropItemRow(null);        
    //    }
    //}

    //List<Board> arround = new List<Board>();
    bool IsJamBoard = false;
    List<Board> list_x = new List<Board>();
    List<Board> list_y = new List<Board>();
    public void CheckMatchCondition(List<Board> boards)
    {
        //arround.Clear();
        //여기서 조건 체크를 통해 변경되야할 아이템을 지정한다.
        for (int i = (int)ItemType.CondItem_Last; i >= (int)ItemType.Normal; i--)
        {
            for (int k = 0; k < boards.Count; k++)
            {
                //매칭체크는 true이고 드랍,폭발중이 아니어야 한다.
                if (boards[k].m_MatchingCheck && !boards[k].m_DropAnim && !boards[k].m_ItemBrusting && boards[k].IsNowItemMatch)
                {
                    if (i == (int)ItemType.Butterfly)
                    {
                        boards[k].FindMatchesSquare(ref list_x);
                    }
                    else
                    {
                        boards[k].FindMatchesHorizontal(ref list_x);
                        boards[k].FindMatchesVertical(ref list_y);
                    }

                    if (SpecailItemCondition(boards[k], (ItemType)i, list_x, list_y))
                    {
                        //boards[k].m_MatchingCheck = false;
                        //boards[k].m_isMatchBrust = true;

                        IsJamBoard = false;

                        //조건이 중족되면 애니메이션때문에 list_x에 전부 합쳐진다.
                        for (int v = 0; v < list_x.Count; v++)
                        {
                            list_x[v].m_MatchingCheck = false;
                            list_x[v].m_isMatchBrust = true;

                            //JamPanel 증식
                            if (list_x[v].m_IsJamBoard)
                                IsJamBoard = true;
                        }

                        if (IsJamBoard)
                        {
                            for (int v = 0; v < list_x.Count; v++)
                            {
                                if (list_x[v].m_IsJamBoard == false)
                                    list_x[v].JamPanelCreate(0);
                            }
                        }
                        //for (int j = 0; j < list_y.Count; j++)
                        //{
                        //    list_y[j].m_MatchingCheck = false;
                        //    list_y[j].m_isMatchBrust = true;
                        //}

                        MatchBrust = true;
                    }
                }
            }
        }

        //남은 체크는 여기에서 초기화.
        for (int k = 0; k < boards.Count; k++)
        {
            boards[k].m_MatchingCheck = false;
        }

        //모든 Brust에서 처리하도록 변경
        //MatchMgr.AroundBrust(arround);

        //매칭 된 블럭들 Brust + 연쇄 반응!!
        MatchMgr.MatchBrust(boards);
    }


    //스폐셜 아이템으로 바뀔수 있는지 조건 체크
    int var = 0;
    public bool SpecailItemCondition(Board bd, ItemType type, List<Board> list_x, List<Board> list_y)
    {
        bool result = false;

        //******************************************************************
        //순서가 역순으로 체크된다.
        //후순위 조건은 앞순위 조건보다 포괄적여도 문제 없음.
        //*******************************************************************
        switch (type)
        {
            case ItemType.Normal:
                {
                    if (list_x.Count == 2 || list_y.Count == 2)
                    {
                        bd.m_NextItemType = ItemType.None;
                        result = true;

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        list_x.AddRange(list_y);
                        specialEffect(null, list_x); //젤리몬일때만 애니메이션 주므로 기본 bd값은 null을 준다.
                    }
                }
                break;
            case ItemType.Butterfly:
                {
                    if (list_x.Count >= 3)
                    {
                        result = true;

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        //list_x.AddRange(list_y); //list_y를 lixt_x로 옮기는 이유는 초기화를 통일시켜서 list_x만 할려고
                        specialEffect(bd, list_x, () =>
                        {
                            //사운드
                            SoundManager.Instance.PlayEffect("4m");
                        });

                        //스폐셜 아이템 생성!
                        if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) ////젤리몬, 미스테리 등등..
                        {
                            for (int i = 0; i < list_x.Count; i++)
                            {
                                if (list_x[i].m_Item.m_Brust)
                                {
                                    list_x[i].m_NextItemType = ItemType.Butterfly;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            bd.m_NextItemType = ItemType.Butterfly;
                        }
                    }
                }
                break;
            case ItemType.Line_Y:
                {
                    if (list_y.Count >= 3)
                    {
                        result = true;

                        //사운드
                        SoundManager.Instance.PlayEffect("4m");

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        list_x.AddRange(list_y); //list_y를 lixt_x로 옮기는 이유는 초기화를 통일시켜서 list_x만 할려고
                        specialEffect(bd, list_x);

                        //스폐셜 아이템 생성!
                        if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) //젤리몬, 미스테리 등등..
                        {
                            for (int i = 0; i < list_x.Count; i++)
                            {
                                if (list_x[i].m_Item.m_Brust)
                                {
                                    list_x[i].m_NextItemType = ItemType.Line_X;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            bd.m_NextItemType = ItemType.Line_X;
                        }
                    }

                }
                break;
            case ItemType.Line_X:
                {
                    if (list_x.Count >= 3)
                    {
                        result = true;

                        //사운드
                        SoundManager.Instance.PlayEffect("4m");

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        list_x.AddRange(list_y);
                        specialEffect(bd, list_x);

                        //스폐셜 아이템 생성!
                        if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) //젤리몬, 미스테리 등등..
                        {
                            for (int i = 0; i < list_x.Count; i++)
                            {
                                if (list_x[i].m_Item.m_Brust)
                                {
                                    list_x[i].m_NextItemType = ItemType.Line_Y;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            bd.m_NextItemType = ItemType.Line_Y;
                        }
                    }
                }
                break;
            case ItemType.Line_C:
                {
                    //Bomb 조건에 충족이 안됐으면 Cross이다.    
                    if (list_x.Count >= 2 && list_y.Count >= 2)
                    {
                        result = true;

                        //사운드
                        SoundManager.Instance.PlayEffect("tm");

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        list_x.AddRange(list_y);
                        specialEffect(bd, list_x);

                        //스폐셜 아이템 생성!
                        if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) //젤리몬, 미스테리 등등..
                        {
                            for (int i = 0; i < list_x.Count; i++)
                            {
                                if (list_x[i].m_Item.m_Brust)
                                {
                                    list_x[i].m_NextItemType = ItemType.Line_C;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            bd.m_NextItemType = ItemType.Line_C;
                        }
                    }
                }
                break;
            case ItemType.Bomb:
                {
                    if (list_x.Count >= 2 && list_y.Count >= 2)
                    {
                        //Board _borad = list_x[0];

                        //좌:1,우:2,탑:4,바텀:8
                        var = 0;
                        for (int i = 0; i < list_x.Count; i++)
                        {
                            if (bd.X > list_x[i].X)
                                var = var | 1;
                            else
                                var = var | 2;
                        }

                        for (int i = 0; i < list_y.Count; i++)
                        {
                            if (bd.Y > list_y[i].Y)
                                var = var | 8;
                            else
                                var = var | 4;
                        }

                        #region 기존에 list_x, list_y가 현재보드를 포함하고 있을때 체크했던 방식
                        //list_x.Sort(delegate (Board a, Board b)
                        //{
                        //    return a.X.CompareTo(b.X);
                        //});
                        //list_y.Sort(delegate (Board a, Board b)
                        //{
                        //    return a.Y.CompareTo(b.Y);
                        //});

                        //ㄱ자 모양은 내가 움직인 아이템이 중간일테니 그 중간 아이템이 x축에서도 앞이나 뒤인 끝에 있고
                        //y축에서도 앞이나 뒤인 끝에 있어야 한다.
                        //if ((list_x.IndexOf(_borad) == 0 || list_x.IndexOf(_borad) == list_x.Count - 1)
                        //    && (list_y.IndexOf(_borad) == 0 || list_y.IndexOf(_borad) == list_y.Count - 1))
                        #endregion

                        if (var == 5 || var == 9 || var == 6 || var == 10)
                        {
                            result = true;

                            //사운드
                            SoundManager.Instance.PlayEffect("tm");

                            //애니메이션할 보드 세팅
                            list_x.Add(bd);
                            list_x.AddRange(list_y);
                            specialEffect(bd, list_x);

                            //스폐셜 아이템 생성!
                            if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) //젤리몬, 미스테리 등등..
                            {
                                for (int i = 0; i < list_x.Count; i++)
                                {
                                    if (list_x[i].m_Item.m_Brust)
                                    {
                                        list_x[i].m_NextItemType = ItemType.Bomb;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                bd.m_NextItemType = ItemType.Bomb;
                            }
                        }
                    }
                }
                break;
            case ItemType.Rainbow:
                {
                    if (list_x.Count >= 4 || list_y.Count >= 4)
                    {
                        result = true;

                        //사운드
                        SoundManager.Instance.PlayEffect("5m");

                        //애니메이션할 보드 세팅
                        list_x.Add(bd);
                        list_x.AddRange(list_y);
                        specialEffect(bd, list_x);

                        //스폐셜 아이템 생성!
                        if (bd.m_Item.m_Brust == false || bd.m_Item.HaveNextItem) //젤리몬, 미스테리 등등..
                        {
                            for (int i = 0; i < list_x.Count; i++)
                            {
                                if (list_x[i].m_Item.m_Brust)
                                {
                                    list_x[i].m_NextItemType = ItemType.Rainbow;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            bd.m_NextItemType = ItemType.Rainbow;
                        }
                    }
                }
                break;
            default:
                break;
        }

        return result;
    }


    //1. 모이는 연출은 별도의 오브젝트로 해야한다.
    //2. Brust딜레이를 별도로 줘야한다.
    bool m_IsJellyMon = false;
    JellyMon m_JellyMon = null;
    public void specialEffect(Board bd, List<Board> list_b, System.Action action = null)
    {
        //************
        //젤리몬 체크
        //************
        m_IsJellyMon = false;
        if (list_b.Count > 0)
        {
            for (int i = 0; i < list_b.Count; i++)
            {
                if (list_b[i].m_Item is JellyMon)
                {
                    m_JellyMon = (JellyMon)list_b[i].m_Item;

                    if (m_JellyMon.m_IsFull == false)  //젤리몬이 포화상태가 아닐때만 먹는다.
                    {
                        m_IsJellyMon = true;
                        break;
                    }

                    break;
                }
            }
        }


        if (m_IsJellyMon)
        {
            //젤리몬위치.
            Vector3 position = m_JellyMon.transform.position;
            Sequence seq = DOTween.Sequence();

            //젤리몬 애니메이션
            m_JellyMon.Animation_Eat();

            //다른 아이템 이동 애니메이션
            List<Item> list_anim = new List<Item>();
            for (int i = 0, len = list_b.Count; i < len; i++)
            {
                //스폐셜아이템이나 이동불가능한(케이지에갇힌)아이템들은 오지 이동하지 않도록! 예외처리.
                if (list_b[i].IsNowItemDrop && list_b[i].m_Item is JellyMon == false)//(list_b[i].m_Item is NormalItem || list_b[i].m_Item is JellyBearItem))
                {
                    //젤리몬 먹은 카운터 증가
                    m_JellyMon.EatCount();


                    list_b[i].m_Item.m_BrustEffect = false;
                    list_b[i].m_Item.m_BrustScore = false;
                    list_b[i].m_Item.m_BrustSound = false;
                    list_b[i].m_Item.ItemDestoryTime = 0.5f;

                    Item _item = list_b[i].CreateItem(list_b[i].m_Item.m_ItemType, list_b[i].m_Item.m_Color);
                    list_anim.Add(_item);
                    _item.transform.position += new Vector3(0, 0, -1f);
                    //seq = DOTween.Sequence();
                    seq.Insert(0f, _item.transform.DOScale(1.5f, 0.1f).SetEase(Ease.Linear));
                    seq.Insert(0.15f, _item.transform.DOMove(position, 0.25f).SetEase(Ease.Linear));
                    seq.Insert(0.15f, _item.transform.DOScale(0.1f, 0.2f).SetEase(Ease.Linear));
                }
            }
            seq.AppendInterval(0.15f);
            seq.OnComplete(() =>
            {
                for (int i = 0, len = list_anim.Count; i < len; i++)
                {
                    ObjectPool.Instance.Restore(list_anim[i].gameObject);
                }
                list_anim.Clear();
            });
        }
        else if (bd != null) //그냥 3개 매칭일때는 안탄다.
        {
            Vector3 position = bd.m_Item.transform.position + new Vector3(0, 0, -1f);
            Sequence seq = DOTween.Sequence();

            //아이템 이동 애니메이션
            List<Item> list_anim = new List<Item>();
            for (int i = 0, len = list_b.Count; i < len; i++)
            {
                //스폐셜아이템이나 이동불가능한(케이지에갇힌)아이템들은 오지 이동하지 않도록! 예외처리.
                if (list_b[i].IsNowItemDrop)
                {
                    if (list_b[i].m_Item.m_ItemType == ItemType.Normal
                        || list_b[i].m_Item.m_ItemType == ItemType.JellyBear
                        || list_b[i].m_Item.m_ItemType == ItemType.Chameleon)
                    {
                        list_b[i].m_Item.m_BrustEffect = false;
                        list_b[i].m_Item.m_BrustScore = false;
                        list_b[i].m_Item.m_BrustSound = false;
                        list_b[i].m_Item.ItemDestoryTime = 0.5f;

                        Item _item = list_b[i].CreateItem(list_b[i].m_Item.m_ItemType, list_b[i].m_Item.m_Color);
                        list_anim.Add(_item);
                        _item.transform.position += new Vector3(0, 0, -1f);
                        //seq = DOTween.Sequence();
                        seq.Insert(0f, _item.transform.DOScale(1.5f, 0.1f).SetEase(Ease.Linear));
                        seq.Insert(0.15f, _item.transform.DOMove(position, 0.15f).SetEase(Ease.Linear));
                        seq.Insert(0.15f, _item.transform.DOScale(1f, 0.05f).SetEase(Ease.Linear));
                    }
                }
            }
            seq.AppendInterval(0.1f);
            seq.AppendCallback(() =>
            {
                //사운드.. 각각 사운드가 달라서 Delegate로 처리
                if(action != null)
                    action();

                //별이펙트
                EffectManager.Instance.ItemChangeEffect(bd.transform.position);
            });
            seq.AppendInterval(0.1f);
            seq.OnComplete(() =>
            {
                ////별이펙트
                //ItemEffect effect = EffectManager.Instance.GetItemEffect();
                //effect.SpecialCreateEffect(bd.transform.position);

                //BMPool.Abandon(effectObj1);
                //foreach (Board _bd in list_b)
                for (int i = 0, len = list_anim.Count; i < len; i++)
                {
                    ObjectPool.Instance.Restore(list_anim[i].gameObject);
                }
                list_anim.Clear();
            });
        }
    }
}
