﻿using UnityEngine;
using System.Collections;

public class WaitStep : BaseStep
{

    bool focuscheck = false;
    float hinttime = 0;
    float GravityShowTime = 0;

    public override void Step_Init()
    {
        base.Step_Init();
        focuscheck = true;
    }

    public override void Step_Play()
    {
        base.Step_Play();

        if (MatchMgr.ShufflingCheck())
        {
            MatchMgr.SetStep(StepType.Shuffling);
        }

        //게임시작 후 처음대기상태에서 포커스아이템
        if (focuscheck)
        {
            focuscheck = false;
            MatchMgr.FocusShow();
            MatchMgr.ShowGravity();
        }
        else
        {
            GravityShowTime = 0f;
        }

        hinttime = 0;
    }

    public override void Step_Process()
    {
        base.Step_Process();

        //클리어나 실패 하면 ClearStep, FailStep으로 빠진다.
        //if (MatchMgr.m_MatchState == MatchState.BonusTime)
        //    return;
        //if (MatchMgr.m_MatchState == MatchState.GameClear)
        //    return;

        if (MatchMgr.m_IsFocus == false && MatchManager.Instance.isSwitching == false)
        {
            hinttime += Time.deltaTime;

            if (hinttime > 5)
            {
                hinttime = 0;

                MatchMgr.HintOffer();
            }
        }

        GravityShowTime += Time.deltaTime;
        if (GravityShowTime > 8f && !GravityDisplayer.useDefaultGravity)
        {
            GravityShowTime = 0f;
            MatchMgr.ShowGravity();
        }
    }
}
