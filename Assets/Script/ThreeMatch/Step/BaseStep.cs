﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum  StepType
{
    Wait = 0,   //대기상태
    Shuffling,  //셔플링

    //Drop,       //드랍상태  Matching과 통합됨.
    Matching,   //매칭상태

    TimeBomb,   //시간폭탄
    IceCream,   //아이스크림
    ConveyerBelt,//컨베이어벨트
    Chameleon,  //카멜레온
    MagicColor, //매직칼라
    BearJump,   //젤리베어 점프
    BearSpawn,  //젤리베어 생성
  
    //Check,      //음식같은아이템체크  수시로 체크가 필요해서 BaseStep으로 이동.

    Mission,    //미션 체크
    Clear,      //클리어
    Fail,       //실패

    
}

public class BaseStep {

    public MatchManager MatchMgr;
    public MissionManager MissionMgr;

    static public bool isItemStep = false;

    public BaseStep()
    {
        MatchMgr = MatchManager.Instance;
        MissionMgr = MissionManager.Instance;
    }


    public virtual void Step_Init()
    {

    }

    //단계가 바꼈을때 1회 호출
    public virtual void Step_Play()
    {

    }

    //단계가 유지되는 동안 계속 호출
    public virtual void Step_Process()
    {

    }

    #region 아이템 작동Step 관리.
    public bool SpecialItmeStep_Before()
    {
        ////아이템스텝을 하기전에 체크를 한번 한다!
        //if (CheckProcess())
        //    return true;

        //아이템들의 효과(이동)로 인해 다시 매칭Step으로 갔을때
        //다시 아이템의 효과가 발생하지 않도록 MissionStep으로 건너뛴다.
        if (isItemStep == false )
        {
            if(MissionMgr.GetMissionLeftCnt(MissionType.Bear, MissionKind.Bear) <= 0 && JellyBearItem.m_Count > 0)
            {
                var bear_jump_step = MatchMgr.m_DicStep[StepType.BearJump] as BearJumpStep;
                bear_jump_step.ReplaceBear();
            }

            MatchMgr.SetStep(StepType.Mission);

            return true;
        }

        return false;
    }

    public void SpecialItemStep_After()
    {
        ////아이템스텝을 한후에 체크를 다시 한다.
        //if (CheckProcess())
        //    return;

        MatchMgr.SetStep(StepType.Mission);
    }
    #endregion


    #region 음식 및 .. 미션 체크
    public void CheckFood()
    {
        //푸드 체크
        Board bd = null;
        for (int i = 0, len = Board.m_ListFoodArvPanel.Count; i < len; i++)
        {
            bd = Board.m_ListFoodArvPanel[i].m_Board;
            if (bd.IsItemExist && bd.m_Item is FoodItem)
            {
                if (bd.m_DropAnim == false && bd.m_ItemBrusting == false)
                {
                    bd.Take();
                }
            }
        }
    }
    #endregion

    #region 컬러 링
    public void CheckRing()
    {
        //푸드 체크
        //RingPanel ringpanel = null;
        //Board top = null;
        //for (int i = Board.m_ListRingPanel.Count -1 ; i >= 0; i--)
        //{
        //    ringpanel = Board.m_ListRingPanel[i];
        //    top = ringpanel.m_Board[SQR_DIR.TOP];
        //    if (top == null)
        //    {
        //        ringpanel.m_Board.m_IsRingBoard = false;
        //        Board.m_ListRingPanel.RemoveAt(i);
        //        Debug.LogError("Top보드가 없는 링은 동작에서 제외됩니다.");
        //        continue;
        //    }

        //    if(top.IsItemExist == false || top.m_ItemBrusting || top.m_DropAnim)
        //        continue;

        //    if (top.m_Item.m_ItemType == ItemType.Normal && ringpanel.m_Color == top.m_Item.m_Color)
        //    {
        //        //여기를 타는 순간 현재 아이템은 링패널이 있는 보드의 영향 아래에 놓인다.
        //        //그래서 링패널이 없어지기전까지 매칭 일어나지 않는다.
        //        //하지만 스폐셜폭발로 터질수 있으니 m_ItemBrusting을 true해준다.
        //        ringpanel.m_Board.SeleteDropIndex = j;
        //        ringpanel.Brust(null, (isBrust, panel)=>
        //        {
        //            //동작은 전부 Brust안에 코루틴에서 한다.
        //            //일단 목록은 바로 삭제 해버리자..
        //            panel.m_Board.m_IsRingBoard = false;
        //            Board.m_ListRingPanel.Remove((RingPanel)panel);//관리목록에서 삭제..
        //        });

        //    }
        //}
        RingPanel ringpanel = null;
        for (int i = Board.m_ListRingPanel.Count - 1; i >= 0; i--)
        {
            ringpanel = Board.m_ListRingPanel[i];
            Board _board = ringpanel.m_Board;

            List<Board> drops = _board.Drops;
            for (int j = 0; j < drops.Count; j++)
            {
                if (drops[j] == null || (drops[j].IsItemExist == false || drops[j].m_ItemBrusting || drops[j].m_DropAnim))
                    continue;
                if (drops[j].m_Item.m_ItemType == ItemType.Normal && ringpanel.m_Color == drops[j].m_Item.m_Color)
                {
                    //여기를 타는 순간 현재 아이템은 링패널이 있는 보드의 영향 아래에 놓인다.
                    //그래서 링패널이 없어지기전까지 매칭 일어나지 않는다.
                    //하지만 스폐셜폭발로 터질수 있으니 m_ItemBrusting을 true해준다.
                    ringpanel.m_Board.SeleteDropIndex = j;
                    ringpanel.Brust(drops[j], (isBrust, panel) =>
                    {
                        //동작은 전부 Brust안에 코루틴에서 한다.
                        //일단 목록은 바로 삭제 해버리자..
                        panel.m_Board.m_IsRingBoard = false;
                        Board.m_ListRingPanel.Remove((RingPanel)panel);//관리목록에서 삭제..
                    });

                }
            }
        }
    }
    #endregion
}
