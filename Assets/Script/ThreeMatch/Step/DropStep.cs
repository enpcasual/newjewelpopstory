﻿using UnityEngine;
using System.Collections;

public class DropStep : BaseStep {

    public override void Step_Play()
    {
        base.Step_Play();
    }

    public override void Step_Process()
    {
        base.Step_Process();

        for (int x = 0; x < MatchDefine.MaxX; x++)
        {
            //MatchMgr.m_ListBoard[MatchDefine.MaxX * (MatchDefine.MaxY - 1) + x].DropItemRow(null);
            MatchMgr.m_ListBoard[MatchDefine.MaxX * (MatchDefine.MaxY - 1) + x].GravityDropItemRow(null);
            //Debug.LogError("X : " + MatchMgr.m_ListSquare[((MatchManager.maxRows - 1) * MatchManager.maxCols) + x].X + "Y : " + MatchMgr.m_ListSquare[((MatchManager.maxRows - 1) * MatchManager.maxCols) + x].Y);
        }

        //모든 애니메이션이 종료 됐다면 빠져 나가자!!
        //bool AllEnd = true;
        //for (int i = 0; i < MatchDefine.MaxX * MatchDefine.MaxY; i++)
        //{
        //    if (MatchMgr.m_ListBoard[i].m_Brusting == true || MatchMgr.m_ListBoard[i].m_DropAnim == true)
        //        AllEnd = false;
        //}

        MatchMgr.SetStep(StepType.Matching);
    }
}
