﻿using UnityEngine;
using System.Collections;

public class MissionStep : BaseStep
{
    public MissionStep():base()
    {
      
    }

    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        //아이템스텝들 다시 타지 않도록 여기서 세팅
        isItemStep = false;

        //Wait스텝에서 하던 부분을 MissionStep으로 옮긴다.
        //(현재상태가 완료되면 클리어나 실패를 체크해야한다.)
        foreach (Board board in MatchMgr.m_ListBoard)
        {
            if (board.m_MatchingCheck)
            {
                MatchMgr.SetStep(StepType.Matching);
                return;
            }
        }

        if (MissionManager.Instance.CheckMissionClear())
        {
            MatchMgr.SetStep(StepType.Clear);
        }
        else if (MissionManager.Instance.CheckMissionFail())
        {
            MissionManager.Instance.MissionFailAnimation(()=>
            {
                MatchMgr.SetStep(StepType.Fail);
            });
        }
        else
        {
            MatchMgr.SetStep(StepType.Wait);
        }

    }

    public override void Step_Process()
    {
        base.Step_Process();
    }

}
