﻿using UnityEngine;
using System.Collections;

public class ShufflingStep : BaseStep {

    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        MatchMgr.SetMatchState(MatchState.Shuffling);
    }

    public override void Step_Process()
    {
        base.Step_Process();
    }
}
