﻿using UnityEngine;
using System.Collections;

public class FailStep : BaseStep
{
    public override void Step_Init()
    {
        base.Step_Init();
    }

    public override void Step_Play()
    {
        base.Step_Play();

        //실패하면
        MatchMgr.SetMatchState(MatchState.GameFail);
       
    }

    public override void Step_Process()
    {
        base.Step_Process();
    }

    public void FailPopupSetting()
    {
      
    }

}
