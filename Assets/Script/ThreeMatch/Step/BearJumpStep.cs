﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class BearJumpStep : BaseStep//ParentItemStep
{
    private List<Item> Bears = new List<Item>();

    int JumpCnt = 0;

    Board board;
    Board top_board;

    public override void Step_Init()
    {
        base.Step_Init();

        JumpCnt = 0;
        board = null;
        top_board = null;
    }

    public void ReplaceBear()
    {
        for (int i = 0; i < MatchMgr.m_ListBoard.Count; i++)
        {
            board = MatchMgr.m_ListBoard[i];

            if (board.m_Item is JellyBearItem)
            {

                ColorType colorIndex = board.m_Item.m_Color;
                board.GenItem(ItemType.Normal, colorIndex); //체인지

                //사운드
                SoundManager.Instance.PlayEffect("bear_goal");
            }
        }
    }

    public override void Step_Play()
    {
        Bears.Clear();
        base.Step_Play();      

        JumpCnt = 0;

        if (MatchMgr.m_CSD.isBearMission)
        {
            //미션이 베어일때 미션 달성하면 남은 베어들 일반블럭으로 ..
            if (MissionMgr.GetMissionLeftCnt(MissionType.Bear, MissionKind.Bear) <= 0 && JellyBearItem.m_Count > 0)
            {
                ReplaceBear();
                return;
            }
        }

        for (int i = 0; i< MatchMgr.m_ListBoard.Count; i++)
        {
            board = MatchMgr.m_ListBoard[i];

            if (board.IsItemExist == false) continue;
            if (board.IsPanelItemDrop == false) continue;
            if ((board.m_Item is JellyBearItem) == false) continue;

            top_board = board.GetDropBoard();

            if (top_board == null || board.m_IsWarpOutBoard)
            {
                if (board.GetConveyerBeltPanel() != null)
                {
                    //컨베이어벨트위에 있을때는 일반아이템으로 바뀌지 않는다.
                }
                else
                {
                    ColorType colorIndex = board.m_Item.m_Color;
                    board.GenItem(ItemType.Normal, colorIndex); //체인지

                    //사운드
                    SoundManager.Instance.PlayEffect("bear_goal");
                }
            }
            else
            {
                //위쪽보드 조건
                if (top_board.IsItemExist == false) continue;
                if (top_board.IsPanelItemDrop == false) continue;
                if (top_board.m_Item is JellyBearItem) continue;
                if (Bears.Contains(board.m_Item)) continue;
                //if (board.m_BearMoveing) continue;
                Bears.Add(board.m_Item);

                JumpCnt++;

                JellyBearJump(board, top_board, () =>
                {
                    JumpCnt--;
                });
            }
        }
    }

    public override void Step_Process()
    {
        base.Step_Process();

        if (JumpCnt <= 0)
        {
            MatchMgr.SetStep(StepType.BearSpawn);
            return;
        }
    }


    void JellyBearJump(Board board, Board top, System.Action Complete)
    {
        //top.m_BearMoveing = true;

        //사운드
        SoundManager.Instance.PlayEffect("bear_jump");

        Item board_Item = board.m_Item;
        Item top_Item = top.m_Item;

        board_Item.m_Board = top;
        top_Item.m_Board = board;

        board.m_Item = top_Item;
        top.m_Item = board_Item;


        board_Item.transform.position += Vector3.back * 0.3f;
        Sequence seq = DOTween.Sequence();
        seq.Append(board_Item.transform.DOScaleY(0.8F, 0.1F).SetEase(Ease.InQuart));
        seq.Append(board_Item.transform.DOScaleY(1.1F, 0.3F).SetEase(Ease.OutQuart));
        seq.Append(board_Item.transform.DOScaleY(1f, 0.1F).SetEase(Ease.InQuart));
        seq.Insert(0F, top_Item.transform.DOMoveY(board.transform.position.y, 0.5F).SetEase(Ease.OutQuart));
        seq.Insert(0F, top_Item.transform.DOMoveX(board.transform.position.x, 0.5F).SetEase(Ease.OutQuart));
        seq.Insert(0F, board_Item.transform.DOMoveY(top.transform.position.y, 0.5F).SetEase(Ease.InOutBack));
        seq.Insert(0F, board_Item.transform.DOMoveX(top.transform.position.x, 0.5F).SetEase(Ease.InOutBack));
        seq.Insert(0.1F, board_Item.transform.DOScaleX(0.9F, 0.1F).SetEase(Ease.OutQuart));
        seq.Insert(0.4F, board_Item.transform.DOScaleX(1, 0.1F).SetEase(Ease.OutQuart));
        seq.OnComplete(() =>
        {
            //여기서 처리 해야한다.
            board.m_MatchingCheck = true;
            top.m_MatchingCheck = true;

            Complete();

            //top.m_BearMoveing = false;
        });
    }
}
