﻿using UnityEngine;
using System.Collections;

public class MatchDefine
{
    //*************
    //Const Value
    //*************
    public const int MaxX = 9;
    public const int MaxY = 9;
    public const int MaxColor = 6;
    public const float BoardWidth = 0.72f;
    public const float BoardHeight = 0.72f;
}