﻿using UnityEngine;
using System.Collections;

public class BaseRestore : MonoBehaviour {

    public void SetRestoreTime(float time)
    {
        CancelInvoke();
        Invoke("Restore", time);
    }

    public void Restore()
    {
        ObjectPool.Instance.Restore(this.gameObject);
    }
}
