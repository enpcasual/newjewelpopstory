﻿using UnityEngine;
using System.Collections;

public class AnimatorRestore : BaseRestore{

    private Animator m_Animator = null;
    private AnimatorStateInfo m_AnimStateInfo;
    // Use this for initialization
    void Awake()
    {
        m_Animator = transform.GetComponent<Animator>();
    }

    public void OnEnable()
    {
        //애니메이션 play
        m_AnimStateInfo = m_Animator.GetCurrentAnimatorStateInfo(0);

        m_Animator.Play(m_AnimStateInfo.shortNameHash, -1, 0);

        //플레이 시간.
        Invoke("Restore", m_AnimStateInfo.length);

    }
}
