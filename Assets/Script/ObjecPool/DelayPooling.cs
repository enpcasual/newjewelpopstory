﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayPooling : MonoBehaviour {

    public float DelayTime = 1.0f;

    private void OnEnable()
    {
        StartCoroutine(Pooling());
    }

    private IEnumerator Pooling()
    {
        yield return new WaitForSeconds(DelayTime);

        ObjectPool.Instance.Restore(this.gameObject);
    }
}
