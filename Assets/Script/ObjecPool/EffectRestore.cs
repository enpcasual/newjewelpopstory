﻿using UnityEngine;
using System.Collections;

public class EffectRestore : BaseRestore{

    public float RestoreTime = 1f;

    void OnEnable()
    {
       Invoke("Restore", RestoreTime);
    }
}