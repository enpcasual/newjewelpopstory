﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddUITextList : MonoBehaviour {
    private UITextList m_UITextList = null;
    private string m_Text = string.Empty;

    private void Awake()
    {
        m_UITextList = GetComponent<UITextList>();
        m_Text = m_UITextList.textLabel.text.ToString();
        OnEnable();
    }

    private void OnEnable()
    {
        m_UITextList.scrollBar.value = 0;
        m_UITextList.Add(m_Text);
    }
}
