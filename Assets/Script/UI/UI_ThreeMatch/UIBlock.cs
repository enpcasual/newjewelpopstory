﻿using UnityEngine;
using System.Collections;

public class UIBlock : MonoBehaviour {
    public UISprite Top_spr;
    public UISprite Bottom_spr;

    void OnEnable()
    {
        //가림막 이미지의 y크기
        float top_hight = Top_spr.localSize.y;
        float bottom_hight = Bottom_spr.localSize.y;

        //보드의 가장 윗쪽 좌표 
        float board_top = MatchManager.Instance.GameField.transform.position.y + (MatchDefine.BoardHeight/2) - MatchDefine.BoardHeight * MatchManager.Instance.TopEmptyCnt;
        //보드의 가장 아랫쪽 좌표
        float board_bottom = MatchManager.Instance.GameField.transform.position.y + (MatchDefine.BoardHeight / 2) - (MatchDefine.BoardHeight * MatchDefine.MaxY) + MatchDefine.BoardHeight * MatchManager.Instance.BottomEmptyCnt; ;

        Vector3 screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(new Vector3(0, board_top, 0));
        Vector3 uipos = new Vector3(0, GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos).y, 0);
        Top_spr.transform.position = uipos;                                 //보드의 위치는 world좌표
        Top_spr.transform.localPosition += new Vector3(0, top_hight/2, 0);  //이미지 위치는 Local좌표
        Top_spr.transform.localPosition = new Vector3(0, Top_spr.transform.localPosition.y, 0);

        screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(new Vector3(0, board_bottom, 0));
        uipos = new Vector3(0, GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos).y, 0);
        Bottom_spr.transform.position = uipos;                                 //보드의 위치는 world좌표
        Bottom_spr.transform.localPosition += new Vector3(0, -bottom_hight / 2, 0);  //이미지 위치는 Local좌표
        Bottom_spr.transform.localPosition = new Vector3(0, Bottom_spr.transform.localPosition.y, 0);
    }

}
