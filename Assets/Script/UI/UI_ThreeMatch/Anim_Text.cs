﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Anim_Text : MonoBehaviour
{
    UISprite m_UISpr;
    Animation m_Anim;
    public void Awake()
    {
        m_UISpr = GetComponent<UISprite>();
        m_Anim = GetComponent<Animation>();

        if(m_UISpr == null)
            m_UISpr = GetComponentInChildren<UISprite>();

        if(m_Anim == null)
            m_Anim = GetComponentInChildren<Animation>();

    }

    public void SetSprite(string spritename)
    {
        m_UISpr.spriteName = spritename;
        m_UISpr.MakePixelPerfect();

        m_Anim.Play();

        AnimationClip clip = m_Anim.clip; //The default animation.
        Invoke("Restore", clip.length);
    }

    public void Restore()
    {
        ObjectPool.Instance.Restore(gameObject);
    }
}
