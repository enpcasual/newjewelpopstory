﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsHightController : MonoBehaviour {

    void OnEnable()
    {
        //배너위치가 top으로 바뀌면서 주석처리!!
        ////광고 삭제
        if (GameManager.Instance.m_GameData.AdsFreeChk)
            transform.localPosition = Vector3.zero;
        else
        {
            //광고에 따른 바텀 위치
            transform.localPosition = new Vector3(0, -AdMob_Manager.Instance.AdMob_Height, 0);
        }
    }
}
