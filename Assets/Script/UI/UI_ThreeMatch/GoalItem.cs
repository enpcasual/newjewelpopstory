﻿using UnityEngine;
using System.Collections;

public class GoalItem : MonoBehaviour {

    public Vector3 m_LocalPos;
    public UISprite m_Sprite;
    public UILabel m_Label;
    public UISprite m_Check;

    public MissionInfo m_Info;

    public void SetInfo(MissionInfo info, int index, int totalcnt)
    {
        m_Info = info;

        SetSprite(m_Info.kind);

        m_Label.gameObject.SetActive(true);
        m_Check.gameObject.SetActive(false);

        //SetLabel(m_Info.count);
        if (info.kind == MissionKind.Jam)
            SetLabel(((MissionManager.Instance.Jam_RemainCnt - info.count) * 100 / MissionManager.Instance.Jam_RemainCnt), true);
        else
            SetLabel(info.count, false);

        transform.localPosition = m_LocalPos;

        //미션 갯수에 따른 좌표 세팅

        if (totalcnt == 1)   //미션이 1개일때
        {
            transform.localPosition = new Vector3(0, 4, 0);
        }
        else if (totalcnt == 2) //미션이 2개일때
        {
            transform.localPosition = new Vector3(m_LocalPos.x, 4, 0);
        }
        else if (totalcnt == 3)
        {
            if (index == 0)
                transform.localPosition = new Vector3(0, m_LocalPos.y, 0);
            else if(index == 1)
                transform.localPosition = UI_ThreeMatch.Instance.m_ListGoalItem[2].m_LocalPos;
            else if(index == 2)
                transform.localPosition = UI_ThreeMatch.Instance.m_ListGoalItem[3].m_LocalPos;
        }
    }

    public void SetSprite(MissionKind kind)
    {
        m_Sprite.spriteName = GetMissionSpriteName(kind);
    }

    public void SetLabel(int num, bool per)
    {
        if (num <= 0 && per == false)
        {
            m_Label.gameObject.SetActive(false);
            m_Check.gameObject.SetActive(true);
        }
        else
        {
            m_Label.gameObject.SetActive(true);
            m_Check.gameObject.SetActive(false);
            if (per)
                m_Label.text = num.ToString() + "%";
            else
                m_Label.text = num.ToString();
        }

    }


    static public string GetMissionSpriteName(MissionKind kind)
    {
        string spriteName = "";
        switch (kind)
        {
            case MissionKind.Red:
                spriteName = "item_01";
                break;
            case MissionKind.Yellow:
                spriteName = "item_02";
                break;
            case MissionKind.Green:
                spriteName = "item_03";
                break;
            case MissionKind.Blue:
                spriteName = "item_04";
                break;
            case MissionKind.Purple:
                spriteName = "item_05";
                break;
            case MissionKind.Orange:
                spriteName = "item_06";
                break;
            case MissionKind.Donut:
                spriteName = "item_donut";
                break;
            case MissionKind.Spiral:
                spriteName = "item_spiral";
                break;
            case MissionKind.Bread:
                spriteName = "panel_bread_01";
                break;
            case MissionKind.LollyCage:
                spriteName = "panel_lolly_cage_01";
                break;
            case MissionKind.IceCage:
                spriteName = "panel_ice_cage_01";
                break;
            case MissionKind.Cracker:
                spriteName = "panel_cracker_rnd";
                break;
            case MissionKind.Bottle:
                spriteName = "panel_bottle_01";
                break;
            case MissionKind.Line:
                spriteName = "Mission_L";
                break;
            case MissionKind.Line_Line:
                spriteName = "Mission_LL";
                break;
            case MissionKind.Cross_Line:
                spriteName = "Mission_LC";
                break;
            case MissionKind.Bomb_Line:
                spriteName = "Mission_LB";
                break;        
            case MissionKind.Rainbow_Line:
                spriteName = "Mission_LR";
                break;
            case MissionKind.Cross:
                spriteName = "Mission_C";
                break;
            case MissionKind.Cross_Cross:
                spriteName = "Mission_CC";
                break;
            case MissionKind.Bomb_Cross:
                spriteName = "Mission_BC";
                break;
            case MissionKind.Rainbow_Cross:
                spriteName = "Mission_CR";
                break;
            case MissionKind.Bomb:
                spriteName = "Mission_B";
                break;
            case MissionKind.Bomb_Bomb:
                spriteName = "Mission_BB";
                break;       
            case MissionKind.Rainbow_Bomb:
                spriteName = "Mission_BR";
                break;
            case MissionKind.Rainbow:
                spriteName = "item_rainbow";
                break;
            case MissionKind.Rainbow_Rainbow:
                spriteName = "Mission_RR";
                break;
            case MissionKind.TimeBomb:
                spriteName = "Mission_TimeBomb";
                break;
            case MissionKind.Wafer:
                spriteName = "panel_wafer_01";
                break;
            case MissionKind.StrawberryCake:
                spriteName = "Food_01";
                break;
            case MissionKind.ChocolatePiece:
                spriteName = "Food_02";
                break;
            case MissionKind.MintCake:
                spriteName = "Food_03";
                break;
            case MissionKind.Parfait:
                spriteName = "Food_04";
                break;
            case MissionKind.WhiteCake:
                spriteName = "Food_05";
                break;
            case MissionKind.Hamburger:
                spriteName = "Food_06";
                break;
            case MissionKind.Bear:
                spriteName = "Mission_Bear";
                break;
            case MissionKind.IceCream:
                spriteName = "panel_icecream_01";
                break;
            case MissionKind.Jam:
                spriteName = "panel_jam";
                break;
            case MissionKind.S_Tree:
                spriteName = "panel_s_tree_03";
                break;
            case MissionKind.Stele:
                spriteName = "panel_Stele";
                break;
            default:
                spriteName = "";
                break;
        }

        return spriteName;
    }
}