﻿using UnityEngine;
using System.Collections;

public class UI_Main : UI_Base {

    public static UI_Main Instance;

    //BG
    public UISprite Title_BG;

    //Top
    public UIAnchor Top_Anchor;     //상단 앵커관리
    public UISprite titleTxt;       //타이틀 이미지
    //public UISprite titleTruck;     //우측상단 트럭 이미지

    //Bottom
    public UIAnchor Bottom_Anchor;
    public UISprite Progress_bar;
    public UISprite Progress_barbg;
    public UILabel Porcess_txt;
    public UISprite titelbtn;

    //Effect
    public GameObject Particle_Light;
    

    int m_MaxProcess = 10;
    int m_Process = 0;

    void Awake()
    {
        Instance = this;

        //        "ko",//한국어
        //        "eng",//영어
        //        "ja",//일어
        //        "zh_TW",//중국(번체)
        //        "zh_CN",//중국(간체)
        //        "hi",//인도
        //        "ar",//아랍
        //        "ru",//러시아
        //        "de",//독일
        //        "pl"//폴란드

        //int lang = SecurityPlayerPrefs.GetInt("CurrenLanguage", -1);
        //string getlanguagetype = Native_Brige.GetLanguage();
        ////처음 켰을때 기기언어가 일본이거나  게임중 언어설정을 일본으로 했을때
        //if ((lang == -1 && getlanguagetype.Equals("ja")) || lang == 2)
        //{
        //    titleTxt.spriteName = "titletxt_jp";
        //    titelbtn.spriteName = "titlebtn_jp";
        //    //titleTruck.transform.localPosition = new Vector3(150, 65, 0);
        //}

        //태블릿일때 좌표 수정
        float ratio = (float)Screen.height / Screen.width;
        Debug.Log("화면 비율 : " + ratio);
        if (ratio < 1.5) //아이패드이면.. (요즘폰 1.77, 아이폰4s 1.5 아이패드 1.33)
        {
            Title_BG.transform.localPosition = new Vector3(0, -45, 0);
            Top_Anchor.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            Top_Anchor.relativeOffset = new Vector2(0, 0.01f);
            Bottom_Anchor.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            Bottom_Anchor.relativeOffset = new Vector2(0, 0.1f);
        }
    }
    
    public bool CheckGDPR()
    {
        //Debug.Log(" On Check GDPR");
        //Debug.Log(" Native Contry : " + Native_Brige.GetCountry_ISO_3166_1());
        //switch (Native_Brige.GetCountry_ISO_3166_1())
        //{
        //    case "GRC": case "GR":  //그리스: GR
        //    case "NLD": case "NL":  //네덜란드:NL
        //    case "DNK": case "DK":  //덴마크:DK
        //    case "DEU": case "DE":  //독일:DE
        //    case "LVA": case "LV":  //라트비아:LV
        //    case "ROU": case "RO":  //루마니아:RO
        //    case "LUX": case "LU":  //룩셈부르크:LU
        //    case "LTU": case "LT":  //리투아니아:LT
        //    case "MLT": case "MT":  //몰타:MT
        //    case "BEL": case "BE":  //벨기에:BE
        //    case "BGR": case "BG":  //불가리아:BG
        //    case "SWE": case "SE":  //스웨덴:SE
        //    case "ESP": case "ES":  //스페인:ES
        //    case "SVK": case "SK":  //슬로바키아:SK
        //    case "SVN": case "SI":  //슬로베니아:SI
        //    case "IRL": case "IE":  //아일랜드:IE
        //    case "EST": case "EE":  //에스토니아:EE
        //    case "GBR": case "GB":  //영국:GB
        //    case "AUT": case "AT":  //오스트리아:AT
        //    case "ITA": case "IT":  //이탈리아:IT
        //    case "CZE": case "CZ":  //체코:CZ
        //    case "HRV": case "HR":  //크로아티아:HR
        //    case "CYP": case "CY":  //키프로스:CY
        //    case "PRT": case "PT":  //포르투갈:PT
        //    case "POL": case "PL":  //폴란드:PL
        //    case "FRA": case "FR":  //프랑스:FR
        //    case "FIN": case "FI":  //핀란드:FI
        //    case "HUN": case "HU":  //헝가리:HU
        //        return true;
        //    default:
        //        return false;
        //}
        return false;
    }

    //public void titleloading()
    //{
    //    GameManager.Instance.titleloadingstart();

    //    GetComponent<Animator>().SetBool("TXT_ANI", true);
    //}

    public void ProgressLoading()
    {
         Progress_bar.fillAmount = 0.0f;

        Progress_bar.gameObject.SetActive(true);

        Progress_barbg.gameObject.SetActive(true);

        Porcess_txt.gameObject.SetActive(false);
    }

    public void ProgresstxtDisplay()
    {
        Porcess_txt.gameObject.SetActive(true);
    }

    public void ProgressLoadingEnd()
    {
        Porcess_txt.gameObject.SetActive(false);
        Progress_bar.gameObject.SetActive(false);
        Progress_barbg.gameObject.SetActive(false);

        SoundManager.Instance.PlayBGM("intro_bgm");

        ChanageState();


        //titelbtn.gameObject.SetActive(true);

        //이펙트 출력
        Particle_Light.SetActive(true);
    }

    public void ProgressState()
    {
        m_Process++;
        Progress_bar.fillAmount = (float)m_Process / (float)m_MaxProcess;

        if (m_Process >= 10)
        {
            //100% 문구 출력
            ProgresstxtDisplay();
        }
                  

       // loadinglabel.text = GameManager.Instance.m_loadingstate;
    }

	//void Start () {
      
 //   }
	
	//// Update is called once per frame
	//void Update () {
	
	//}

    public override void StartUI()
    {
        //GoogleAnalytics_Manager.Instance.LogScreen("Main Screen");
    }

    public override void EndUI()
    {     

    }

    public void PlayClick()
    {

        
        titelbtn.GetComponent<Animator>().SetBool("BUTTON_MOVE", true);

        StartCoroutine(Playstart());
    }

    IEnumerator Playstart()
    {
        yield return new WaitForSeconds(0.5f);
        ChanageState();

    }
    public void ChanageState()
    {

        SoundManager.Instance.PlayEffect("play_click");
        StageItemManager.Instance.Init();
                  
        
        //StageItemManager.Instance.nowPart = StageItemManager.Instance.CurrentPart;

        GameManager.Instance.m_startCarport = true;

        /*
        for (int i = 0; i < StageItemManager.Instance.part.Length; i++)
        {
            if (StageItemManager.Instance.part[i].activeSelf)
            {
                StageItemManager.Instance.part[i].SetActive(false);
            }


        }
         * */

        if (GameManager.Instance.m_GameData.GamePlayFirst)
        {
            GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);

            GameManager.Instance.GameStart((int)StageItemManager.Instance.TotalnowStage + 1);
            
            //StageItemManager.Instance.nextplaychk = true;
        }
        else
        {
            
            GameManager.Instance.SetGameState(GAMESTATE.WorldMap);
        }


        




        /*
        if (GameManager.Instance.GamePlayFirst)
        {
            //
            GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);

           // GameManager.Instance.SetGameState(GAMESTATE.WorldMap);


          
            //
        }
        else
        {
            GameManager.Instance.SetGameState(GAMESTATE.WorldMap);
            //StageItemManager.Instance.Character.GetComponent<CarPortTruck>().IdleTruck();
           
        }
         * */


        /*
        //일단 여기에서 애드몹 광고 출력
        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            AdMob_Manager.Instance.AdMob_Banner_Show();
        }
        */

    }

    public void ButtonClick(GameObject obj)
    {
        switch (obj.name)
        {
            case "Button 1":
                GameManager.Instance.SetGameState(GAMESTATE.WorldMap);
                break;
            case "Button 2":
                GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);
                break;
            case "Button 3":
                GameManager.Instance.SetGameState(GAMESTATE.CarPort);
                break;

        }

    }

  
}
