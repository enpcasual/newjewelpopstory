﻿using UnityEngine;
using System.Collections;

public abstract class UI_Base : MonoBehaviour {

    public GAMESTATE m_GameState= GAMESTATE.Main;

	public abstract void StartUI();

    public abstract void EndUI();

     public virtual void OnDestroy()
    {
        //게임에서 EditorMode로 넘어갈때 호출해줘야한다.
        //아니면 static이라서 delegate가 남아 있으니 호출되는 문제 발생. 
        EndUI();
    }

}
