﻿using UnityEngine;
using System.Collections;

public class Agreement : MonoBehaviour
{
    private bool isAgreePrivacy = false;
    private bool isAgreeService = false;

    [SerializeField]
    private GameObject PrivacyCheck;

    [SerializeField]
    private GameObject ServiceCheck;

    [SerializeField]
    private GameObject ButtonCover;

    private event System.Action OnComplete;

    public void Init(System.Action onComplete = null)
    {
        if (SecurityPlayerPrefs.GetInt("GamePlayAgreement", 0) == 1)
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }

            return;
        }

        PrivacyCheck.SetActive(false);
        ServiceCheck.SetActive(false);

        ButtonCover.SetActive(true);

        gameObject.SetActive(true);

        if (onComplete != null)
        {
            OnComplete = onComplete;
        }
    }

    public void OnClickAgreePrivacy()
    {
        isAgreePrivacy = !isAgreePrivacy;

        PrivacyCheck.SetActive(isAgreePrivacy);

        CheckContinue();
    }

    public void OnClickAgreeService()
    {
        isAgreeService = !isAgreeService;

        ServiceCheck.SetActive(isAgreeService);

        CheckContinue();
    }

    public void CheckContinue()
    {
        if(isAgreeService && isAgreePrivacy)
        {
            ButtonCover.SetActive(false);
        }
    }

    public void OnClickContinue()
    {
        SecurityPlayerPrefs.SetInt("GamePlayAgreement", 1);

        gameObject.SetActive(false);

        if(OnComplete != null)
        {
            OnComplete.Invoke();
        }
    }
}