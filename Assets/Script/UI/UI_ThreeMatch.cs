﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;

public enum TruckAbilityType
{
    MovePlus,
    ScorePlus,
    BonusPlus,
    ButterFlyPlus,
}
public class UI_ThreeMatch : UI_Base
{
    public static UI_ThreeMatch Instance;


    //***************
    //Inspector 세팅
    //***************    
    public GameObject ThreeMatch_Obj;
    [HideInInspector]
    public GameManager GameMgr;
    public MatchManager MatchMgr;

    //Root
    //public Transform TOP_UI;
    public Transform BOTTOM_UI;
    public Transform AnimTextPoint; //트럭효과 텍스트의 Parent용.(애니메이션이 좌표를 건들고 있어서 부모밑으로 넣어서 자리를 잡아줘야함)
    public Transform TruckPoint;
    public Transform Top_Point;
    public Transform Bottom_Point;

    public UISprite m_TopBG;
    //트럭
    public UISprite m_TruckSprite;
    //미션
    public List<GoalItem> m_ListGoalItem;
    //무브횟수
    public UILabel MoveLabel;

    //점수
    public Transform Score1_trm;    //점수에 따른 위치 세팅용
    public Transform Score2_trm;    //점수에 따른 위치 세팅용
    public Transform Score3_trm;    //점수에 따른 위치 세팅용
    public GameObject Star1;        //점수에 따른 별 표시용
    public GameObject Star2;        //점수에 따른 별 표시용
    public GameObject Star3;        //점수에 따른 별 표시용
    public UISprite ScoreGauge;     //점수 게이지
    public UILabel ScoreLabel;      //점수 숫자

    //무료트럭 텍스트
    public UILabel FreeTruckLabel; 

    //스테이지
    public UILabel Stage;

    //캐쉬아이템
    public List<UISprite> List_CashItemIcon;
    public List<UILabel> List_CashItemLabel;
    public List<GameObject> List_CashItemPlus;
    public GameObject UI_Block; //캐쉬아이템 사용시 UI막음.
    GameObject SelectCashItemObject;   //선택한 캐쉬아이템.
    public int[] CashUseCnt = new int[3];

    public List<GameObject> CashItemCovers = new List<GameObject>();
    public List<GameObject> CashItemEffect = new List<GameObject>();

    //특정아이템 텍스트
    public GameObject UI_JellyMonText;

    //나중에 삭제.
    public UIButton m_StartButton;
    public UIButton m_EndButton;

    //*************
    //Prefab
    //**************
    public GameObject PF_GetMissionItem;
    public GameObject PF_ScoreText;
    public GameObject PF_ComboText;
    public GameObject PF_BonusTime;
    public GameObject PF_BonusStar;
    public GameObject PF_BonusCircle;
    public GameObject PF_MoveWarning;
    public GameObject PF_Cash_Hammer;
    public GameObject PF_Cash_Bomb;
    public GameObject PF_Cash_Thunder;
    public GameObject PF_Anim_Text;
    public GameObject PF_TruckText;
    public GameObject PF_StelePiece;

    public List<TutorialTarget> List_Tutorial_Target;

    //광고버튼관련
    public UISprite freecoin;
    public UILabel freecointime;
    public UISprite freecoineffect;

    public GameObject FreecoinObj = null;

    public List<UISprite> LoadedMissionItemTemps = new List<UISprite>();
    
    void Awake()
    {
        Instance = this;
#if UNITY_EDITOR
        m_StartButton.gameObject.SetActive(true);
        m_EndButton.gameObject.SetActive(true);
#else
        m_StartButton.gameObject.SetActive(false);
        m_EndButton.gameObject.SetActive(false);
#endif
    }

    // Use this for initialization
    void Start () {
        
        //*****************************
        //UI쪽에서 사용하는 ObjectPool
        //*****************************
        ObjectPool.Instance.CreatePool(PF_GetMissionItem, 50);
        ObjectPool.Instance.CreatePool(PF_ScoreText, 50);

        List<UILabel> li_Label = new List<UILabel>();
        for (int i = 0; i < 90; i++)
        {
            UILabel label = ObjectPool.Instance.GetObject<UILabel>(PF_ScoreText, this.transform);
            label.transform.localScale = Vector3.zero;
            li_Label.Add(label);
        }
        
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(2f);
        seq.OnComplete(() =>
        {
            foreach (UILabel label in li_Label)
                ObjectPool.Instance.Restore(label.gameObject);
        });
    }

    void OnEnable()
    {
        if (!GameManager.Instance.m_GameData.AdsFreeChk && StageItemManager.Instance.TotalnowStage != 0)
        {
            AdMob_Manager.Instance.AdMob_Banner_Show();
        }
    }

    void OnDisable()
    {
        ClearMissionItemAnim();
        AdMob_Manager.Instance.AdMob_Banner_Hide();
    }

    // Update is called once per frame
    // void Update () {

    //}

    public override void StartUI()
    {
        ThreeMatch_Obj.SetActive(true);
        
        GameMgr = GameManager.Instance;
        MatchMgr = MatchManager.Instance;

        //게임 시작할때로 이동!
        //int index = UnityEngine.Random.Range(0, 3);
        //if (index == 0)
        //    SoundManager.Instance.PlayBGM("play_bgm1");
        //else if (index == 1)
        //    SoundManager.Instance.PlayBGM("play_bgm2");
        //else
        //    SoundManager.Instance.PlayBGM("intro_bgm");

    }

    public override void EndUI()
    {
        //Destory될때는 이미 없어서 에러가 난다.
        if (ThreeMatch_Obj != null)
            ThreeMatch_Obj.SetActive(false);

    }

    //bool isTestMediumAds = true;
    public void ButtonClick(GameObject obj)
    {
        switch (obj.name)
        {
            case "Pause":
                if (MatchMgr.m_StepType == StepType.Wait && MatchManager.Instance.isSwitching == false)
                {
                    Popup_Manager.INSTANCE.popup_index = 17;
                    Popup_Manager.INSTANCE.Popup_Push();
                }
                //클리어화면에서 멍통됐을때 월드맵으로 나갈수 있도록 임시방편..
                else if (MatchMgr.m_MatchState == MatchState.GameClear)
                {
                    if(Popup_Manager.INSTANCE.popup_stack.Count <= 0)
                    {
                        Popup_Manager.INSTANCE.popup_index = 17;
                        Popup_Manager.INSTANCE.Popup_Push();
                    }
                }
                break;
            case "C_Item1":
                SelectCashItemObject = obj;
                if (SelectCashItem == CashItem.Hammer) //이미 선택했는데 또 선택
                    CashItemSelect(CashItem.None);
                else
                    CashItemSelect(CashItem.Hammer);
                break;
            case "C_Item2":
                SelectCashItemObject = obj;
                if (SelectCashItem == CashItem.Bomb) //이미 선택했는데 또 선택
                    CashItemSelect(CashItem.None);
                else
                    CashItemSelect(CashItem.Bomb);
                break;
            case "C_Item3":
                SelectCashItemObject = obj;
                if (SelectCashItem == CashItem.Lightning) //이미 선택했는데 또 선택
                    CashItemSelect(CashItem.None);
                else
                    CashItemSelect(CashItem.Lightning);
                break;
            case "Button_Start":
                if (MatchMgr.m_StepType == StepType.Wait)
                {
                    MatchMgr.StartGame();
                    SoundManager.Instance.PlayEffect("play_click");
                }
                break;
            case "Button_End":
                if (MatchMgr.m_StepType == StepType.Wait)
                {
                    MissionManager.Instance.m_FailType = FailType.Limit_Move;
                    MatchMgr.SetMatchState(MatchState.GameFail);
                }
                break;
            case "Button_Editor":
#if UNITY_EDITOR
                if (GameManager.Instance.isPlayFromEditor)
                {
                    MatchMgr.GoToEditor();
                }
#endif
                break;
        }
    }


#region 시작세팅,UI
    public int CurrentTruckGrade = 0; //무료트럭때문에 현재 트럭상태로 능력치 부여한다.

    //게임 시작할때마다 다시 세팅해줘야 한다.
    public void UI_Init()
    {
        //상단배경 세팅 
        //임시주석. 테마별 이미지 바뀌면 주석해제
        //int num = StageItemManager.Instance.nowPart + 1;
        //m_TopBG.spriteName = "Theme" + num;

        FreecoinObj.SetActive(AdManager.Instance.IsAdReady);

        //트럭세팅
        if (GameMgr.FreeUsecnt > 0 && GameMgr.FreeUseTruckGrade > GameMgr.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
        {
            CurrentTruckGrade = GameMgr.FreeUseTruckGrade;
            m_TruckSprite.spriteName = GameMgr.FreeUseTruckGrade.ToString();
            FreeTruckLabel.gameObject.SetActive(true);
            FreeTruckLabel.text = "X" + GameMgr.FreeUsecnt.ToString();
            FreeTruckLabel.transform.localScale = new Vector3(2, 2, 2);
            Tweener tw = FreeTruckLabel.transform.DOScale(1f, 1f);
            tw.SetDelay(0.2f);
            tw.SetEase(Ease.OutBack);

            //감소 & 세이브
            GameMgr.FreeUsecnt--;
            SecurityPlayerPrefs.SetInt("FreeUsecnt", GameMgr.FreeUsecnt);
            if (GameMgr.FreeUsecnt <= 0)
            {
                GameMgr.FreeUseTruckGrade = 0;
                SecurityPlayerPrefs.SetInt("FreeUseTruckGrade", GameMgr.FreeUseTruckGrade);
            }

        }
        else
        {
            CurrentTruckGrade = GameMgr.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade;
            m_TruckSprite.spriteName = GameMgr.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade.ToString();
            FreeTruckLabel.gameObject.SetActive(false);
        }

        m_TruckSprite.MakePixelPerfect();
        //m_TruckSprite.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);//애니메이션때문에 안됨. 부모오브젝트를 1.2배

        //캐쉬아이템

        CashItemInit();

        for (int i = 0; i < GameMgr.m_GameData.MyItemCnt.Count; i++)
        {
            CashItemUpdate(i);
        }

        CashUseCnt[0] = 0;
        CashUseCnt[1] = 0;
        CashUseCnt[2] = 0;

        //초기화
        UI_Block.SetActive(false);
        UI_JellyMonText.SetActive(false);
        ClearMissionItemAnim();
        CashItemSelect(CashItem.None);
        //코인버튼
        FreeCoinSetting();

        MovesAnimationSetting(false);
    }

    public void CashItemInit()
    {
        for (int i = 0; i < CashItemCovers.Count; i++)
        {
            CashItemCovers[i].SetActive(SecurityPlayerPrefs.GetBool("ItemCovers" + i, true));
        }

        for (int i = 0; i < CashItemCovers.Count; i++)
        {
            CashItemEffect[i].SetActive(false);
        }
    }

    public void TutorialCashItemInit(int index)
    {
        CashItemEffect[index].SetActive(true);
        CashItemCovers[index].SetActive(false);
        CashItemUpdate(index);
    }

    #endregion

    #region 미션관련
    public void MissionInit(List<MissionInfo> listInfo)
    {
        FirebaseManager.Instance.LogScreen("ThreeMatch Screen ", MatchMgr.m_StageIndex);

        //일단 미션 초기화 할때 스테이지 보여준다.나중에 수정..
        Stage.text = MatchMgr.m_StageIndex.ToString();
        if (MatchMgr.m_StageIndex > 10000)
            Stage.text = "S " + MatchMgr.m_StageIndex%10;


        //미션UI 초기화
        for (int i = 0, len = m_ListGoalItem.Count; i < len; i++)
        {
            m_ListGoalItem[i].gameObject.SetActive(false);
        }


        for (int i = 0; i < listInfo.Count; i++)
        {
            m_ListGoalItem[i].gameObject.SetActive(true);
            m_ListGoalItem[i].SetInfo(listInfo[i], i, listInfo.Count);
        }
    }

    public void MissionUpdate(int index, MissionInfo info)
    {
        if (info.kind == MissionKind.Jam)
            m_ListGoalItem[index].SetLabel(((MissionManager.Instance.Jam_RemainCnt - info.count) * 100 / MissionManager.Instance.Jam_RemainCnt ), true);
        else
            m_ListGoalItem[index].SetLabel(info.count, false);
        Sequence seq = DOTween.Sequence();
        seq.Append(m_ListGoalItem[index].transform.DOScale(1.4f, 0.1f));
        seq.Append(m_ListGoalItem[index].transform.DOScale(1, 0.05f));
        //seq.OnComplete(() =>
        //{
        //    m_ListGoalItem[i].SetLabel(var2.ToString());
        //});
    }
    #endregion

    #region 무브횟수

    [Header("Move Warning")]
    
    [SerializeField]
    private TweenScale MovesScale;

    [SerializeField]
    private TweenRotation MovesRotate;

    [SerializeField]
    private ParticleSystem MoveAddEffect;

    [SerializeField]
    private float MovesAnimationLoopTime;

    [SerializeField]
    private Color MovesColor;

    [SerializeField]
    private Color MovesEffectColor;

    public void MoveWarning()
    {
        GameObject obj = ObjectPool.Instance.GetObject(PF_MoveWarning, this.transform);
        obj.transform.localScale = new Vector3(1f, 1f, 1f);

        Invoke("MoveWarning_Sound", 0.5f);
    }

    public void MovesAnimationSetting(bool useFast)
    {
        if (useFast && MatchMgr.m_MatchState != MatchState.BonusTime)
        {
            MovesScale.duration = MovesAnimationLoopTime * 0.05f;
            MovesRotate.duration = MovesAnimationLoopTime * 0.1f;
            MoveLabel.color = _red;
            MoveLabel.effectColor = _red_effect;
        }
        else
        {
            MovesScale.duration = MovesAnimationLoopTime * 0.5f;
            MovesRotate.duration = MovesAnimationLoopTime;
            MoveLabel.color = MovesColor;
            MoveLabel.effectColor = MovesEffectColor;
        }
    }

    public void MoveWarning_Sound()
    {
        SoundManager.Instance.PlayEffect("5move_reminder");
        SoundManager.Instance.PlayEffect("5move_reminder2");
    }

    public void MoveWaringRestore()
    {

    }

    public void MoveLimitSetting(int cnt, bool moveAddEffect = false)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(MoveLabel.transform.DOScale(cnt <= 5 ? 1.5f : 1.2f, 0.07f));
        seq.Append(MoveLabel.transform.DOScale(1f, 0.03f));
        seq.OnComplete(() =>
        {
            MoveLabel.text = cnt.ToString();
            MovesAnimationSetting(cnt <= 5);
        });

        if (moveAddEffect)
        {
            MoveAddEffect.Stop();
            MoveAddEffect.Play();
        }
    }
    #endregion

    #region 미션, 점수획득, 콤보, 셔플링, 보너스타임, 무브5회
    public IEnumerator GetMissionItemAnim(GameObject target, int index, MissionKind kind, int sequence, bool scale, System.Action action)
    {
        //날라가는 Object 세팅
        UISprite sr = ObjectPool.Instance.GetObject<UISprite>(PF_GetMissionItem, this.transform);
        sr.transform.localScale = Vector3.one;
        sr.spriteName = GoalItem.GetMissionSpriteName(kind);
        sr.MakePixelPerfect();
        LoadedMissionItemTemps.Add(sr); // 코루틴 중간 종료시 클리어(restore을 위해).OnDisable / ClearMissionItemAnim 참고

        //시작 위치
        Vector3 FirstPos = GameManager.Instance.MatchCamera.WorldToScreenPoint(target.transform.position);
        FirstPos = GameManager.Instance.UICamera.ScreenToWorldPoint(FirstPos);
        FirstPos.z = 0;

        //목표 위치
        Vector3 EndPos = m_ListGoalItem[index].transform.position;


        float range = UnityEngine.Random.Range(-1.0f, 1.0f) + ((FirstPos.x + EndPos.x) * 0.5f);
        Vector3 MiddlePos = new Vector3(range, (FirstPos.y + EndPos.y) * 0.5f, 0.1f);

        float speed = 0.025f;
        float Delta = 0f;
        float accel = 0f;

        float MiddleTime = sequence;

        //sr.enabled = false;

        //yield return new WaitForSeconds(sequence/3f);

        //sr.enabled = true;

        speed -= 0.003f * sequence;
        if (speed < 0)
            speed = 0;

        sr.transform.localEulerAngles = Vector3.zero;
        sr.transform.position = FirstPos;
        if (scale)
        {
            sr.transform.DOScale(1.5f, 0.2f).SetLoops(2, LoopType.Yoyo);
            yield return new WaitForSeconds(0.2f);
        }

        while (true)
        {
            accel = Time.deltaTime * 0.01f;//accel = 10 * Time.deltaTime / 1000;
            speed += accel;

            Delta += speed;

            sr.transform.localEulerAngles = new Vector3(0, 0, 360 * Delta);
            sr.transform.position = Bezier.Bezier2(FirstPos, MiddlePos, EndPos, Delta);
            //if coroutine is broken/

            if (Delta > 1)
                break;

            yield return null;
        }

        ObjectPool.Instance.Restore(sr.gameObject);
        action();

        LoadedMissionItemTemps.Remove(sr);

        //float x = Random.Range(0.5f, 1f);
        //float y = Random.Range(0.3f, 0.7f);
        //Vector3 fallPos = target.transform.position + new Vector3(Random.Range(0,2)==0? x : -x, -y, 0);

        //Sequence seq = DOTween.Sequence();
        //seq.Append(sr.transform.DOMove(fallPos, 0.4f).SetEase(Ease.OutQuad));
        //seq.Insert(0f, sr.transform.DOScale(1.2f, 0.2f);
        //seq.Insert(0.4f, sr.transform.DOScale(1, 0.15f));

        //seq.Append(sr.transform.DOMove(dest, 0.6f).SetEase(Ease.InOutSine));
        ////seq.Append(sr.transform.DOScale(1.2f, 0.3f));
        ////seq.Append(sr.transform.DOScale(1f, 0.2f));
        //seq.OnComplete(() =>
        //{
        //    ObjectPool.Instance.Restore(sr.gameObject);
        //    action();
        //});
    }

    private void ClearMissionItemAnim()
    {
        if (ObjectPool.Instance == null) // 종료시
            return;

        if (LoadedMissionItemTemps == null || LoadedMissionItemTemps.Count <= 0)
            return;

        bool isPoolAlive = ObjectPool.Instance.gameObject.activeInHierarchy;
        
        if(isPoolAlive)
        {
            var e = LoadedMissionItemTemps.GetEnumerator();
            while (e.MoveNext())
            {
                ObjectPool.Instance.Restore(e.Current.gameObject);
            }
            LoadedMissionItemTemps.Clear();
        }
        else
        {
            var e = LoadedMissionItemTemps.GetEnumerator();
            while (e.MoveNext())
            {
                e.Current.gameObject.SetActive(false);
            }
        }

    }

    public IEnumerator GetMissionItemAnim_Stele(GameObject target, int index, MissionKind kind, int sequence, bool scale, int steletype, System.Action action)
    {
        //날라가는 Object 세팅
        UISprite sr = ObjectPool.Instance.GetObject<UISprite>(PF_GetMissionItem, this.transform);
        sr.transform.localScale = Vector3.one;
        sr.spriteName = GoalItem.GetMissionSpriteName(kind) + "_0" + steletype.ToString();
        sr.MakePixelPerfect();

        //시작 위치 세팅
        Vector3 FirstPos = GameManager.Instance.MatchCamera.WorldToScreenPoint(target.transform.position);
        FirstPos = GameManager.Instance.UICamera.ScreenToWorldPoint(FirstPos);
        FirstPos.z = 0;

        //초기화
        sr.transform.position = FirstPos;
        sr.transform.localEulerAngles = Vector3.zero;
        sr.transform.localScale = Vector3.one;
        if (scale)
            sr.transform.DOScale(1.3f, 0.2f);

        //흔들거리는 애님.
        bool isAnd = false;
        Sequence seq = DOTween.Sequence();
        seq.Append(sr.transform.DORotate(new Vector3(0, 0, -10), 0.1f).SetEase(Ease.Linear));
        seq.Append(sr.transform.DORotate(new Vector3(0, 0, 10), 0.1f).SetEase(Ease.Linear).SetLoops(6, LoopType.Yoyo));
        seq.Append(sr.transform.DORotate(new Vector3(0, 0, 0), 0.1f).SetEase(Ease.Linear));
        seq.OnComplete(() =>
        {
            isAnd = true;
        });


        //흔들거릴때 조각
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_StelePiece, transform);

        //파티클이 뿌려질 영역을 설정한다. 이미지마다 크기가 달라서 다르게 정해줘야 한다.
        //또한 NGUI의 UI쪽 배율 때문인지 이미지 사이즈와 전혀 다른 수치라서 이미지와 파티클을 놓고 크기를 측정했따..
        switch (steletype)
        {
            case 1:
                obj.SetShapeBox(new Vector3(0.252f, 0.126f, 0));
                break;
            case 2:
                obj.SetShapeBox(new Vector3(0.126f, 0.252f, 0));
                break;
            case 3:
                obj.SetShapeBox(new Vector3(0.252f, 0.252f, 0));
                break;
            case 4:
                obj.SetShapeBox(new Vector3(0.378f, 0.252f, 0));
                break;
            case 5:
                obj.SetShapeBox(new Vector3(0.252f, 0.378f, 0));
                break;
        }
        obj.Play(sr.transform.position, 3); //블루색으로 나올꺼라 3번.. 나중에 따로 이펙트 이미지 추가되면 수정.

        while (true)
        {
            if (isAnd)
                break;
            yield return null;
        }

        if (scale)
            sr.transform.DOScale(1f, 0.2f);

        //날라가는 사운드
        SoundManager.Instance.PlayEffect("object_down");

        //목표 위치로 이동
        Vector3 EndPos = m_ListGoalItem[index].transform.position;

        Vector3 MiddlePos = new Vector3(0.9f, (FirstPos.y + EndPos.y) * 0.5f, 0) + GameManager.Instance.UICamera.transform.position;

        float speed = 0.020f;
        float Delta = 0f;
        float accel = 0f;

        //float MiddleTime = sequence;

        speed -= 0.003f * sequence;
        if (speed < 0)
            speed = 0;

        while (true)
        {
            accel = Time.deltaTime * 0.01f;//accel = 10 * Time.deltaTime / 1000;
            speed += accel;

            Delta += speed;

            sr.transform.localEulerAngles = new Vector3(0, 0, 360 * Delta);
            sr.transform.position = Bezier.Bezier2(FirstPos, MiddlePos, EndPos, Delta);

            if (Delta > 1)
                break;

            yield return null;
        }

        ObjectPool.Instance.Restore(sr.gameObject);
        action();
    }

    Color32 _orange = new Color32(255, 125, 0, 255);
    Color32 _orange_effect = new Color32(125, 60, 0, 255);
    Color32 _red = new Color32(255, 40, 40, 255);
    Color32 _red_effect = new Color32(125, 20, 20, 255);
    Color32 _yellow = new Color32(255, 255, 0, 255);
    Color32 _yellow_effect = new Color32(125, 125, 0, 255);
    Color32 _purple = new Color32(255, 0, 255, 255);
    Color32 _purple_effect = new Color32(125, 0, 125, 255);
    Color32 _blue = new Color32(0, 160, 255, 255);
    Color32 _blue_effect = new Color32(0, 80, 125, 255);
    Color32 _green = new Color32(16, 228, 16, 255);
    Color32 _green_effect = new Color32(8, 114, 8, 255);
    Color32 _white = Color.white;
    Color32 _white_effect = new Color32(56, 58, 58, 255);
    public void ScoreText(int Score, Vector3 pos, ColorType type)
    {
        UILabel label = ObjectPool.Instance.GetObject<UILabel>(PF_ScoreText, this.transform);
        //GameObject label = ObjectPool.Instance.GetObject(PF_ScoreText, this.transform);

        label.text = Score.ToString();

        switch (type)
        {
            case ColorType.ORANGE:
                label.color = _orange;
                label.gradientBottom = _orange_effect;
                label.effectColor = Color.white;
                break;
            case ColorType.RED:
                label.color = _red;
                label.gradientBottom = _red_effect;
                label.effectColor = Color.white;
                break;
            case ColorType.YELLOW:
                label.color = _yellow;
                label.gradientBottom = _yellow_effect;
                label.effectColor = Color.white;
                break;
            case ColorType.PURPLE:
                label.color = _purple;
                label.gradientBottom = _purple_effect;
                label.effectColor = Color.white;
                break;
            case ColorType.BLUE:
                label.color = _blue;
                label.gradientBottom = _blue_effect;
                label.effectColor = Color.white;
                break;
            case ColorType.GREEN:
                label.color = _green;
                label.gradientBottom = _green_effect;
                label.effectColor = Color.white;
                break;
            default:
                label.color = _white;
                label.gradientBottom = _white_effect;
                label.effectColor = Color.white;
                break;
        }
        //label.color = Color.white;

        Vector3 screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(pos);
        Vector3 uipos = GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos);
        label.transform.position = new Vector3(uipos.x, uipos.y, 0);
        label.transform.localScale = Vector3.zero; //아래 트윈에서 스케일준다.

        Sequence seq = DOTween.Sequence();
        seq.Append(label.transform.DOScale(1.2f, 0.5f).SetEase(Ease.OutCubic));
        seq.Append(label.transform.DOScale(1f, 0.3f).SetEase(Ease.Linear));
        seq.AppendInterval(2f);
        seq.Insert(0.5F, label.transform.DOMoveY(uipos.y + 0.1f, 0.4f).SetEase(Ease.OutQuad));
        seq.Insert(0.7F, DOTween.ToAlpha(() => label.color, _color => label.color = _color, 0f, 0.3F));
        seq.OnComplete(() =>
        {
            ObjectPool.Instance.Restore(label.gameObject);
        });
    }

    public void ComboText(int combo)
    {
        if (combo > 3)
        {
            Text_Ani textani = ObjectPool.Instance.GetObject<Text_Ani>(PF_ComboText, this.transform);

            switch (combo)
            {
                case 4:
                    textani.SetTextAni(0);
                    SoundManager.Instance.PlayEffect("compose_well_done");
                    break;
                case 5:
                    textani.SetTextAni(1);
                    SoundManager.Instance.PlayEffect("compose_perfect");
                    break;
                case 6:
                    textani.SetTextAni(2);
                    SoundManager.Instance.PlayEffect("compose_victory");
                    break;
                case 7:
                    textani.SetTextAni(3);
                    SoundManager.Instance.PlayEffect("compose_fantastic");
                    break;
                case 8:
                    textani.SetTextAni(4);
                    SoundManager.Instance.PlayEffect("compose_amazing");
                    break;
                default:
                    textani.SetTextAni(5);
                    SoundManager.Instance.PlayEffect("compose_follow me");
                    break;

            }
        }
    }

    public void ShufflingText()
    {
        Text_Ani textani = ObjectPool.Instance.GetObject<Text_Ani>(PF_ComboText, this.transform);
        textani.SetTextAni(6);
    }

    public void BonusTime()
    {
        if (Cheat.Instance.ShowLeftMove)
            Stage.text = Stage.text + " LeftMove : " + MatchMgr.m_CSD.limit_Move.ToString();

        EffectRestore bonustime = ObjectPool.Instance.GetObject<EffectRestore>(PF_BonusTime, this.transform);
        bonustime.transform.localScale = Vector3.one;
        bonustime.SetRestoreTime(1.1f);
    }

    //float movetime = 0.6f;
    float speed = 4f;
    
    public IEnumerator Co_BonusTimeEffect(Board board, bool truckbonus)
    {
        Vector3 screepos = GameManager.Instance.MatchCamera.WorldToScreenPoint(board.transform.position);
        Vector3 uiboardpos = GameManager.Instance.UICamera.ScreenToWorldPoint(screepos);

        Vector3 from;
        if (truckbonus)
            from = TruckPoint.position;
        else
            from = MoveLabel.transform.position;

        Vector3 to = new Vector3(uiboardpos.x, uiboardpos.y, 0);


        //셔클
        ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BonusCircle, this.transform);
        obj.Play(from, 0);

        //별
        GameObject bonusStar = ObjectPool.Instance.GetObject(PF_BonusStar, this.transform);
        bonusStar.gameObject.SetActive(true);
        bonusStar.transform.localScale = Vector3.one;
        bonusStar.transform.position = from;

        //Sequence seq = DOTween.Sequence();
        //seq.Append(bonusStar.transform.DOMove(to, movetime));
        //seq.Insert(0, bonusStar.transform.DOLocalRotate(new Vector3(0,0,720), movetime, RotateMode.LocalAxisAdd));
        //seq.OnComplete(() =>
        //{
        //    ObjectPool.Instance.Restore(bonusStar.gameObject);
        //});
        //yield return new WaitForSeconds(movetime);

        Vector3 dir = (to - from).normalized;

        float time = 0;
        while (time < 0.5f)
        {
            yield return new WaitForEndOfFrame();
            time += Time.deltaTime;

            if (bonusStar.transform.position.y <= to.y)
            {
                bonusStar.transform.position = to;
                break;
            }

            bonusStar.transform.localEulerAngles = bonusStar.transform.localEulerAngles + new Vector3(0, 0, 10);

            bonusStar.transform.position += dir * speed * Time.deltaTime;

        }

        yield return new WaitForEndOfFrame();

        //여기서 이펙트를 사라지게 하면 이펙트가 끊긴다.
        //이펙트의 EffectRestore컴포넌트에서 오브젝트 풀로 돌려준다.
        //ObjectPool.Instance.Restore(bonusStar.gameObject);
    }



    #endregion

    #region MoveWarning
    #endregion

    #region 점수 게이지
    float Start1_Score_Percent = 0f;    //3단계점수를 기준으로 점수상의 퍼센트 위치
    float Start2_Score_Percent = 0f;    //3단계점수를 기준으로 점수상의 퍼센트 위치
    //float Start3_Score_Percent = 1f;  //이값은 당연히 1이므로 생략
    float Start3_Gauge_Percent = 0f;    //전체 게이지 상에서 3단계별이 있는 퍼센트위치
    public void ScoreUIInit()
    {
        Star1.SetActive(false);
        Star2.SetActive(false);
        Star3.SetActive(false);

        ScoreGauge.fillAmount = 0;
        ScoreLabel.text = "0";

        //**********************
        //스코어 점수 위치 세팅
        //**********************
        float GaugeWidth = 136;             //게이지 길이.
        float LineHalfWidth = 0;            //이미지에 도착하면 달성으로 봐야한다. 그래서 작대기 넓이의 절반만큼을 추가 계산.
        float StartPos = -GaugeWidth/2;     //x가 0일때 중심.
        float EndPos = Score3_trm.localPosition.x - LineHalfWidth;
        float TotalWidth = EndPos - StartPos;

        if (MatchMgr.m_CSD.scoreStar3 == 0)
            return;

        Start1_Score_Percent = (float)MatchMgr.m_CSD.scoreStar1/ MatchMgr.m_CSD.scoreStar3;
        Score1_trm.localPosition = new Vector3( StartPos + TotalWidth* Start1_Score_Percent + LineHalfWidth, Score1_trm.localPosition.y, 0);

        Start2_Score_Percent = (float)MatchMgr.m_CSD.scoreStar2 / MatchMgr.m_CSD.scoreStar3;
        Score2_trm.localPosition = new Vector3(StartPos + TotalWidth * Start2_Score_Percent + LineHalfWidth, Score2_trm.localPosition.y, 0);


        //***************************
        //3단계 게이지퍼센트 세팅
        //***************************
        Start3_Gauge_Percent = (GaugeWidth/2 + EndPos)/GaugeWidth;


    }
    public void ScoreUpdate(int score)
    {    
        Sequence seq = DOTween.Sequence();
        seq.Append(ScoreLabel.transform.DOScale(1.2f, 0.07f));
        seq.Append(ScoreLabel.transform.DOScale(1f, 0.03f));
        seq.OnComplete(() =>
        {
            ScoreLabel.text = score.ToString();
        });

        //퍼센트계산
        float current_Percent = (float)score / MatchMgr.m_CSD.scoreStar3;

        //첫번째 별 세팅  
        if (current_Percent >= Start1_Score_Percent)
        { 
            if (!Star1.activeSelf)
            {
                Star1.SetActive(true);
                Star1.transform.localScale = Vector3.one * 2f;
                Star1.transform.DOScale(1,0.3f).SetEase(Ease.InQuad);
            }
            else
            {
                Star1.SetActive(true);
            }
        }

        //두번째 별 세팅
        if (current_Percent >= Start2_Score_Percent)
        {
            if (!Star2.activeSelf)
            {
                Star2.SetActive(true);
                Star2.transform.localScale = Vector3.one * 2f;
                Star2.transform.DOScale(1, 0.3f).SetEase(Ease.InQuad);
            }
            else
            {
                Star2.SetActive(true);
            }
        }

        //세번째 별 세팅
        if (current_Percent >= 1)
        {
            if (!Star3.activeSelf)
            {
                Star3.SetActive(true);
                Star3.transform.localScale = Vector3.one * 2f;
                Star3.transform.DOScale(1, 0.3f).SetEase(Ease.InQuad);
            }
            else
            {
                Star3.SetActive(true);
            }
        }

        //게이지세팅
        ScoreGauge.fillAmount = current_Percent * Start3_Gauge_Percent;
    }
#endregion

    #region 캐쉬아이템
    public enum CashItem
    {
        None = -1,
        Hammer = 0,
        Bomb,
        Lightning,
    }
    [NonSerialized]
    public CashItem SelectCashItem = CashItem.None;
    public void CashItemSelect(CashItem c_item)
    {
        if (MatchMgr.m_StepType != StepType.Wait)
            return;
        if (SelectCashItem == c_item)
            return;

        if (c_item != CashItem.None)
        {
            if (GameMgr.m_GameData.MyItemCnt[(int)c_item] <= 0)
            {
                //구매팝업.
                Popup_Manager.INSTANCE.popup_index = 15;
                Popup_Manager.INSTANCE.popup_list[15].GetComponent<ItemBuyPop>().itemkind = (int)c_item;
                Popup_Manager.INSTANCE.Popup_Push();

                return;
            }
        }


        SelectCashItem = c_item;

        switch (SelectCashItem)
        {
            case CashItem.None:
                {
                    UI_Block.SetActive(false);

                    MatchMgr.SetTouchState(TouchState.Switching);

                    UISprite[] spr = SelectCashItemObject.GetComponentsInChildren<UISprite>();
                    for (int i = 0; i < spr.Length; i++)
                    {
                        if (spr[i].depth >= 1001) //혹시라도 중복 적용?
                            spr[i].depth -= 1001;
                    }

                    UILabel[] lb = SelectCashItemObject.GetComponentsInChildren<UILabel>();
                    for (int i = 0; i < lb.Length; i++)
                    {
                        if (lb[i].depth >= 1001) //혹시라도 중복 적용?
                            lb[i].depth -= 1001;
                    }
                }
                break;
            case CashItem.Hammer:
            case CashItem.Bomb:
            case CashItem.Lightning:
                {  
                    //선택 연출
                    UI_Block.SetActive(true);
                    //터치 분기
                    MatchMgr.SetTouchState(TouchState.CashItemUse);

                    UISprite[] spr = SelectCashItemObject.GetComponentsInChildren<UISprite>();
                    for (int i = 0; i < spr.Length; i++)
                    {
                        if (spr[i].depth < 1001) //혹시라도 중복 적용?
                            spr[i].depth += 1001;
                    }
                    UILabel[] lb = SelectCashItemObject.GetComponentsInChildren<UILabel>();
                    for (int i = 0; i < lb.Length; i++)
                    {
                        if (lb[i].depth < 1001) //혹시라도 중복 적용?
                            lb[i].depth += 1001;
                    }
                }

                break;
            default:
                break;
        }
    }

    public void CashItemUse(Board board)
    {
        //사용 불가 조건
        if (SelectCashItem == CashItem.Lightning)
            if (board.IsItemExist == false || board.m_Item.m_Color == ColorType.None)
                return;

        StartCoroutine(C0_CashItemUse(board, SelectCashItem));

        CashItemUseApply();
    }

    //아이템 사용후 남은 갯수 저장
    public void CashItemUseApply()
    {
        GameMgr.m_GameData.MyItemCnt[(int)SelectCashItem]--;
        SecurityPlayerPrefs.SetInt("MyItem" + (int)SelectCashItem, GameMgr.m_GameData.MyItemCnt[(int)SelectCashItem]);

        CashItemUpdate((int)SelectCashItem);

        //이펙트가 켜져있을시 이펙트 종료.
        if (CashItemEffect[(int)SelectCashItem].activeInHierarchy)
            CashItemEffect[(int)SelectCashItem].SetActive(false);

        CashItemSelect(CashItem.None);
    }

    //UI 갱신
    public void CashItemUpdate(int index)
    {
        List_CashItemLabel[index].gameObject.SetActive(false);
        List_CashItemPlus[index].gameObject.SetActive(false);
        
        if(CashItemCovers[index].gameObject.activeSelf)
        {
            return;
        }

        if (GameMgr.m_GameData.MyItemCnt[index] > 0)
        {
            List_CashItemLabel[index].gameObject.SetActive(true);
            List_CashItemLabel[index].text = GameMgr.m_GameData.MyItemCnt[index].ToString();
        }
        else
        {
            List_CashItemPlus[index].gameObject.SetActive(true);
        }
    }

    //아이템 사용 연출
    IEnumerator C0_CashItemUse(Board board, CashItem c_item)//, System.Action Complete)
    {
        //아이템 사용 연출..
        switch (c_item)
        {
            case CashItem.None:
                break;
            case CashItem.Hammer:
                yield return StartCoroutine(Co_Hammer_Scene(board, () =>
                {
                    StartCoroutine(AbilityManager.Instance.Hammer(board, () =>
                    {
                        //패널도 터지면 MatchingStep으로 간다 여기서 할필요없다.
                        //추후 문제 없으면 삭제!!
                        //MatchMgr.SetStep(StepType.Matching);
                    }));
                }));
                //Analytics
                CashUseCnt[(int)CashItem.Hammer]++;            

                break;
            case CashItem.Bomb:
                yield return StartCoroutine(Co_Bomb_Scene(board, () =>
                {
                    StartCoroutine(AbilityManager.Instance.CrossBomb(board, () =>
                    {
                        //MatchMgr.SetStep(StepType.Matching);
                    }));
                }));
                //Analytics
                CashUseCnt[(int)CashItem.Bomb]++;

                break;
            case CashItem.Lightning:
                yield return StartCoroutine(Co_Thunder_Scene(board, () =>
                {
                    StartCoroutine(AbilityManager.Instance.ColorThunder(board, () =>
                    {
                        //MatchMgr.SetStep(StepType.Matching);
                    }));
                }));
                //Analytics
                CashUseCnt[(int)CashItem.Lightning]++;

                break;
            default:
                break;
        }
    }


    public IEnumerator Co_Hammer_Scene(Board board, System.Action ability)
    {
        Animation anim = ObjectPool.Instance.GetObject<Animation>(PF_Cash_Hammer, this.transform);
        anim.transform.localScale = new Vector3(1f, 1f, 1f);

        Vector3 screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(board.transform.position);
        Vector3 uipos = GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos);

        anim.transform.position = new Vector3(uipos.x, uipos.y, 0);

        //anim.transform.localPosition += new Vector3(200, -100, 0);

        anim.Play();

        yield return new WaitForSeconds(0.55f);
        ability();

        while (anim.isPlaying)
        {
            yield return null;
        }

        ObjectPool.Instance.Restore(anim.gameObject);
    }

    public IEnumerator Co_Bomb_Scene(Board board, System.Action ability)
    {
        Animation anim = ObjectPool.Instance.GetObject<Animation>(PF_Cash_Bomb, this.transform);
        anim.transform.localScale = new Vector3(1f, 1f, 1f);

        Vector3 screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(board.transform.position);
        Vector3 uipos = GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos);

        anim.transform.position = new Vector3(uipos.x, uipos.y, 0);

        anim.transform.localPosition += new Vector3(0, 15, 0);

        anim.Play();

        yield return new WaitForSeconds(0.35f);
        ability();

        ObjectPool.Instance.Restore(anim.gameObject);

    }

    public IEnumerator Co_Thunder_Scene(Board board, System.Action ability)
    {
        Animation anim = ObjectPool.Instance.GetObject<Animation>(PF_Cash_Thunder, this.transform);
        anim.transform.localScale = new Vector3(1f, 1f, 1f);

        Vector3 screenpos = GameManager.Instance.MatchCamera.WorldToScreenPoint(board.transform.position);
        Vector3 uipos = GameManager.Instance.UICamera.ScreenToWorldPoint(screenpos);

        anim.transform.position = new Vector3(uipos.x, uipos.y, 0);

        anim.transform.localPosition += new Vector3(0, 15, 0);

        anim.Play();

        yield return new WaitForSeconds(0.35f);
        ability();

        ObjectPool.Instance.Restore(anim.gameObject);
    }
    #endregion

    #region 광고 버튼(월드맵에서 소스,버튼오브젝트 복사함)
    public void FreeCoinSetting()
    {
        if (AdManager.Instance.IsAdReady)
        {
            if (TimeManager.Instance.freecoinchk)
            {
                //비활성화
                freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

                freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

                freecoineffect.gameObject.SetActive(false);

                freecointime.gameObject.SetActive(true);
            }
            else
            {
                freecoindisplay();  //활성화
            }
        }
        else
        {
            //비활성화
            freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

            freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

            freecoineffect.gameObject.SetActive(false);
        }

    }

    //1.보상받고 비활성화 시킬때
    public void VideoRewardResultSetting()
    {
        TimeManager.Instance.freecoinchk = true;

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

        freecoineffect.gameObject.SetActive(false);

        freecointime.gameObject.SetActive(true);
    }
    //2.보상받고 비활성화 시킬때
    public void UnityAdsRewardResult()
    {


        GameManager.Instance.m_adsRewardchk = false;

        long cooltime = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].cooltime;


        long time = DateTime.Now.AddSeconds(cooltime).Ticks;


        TimeManager.Instance.m_freecointime = time;


        string cooltimestr = TimeManager.Instance.RefreshTimer(TimeManager.Instance.m_freecointime, 3);

        //텍스트갱신 1.월드맵
        if (WorldMapUI.instance != null)
            WorldMapUI.instance.freecointimedisplay(cooltimestr);
        //텍스트갱신 2.샵
        Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().freecointimedisplay(cooltimestr);
        //텍스트갱신 3.인게임버튼
        if (UI_ThreeMatch.Instance != null)
            UI_ThreeMatch.Instance.freecointimedisplay(cooltimestr);

        switch (TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].type)
        {
            case 0://골드
                {

                    GoldSetting(TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].reward);

                    GameManager.Instance.m_adsRewardchk = true;
                    Popup_Manager.INSTANCE.popup_index = 0;

                    Popup_Manager.INSTANCE.Popup_Push();
                }
                break;
            case 1://아이템
                {
                    GameManager.Instance.m_adsRewardchk = true;

                    Popup_Manager.INSTANCE.popup_index = 0;

                    Popup_Manager.INSTANCE.Popup_Push();

                    /*
                    int itemindex = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].kind - 1;
                    GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                    SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                     * */
                }
                break;
        }


        SecurityPlayerPrefs.SetLong("FreeCoinTime", TimeManager.Instance.m_freecointime);


    }

    public void freecoindisplay()
    {
        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", false);

        freecoineffect.gameObject.SetActive(true);

        freecointime.gameObject.SetActive(false);

    }


    //보상형 광고 클릭
    public bool InGameAdsClick = true;
    public void freecoinclick()
    {
        if (AdManager.Instance.IsAdReady)
        {
            if (!TimeManager.Instance.freecoinchk)
            {
                InGameAdsClick = true;
                AdManager.AdsRewardResult = null;
                AdManager.Instance.Ad_Show("InGame");
            }
        }
        else
        {
            Popup_Manager.INSTANCE.popup_index = 33;
            Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(199));
            Popup_Manager.INSTANCE.Popup_Push();
        }
    }

    //public void freecoinlistclick()
    //{
    //    Popup_Manager.INSTANCE.popup_index = 34;

    //    Popup_Manager.INSTANCE.Popup_Push();
    //}

    public void freecointimedisplay(string time)
    {
        freecointime.text = time;
    }

    public void GoldSetting(int rewardmoney)
    {
        //nowMoney = GameManager.Instance.m_GameData.MyGold;

        GameManager.Instance.m_GameData.MyGold += rewardmoney;

        //addmoney = rewardmoney;

        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

        //if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.WorldMap)
        //{
        //    GameManager.Instance.m_isTouch = false; //moneyadd()코루틴에서 true
        //    dailyrewardgold.gameObject.SetActive(true);
        //}

    }

    public void OnClickFreeCashItemButton()
    {
        if (MatchMgr.m_StepType == StepType.Wait && AdManager.Instance.IsAdReady)
        {
            AdManager.AdsRewardResult = () =>
            {
                Board board = MatchMgr.GetNormalItemBoards(1)[0];

                if (board != null)
                {
                    ItemType itemType = (ItemType)UnityEngine.Random.Range((int)ItemType.Line_X, (int)ItemType.Rainbow + 1);
                    Item item_Obj = ItemManager.Instance.CreateItem(itemType).GetComponent<Item>();
                    item_Obj.Init(null, itemType, itemType == ItemType.Rainbow ? ColorType.None : ColorType.Rnd);
                    item_Obj.transform.position = Vector3.zero;

                    float duration = 0.4f;

                    FreecoinObj.SetActive(false);

                    Popup_Manager.INSTANCE.popup_index = 41;
                    OneButtonPopup.TitleIndex = 18;
                    OneButtonPopup.NoticeIndex = -1;
                    OneButtonPopup.Count = 1;
                    OneButtonPopup.ImageName = GetItemTypeToSpritName(itemType, item_Obj.m_Color);
                    OneButtonPopup.OKButtonAction = () =>
                    {
                        Sequence seq = DOTween.Sequence();
                        seq.Insert(0f, item_Obj.transform.DOScale(2f, duration).SetEase(Ease.OutCubic));
                        seq.Insert(duration, item_Obj.transform.DOScale(1f, duration).SetEase(Ease.InCubic));
                        seq.Insert(0f, item_Obj.transform.DOMove(board.transform.position + new Vector3(0, 0, -1f), duration * 2).SetEase(Ease.Linear)); //패널들보다 위로 날라가야한다.
                        seq.OnComplete(() =>
                        {
                            ObjectPool.Instance.Restore(item_Obj.gameObject);
                            board.GenItem(itemType, item_Obj.m_Color);
                            board.m_MatchingCheck = true;
                            MatchMgr.SetStep(StepType.Matching);
                            MatchMgr.m_Step.Step_Process();
                        });
                    };

                    Popup_Manager.INSTANCE.Popup_Push();
                }

                GameManager.Instance.m_adsRewardchk = true;


                //캐쉬아이템 제공 로직
                //int itemindex = UnityEngine.Random.Range(0, 3);
                //GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                //SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                //List_CashItemLabel[itemindex].text = GameMgr.m_GameData.MyItemCnt[itemindex].ToString();

                //Popup_Manager.INSTANCE.popup_index = 41;
                //OneButtonPopup.TitleIndex = 18;
                //OneButtonPopup.NoticeIndex = -1;
                //OneButtonPopup.Count = 1;
                //OneButtonPopup.ImageName = "item_0" + (itemindex + 1);
                //Popup_Manager.INSTANCE.Popup_Push();
            };

            AdManager.Instance.Ad_Show("FreeCashItem");
        }
    }

    private string GetItemTypeToSpritName(ItemType itemType, ColorType colorType)
    {
        switch (itemType)
        {
            case ItemType.Line_X:
                return "item_0" + (int)colorType + "_line_x";
            case ItemType.Line_Y:
                return "item_0" + (int)colorType + "_line_y";
            case ItemType.Line_C:
                return "item_0" + (int)colorType + "_line_c";
            case ItemType.Bomb:
                return "item_0" + (int)colorType + "_bomb";
            case ItemType.Rainbow:
                return "item_rainbow";
        }

        return string.Empty;
    }
    #endregion

    #region 푸드트럭 능력

    bool isCutScene = false;
    public IEnumerator FoodTruck_CutScene(TruckAbilityType type)
    {
        //다른컷씬이 진행중이면 기다려야한다.
        while (isCutScene)
        {
            yield return null;
        }

        FoodTruckinfo truckinfo = TableDataManager.Instance.m_Foodtruck[CurrentTruckGrade];// GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade];

        bool isAbility = false;
        switch (type)
        {
            case TruckAbilityType.MovePlus:
                if(truckinfo.moveplus > 0)
                    isAbility = true;
                break;

            case TruckAbilityType.ScorePlus:
                if (truckinfo.scoreplus > 0)
                    isAbility = true;
                break;

            case TruckAbilityType.BonusPlus:
                if (truckinfo.bonusplus > 0)
                    isAbility = true;
                break;

            case TruckAbilityType.ButterFlyPlus:
                if (truckinfo.butterflyplus > 0)
                    isAbility = true;
                break;

            default:
                break;
        }

        //트럭에 없는 효과는 진행 안함.
        if (isAbility == false)
            yield break;

        isCutScene = true;

        //터치 불가
        MatchMgr.SetTouchState(TouchState.NotTouch);

        //만약에 무브1남았을때 클리어 해서 무브 플러스다음에 바로 다른 효과나오게 되면 트럭 애니메이션 멈춰야함.
        m_TruckSprite.GetComponent<Animation>().Stop();

        Sequence seq = DOTween.Sequence();
        //상단에서 좌측으로 이동
        float _movetime1 = 0.7f;
        seq.Append(m_TruckSprite.transform.DOLocalMoveX(-400, _movetime1).SetEase(Ease.InQuart));

        //중간에서 우측 시작, +스케일
        seq.AppendCallback(() =>
        {
            m_TruckSprite.transform.localPosition = new Vector3(400, -380, 0);
            m_TruckSprite.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        });
        seq.AppendInterval(0.01f); //인터벌을 안주면 AppendCallback이 아래 Append보다 늦게 호출된다;;;

        //중간우측에서 중앙으로 이동
        float _movetime2 = 0.5f;       
        seq.Append(m_TruckSprite.transform.DOLocalMoveX(0, _movetime2).SetEase(Ease.OutQuart));

        //효과 텍스트이미지
        seq.AppendCallback(() =>
        {
            TruckAbilityText(type);
        });
        seq.AppendInterval(0.5f);

        //중앙에서 무브 추가
        seq.AppendCallback(() =>
        {
            switch (type)
            {
                case TruckAbilityType.MovePlus:
                    StartCoroutine(FoodTruck_MovePlus(truckinfo.moveplus));
                    break;
                case TruckAbilityType.ScorePlus:
                    //스코어는 BonusPlus와 중복되는 연출상의 문제로 직접호출한다.
                    //여기서 호출되지 않는다는걸 표시하는 의미로 주석처리함.
                    //StartCoroutine(FoodTruck_ScorePlus(truckinfo.scoreplus));
                    break;
                case TruckAbilityType.BonusPlus:
                    StartCoroutine(FoodTruck_BonusPlus(truckinfo.bonusplus));
                    break;
                case TruckAbilityType.ButterFlyPlus:
                    FoodTruck_ButterFlyPlus(truckinfo.butterflyplus);
                    break;

                default:
                    break;
            }
            
        });

        //효과를 적용하는 시간을 기다린다.(정확히는 이펙트 연출 시간)
        seq.AppendInterval(0.5f);

        //중앙에서 좌측으로 사라짐
        float _movetime3 = 0.5f;
        seq.Append(m_TruckSprite.transform.DOLocalMoveX(-450, _movetime3).SetEase(Ease.InQuart));

        ////상단 우측으로 이동
        seq.AppendCallback(() =>
        {
            m_TruckSprite.transform.localPosition = new Vector3(350, 0, 0);
            m_TruckSprite.transform.localScale = new Vector3(1f, 1f, 1f);

            //터치 가능
            MatchMgr.SetTouchState(TouchState.Switching);
        });
        seq.AppendInterval(0.01f); //인터벌을 안주면 AppendCallback이 아래 Append보다 늦게 호출된다;;;

        //상단우측에서 상당 중앙으로 이동
        float _movetime4 = 0.5f;
        seq.Append(m_TruckSprite.transform.DOLocalMoveX(0, _movetime4).SetEase(Ease.OutQuart));
        seq.AppendCallback(() =>
        {
            m_TruckSprite.GetComponent<Animation>().Play();
            isCutScene = false;
        });

        yield return new WaitForSeconds(_movetime1 + _movetime2 + _movetime3 + _movetime4);


    }

    public IEnumerator FoodTruck_MovePlus(int starcnt)
    {
        //추가 무브숫자 날라가는 연출
        Vector3 startpos = TruckPoint.position;
        Vector3 Despos = MoveLabel.transform.position;
        float delta = 0.5f / starcnt;

        for (int i = 0; i < starcnt; i++)
        {
            //셔클
            ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BonusCircle, this.transform);
            obj.Play(startpos, 0);

            //별(라인으로 수정)
            GameObject bonusStar = ObjectPool.Instance.GetObject(PF_BonusStar, this.transform);
            bonusStar.gameObject.SetActive(true);
            bonusStar.transform.localScale = Vector3.one;
            bonusStar.transform.position = startpos;

            //곡선 그리며 이동 하기
            Vector3 _dir = (Despos - startpos).normalized;
            Vector3 OrthoVector = Vector3.zero;
            Vector3.OrthoNormalize(ref _dir, ref OrthoVector);          //목표방향에 수직인 벡터를 구함.

            int ran = UnityEngine.Random.Range(1,4) * 8;
            if(i%2 == 0) ran *= -1;

            Sequence seq = DOTween.Sequence();
            seq.Append(bonusStar.transform.DOBlendableMoveBy(Despos - startpos, 0.5f)).SetEase(Ease.Linear);
            seq.Insert(0f, bonusStar.transform.DOBlendableMoveBy(OrthoVector / ran, 0.1f));
            seq.Insert(0.1f, bonusStar.transform.DOBlendableMoveBy(-OrthoVector / ran, 0.4f));
            //seq.Insert(0, bonusStar.transform.DOScale(0.7f, 0.5f)).SetEase(Ease.InQuart);
            //seq.Insert(0, bonusStar.transform.DOLocalRotate(new Vector3(0, 0, 720), 0.3f, RotateMode.LocalAxisAdd));
            seq.OnComplete(() =>
            {
                //여기서 이펙트를 사라지게 하면 이펙트가 끊긴다.
                //이펙트의 EffectRestore컴포넌트에서 오브젝트 풀로 돌려준다.
                //ObjectPool.Instance.Restore(bonusStar.gameObject);

                //****************
                //무브 횟수 증가
                //****************
                MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.TruckStat, 1);

                //사운드
                SoundManager.Instance.PlayEffect("tm");
            });
            yield return new WaitForSeconds(delta);
        }
    }

    public IEnumerator FoodTruck_ScorePlus(int score = 0)
    {
        //******************* Start 스코어플러스만 예외처리 영역 *******************
        //스코어 플러스는 보너스플러스와 연속처리되는 문제로 인해 트럭이 움직이지 않는 상태에서 적용된다.
        //그래서 스코어플러스는 이 함수를 직접 호출하므로 FoodTruck_CutScene()에서 하는 부분이 추가되어 있다.
        while (isCutScene)
        {
            yield return null;
        }

        bool ing = false;
        FoodTruckinfo truckinfo = TableDataManager.Instance.m_Foodtruck[CurrentTruckGrade]; //GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade];

        if (truckinfo.scoreplus > 0)
        {
            ing = true;

            m_TruckSprite.GetComponent<Animation>().Play();

            //텍스트 이미지 애니메이션
            TruckAbilityText(TruckAbilityType.ScorePlus);

            yield return new WaitForSeconds(0.5f);
       //******************* End 스코어플러스만 예외처리 영역 *******************

            //추가 무브숫자 날라가는 연출
            Vector3 startpos = TruckPoint.position;
            Vector3 Despos = ScoreLabel.transform.position;

            //셔클
            ParticleManager obj = ObjectPool.Instance.GetObject<ParticleManager>(PF_BonusCircle, this.transform);
            obj.Play(startpos, 0);

            //숫자
            UILabel label = ObjectPool.Instance.GetObject<UILabel>(PF_TruckText, TruckPoint);
            label.transform.position = startpos;
            label.transform.localPosition += new Vector3(0, -50, 0);
            label.transform.localScale = Vector3.zero; //아래 트윈에서 스케일준다.
            label.text = truckinfo.scoreplus.ToString();

            Sequence seq = DOTween.Sequence();
            seq.Append(label.transform.DOScale(1.5f, 0.3f).SetEase(Ease.OutCubic));
            seq.Append(label.transform.DOScale(0.5f, 0.2f).SetEase(Ease.Linear));
            seq.Insert(0.3F, label.transform.DOMove(Despos, 0.4f).SetEase(Ease.InCubic));
            //seq.Insert(0.7F, DOTween.ToAlpha(() => label.color, _color => label.color = _color, 0f, 0.3F));
            seq.OnComplete(() =>
            {
                ObjectPool.Instance.Restore(label.gameObject);

            //**********
            //점수 추가
            //**********
            MatchMgr.ScoreTextShow(truckinfo.scoreplus, null, ColorType.None);

                ing = false;
            });
        }

        while (ing)
            yield return null;

        yield return null;
    }

    public IEnumerator FoodTruck_BonusPlus(int bonusplus)
    {
        //트럭에서 직접 별을 날린다.
        for (int i = 0; i < bonusplus; i++)
        {
            Board bd = MatchMgr.GetNormalItemBoard();
            if (bd != null)
            {
                StartCoroutine(MatchMgr.CreateBonusItem(bd, true));

                //yield return new WaitForSeconds(0.15F);
            }
        }
        yield return null;
    }

    public void FoodTruck_ButterFlyPlus(int bonusplus)
    {
        var ItemTypes = new ItemType[bonusplus];
        var ColorTypes = new ColorType[bonusplus];

        for (int i = 0; i < bonusplus; ++i)
        {
            ItemTypes[i] = ItemType.Butterfly;
            ColorTypes[i] = GetAppearColorType();
        }

        MatchManager.Instance.Truck_BonusItem(ItemEffect,/*FoodTruckEffectDelay*/ 0f, ItemTypes, ColorTypes);

    }
    private void ItemEffect(List<Board> boards)
    {
        for (int i = 0; i < boards.Count; ++i)
        {
            StartCoroutine(Co_BonusTimeEffect(boards[i], true));

        }

    }

    private ColorType GetAppearColorType()
    {
        return MatchManager.Instance.m_AppearColor[UnityEngine.Random.Range(0, MatchManager.Instance.m_AppearColor.Count)];
    }

    public void TruckAbilityText(TruckAbilityType type)
    {
        //사운드
        SoundManager.Instance.PlayEffect("plus");

        //텍스트 이미지 애니메이션
        Anim_Text anim_text = ObjectPool.Instance.GetObject<Anim_Text>(PF_Anim_Text, this.transform);
        anim_text.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f); //NGU으로 밑으로 들어가면서 발생하는 스케일 문제 and 1.3배 확대
        anim_text.transform.position = m_TruckSprite.transform.position;
        switch (type)
        {
            case TruckAbilityType.MovePlus:
                anim_text.transform.localPosition += new Vector3(0, 300, 0);
                anim_text.transform.localScale = Vector3.one;
                anim_text.SetSprite("move plus");
                break;
            case TruckAbilityType.ScorePlus:
                anim_text.transform.localPosition += new Vector3(0, 80, 0);
                anim_text.SetSprite("score plus");
                break;
            case TruckAbilityType.BonusPlus:
                anim_text.transform.localPosition += new Vector3(0, 300, 0);
                anim_text.transform.localScale = Vector3.one;
                anim_text.SetSprite("bonus plus");
                break;

            case TruckAbilityType.ButterFlyPlus:
                anim_text.transform.localPosition += new Vector3(0, 300, 0);
                anim_text.transform.localScale = Vector3.one;
                anim_text.SetSprite("bonus plus");
                break;
            default:
                break;
        }
    }
    #endregion

    #region 아이템 능력 연출
    public void JellyMon_Select(bool var)
    {
        UI_Block.SetActive(var);
        UI_JellyMonText.SetActive(var);
    }
#endregion
}
