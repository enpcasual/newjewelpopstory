﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BoardInfo
{
    public bool touch = false;
    public Vector2 coordinate;
    public Vector2 addPos;
}

[System.Serializable]
public class TargetInfo
{
    public bool touch = false;
    public bool arrow = false;
    public UIType type;
    public int index;
    public Vector2 bound;
}

[System.Serializable]
public class TutorialTarget
{
   public int Stage;
   public int Page;
   public int Index;
   public  GameObject Obj;
}

public enum TutorialCharacterPosition
{
    None,
    Left,
    Right
}



public class TutorialInfo : MonoBehaviour {

    public int m_Stage;
    public int m_Page;
    public List<BoardInfo> List_MaskBoard;
    public List<TargetInfo> List_MaskTarget;
    public TutorialCharacterPosition CharacterPosition = TutorialCharacterPosition.Right;

    [Range(340f, -400f)]
    public float m_TalkBox_Hight;
    public bool m_ArrowBottom = false;
    public bool m_BtnBottom = false;
    //[TextArea]
    public int m_TextTableIndex;

    public List<ColorType> List_Seed;
}
