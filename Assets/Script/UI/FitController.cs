﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UIRoot))]
public class FitController : MonoBehaviour
{
    //기본은 Width. 
    //하지만 Height로 대응할 해상도 비율을 추가.
    public List<Vector2> m_ListPPI_Ratio;
    UIRoot uiroot;

    void Awake()
    {
        uiroot = transform.GetComponent<UIRoot>();
        uiroot.fitWidth = true;
        uiroot.fitHeight = false;

        float device = (float)Screen.width / Screen.height;
        float ppi = 0f;
        for (int i = 0; i < m_ListPPI_Ratio.Count; i++)
        {
            ppi = m_ListPPI_Ratio[i].x / m_ListPPI_Ratio[i].y;
            if (device == ppi)
            {
                uiroot.fitWidth = false;
                uiroot.fitHeight = true;
                break;
            }
        }

    }
}
