﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum UIType
{
    WorldMap,
    CarPort,
    ThreeMatch,
    Popup,
}

public class TutorialManager : MonoBehaviour {

    [HideInInspector]
    public static TutorialManager Instance;
    [HideInInspector]
    public MatchManager MatchMgr;
    //**************
    //튜토리얼 정보
    //*************
    public List<TutorialInfo> List_TutoInfo;
    public TutorialInfo CurrentInfo;

    //기본 UI콤보넌트
    public GameObject Root;
    public UISprite Finger;
    public GameObject Arrow;
    public UISprite TalkBox;
    public GameObject Btn_Next;
    public GameObject Btn_Skip;

    public UISprite Character;
    public Transform TextBox;

    //추가 생성 콤보넌트
    public GameObject PF_Mask;
 
    //변수
    [HideInInspector]
    public bool isTutorial = false;
    [HideInInspector]
    public List<TutorialInfo> List_CurrentInfo = new List<TutorialInfo>();
    [HideInInspector]
    public int CurrentInfoIndex = 0;
    [HideInInspector]
    public List<GameObject> List_Mask;
    public object seq_id;

    [SerializeField]
    private List<int> TutorialItemUnlockStage = new List<int>();

    private bool isDirty = false;

    public void Awake()
    {
        Instance = this;
        List_TutoInfo.RemoveAll((temp) => { return temp == null; });
    }

    public void Tutorial_Init(bool pageinit = false)
    {
        int copiedStage = -1;
        if (CurrentInfo != null)
            copiedStage = CurrentInfo.m_Stage;

        if (pageinit == false)
        {
            List_CurrentInfo.Clear();
            CurrentInfo = null;
            CurrentInfoIndex = 0;
            isTutorial = false;

            if(!GravityDisplayer.useDefaultGravity)
            {
                MatchMgr.ShowGravity();
            }
        }

        if (isDirty == false)
            return;

        MatchManager.SwitchingApply -= Tutorial_End;

        Root.SetActive(false);
        Btn_Skip.SetActive(false);
        Btn_Next.SetActive(false);
        DOTween.Kill(Arrow.transform);
        Arrow.SetActive(false);
        Finger_Seq.Kill();
        Finger.gameObject.SetActive(false);

        //Mask 정리
        for (int i = 0; i < List_Mask.Count; i++)
        {
            ObjectPool.Instance.Restore(List_Mask[i]);
        }

        List_Mask.Clear();

        SetitemCoverEffect(copiedStage);

        isDirty = false;
        Debug.Log("::::::::::::::::::::::::Tutorial Init ::::::::::::::::::::::::::::");
    }

    private void SetItemCoverData(int stage)
    {
        ApplyItemCover(stage, true, false);
    }

    private void SetitemCoverEffect(int stage)
    {
        ApplyItemCover(stage, false, true);
    }

    private void ApplyItemCover(int stage, bool data, bool effect)
    {
        for (int i = 0; i < TutorialItemUnlockStage.Count; ++i)
        {
            if (stage == TutorialItemUnlockStage[i])
            {
                if (data)
                {
                    SecurityPlayerPrefs.SetBool("ItemCovers" + i, false); //cover를 제거하여 아이템 사용할 수 있게 활성화.

                    if (GameManager.Instance != null)
                    {
                        GameManager.Instance.m_GameData.CashItemCover[i] = (true);
                    }
                }
                if(effect)
                {
                    UI_ThreeMatch.Instance.TutorialCashItemInit(i);
                }
            }
        }
    }

    #region 튜토리얼 데이타(게임중에 데이타를 세팅할 필요가 있을때)
    public TutorialInfo GetTutorialInfo(int stage, int page)
    {
         return List_TutoInfo.Find(find => find.m_Stage == stage && find.m_Page == page);
    }
    #endregion

    //스테이지 마다 초기화 해야할것들
    public void Tutorial_Stage_Init()
    {
        JellyMon.firstFullJellyMon = true;
    }

    public bool Tutorial_Start(int stage, int start_page = 0, int end_page = 10)
    {
        MatchMgr = MatchManager.Instance; //처음에 MatchManger는 꺼져있을테니 일단 여기서 한다.

        SetItemCoverData(stage);

        List_CurrentInfo = List_TutoInfo.FindAll(find => find.m_Stage == stage && find.m_Page >= start_page && find.m_Page <= end_page);
        if (List_CurrentInfo.Count == 0)
        {
            SetitemCoverEffect(stage);
            return false;
        }
        else
        {
            Tutorial_Set(List_CurrentInfo);

            //GoogleAnalytics_Manager.Instance.LogScreen("Tutorial ", stage);
            return true;
        }

    }

    Sequence Finger_Seq;
    public void Tutorial_Set(List<TutorialInfo> list_lnfo)
    {
        if (CurrentInfoIndex >= list_lnfo.Count)
        {
            Tutorial_Init();
            return;
        }
        CurrentInfo = list_lnfo[CurrentInfoIndex];
        CurrentInfoIndex++;
        isTutorial = true;
        Root.SetActive(true);

        //사운드
        SoundManager.Instance.PlayEffect("popup");

        //Character.cachedGameObject.SetActive(CurrentInfo.CharacterPosition != TutorialCharacterPosition.None);
        //Character.cachedTransform.localPosition = new Vector3(CurrentInfo.CharacterPosition == TutorialCharacterPosition.Right ? 214 : -214, 11.5f, 0);
        //Character.flip = CurrentInfo.CharacterPosition == TutorialCharacterPosition.Right ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;

        //TextBox.transform.localPosition = new Vector3(CurrentInfo.CharacterPosition == TutorialCharacterPosition.Right ? -94 : 94, 42, 0);

        //TextBox 세팅
        TalkBox.transform.localPosition = new Vector3(-700, CurrentInfo.m_TalkBox_Hight, 0);
        TalkBox.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.OutSine);

        UILabel label = TalkBox.GetComponentInChildren<UILabel>();
        label.text = TableDataManager.Instance.GetLocalizeText(CurrentInfo.m_TextTableIndex);


        List<Vector3> List_Touch = new List<Vector3>();

        //MaskBoard 세팅
        float width = 70.3f;   //여기 문제가 좀 있다. 72 * 9를 하면640이 넘어간다. 그래서 게임쪽 카메라가 더 넓게 잡고 있다. 그말은 즉, 이미지크기(72)가 화면 픽셀과 맞지 않다. 그래서 UI쪽에서 72로 계산하면 사이즈가 맞지 않게 된다. 대충 수동으로 잡아주자.
        float offset_x = MatchMgr.GameField.transform.position.x / 0.72f; //글로벌 좌표에서 칸수를 구한후
        float offset_y = MatchMgr.GameField.transform.position.y / 0.72f; //아래에서는 로컬좌표로 곱해준다.
        float mask_offset = width / 2; //마스크의 앵커가 BottomLeft라서 Center로 바꿔줘야한다. 중요!!!
        
        for (int i = 0; i < CurrentInfo.List_MaskBoard.Count; i++)
        {
            UIMask _mask = ObjectPool.Instance.GetObject<UIMask>(PF_Mask, Root.transform);
            _mask.transform.localScale = Vector3.one;
            _mask.transform.localPosition = Vector3.zero;
            _mask.transform.localPosition += new Vector3(((int)CurrentInfo.List_MaskBoard[i].coordinate.x + offset_x) * width - mask_offset + CurrentInfo.List_MaskBoard[i].addPos.x, (-(int)CurrentInfo.List_MaskBoard[i].coordinate.y + offset_y) * width - mask_offset + -(CurrentInfo.List_MaskBoard[i].addPos.y), 0);
            _mask.size = new Vector2(width, width);

            _mask.Build();

            List_Mask.Add(_mask.gameObject);

            //아이템 위치 변경(터치를 위해 콜라이더보다 앞으로 와야함)
            if (CurrentInfo.List_MaskBoard[i].touch)
            {
                Board _bd = MatchMgr.GetBoard((int)CurrentInfo.List_MaskBoard[i].coordinate.x, (int)CurrentInfo.List_MaskBoard[i].coordinate.y);
                var targetItem = _bd.m_Item != null ? _bd.m_Item.transform : _bd.transform;

                targetItem.DOKill(true);
                targetItem.position = new Vector3(targetItem.position.x, targetItem.position.y, Root.transform.position.z - 1);

                //손가락 이동 좌표 추가..블럭은 UI와 좌표가 다르므로 마스크의 좌표를 추가. offset은 다시 빼줘야함.
                List_Touch.Add(_mask.transform.localPosition + new Vector3(mask_offset, mask_offset, -10));
            }
        }

        //MaskTarget 세팅
        for (int i = 0; i < CurrentInfo.List_MaskTarget.Count; i++)
        {
            TutorialTarget Target = null;
            switch (CurrentInfo.List_MaskTarget[i].type)
            {
                case UIType.WorldMap:
                    break;
                case UIType.CarPort:
                    break;
                case UIType.ThreeMatch:
                    Target = UI_ThreeMatch.Instance.List_Tutorial_Target.Find(find => find.Stage == CurrentInfo.m_Stage && find.Page == CurrentInfo.m_Page && find.Index == CurrentInfo.List_MaskTarget[i].index);
                    break;
                case UIType.Popup:
                    Target = Popup_Manager.INSTANCE.List_Tutorial_Target.Find(find => find.Stage == CurrentInfo.m_Stage && find.Page == CurrentInfo.m_Page && find.Index == CurrentInfo.List_MaskTarget[i].index);
                    break;
            }
            if (Target != null)
            {
                Transform _trm = Target.Obj.transform;

                UIMask _mask = ObjectPool.Instance.GetObject<UIMask>(PF_Mask, Root.transform);
                _mask.transform.localScale = Vector3.one;
                _mask.transform.position = new Vector3(_trm.position.x, _trm.position.y, Root.transform.position.z);
                _mask.transform.localPosition += new Vector3(-CurrentInfo.List_MaskTarget[i].bound.x / 2, -CurrentInfo.List_MaskTarget[i].bound.y / 2, 0); //Mask offset 처리
                _mask.size = new Vector2(CurrentInfo.List_MaskTarget[i].bound.x, CurrentInfo.List_MaskTarget[i].bound.y);

                _mask.Build();

                List_Mask.Add(_mask.gameObject);

                //아이템 위치 변경(터치를 위해 콜라이더보다 앞으로 와야함)
                if (CurrentInfo.List_MaskTarget[i].touch)
                {
                    _trm.position = new Vector3(_trm.position.x, _trm.position.y, Root.transform.position.z - 1);

                    //블럭은 UI와 좌표가 다르므로 마스크의 좌표를 추가. offset은 다시 빼줘야함.
                    List_Touch.Add(_mask.transform.localPosition + new Vector3(CurrentInfo.List_MaskTarget[i].bound.x/2, CurrentInfo.List_MaskTarget[i].bound.y/2, -10));
                }

                if(CurrentInfo.List_MaskTarget[i].arrow)
                {
                    Arrow.SetActive(true);
                    float arrow_pos_y = 50;
                    float anim_move_dis = 20f;
                    if(CurrentInfo.m_ArrowBottom) //Talk박스의 위치로 나눔
                    {
                        Arrow.transform.eulerAngles = Vector3.zero;
                        Arrow.transform.localPosition = _mask.transform.localPosition + new Vector3(CurrentInfo.List_MaskTarget[i].bound.x / 2, CurrentInfo.List_MaskTarget[i].bound.y / 2, 0); //Mask의 중앙 위치
                        Arrow.transform.localPosition -= new Vector3(0, CurrentInfo.List_MaskTarget[i].bound.y / 2 + arrow_pos_y, 0);
                        Arrow.transform.DOLocalMoveY(Arrow.transform.localPosition.y - anim_move_dis, 0.4f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
                    }
                    else
                    {
                        Arrow.transform.eulerAngles = new Vector3(0,0, 180);
                        Arrow.transform.localPosition = _mask.transform.localPosition + new Vector3(CurrentInfo.List_MaskTarget[i].bound.x / 2, CurrentInfo.List_MaskTarget[i].bound.y / 2, 0); //Mask의 중앙 위치
                        Arrow.transform.localPosition += new Vector3(0, CurrentInfo.List_MaskTarget[i].bound.y / 2 + arrow_pos_y, 0);
                        Arrow.transform.DOLocalMoveY(Arrow.transform.localPosition.y + anim_move_dis, 0.4f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
                    }                  
                }
            }
            else
                Debug.LogError("타겟을 찾지 못했습니다.!!");
        }

        //핑거 애니메이션 세팅
        if(List_Touch.Count == 1)
        {
            Finger.gameObject.SetActive(true);
            Finger.transform.localPosition = List_Touch[0];      
        }
        else if (List_Touch.Count > 1)
        {
            Finger.gameObject.SetActive(true);
            Finger.transform.localPosition = List_Touch[0];
            //Finger.transform.DOLocalMove(List_Touch[1], 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);

            Finger_Seq = DOTween.Sequence();
            Finger_Seq.AppendCallback(() =>
            {
                Finger.transform.localPosition = List_Touch[0];
                Finger.color = Color.white;
            });
            Finger_Seq.Append(Finger.transform.DOLocalMove(List_Touch[1], 1f).SetEase(Ease.Linear));
            Finger_Seq.Insert(1f, DOTween.ToAlpha(() => Finger.color, _color => Finger.color = _color, 0f, 0.2F));
            Finger_Seq.SetLoops(-1, LoopType.Restart);                  
        }

        //종료세팅
        if (List_Touch.Count == 1)
        {
            //사용하는 곳에서 직접 Tutorial_End 호출한다.
        }
        else if (List_Touch.Count > 0)
        {
            Btn_Next.SetActive(false);
            //스위칭할때 Tutorial_End를 호출한다.
            MatchManager.SwitchingApply += Tutorial_End;
        }
        else
        {
            Btn_Next.SetActive(true);
            SetBtnPostion(Btn_Next.transform, 0);

            if (CurrentInfoIndex == list_lnfo.Count)
                Btn_Next.GetComponentInChildren<UILabel>().text = "DONE";
            else
                Btn_Next.GetComponentInChildren<UILabel>().text = "NEXT";
        }

        //스킵버튼 세팅
        Btn_Skip.SetActive(true);
        if (Btn_Next.activeSelf)
            SetBtnPostion(Btn_Skip.transform, 1);
        else
            SetBtnPostion(Btn_Skip.transform, -1);

        //Seed 세팅
        if (CurrentInfo.List_Seed.Count > 0)
        {
            Board.isTutorialSeed = true;
            Board.List_Seed.Clear();
            for (int i = 0; i < CurrentInfo.List_Seed.Count; i++)
            {
                Board.List_Seed.Add(CurrentInfo.List_Seed[i]);
            }
        }

        isDirty = true;
    }

    public void SetBtnPostion(Transform obj, int index)
    {
        //if (CurrentInfo.m_BtnBottom)
        //{
        if(index == -1)
            obj.localPosition = new Vector3(0, -83, 0);
        else if (index == 0)
            obj.localPosition = new Vector3(120, -83, 0);
        else
            obj.localPosition = new Vector3(-150, -83, 0);
        //}
        //else
        //{
        //    if (index == 0)
        //        obj.localPosition = new Vector3(230, 105, 0);
        //    else
        //        obj.localPosition = new Vector3(65, 105, 0);
        //}
    }


    public void Tutorial_End()
    {
        StartCoroutine(Co_Tutorial_End());
    }
    IEnumerator Co_Tutorial_End()
    {
        //TextBox 세팅
        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);

        //Tutorial_Analytics(false);

        Tutorial_Init(true);

        while (MatchMgr.m_StepType != StepType.Wait)
        {
            //다음페이지가 있어도 클리어하면 튜토리얼 종료
            if (MatchMgr.m_StepType == StepType.Clear)
            {
                //여기서 코루틴을 빠져 나가면 CurrentInfoIndex값이 초기화 되지 않아서 다음 튜토리얼에 영향을 준다.
                //페이지값을 변경하여 Tutorial_Set를 태워서 튜토리얼을 끝내도록 하자.
                CurrentInfoIndex = 99; 
                break;
            }

            yield return null;
        }
          

        Tutorial_Set(List_CurrentInfo);
    }

    public void OnClick_Next()
    {   
        Tutorial_End();
        SoundManager.Instance.PlayEffect("play_click");
    }

    public void OnClick_Skip()
    {
        //Tutorial_Analytics(true);
        Tutorial_Init();
        SoundManager.Instance.PlayEffect("play_click");
    }

    //public void Tutorial_Analytics(bool Skip)
    //{
    //    if(List_CurrentInfo.Count < 0)
    //        return;
    //    //1. CurrentInfoIndex는 0부터 시작하지만 페이지를 열자마자 ++해주기때문에 튜토리얼이 열린시점에서는 페이지값과 동일하다.
    //    //2. 스킵일때는 남은 로그를 다 날려준다.
    //    for (int i = CurrentInfoIndex; i <= List_CurrentInfo.Count; i++)
    //    {
    //        switch (List_CurrentInfo[0].m_Stage)
    //        {
    //            case 1:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N05_Tutorial_1_1Finished);
    //                    else if (CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N06_Tutorial_1_2Finished);
    //                    else if (CurrentInfoIndex == 3)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N07_Tutorial_1_3Finished);
    //                }
    //                break;
    //            case 2:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N09_Tutorial_2_1Finished);
    //                    else if (CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N10_Tutorial_2_2Finished);
    //                    else if (CurrentInfoIndex == 3)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N11_Tutorial_2_3Finished);
    //                }
    //                break;
    //            case 3:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N12_Tutorial_3_1Finished);
    //                    else if(CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N13_Tutorial_3_2Finished);
    //                    else if(CurrentInfoIndex == 3)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N14_Tutorial_3_3Finished);
    //                    else if(CurrentInfoIndex == 4)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N15_Tutorial_3_4Finished);
    //                }
    //                break;
    //            case 4:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N16_Tutorial_4_1Finished);
    //                    else if(CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N17_Tutorial_4_2Finished);
    //                    else if(CurrentInfoIndex == 3)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N18_Tutorial_4_3Finished);
    //                }
    //                break;
    //            case 5:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N19_Tutorial_5_1Finished);
    //                    else if(CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N20_Tutorial_5_2Finished);
    //                    else if(CurrentInfoIndex == 3)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N21_Tutorial_5_3Finished);
    //                }
    //                break;
    //            case 6:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N22_Tutorial_6_1Finished);
    //                }
    //                break;
    //            case 7:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N23_Tutorial_7_1Finished);
    //                    else if(CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N24_Tutorial_7_2Finished);
    //                }
    //                break;
    //            case 8:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N25_Tutorial_8_1Finished);
    //                    else if(CurrentInfoIndex == 2)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N26_Tutorial_8_2Finished);
    //                }
    //                break;
    //            case 10:
    //                {
    //                    if (CurrentInfoIndex == 1)
    //                        GoogleAnalytics_Manager.Instance.LogEvent_NewUser(NewUserStep.N27_Tutorial_10_1Finished);
    //                }
    //                break;
    //        }

    //        //Skip일때만 for문 돈다.
    //        if(Skip == false)
    //            break;
    //    }      
    //}

	//// Use this for initialization
	//void Start () {
	
	//}
	
	//// Update is called once per frame
	//void Update () {
	
	//}

}
