﻿using UnityEngine;
using System.Collections;
using System;

public class UI_WorldMap : UI_Base {

    public static UI_WorldMap Instance;

    //public UI_CarPort ui_carport;

    public UISprite notice;

    public UISprite carporticon;

    void Awake()
    {
        Instance = this;
    }

    public override void StartUI()
    {
        //월드맵에선 배너광고 보이지 않는다.
        AdMob_Manager.Instance.AdMob_Banner_Hide();


        GameManager.Instance.m_specialmissionchk = false;
        GameManager.Instance.m_specialmissionstart = false;

        GameManager.Instance.m_adsRewardchk = false;

        Popup_Manager.INSTANCE.Init();

        FirebaseManager.Instance.LogScreen("WorldMap Screen");

        if (GameManager.Instance.m_GameData.sharechk)
        {
            WorldMapUI.instance.Share_icon.gameObject.SetActive(false);
            WorldMapUI.instance.BottomUIGrid.Reposition();
        }
        else
        {
            WorldMapUI.instance.Share_icon.gameObject.SetActive(true);
            WorldMapUI.instance.BottomUIGrid.Reposition();
        }

        if (!TimeManager.Instance.timemanagerInitchk)
        {
            TimeManager.Instance.Init();
        }

        //GameManager.Instance.dailyrewardchk = true;

        if (GameManager.Instance.dailyrewardchk)
        {

            GameManager.Instance.dailyrewardchk = false;

            Popup_Manager.INSTANCE.popup_index = 0;

            Popup_Manager.INSTANCE.Popup_Push();

        }

        else
        {


            //Debug.Log("Popup_Manager.INSTANCE.add_displaychk 1111111111:::::::::::::" + Popup_Manager.INSTANCE.add_displaychk);
            
            //광고가 나왔을때 애드프리팝업을 띄울지 체크한다.
            if (Popup_Manager.INSTANCE.add_displaychk)
            {
               // Debug.Log("Popup_Manager.INSTANCE.add_displaychk 2222222222:::::::::::::" + Popup_Manager.INSTANCE.add_displaychk);
                Popup_Manager.INSTANCE.add_displaychk = false;
                Popup_Manager.INSTANCE.AddFreePopSetting();
            }

            if (!StageItem.isNextPlayButtonClick && Popup_Manager.INSTANCE.addfree_popupchk)
            {
                SecurityPlayerPrefs.SetInt("Addfreepopupcnt", GameManager.Instance.m_GameData.AdsFreeDisplayCnt);

                GameManager.Instance.m_GameData.AdsFreeDisplayCnt++;

                Popup_Manager.INSTANCE.popup_index = 7;

                Popup_Manager.INSTANCE.Popup_Push();
            }
            else
            {
                if (!GameManager.Instance.m_GameData.RateRecodeChk)
                {
#if !UNITY_IOS
                    if (!StageItem.isNextPlayButtonClick && StageItemManager.Instance.TotalnowStage >= GameManager.Instance.m_GameData.RateStageIndex && StageItemManager.Instance.TotalnowStage < 100)
                    {
                        GameManager.Instance.m_GameData.RateStageIndex = (GameManager.Instance.m_GameData.RateStageIndex * 2) + 1;
                        SecurityPlayerPrefs.SetInt("ratestageindex", GameManager.Instance.m_GameData.RateStageIndex);

                        if (!GameManager.Instance.dailyrewardchk)
                        {
                            GameManager.Instance.m_ratepopchk = true;

                            Popup_Manager.INSTANCE.popup_index = 8;

                            Popup_Manager.INSTANCE.Popup_Push();
                        }
                    }
#endif
                }
            }
            
        }

       

        MyDataSetting();

        WorldMapUI.instance.FreeCoinSetting();

        carporticon.GetComponent<Animator>().SetBool("IDLE_ANI", false);

        if (TimeManager.Instance.trucksaletimechk && GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade<29)
        {


            notice.gameObject.SetActive(true);

            carporticon.GetComponent<Animator>().SetBool("IDLE_ANI", true);
        }
        

       
    }

    IEnumerator SpecialPopDisplay()
    {

       // GameManager.Instance.m_specialPopchk = true;

       // SecurityPlayerPrefs.SetInt("SpecialPopchk", GameManager.Instance.m_specialPopchk == true ? 1 : 0);

        yield return new WaitForSeconds(1f);


        Popup_Manager.INSTANCE.popup_index = 33;
        Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(202));
        Popup_Manager.INSTANCE.Popup_Push();


        


    }

    
    public override void EndUI()
    {

    }

    public void MyDataSetting()
    {
        StageItemManager.Instance.MyPostionSetting();

        //StageItemManager.Instance.MyBestFoodTruckSetting();

        WorldMapUI.instance.MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
        WorldMapUI.instance.MyStar.text = GameManager.Instance.m_GameData.MyStar.ToString("N0");

        
        //StageItemManager.Instance.EnterWorldMap();



        if (StageItemManager.Instance.TotalnowStage < GameManager.Instance.m_rewardtrucklevel - 1)
        {
            carporticon.gameObject.SetActive(false);
        }
        else
        {
            carporticon.gameObject.SetActive(true);
        }

        if (TimeManager.Instance.supersaletimechk)
        {
            WorldMapUI.instance.supersaleicon.gameObject.SetActive(true);
            WorldMapUI.instance.supersaleAnimation.special_ani = true;
        }
        else
        {
            WorldMapUI.instance.supersaleicon.gameObject.SetActive(false);
            WorldMapUI.instance.supersaleAnimation.special_ani = false;
        }

        if (TimeManager.Instance.missiontimechk)
        {
            //미션 안나오도록.
            //WorldMapUI.instance.missionicon.gameObject.SetActive(true);
        }
        else
        {
            WorldMapUI.instance.missionicon.gameObject.SetActive(false);
        }


        WorldMapUI.instance.package1.gameObject.SetActive(false);
        WorldMapUI.instance.package2.gameObject.SetActive(false);

        if (StageItemManager.Instance.TotalnowStage >= 29)
        {
            if (GameManager.Instance.m_GameData.Packageitem1buychk && GameManager.Instance.m_GameData.Packageitem2buychk && GameManager.Instance.m_GameData.Packageitem3buychk && GameManager.Instance.m_GameData.Packageitem4buychk)
            {
                WorldMapUI.instance.package1.gameObject.SetActive(false);
                WorldMapUI.instance.package2.gameObject.SetActive(false);
            }
            else
            {
                if (WorldMapUI.instance.missionicon.gameObject.activeSelf)
                {
                    WorldMapUI.instance.package1.gameObject.SetActive(true);
                    WorldMapUI.instance.package2.gameObject.SetActive(false);
                }
                else
                {
                    WorldMapUI.instance.package1.gameObject.SetActive(false);
                    WorldMapUI.instance.package2.gameObject.SetActive(true);
                }
            }

        }

        SoundManager.Instance.PlayBGM("intro_bgm");

        WorldMapUI.instance.WorldmapStart();
    }
}