﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControl : MonoBehaviour {

    public const float m_DelayTime = 0.1f;
    public List<GameObject> m_ListObj;

    void Awake()
    {
        for (int i = 0; i < m_ListObj.Count; i++)
        {
            if (m_ListObj[i] == null)
                continue;

            m_ListObj[i].SetActive(false);
        }
    }

    void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Co_Active());
    }

    IEnumerator Co_Active()
    {
        for (int i = 0; i < m_ListObj.Count; i++)
        {
            if(m_ListObj[i] == null)
                continue;

            m_ListObj[i].SetActive(true);
            yield return new WaitForSeconds(m_DelayTime);
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();

        for (int i = m_ListObj.Count - 1; i >= 0; i--)
        {
            if (m_ListObj[i] == null)
                continue;

            m_ListObj[i].SetActive(false);
        }
    }
}
