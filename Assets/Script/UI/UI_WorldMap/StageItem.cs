﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StageData
{
    public int StageNum = 0;
    public bool StageOpne = false;


    public bool isGoldBox = false;
    public bool isGoldBoxOpen = false;
    public int Gold = 0;
    public int StarCount = 0;
}

public class StageItem : MonoBehaviour
{
    public static bool isNextPlayButtonClick = true;

    public StageData StageData = null;

    [SerializeField]
    private List<UISprite> m_Stars = new List<UISprite>();

    [SerializeField]
    private GameObject m_Cover = null;

    [SerializeField]
    private UILabel m_StageNum = null;

    [SerializeField]
    private GoldBox m_GoldBox = null;

    // Use this for initialization
    void Start()
    {
        Init(StageData);
    }

    public void Init(StageData data)
    {
        if (data != null)
        {
            StageData = data;

            m_Cover.SetActive(!data.StageOpne);
            m_StageNum.text = data.StageNum.ToString();
            m_GoldBox.gameObject.SetActive(data.isGoldBox && !data.isGoldBoxOpen);
            for (int i = 0; i <m_Stars.Count; i++)
            {
                m_Stars[i].gameObject.SetActive(i < data.StarCount);
            }
            
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }

    }
    
    public void StarDisplay(int starCount)
    {
        if (isNextPlayButtonClick)
            NowPlay(StageData.StarCount);
        else
            StartCoroutine(IStarDisplay(StageData.StarCount));
        StageData.StarCount = starCount;
    }

    public void NowPlay(int startStar)
    {
        isNextPlayButtonClick = false;
        StageItemManager.Instance.NextStageMove();
        int endStar = StageData.StarCount;
        for (int i = 0; i < m_Stars.Count; i++)
        {
            m_Stars[i].gameObject.SetActive(startStar >= endStar);
        }
        
        GameManager.Instance.m_GameData.MyGold += StageData.Gold;
        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);
    }

    IEnumerator IStarDisplay(int startStar)
    {
        int endStar = StageData.StarCount;
        GameManager.Instance.m_isTouch = false;
        
        while (startStar < m_Stars.Count)
        {
            m_Stars[startStar].gameObject.SetActive(startStar >= endStar);

            startStar++;

            SoundManager.Instance.PlayEffect("star_c");
            yield return new WaitForSeconds(0.3f);
        }
        
        GameManager.Instance.m_isTouch = true;

        if (StageData.isGoldBox && !StageData.isGoldBoxOpen)
        {
            StageData.isGoldBoxOpen = true;
            Special_Gift.Gold = StageData.Gold;
            
            m_GoldBox.openAniSetting();

            yield return new WaitForSeconds(0.5f);

            Popup_Manager.INSTANCE.Popup_Push(9);
            StageItemManager.Instance.NextStageMove();
        }
        else
            StageItemManager.Instance.NextStageMove();
        
    }


    public void OnStageStartButtonClick()
    {
        SoundManager.Instance.PlayEffect("play_click");

        GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);

        GameManager.Instance.GameStart(StageData.StageNum);
    }

}
