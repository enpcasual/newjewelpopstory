﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StageItemManager : MonoBehaviour
{
    public static StageItemManager Instance = null;
    private List<StageData> m_StageDatas = new List<StageData>();

    [SerializeField]
    private Vector3 StartPos
    {
        get
        {
            if (TotalnowStage > 3)
                return new Vector3(0, 360);
            else
                return new Vector3(0, 270);
        }
    }

    [SerializeField]
    private List<StageItemLien> m_NewStageItemLiens = new List<StageItemLien>();

    [SerializeField]
    private UIScrollView m_ScrollView = null;
    [SerializeField]
    private UIWrapContent m_WrapContent = null;
    [SerializeField]
    private SpringPanel m_SpringPnael = null;
    
    [SerializeField]
    private GameObject m_MyCharacter = null;
    
    [SerializeField]
    private UIWidget m_Widget = null;

    //프리펩
    //public ParticleSystem arrivedeffect;

    public int CurrentPart;
    public int CurrentStage;
    public int TotalnowStage;

    public int LastStage = 300;

    public int rewardstar = 0;
    public int nowplaystage = 0;

    [HideInInspector]
    public bool isLastStage
    {
        get { return LastStage == TotalnowStage; }
    }

    [HideInInspector]
    public bool isNewStage
    {
        get { return nowplaystage == TotalnowStage; }
    }

    public void Awake()
    {
        if(Instance == null)
            Instance = this;
    }
    
    public IEnumerator Start()
    {
        yield return null;
        MyPostionSetting();
        m_WrapContent.WrapContent();
        yield return new WaitUntil(() => FindStageToStageIndex(TotalnowStage + 1) != null);
        m_MyCharacter.transform.parent = FindStageToStageIndex(TotalnowStage + 1).transform;
        m_MyCharacter.SetActive(FindStageToStageIndex(TotalnowStage + 1) != null);
        m_MyCharacter.transform.localPosition = Vector3.zero;
    }

    private void OnEnable()
    {
        StartCoroutine(Start());
    }

    // Use this for initialization
    public void Init()
    {
        m_WrapContent.onInitializeItem += OnInitailizeItem;

        CurrentPart = GameManager.Instance.m_GameData.CurrentPart;
        CurrentStage = GameManager.Instance.m_GameData.CurrentStage;
        TotalnowStage = CurrentStage;
        
        for (int i = 0; i < LastStage; i++)
        {
            StageData data = new StageData();
            data.StageNum = i + 1;
            data.StageOpne = CurrentStage >= i;
            GoldBoxStage goldbox = TableDataManager.Instance.m_GoldBoxStage.Find(r => r.stage == i + 1);
            data.isGoldBox = goldbox != null;
            data.isGoldBoxOpen = data.isGoldBox && TotalnowStage > i;
            data.Gold = data.isGoldBox ? goldbox.gold : 0;

            if (TotalnowStage > i)
                data.StarCount = GameManager.Instance.m_GameData.GetBestStarCount(i + 1);
            else
                data.StarCount = 0;

            m_StageDatas.Add(data);
        }
        
        m_WrapContent.minIndex = -(m_StageDatas.Count / 4 - 1);
        m_Widget.height = (m_StageDatas.Count / 4 - 1) * m_WrapContent.itemSize;

        MyPostionSetting();
        StartCoroutine(Start());
    }

    private void OnInitailizeItem(GameObject go, int wrapIndex, int realIndex)
    {
        StageItemLien item = go.GetComponent<StageItemLien>();

        realIndex = Mathf.Abs(realIndex);
        if (realIndex >= 0 && realIndex < m_StageDatas.Count)
        {
            item.Init(m_StageDatas.GetRange(realIndex * item.ItemCount, item.ItemCount));
        }
        else
        {
            item.Init(null);
        }

        m_MyCharacter.SetActive(FindStageToStageIndex(TotalnowStage + 1) != null);
    }

    public void ItemAllUpdata()
    {
        //m_SpringPnael.gameObject.transform.localPosition = new Vector3(0, startPosY);

        //m_NewStageItemLiens.Sort(delegate (StageItemLien a, StageItemLien b) { return b.transform.position.y.CompareTo(a.transform.position.y); });
        for (int i = 0; i < m_NewStageItemLiens.Count; i++)
        {
            if (m_NewStageItemLiens.Count > i)
            {
                int realIndex = PostionToIndex(m_NewStageItemLiens[i].transform.localPosition.y);

                int itemCount = m_NewStageItemLiens[i].ItemCount;
                if (m_StageDatas.Count > realIndex * itemCount + itemCount - 1)
                {
                    m_NewStageItemLiens[i].Init(m_StageDatas.GetRange(realIndex * itemCount, itemCount));
                }
                else
                {
                    m_NewStageItemLiens[i].Init(null);
                }
            }
        }
    }

    public void ItemAllRePostion()
    {
        for (int i = 0; i < m_NewStageItemLiens.Count; i++)
        {
            m_NewStageItemLiens[i].transform.localPosition = -(m_SpringPnael.transform.localPosition - StartPos) - new Vector3(0, (m_WrapContent.itemSize * i));
        }
    }

    public void NextStageMove()
    {
        if (m_StageDatas.Count != 0 && isNewStage && GameManager.Instance.newstageclear)
        {
            CharactermoveStart();
        }
    }

    public void CharactermoveStart()
    {
        m_MyCharacter.transform.parent = FindStageToStageIndex(TotalnowStage + 1).transform;
        m_MyCharacter.transform.localPosition = Vector3.zero;

        if (TotalnowStage + 1 >= LastStage)
            return;

        CurrentStage++;
        TotalnowStage++;
        StageItem item = FindStageToStageIndex(TotalnowStage + 1);

        m_MyCharacter.transform.DOMove(item.transform.position, 0.1f);
        m_MyCharacter.transform.parent = item.transform;

        //arrivedeffect.transform.localPosition = m_MyCharacter.transform.localPosition + new Vector3(0, -10, 0);

        SoundManager.Instance.PlayEffect("5m");

        //arrivedeffect.Play();

        GameManager.Instance.newstageclear = false;

        SecurityPlayerPrefs.SetInt("CurrentStage", CurrentStage);

        OpneStage(CurrentStage + 1);
    }

    public void OpneStage(int index)
    {
        StageItem itme = FindStageToStageIndex(index);
        m_StageDatas[index - 1].StageOpne = true;
        itme.Init(m_StageDatas[index - 1]);
    }

    //현재 내 스테이지위치를 세팅한다
    public void MyPostionSetting()
    {
        float startPosY = StartPos.y;
        //시작 지점 위치 보정
        if (m_WrapContent.itemSize * (TotalnowStage / 4 - 3) > 0)
            startPosY += m_WrapContent.itemSize * (TotalnowStage / 4 - 3);

        //끝 지점 위치 보정
        int index = PostionToIndex(startPosY) + 4;
        if (index > (m_StageDatas.Count / 4) - 1)
            startPosY -= m_WrapContent.itemSize * (index - (m_StageDatas.Count / 4) - 1);

        SpringPanel.Begin(m_SpringPnael.gameObject, new Vector3(0, startPosY));
        ItemAllRePostion();
        ItemAllUpdata();
    }

    //플레이 하고 돌아왔을때
    public void StageSartSetting(int cnt)
    {
        StageItem item = FindStageToStageIndex(nowplaystage + 1);

        if (item != null && item.StageData.StarCount < cnt)
        {
            //획득한 별수 표시
            item.StarDisplay(cnt);
        }

        rewardstar = 0;
    }

    public StageItem FindStageToStageIndex(int index)
    {
        StageItem item = null;
        for (int i = 0; i < m_NewStageItemLiens.Count; i++)
        {
            item = m_NewStageItemLiens[i].FindStageToStageIndex(index);

            if (item != null)
                break;
        }

        return item;
    }

    private int PostionToIndex(float posY)
    {
        int realIndex = Mathf.RoundToInt(posY / m_WrapContent.itemSize);
        realIndex = Mathf.Abs(realIndex);

        return realIndex;
    }
}
