﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageItemLien : MonoBehaviour
{

    [SerializeField]
    private List<StageItem> Items = new List<StageItem>();
    [HideInInspector]
    public int ItemCount { get { return Items.Count; } }

    public void Init(List<StageData> datas)
    {
        for (int i = 0; i < Items.Count; i++)
        {
            if (datas != null && i < datas.Count)
            {
                Items[i].Init(datas[i]);
                gameObject.SetActive(true);
            }
            else
            {
                Items[i].Init(null);
                gameObject.SetActive(false);
            }
        }
    }

    public StageItem FindStageToStageIndex(int index)
    {
        return Items.Find((temp) => { return temp.StageData.StageNum == index; });
    }
}
