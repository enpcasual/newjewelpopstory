using UnityEngine;
using System;
using System.Collections;


//BJS
// * ExposeProperties.cs와 한쌍이다. GetProperties() 함수에서 사용한다.
// - Property에 속성을 줘서 Editor에서 inspector에 보여줄 Property를 지정할수 있다.
// 잠점1. GetProperties()함수는 클래스의 모든 property를 체크하므로 Editor에서 모든 property를 한번에 처리할수 있다.
// 장점2. 원하는 Property를 선별할수 있으며 간단하게 추가 할수 있다.
// 장점3. Inspector에 보이지 않는 property를 쉽게 추가 할수 있다. 
// 장점4. Property가 수정, 추가, 삭제 될때마다 Editor쪽 코드를 수정할 필요가 없다.

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class ExposePropertyAttribute : Attribute
{
 
}