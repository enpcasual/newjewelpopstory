﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Net;
using System.Net.Sockets;
using System;


public enum GAMESTATE
{
    Main, //게임 시작 화면
    WorldMap,
    ThreeMatch,
    CarPort
}


public class Mytruck
{
    public int truckindex;
    public int truckgrade;
}

public class myfoodtruckdata
{
    public List<Mytruck> gradelist = new List<Mytruck>();

    public int m_MyFoodTruckGrade;
}

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    //******************
    //인스펙터 세팅
    //******************
    public Camera WorldCamera;
    public Camera UICamera;
    public Camera MatchCamera;

    public GameObject m_WorldMap;
    public List<UI_Base> m_UIRootInfo = new List<UI_Base>();
    [HideInInspector]
    public UI_Base m_CrruntUI;

    //********************
    //미션데이타,SaveData
    //********************
    public WorldData m_WorldData;
    public GameData m_GameData = new GameData();

    //********************************
    //PlayerPrefs에만 세이브하는 변수
    //********************************
    public int FreeUseTruckGrade;
    public int FreeUsecnt;

    //*******************
    //세이브 안하는 변수
    //*******************
    public bool isPlayFromEditor = false;
    //public Mytruck m_mytruck;
    public int rewardTruckgrade;//레벨 클리어후 결과 팝업에서 푸드트럭 등급
    public int m_FoodTruckAlbumIndex;
    public bool carportmovechk;
    public int m_fulladdisplaycnt;//전면광고 횟수
    public bool m_gameresult_add_displaychk;// 결과화면에서 광고를 노출시킬것인가?
    public bool m_add_displaychk;//지금 광고를 노출시켜도 되는가?
    public bool playstatechk;// 플레이 화면에서 차고로 갔다가 다시 돌아올때 사용
    public bool gameresultchk;// 플레이 화면에서 차고로 갔다가 다시 돌아올때 사용
    public bool resumechk;
    public bool OnResumechk;
    public bool dailyrewardchk;
    public bool playquitchk;
    public bool newstageclear;
    public bool unclokgoldchk;//게이트 웨이 비용지불후 제거
    //public bool myfoodtruckchange;//푸드트럭이 바뀌었을때  
    public bool m_specialmissionchk;
    public bool m_specialmissionstart;
    public bool m_restatchk;
    public bool m_replaychk;
    //public bool m_popup_pop_sound;
    //public bool m_button_release_sound;
    //public bool m_backbuttonapply;
    public bool m_addfreedisplaychk;
    //public bool m_Achievementchk;
    //public bool m_titleloginchk;
    public bool m_Achivementloginchk;
    public bool m_LeadBoardloginchk;
    public bool m_coupongoldechk;
    public int m_coupongold;
    public int m_rewardtrucklevel;
    public bool m_trucktutorialstatus;
    public bool m_specialPopchk;

    public bool gamecontinuefree;

    public bool freemovechk;

    public bool m_isTouch = true;

    public bool m_ratechk;

    public bool m_ratepopchk;

    public bool m_startCarport;

    public int m_randomGrade;

    public bool m_randombestgrade;//랜덤박스에서 현재 최고등급보다 높은 등급이 나왔을때

    public bool m_adsRewardchk;
  
    void Awake()
    {
        Instance = this;

#if UNITY_EDITOR
        isPlayFromEditor = UnityEditor.EditorPrefs.GetBool(EditorManager.EditorToPlayKey, false);
        UnityEditor.EditorPrefs.SetBool(EditorManager.EditorToPlayKey, false);
        //Webplayer와 Standalone에만 적용됨. 즉PC에서만 포커싱이 아니어도 돌아가도록함.
        Application.runInBackground = true;
#endif

        //화면 꺼지지 않음.
        //Screen.sleepTimeout = SleepTimeout.NeverSleep;
        DOTween.SetTweensCapacity(3000, 1000);

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        Init();
    }

    void Init()
    {
        m_gameresult_add_displaychk = false;

        m_add_displaychk = false;

        m_rewardtrucklevel = 20;

        playstatechk = false;

        gameresultchk = false;

        //m_UIRootInfo[0].gameObject.SetActive(true);
        //m_UIRootInfo[1].gameObject.SetActive(true);
        
        m_isTouch = true;
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("test ::::::::::::::::: GameManager.Start()");
        //UI_Main.Awake에서 uuid를 사용하므로 Native_Brige.Awake가 먼저 호출되어야한다.
        //하지만 SetActive는 Awake를 호출시킨다. 그래서 SetGameState를 Start에서 호출한다.
        SetGameState(GAMESTATE.Main);

        StartCoroutine(Process());
    }

    IEnumerator Process()
    {
        Debug.Log("test ::::::::::::::::: GameManager.Process()");

        if (!isPlayFromEditor)
        {
            //1.타이틀화면 및 게이지 출력
            UI_Main.Instance.ProgressLoading();
            TableLoadPlus();
            yield return null;
        }

        //2.데이타 로드, 화면 변경
        m_WorldData = DataManager.LoadWorld();
        TableLoadPlus();
        yield return null;

        MyDataload();
        TableLoadPlus();
        yield return null;

        //3.테이블 언어
        if (m_GameData.GamePlayFirst)
            TableDataManager.Instance.LocalTextSetting(Application.systemLanguage);
        else
            TableDataManager.Instance.currentLanguage = (TableDataManager.Language)SecurityPlayerPrefs.GetInt("CurrenLanguage", 0);

        TableDataManager.Instance.Load_LocalizingTable(); 
        TableLoadPlus();
        yield return null;

        //3.테이블 1차
        TableDataManager.Instance.Load_GateWay(); 
        TableDataManager.Instance.Load_GoldBox();
        TableLoadPlus();
        yield return null;

        //4.테이블 2차
        TableDataManager.Instance.Load_Shop();
        TableDataManager.Instance.Load_DailyReward();
        TableDataManager.Instance.Load_FoodTruck();
        TableDataManager.Instance.Load_MissionGold();
        TableDataManager.Instance.Load_BuyGold();
        TableDataManager.Instance.Load_Achivement();
        TableDataManager.Instance.Load_Cp();
        TableDataManager.Instance.Load_FreeCoinReward();
        TableDataManager.Instance.Load_PackageItem();
        TableDataManager.Instance.Load_FreeBoxReward();
        TableLoadPlus();
        yield return null;

        
        //5.팝업 초기화
        //PopupManager.SetActive(true);
        if (isPlayFromEditor)
        {
            SetGameState(GAMESTATE.ThreeMatch);
            yield break;
        }
        else
            SetGameState(GAMESTATE.Main);
        TableLoadPlus();
        yield return null;

        //약관동의 팝업출력
        bool isAgree = false;
#if !IGNORE_AGREE
        Popup_Manager.INSTANCE.UIAgreement.Init(() => { isAgree = true; });
#else
        isAgree = true;
#endif

        //6.다에리 네트워크
        //NetWorkManager.instance.requestGetMailList();
        //NetWorkManager.instance.requestGetCurrentTime();
        GetCurrentTime();
        TableLoadPlus();
        yield return null;

        //7.월드맵 초기화
        //m_WorldMap.SetActive(true);
        //StageItemManager.Instance.Init();
        //m_WorldMap.SetActive(false);
        TableLoadPlus();
        yield return null;

        //8.차고 초기화
        m_UIRootInfo[3].gameObject.SetActive(true);
        //UI_CarPort.Instance.m_carportmanager.MyFoodTruckSetting();
        m_UIRootInfo[3].gameObject.SetActive(false);
        TableLoadPlus();
        yield return null;

        //9.오디오데이타 로드
        //SoundManager.Instance.PreLoadAudioData();
        //TableLoadPlus();
        yield return null;

        //TableLoadPlus();
        //yield return null;

        //100% 문구 출력
        // UI_Main.Instance.ProgresstxtDisplay();
        
        //약관동의 대기 
        yield return new WaitUntil(() => isAgree);

        m_WorldMap.gameObject.SetActive(true);

        UI_Main.Instance.ProgressLoadingEnd();
    }

#region NTP서버에서 시간가져와서 HotTime 적용
    //*******************************
    //NTP 서버에서 시간 가져오는 소스
    //https://stackoverflow.com/questions/1193955/how-to-query-an-ntp-server-using-c
    //********************************
    public static DateTime GetNetworkTime()
    {
        try
        {
            //default Windows time server
            const string ntpServer = "time3.google.com";//"time.windows.com";//차선

            // NTP message size - 16 bytes of the digest (RFC 2030)
            var ntpData = new byte[48];

            //Setting the Leap Indicator, Version Number and Mode values
            ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

            var addresses = Dns.GetHostEntry(ntpServer).AddressList;

            //The UDP port number assigned to NTP is 123
            var ipEndPoint = new IPEndPoint(addresses[0], 123);
            //NTP uses UDP

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                socket.Connect(ipEndPoint);

                //Stops code hang if NTP is blocked
                socket.ReceiveTimeout = 3000;

                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();
            }

            //Offset to get to the "Transmit Timestamp" field (time at which the reply 
            //departed the server for the client, in 64-bit timestamp format."
            const byte serverReplyTime = 40;

            //Get the seconds part
            ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

            //Get the seconds fraction
            ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

            //Convert From big-endian to little-endian
            intPart = SwapEndianness(intPart);
            fractPart = SwapEndianness(fractPart);

            var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

            //**UTC** time
            var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

            //return networkDateTime.ToLocalTime();
            return networkDateTime;
        }
        catch (Exception)
        {
            Debug.Log("인터넷이 안돼서 시간못가져옴");
            return new DateTime(0);
        }
    }

    // stackoverflow.com/a/3294698/162671
    static uint SwapEndianness(ulong x)
    {
        return (uint)(((x & 0x000000ff) << 24) +
                       ((x & 0x0000ff00) << 8) +
                       ((x & 0x00ff0000) >> 8) +
                       ((x & 0xff000000) >> 24));
    }

    public void GetCurrentTime()
    {
        dailyrewardchk = false;
        m_adsRewardchk = false;

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            DateTime date = GetNetworkTime();

            Debug.Log("현재 시간 : " + date);

            //만약 시간을 못가져오면 리턴!!
            if (date.Ticks == 0)
                return;

            int day = date.Day;
            int hour = date.Hour;


            if (m_GameData.HotTimeDay != day)
            {
                SecurityPlayerPrefs.SetInt("HotTimeDay", day);
                SecurityPlayerPrefs.SetInt("HotTimeHour1", 0);
                SecurityPlayerPrefs.SetInt("HotTimeHour2", 0);
                SecurityPlayerPrefs.SetInt("HotTimeHour3", 0);
            }

            if ((2 <= hour && hour < 5))
            {
                if (!m_GameData.HotTimeHour1)
                {
                    dailyrewardchk = true;
                    m_GameData.HotTimeHour1 = true;

                    SecurityPlayerPrefs.SetInt("HotTimeHour1", 1);
                }
                //핫타임시간에 접속하면 슈퍼세일을 진행한다.
                TimeManager.Instance.supersaletimechk = true;
            }
            else if ((10 <= hour && hour < 13))
            {
                if (!m_GameData.HotTimeHour2)
                {
                    dailyrewardchk = true;
                    m_GameData.HotTimeHour2 = true;

                    SecurityPlayerPrefs.SetInt("HotTimeHour2", 1);
                }
                //핫타임시간에 접속하면 슈퍼세일을 진행한다.
                TimeManager.Instance.supersaletimechk = true;
            }
            else if ((19 <= hour && hour < 22))
            {
                if (!m_GameData.HotTimeHour3)
                {
                    dailyrewardchk = true;
                    m_GameData.HotTimeHour3 = true;

                    SecurityPlayerPrefs.SetInt("HotTimeHour3", 1);
                }
                //핫타임시간에 접속하면 슈퍼세일을 진행한다.
                TimeManager.Instance.supersaletimechk = true;
            }
        }
    }

#endregion


    public void GameResultAddChk()
    {
       
        StartCoroutine(AddTimechk());
    }

    IEnumerator AddTimechk()
    {
        yield return new WaitForSeconds(120f);

        m_gameresult_add_displaychk = true;
                
        
    }

    public void ResumeAdsTimechk()
    {

        Debug.Log("ResumeAdsTimechk::::::::" + m_add_displaychk);

        m_add_displaychk = false;
       
        StartCoroutine(Add_Display_Time());
        
     }

    IEnumerator Add_Display_Time()
    {
        yield return new WaitForSeconds(120f);

      
        m_add_displaychk = true;

    }

    public void TableLoadPlus()
    {


        UI_Main.Instance.ProgressState();
       
        
    }
    void MyDataload()
    {
        ///////////////Cheat//////////////////////////
        if (Cheat.Instance.isCheatMode && SecurityPlayerPrefs.GetInt("GamePalyFirst", 1) == 1 ? true : false)
        {
            int c_stage = StageItemManager.Instance.LastStage - 1;        //0부터 시작(99가 100스테이지)
            int stageIndex = StageItemManager.Instance.LastStage;   //1부터 시작    
            int mystar = 0;
            SSD stage_data;
            for (int i = 1; i <= stageIndex; i++)
            {
                stage_data = m_GameData.SSD_GetData(i);
                stage_data.BestScore = 9999;
                stage_data.BestStartCnt = 1;
                m_GameData.SSD_Save(i, stage_data);

                mystar += 1;
            }
            SecurityPlayerPrefs.SetInt("CurrentStage", c_stage);
            SecurityPlayerPrefs.SetInt("MyStar", mystar);

            SecurityPlayerPrefs.SetInt("MyGold", 10000);
        }
        ////////////////////////////////////////////////////////////////////

        m_GameData.LoadData_PlayerPrefs();

        //무료트럭
        FreeUseTruckGrade = SecurityPlayerPrefs.GetInt("FreeUseTruckGrade", 0);
        FreeUsecnt = SecurityPlayerPrefs.GetInt("FreeUsecnt", 0);


        m_specialPopchk = SecurityPlayerPrefs.GetInt("SpecialPopchk", 0) == 1 ? true : false;

        if (m_GameData.GamePlayFirst)
        {
#if UNITY_ANDROID
            //GPGS_Manager.Instance.LoginGPGS();
#elif UNITY_IOS
            //GCIC_Manager.Instance.LoginGC();
#endif

            //SecurityPlayerPrefs.SetInt("GamePalyFirst", 0);

            TimeManager.Instance.AddFreeSetting();//첫날은 광고제거 팝업이 나오지 않는다

            TimeManager.Instance.SuperSaleDaySetting();//  슈퍼세일 5일단위로 체크

            TimeManager.Instance.MissionDaySetting(); //

            TimeManager.Instance.FoodTruckSaleSetting();//5일 단위로 체크

        }
        else
        {
            if (m_GameData.LoginChk)
            {
#if UNITY_ANDROID
                GPGS_Manager.Instance.LoginGPGS();
#elif UNITY_IOS
                GCIC_Manager.Instance.LoginGC();
#endif
            }
        }
              
      
        if (!m_GameData.AdsFreeChk)
        {
            GameResultAddChk();

            ResumeAdsTimechk();

            AdMob_Manager.Instance.Init_Banner();
        }

        if (TimeManager.Instance.AddFreeCheck())
        {
            m_addfreedisplaychk = true;
        }
       
        if (TimeManager.Instance.SuperSaleDayChk())
        {
            
            if (!m_GameData.BeforeSuperSale)//이전 5일전에 슈퍼세일하지 않았다
            {
                TimeManager.Instance.SetSaleTimer(0);

                SecurityPlayerPrefs.SetInt("beforesupersale", 1);
            }
            else if(m_GameData.BeforeSuperSale)
            {
                int randomint = UnityEngine.Random.Range(0, 100);

                if (randomint < 50)
                {
                    TimeManager.Instance.SetSaleTimer(0);

                    SecurityPlayerPrefs.SetInt("beforesupersale", 1);
                }
                else
                {

                    SecurityPlayerPrefs.SetInt("beforesupersale", 0);
                }

                
            }

            TimeManager.Instance.SuperSaleDaySetting();
        }

      

        if (TimeManager.Instance.MissionDayChk())
        {
            TimeManager.Instance.SetSaleTimer(1);

            TimeManager.Instance.MissionDaySetting();


            SecurityPlayerPrefs.SetInt("nowmissionlevel", 0);

            m_GameData.NowMissionLevel = 0;

        }


       if (TimeManager.Instance.FoodTruckSalechk())
        {
            int randomint = UnityEngine.Random.Range(0, 100);

            if (randomint < 50)
            {
                TimeManager.Instance.SetSaleTimer(2);

                
            }
            
            TimeManager.Instance.FoodTruckSaleSetting();



        }


        if (m_GameData.MyFoodTruckData == null || m_GameData.MyFoodTruckData.gradelist.Count<=0)
        {

            m_GameData.MyFoodTruckData = new myfoodtruckdata();

            m_GameData.MyFoodTruckData.m_MyFoodTruckGrade = 0;

            Mytruck temptruck = new Mytruck();

            temptruck.truckgrade = 0;

            temptruck.truckindex = 0;

            m_GameData.MyFoodTruckData.gradelist.Add(temptruck);

            DataManager.SaveToPlayerPrefs<myfoodtruckdata>("myfoodtruckdata", GameManager.Instance.m_GameData.MyFoodTruckData);
        }
           

         for (int i = 0; i < m_GameData.MyFoodTruckData.gradelist.Count; i++)
         {
             if (m_GameData.MyFoodTruckData.gradelist[i].truckgrade >= m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
             {
                m_GameData.MyFoodTruckData.m_MyFoodTruckGrade = m_GameData.MyFoodTruckData.gradelist[i].truckgrade;
             }
         }

     }

    //Update is called once per frame
#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        { 
            if (m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch && MatchManager.Instance.m_StepType == StepType.Wait)
            {
                //치트키일때 링패널 없앤다.
                for (int i = Board.m_ListRingPanel.Count - 1; i >= 0; i--)
                {                   
                    Board.m_ListRingPanel[i].m_Board.PanelDestroy(Board.m_ListRingPanel[i]);
                    Board.m_ListRingPanel.Remove(Board.m_ListRingPanel[i]);//관리목록에서 삭제.
                }

                    bool ok_a = false;
                for (int i = 0; i < MatchManager.Instance.m_ListBoard.Count; i++)
                {
                    if (ok_a == false)
                    {
                        if (MatchManager.Instance.m_ListBoard[i].IsItemExist && MatchManager.Instance.m_ListBoard[i].m_Item is NormalItem)
                        {
                            Item.Swap_A = MatchManager.Instance.m_ListBoard[i].m_Item;
                            ok_a = true;
                        }
                    }
                    else
                    {
                        if (MatchManager.Instance.m_ListBoard[i].IsItemExist && MatchManager.Instance.m_ListBoard[i].m_Item is NormalItem)
                        {
                            Item.Swap_B = MatchManager.Instance.m_ListBoard[i].m_Item;
                            break;
                        }
                    }
                }
                AbilityManager.Instance.Ability_RainbowRainbow(Item.Swap_A.m_Board, null);


            }
        }
        else if(Input.GetKeyDown(KeyCode.F2))
        {
            m_GameData.MyGold += 10000;
            SecurityPlayerPrefs.SetInt("MyGold", m_GameData.MyGold);
        }

        else if (Input.GetKeyDown(KeyCode.F3))
        {
            for (int i = 0; i < 600; i++)
            {
                SSD _ssd = m_GameData.SSD_GetData(i+1);
                 _ssd.Init();
               m_GameData.SSD_Save(i+1, _ssd);
            }

            SecurityPlayerPrefs.SetInt("GamePalyFirst", 1);

            SecurityPlayerPrefs.SetInt("Login", 0);

            SecurityPlayerPrefs.SetInt("CurrenPart", 0);

            SecurityPlayerPrefs.SetInt("CurrentStage", 0);


            DataManager.SaveToPlayerPrefs<myfoodtruckdata>("myfoodtruckdata", null);

           SecurityPlayerPrefs.SetInt("MyGold", 0);

           SecurityPlayerPrefs.SetInt("MyStar", 0);

           SecurityPlayerPrefs.SetInt("trucktutorialchk", 0);


           SecurityPlayerPrefs.SetInt("GPGSclearstageindex", 0);
           SecurityPlayerPrefs.SetInt("GPGSmybesttruckindex", 0);
           SecurityPlayerPrefs.SetInt("GPGSmystarindex", 0);
           SecurityPlayerPrefs.SetInt("GPGScomboindex", 0);
           SecurityPlayerPrefs.SetInt("GPGSclearpoint", 0);
           SecurityPlayerPrefs.SetInt("addfreechk", 0);
           SecurityPlayerPrefs.SetInt("raterecodechk", 0);
           SecurityPlayerPrefs.SetInt("ratestageindex", 24);

           SecurityPlayerPrefs.SetLong("FreeCoinTime", 0);

           SecurityPlayerPrefs.SetInt("FreeRewardnowindex", 0);

           SecurityPlayerPrefs.SetInt("SpecialPopchk", 0);
           
           for (int i = 0; i < GameManager.Instance.m_GameData.MyItemCnt.Count; i++)
           {
               SecurityPlayerPrefs.SetInt("MyItem" + i, 0);
           }

           SecurityPlayerPrefs.SetInt("SpecialPopchk", 0);
           SecurityPlayerPrefs.SetInt("Packageitem1buychk", 0);
           SecurityPlayerPrefs.SetInt("Packageitem2buychk", 0);
           SecurityPlayerPrefs.SetInt("Packageitem3buychk", 0);
           SecurityPlayerPrefs.SetInt("Packageitem4buychk", 0);

           SecurityPlayerPrefs.SetInt("FreeUsecnt", 0);
           SecurityPlayerPrefs.SetInt("FreeUseTruckGrade", 0);

           SecurityPlayerPrefs.SetInt("sharechk", 0);

            int cashItemCount = UI_ThreeMatch.Instance != null ? UI_ThreeMatch.Instance.CashItemCovers.Count : 3;
            for (int i = 0; i < cashItemCount; i++)
            {
                SecurityPlayerPrefs.SetBool("ItemCovers" + i, true);
            }



        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            Popup_Manager.INSTANCE.popup_index = 7;

            Popup_Manager.INSTANCE.Popup_Push();
        }
        else if (Input.GetKeyDown(KeyCode.F5))
        {
            SecurityPlayerPrefs.DeleteAll();
        }
        else if (Input.GetKeyDown(KeyCode.F6))
        {
            //StageItemManager.Instance.playExitchk = true;
            StageItemManager.Instance.nowplaystage = StageItemManager.Instance.TotalnowStage;
            GameManager.Instance.newstageclear = true;
            StageItemManager.Instance.NextStageMove();
        }
        else if (Input.GetKeyDown(KeyCode.F9))
        {
            int c_part = 0;         //0부터 시작
            int c_stage = 299;        //0부터 시작(99가 100스테이지)
            int stageIndex = 300;   //1부터 시작    
            SSD stage_data;
            for (int i = 1; i <= stageIndex; i++)
            {
                stage_data = m_GameData.SSD_GetData(i);
                stage_data.BestScore = 9999;
                stage_data.BestStartCnt = 3;
                m_GameData.SetBestStarCount(i, 3);
                m_GameData.SSD_Save(i, stage_data);
            }
            SecurityPlayerPrefs.SetInt("CurrenPart", c_part);
            SecurityPlayerPrefs.SetInt("CurrentStage", c_stage);
        }
        else if(Input.GetKeyDown(KeyCode.F12))
        {
            //StartCoroutine(UI_ThreeMatch.Instance.FoodTruck_BonusMovePlus());
        }
    }
#endif

#region 게임 상태 관리
    public static bool EqualCurrentGameState(GAMESTATE state)
    {
        if (Instance != null)
        {
            return Instance.GetGameState() == state;
        }
        else
        {
            Debug.LogError("GameManager Instance is NOT Created. CANNOT fount GameState");
            return false;
        }
    }

    public GAMESTATE GetGameState()
    {
        return m_CrruntUI.m_GameState;
    }
    public void SetGameState(GAMESTATE gamestate)
    {

        switch (gamestate)
        {
            case GAMESTATE.Main:
                WorldCamera.enabled = true;
                m_WorldMap.SetActive(false);//월드맵 세팅
                break;
            case GAMESTATE.WorldMap:
                WorldCamera.enabled = true;
                m_WorldMap.SetActive(true);//월드맵 세팅
                break;
            case GAMESTATE.ThreeMatch:
                WorldCamera.enabled = false;
                m_WorldMap.SetActive(false);//월드맵 세팅
                break;
            case GAMESTATE.CarPort:
                WorldCamera.enabled = false;
                m_WorldMap.SetActive(false);//월드맵 세팅
                break;
            default:
                break;
        }



        if (m_CrruntUI != null)
        {
            if (m_CrruntUI.m_GameState != gamestate)
                m_CrruntUI.EndUI();
            else
                return;
        }

        for(int i = 0, len = m_UIRootInfo.Count; i <len; i++)
        {
            if(m_UIRootInfo[i] == null) continue;
            if(m_UIRootInfo[i].m_GameState == gamestate)
            {
                m_UIRootInfo[i].gameObject.SetActive(true);
                m_CrruntUI = m_UIRootInfo[i];
            }              
            else
                m_UIRootInfo[i].gameObject.SetActive(false);
        }

        m_CrruntUI.StartUI();  
    }

#endregion

#region 매칭 게임
    public void GameStart(int stage)
    {
        StartCoroutine(Co_GameStart(stage));

    }
    IEnumerator Co_GameStart(int stage)
    {
        //ThreeMatch쪽 Start()함수를 기다려야한다.
        yield return null;

        StageItemManager.Instance.nowplaystage = stage - 1;
        MatchManager.Instance.StartGame(stage);
    }
#endregion


    public void GPGS_LeaderBoardUpdate(int type, int value)
    {

        if (!m_GameData.LoginChk)
            return;


      

        switch (type)
        {
            case 0:
                {
                    GameManager.Instance.m_GameData.MyBestScore = value;

                    SecurityPlayerPrefs.SetInt("GPGSmybestscore", GameManager.Instance.m_GameData.MyBestScore);

#if UNITY_ANDROID
                    GPGS_Manager.Instance.ReportScore(GPGSIds.leaderboard_high_score, value);
#elif UNITY_IOS
                    GCIC_Manager.Instance.ReportScore(GPGSIds.leaderboard_high_score, value);
#endif
                }
                break;
//            case 1:
//                {
//#if UNITY_ANDROID
//                    GPGS_Manager.Instance.ReportScore(GPGSIds.leaderboard_truck_grade, value);
//#elif UNITY_IOS
//                    GCIC_Manager.Instance.ReportScore(GPGSIds.leaderboard_truck_grade, value);
//#endif
//                }
//                break;
            case 2:
                {
#if UNITY_ANDROID
                    GPGS_Manager.Instance.ReportScore(GPGSIds.leaderboard_best_stage, value);
#elif UNITY_IOS
                    GCIC_Manager.Instance.ReportScore(GPGSIds.leaderboard_best_stage, value);
#endif
                }
                break;
            case 3:
                {
#if UNITY_ANDROID
                    GPGS_Manager.Instance.ReportScore(GPGSIds.leaderboard_star_collector, value);
#elif UNITY_IOS
                    GCIC_Manager.Instance.ReportScore(GPGSIds.leaderboard_star_collector, value);
#endif
                }
                break;
        }
    }
    public void GPGS_AchivenmentCompare(int type, int value)
    {

        if (!m_GameData.LoginChk)
            return;

        bool achivementupdatechk = false;

        for (int i = 0; i < TableDataManager.Instance.m_achivement.Count; i++)
        {
            if (type == TableDataManager.Instance.m_achivement[i].type)
            {
                switch (type)
                {
                    case 0:
                        {
                            if ((i + m_GameData.NowClearStageIndex) >= TableDataManager.Instance.m_achivement.Count)
                            {
                                break;
                            }
                            if (TableDataManager.Instance.m_achivement[i + m_GameData.NowClearStageIndex].type == type && value >= TableDataManager.Instance.m_achivement[i + m_GameData.NowClearStageIndex].value)
                            {

                                achivementupdatechk = true;
                               // GPGS_AchivementUpdate(type);
                                break;
                            }
                        }
                        break;
                    case 1:
                        {
                            if ((i + m_GameData.NowMyBestTruckIndex) >= TableDataManager.Instance.m_achivement.Count)
                            {
                                break;
                            }
                            if (TableDataManager.Instance.m_achivement[i + m_GameData.NowMyBestTruckIndex].type == type && value >= TableDataManager.Instance.m_achivement[i + m_GameData.NowMyBestTruckIndex].value)
                            {
                                achivementupdatechk = true;
                                break;
                            }
                        }
                        break;
                    case 2:
                        {
                            if ((i + m_GameData.NowMyStarIndex) >= TableDataManager.Instance.m_achivement.Count)
                            {
                                break;
                            }
                            if (TableDataManager.Instance.m_achivement[i + m_GameData.NowMyStarIndex].type == type && value >= TableDataManager.Instance.m_achivement[i + m_GameData.NowMyStarIndex].value)
                            {
                                achivementupdatechk = true;
                                break;
                            }
                        }
                        break;
                    case 3:
                        {
                            if ((i + m_GameData.NowComboIndex) >= TableDataManager.Instance.m_achivement.Count)
                            {
                                break;
                            }
                            if (TableDataManager.Instance.m_achivement[i + m_GameData.NowComboIndex].type == type && value >= TableDataManager.Instance.m_achivement[i + m_GameData.NowComboIndex].value)
                            {
                                achivementupdatechk = true;
                                break;
                            }
                        }
                        break;
                    case 4:
                        {
                            if ((i + m_GameData.NowClearPointIndex) >= TableDataManager.Instance.m_achivement.Count)
                            {
                                break;
                            }
                            if (TableDataManager.Instance.m_achivement[i + m_GameData.NowClearPointIndex].type == type && value >= TableDataManager.Instance.m_achivement[i + m_GameData.NowClearPointIndex].value)
                            {
                                achivementupdatechk = true;
                                break;
                            }
                        }
                        break;
                }
            }
        }

        if (achivementupdatechk)
        {
            GPGS_AchivementUpdate(type);
        }
    }

    public void GPGS_AchivementUpdate(int type)
    {
        string achivement_str="";

        switch (type)
        {

                //clear stage
            case 0:
                {
                    switch (m_GameData.NowClearStageIndex)
                    {
                        case 0:
                            achivement_str = GPGSIds.achievement_stage_20_clear;
                            break;
                        case 1:
                            achivement_str = GPGSIds.achievement_stage_50_clear;
                            break;
                        case 2:
                            achivement_str = GPGSIds.achievement_stage_100_clear;
                            break;
                        case 3:
                            achivement_str = GPGSIds.achievement_stage_150_clear;
                            break;
                        case 4:
                            achivement_str = GPGSIds.achievement_stage_200_clear;
                            break;
                        case 5:
                            achivement_str = GPGSIds.achievement_stage_250_clear;
                            break;
                        case 6:
                            achivement_str = GPGSIds.achievement_stage_300_clear;
                            break;

                        //6.11 Added
                        //case 7:
                        //    achivement_str = GPGSIds.achievement_stage_350_clear;
                        //    break;
                        //case 8:
                        //    achivement_str = GPGSIds.achievement_stage_400_clear;
                        //    break;
                        //case 9:
                        //    achivement_str = GPGSIds.achievement_stage_450_clear;
                        //    break;
                        //case 10:
                        //    achivement_str = GPGSIds.achievement_stage_500_clear;
                        //    break;


                    }

                    m_GameData.NowClearStageIndex++;

                    SecurityPlayerPrefs.SetInt("GPGSclearstageindex", m_GameData.NowClearStageIndex);

                }
                break;
                //my best food truck
            //case 1:
            //    {
            //        switch (m_GameData.NowMyBestTruckIndex)
            //        {
            //            case 0:
            //                achivement_str = GPGSIds.achievement_food_truck_5_grade;
            //                break;
            //            case 1:
            //                achivement_str = GPGSIds.achievement_food_truck_10_grade;
            //                break;
            //            case 2:
            //                achivement_str = GPGSIds.achievement_food_truck_11_grade;
            //                break;
            //            case 3:
            //                achivement_str = GPGSIds.achievement_food_truck_12_grade;
            //                break;
            //            case 4:
            //                achivement_str = GPGSIds.achievement_food_truck_13_grade;
            //                break;
            //            case 5:
            //                achivement_str = GPGSIds.achievement_food_truck_14_grade;
            //                break;
            //            case 6:
            //                achivement_str = GPGSIds.achievement_food_truck_15_grade;
            //                break;
            //            case 7:
            //                achivement_str = GPGSIds.achievement_food_truck_16_grade;
            //                break;
            //            case 8:
            //                achivement_str = GPGSIds.achievement_food_truck_17_grade;
            //                break;
            //            case 9:
            //                achivement_str = GPGSIds.achievement_food_truck_18_grade;
            //                break;
            //            case 10:
            //                achivement_str = GPGSIds.achievement_food_truck_19_grade;
            //                break;
            //            case 11:
            //                achivement_str = GPGSIds.achievement_food_truck_20_grade;
            //                break;
            //            case 12:
            //                achivement_str = GPGSIds.achievement_food_truck_21_grade;
            //                break;
            //            case 13:
            //                achivement_str = GPGSIds.achievement_food_truck_22_grade;
            //                break;
            //            case 14:
            //                achivement_str = GPGSIds.achievement_food_truck_23_grade;
            //                break;
            //            case 15:
            //                achivement_str = GPGSIds.achievement_food_truck_24_grade;
            //                break;
            //            case 16:
            //                achivement_str = GPGSIds.achievement_food_truck_25_grade;
            //                break;
            //            case 17:
            //                achivement_str = GPGSIds.achievement_food_truck_26_grade;
            //                break;
            //            case 18:
            //                achivement_str = GPGSIds.achievement_food_truck_27_grade;
            //                break;
            //            case 19:
            //                achivement_str = GPGSIds.achievement_food_truck_28_grade;
            //                break;
            //            case 20:
            //                achivement_str = GPGSIds.achievement_food_truck_29_grade;
            //                break;
            //            case 21:
            //                achivement_str = GPGSIds.achievement_food_truck_30_grade;
            //                break;
            //        }

            //        m_GameData.NowMyBestTruckIndex++;

            //        SecurityPlayerPrefs.SetInt("GPGSmybesttruckindex", m_GameData.NowMyBestTruckIndex);
            //    }
            //    break;

                //star collection
            case 2:
                {
                    switch (m_GameData.NowMyStarIndex)
                    {
                        case 0:
                            achivement_str = GPGSIds.achievement_collect_30_stars;
                            break;
                        case 1:
                            achivement_str = GPGSIds.achievement_collect_50_stars;
                            break;
                        case 2:
                            achivement_str = GPGSIds.achievement_collect_100_stars;
                            break;
                        case 3:
                            achivement_str = GPGSIds.achievement_collect_150_stars;
                            break;
                        case 4:
                            achivement_str = GPGSIds.achievement_collect_200_stars;
                            break;
                        case 5:
                            achivement_str = GPGSIds.achievement_collect_300_stars;
                            break;
                        case 6:
                            achivement_str = GPGSIds.achievement_collect_400_stars;
                            break;
                        case 7:
                            achivement_str = GPGSIds.achievement_collect_500_stars;
                            break;
                        case 8:
                            achivement_str = GPGSIds.achievement_collect_600_stars;
                            break;

                        //case 9:
                        //    achivement_str = GPGSIds.achievement_collect_700_stars;
                        //    break;
                    }
                    m_GameData.NowMyStarIndex++;

                    SecurityPlayerPrefs.SetInt("GPGSmystarindex", m_GameData.NowMyStarIndex);
                }
                break;
                //콤보
            case 3:
                {
                    switch (m_GameData.NowComboIndex)
                    {
                        case 0:
                            achivement_str = GPGSIds.achievement_5_combo;
                            break;
                        case 1:
                            achivement_str = GPGSIds.achievement_7_combo;
                            break;
                        case 2:
                            achivement_str = GPGSIds.achievement_9_combo;
                            break;
                        case 3:
                            achivement_str = GPGSIds.achievement_12_combo;
                            break;
                        case 4:
                            achivement_str = GPGSIds.achievement_15_combo;
                            break;

                    }
                    m_GameData.NowComboIndex++;

                    SecurityPlayerPrefs.SetInt("GPGScomboindex", m_GameData.NowComboIndex);
                }
                break;
            case 4:
                {
                    switch (m_GameData.NowClearPointIndex)
                    {
                        case 0:
                            achivement_str = GPGSIds.achievement_clear_score_5000;
                            break;
                        case 1:
                            achivement_str = GPGSIds.achievement_clear_score_10000;
                            break;
                        case 2:
                            achivement_str = GPGSIds.achievement_clear_score_15000;
                            break;
                        case 3:
                            achivement_str = GPGSIds.achievement_clear_score_20000;
                            break;
                        case 4:
                            achivement_str = GPGSIds.achievement_clear_score_25000;
                            break;
                        case 5:
                            achivement_str = GPGSIds.achievement_clear_score_30000;
                            break;
                        case 6:
                            achivement_str = GPGSIds.achievement_clear_score_40000;
                            break;
                        case 7:
                            achivement_str = GPGSIds.achievement_clear_score_50000;
                            break;
                    }
                    m_GameData.NowClearPointIndex++;

                    SecurityPlayerPrefs.SetInt("GPGSclearpoint", m_GameData.NowClearPointIndex);
                }
                break;
        }
#if UNITY_ANDROID
        GPGS_Manager.Instance.TestResetupPopups();
        GPGS_Manager.Instance.ReportProgress(achivement_str, 100f);
#elif UNITY_IOS
        GCIC_Manager.Instance.ReportProgress(achivement_str, 100f);
#endif
    }


}