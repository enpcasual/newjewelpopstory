﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

//AOS와 IOS 공통 로그 출력 개선
#if !UNITY_EDITOR
public static class Debug
{
#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void NSLog_iOS(string log);
#endif

    public static bool isDebugBuild
    {
        get { return UnityEngine.Debug.isDebugBuild; }
    }
		
    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void Log(object message)
    {
#if  UNITY_ANDROID
        UnityEngine.Debug.Log(message);
#elif UNITY_IOS
        NSLog_iOS(message.ToString());
#endif
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void Log(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.Log(message, context);
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void LogError(object message)
    {
        UnityEngine.Debug.LogError(message);
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void LogError(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogError(message, context);
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void LogWarning(object message)
    {
        UnityEngine.Debug.LogWarning(message.ToString());
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void LogWarning(object message, UnityEngine.Object context)
    {
        UnityEngine.Debug.LogWarning(message.ToString(), context);
    }

	public static void LogException(Exception exception)
	{
		UnityEngine.Debug.LogException(exception);
	}

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void Break()
    {
        UnityEngine.Debug.Break();
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void DrawLine(Vector3 start, Vector3 end, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
        UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
        UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest);
    }

    //[System.Diagnostics.Conditional("DEBUG_LOG")]
    public static void Assert(bool condition)
    {
        if (!condition) throw new Exception();
    }
}
#endif