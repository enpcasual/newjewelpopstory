﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextLocalization : MonoBehaviour
{
    private UILabel Text = null;
    public int TextIndex = 0;

    public void OnEnable()
    {
        if(Text == null)
            Text = GetComponent<UILabel>();

        Text.text = TableDataManager.Instance.GetLocalizeText(TextIndex);
    }
}
