﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ScreenCapture : MonoBehaviour
{
    int height = Screen.height;
    int width = Screen.width;

    Texture2D captureTexture;
    float startTime;
    string saveDir = "ScreenShot"; // 저장 폴더 이름.

    bool draw = false;

    /*
    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 50, 100, 50), "Capture"))
        {
            StartCoroutine(screenCapture());
        }
    }
    */


    IEnumerator screenCapture()
    {
        yield return new WaitForEndOfFrame();
        captureTexture = new Texture2D(width, height, TextureFormat.RGB24, true); // texture formoat setting
        captureTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0, true); // readPixel
        captureTexture.Apply(); // readPixel data apply


    }
    IEnumerator screenCapture2()
    {
        yield return new WaitForEndOfFrame();
        captureTexture = new Texture2D(width, height, TextureFormat.RGB24, true); // texture formoat setting
        captureTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0, true); // readPixel
        captureTexture.Apply(); // readPixel data apply

        byte[] bytes = captureTexture.EncodeToPNG();

        File.WriteAllBytes(getCaptureName(), bytes);
        Debug.Log(string.Format("Capture Success: {0}", getCaptureName()));

    }
    string getCaptureName()
    {
        string dirPath = Application.dataPath + "/" + saveDir;
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }
        return string.Format("{0}/capture_{1}.png",
                             dirPath,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

}