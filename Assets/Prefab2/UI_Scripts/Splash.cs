﻿using UnityEngine;  
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Splash : MonoBehaviour
{
    public GameObject googlelogo;

    public GameObject companylogo;

    private void Awake()
    {

        //해상도 설정.. 풀스크린은 이미지가 찌그러지므로 하지 않는다.
        //이 설정을 해줘야 폰에서 앱의 화면 크기를 파악하고 아래 탭이 출력되는듯 하다.
        Screen.SetResolution(Screen.width, Screen.width * 16 / 9, false);

    }

    void Start()
    {
#if UNITY_ANDROID
        googlelogo.SetActive(true);
        companylogo.SetActive(false);
        StartCoroutine(CompanylogoStart());
#else
		googlelogo.SetActive(false);
		companylogo.SetActive(true);
		StartCoroutine(IOSStart());
#endif

    }

    IEnumerator CompanylogoStart()
    {
        yield return new WaitForSeconds(0.9f);

        googlelogo.SetActive(false);

        companylogo.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        SceneManager.LoadScene("Threematch");
    }

	IEnumerator IOSStart()
	{
		yield return new WaitForSeconds(0.1f);

		SceneManager.LoadScene("Threematch");
	}
   
    /*
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        Debug.Log("Wait to 3 Second for Splash:   " + Time.timeSinceLevelLoad);
        SceneManager.LoadScene("Threematch");
        Debug.Log("Loading 100% :" + async);
        yield return async;
    }
     * */
}
