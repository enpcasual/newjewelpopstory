﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UILabel))]
public class FontManager : MonoBehaviour
{
    private const string BMFontName = "BM JUA_TTF";
    private const string BMFontPath = "Font/BMJUA_TTF";

    private const string JandaFontName = "Janda Manatee Solid";
    private const string JandaFontPath = "Font/JandaManateeSolid";

    private const string ComfortaaFontName = "Comfortaa-Bold";
    private const string ComfortaaFontPath = "Font/Comfortaa-Bold";


    private static Dictionary<string, Font> Fonts = new Dictionary<string, Font>();

    void OnEnable()
    {
        FontSetting();
    }
    
    public void FontSetting()
    {
        UILabel label = this.GetComponent<UILabel>();
        if (label == null) return;

        //Debug.LogFormat("[FontManager] language<{0}> : font is {1}", TableDataManager.Instance.currentLanguage, label.trueTypeFont);

        switch (TableDataManager.Instance.currentLanguage)
        {
            //BMFont
            case TableDataManager.Language.KO:
                {
                    if(!label.trueTypeFont.fontNames[0].Equals(BMFontName))
                    {
                        if(!Fonts.ContainsKey(BMFontName))
                        {
                            Fonts[BMFontName] = Resources.Load(BMFontPath, typeof(Font)) as Font;
                        }
                        label.trueTypeFont = Fonts[BMFontName];
                    }
                }

                break;

            //Janda Font
            case TableDataManager.Language.EN:
            case TableDataManager.Language.JA:
            case TableDataManager.Language.ZH_CN:
            case TableDataManager.Language.ZH_TW:
            case TableDataManager.Language.DE:
            case TableDataManager.Language.FR:
                {
                    if (!label.trueTypeFont.fontNames[0].Equals(JandaFontName))
                    {
                        if (!Fonts.ContainsKey(JandaFontName))
                        {
                            Fonts[JandaFontName] = Resources.Load(JandaFontPath, typeof(Font)) as Font;
                        }
                        label.trueTypeFont = Fonts[JandaFontName];
                    }
                }
                break;
            default:
                break;

        }
    }
}
