﻿using UnityEngine;
using System.Collections;

public enum Button_Type
{
    BLUE,
    GREEN,
    RED
}

[RequireComponent(typeof(Animator))]
public class ButtonAniManager : MonoBehaviour
{
    private bool m_special_ani;
    public bool special_ani
    {
        get { return m_special_ani; }
        set { m_special_ani = value; Init(); }
    }

    public bool default_ani;

    public bool button_diplay;

    public bool button_pop;

    private Animator m_animator;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        Init();
    }
    
    public void SetPressAni()
    {
        m_animator.SetBool("BUTTON_PRESS", true);
        m_animator.Play("Button_press");
    }

    public void SetPressAniEnd()
    {
        m_animator.SetBool("BUTTON_PRESS", false);
    }

    public void ButtonDisplayEnd()
    {
        m_animator.SetBool("BUTTON_DISPLAY", false);
    }

    public void Init()
    {
        if (m_animator.GetBool("BUTTON_PRESS"))
        {
            SetPressAniEnd();
        }
        // || (button_pop && GameManager.Instance.m_specialPopchk)

        if (default_ani)
        {
            //m_animator.SetBool("IDLE_ANI", true);
            m_animator.Play("Button");
        }
        else if(special_ani)
        {
            m_animator.Play("Button_Special");
        }

        if (button_diplay)
        {
            //m_animator.SetBool("BUTTON_DISPLAY", true);
            m_animator.Play("Button_Display");
        }

        if (GameManager.Instance != null)
        {
            if (button_pop && !GameManager.Instance.m_specialPopchk)
            {
                m_animator.SetBool("BUTTON_POP", true);
            }

            if (button_pop && GameManager.Instance.m_specialPopchk)
            {
                //m_animator.SetBool("IDLE_ANI", true);
                m_animator.Play("Button");
            }
        }
    }

    public void EndButtonPop()
    {
        m_animator.SetBool("BUTTON_POP", false);

        default_ani = !special_ani;

        button_pop = false;

        Init();
    }
}
