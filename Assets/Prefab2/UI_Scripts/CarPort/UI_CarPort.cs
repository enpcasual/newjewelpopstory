﻿using UnityEngine;
using System.Collections;

public class UI_CarPort : UI_Base {


    public static UI_CarPort Instance;

    public CarPortManager m_carportmanager;

    void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void StartUI()
    {
        FirebaseManager.Instance.LogScreen("CarPort Screen");

        CarPortManager.Instance.m_truckshop.gameObject.SetActive(true);

        

        if (StageItemManager.Instance.TotalnowStage < 100)
        {
            CarPortManager.Instance.m_truckshop.gameObject.SetActive(false);
        }

        if (GameManager.Instance.m_startCarport)
        {
            m_carportmanager.MyFoodTruckSetting();
        }
        else
        {
            m_carportmanager.Init();
        }

        for (int i = 0; i < CarPortManager.Instance.m_MyFoodTruckList.Count; i++)
        {
            CarPortManager.Instance.m_MyFoodTruckList[i].GetComponent<CarPortTruck>().StopTruck();
        }
        CarPortManager.Instance.m_myBestFoodTruck.GetComponent<CarPortTruck>().StopTruck();

       

        GameManager.Instance.m_startCarport = false;
    }

    public override void EndUI()
    {

    }

    public void MoveToWorldMap()
    {

        SoundManager.Instance.PlayEffect("button_click");

        if (GameManager.Instance.playstatechk || GameManager.Instance.gameresultchk)
        {
            //gameObject.SetActive(false);

            GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);

            if (GameManager.Instance.gameresultchk)
            {

                Popup_Manager.INSTANCE.popup_list[3].GetComponent<GameResultPop>().MyFoodTruckChange();
               

                Popup_Manager.INSTANCE.popup_list[3].GetComponent<GameResultPop>().playbuttondisplay();
            }
            else if (GameManager.Instance.playstatechk)
            {
                Popup_Manager.INSTANCE.popup_index = 14;
                Popup_Manager.INSTANCE.Popup_Push();
            }
    
        }
        else
        {
            GameManager.Instance.SetGameState(GAMESTATE.WorldMap);
        }
        

        GameManager.Instance.playstatechk = false;

        GameManager.Instance.gameresultchk = false;
    }
}
