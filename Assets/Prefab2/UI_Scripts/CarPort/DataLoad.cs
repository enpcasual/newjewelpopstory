﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Text;
using System;

public class DataLoad : MonoBehaviour {

    string m_strName = "FoodTruck.xml";

    public GameObject Inventory;

    public GameObject CarPort;


    void Start()
    {
        StartCoroutine(Process());

    }

    IEnumerator Process()
    {
        string strPath = string.Empty;

#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        strPath +=("file://");
        strPath += (Application.streamingAssetsPath + "/" + m_strName);
#elif UNITY_ANDROID
        strPath = "jar:file://" + Application.dataPath + "!/assets/" + m_strName;
#endif
        WWW www = new WWW(strPath);

        yield return www;

        Debug.Log("Read Contents: " + www.text);

        Interpret(www.text);

       // CarPort.SetActive(true);
    }

    void Interpret(string _strSource)
    {
        StringReader stringReader = new StringReader(_strSource);

        stringReader.Read();    // BOM 제거 한 데이터로 파싱해요.

        XmlNodeList xmlNodeList = null;

        XmlDocument xmlDoc = new XmlDocument();
        try
        {
            // XML 로드하고.
            xmlDoc.LoadXml(stringReader.ReadToEnd());
        }
        catch (Exception e)
        {
            xmlDoc.LoadXml(_strSource);
        }
        // 최 상위 노드 선택.
        xmlNodeList = xmlDoc.SelectNodes("FoodTruck");

        foreach (XmlNode node in xmlNodeList)
        {
            if (node.Name.Equals("FoodTruck") && node.HasChildNodes)
            {
                foreach (XmlNode child in node.ChildNodes)
                {

                    ItemInfo iteminfo = new ItemInfo();

                    iteminfo.ID = int.Parse(child.Attributes.GetNamedItem("id").Value);

                    iteminfo.NAME = child.Attributes.GetNamedItem("name").Value;

                    iteminfo.ICON = child.Attributes.GetNamedItem("icon").Value;

                    iteminfo.BUY_COST = int.Parse(child.Attributes.GetNamedItem("price").Value);



                    //FoodTruckManager.INSTANCE.AddItem(iteminfo);

                }
            }
        }

       
    }
}
