﻿using UnityEngine;

/// <summary>
/// Selectable sprite that follows the mouse.
/// </summary>

[RequireComponent(typeof(UISprite))]
public class CursorManager : MonoBehaviour
{
    static public CursorManager instance;

    // Camera used to draw this cursor
    public Camera uiCamera;

    Transform mTrans;
    public UISprite mSprite;

    public FoodTruckinfo m_Item;

    void Awake() { instance = this; }
    void OnDestroy() { instance = null; }

    public int m_truckindex;

    /// <summary>
    /// Cache the expected components and starting values.
    /// </summary>

    void Start()
    {
        m_truckindex = -1;

        mTrans = transform;
        mSprite = GetComponent<UISprite>();

        if (uiCamera == null)
            uiCamera = NGUITools.FindCameraForLayer(gameObject.layer);
    }

    /// <summary>
    /// Reposition the widget.
    /// </summary>

    void Update()
    {
        Vector3 pos = Vector3.zero;
        if (Application.platform == RuntimePlatform.Android ||
           Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount > 0)
            {
                pos = Input.GetTouch(0).position;
            }
            else
            {
                pos = Vector3.zero;
            }
        }
        else
        {
            pos = Input.mousePosition;
        }


        if (uiCamera != null)
        {
            // Since the screen can be of different than expected size, we want to convert
            // mouse coordinates to view space, then convert that to world position.
            pos.x = Mathf.Clamp01(pos.x / Screen.width);
            pos.y = Mathf.Clamp01(pos.y / Screen.height);

            
            if (pos.x < 0.15f)
            {
                pos.x = 0.15f;
            }
            else if (pos.x > 0.85f)
            {
                pos.x = 0.85f;
            }

            if (pos.y > 0.73f)
            {
                pos.y = 0.73f;
            }
            else if (pos.y < 0.2f)
            {
                pos.y = 0.2f;
            }


            //Debug.Log("pos.x:::" + pos.x + "  pos.y::::::::" + pos.y);


            mTrans.position = uiCamera.ViewportToWorldPoint(pos);

            // For pixel-perfect results
            if (uiCamera.orthographic)
            {
                Vector3 lp = mTrans.localPosition;
                lp.x = Mathf.Round(lp.x);
                lp.y = Mathf.Round(lp.y);
                mTrans.localPosition = lp;
            }
        }
        else
        {
            // Simple calculation that assumes that the camera is of fixed size
            pos.x -= Screen.width * 0.5f;
            pos.y -= Screen.height * 0.5f;
            pos.x = Mathf.Round(pos.x);
            pos.y = Mathf.Round(pos.y);
            mTrans.localPosition = pos;
        }
    }

    /// <summary>
    /// Clear the cursor back to its original value.
    /// </summary>

    void OnTriggerStay(Collider other)
    {
        //Debug.Log("other.gameobject.name::::::" + other.gameObject.name);

       

        if (instance.mSprite.enabled)
        {
            m_truckindex = -1;

            if (other.tag == "FoodTruck")
            {
                m_truckindex = other.gameObject.GetComponent<TruckScript>().Gettruckindex();


            }
           // Debug.Log("m_truckindex:::" + m_truckindex + " other.tag::::::::" + other.tag);
        }
       
    }

    void OnTriggerExit(Collider other)
    {
        m_truckindex = -1;

        //Debug.Log("OnTriggerExit  ::::::" + other.gameObject.name);

    }

    


    public void OnClick()
    {

      //  CursorTruckDrop();

       
    }


    public void DragOVer()
    {
       // Debug.Log("DragOVer::::::::::::::::::::::::::");
    }

    public void DragOut()
    {
       // Debug.Log("DragOut::::::::::::::::::::::::::");
    }

    public void DragEnd()
    {
       // Debug.Log("DragEnd::::::::::::::::::::::::::");
    }

#if (UNITY_ANDROID||UNITY_IPHONE) && !UNITY_EDITOR
	
   public void OnDrop(GameObject dropped)
    {

        Debug.Log("OnDrop::::::::::::::::::::::::::");
        CursorReleas();


    }
#endif

    public void CursorReleas()
    {

        SoundManager.Instance.PlayEffect("button_click");

        if (instance.mSprite.enabled)
        {
            
            if (m_truckindex == -1)
            {
                
                CarPortManager.Instance.CursorTruckDrop();
                Clear();
            }
            else
            {
               
                for (int i = 0; i < CarPortManager.Instance.m_MyFoodTruckList.Count; i++)
                {
                    if (CarPortManager.Instance.m_MyFoodTruckList[i].index == m_truckindex)
                    {
                       

                        CarPortManager.Instance.m_MyFoodTruckList[i].CalculateTruck();
                        break;
                    }
                }
            }
        }
        
    }
    public void Clear()
    {
        if (instance != null)
            Set("");
    }

    /// <summary>
    /// Override the cursor with the specified sprite.
    /// </summary>
    static public void Set(string spriteName)
    {
        if (instance != null && instance.mSprite)
        {
            if (spriteName == "")
            {
                instance.mSprite.enabled = false;
                
                return;
            }
           
            instance.mSprite.enabled = true;
            instance.mSprite.spriteName = spriteName;
            instance.mSprite.MakePixelPerfect();
            instance.Update();
        }
    }



}