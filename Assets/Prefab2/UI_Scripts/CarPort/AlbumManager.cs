﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class AlbumManager: MonoBehaviour {



   // List<string> m_ItemNames = new List<string>();

    List<CarPortTruck> m_Items = new List<CarPortTruck>();

    public GameObject m_gObjSampleItem;

    public  UIScrollView m_scrollView;

    public UIGrid m_grid;

    CarPortTruck m_selectItem;
        
    int m_totalGold;

    public UILabel TruckinfoTxt;

    public SelectTrunkInfo selectTruckInfo;

    public CarPortManager m_carPortManager;

    public TweenPosition m_twpos;



   
    void Awake()
    {

       
    }

	// Use this for initialization
	void Start () {


        Debug.Log("Start::::::::::::::::::::::");

        m_selectItem = null;

      //  m_SellBtn.SetActive(false);

       // m_iteminfotxt.text = string.Empty;


      //  Debug.Log("ItemSetting::::::::::::::::::::::");
        ItemSetting();
	}

    /*
    public void ResetTween()
    {

        Debug.Log("Tween resetting:::::::::::::::::::::");
        m_twpos.ResetToBeginning();
        m_twpos.enabled = true;
        m_twpos.PlayForward();
    }
    */
	// Update is called once per frame
	void Update () {
	    // I 키를 누르면 아이템이 추가됩니다.
	    if(Input.GetKeyDown(KeyCode.I))
        {
            //AddItem();
        }
        // C키를 누르면 모두 삭제합니다.
        if(Input.GetKeyDown(KeyCode.C))
        {
            ClearAll();
        }
        // R키를 눌르면 랜덤으로 하나가 삭제됩니다.
        if(Input.GetKeyDown(KeyCode.R))
        {
            ClearRand();
        }
	}

    public void SelectedItem(CarPortTruck itemscript)
    {
        m_selectItem = itemscript;

        for (int i = 0; i < m_Items.Count; i++)
        {
            if (m_selectItem == m_Items[i])
            {
               // m_Items[i].SetSelected(true);
            }
            else
            {
                //m_Items[i].SetSelected(false);
            }
        }

       // m_iteminfotxt.text = "이름 " + m_selectItem.m_Item.NAME + "\n 판매금액:" + m_selectItem.m_Item.SELL_COST.ToString();

        //m_SellBtn.gameObject.SetActive(true);
        selectTruckInfo.gameObject.SetActive(true);

       // selectTruckInfo.SetInfo(m_selectItem.m_Item);

     
    }
    public void CloseSelectedPopup()
    {
        selectTruckInfo.gameObject.SetActive(false);
    }

    public void GotoCarPort()
    {
        
        m_carPortManager.gameObject.SetActive(true);
        //m_carPortManager.AddFoodTruck(1);
        //m_carPortManager.ResettingMyFoodTruck();
        this.gameObject.SetActive(false);
    }
    void ItemSetting()
    {

        //Debug.Log("ItemManager.INSTANCE.getItemCount()::::::::::::::::::::::" + ItemManager.INSTANCE.getItemCount());
        for (int i = 0; i < TableDataManager.Instance.m_Foodtruck.Count; i++)
        {
            AddItem(i+1);
        }
    }


    public void AddItem(int index)
    {
        //int RandomIndex = Random.Range(1, ItemManager.INSTANCE.getItemCount()+1);

        GameObject gObjItem = NGUITools.AddChild(m_grid.gameObject, m_gObjSampleItem);

        gObjItem.SetActive(true);

              

        m_grid.Reposition();

        m_scrollView.ResetPosition();

           }


    public void Sell()
    {
        for (int i = 0; i < m_Items.Count; i++)
        {
            if (m_selectItem == m_Items[i])
            {
                DestroyImmediate(m_Items[i].gameObject);
                m_Items.RemoveAt(i);
            }
        }

       // m_totalGold += m_selectItem.m_Item.SELL_COST;
        
        m_selectItem = null;

        m_grid.Reposition();

        m_scrollView.ResetPosition();
        


    }

    void ClearAll()
    {
        for (int index = 0; index < m_Items.Count; index++)
        {
            if (m_Items[index] != null && m_Items[index].gameObject != null)
            {
                Destroy(m_Items[index].gameObject);
            }
        }

        m_Items.Clear();

        m_grid.Reposition();

        m_scrollView.ResetPosition();
    }

    void ClearRand()
    {
        if (m_Items.Count == 0) return;

        int Randomindex = Random.Range(0, m_Items.Count);

        DestroyImmediate(m_Items[Randomindex].gameObject);

        m_Items.RemoveAt(Randomindex);

        m_grid.Reposition();

        m_scrollView.ResetPosition();

    }

    void OnDestroy()
    {
        for (int nIndex = 0; nIndex < m_Items.Count; nIndex++)
        {
            // 혹시 모르니까 null이 아닐 때에만 삭제하도록 해요.
            if (m_Items[nIndex] != null && m_Items[nIndex].gameObject != null)
                Destroy(m_Items[nIndex].gameObject);
        }

        m_Items.Clear();
        m_Items = null;

       // m_ItemNames.Clear();
      //  m_ItemNames = null;
    }
	
}
