﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodTruckManager : MonoBehaviour {

    static FoodTruckManager instance;

    static object m_pLock = new Object();

    /*
    public static FoodTruckManager INSTANCE
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(FoodTruckManager)) as FoodTruckManager;
            }
            if (instance == null)
            {
                GameObject obj = new GameObject("FoodTruckManager");

                instance = obj.AddComponent(typeof(FoodTruckManager)) as FoodTruckManager;

                DontDestroyOnLoad(instance);
            }


            return instance;
        }
    }
     * */

    public Dictionary<int, ItemInfo> m_dicData= new Dictionary<int, ItemInfo>();


    public void AddItem(ItemInfo iteminfo)
    {
        //현재 아이템딕셔너리에 같은게 있으면 리턴
        if (m_dicData.ContainsKey(iteminfo.ID)) return;

        m_dicData.Add(iteminfo.ID,iteminfo);
    }

    public ItemInfo getItem(int m_ID)
    {
        if(m_dicData.ContainsKey(m_ID))
        return m_dicData[m_ID];

        return null;

    }

    public Dictionary<int, ItemInfo> getAllItems()
    {
        return m_dicData;
    }

    public int getItemCount()
    {
        return m_dicData.Count;
    }

   
}

public class ItemInfo
{
    int m_nID;
    string m_strName;
    string m_strIcon;
    int m_BuyCost;
  

    public int ID
    {
        set
        {
            m_nID = value;
        }
        get
        {
            return m_nID;
        }
    }

    public string NAME
    {
        set
        {
            m_strName = value;
        }
        get
        {
            return m_strName;
        }
    }

    public string ICON
    {
        set
        {
            m_strIcon = value;
        }
        get
        {
            return m_strIcon;
        }
    }

    public int BUY_COST
    {
        set
        {
            m_BuyCost = value;
        }
        get
        {
            return m_BuyCost;
        }
    }

    

}
