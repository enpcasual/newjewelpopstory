﻿using UnityEngine;
using System.Collections;

public class SelectTrunkInfo : MonoBehaviour {

    public UILabel truckinfoTxt;

    public UISprite selectedTruck;

    ItemInfo m_Item;

    public void SetInfo(ItemInfo iteminfo)
    {
        m_Item = iteminfo;

        selectedTruck.spriteName = m_Item.ICON;

        truckinfoTxt.text = m_Item.NAME;
        // m_strName = spriteName;
    }


}
