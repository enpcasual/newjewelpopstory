﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarPortManager : MonoBehaviour
{
    public static CarPortManager Instance;

    public GameObject album;

    public List<CarPortTruck> m_MyFoodTruckList = new List<CarPortTruck>();

    public List<int> m_myFoodTruckindex = new List<int>();

    public CarPortTruck m_myBestFoodTruck;

    public CarPortTruck FoodTruck;


    public int m_nowTruckIndex;

    public UISprite SuperSaleIcon;

    public UILabel saletime;

    public UILabel m_mytruckname;

    public List<UISprite> combinespr = new List<UISprite>();

    int combineindex;

    public Vector3 combinePos;

    public bool m_mybesttruckclickchk;

    public UISprite m_truckshop;

    void Awake()
    {
        Instance = this;
    }
    
    void Start()
    {
        // ItemInfo tempitem = FoodTruckManager.INSTANCE.getItem(GameManager.Instance.m_MyFoodTruckGrade);

        //-100.
    }


    public void truckcombineeffect(Vector3 pos)
    {
        combinePos = pos;

        StartCoroutine(combineeffect());
    }

    IEnumerator combineeffect()
    {
        combineindex = 0;

        for (int i = 0; i < combinespr.Count; i++)
        {
            combinespr[i].gameObject.SetActive(false);

            combinespr[i].gameObject.transform.position = combinePos;
        }

        combinespr[0].gameObject.SetActive(true);
        yield return new WaitForSeconds(0.07f);

        combinespr[0].gameObject.SetActive(false);
        combinespr[1].gameObject.SetActive(true);
        yield return new WaitForSeconds(0.07f);


        combinespr[1].gameObject.SetActive(false);
        combinespr[2].gameObject.SetActive(true);
        yield return new WaitForSeconds(0.07f);

        combinespr[2].gameObject.SetActive(false);
    }

    public void trucksaletimedisplay(string time)
    {
        saletime.text = time;
    }

    public void DropTruck()
    {

    }
    
    public void Init()
    {
        FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade];


        m_myBestFoodTruck.SetInfo(tempitem);


        m_mytruckname.text = TableDataManager.Instance.GetLocalizeText(58 + GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade);


        m_mybesttruckclickchk = false;

    }

    public void MyfoodTruckNameChange()
    {
        m_mytruckname.text = TableDataManager.Instance.GetLocalizeText(58 + GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade);
    }

    public void MyFoodTruckSetting()
    {
        Init();

        //초기화
        for (int i = 0; i < m_MyFoodTruckList.Count; i++)
        {
            Destroy(m_MyFoodTruckList[i].gameObject);
        }
        m_MyFoodTruckList.Clear();



        for (int i = 0; i < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; i++)
        {
            CarPortTruck item = Instantiate(FoodTruck) as CarPortTruck;

            FoodTruckinfo temptruck = new FoodTruckinfo();

            temptruck = TableDataManager.Instance.m_Foodtruck[GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[i].truckgrade];
            
            item.gameObject.SetActive(true);

            item.index = m_nowTruckIndex;

            item.SetInfo(temptruck);
            
            item.transform.parent = this.transform;
            
            item.gameObject.transform.localPosition = new Vector3(Random.Range(-170, 170), Random.Range(-340, 250), 0);
            
            item.transform.localScale = new Vector3(0.6f, 0.6f, 0);
            
            GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[i].truckindex = m_nowTruckIndex;

            m_MyFoodTruckList.Add(item);
            
            m_nowTruckIndex++;
        }
    }


    public void mybesttruckclick()
    {
        //m_mybesttruckclickchk = true;

        //Popup_Manager.INSTANCE.popup_index = 25;

        //Popup_Manager.INSTANCE.Popup_Push();
    }

    public void tutorialturcksetting()
    {
        return;

        //StartCoroutine(trucktutorialstart());
    }

    //IEnumerator trucktutorialstart()
    //{

    //    m_MyFoodTruckList[0].gameObject.transform.localPosition = new Vector3(-100, 0, 0);

    //    m_MyFoodTruckList[1].gameObject.transform.localPosition = new Vector3(100, 0, 0);


    //    yield return new WaitForSeconds(0.5f);


    //    Popup_Manager.INSTANCE.popup_index = 29;
    //    Popup_Manager.INSTANCE.Popup_Push();
    //}

    //푸드트럭 추가한다
    public void AddFoodTruck(int Grade)
    {

        if (GameManager.Instance.m_startCarport)
        {
            MyFoodTruckSetting();
        }
        GameManager.Instance.m_startCarport = false;

        CarPortTruck item = Instantiate(FoodTruck) as CarPortTruck;

        FoodTruckinfo temptruckinfo = new FoodTruckinfo();

        temptruckinfo = TableDataManager.Instance.m_Foodtruck[Grade];

        item.index = m_nowTruckIndex;

        item.gameObject.SetActive(true);

        item.SetInfo(temptruckinfo);

        item.transform.parent = this.transform;

        item.gameObject.transform.localPosition = new Vector3(Random.Range(-170, 170), Random.Range(-340, 250), 0);

        item.transform.localScale = new Vector3(0.6f, 0.6f, 0);

        item.GetComponent<CarPortTruck>().StopTruck();

        m_MyFoodTruckList.Add(item);

        MyFoodTruckChange(temptruckinfo);

        Mytruck temptruck = new Mytruck();

        temptruck.truckgrade = Grade;
        temptruck.truckindex = m_nowTruckIndex;

        GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Add(temptruck);

        DataManager.SaveToPlayerPrefs<myfoodtruckdata>("myfoodtruckdata", GameManager.Instance.m_GameData.MyFoodTruckData);



        m_nowTruckIndex++;


    }

    public void TestAddFoodTruck()
    {


        AddFoodTruck(GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade);
        // AddFoodTruck(0);

    }

    //드랍된 원래 트럭은 빈 트럭으로처리한다
    public void FoodTruckDestroy()
    {
        int index = 0;
        for (int i = 0; i < m_MyFoodTruckList.Count; i++)
        {
            if (m_MyFoodTruckList[i].m_isFoodTruckMove)
            {

                for (int j = 0; j < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; j++)
                {
                    if (m_MyFoodTruckList[i].index == GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[j].truckindex)
                    {
                        index = i;

                        GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.RemoveAt(j);
                    }
                }




                break;
            }
        }


        Destroy(m_MyFoodTruckList[index].gameObject);

        m_MyFoodTruckList.RemoveAt(index);
    }
    //현재 가장 좋은 푸드트럭을 정한다
    public bool MyFoodTruckChange(FoodTruckinfo iteminfo)
    {
        bool isReturn = false;



        if (m_myBestFoodTruck.m_Item.grade < iteminfo.grade)
        {
            isReturn = true;

            m_myBestFoodTruck.SetInfo(iteminfo);


            GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade = iteminfo.grade;


            //if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.CarPort)
            //{
            //    m_myBestFoodTruck.gameObject.SetActive(false);

            //    if (!GameManager.Instance.m_randombestgrade)
            //    {
            //        Popup_Manager.INSTANCE.popup_index = 25;

            //        Popup_Manager.INSTANCE.Popup_Push();
            //    }


            //}

            GameManager.Instance.GPGS_LeaderBoardUpdate(1, GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade + 1);

            GameManager.Instance.GPGS_AchivenmentCompare(1, GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade + 1);
        }

        return isReturn;
    }

    public void CursorTruckDrop()
    {
        for (int i = 0; i < m_MyFoodTruckList.Count; i++)
        {
            if (m_MyFoodTruckList[i].m_isFoodTruckMove)
            {
                m_MyFoodTruckList[i].m_isFoodTruckMove = false;

                m_MyFoodTruckList[i].gameObject.SetActive(true);

                //m_MyFoodTruckList[i].IdleTruck();

                m_MyFoodTruckList[i].gameObject.transform.localPosition = CursorManager.instance.gameObject.transform.localPosition;



                break;
            }
        }
    }

    public void MoveToAlbum()
    {
        Popup_Manager.INSTANCE.popup_index = 21;

        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void supersalepop()
    {
        //SoundManager.Instance.PlayEffect("button_click");

        //Popup_Manager.INSTANCE.popup_index = 20;

        //Popup_Manager.INSTANCE.Popup_Push();
    }

    public void truckshopClick()
    {
        SoundManager.Instance.PlayEffect("button_click");

        Popup_Manager.INSTANCE.popup_index = 36;

        Popup_Manager.INSTANCE.Popup_Push();
    }
}
