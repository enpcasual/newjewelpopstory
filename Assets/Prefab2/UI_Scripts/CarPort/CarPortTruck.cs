﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CarPortTruck : MonoBehaviour {


    public FoodTruckinfo m_Item;

    public UISprite m_sprIcon;

    //public CarPortManager m_carPortManager;
    
    //public CursorManager m_cursorManager;

    public bool m_isMyTruck;// 현재 보유한 푸드트럭인가??



    public bool m_isFoodTruckMove = false;//지금 커서로 이동하는 푸드트럭이 자신의 것인지를 체크(다른 푸드트럭과 결합시 조건이 맞지 않을때 원상태로 복귀)

    public Animator ani;

    public int index;

    public UILabel gradetxt;

    

    public void SetInfo(FoodTruckinfo iteminfo,bool useFixelPerfect = true)
    {

        ani = GetComponentInChildren<Animator>();//GetComponent<Animator>();

       
        m_Item = iteminfo;

        m_isMyTruck = false;//

        if (iteminfo == null)
        {
            m_sprIcon.enabled = false;
        }
        else
        {
            m_isMyTruck = true;//현재 보유한 드럭이다
            m_sprIcon.enabled = true;
            m_sprIcon.spriteName = m_Item.grade.ToString();

            if (useFixelPerfect)
                m_sprIcon.MakePixelPerfect();

        }

       
        if (gradetxt != null)
        {
            gradetxt.gameObject.SetActive(false);

            gradetxt.text = index.ToString();
            //gradetxt.text = (m_Item.grade + 1).ToString();
        }


    }

    /*
    public void OnClick()
    {
#if UNITY_EDITOR || UNITY_WEBPLAYER
        //Calculate();
        CalculateTruck();
#endif
    }
#if (UNITY_ANDROID||UNITY_IPHONE) && !UNITY_EDITOR
	void OnPress(bool isDown)
	{
		if(isDown)
			CalculateTruck();
	}
	
	void OnDrop (GameObject go)
	{
		CalculateTruck();
	}
#endif
    */
    /*
    public void Calculate()
    {
        if (m_cursorManager.m_Item != null)
        {
            // cursor - exist / slot - none		==> drop(equipt)
            if (m_Item == null)
            {
       
                
               m_carPortManager.ReturnFoodTruck(m_cursorManager.m_Item);

                m_cursorManager.m_Item = null;

                UpdateCursor();
                
            }
            // cursor - exist / slot - exist	==> replace
            else
            {

                if (m_cursorManager.m_Item != null && m_Item.grade == m_cursorManager.m_Item.grade && m_Item.grade<30)
                {
                    //이동하기전 위치의 푸드트럭 없애기
                    m_carPortManager.FoodTruckDestroy();


                    m_Item = TableDataManager.Instance.m_Foodtruck[m_Item.grade];

                    if (m_carPortManager.MyFoodTruckChange(m_Item))
                    {

                        m_Item = null;

                        SetInfo(m_Item);

                        m_cursorManager.m_Item = null;

                        UpdateCursor();
                    }
                    else
                    {
                       

                        m_cursorManager.m_Item = null;

                        SetInfo(m_Item);

                        UpdateCursor();
                    }
                   


                }
                else
                {
                    m_carPortManager.ReturnFoodTruck(m_cursorManager.m_Item);

                    m_cursorManager.m_Item = null;
                                       
                    UpdateCursor();

                    
                     
                }
               
            }
        }
        // cursor - none / slot - exist		==> pickup
        else if (m_Item != null)
        {
            
            m_cursorManager.m_Item = m_Item;

            m_isFoodTruckMove = true;

            m_Item = null;

            SetInfo(m_Item);

            UpdateCursor();
           

           
            
        }
    }
    */
    
    public void CalculateTruck()
    {
        if (CursorManager.instance.mSprite.enabled)
        {
       
            CursorManager.instance.mSprite.enabled = false;
          
            if (m_Item.grade ==  CursorManager.instance.m_Item.grade && m_Item.grade < 29)
            {
                if (!GameManager.Instance.m_GameData.TruckTutorialChk )
                {
                    Popup_Manager.INSTANCE.popup_list[29].GetComponent<FoodTruckTutorial2>().FingerEnd();

                    Invoke("FoodTruckCombine",0.5f);
                }
                else
                {
                    FoodTruckCombine();
                }

                
                                       
              


            }//다른 등급이면 그냥 드랍
            else if (m_Item.grade >= 29 || m_Item.grade != CursorManager.instance.m_Item.grade)
            {
                CarPortManager.Instance.CursorTruckDrop();
                CursorManager.instance.Clear();

            }
            else
            {
                Debug.Log("drop에 문제가 있음!!!!!");
            }

        }
        else 
        {
            SoundManager.Instance.PlayEffect("button_click");

            CursorManager.instance.m_Item = m_Item;

            m_isFoodTruckMove = true;

            gameObject.SetActive(false);

            CursorManager.instance.mSprite.enabled = true;

            CursorManager.instance.m_truckindex = -1;

             UpdateCursor();

            

        }
    }

    public void  FoodTruckCombine()
    {


        SoundManager.Instance.PlayEffect("car_up");
        

        CarPortManager.Instance.truckcombineeffect(gameObject.transform.position);

                   
        m_Item = TableDataManager.Instance.m_Foodtruck[m_Item.grade + 1];

      

        CarPortManager.Instance.MyFoodTruckChange(m_Item);

        StopTruck();

        SetInfo(m_Item);

        for (int j = 0; j < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; j++)
        {
            if (index == GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[j].truckindex)
            {
                GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[j].truckgrade = m_Item.grade;

                break;
            }
        }

        

        
        

        UpdateCursor();

        //이동하기전 위치의 푸드트럭 없애기
        CarPortManager.Instance.FoodTruckDestroy();


        DataManager.SaveToPlayerPrefs<myfoodtruckdata>("myfoodtruckdata", GameManager.Instance.m_GameData.MyFoodTruckData);

       

    }
    public void UpdateCursor()
    {
        if (CursorManager.instance.mSprite.enabled )
        {
            CursorManager.Set((CursorManager.instance.m_Item.grade).ToString());

        }
        else
        {
            CursorManager.instance.Clear();
        }
    }

  
    public void IdleTruck()
    {
        ani.SetBool("IDLE_CHK", true);
    }

    public void MoveTruck()
    {
        ani.SetBool("MOVE_CHK", true);
    }

    public void StopTruck()
    {
        ani.SetBool("STOP_CHK", true);
    }

    public void MoveTo(Vector3 from, Vector3 to, System.Action onComplete)
    {
        if (onComplete == null)
        {
            transform.localPosition = from;
            transform.DOLocalMove(to, 0.5f).SetEase(Ease.OutQuad);
        }
        else
        {
            transform.localPosition = from;
            transform.DOLocalMove(to, 0.5f).SetEase(Ease.OutQuad).OnComplete(new TweenCallback(onComplete));
        }
    }
}
