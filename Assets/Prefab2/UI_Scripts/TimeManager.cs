﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class TimeManager : Singleton<TimeManager>
{
    long m_supersaleremaintime;

    public long m_trucksaleremaintime;

   public long m_missionremaintime;

   public long m_freecointime;

    public string strTime;

    public float duringtime = 86400f;

    public UILabel worldmapsaletime;
    
    public UILabel shopsaletiem;

    public bool supersaletimechk;//슈퍼세일중인가?

    public bool trucksaletimechk;//푸드트럭 판매중인가?

    public bool missiontimechk;//미션실행중인가?

    public int notification;

    public bool freecoinchk;

    public bool timemanagerInitchk;

    
    //void Start()
    //{

    //}

    public void Init()
    {


        timemanagerInitchk = true;

        m_supersaleremaintime = SecurityPlayerPrefs.GetLong("SuperSaleTime", 0);

        m_missionremaintime = SecurityPlayerPrefs.GetLong("MissionTime", 0);


        m_trucksaleremaintime = SecurityPlayerPrefs.GetLong("TruckSaleTime", 0);

        m_freecointime = SecurityPlayerPrefs.GetLong("FreeCoinTime", 0);

        //GamePlaychk("GamePlayChk");


       

        optionSetting();

        //supersaletimechk = false;

        trucksaletimechk = false;

        missiontimechk = false;

        freecoinchk = false;

        if (!CheckCoolTime(m_supersaleremaintime))
        {
            //worldmapsaletime.gameObject.SetActive(true);
            //shopsaletiem.gameObject.SetActive(true);
            //supersaletimechk = true;

        }
       if (!CheckCoolTime(m_missionremaintime))
        {
            missiontimechk = true;



        }

      
       if (!CheckCoolTime(m_trucksaleremaintime))
        {
            trucksaletimechk = true;
        }

       if (!CheckCoolTime(m_freecointime))
       {
            if (AdManager.Instance.IsAdReady)
            {
                freecoinchk = true;
           }
           
       }

            /*
        else
        {
            worldmapsaletime.gameObject.SetActive(false);
            shopsaletiem.gameObject.SetActive(false);
            supersaletimechk = false;
        }
        */

       timechk();

      
       
     
       
    }

    public void optionSetting()
    {
        //effect_snd = SecurityPlayerPrefs.GetInt("effect_snd", 1);

       // bg_snd = SecurityPlayerPrefs.GetInt("bg_snd", 1);

        notification = SecurityPlayerPrefs.GetInt("notification", 1);

       
    }

    public bool AddFreeCheck()
    {
        System.DateTime rewardTime = new System.DateTime();


        if (System.DateTime.TryParse(PlayerPrefs.GetString("AddFreeDate"), out rewardTime) == false)
        {
            return true;
        }

        int result = DateTime.Compare(rewardTime, System.DateTime.Now);


        return rewardTime < System.DateTime.Now;
    }

    public void AddFreeSetting()
    {


        // System.DateTime alarmTime = System.DateTime.Now.AddDays(1);

        //System.DateTime dailytime = new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, DateTime.Now.Day + 1, 10, 0, 0);

        System.DateTime dailytime = System.DateTime.Now.AddDays(1);

        PlayerPrefs.SetString("AddFreeDate", dailytime.ToString("yyyy/MM/dd HH:mm:ss"));
        PlayerPrefs.Save();

    }

    public bool SuperSaleDayChk()
    {
        System.DateTime rewardTime = new System.DateTime();


        if (System.DateTime.TryParse(PlayerPrefs.GetString("SuperSaleDay"), out rewardTime) == false)
        {
            return true;
        }

        int result = DateTime.Compare(rewardTime, System.DateTime.Now);


        return rewardTime < System.DateTime.Now;
    }

    public void SuperSaleDaySetting()
    {


       

        System.DateTime dailytime = System.DateTime.Now.AddDays(5);

        //System.DateTime dailytime = System.DateTime.Now.AddMinutes(5);

        PlayerPrefs.SetString("SuperSaleDay", dailytime.ToString("yyyy/MM/dd HH:mm:ss"));
        PlayerPrefs.Save();

    }


    public bool MissionDayChk()
    {
        System.DateTime rewardTime = new System.DateTime();


        if (System.DateTime.TryParse(PlayerPrefs.GetString("MissionDay"), out rewardTime) == false)
        {
            return true;
        }

        int result = DateTime.Compare(rewardTime, System.DateTime.Now);


#if ENABLE_CHEAT
        return true;
#endif

        return rewardTime < System.DateTime.Now;
    }

    public void MissionDaySetting()
    {


        // System.DateTime alarmTime = System.DateTime.Now.AddDays(1);

        //System.DateTime dailytime = new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, DateTime.Now.Day + 1, 10, 0, 0);

       // System.DateTime dailytime = System.DateTime.Now.AddMinutes(3);

        System.DateTime dailytime = System.DateTime.Now.AddDays(3);

        PlayerPrefs.SetString("MissionDay", dailytime.ToString("yyyy/MM/dd HH:mm:ss"));
        PlayerPrefs.Save();

    }

    
    public bool DailyCheck()
    {
        System.DateTime rewardTime = new System.DateTime();

              
        if (System.DateTime.TryParse(PlayerPrefs.GetString("dailyRewardDate"), out rewardTime) == false)
        {
            return true;
        }

        int result = DateTime.Compare(rewardTime, System.DateTime.Now);

        
        return rewardTime < System.DateTime.Now;
    }

    public void DailySetting()
    {


        // System.DateTime alarmTime = System.DateTime.Now.AddDays(1);

        //System.DateTime dailytime = new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, DateTime.Now.Day + 1, 10, 0, 0);

        System.DateTime dailytime = System.DateTime.Now.AddDays(1);

        PlayerPrefs.SetString("dailyRewardDate", dailytime.ToString("yyyy/MM/dd HH:mm:ss"));
        PlayerPrefs.Save();

    }
    

    public bool FoodTruckSalechk()
    {
        System.DateTime rewardTime = new System.DateTime();


        if (System.DateTime.TryParse(PlayerPrefs.GetString("foodtrucksale"), out rewardTime) == false)
        {
            return true;
        }

        int result = DateTime.Compare(rewardTime, System.DateTime.Now);


        return rewardTime < System.DateTime.Now;
    }
    public void FoodTruckSaleSetting()
    {


        // System.DateTime alarmTime = System.DateTime.Now.AddDays(1);

        //System.DateTime dailytime = new System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, DateTime.Now.Day + 1, 10, 0, 0);

        //System.DateTime dailytime = System.DateTime.Now.AddMinutes(3);

        System.DateTime dailytime = System.DateTime.Now.AddDays(5);

        PlayerPrefs.SetString("foodtrucksale", dailytime.ToString("yyyy/MM/dd HH:mm:ss"));
        PlayerPrefs.Save();

    }


    
    public void SetSaleTimer(int type)
    {
     
        //현재 시간으로 부터 일정 시간(쿨타임) 뒤
        long time = DateTime.Now.AddSeconds(duringtime).Ticks;


        switch (type)
        {
            case 0://supresale
                SecurityPlayerPrefs.SetLong("SuperSaleTime", time);
                             

                break;
            case 1://mission
                SecurityPlayerPrefs.SetLong("MissionTime", time);
                break;
            case 2://trucksale 
                SecurityPlayerPrefs.SetLong("TruckSaleTime", time);
                break;
        }
                
      //  worldmapsaletime.gameObject.SetActive(true);
       
        SecurityPlayerPrefs.Save();

       // RefreshTimer(time, type);
    }


    /*
    TimeSpan sp;
    public void RefreshTimer(long time)
    {
        //if (currentButton == QuestButtonState.Timer)
        //{
            sp = new TimeSpan(time - DateTime.Now.Ticks);
            shopsaletiem.text = MakeTimeFromSecond((int)sp.TotalSeconds);
            worldmapsaletime.text = MakeTimeFromSecond((int)sp.TotalSeconds);
            //시간 다되면 변경
            if (CheckCoolTime(time) == true)
            {
                shopsaletiem.gameObject.SetActive(false);
                worldmapsaletime.gameObject.SetActive(false);
            }
           
        //}
    }
    */

    TimeSpan sp;
    public string RefreshTimer(long time,int type)
    {
        //if (currentButton == QuestButtonState.Timer)
        //{
        string timestring;

        sp = new TimeSpan(time - DateTime.Now.Ticks);
        
        timestring = MakeTimeFromSecond((int)sp.TotalSeconds);

        if (CheckCoolTime(time))
        {
            switch (type)
            {
                case 0:
                    //supersaletimechk = false;
                    //WorldMapUI.instance.supersaleicon.gameObject.SetActive(false);
                    //SecurityPlayerPrefs.SetLong("SuperSaleTime", 0);
                    break;
                case 1:
                    missiontimechk = false;
                    WorldMapUI.instance.missionicon.gameObject.SetActive(false);
                    SecurityPlayerPrefs.SetLong("MissionTime", 0);
                    SecurityPlayerPrefs.SetInt("nowmissionlevel",0);
                   
                    break;
                case 2:
                    trucksaletimechk = false;

                    CarPortManager.Instance.SuperSaleIcon.gameObject.SetActive(false);

                    SecurityPlayerPrefs.SetLong("TruckSaleTime", 0);
                    break;
                case 3:
                    //광고시청이 가능하다
                    Debug.Log("광고 쿨타임 종료 freecoinchk 111111:::::::::::::::::::::::::");
                    if (freecoinchk)
                    {

                        Debug.Log("광고 쿨타임 종료 freecoinchk 22222222222:::::::::::::::::::::::::");
                        if (AdManager.Instance.IsAdReady)
                        {
                            WorldMapUI.instance.freecoindisplay();

                            Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().freecoindisplay();

                            if (UI_ThreeMatch.Instance)
                                UI_ThreeMatch.Instance.freecoindisplay();
                        }

                        freecoinchk = false;

                        m_freecointime = 0;

                        SecurityPlayerPrefs.SetLong("FreeCoinTime", 0);

                        GameManager.Instance.m_GameData.FreeRewardnowindex++;

                        if (GameManager.Instance.m_GameData.FreeRewardnowindex >= TableDataManager.Instance.m_FreeRewardlist.Count)
                        {
                            GameManager.Instance.m_GameData.FreeRewardnowindex = 0;
                        }

                        SecurityPlayerPrefs.SetInt("FreeRewardnowindex", GameManager.Instance.m_GameData.FreeRewardnowindex);
                    }
                   
                  
                    break;

            }

            timestring = null;
        }

        return timestring;
        //}
    }

    bool CheckCoolTime(long time)
    {
        if (time - DateTime.Now.Ticks > 0)
        {
            
            return false;
        }
       
        return true;
    }
    
    public string MakeTimeFromSecond(int Now)
    {
        int remainTime = Now;
        int min = remainTime / 60;
        int sec = remainTime % 60;
        int hour = min / 60;

        if (hour > 0)
        {
            min = min % 60;
            return hour.ToString("00") + ":" + min.ToString("00") + ":" + sec.ToString("00");
        }

        return min.ToString("00") + ":" + sec.ToString("00");
    }

    public void timechk()
    {
        StartCoroutine(Co_TimeCheck());
    }
    public IEnumerator Co_TimeCheck()
    {
        while(true)
        {
            if (supersaletimechk)
            {
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().saletimedisplay(RefreshTimer(m_supersaleremaintime, 0));

                WorldMapUI.instance.saletimedisplay(RefreshTimer(m_supersaleremaintime, 0));
            }
            if (missiontimechk)
            {
                RefreshTimer(m_missionremaintime, 1);

                WorldMapUI.instance.missiontimedisplay(RefreshTimer(m_missionremaintime, 1));
            }
            if (trucksaletimechk && GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade<29)
            {
                if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.CarPort)
                {
                    CarPortManager.Instance.trucksaletimedisplay(RefreshTimer(m_trucksaleremaintime, 2));
                }
                  
                
                
                //RefreshTimer(m_trucksaleremaintime,2);
            }

            if (freecoinchk)
            {
                //텍스트갱신 1.월드맵
                if (WorldMapUI.instance != null)
                    WorldMapUI.instance.freecointimedisplay(RefreshTimer(m_freecointime, 3));
                //텍스트갱신 2.샵
                Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().freecointimedisplay(RefreshTimer(m_freecointime, 3));
                //텍스트갱신 3.인게임버튼
                if(UI_ThreeMatch.Instance != null)
                    UI_ThreeMatch.Instance.freecointimedisplay(RefreshTimer(m_freecointime, 3));

            }

            yield return new WaitForSeconds(1f);
        }   
    }
}
