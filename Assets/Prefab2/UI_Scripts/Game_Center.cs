﻿using UnityEngine;
using System.Collections;

public class Game_Center : BackClickPop
{
    bool label_chk;

    public GameObject Sign_in;

    public UILabel login_title;

    public UILabel login_leaderboard;

    public UILabel login_achievement;

    public UILabel SignOutlabel;

    public GameObject Sign_out;

    public UILabel logout_title;

    public UILabel logout_leaderboard;

    public UILabel logout_achievement;

    public GameObject leaderboard;

    public GameObject achievement;

    public GameObject logoutobj;


    public void Init()
    {
        PopUpSetting();
    }

    public void PopUpSetting()
    {
        bool isLogin = GameManager.Instance.m_GameData.LoginChk;

        Sign_in.SetActive(isLogin);
        Sign_out.SetActive(!isLogin);

        if (isLogin)
        {
            SignOutlabel.text = TableDataManager.Instance.GetLocalizeText(34);
            login_title.text = TableDataManager.Instance.GetLocalizeText(33);
            login_leaderboard.text = TableDataManager.Instance.GetLocalizeText(0);
            login_achievement.text = TableDataManager.Instance.GetLocalizeText(1);
        }
        else
        {
            logout_title.text = TableDataManager.Instance.GetLocalizeText(33);
            logout_leaderboard.text = TableDataManager.Instance.GetLocalizeText(0);
            logout_achievement.text = TableDataManager.Instance.GetLocalizeText(1);
        }
    }

    public void GoToLeaderBoard()
    {
        if (GameManager.Instance.m_GameData.LoginChk)
        {
            GPGS_Manager.Instance.ShowLeaderboardUI();
        }
        else
        {
            GameManager.Instance.m_LeadBoardloginchk = true;

            GPGS_Manager.Instance.LoginGPGS();
        }
    }

    public void GotoAhievement()
    {
        if (GameManager.Instance.m_GameData.LoginChk)
        {
            GPGS_Manager.Instance.ShowAchievementsUI();
        }
        else
        {
            GameManager.Instance.m_Achivementloginchk = true;

            GPGS_Manager.Instance.LoginGPGS();
        }
    }

    public void logout()
    {
        logoutobj.GetComponent<ButtonAniManager>().Init();

        Sign_in.SetActive(false);
        Sign_out.SetActive(true);

        GameManager.Instance.m_GameData.LoginChk = false;

        SecurityPlayerPrefs.SetInt("Login", 0);

        GPGS_Manager.Instance.LogoutGPGS();

        PopUpSetting();
    }

    public override void BackButtonClick()
    {
        GetComponent<Parent_Pop>().MoveToBottom();
    }
}