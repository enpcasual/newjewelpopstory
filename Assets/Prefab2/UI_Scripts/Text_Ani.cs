﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Text_Ani : MonoBehaviour {

    public List<ArchedText> text_ArchedText = new List<ArchedText>();

    void Start()
    {
       
    }

    public void SetTextAni(int index)
    {
        
        for (int i = 0; i < text_ArchedText.Count; i++)
        {
            text_ArchedText[i].gameObject.SetActive(false);
        }

        if (index >= 0 && index < text_ArchedText.Count)
        {
            text_ArchedText[index].gameObject.SetActive(true);
            text_ArchedText[index].UpdateCurveMesh();
        }
        

        Invoke("Restore", 1.03f);
    }

    public void Restore()
    {
        ObjectPool.Instance.Restore(gameObject);
    }

}
