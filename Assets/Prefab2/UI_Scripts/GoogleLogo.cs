﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GoogleLogo : MonoBehaviour {


    float Starttime = 0f;

    // Use this for initialization
    void Start () {

        Starttime = Time.realtimeSinceStartup;
        Handheld.PlayFullScreenMovie("play.mp4", Color.white, FullScreenMovieControlMode.Hidden);

    }
    
    void Update()
    {
        if (Time.realtimeSinceStartup - Starttime > 2)
            SceneManager.LoadScene("logo");
    }
	
}
