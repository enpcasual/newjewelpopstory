﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckShopPop : BackClickPop
{
    private const int MIN_TRUCK_GRADE = 9;
    private const int TRUCK_COUNT = 5;

    public UILabel titletxt;
    
    public UILabel TruckName;

    public UILabel TruckStatus;

    public UILabel TruckPrice;

    [SerializeField]
    private UISprite TruckSprite;

    [SerializeField]
    private GameObject TruckCover;

    [SerializeField]
    private GameObject BuyCover;
    
    [SerializeField]
    private GameObject ItemRoot;

    [SerializeField]
    private List<GameObject> HideButtons = new List<GameObject>();

    //int[] price = { 5000, 12000, 25000, 123, 456 };

    public int SelectedItemIndex;
    public int SelectedTruckIndex;

    public void Init()
    {
        GameManager.Instance.m_randombestgrade = false;

        titletxt.text = TableDataManager.Instance.GetLocalizeText(210);

        ShowTruck(Mathf.Clamp(GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade + 1, MIN_TRUCK_GRADE, MIN_TRUCK_GRADE + (TRUCK_COUNT - 1)));
    }

    private void ShowTruck(int truckIndex)
    {
        Debug.Log("Truck index : " + truckIndex);

        SelectedTruckIndex = truckIndex;
        SelectedItemIndex = truckIndex - MIN_TRUCK_GRADE;

        TruckSprite.spriteName = truckIndex.ToString();

        if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade >= truckIndex - 1 || truckIndex == MIN_TRUCK_GRADE)
        {
            //Accessable
            TruckSprite.color = Color.white;
            TruckCover.SetActive(false);

            if (SelectedTruckIndex > GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
                BuyCover.SetActive(false);
            else
                BuyCover.SetActive(true);
        }
        else
        {
            //InAccessable
            TruckSprite.color = Color.black;
            TruckCover.SetActive(true);
            BuyCover.SetActive(true);
        }

        TruckName.text = TableDataManager.Instance.GetLocalizeText(58 + truckIndex);       //TruckName

        TruckPrice.text = TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice.ToString("N0");// price[SelectedItemIndex]

        TruckStatus.text = GetTruckStatusInfo(truckIndex);

    }

    public string GetTruckStatusInfo(int targetTruck)
    {
        string tempstring =
            TableDataManager.Instance.GetLocalizeText(120) + ":" + TableDataManager.Instance.m_Foodtruck[targetTruck].moveplus.ToString() + "\n" +
            TableDataManager.Instance.GetLocalizeText(121) + ":" + TableDataManager.Instance.m_Foodtruck[targetTruck].butterflyplus.ToString() + "\n" +
            TableDataManager.Instance.GetLocalizeText(122) + ":" + TableDataManager.Instance.m_Foodtruck[targetTruck].goldplus.ToString() + "\n" +
            TableDataManager.Instance.GetLocalizeText(123) + ":" + TableDataManager.Instance.m_Foodtruck[targetTruck].bonusplus.ToString();

        return tempstring;
    }

    public void OnClickLeftItem()
    {
        if (SelectedItemIndex == 0)
            SelectedItemIndex = TRUCK_COUNT;

        SelectedItemIndex -= 1;
        SelectedItemIndex %= TRUCK_COUNT;

        SetButtonActive(false);
        MoveItem(450, () =>
        {
            ShowTruck(SelectedItemIndex + MIN_TRUCK_GRADE);
        }, () =>
        {
            SetButtonActive(true);
        });
    }

    public void OnClickRightItem()
    {
        SelectedItemIndex += 1;
        SelectedItemIndex %= TRUCK_COUNT;

        SetButtonActive(false);
        MoveItem(-450, () =>
        {
            ShowTruck(SelectedItemIndex + MIN_TRUCK_GRADE);
        }, () =>
        {
            SetButtonActive(true);
        });
    }

    private void SetButtonActive(bool value)
    {
        var e = HideButtons.GetEnumerator();
        while (e.MoveNext())
        {
            e.Current.SetActive(value);
        }
    }

    private void MoveItem(float to, TweenCallback unShown, TweenCallback complete)
    {
        var seq = DOTween.Sequence();
        seq.Append(ItemRoot.transform.DOLocalMoveX(to, 0.25f).SetEase(Ease.OutQuad));
        seq.AppendCallback(unShown);
        seq.AppendCallback(() => { ItemRoot.transform.localPosition = new Vector3(-to, 0, 0); });
        seq.AppendInterval(0.01f);
        seq.Append(ItemRoot.transform.DOLocalMoveX(0, 0.25f).SetEase(Ease.OutQuad));
        seq.AppendCallback(complete);
        seq.Play();
    }

    public void OnClickBuyItem()
    {
        GameManager.Instance.m_randombestgrade = false;

        if (GameManager.Instance.m_GameData.MyGold >= TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice && SelectedTruckIndex > GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
        {
            //재화 충분 및 소지 등급보다 높을 경우에만
            GameManager.Instance.m_GameData.MyGold -= TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice;
            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            if (WorldMapUI.instance)
            {
                WorldMapUI.instance.GoldRefresh();
            }

            Popup_Manager.INSTANCE.Popup_Pop();

            GameManager.Instance.m_randomGrade = SelectedTruckIndex;

            Popup_Manager.INSTANCE.popup_index = 37;

            Popup_Manager.INSTANCE.Popup_Push();

            if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < GameManager.Instance.m_randomGrade)
            {
                GameManager.Instance.m_randombestgrade = true;
            }

            CarPortManager.Instance.AddFoodTruck(GameManager.Instance.m_randomGrade);
            
            if (MatchManager.Instance != null)
                FirebaseManager.Instance.LogEvent_UseGold(MatchManager.Instance.m_StageIndex, "FoodTruck", TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice);
            else
                FirebaseManager.Instance.LogEvent_UseGold(StageItemManager.Instance.TotalnowStage, "FoodTruck", TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice);

        }
        else if (GameManager.Instance.m_GameData.MyGold < TableDataManager.Instance.m_Foodtruck[SelectedTruckIndex].buyprice)
        {
            Popup_Manager.INSTANCE.popup_index = 2;

            Popup_Manager.INSTANCE.Popup_Push();
        }

    }

    //public void RandomBoxClick()
    //{
    //    GameManager.Instance.m_randombestgrade = false;
        
    //    if (GameManager.Instance.m_GameData.MyGold >= price[randomboxindex])
    //    {
    //        GameManager.Instance.m_GameData.MyGold -= price[randomboxindex];

    //        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

    //        Popup_Manager.INSTANCE.Popup_Pop();

    //        GameManager.Instance.m_randomGrade = randomgrade[randomboxindex] + Random.Range(0, 4);

    //        Popup_Manager.INSTANCE.popup_index = 37;

    //        Popup_Manager.INSTANCE.Popup_Push();

    //        if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < GameManager.Instance.m_randomGrade)
    //        {
    //            GameManager.Instance.m_randombestgrade = true;
    //        }

    //        CarPortManager.Instance.AddFoodTruck(GameManager.Instance.m_randomGrade);

    //        //애널리틱스
    //        //GoogleAnalytics_Manager.Instance.LogEvent_CashItem_RandomBox(StageItemManager.Instance.TotalnowStage, randomboxindex);

    //        if (MatchManager.Instance != null)
    //            GoogleAnalytics_Manager.Instance.LogEvent_UseGold(MatchManager.Instance.m_StageIndex, "FoodTruck", price[randomboxindex]);
    //        else
    //            GoogleAnalytics_Manager.Instance.LogEvent_UseGold(StageItemManager.Instance.TotalnowStage, "FoodTruck", price[randomboxindex]);

    //    }
    //    else if (GameManager.Instance.m_GameData.MyGold < price[randomboxindex])
    //    {
    //        Popup_Manager.INSTANCE.popup_index = 2;

    //        Popup_Manager.INSTANCE.Popup_Push();
    //    }
    //}

    public override void BackButtonClick()
    {
        ExitClick();
    }


    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public void BuyResult(int packageindex)
    {



    }
}
