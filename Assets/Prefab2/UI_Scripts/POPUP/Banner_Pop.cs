﻿using UnityEngine;
using System.Collections;

public class Banner_Pop : MonoBehaviour {

    Animator ani;

    void Start()
    {
        ani = GetComponent<Animator>();


    }

    public void MoveToBottom()
    {
        ani.SetBool("TO_BOTTOM", true);
    }


    public void Popup_pop()
    {
        ani.SetBool("TO_BOTTOM", false);
        Popup_Manager.INSTANCE.Popup_Pop();
    }
}
