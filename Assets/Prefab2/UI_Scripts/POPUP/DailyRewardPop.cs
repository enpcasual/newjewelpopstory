﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DailyRewardPop : BackClickPop
{


    public UISprite DailyRewardSpr;

    public UILabel DailyRewardLabel;

    public int DailyRewardtype;

    public int DailyRewardcnt;

    public int itemtype;

    public UILabel title;

    public UILabel button;

    public UISprite DailyRewardTruckSpr;

    

    public void Init()
    {

        if (GameManager.Instance.m_adsRewardchk)
        {
            title.text = TableDataManager.Instance.GetLocalizeText(18);
        }
        else
        {
            title.text = TableDataManager.Instance.GetLocalizeText(119);
        }
        


        button.text = TableDataManager.Instance.GetLocalizeText(10);

        DailyRewardLabel.color = Color.white;

        DailyRewardLabel.gameObject.transform.localPosition = new Vector3(0, -19, 0);

        if (GameManager.Instance.m_adsRewardchk)
        {
            AdsReward();
        }
        else
        {
            DailyRewardSetting();
        }
        
    }

    public override void BackButtonClick()
    {
        //인게임안에서 동영상광고를 클릭했을때
        if (GameManager.Instance.GetGameState() == GAMESTATE.ThreeMatch && UI_ThreeMatch.Instance.InGameAdsClick)
        {
            UI_ThreeMatch.Instance.InGameAdsClick = false;
            transform.DOBlendableMoveBy(new Vector3(0, -1, 0), 0.5f).OnComplete(() =>
            {
                Popup_Manager.INSTANCE.Popup_Pop();             
            });           
        }
        else
        {
            GetComponent<Parent_Pop>().MoveToBottom();
        }       
    }

    public void DailyRewardSetting()
    {
       if (StageItemManager.Instance.CurrentPart > 1 && GameManager.Instance.FreeUsecnt <= 0 && GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < 26) //레벨이 100이상일때
        {
            int randindex = Random.Range(0, 11);

            if (randindex < 3)
            {
                if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < 6)
                {

                    GameManager.Instance.FreeUseTruckGrade = 6;
                }
                else if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade >= 6)
                {
                    GameManager.Instance.FreeUseTruckGrade = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade + 3;
                }

                DailyRewardSpr.gameObject.SetActive(false);

                DailyRewardTruckSpr.gameObject.SetActive(true);

                DailyRewardTruckSpr.spriteName = GameManager.Instance.FreeUseTruckGrade.ToString();
                DailyRewardTruckSpr.MakePixelPerfect();

                DailyRewardLabel.text = TableDataManager.Instance.GetLocalizeText(209);

                DailyRewardLabel.gameObject.transform.localPosition = new Vector3(0, -74, 0);

                DailyRewardLabel.color = new Color32(227, 8, 8, 255);

                DailyRewardtype = 2;

                GameManager.Instance.FreeUsecnt = 3;

                
                SecurityPlayerPrefs.SetInt("FreeUsecnt",GameManager.Instance.FreeUsecnt);

                SecurityPlayerPrefs.SetInt("FreeUseTruckGrade", GameManager.Instance.FreeUseTruckGrade);

                return;

            }
        }
        int rewardindex = Random.Range(0, 11);

        DailyRewardtype =TableDataManager.Instance.m_DailyReward[rewardindex].type;

        DailyRewardSpr.gameObject.SetActive(true);

        DailyRewardTruckSpr.gameObject.SetActive(false);

        //보상이 골드이면
        if (DailyRewardtype == 0)
        {
            DailyRewardSpr.spriteName = "gold_02";

            //1~20레벨까지
            if(StageItemManager.Instance.TotalnowStage <=20)
            {
                DailyRewardcnt = TableDataManager.Instance.m_DailyReward[rewardindex].reward1;
            }else
            {
                DailyRewardcnt = TableDataManager.Instance.m_DailyReward[rewardindex].reward2;
            }

            DailyRewardLabel.text = DailyRewardcnt.ToString();

            //골드 이미지가 크지 않으므로 원래 이미지 사이즈로 변경
            DailyRewardSpr.MakePixelPerfect();
        }
            //아이템이면
        else if (DailyRewardtype == 1)
        {
            itemtype = TableDataManager.Instance.m_DailyReward[rewardindex].reward1;
            switch (itemtype)
            {
                case 0:
                    DailyRewardSpr.spriteName = "item_01";
                    break;
                case 1:
                    DailyRewardSpr.spriteName = "item_02";
                    break;
                case 2:
                    DailyRewardSpr.spriteName = "item_03";
                    break;
            }

            DailyRewardLabel.text = TableDataManager.Instance.GetLocalizeText(48 + itemtype);

            //이번 캐쉬템 이미지는 크기가 크므로 지정함.
            DailyRewardSpr.width = 100;
            DailyRewardSpr.height = 100;
        }
    }


    public void AdsReward()
    {

        DailyRewardSpr.gameObject.SetActive(true);

        DailyRewardTruckSpr.gameObject.SetActive(false);

        
        int itemindex = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].kind - 1;
        switch (itemindex)
        {
            case 0:
                DailyRewardSpr.spriteName = "item_01";
                GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                DailyRewardLabel.text = TableDataManager.Instance.GetLocalizeText(48 + itemindex);
                break;
            case 1:
                DailyRewardSpr.spriteName = "item_02";
                GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                DailyRewardLabel.text = TableDataManager.Instance.GetLocalizeText(48 + itemindex);
                break;
            case 2:
                DailyRewardSpr.spriteName = "item_03";
                GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                DailyRewardLabel.text = TableDataManager.Instance.GetLocalizeText(48 + itemindex);
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                //bjs 골드 추가
                //지급은 여기서 오기전에 각각 해준다.(월드맵, 샾, 인게임 각각 다른 연출 지급..)
                DailyRewardSpr.spriteName = "gold_0" + (itemindex - 2); //이미 위에서 1을 뺐으니까 2만 더 빼자.
                DailyRewardLabel.text = "Gold " + TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].reward;
                break;
        }

        
        DailyRewardSpr.MakePixelPerfect();

        if (DailyRewardSpr.width > 200) // 팝업 뒷 배경보다 아이콘이 클 경우.
        {
            DailyRewardSpr.width = Mathf.RoundToInt(DailyRewardSpr.width * 0.5f);
            DailyRewardSpr.height = Mathf.RoundToInt(DailyRewardSpr.height * 0.5f);
        }
    }
}
