﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguagePop : BackClickPop
{


    public List<UISprite> languagelist = new List<UISprite>();

    public int nowLanguageindex;

    

    public UILabel titletxt;
    /*
     *  "ko",//한국어
                "eng",//영어
                "ja",//일어
                "zh_TW",//중국(번체)
                "zh_CN",//중국(간체)
                "hi",//인도
                "ar",//아랍
                "ru",//러시아
                "de",//독일
                "pl"//폴란드
                "fr"//프랑스
     */
    public void Init()
    {
        nowLanguageindex = (int)TableDataManager.Instance.currentLanguage;

        for (int i = 0; i < languagelist.Count; i++)
        {
            if(languagelist[i] == null)
                continue;

            languagelist[i].gameObject.SetActive(false);
        }

        languagelist[nowLanguageindex].gameObject.SetActive(true);

      //  FontManager.instance.FontSetting(titletxt);

        titletxt.text = TableDataManager.Instance.GetLocalizeText(184);

        titletxt.GetComponentInChildren<FontManager>().FontSetting();
    }

    public void ko_Click()
    {
        nowLanguageindex = 0;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.Korean);//"ko");

        Init();
    }

    public void en_Click()
    {
        nowLanguageindex = 1;

        TableDataManager.Instance.LocalTextSetting(SystemLanguage.English);//"en");

        Init();
    }
    public void ja_Click()
    {
        nowLanguageindex = 2;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.Japanese);//"ja");

        Init();
    }
    public void ch_tw_Click()
    {
        nowLanguageindex = 3;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.ChineseTraditional);//"zh_TW");

        Init();
    }
    public void ch_cn_Click()
    {
        nowLanguageindex = 4;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.ChineseSimplified);//"zh_CN");

        Init();
    }
    public void de_Click()
    {
        nowLanguageindex = 8;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.German);//"de");

        Init();
    }

    public void fr_Click()
    {
        nowLanguageindex = 10;



        TableDataManager.Instance.LocalTextSetting(SystemLanguage.French);//"fr");

        Init();
    }



    public override void BackButtonClick()
    {
        ExitClick();
    }

    public void ExitClick()
    {
        
          GetComponent<Parent_Pop>().MoveToBottom();
        
    }
}
