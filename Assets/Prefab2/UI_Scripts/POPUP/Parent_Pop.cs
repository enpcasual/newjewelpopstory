﻿using UnityEngine;
using System.Collections;

public class Parent_Pop : MonoBehaviour {

    public System.Action Completer = null;

    Animator ani;

    void Start()
    {
        ani = GetComponent<Animator>();

       
    }

   public void MoveToBottom()
    {
        ani.SetBool("TO_BOTTOM", true);
    }

   public void ComeBackGame()
   {
       ani.SetBool("COMEBACK_GAME", true);
   }

   public void Vib_Start()
   {
       ani.SetBool("VIB_CHK", true);
   }

   public void Vib_End()
   {
       ani.SetBool("VIB_CHK", false);
   }
    public void Popup_pop_WorldMap()
    {
        ani.SetBool("TO_BOTTOM", false);

        Popup_Manager.INSTANCE.Popup_Pop();

        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
        MatchManager.Instance.EndGame();

        //if (GameManager.Instance.carportmovechk)
        // {
        //     MatchManager.Instance.GameField.SetActive(false);

        //     GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        //     if (TimeManager.Instance.trucksaletimechk && GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < 29)
        //     {

        //         CarPortManager.Instance.SuperSaleIcon.gameObject.SetActive(true);
        //     }
        // }

        // GameManager.Instance.carportmovechk = false;

        if (Completer != null)
        {
            Completer();
            Completer = null;
        }
    }

    public void Popup_pop_Game()
    {
        ani.SetBool("COMEBACK_GAME", false);

        Popup_Manager.INSTANCE.Popup_Pop();
        
        if (GameManager.Instance.m_restatchk || GameManager.Instance.m_replaychk || GameManager.Instance.m_specialmissionstart)
          {

              if (GameManager.Instance.m_specialmissionchk)
              {
                  int missionlevel = 10000 + StageItemManager.Instance.CurrentPart * 10 + GameManager.Instance.m_GameData.NowMissionLevel;

                  if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
                  {
                      MatchManager.Instance.StartGame(missionlevel + 1);
                  }
                  else
                  {
                      GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);

                      GameManager.Instance.GameStart(missionlevel + 1);
                  }

              }
              else
              {
                  MatchManager.Instance.StartGame(MatchManager.Instance.m_StageIndex);          
              }

            GameManager.Instance.m_specialmissionstart = false;
            GameManager.Instance.m_restatchk = false;
            GameManager.Instance.m_replaychk = false;

            // GameManager.Instance.m_specialmissionchk = false;

        }

        if (Completer != null)
        {
            Completer();
            Completer = null;
        }
    }
}
