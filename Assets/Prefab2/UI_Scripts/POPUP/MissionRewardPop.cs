﻿using UnityEngine;
using System.Collections;

public class MissionRewardPop : BackClickPop {

    public CarPortTruck missiontruck;

    public UILabel titletxt;

    public UILabel getbutton;

    public UILabel goldtxt;

    public UILabel truckgrade;

    int rewardtruckgrade;

    int rewardgold;

    public void Init()
    {

        SecurityPlayerPrefs.SetInt("nowmissionlevel", GameManager.Instance.m_GameData.NowMissionLevel);

         titletxt.text = TableDataManager.Instance.GetLocalizeText(144);

        getbutton.text = TableDataManager.Instance.GetLocalizeText(145);

        rewardgold = TableDataManager.Instance.m_missiongold[StageItemManager.Instance.CurrentPart].rewardgold;

        rewardtruckgrade = TableDataManager.Instance.m_missiongold[StageItemManager.Instance.CurrentPart].rewardtruck;

        goldtxt.text = rewardgold.ToString();

        MissionRewardTruckSetting();

    }



    public void MissionRewardTruckSetting()
    {

       // rewardtruckgrade = TableDataManager.Instance.m_missiongold[StageItemManager.Instance.CurrentPart].rewardtruck;

        if (rewardtruckgrade < 0)
        {
            rewardtruckgrade = 0;
        }
        FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[rewardtruckgrade];

        missiontruck.SetInfo(tempitem);

        truckgrade.gameObject.SetActive(true);

        truckgrade.text = (rewardtruckgrade ).ToString() + "Grade";
        
    }

    public void RewardClick()
    {
        GetComponent<Parent_Pop>().MoveToBottom();

        TimeManager.Instance.m_missionremaintime = 0;

        SecurityPlayerPrefs.SetInt("nowmissionlevel", 0);

        CarPortManager.Instance.AddFoodTruck(rewardtruckgrade);

        WorldMapUI.instance.GoldSetting(rewardgold);
    }

    public override void BackButtonClick()
    {
        RewardClick();
    }
}
