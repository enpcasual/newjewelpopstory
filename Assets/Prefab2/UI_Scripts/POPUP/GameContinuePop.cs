﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameContinuePop : BackClickPop {

    public UILabel nowgold;

    public UILabel infotxt;

    public UILabel endgame;

    public UILabel keepplay;

    public List<GameObject> continuelist = new List<GameObject>();

    public int continueType;
    public int failCnt;

    public UILabel continuegoldlabel;

    int continueGold;

    public GameObject continugold;

    public UISprite freecontinue;

    public UISprite Specialicon;

    public UILabel LevelText = null;

    public void Init()
    {

        NeedGold();

        continuegoldlabel.text = continueGold.ToString();
        
        nowgold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");

        LevelText.text = string.Format("Level {0}", MatchManager.Instance.m_StageIndex.ToString());


        endgame.text = TableDataManager.Instance.GetLocalizeText(45);
        
        keepplay.text = TableDataManager.Instance.GetLocalizeText(38);

        continugold.SetActive(false);

        for (int i = 0; i < 7; i++)
        {
            continuelist[i].SetActive(false);
        }

        continuelist[continueType].SetActive(true);

        freecontinue.gameObject.SetActive(true);

        //bjs 광고 없앰
        if (MatchManager.Instance.m_StageIndex <= 20)
        {
            continuegoldlabel.text = "FREE";

            continugold.SetActive(true);

            freecontinue.gameObject.SetActive(false);
        }
        else
        {
            if (failCnt == 1 && AdManager.Instance.IsAdReady)
            {
                continugold.SetActive(false);

                freecontinue.gameObject.SetActive(true);
            }

            if (failCnt > 1)
            {
                continugold.SetActive(true);

                freecontinue.gameObject.SetActive(false);
            }
        }
          

        switch (continueType)
        {
            case 0:
            case 1:
            case 2:
                infotxt.text = TableDataManager.Instance.GetLocalizeText(44);
                break;
            case 3://bear arrived
            case 4:
                infotxt.text = TableDataManager.Instance.GetLocalizeText(46);
                break;
            case 5://bomb timeout
            case 6:
                infotxt.text = TableDataManager.Instance.GetLocalizeText(47);
                break;
        }

        Specialicon.gameObject.SetActive(false);

        if (StageItemManager.Instance.TotalnowStage >= 30)
        {
            Specialicon.gameObject.SetActive(true);
        }
    }


    public void nowGoldSetting()
    {
        nowgold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
    }
    public void EndGameClick()
    {
        Popup_Manager.INSTANCE.Popup_Pop();

        Popup_Manager.INSTANCE.popup_index = 14;

        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void SpecialiconClick()
    {
        Popup_Manager.INSTANCE.popup_index = 35;
        Popup_Manager.INSTANCE.Popup_Push();
    }
    public override void BackButtonClick()
    {

        EndGameClick();
    }

    public void NeedGold()
    {
        continueGold = 0;

        if (failCnt >= 3)
        {
            failCnt = 3;
        }

        switch (failCnt)
        {
            case 1:
                continueGold = TableDataManager.Instance.m_buyprice[1].buyprice;
                break;
            case 2:
                continueGold = TableDataManager.Instance.m_buyprice[2].buyprice;
                break;
            case 3:
                continueGold = TableDataManager.Instance.m_buyprice[3].buyprice;
                break;
        }
    }

    public void continugoldchk()
    {
        if (GameManager.Instance.m_GameData.MyGold >= continueGold)
        {

            GameManager.Instance.m_GameData.MyGold -= continueGold;

            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            GetComponent<Banner_Pop>().MoveToBottom();

            MissionManager.Instance.ContinueBonus(continueType);

            FirebaseManager.Instance.LogEvent_UseGold(MatchManager.Instance.m_StageIndex, "Continu", continueGold);

        }
        else
        {
            Popup_Manager.INSTANCE.popup_index = 2;

            Popup_Manager.INSTANCE.Popup_Push();
        }
    }
    public void Click_Continue()
    {
        SoundManager.Instance.PlayEffect("play_click");
        SoundManager.Instance.PlayEffect("buy");

        //bjs 광고 없앰
        if (MatchManager.Instance.m_StageIndex <= 20)
        {
            GetComponent<Banner_Pop>().MoveToBottom();

            MissionManager.Instance.ContinueBonus(continueType);
        }
        else if (failCnt == 1)
        {
            if (AdManager.Instance.IsAdReady)
            {
                GameManager.Instance.gamecontinuefree = true;

                AdManager.AdsRewardResult = null;
                AdManager.Instance.Ad_Show("Continue");

                // GetComponent<Banner_Pop>().MoveToBottom();
            }
            else
            {
                continugoldchk();
            }
        }
        else
        {
            continugoldchk();
        }
    }

    public void Click_EndGame()
    {
        SoundManager.Instance.PlayEffect("play_click");
        SoundManager.Instance.PlayEffect("buy");

        GetComponent<Banner_Pop>().Popup_pop();

        //GameOverPopupSetting
        Popup_Manager.INSTANCE.popup_index = 14;
        Popup_Manager.INSTANCE.Popup_Push();
    }
}
