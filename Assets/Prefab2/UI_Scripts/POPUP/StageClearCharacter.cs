﻿using UnityEngine;
using System.Collections;

public class StageClearCharacter : MonoBehaviour {

    public Text_Ani cleartxt_ani;


    public void StageClearCharacter_Down()
    {
        
        GetComponent<Animator>().SetBool("CHARACTER_DOWN", true);
    }


    public void StageClearTxt_Ani()
    {
        cleartxt_ani.gameObject.SetActive(true);

        SoundManager.Instance.PlayEffect("clear");
    }
    public void StageClear_Pop()
    {
        cleartxt_ani.gameObject.SetActive(false);
        gameObject.SetActive(false);
        GetComponent<Animator>().SetBool("CHARACTER_DOWN", false);
        Popup_Manager.INSTANCE.Popup_Pop();

        if (GameManager.Instance.m_specialmissionchk)
        {
            GameManager.Instance.m_GameData.NowMissionLevel++;

            SecurityPlayerPrefs.SetInt("nowmissionlevel", GameManager.Instance.m_GameData.NowMissionLevel);

            if (GameManager.Instance.m_GameData.NowMissionLevel < 5)
            {
                Popup_Manager.INSTANCE.popup_index = 26;

                Popup_Manager.INSTANCE.Popup_Push();
            }
            else
            {
                Popup_Manager.INSTANCE.popup_index = 27;

                Popup_Manager.INSTANCE.Popup_Push();
            }

            GameManager.Instance.m_specialmissionchk = false;
            GameManager.Instance.m_specialmissionstart = false;
        }
        else
        {
            Popup_Manager.INSTANCE.popup_index = 18;
            Popup_Manager.INSTANCE.Popup_Push();
        }
       
    }
}
