﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RewardtruckPop : BackClickPop
{
    //public List<GameObject> infolist = new List<GameObject>();
    public GameObject info;

    public UILabel titletxt;

    public UILabel sharetxt;

    public UILabel garagetxt;

    public UILabel oktxt;

    public CarPortTruck MyTruck;

    public List<string> infotxtlist;

    public int truckGrade;

    public List<ParticleSystem> effectstar = new List<ParticleSystem>();

    public int effect_index = 0;

    public GameObject gotocarport;

    public UISprite carporticon;

    int gradecnt;

    public UILabel warnninglabel;

    public UISprite fadeinbg;

    public GameObject okbtn;

    //public GameObject basictruck;

    //public GameObject othertruck;


    int icondisplaycnt;


    public bool tutorialstatus;

    public void Init()
    {

        isClicked = false;

        gradecnt = 0;

        icondisplaycnt = 0;

        tutorialstatus = false;

        MyTruck.ani.SetBool("STOP_CHK", true);

        for (int i = infotxtlist.Count-1; i >= 0; i--)
        {
            infotxtlist.RemoveAt(i);
        }
       

        //for (int i = 0; i < infolist.Count; i++)
        //{
        //    infolist[i].gameObject.SetActive(false);

        //}

        effect_index = 0;
        for (int i = 0; i < effectstar.Count; i++)
        {
            effectstar[i].gameObject.SetActive(false);
            effectstar[i].Stop();
        }

        okbtn.SetActive(false);

        fadeinbg.gameObject.SetActive(false);

        //basictruck.SetActive(false);

        //othertruck.SetActive(false);

       
      
         truckGrade = GameManager.Instance.rewardTruckgrade;

         titletxt.text = (truckGrade + 1).ToString() + ". " + TableDataManager.Instance.GetLocalizeText(58 + truckGrade);


        //if (truckGrade == 0)
        //{
            //basictruck.SetActive(true);
            //othertruck.SetActive(false);

            //gotocarport.transform.localPosition = new Vector3(0, 41, 0);

            //okbtn.transform.localPosition = new Vector3(0, -148, 0);
        //}
        //else if (truckGrade > 0)
        //{
            //basictruck.SetActive(false);
            //othertruck.SetActive(true);

            //gotocarport.transform.localPosition = new Vector3(0, -20, 0);

            //okbtn.transform.localPosition = new Vector3(0, -220, 0);
        //}
        gotocarport.SetActive(false);

            


            foodtruckdisplayChk();

            StartCoroutine(EffectStart());


        oktxt.text = TableDataManager.Instance.GetLocalizeText(10);

        FoodTruckinfo temptruck = TableDataManager.Instance.m_Foodtruck[truckGrade];

        MyTruck.SetInfo(temptruck);

         TruckinfoSetting();

       
    }

    IEnumerator EffectStart()
    {
        /*
        yield return new WaitForSeconds(0.7f);

        while (effect_index < effectstar.Count)
        {

            effectstar[effect_index].gameObject.SetActive(true);
           // effectstar[effect_index].gameObject.transform.localPosition = new Vector3(Random.Range(-250f, 250f), Random.Range(350, 500f), 0f);
            effectstar[effect_index].Play();
            effect_index++;
            yield return new WaitForSeconds(0.2f);
        }

        */


        //if (!GameManager.Instance.m_GameData.TruckTutorialChk)
        //{

        //    if (CarPortManager.Instance.m_MyFoodTruckList.Count == 2)
        //    {
        //        tutorialstatus = true;

        //        yield return new WaitForSeconds(1f);

        //        Popup_Manager.INSTANCE.popup_index = 28;

        //        Popup_Manager.INSTANCE.Popup_Push();

        //        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);
        //    }
        //}
        //else
        //{
        yield return new WaitForSeconds(0.5f);
        //}


        if (gradecnt > 0)
        {


            warnninglabel.text = TableDataManager.Instance.GetLocalizeText(140);


            gotocarport.SetActive(true);


            //bool istuto = TutorialManager.Instance.Tutorial_Start(10000); 
        }


        carporticon.GetComponent<Animator>().SetBool("IDLE_ANI", true);

        yield return new WaitForSeconds(0.5f);

        okbtn.SetActive(true);


      
    }



    public void TruckinfoSetting()
    {

        string tempstring = TableDataManager.Instance.GetLocalizeText(120) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].moveplus.ToString();
        infotxtlist.Add(tempstring);


        tempstring = TableDataManager.Instance.GetLocalizeText(121) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].butterflyplus.ToString();
        infotxtlist.Add(tempstring);


        tempstring = TableDataManager.Instance.GetLocalizeText(122) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].goldplus.ToString();
        infotxtlist.Add(tempstring);

        tempstring = TableDataManager.Instance.GetLocalizeText(123) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].bonusplus.ToString();
        infotxtlist.Add(tempstring);



        for (int i = 0; i < infotxtlist.Count; i++)
        {
            info.GetComponent<truckinfotxt>().infotxt[i].text = infotxtlist[i];
        }

        //if (abilitycnt >= 0)
        //{
        //    infolist[abilitycnt].SetActive(true);

        //    for (int i = 0; i < infotxtlist.Count; i++)
        //    {
        //        infolist[abilitycnt].GetComponent<truckinfotxt>().infotxt[i].text = infotxtlist[i];
        //    }


        //}
    }

    private bool isClicked = false;
    public override void BackButtonClick()
    {
        //if(isClicked)
        //    return;

        //MyTruck.MoveTo(MyTruck.transform.localPosition, MyTruck.transform.localPosition + new Vector3(-400, 0, 0), () =>
        //{
            Popup_Manager.INSTANCE.Popup_Pop();
            //isClicked = false;
        //});
        //isClicked = true;
        //GetComponent<Child_Pop>().MoveToTop();
    }

    public void buttonclick()
    {
        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.CarPort)
        {
            OnShareButton();
        }
        else
        {
            //모든 팝업제거

            GameManager.Instance.SetGameState(GAMESTATE.CarPort);
            Popup_Manager.INSTANCE.Init();
        }

       

    }
   //Texture2 _myScreenShot;
 
    public void OnShareButton ()
    {
        // OS별 일반적 공유
        GeneralShare generalShare = new GeneralShare ();        
        generalShare.shareText ("MyGameApp", "Message");
 
        // 이미지 공유시
        //generalShare.shareImageWithText ("MyGameApp", "Message", _myScreenShot);
    }


    public void foodtruckdisplayChk()
    {

        gradecnt = 0;

        for (int i = 0; i < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; i++)
        {
            int grade1 = GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[i].truckgrade;

            for (int j = 0; j < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; j++)
            {
                if (j != i)
                {
                    if (grade1 == GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[j].truckgrade)
                    {
                        gradecnt++;
                        break;
                    }
                }
            }
        }


       // Debug.Log("gradecnt:::::::::::::" + gradecnt);

    }

    public void carportmoveclick()
    {
        // GameManager.Instance.carportmovechk = true;

        // GetComponent<Parent_Pop>().MoveToBottom();

        

        carporticon.GetComponent<Animator>().SetBool("BUTTON_PRESS", false);

        GameManager.Instance.gameresultchk = true;

        
        
        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        Popup_Manager.INSTANCE.Popup_Pop();

        
        //튜토리얼 진행시에는 팝업이 2개인 상태이다
        if (tutorialstatus)
        {
            Popup_Manager.INSTANCE.Popup_Pop();

            tutorialstatus = false;
        }
         
       
       
       // StartCoroutine(FadeinCarport());

      }

    IEnumerator FadeinCarport()
    {
        fadeinbg.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.2f);

       
        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        Popup_Manager.INSTANCE.Popup_Pop();
    }
}
