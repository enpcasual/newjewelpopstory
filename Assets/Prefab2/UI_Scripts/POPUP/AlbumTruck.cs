﻿using UnityEngine;
using System.Collections;

public class AlbumTruck : MonoBehaviour {

    public UISprite truckicon;

    public UILabel gradelabel;

    public Animator ani;

    public UISprite bg;

    public int Grade;

    public void setinfo(int grade)
    {

        Grade = grade;

        truckicon.spriteName = Grade.ToString();

        gradelabel.text = (Grade + 1).ToString() + ". " + TableDataManager.Instance.GetLocalizeText(58 + Grade);

        bg.color = Color.white;

        truckicon.enabled = true;
    }


    public void Truckinfo()
    {

        if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade >= Grade)
        {
            Popup_Manager.INSTANCE.popup_index = 19;

            GameManager.Instance.m_FoodTruckAlbumIndex = Grade;

            Popup_Manager.INSTANCE.Popup_Push();
        }
    }


  
}
