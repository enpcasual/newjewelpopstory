﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialItemPop : BackClickPop
{

    public UIScrollView m_scrollview;

    public Transform m_Grid;

    public UILabel titletxt;

    public void Init()
    {
        titletxt.text = TableDataManager.Instance.GetLocalizeText(200);
       
        GridSetting();
    }


    void GridSetting()
    {
        for (int i = 0; i < TableDataManager.Instance.m_packageitem.Count; i++)
        {
            Transform gridchild = m_Grid.GetChild(i);

            gridchild.GetComponent<Specialitem>().Init(i);

        }

    }

    public override void BackButtonClick()
    {
        ExitClick();
    }


    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public void BuyResult(int packageindex)
    {
        GameManager.Instance.m_GameData.MyGold += TableDataManager.Instance.m_packageitem[packageindex].glodcnt;

        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

        WorldMapUI.instance.GoldRefresh();

        Popup_Manager.INSTANCE.popup_list[4].GetComponent<GameContinuePop>().nowGoldSetting();

        for (int i = 0; i < GameManager.Instance.m_GameData.MyItemCnt.Count; i++)
        {
            GameManager.Instance.m_GameData.MyItemCnt[i] += TableDataManager.Instance.m_packageitem[packageindex].itemcnt;

            SecurityPlayerPrefs.SetInt("MyItem" + i, GameManager.Instance.m_GameData.MyItemCnt[i]);

            //UI갱신
            if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
                UI_ThreeMatch.Instance.CashItemUpdate(i);
        }

        switch (packageindex)
        {
            case 0:
                 GameManager.Instance.m_GameData.Packageitem1buychk = true;
                 SecurityPlayerPrefs.SetInt("Packageitem1buychk", 1);
                
                break;
            case 1:
                 GameManager.Instance.m_GameData.Packageitem2buychk = true;
                 SecurityPlayerPrefs.SetInt("Packageitem2buychk", 1);
                break;
            case 2:
                 GameManager.Instance.m_GameData.Packageitem3buychk = true;
                 SecurityPlayerPrefs.SetInt("Packageitem3buychk", 1);
                break;
            case 3:
                GameManager.Instance.m_GameData.Packageitem4buychk = true;
                SecurityPlayerPrefs.SetInt("Packageitem4buychk", 1);
                break;
           
        }

        GridSetting();
    }
}
