﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionMenu : MonoBehaviour {

    public List<GameObject> miconlist = new List<GameObject>();

    public void MissionListSetting(List<MissionInfo> list_info)
    {
        for (int i = 0, len = miconlist.Count; i < len; i++)
        {
            miconlist[i].GetComponent<UISprite>().spriteName = GoalItem.GetMissionSpriteName(list_info[i].kind);

            miconlist[i].GetComponentInChildren<UILabel>().text = list_info[i].count.ToString();
        }
    }
}