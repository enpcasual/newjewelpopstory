﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRewardPop : BackClickPop
{

    public UIScrollView m_scrollview;

    public UIGrid m_Grid;

    public UILabel titletxt;

    public UILabel rewardinfo;

    public void Init()
    {
        titletxt.text = TableDataManager.Instance.GetLocalizeText(197);
        rewardinfo.text = TableDataManager.Instance.GetLocalizeText(198);
        GridSetting();
    }


    void GridSetting()
    {
        for (int i = 0; i < TableDataManager.Instance.m_FreeRewardlist.Count; i++)
        {
            Transform gridchild = m_Grid.GetChild(i);

            gridchild.GetComponent<FreeRewarditem>().Init(i);

        }
       
    }

    public override void BackButtonClick()
    {
        OkClick();
    }


    public void OkClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
