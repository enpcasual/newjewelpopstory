﻿using UnityEngine;
using System.Collections;

public class Child_Pop : MonoBehaviour
{

    Animator ani;

    void Start()
    {
        ani = GetComponent<Animator>();


    }

    public void MoveToTop()
    {

        ani.SetBool("TO_TOP", true);
    }

    void Popup_pop()
    {
        ani.SetBool("TO_TOP", false);
        Popup_Manager.INSTANCE.Popup_Pop();

        if (GameManager.Instance.unclokgoldchk)
        {
            GameManager.Instance.unclokgoldchk = false;

            StageItemManager.Instance.CharactermoveStart();
        }
    }

   
}
