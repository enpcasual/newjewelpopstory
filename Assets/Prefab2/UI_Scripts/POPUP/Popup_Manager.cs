﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum PopUpType
{
    parents,
    child
}

public class Popup_Manager : MonoBehaviour {

    public GameObject popup_bg;

    public List<GameObject> popup_list = new List<GameObject>();

    public Stack<int> popup_stack = new Stack<int>();

    static Popup_Manager instance;

    public int popup_index = 0;

    int before_depth = 0;

    public PopUpType m_popuptype = PopUpType.parents;

    public bool salechk;

    public int missioncnt = 1; 

    public bool rewardtruckpopupclose;

    public bool shoppopchk;//지금 상점 팝업인가? 골드 획득시 연출때문에 
    
    public int beforeindex;

    public bool addfree_popupchk;

    public bool add_displaychk;//현재 광고가 나온후인가?

    public List<TutorialTarget> List_Tutorial_Target;

    public Agreement UIAgreement;

    public static Popup_Manager INSTANCE
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(Popup_Manager)) as Popup_Manager;
            }
            if (instance == null)
            {
                GameObject obj = new GameObject("Popup_Manager");

                instance = obj.AddComponent(typeof(Popup_Manager)) as Popup_Manager;

                DontDestroyOnLoad(instance);
            }


            return instance;
        }
    }


    void Start()
    {

        Init();
        
    }

    public void Init()
    {
        for (int i = 0; i < popup_list.Count; i++)
        {
            popup_list[i].gameObject.SetActive(false);
        }

        beforeindex = -1;

        popup_stack.Clear();

        popup_bg.GetComponent<UIPanel>().depth = 5;

        popup_bg.SetActive(false);
    }

    public void Popup_Push(int index = 0)
    {
        if (index != 0)
            popup_index = index;

        if (beforeindex == popup_index)
            return;

        beforeindex = popup_index;

        switch (popup_index)
        {
            case 0:
            case 1:
            case 2:
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 21:
            case 26:
            case 27:

            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 38:
                m_popuptype = PopUpType.parents;
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 12:
            case 15:
            case 19:
            case 20:
            case 22:
            case 23:
            case 24:
                m_popuptype = PopUpType.child;
                break;

        }
        StartCoroutine(displaypop());
        
       

       
    }
    IEnumerator displaypop()
    {

        popup_bg.GetComponent<UIPanel>().depth = 5;

        popup_stack.Push(popup_index);

        /*
        if (popup_index == 19 || popup_index == 20 || popup_index == 21 || popup_index == 25)
        {
            popup_bg.GetComponent<UIPanel>().depth = 13;
        }

        if (popup_stack.Count > 0)
        {
            int bg_depth = popup_stack.Peek();
            popup_bg.GetComponent<UIPanel>().depth = popup_list[bg_depth].GetComponent<UIPanel>().depth+1;

         }
        */

        int bg_depth = popup_stack.Peek();
        popup_bg.GetComponent<UIPanel>().depth = popup_list[bg_depth].GetComponent<UIPanel>().depth - 1 ;

       // popup_stack.Push(popup_index);

        popup_bg.SetActive(true);

        if (popup_index == 29 )
        {
            popup_bg.SetActive(false);
        }

        GameManager.Instance.m_isTouch = false;
        if (m_popuptype == PopUpType.parents && popup_index != 14)
            yield return new WaitForSeconds(0.2f);
        else
            yield return null;

        GameManager.Instance.m_isTouch = true;

        popup_list[popup_index].gameObject.SetActive(true);

        Debug.Log("popup_index::::::::::::::::::::" + popup_index);

        Debug.Log("popup_stack::::::::::::::::::::" + popup_stack.Count);

        switch (popup_index)
        {
            case 0:
                popup_list[popup_index].GetComponent<DailyRewardPop>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 1:
                popup_list[popup_index].GetComponent<OptionPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 2:
                popup_list[popup_index].GetComponent<ShopPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 4:
                popup_list[popup_index].GetComponent<GameContinuePop>().Init();
                SoundManager.Instance.PlayEffect("gameover");
                SoundManager.Instance.PlayEffect("gameover2");
                break;
            case 5:
                popup_list[popup_index].GetComponent<GameinfoPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 6:
                popup_list[popup_index].GetComponent<MissionPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 7:
                popup_list[popup_index].GetComponent<AD_Free>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 8:
                popup_list[popup_index].GetComponent<Rate>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 9://골드 기프트 팝업  골드세팅
                popup_list[popup_index].GetComponent<Special_Gift>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 10:
                popup_list[popup_index].GetComponent<Game_Center>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 11:
                popup_list[popup_index].GetComponent<GateWayPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 12:
                popup_list[popup_index].GetComponent<SuperSale>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 13:
                popup_list[popup_index].GetComponent<GameQuitPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 14:
                popup_list[popup_index].GetComponent<GameOverPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 15:
                popup_list[popup_index].GetComponent<ItemBuyPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 16:
                popup_list[popup_index].GetComponent<StageClearPop>().Init();

               
                break;
            case 17:
                popup_list[popup_index].GetComponent<PausePop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 18:
                popup_list[popup_index].GetComponent<GameResultPop>().Init();

                break;
            case 19:
                popup_list[popup_index].GetComponent<TruckInfoPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 20:
                popup_list[popup_index].GetComponent<FoodTruckSalePop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 21:
                popup_list[popup_index].GetComponent<TruckAlbumPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 22:
                popup_list[popup_index].GetComponent<TruckAlbumPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 24:
                popup_list[popup_index].GetComponent<RewardtruckPop>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 25:
                popup_list[popup_index].GetComponent<MyBestTruckPop>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 26:
                popup_list[popup_index].GetComponent<SpecialMissionPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 27:
                popup_list[popup_index].GetComponent<MissionRewardPop>().Init();
                SoundManager.Instance.PlayEffect("reward");
                break;
            case 28:
                popup_list[popup_index].GetComponent<FoodTruckTutorial1>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 29:
                popup_list[popup_index].GetComponent<FoodTruckTutorial2>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 30:
                popup_list[popup_index].GetComponent<FoodTruckTutorial3>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 31:
                popup_list[popup_index].GetComponent<FoodTruckTutorial4>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 32:
                popup_list[popup_index].GetComponent<LanguagePop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 33:
                popup_list[popup_index].GetComponent<NoticePop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 34:
                popup_list[popup_index].GetComponent<FreeRewardPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 35:
                popup_list[popup_index].GetComponent<SpecialItemPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 36:
                popup_list[popup_index].GetComponent<TruckShopPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 37:
                popup_list[popup_index].GetComponent<RandomBoxTruckPop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 38:
                popup_list[popup_index].GetComponent<SharePop>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 41:
                popup_list[popup_index].GetComponent<OneButtonPopup>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
            case 42:
                popup_list[popup_index].GetComponent<TwoButtonPopup>().Init();
                SoundManager.Instance.PlayEffect("popup");
                break;
        }

       

      
    }
    public void Popup_Push1()
    {
        
        if (popup_stack.Count > 0)
        {
            int bg_depth = popup_stack.Peek();
            popup_bg.GetComponent<UIPanel>().depth = popup_list[bg_depth].GetComponent<UIPanel>().depth;
        }

        popup_list[popup_index].gameObject.SetActive(true);
        popup_stack.Push(popup_index);

        popup_bg.SetActive(true);
    }

    public bool isPopupAlive(int index)
    {
        return popup_stack.Contains(index) && popup_list[index].gameObject.activeInHierarchy;
    }

    public void Popup_Pop()
    {

       // Debug.Log("popup_stack.Count::::::::::::::" + popup_stack.Count);

       
         SoundManager.Instance.PlayEffect("button_click");
               
        //GameManager.Instance.m_button_release_sound = false;

        int last_index = 0;

        beforeindex = -1;

        if (popup_stack.Count > 0)
        {
           
           int pop_index = popup_stack.Pop();
           popup_list[pop_index].gameObject.SetActive(false);

           if (popup_stack.Count > 0)
           {
               last_index = popup_stack.Peek();
               popup_bg.GetComponent<UIPanel>().depth = popup_list[last_index].GetComponent<UIPanel>().depth-1;
               popup_list[last_index].gameObject.SetActive(true);

            }
           else
           {
                beforeindex = -1;
                popup_bg.gameObject.SetActive(false);
           }
        }
        else if (popup_stack.Count <= 0)
        {
            beforeindex = -1;
            popup_bg.SetActive(false);
           
            if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
            {

            }
            else
            {
                popup_index = 13;
                Popup_Push();
               
            }

           


        }
        switch (last_index)
        {
            case 11://게이트 웨이
                popup_list[11].GetComponent<GateWayPop>().MyGoldSetting();
                break;
            case 4:
                popup_list[4].GetComponent<GameContinuePop>().nowGoldSetting();
                break;
            case 1:
                popup_list[1].GetComponent<OptionPop>().Init();
                break;
            case 24:
                if (popup_stack.Count > 0 && !GameManager.Instance.gameresultchk)
                {

                    popup_list[3].GetComponent<GameResultPop>().playbuttondisplay();
                }
                break;
          
        }

        switch (popup_index)
        {
            //데일리 보상이면 적용
            case 0:
                if (GameManager.Instance.m_adsRewardchk)
                {
                }
                else
                {
                    MyInfoSetting();

                    TimeManager.Instance.DailySetting();

                }

                GameManager.Instance.m_adsRewardchk = false;
 
                break;
            case 1:
                if (GameManager.Instance.m_coupongoldechk)
                {
                    WorldMapUI.instance.GoldSetting(GameManager.Instance.m_coupongold);
                }

                GameManager.Instance.m_coupongoldechk = false;
                break;
            case 2:
                shoppopchk = false;
                break;
            case 7:

                 Popup_Manager.instance.addfree_popupchk = false;
                 if (GameManager.Instance.newstageclear)
                 {
                     StageItemManager.Instance.StageSartSetting(StageItemManager.Instance.rewardstar);
                 }

                break;
            case 8:
                GameManager.Instance.m_ratepopchk = false;
                if (GameManager.Instance.newstageclear)
                {
                    StageItemManager.Instance.StageSartSetting(StageItemManager.Instance.rewardstar);
                }
                

                break;
            case 9:
                GameManager.Instance.m_isTouch = false; //moneyadd()코루틴에서 true
                WorldMapUI.instance.dailyrewardgold.gameObject.SetActive(true);
               
                break;
            //푸드트럭 보상팝업이 사라질때 게임결과창 버튼 생성
            case 24:
                if (popup_stack.Count > 0 && !GameManager.Instance.gameresultchk)
                {
         
                    popup_list[3].GetComponent<GameResultPop>().playbuttondisplay();
                }
                break;
            case 33:
                if (StageItemManager.Instance.TotalnowStage == 29 && !GameManager.Instance.m_specialPopchk)
                {
                     GameManager.Instance.m_specialPopchk = true;
                     SecurityPlayerPrefs.SetInt("SpecialPopchk", GameManager.Instance.m_specialPopchk == true ? 1 : 0);
                     WorldMapUI.instance.WorldmapStart();
                }

              
                 
                break;
        }
    }

    public void MyInfoSetting()
    {
        //골드이면
        if (popup_list[popup_index].GetComponent<DailyRewardPop>().DailyRewardtype == 0)
        {

            int rewardgold = popup_list[popup_index].GetComponent<DailyRewardPop>().DailyRewardcnt;

            WorldMapUI.instance.GoldSetting(rewardgold);
           // WorldMapUI.instance.AddMyGold();

         }
            //아이템이면
        else if (popup_list[popup_index].GetComponent<DailyRewardPop>().DailyRewardtype == 1)
        {
            int myitemtype = popup_list[popup_index].GetComponent<DailyRewardPop>().itemtype;
            GameManager.Instance.m_GameData.MyItemCnt[myitemtype]++;
            SecurityPlayerPrefs.SetInt("MyItem" + myitemtype, GameManager.Instance.m_GameData.MyItemCnt[myitemtype]);
            
        }else if(popup_list[popup_index].GetComponent<DailyRewardPop>().DailyRewardtype == 2)
        {
            Debug.Log("Food truck Free Use:::::::::::::::::");
            
        }
    }


    public void AddFreePopSetting()
    {


        if (StageItemManager.Instance.TotalnowStage < 10)
            return;

        //광고제거 결제 하지 않고
        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {

           // Debug.Log("GameManager.Instance.m_fulladdisplaycnt 1111111111:::::::::::::::::" + GameManager.Instance.m_fulladdisplaycnt);

            //첫날이 지났을때
            if (GameManager.Instance.m_addfreedisplaychk)
            {

                //Debug.Log("GameManager.Instance.m_fulladdisplaycnt 222222222222:::::::::::::::::" + GameManager.Instance.m_fulladdisplaycnt);

                //Debug.Log("GameManager.Instance.m_fulladdisplaycnt 222222222222:::::::::::::::::" + GameManager.Instance.m_GameData.AdsFreeDisplayCnt);
              if ((GameManager.Instance.m_fulladdisplaycnt == 2 || GameManager.Instance.m_fulladdisplaycnt == 4) && GameManager.Instance.m_GameData.AdsFreeDisplayCnt < 8)
                {

                   // Debug.Log("GameManager.Instance.m_fulladdisplaycnt 3333333333333:::::::::::::::::" + GameManager.Instance.m_fulladdisplaycnt);

                   // GameManager.Instance.m_GameData.AdsFreeDisplayCnt++;

                    addfree_popupchk = true;

                    //SecurityPlayerPrefs.SetInt("Addfreepopupcnt", GameManager.Instance.m_GameData.AdsFreeDisplayCnt);

                    //Popup_Manager.INSTANCE.popup_index = 7;

                    //Popup_Manager.INSTANCE.Popup_Push();

                    //광고 제거 팝업
                }
            }
            
        }
        

   
    }

    bool isPause = false;
    void OnApplicationPause(bool pauseStatus)
    {
        //true 이면 pause
        //false 이면 resume

        //게임이 시작할때 시작Scene이면 false(resume)값으로 이 함수가 호출된다.
        //그래서 isPause변수로 pause가 됐을때만 동작하도록 체크한다.
        //하지만 실제 디바이스에서는 Logo씬부터 타기 때문에 문제가 없다.
        //PC상에서 Threematch씬으로 실행할때 뜨는 에러 방지용이다.

        if (GameManager.Instance.m_GameData.GamePlayFirst) return;

        if (pauseStatus)
        {
            isPause = true;          
        }
        else if(isPause)
        {
            isPause = false;
            OnResumePauseChk();
        }
    }

    public void OnResumePauseChk()
    {
        if (GameManager.Instance.freemovechk)
        {
            //광고 노출 시간 체크
            GameManager.Instance.ResumeAdsTimechk();
            GameManager.Instance.freemovechk = false;
            return;
        }

        if (GameManager.Instance.m_ratechk)
        {
            GameManager.Instance.ResumeAdsTimechk();
            GameManager.Instance.m_ratechk = false;
            return;
        }

        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch )
        {
            if (!TutorialManager.Instance.isTutorial &&
                MatchManager.Instance.m_MatchState != MatchState.BonusTime &&
                MatchManager.Instance.m_MatchState != MatchState.GameClear &&
                MatchManager.Instance.m_MatchState != MatchState.GameFail)
            {
                if (Popup_Manager.INSTANCE.popup_stack.Count <= 0)
                {
                    Popup_Manager.INSTANCE.popup_index = 17;

                    Popup_Manager.INSTANCE.Popup_Push();

                    GameManager.Instance.OnResumechk = true;
                }
            }
            
        }
    }

    public bool addisplaychk()
    {
        if (StageItemManager.Instance.TotalnowStage < 10)
            return false;


        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            if (GameManager.Instance.m_gameresult_add_displaychk)
            {

                return true;

                
            }
        }

        return false;
    }
    public void adddisplaysetting()
    {

        if (StageItemManager.Instance.TotalnowStage < 10)
            return;


        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            if (GameManager.Instance.m_gameresult_add_displaychk )
            {

                GameManager.Instance.m_gameresult_add_displaychk = false;

                               

                GameManager.Instance.m_fulladdisplaycnt++;



                //광고 노출 시간 체크
                GameManager.Instance.GameResultAddChk();
              
                  //광고 노출
                AdMob_Manager.Instance.AdMob_FullAd_Show();
                 
                              

            }
        }
    }

    //private bool isTutorialCheck()
    //{
    //    if (!(!TutorialManager.Instance.isTutorial || TutorialManager.Instance.isClose))
    //    {
    //        TutorialManager.Instance.OnPlayButton();
            
    //        return true;
    //    }

    //    return false;
    //}

    void Update()
    {
#if UNITY_ANDROID

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (TutorialManager.Instance.isTutorial)
            {
                TutorialManager.Instance.OnClick_Skip();
                return;
            }

            if (GameManager.Instance.m_isTouch == false)
                return;

            if (popup_stack.Count > 0)
            {
               
                if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.CarPort && (GameManager.Instance.playstatechk || GameManager.Instance.gameresultchk))
                {
                    int index = popup_stack.Peek();

                    if (index != 18)
                    {
                        popup_list[index].GetComponent<BackClickPop>().BackButtonClick();
                    }

                    if (index == 18)
                    {
                        UI_CarPort.Instance.MoveToWorldMap();
                    }
                }
                else
                {
                    int index = popup_stack.Peek();

                    popup_list[index].GetComponent<BackClickPop>().BackButtonClick();
                }
                
               
                
            }
               
            else if (popup_stack.Count <= 0 )
            {
                switch (GameManager.Instance.m_CrruntUI.m_GameState)
                {
                    case GAMESTATE.CarPort:
                        UI_CarPort.Instance.MoveToWorldMap();
                        break;
                    case GAMESTATE.Main:
                        popup_index = 13;
                        Popup_Push();
                        break;
                    case GAMESTATE.ThreeMatch:
                        {
                            if (MatchManager.Instance.m_StepType == StepType.Wait && MatchManager.Instance.isSwitching == false)
                            {
                                popup_index = 17;
                                Popup_Push();
                            }

                            else if (MatchManager.Instance.m_MatchState == MatchState.GameClear)
                            {
                                popup_index = 17;
                                Popup_Push();
                            }

                        }
                        break;
                    case GAMESTATE.WorldMap:
                         popup_index = 13;
                        Popup_Push();
                        break;

                }
            }

            
             
            
        }
#endif
    }

  
}
