﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneButtonPopup : BackClickPop
{
    public static int TitleIndex = 0;
    public static int NoticeIndex = 0;
    public static int Count = 0;
    public static string ImageName = string.Empty;
    public static System.Action OKButtonAction = null;

    [SerializeField]
    private UILabel TitleText = null;
    [SerializeField]
    private UILabel NoticeText = null;
    [SerializeField]
    private UILabel CountText = null;
    [SerializeField]
    private UISprite ImageSprite = null;


    public virtual void Init()
    {
        TitleText.text = TableDataManager.Instance.GetLocalizeText(TitleIndex);
        if (NoticeIndex >= 0)
            NoticeText.text =TableDataManager.Instance.GetLocalizeText(NoticeIndex);
        else
            NoticeText.text = string.Empty;

        if(Count >= 0)
            CountText.text = "+" + Count.ToString();
        else
            CountText.text = string.Empty;

        ImageSprite.spriteName = ImageName;
    }

    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public override void BackButtonClick()
    {
        ExitClick();
        OnOKClickButton();
    }

    public void OnOKClickButton()
    {
        if (OKButtonAction != null)
            OKButtonAction();

        OKButtonAction = null;

        ExitClick();
    }

    public void Show()
    {

    }
}
