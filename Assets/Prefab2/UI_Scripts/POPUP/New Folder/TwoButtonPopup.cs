﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoButtonPopup : OneButtonPopup
{
    public static System.Action CloseButtonAction = null;

    public void OnCloseClickButton()
    {
        if (CloseButtonAction != null)
            CloseButtonAction();

        CloseButtonAction = null;

        ExitClick();
    }

    public override void BackButtonClick()
    {
        ExitClick();
        OnCloseClickButton();
    }
}
