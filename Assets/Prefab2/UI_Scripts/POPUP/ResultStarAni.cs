﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResultStarAni : MonoBehaviour {


    public GameResultPop m_gameresultpop;

    public void NextSartAni()
    {

        m_gameresultpop.ResultStarNext();
    }

    public void GoldPlusAni()
    {
        m_gameresultpop.RewardGoldSetting();
    }

    public void starEffect()
    {
        m_gameresultpop.starEffect();
    }

   
}
