﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class StageClearPop : MonoBehaviour {

    public List<ParticleSystem> effectstar = new List<ParticleSystem>();

    public CarPortTruck truck;

    public GameObject cleartxt;
    //-----------------------------------
    public UISprite stageclearcharacter;

    public int effect_index = 0;

    public GameObject finish_Effect;
 
    public void Init()
    {
        StageClear_Pop();
        //effect_index = 0;
        //for (int i = 0; i < effectstar.Count; i++)
        //{
        //    effectstar[i].gameObject.SetActive(false);
        //    effectstar[i].Stop();
        //}

        ////폭죽 나오는거 삭제
        ////StartCoroutine(EffectStart());

        ////트럭이미지세팅
        //FoodTruckinfo mybestfoodtruck = new FoodTruckinfo();
        //mybestfoodtruck.grade = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade;
        //truck.SetInfo(mybestfoodtruck);
        //truck.transform.localPosition = new Vector3(450, 82, 0);
        ////truck.transform.localScale = new Vector3(2f, 2f, 2f);
        //TruckStart();
    }

    IEnumerator EffectStart()
    {
        //ObjectPool.Instance.GetObject(finish_Effect, transform.parent).transform.localScale = new Vector3(100f, 100f, 100f );

        while (effect_index < effectstar.Count)
        {

            effectstar[effect_index].gameObject.SetActive(true);
            // effectstar[effect_index].gameObject.transform.localPosition = new Vector3(Random.Range(-300f, 300f), Random.Range(-400, 500f), 0f);
            effectstar[effect_index].Play();
            SoundManager.Instance.PlayEffect("hiscore");
            effect_index++;
            yield return new WaitForSeconds(0.25f);
        }

        //stageclearcharacter.gameObject.SetActive(true);

    }

    void TruckStart()
    {
        Sequence seq = DOTween.Sequence();
        //1. 트럭:중간우측에서 중앙으로 이동
        float _movetime2 = 0.5f;
        seq.Append(truck.transform.DOLocalMoveX(0, _movetime2).SetEase(Ease.OutQuart));

        //2. 대기:폭죽 터지는 시간
        //seq.AppendInterval(1f); //0.25f * 4;

        //3. 텍스트
        seq.AppendCallback(() =>
        {
            StageClearTxt_Ani();
        });
        
        //4. 대기:텍스트 애님
        seq.AppendInterval(1f);

        //5. 트럭:중앙에서 좌측으로 사라짐
        float _movetime3 = 0.5f;
        seq.Append(truck.transform.DOLocalMoveX(-450, _movetime3).SetEase(Ease.InQuart));

        //끝
        seq.AppendCallback(() =>
        {
            StageClear_Pop();
        });

    }


    //다른곳에 있는 함수 복사해 옴
    public void StageClearTxt_Ani()
    {
        cleartxt.gameObject.SetActive(true);

        SoundManager.Instance.PlayEffect("clear");

        //Invoke("StageClear_Pop", 1);
    }

    public void StageClear_Pop()
    {
        cleartxt.gameObject.SetActive(false);
        //gameObject.SetActive(false);
        //GetComponent<Animator>().SetBool("CHARACTER_DOWN", false);
        Popup_Manager.INSTANCE.Popup_Pop();

        if (GameManager.Instance.m_specialmissionchk)
        {
            GameManager.Instance.m_GameData.NowMissionLevel++;

            SecurityPlayerPrefs.SetInt("nowmissionlevel", GameManager.Instance.m_GameData.NowMissionLevel);

            if (GameManager.Instance.m_GameData.NowMissionLevel < 5)
            {
                Popup_Manager.INSTANCE.popup_index = 26;

                Popup_Manager.INSTANCE.Popup_Push();
            }
            else
            {
                Popup_Manager.INSTANCE.popup_index = 27;

                Popup_Manager.INSTANCE.Popup_Push();
            }

            GameManager.Instance.m_specialmissionchk = false;
            GameManager.Instance.m_specialmissionstart = false;
        }
        else
        {
            Popup_Manager.INSTANCE.popup_index = 18;
            Popup_Manager.INSTANCE.Popup_Push();
        }

    }
}
