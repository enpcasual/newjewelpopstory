﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FoodTruckTutorial2 : BackClickPop {

    public UISprite Finger;

    public GameObject TalkBox;

    public GameObject skipbutton;

    Sequence Finger_Seq;

    public void Init()
    {
        //사운드
        SoundManager.Instance.PlayEffect("popup");

        //TextBox 세팅
        TalkBox.transform.localPosition = new Vector3(-700, -332, 0);
        TalkBox.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.OutSine);

        UILabel label = TalkBox.GetComponentInChildren<UILabel>();
        label.text = TableDataManager.Instance.GetLocalizeText(148);

        skipbutton.SetActive(true);


        FingerMove();

        FirebaseManager.Instance.LogScreen("food tutorial2");
    }
     
    public void FingerMove()
    {
         Finger.gameObject.SetActive(true);
         Finger.transform.localPosition = CarPortManager.Instance.m_MyFoodTruckList[0].gameObject.transform.localPosition;
            //Finger.transform.DOLocalMove(List_Touch[1], 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);

            Finger_Seq = DOTween.Sequence();
            Finger_Seq.AppendCallback(() =>
            {
                Finger.transform.localPosition = CarPortManager.Instance.m_MyFoodTruckList[0].gameObject.transform.localPosition;
                Finger.color = Color.white;
            });
            Finger_Seq.Append(Finger.transform.DOLocalMove(CarPortManager.Instance.m_MyFoodTruckList[1].gameObject.transform.localPosition, 1f).SetEase(Ease.Linear));
            Finger_Seq.Insert(1f, DOTween.ToAlpha(() => Finger.color, _color => Finger.color = _color, 0f, 0.2F));
            Finger_Seq.SetLoops(-1, LoopType.Restart);
    }
	
    public void FingerEnd()
    {
        Finger_Seq.Kill();
        Finger.gameObject.SetActive(false);

        StartCoroutine(tutotialend());
    }

    public void trucktutorialend()
    {
        StartCoroutine(tutotialend());
    }

    IEnumerator tutotialend()
    {
        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        skipbutton.SetActive(false);

        yield return new WaitForSeconds(0.2f);
      
        Popup_Manager.INSTANCE.Popup_Pop();
    }


    public override void BackButtonClick()
    {
        Finger_Seq.Kill();
     
        Finger.gameObject.SetActive(false);

        skipbutton.SetActive(false);

        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        GameManager.Instance.m_GameData.TruckTutorialChk = true;

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);

         Popup_Manager.INSTANCE.Popup_Pop();

    }

}
