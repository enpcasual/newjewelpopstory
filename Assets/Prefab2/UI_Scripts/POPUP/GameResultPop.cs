﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameResultPop : BackClickPop
{


    public List<UISprite> m_resultstar = new List<UISprite>();

    public List<ParticleSystem> effectstar = new List<ParticleSystem>();

    public List<ParticleSystem> m_newscoreeffect = new List<ParticleSystem>();

    public int starcnt = 1;

    public int nowcnt = 0;

    public UILabel rewardgold;

    public UILabel myscore;

    public int nowgold = 0;

    public int addgold = 0;
    public CarPortTruck rewardtruck;

    //public UISprite balloon;

    public UISprite newscore;

    public CarPortTruck myfoodtruck;

    public UILabel titletxt;

    public int nowscore;

    public int bestscore;

    int rewardtruckindex;

    public UISprite restart;

    public UISprite nextlevel;

    public int beforestarcnt;

    public int[] rewardstarlist = new int[3];

    public UISprite[] rewardgoldlist = new UISprite[3];

    public UILabel[] rewardgoldtxtlist = new UILabel[3];

    public bool foodtruckabilitychk = false;

    public int tempgrade;

    public int tempStage;

    public ScaleAni m_scaleani;

    public UISprite truckgoldani;

    public UISprite goldplus;

    public UILabel nextlabel;

    public bool playbuttondisplaychk;//플레이 버튼이 노출되었는가?

    public bool RewardTruckPopchk;

    public GameObject AdObj = null;
    public GameObject AdCoverObj = null;
   // public UILabel totalplay;


    int bgclickcnt;

    bool firstskipchk;

    bool secondskipchk;

    int totalrewardgold ;

    bool resultskipchk;


    bool nextlevelclickchk;


    int bestStarCnt;
    

    public void Init()
    {
        totalrewardgold = 0;

        bgclickcnt = 0;

        firstskipchk = false;

        secondskipchk = false;

        RewardTruckPopchk = false;

        resultskipchk = false;

        nextlevelclickchk = false;

        //totalplay.text = "playcnt: " + GameManager.Instance.totalplaycnt.ToString();

        //StageItemManager.Instance.stagestarchange = false;

         nowgold = 0;

        addgold = 0;

        nowcnt = 0;

        StartCount = 0;

        playbuttondisplaychk = false;

        foodtruckabilitychk = false;

        //balloon.gameObject.SetActive(false);
        
        rewardtruck.gameObject.SetActive(false);

       rewardtruckindex = 0;

        GameManager.Instance.m_restatchk = false;

        restart.gameObject.SetActive(false);

        nextlevel.gameObject.SetActive(false);

        nextlabel.gameObject.SetActive(false);

        newscore.gameObject.SetActive(false);

        truckgoldani.gameObject.SetActive(false);//푸드트럭 골드플러스 능력치에 의해 보여지는 연출에 나오는 골드

        goldplus.gameObject.SetActive(false);


        //m_scaleani.gameObject.SetActive(false);


        myfoodtruck.ani.SetBool("STOP_CHK", true);

        titletxt.text = "Level "+ (StageItemManager.Instance.nowplaystage+1).ToString() ;

        myscore.text = TableDataManager.Instance.GetLocalizeText(57);



        rewardgold.text = "+" + nowgold.ToString();

       
        

        for (int i = 0; i < m_resultstar.Count; i++)
        {
            m_resultstar[i].gameObject.SetActive(false);
        }


        particledisable();
       


        for (int i = 0; i < 3; i++)
        {
            rewardgoldlist[i].gameObject.SetActive(true);
            rewardgoldtxtlist[i].gameObject.SetActive(true);
        }



        //********인게임 정보 세팅**************
        SSD sd = GameManager.Instance.m_GameData.SSD_GetData(MatchManager.Instance.m_StageIndex);
        bestscore = sd.BestScore;
        nowscore = MatchManager.Instance.GameScore;

        beforestarcnt = sd.BestStartCnt;

        

        starcnt = 1;    //클리어 했다면 무조껀 별 한개로 처리!
        if (MatchManager.Instance.m_CSD.scoreStar2 <= nowscore)
            starcnt++;
        if (MatchManager.Instance.m_CSD.scoreStar3 <= nowscore)
            starcnt++;
        

        //UI처리
        myscore.text += " " + nowscore.ToString();



        ////////////////월드맵에서 푸드트럭 2칸 넘어가는 현상 테스트/////////////////////
       /*
        if (GameManager.Instance.m_restartcnt == 0)
        {
            beforestarcnt = 2;
        }
        else
        {
            beforestarcnt = 3;
        }
        */
        /////////////////////////////////////////////////////////////////////////
        //저장
        sd.BestScore = bestscore > nowscore ? bestscore : nowscore;
        sd.BestStartCnt = beforestarcnt > starcnt ? beforestarcnt : starcnt;
        GameManager.Instance.m_GameData.SSD_Save(MatchManager.Instance.m_StageIndex, sd);

        //*****************************************


        bestStarCnt = sd.BestStartCnt;

        if (nowscore > GameManager.Instance.m_GameData.MyBestScore)
        {


            GameManager.Instance.GPGS_LeaderBoardUpdate(0, nowscore);
        }


        MyFoodTruckChange();

        rewardachive();//보상골드와 별 저장
        
        NextStageSetting();

        RewardTruckSetting();////별3개일때 푸드트럭 추가 보상
        
        if(!AdManager.Instance.IsAdReady || totalrewardgold <= 0)
        {
            AdObj.SetActive(false);
            AdCoverObj.SetActive(false);
        }
        else
        {
            AdObj.SetActive(true);
            AdCoverObj.SetActive(false);
        }

        StartCoroutine(ResultStarAni());
    }

    private void OnDisable()
    {
        if (nowgold > 0)
        {
            FirebaseManager.Instance.LogEvent_AdLocation(MatchManager.Instance.m_StageIndex, "AdditionalCoin_Validate");
        }
    }

    public void MyFoodTruckChange()
    {

        int gradeindex = UI_ThreeMatch.Instance.CurrentTruckGrade;

        if (UI_ThreeMatch.Instance.CurrentTruckGrade < GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
        {
            gradeindex = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade;
        }

        FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[gradeindex];

        myfoodtruck.SetInfo(tempitem);
        /*
        if ( GameManager.Instance.FreeUseTruckGrade > GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade)
        {
            FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[GameManager.Instance.FreeUseTruckGrade];

            myfoodtruck.SetInfo(tempitem);

            if (GameManager.Instance.FreeUsecnt <= 0)
            {
                GameManager.Instance.FreeUseTruckGrade = 0;

                SecurityPlayerPrefs.SetInt("FreeUseTruckGrade", 0);
            }
        }
        else
        {
            FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade];

            myfoodtruck.SetInfo(tempitem);
        }
       
        */
    }

    public void NextStageSetting()
    {
        if (StageItemManager.Instance.isNewStage)
        {
            GameManager.Instance.newstageclear = true;

            GameManager.Instance.GPGS_LeaderBoardUpdate(2, (int)StageItemManager.Instance.TotalnowStage + 1);

            GameManager.Instance.GPGS_AchivenmentCompare(0, (int)StageItemManager.Instance.TotalnowStage + 1);

            if (!StageItemManager.Instance.isLastStage && StageItemManager.Instance.isNewStage)
            {
                int nextstage = (int)StageItemManager.Instance.CurrentStage;

                nextstage++;

                SecurityPlayerPrefs.SetInt("CurrentStage", nextstage);
            }
        }
    }

    public void particledisable()
    {
        for (int i = 0; i < effectstar.Count; i++)
        {
            effectstar[i].gameObject.SetActive(false);
            effectstar[i].Stop();
        }

        for (int i = 0; i < m_newscoreeffect.Count; i++)
        {
            m_newscoreeffect[i].gameObject.SetActive(false);
            m_newscoreeffect[i].Stop();
        }
    }
    //별 하나일때
    
    IEnumerator ResultStarAni()
    {


        yield return new WaitForSeconds(0.8f);

        if (!resultskipchk)
        {
            addgold = rewardstarlist[nowcnt];

            m_resultstar[nowcnt].gameObject.SetActive(true);


            nowcnt++;
        }

    }

    public bool BestScorechk()
    {
        bool Breturn = false;

        if (nowscore > bestscore)
        {
            Breturn = true;
        }

        return Breturn;
    }

    public void rewardachive()
    {
        for (int i = 0; i < 3; i++)
        {
            rewardstarlist[i] = 0;
        }

        switch (beforestarcnt)
        {
            case 0:
                rewardstarlist[0] = TableDataManager.Instance.m_buyprice[7].buyprice;
                rewardstarlist[1] = TableDataManager.Instance.m_buyprice[8].buyprice;
                rewardstarlist[2] = TableDataManager.Instance.m_buyprice[9].buyprice;
                break;
            case 1:
                rewardstarlist[0] = 0;
                rewardstarlist[1] = TableDataManager.Instance.m_buyprice[8].buyprice;
                rewardstarlist[2] = TableDataManager.Instance.m_buyprice[9].buyprice;
                break;
            case 2:
                rewardstarlist[0] = 0;
                rewardstarlist[1] = 0;
                rewardstarlist[2] = TableDataManager.Instance.m_buyprice[9].buyprice;
                break;
            case 3:

               rewardstarlist[0] = 0;
                rewardstarlist[1] = 0;
                rewardstarlist[2] = 0;
                break;
        }

        for (int i = 0; i < 3; i++)
        {
            rewardgoldtxtlist[i].text = rewardstarlist[i].ToString();
        }

        for (int i = 0; i < beforestarcnt; i++)
        {
            rewardgoldlist[i].gameObject.SetActive(false);
            rewardgoldtxtlist[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < starcnt; i++)
        {
            totalrewardgold += rewardstarlist[i];
        }

        if (totalrewardgold > 0)
        {
            totalrewardgold += TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].goldplus;
        }
            

        GameManager.Instance.m_GameData.MyGold += totalrewardgold; 

        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

        StageItemManager.Instance.rewardstar = bestStarCnt;

        if (beforestarcnt < starcnt)
        {
            int addstar = starcnt - beforestarcnt;

           

            GameManager.Instance.m_GameData.MyStar += addstar;

            SecurityPlayerPrefs.SetInt("MyStar", GameManager.Instance.m_GameData.MyStar);
            
            GameManager.Instance.GPGS_AchivenmentCompare(2, GameManager.Instance.m_GameData.MyStar);

            GameManager.Instance.GPGS_LeaderBoardUpdate(3, GameManager.Instance.m_GameData.MyStar);
        }


       

        
        
    }





    public void RewardTruckSetting()
    {
        if (MatchManager.Instance.m_StageIndex >= GameManager.Instance.m_rewardtrucklevel && beforestarcnt < 3)
        {
            for (int i = 0; i < TableDataManager.Instance.m_Foodtruck.Count; i++)
            {
                if (TableDataManager.Instance.m_Foodtruck[i].openlevel >= GameManager.Instance.m_rewardtrucklevel && MatchManager.Instance.m_StageIndex == TableDataManager.Instance.m_Foodtruck[i].openlevel)
                {
                    rewardtruckindex = i;
                }
            }

            if(TableDataManager.Instance.m_Foodtruck[rewardtruckindex].grade <= TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].grade)
            {
                Debug.Log("Truck Grade is Low");
                return;
            }

            GameManager.Instance.rewardTruckgrade = rewardtruckindex;
            
            rewardtruck.gameObject.SetActive(true);

            //balloon.gameObject.SetActive(true);

            FoodTruckinfo temptruck = TableDataManager.Instance.m_Foodtruck[rewardtruckindex];

            rewardtruck.SetInfo(temptruck);
            
            if (starcnt == 3 && rewardstarlist[2] != 0)
            {
                CarPortManager.Instance.AddFoodTruck(rewardtruckindex);
            }
        }
    }
   
    public void ResultStarNext()
    {
      

        if (starcnt > nowcnt)
        {
            m_resultstar[nowcnt].gameObject.SetActive(true);

            addgold = rewardstarlist[nowcnt];

            nowcnt++;
        }
    }

    private int StartCount = 0;
    public void RewardGoldSetting()
    {
        if (!resultskipchk)
        StartCoroutine(rewardgoldani());
    }


    IEnumerator rewardgoldani()
    {
        StartCount++;
        nowgold += addgold;

        yield return new WaitForSeconds(0.2f);

        if (addgold > 0)
        {
            rewardgold.gameObject.transform.localScale = new Vector3(1.4f, 1.4f, 1);
        }
        

        rewardgold.text = "+" + nowgold.ToString();

        SoundManager.Instance.PlayEffect("coin");

        yield return new WaitForSeconds(0.5f);
        
        rewardgold.gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1);

        if(StartCount >= 3 && nowgold <= 0)
        {
            AdCoverObj.SetActive(true);
        }
    }

   
    public void BgClickEvent()
    {



        //myfoodtruck goldplus  적용
        // if (foodtruckabilitychk)

        if (!resultskipchk)
        {
            resultskipchk = true;

            for (int i = 0; i < starcnt; i++)
            {
                m_resultstar[i].gameObject.SetActive(true);

            }

            if (nowcnt < starcnt)
            {
                Vib_Start();
            }

            nowcnt = starcnt;


            for (int i = 0; i < effectstar.Count; i++)
            {
                effectstar[i].Stop();
            }





            if (nowcnt == 3)
            {
                //balloon.gameObject.SetActive(false);
                rewardtruck.gameObject.SetActive(false);
            }

            StopCoroutine(foodtruckability());

            myfoodtruck.GetComponent<CarPortTruck>().ani.SetBool("ABILITY_CHK", false);

            truckgoldani.GetComponent<GoldPlueAni>().truckgoldplusaniend();





            goldplus.gameObject.SetActive(false);
            truckgoldani.gameObject.SetActive(false);




            //푸드트럭 보상파업출력(별3개획득,이전획득한 별이 3개가 아닐때,현재레벨이 20이상)
            if (nowcnt == 3
            && rewardstarlist[2] == TableDataManager.Instance.m_buyprice[9].buyprice
            && MatchManager.Instance.m_StageIndex >= GameManager.Instance.m_rewardtrucklevel && beforestarcnt < 3
            && !foodtruckabilitychk
            && TableDataManager.Instance.m_Foodtruck[rewardtruckindex].grade > TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].grade)
            {

                rewardtruck.gameObject.SetActive(false);
                StartCoroutine(RewardFoodTruck());

                //reward food truck popup
            }
            else
            {

                // playbuttondisplay();
                // StopCoroutine(ButtonDiaplay());
                restart.gameObject.SetActive(true);
                nextlevel.gameObject.SetActive(true);
                nextlabel.gameObject.SetActive(true);
                Popup_Manager.INSTANCE.adddisplaysetting();

            }


            if (BestScorechk())
            {
                StopCoroutine(BestScoreDisplay());
                m_newscoreeffect[0].Stop();
                //m_newscoreeffect[1].Stop();
                newscore.gameObject.SetActive(true);

            }

            StopCoroutine(rewardgoldani());
            rewardgold.text = "+" + totalrewardgold.ToString();
            rewardgold.gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1);
            int ng = 0;
            int.TryParse(rewardgold.text, out ng);
            if (ng <= 0)
            {
                AdCoverObj.SetActive(true);
            }
            GameManager.Instance.GPGS_AchivenmentCompare(4, nowscore);
        }
       
    }
    
    public void BgClickEventback()
    {
        if (bgclickcnt == 0)
        {

            firstskipchk = true;

            bgclickcnt++;

            nowcnt = starcnt-1;

            nowgold = 0;

            for (int i = 0; i < starcnt; i++)
            {
                m_resultstar[i].gameObject.SetActive(true);

                
            }

            for (int i = 0; i < rewardstarlist.Length; i++)
            {
                nowgold += rewardstarlist[i];
            }

            rewardgold.text = "+" + nowgold.ToString();
            nowcnt++;

            laststarsetting();


            for (int i = 0; i < effectstar.Count; i++)
            {
                effectstar[i].Stop();
            }

           
            
           

            Vib_Start();

        }
        else if (bgclickcnt == 1)
        {
            secondskipchk = true;
            bgclickcnt++;
            myfoodtruck.GetComponentInChildren<FoodTruckAbility>().AbilityEnd();
            myfoodtruck.GetComponentInChildren<FoodTruckAbility>().GoldPlusenEnd();
           truckgoldani.GetComponent<GoldPlueAni>().truckgoldplusaniend();


           m_newscoreeffect[0].Stop();
           //m_newscoreeffect[1].Stop();
           newscore.gameObject.SetActive(true);
           newscore.GetComponent<ScaleAni>().ScaleAnichk();

            if(!RewardTruckPopchk)
              RewardFoodTruckPop();
        }

        
    }




    public void starEffect()
    {


        if (!resultskipchk)
        {
           

            if (nowcnt > 0)
            {
                effectstar[nowcnt - 1].gameObject.SetActive(true);
                effectstar[nowcnt - 1].Play();
            }


            SoundManager.Instance.PlayEffect("star" + nowcnt.ToString());

            Vib_Start();

            

            laststarsetting();
        }
       
       
   
        
    }

    public void laststarsetting()
    {

        if (starcnt == nowcnt)
        {
            bgclickcnt = 1;

            if (resultskipchk)
                return;
            //myfoodtruck goldplus  적용
            if (TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].goldplus > 0 && totalrewardgold > 0)
            {

                foodtruckabilitychk = true;

                if (nowcnt == 3)
                {
                    //balloon.gameObject.SetActive(false);
                    rewardtruck.gameObject.SetActive(false);
                }


                StartCoroutine(foodtruckability());


            }
            else
            {
                              
                  
                    RewardFoodTruckPop();
                
            }

            

           

            if (BestScorechk())
            {               
                StartCoroutine(BestScoreDisplay());


            }

            GameManager.Instance.GPGS_AchivenmentCompare(4, nowscore);
        }
    }

    IEnumerator BestScoreDisplay()
    {

        yield return new WaitForSeconds(0.5f);

        SoundManager.Instance.PlayEffect("highscore");
       

        newscore.gameObject.SetActive(true);

        newscore.GetComponent<ScaleAni>().ScaleAnichk();
        

        RewardFoodTruckPop();

    }
    
    // myfoodtruck.GetComponent<CarPortTruck>().ani.SsetBool("IDLE_CHk", false);
    public void RewardFoodTruckPop()
    {
        if (resultskipchk)
            return;

        //푸드트럭 보상파업출력(별3개획득,이전획득한 별이 3개가 아닐때,현재레벨이 20이상, 획득하는 트럭의 등급이 더 높을때에만)
        if (nowcnt == 3
            && rewardstarlist[2] == TableDataManager.Instance.m_buyprice[9].buyprice
            && MatchManager.Instance.m_StageIndex >= GameManager.Instance.m_rewardtrucklevel && beforestarcnt < 3
            && !foodtruckabilitychk
            && TableDataManager.Instance.m_Foodtruck[rewardtruckindex].grade > TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].grade)
        {
            //balloon.gameObject.SetActive(false);
            rewardtruck.gameObject.SetActive(false);
            StartCoroutine(RewardFoodTruck());
            //reward food truck popup
        }
        else
        {
            playbuttondisplay();
        }
    }

    public void playbuttondisplay()
    {
        StartCoroutine(ButtonDiaplay());
    }

   

    public void truckgoldplusani()
    {
         if (resultskipchk)
            return;
        truckgoldani.gameObject.SetActive(true);
    }
    public void goldplussetting()
    {
        if (resultskipchk)
            return;

        addgold = TableDataManager.Instance.m_Foodtruck[myfoodtruck.m_Item.grade].goldplus;

        StartCoroutine(rewardgoldani());
    }
    IEnumerator foodtruckability()
    {
        yield return new WaitForSeconds(0.8f);

        SoundManager.Instance.PlayEffect("plus");
        myfoodtruck.GetComponent<CarPortTruck>().ani.SetBool("ABILITY_CHK", true);

    }

    IEnumerator ButtonDiaplay()
    {

       
        yield return new WaitForSeconds(0.8f);

       
           restart.gameObject.SetActive(true);
           nextlevel.gameObject.SetActive(true);

           nextlabel.gameObject.SetActive(true);

           resultskipchk = true; 

          yield return new WaitForSeconds(0.6f);

          Popup_Manager.INSTANCE.adddisplaysetting();

    }


    IEnumerator RewardFoodTruck()
    {
        yield return new WaitForSeconds(0.9f);

        particledisable();

        resultskipchk = true;

        Popup_Manager.INSTANCE.popup_index = 24;

        Popup_Manager.INSTANCE.Popup_Push();

        MyFoodTruckChange();

        //yield return new WaitForSeconds(1f);

        //var defaultTruckPos = myfoodtruck.transform.localPosition;
        //myfoodtruck.transform.localPosition = new Vector3(400, defaultTruckPos.y, defaultTruckPos.z);

        //yield return new WaitWhile(() => Popup_Manager.INSTANCE.isPopupAlive(24));
        
        //myfoodtruck.MoveTo(myfoodtruck.transform.localPosition, defaultTruckPos, null);
    }


   
    public void restartclick()
    {
       
        GameManager.Instance.m_restatchk = true;

        GetComponent<Parent_Pop>().ComeBackGame();


        SoundManager.Instance.PlayEffect("play_click");
    }


    public void nextlevelclick()
    {
#if UNITY_EDITOR
        if (GameManager.Instance.isPlayFromEditor)
        {
            MatchManager.Instance.GoToEditor();
            return;
        }
#endif
        //GetComponent<Parent_Pop>().ComeBackGame();

        //GameManager.Instance.totalplaycnt = 0;

        if (!nextlevelclickchk)
        {
            nextlevelclickchk = true;

            SoundManager.Instance.PlayEffect("play_click");

            GetComponent<Parent_Pop>().Completer = () =>
            {
                if (StageItemManager.Instance.nowplaystage == StageItemManager.Instance.TotalnowStage - 1)
                {
                    GameManager.Instance.SetGameState(GAMESTATE.ThreeMatch);
                    GameManager.Instance.GameStart(StageItemManager.Instance.TotalnowStage + 1);
                }
            };
            GetComponent<Parent_Pop>().MoveToBottom();

            StageItem.isNextPlayButtonClick = true;
        }
    }
    
    public override void BackButtonClick()
    {
        PlayExit();
    }

    public void PlayExit()
    {
#if UNITY_EDITOR
        if (GameManager.Instance.isPlayFromEditor)
        {
            MatchManager.Instance.GoToEditor();
            return;
        }
#endif
        
        if (Popup_Manager.INSTANCE.addisplaychk())
        {
           
            Popup_Manager.INSTANCE.adddisplaysetting();
            GetComponent<Parent_Pop>().MoveToBottom();
            //Popup_Manager.INSTANCE.Popup_Pop();
        }
        else
        {
            GetComponent<Parent_Pop>().MoveToBottom();
        }

    }

    public void Vib_Start()
    {
        gameObject.transform.localScale = new Vector3(1.05f, 1.05f, 1);

        Invoke("Vib_End", 0.1f);
    }

    public void Vib_End()
    {
        
        gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1);
    }

    public void scoregoldani()
    {

        
        m_scaleani.ScaleAnichk();


    }

    public void OnAdditionalCoinButtonClick()
    {
        AdCoverObj.SetActive(true);
        AdManager.AdsRewardResult = () =>
        {
            resultskipchk = false;
            nowgold = (totalrewardgold * 5);
            addgold = 0;
            RewardGoldSetting();

            GameManager.Instance.m_GameData.MyGold += (totalrewardgold * 5) - totalrewardgold;
            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            resultskipchk = true;
        };

        AdManager.Instance.Ad_Show("AdditionalCoin");
    }
}
