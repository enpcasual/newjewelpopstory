﻿using UnityEngine;
using System.Collections;


public class OptionPop : BackClickPop
{
    public UILabel userID;

    public UILabel title;

    public UILabel nitification;

    public UILabel howtoplay;

    public UILabel support;

    public UILabel notification;

    public UILabel PrivacyPolicyLabel;

    public UILabel TermsofServiceLabel;

    public UISprite chkbox;

    public UISprite chkeckon;

    public UISprite effect_off;

    public UISprite bgsound_off;

    public UISprite notification_off;

    //cp
    public GameObject Cp_Obj;
    public UILabel lb_InputField;

    public UILabel language_label;

    //fb
    public UILabel lb_fbLink;

    public TextLocalization SaveLocalization = null;
    public TextLocalization LoadLocalization = null;

    public GameObject CheatObj = null;

    public void Init()
    {
#if UNITY_EDITOR || ENABLE_CHEAT || DEVELOPMENT_BUILD
        CheatObj.SetActive(true);
#endif


        TimeManager.Instance.optionSetting();

        optionchk();
        //FontManager.instance.FontSetting(title);
       // FontManager.instance.FontSetting(howtoplay);
       // FontManager.instance.FontSetting(support);
       // FontManager.instance.FontSetting(language_label);

        fontchange();

        userID.text = string.Empty;

        //userID.text = "USER ID : "+ Native_Brige.GetUUID();

        title.text = TableDataManager.Instance.GetLocalizeText(19);
        
        nitification.text = TableDataManager.Instance.GetLocalizeText(20);
        
        howtoplay.text = TableDataManager.Instance.GetLocalizeText(21);
        
        support.text = TableDataManager.Instance.GetLocalizeText(22);

        language_label.text = TableDataManager.Instance.GetLocalizeText(184);

        lb_InputField.text = TableDataManager.Instance.GetLocalizeText(181);

        PrivacyPolicyLabel.text = TableDataManager.Instance.GetLocalizeText(239);

        TermsofServiceLabel.text = TableDataManager.Instance.GetLocalizeText(240);

        //lb_fbLink.text = FaceBook_Manager.isLoggedIn ? "Logout" : "Login";

        SaveLocalization.OnEnable();
        LoadLocalization.OnEnable();
#if UNITY_IOS
        Cp_Obj.SetActive(false);
#endif
    }

    public void fontchange()
    {
       FontManager [] fontlist =  this.GetComponentsInChildren<FontManager>();

       for (int i = 0; i < fontlist.Length; i++)
       {
           fontlist[i].FontSetting();
       }
    }
    public void optionchk()
    {

        if (SoundManager.Instance.isEffect == false)
        {
            effect_off.gameObject.SetActive(true);
        }
        else
        {
            effect_off.gameObject.SetActive(false);
        }

        if (SoundManager.Instance.isBGM == false)
        {
            bgsound_off.gameObject.SetActive(true);
        }
        else
        {
            bgsound_off.gameObject.SetActive(false);
        }

        if (TimeManager.Instance.notification == 1)
        {
            chkeckon.gameObject.SetActive(true);
        }


    }

    public void EffectSoundSetting()
    {

       
        
        //bool effect_snd = SecurityPlayerPrefs.GetInt("isEffect", 1) == 1 ? true : false;

        if (GameManager.Instance.m_GameData.Effect_Snd == false)
        {
            SoundManager.Instance.isEffect = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SecurityPlayerPrefs.SetInt("isEffect", 1);
            effect_off.gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Effect_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Effect_Snd == true)
        {
            SoundManager.Instance.isEffect = false;
            SecurityPlayerPrefs.SetInt("isEffect", 0);
            effect_off.gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Effect_Snd = false;
        }

        
    }

    public void BGSoundSetting()
    {
        

       // bool bg_snd = SecurityPlayerPrefs.GetInt("isBGM", 1) == 1 ? true : false;

        if (GameManager.Instance.m_GameData.Bg_Snd == false)
        {
            SoundManager.Instance.isBGM = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SoundManager.Instance.PlayBGM("intro_bgm");
            SecurityPlayerPrefs.SetInt("isBGM", 1);
            bgsound_off.gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Bg_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Bg_Snd == true)
        {
            SoundManager.Instance.isBGM = false;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SoundManager.Instance.StopBGM();
            SecurityPlayerPrefs.SetInt("isBGM", 0);
            bgsound_off.gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Bg_Snd = false;
        }
    }

    public void NotificationSetting()
    {

        //GameManager.Instance.m_notification = SecurityPlayerPrefs.GetInt("notification", 1);

        notification_off.gameObject.SetActive(GameManager.Instance.m_GameData.Notification == 1);

        if (GameManager.Instance.m_GameData.Notification == 0)
        {
            GameManager.Instance.m_GameData.Notification = 1;
            SecurityPlayerPrefs.SetInt("notification", GameManager.Instance.m_GameData.Notification);
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            chkeckon.gameObject.SetActive(true);
            //TimeManager.Instance.AlarmSetting("alarmSetting");

            //GCM 푸시 설정
            //IGAWorks_Manager.instance.SetNotificationIcon(true);
        }
        else if (GameManager.Instance.m_GameData.Notification == 1)
        {
            GameManager.Instance.m_GameData.Notification = 0;
            SecurityPlayerPrefs.SetInt("notification", GameManager.Instance.m_GameData.Notification);
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            chkeckon.gameObject.SetActive(false);
            //TimeManager.Instance.releaseAlarm("releasealarm");

            //GCM 푸시 설정
            //IGAWorks_Manager.instance.SetNotificationIcon(false);
        }
    }

    public void Support()
    {
        string email = "Global_apps@enpgames.co.kr";
        string subject = MyEscapeURL("★Food Pop★ Support Mail");
        string body = MyEscapeURL("-------------------------------------\n" /*+ "UserID:" + Native_Brige.GetUUID() + "\n" + "Device Model:" + SystemInfo.deviceModel + "\n" + "Device OS:" + SystemInfo.operatingSystem + "\n-------------------------------------\n"*/);
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    public static string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void LanguageClick()
    {
        Popup_Manager.INSTANCE.popup_index = 32;

        Popup_Manager.INSTANCE.Popup_Push();

    }

    public override void BackButtonClick()
    {
        GetComponent<Parent_Pop>().MoveToBottom();
    }
    public void displayhowtoplay()
    {
        Popup_Manager.INSTANCE.popup_index = 5;

        Popup_Manager.INSTANCE.Popup_Push();
    }

#region Mail
#endregion

#region Save & Load
    public void Save()
    {
        Popup_Manager.INSTANCE.popup_index = 33;
        Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(191), Save_Action);
        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void Save_Action()
    {
#if UNITY_ANDROID
        GPGS_Manager.Instance.OpenSavedGame("rf_sd", GPGS_Manager.SaveGameType.Save);
#elif UNITY_IOS
        GCIC_Manager.Instance.OpenSavedGame(GCIC_Manager.SaveGameType.Save);
#endif
    }

    public void Load()
    {
        Popup_Manager.INSTANCE.popup_index = 33;
        Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(194), Load_Action);
        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void Load_Action()
    {
#if UNITY_ANDROID
        GPGS_Manager.Instance.OpenSavedGame("rf_sd", GPGS_Manager.SaveGameType.Load);
#elif UNITY_IOS
        GCIC_Manager.Instance.OpenSavedGame(GCIC_Manager.SaveGameType.Load);
#endif
    }
    #endregion

    #region FaceBook

    //함수명은 누군가 고쳐줄꺼야..
    public void FacebookLogin()
    {
        //if (!FaceBook_Manager.isLoggedIn)
        //{
        //    FaceBook_Manager.Instance.FBLogin((isSuccess, msg) =>
        //    {
        //        if(string.IsNullOrEmpty(msg))
        //        {
        //            Debug.Log("[OptionPop]FB Login Failed in FacebookLogin.");
        //        }

        //        lb_fbLink.text = isSuccess ? "Logout" : "Logout";
        //    });
        //}
        //else
        //{
        //    FaceBook_Manager.Instance.FBLogout();
        //    lb_fbLink.text = "Login";

        //}
    }
    #endregion

    #region CP
    public void CpApply()
    {
        Debug.Log("okcp : " + lb_InputField.text);

        string input_cp = lb_InputField.text;

        //NetWorkManager.instance.Req_confirmCoupon(input_cp);

        /* 엑셀데이타로 자체 쿠폰 적용
        if (SecurityPlayerPrefs.GetInt("CP_" + input_cp, 0) == 0)
        {
            bool _ok = false;
            for (int i = 0; i < TableDataManager.Instance.m_cp.Count; i++)
            {
                
                if (input_cp.Equals(TableDataManager.Instance.m_cp[i].num))
                {
                    //GameManager.Instance.m_MyGold += TableDataManager.Instance.m_cp[i].value;
                    //SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_MyGold);

                    GameManager.Instance.m_coupongold = TableDataManager.Instance.m_cp[i].value;

                    GameManager.Instance.m_coupongoldechk = true;

                    SecurityPlayerPrefs.SetInt("CP_" + input_cp, 1);

                    _ok = true;
                    lb_InputField.text = "OK";

                    GetComponent<Parent_Pop>().MoveToBottom();
                    break;
                }
            }

            if (_ok == false)
                lb_InputField.text = TableDataManager.Instance.GetLocalizeText(182);
        }
        else
        {
            //이미 사용한 쿠폰.
            lb_InputField.text = TableDataManager.Instance.GetLocalizeText(183);
        }
        */
    }
    #endregion
    #region 이용약관 & 개인정보정책
    public void PrivacyPolicy()
    {
        FirebaseManager.Instance.LogEvent_GDPR(1, Application.systemLanguage.ToString());
        //if (UI_Main.Instance.CheckGDPR())
        //{
        //    OpenDataPrivacyUrl();
        //}
        //else
        {
            //Application.OpenURL("http://member.brabragames.jp/casual_policy/braeve_privacy.html");
            Popup_Manager.INSTANCE.popup_index = 40;
            Popup_Manager.INSTANCE.Popup_Push();
        }
    }

    bool urlOpened = false;

    private void OnFailure(string reason)
    {
        Debug.LogWarning(System.String.Format("Failed to get data privacy url: {0}", reason));
    }

    private void OpenUrl(string url)
    {
        Debug.Log(System.String.Format("Received data privacy URL: {0}", url));
        urlOpened = true;

#if UNITY_WEBGL && !UNITY_EDITOR
            Application.ExternalEval("window.open(\"" + url + "\",\"_blank\")");
#else
        Application.OpenURL(url);
#endif
    }

    private void OpenDataPrivacyUrl()
    {
        UnityEngine.Analytics.DataPrivacy.FetchPrivacyUrl(OpenUrl, OnFailure);
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus && urlOpened)
        {
            urlOpened = false;
            UnityEngine.Analytics.DataPrivacy.FetchOptOutStatus();
        }
    }

    public void TermsofService()
    {
        //Application.OpenURL("http://member.brabragames.jp/casual_policy/braeve_service.html");
        Popup_Manager.INSTANCE.popup_index = 39;
        Popup_Manager.INSTANCE.Popup_Push();
    } 

    #endregion

    public void OpenFBSite()
    {
        Application.OpenURL("https://www.facebook.com/Enpbycasual");
    }

    public void OpenTWSite()
    {
        Application.OpenURL("https://twitter.com/enpbycasual");
    }
}
