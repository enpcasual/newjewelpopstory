﻿using UnityEngine;
using System.Collections;

public class ShopItem : MonoBehaviour
{

    public UILabel paylabel;

    public UILabel goldlabel;

    public UISprite goldicon;

    public UILabel salegold;

    public UILabel beforesalegold;

    public UILabel saleprecent;

    public GameObject shopsale;

    public UISprite mostpopular;

    public UISprite bestvalue;

    public UISprite restroe;

    public UISprite m_button;

    public int itemindex;

    public bool isNoShopItem = false;

    public void Init(int index)
    {
        if (TimeManager.Instance.supersaletimechk && !isNoShopItem)
        {
            shopsale.SetActive(true);
            goldlabel.gameObject.SetActive(false);
        }
        else
        {
            shopsale.SetActive(false);
            goldlabel.gameObject.SetActive(true);
        }

        itemindex = index;
    }

    public void GoldSetting(int gold)
    {
        if(gold != 0 && !isNoShopItem)
        goldlabel.text = gold.ToString("N0");


    }

    public void PaySetting(string pay)
    {
        paylabel.text = pay;
    }

    public void GoldSpriteSetting(string sprname)
    {

        goldicon.spriteName = sprname;

    }

    public void SaleGoldSetting(int beforeglod, int nowgold)
    {
        if (!isNoShopItem)
        {
            beforesalegold.text = beforeglod.ToString("N0");
            salegold.text = nowgold.ToString("N0");
        }
    }

    public void displaymostpopular()
    {
        mostpopular.gameObject.SetActive(true);
    }

    public void displaybestvalue()
    {
        bestvalue.gameObject.SetActive(true);
    }

    public void BuyCoinClick()
    {

        SoundManager.Instance.PlayEffect("button_click");
        Debug.Log("BuyCoinClick  itemindex ::::::" + itemindex);

        switch(itemindex)
        {
            case 1:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_1);
                break;
            case 2:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_2);
                break;
            case 3:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_3);
                break;
            case 4:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_4);
                break;
            case 5:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_5);
                break;
            case 6:
                InAppPurchaser.Instance.BuyProduct(ProductKind.gold_6);
                break;
        }

    }
}