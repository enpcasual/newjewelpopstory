﻿using UnityEngine;
using System.Collections;
using System;

public class ShopPop : BackClickPop
{

    public UIGrid m_Grid;

    public UILabel saletime;

    public bool restorchk;

    public UIScrollView m_scrollView;

    public UIGrid m_grid;

    public UILabel titletxt;

    public UILabel m_nowgold;

    public UISprite buycoin;

    int nowMoney;

    int addmoney;

    int movegab;

     Color orgColor;

     public UISprite freecoin;

     public UILabel freecointime;

     public UISprite freecoineffect;

    public GameObject Restore = null;

    void OnEnable()
    {
        AdMob_Manager.Instance.AdMob_Banner_Hide();
    }

    void OnDisable()
    {
        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch
            && GameManager.Instance.m_GameData.AdsFreeChk == false)
        {
            AdMob_Manager.Instance.AdMob_Banner_Show();
        }
    }

    public void Init()
    {
        FirebaseManager.Instance.LogEvent_AdLocation(0, "Shop_Validate");
        FreeCoinSetting();

        ShopPopSetting();

        m_grid.Reposition();

        m_scrollView.ResetPosition();

        titletxt.text = TableDataManager.Instance.GetLocalizeText(129);

        m_nowgold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");

        Popup_Manager.INSTANCE.shoppopchk = true;

#if UNITY_IOS
        Restore.SetActive(true);
#endif
    }

    public void FreeCoinSetting()
    {
        if (AdManager.Instance.IsAdReady)
        {
            if (TimeManager.Instance.freecoinchk)
            {
                freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

                freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

                freecoineffect.gameObject.SetActive(false);

                freecointime.gameObject.SetActive(true);
            }
            else
            {
                freecoindisplay();
            }
        }
        else
        {
            freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

            freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

            freecoineffect.gameObject.SetActive(false);
        }
       
    }


    public void VideoRewardResultSetting()
    {
        TimeManager.Instance.freecoinchk = true;

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

        freecoineffect.gameObject.SetActive(false);

        freecointime.gameObject.SetActive(true);
    }

    public void UnityAdsRewardResult()
    {


       
        long cooltime = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].cooltime;

     
        long time = DateTime.Now.AddSeconds(cooltime).Ticks;

        TimeManager.Instance.m_freecointime = time;


        string cooltimestr = TimeManager.Instance.RefreshTimer(TimeManager.Instance.m_freecointime, 3);

        WorldMapUI.instance.VideoRewardResultSetting();

        //텍스트갱신 1.월드맵
        if (WorldMapUI.instance != null)
            WorldMapUI.instance.freecointimedisplay(cooltimestr);
        //텍스트갱신 2.샵
        Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().freecointimedisplay(cooltimestr);
        //텍스트갱신 3.인게임버튼
        if (UI_ThreeMatch.Instance != null)
            UI_ThreeMatch.Instance.freecointimedisplay(cooltimestr);

        switch (TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].type)
        {
            case 0://골드
                {

                    GoldSetting(TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].reward);


                }
                break;
            case 1://아이템
                {
                    int itemindex = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].kind - 1;
                    GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                    SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                }
                break;
        }

             

        SecurityPlayerPrefs.SetLong("FreeCoinTime", TimeManager.Instance.m_freecointime);


       

    }

    public void freecoindisplay()
    {
        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", false);

        freecoineffect.gameObject.SetActive(true);

        freecointime.gameObject.SetActive(false);

    }


    //보상형 광고 클릭
    public void freecoinclick()
    {
        if (AdManager.Instance.IsAdReady)
        {
            if (!TimeManager.Instance.freecoinchk)
            {
                AdManager.AdsRewardResult = null;
                AdManager.Instance.Ad_Show("Shop");
            }
        }
    }


    public void freecoinlistclick()
    {
        Popup_Manager.INSTANCE.popup_index = 34;

        Popup_Manager.INSTANCE.Popup_Push();
    }



    public void freecointimedisplay(string time)
    {
        freecointime.text = time;
    }

    public void GoldSetting(int rewardmoney)
    {
        nowMoney = GameManager.Instance.m_GameData.MyGold;

        GameManager.Instance.m_GameData.MyGold += rewardmoney;

        addmoney = rewardmoney;

        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

        if (GameManager.Instance.GetGameState() == GAMESTATE.WorldMap)//신규유저가 월드맵을 가지않고 구매할수도 있다.
            WorldMapUI.instance.MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
            // buycoin.gameObject.SetActive(true);
        AddMyGold();

    }
    public void AddMyGold()
    {

        buycoin.gameObject.SetActive(false);

        orgColor = m_nowgold.color;

        m_nowgold.fontSize = 40;

        nowMoney = GameManager.Instance.m_GameData.MyGold - addmoney;

        SoundManager.Instance.PlayEffect("coin");

        movegab = 1;

        if (addmoney < 100)
        {
            movegab = 20;
        }
        else if (addmoney >= 100 && addmoney < 500)
        {
            movegab = 50;
        }
        else if (addmoney >= 500 && addmoney <= 5000)
        {
            movegab = 200;
        }
        else if (addmoney > 5000 && addmoney<50000)
        {
            movegab = 1000;
        }
        else if (addmoney >= 50000 && addmoney < 500000)
        {
            movegab = 10000;
        }
        else 
        {
            movegab = 50000;
        }

        StartCoroutine(moneyadd());

    }

    IEnumerator moneyadd()
    {
        while (addmoney > 0)
        {
            nowMoney += movegab;
            addmoney -= movegab;

            m_nowgold.text = nowMoney.ToString();

            yield return null;
        }

        yield return null;
        m_nowgold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
        m_nowgold.fontSize = 30;
        m_nowgold.color = orgColor;

       
    }
    public void ShopPopSetting()
    {
        //세일 시간막기.
        //if (TimeManager.Instance.supersaletimechk)
        //{
        //    saletime.gameObject.SetActive(true);
        //}
        //else
        //{
        //    saletime.gameObject.SetActive(false);
        //}

        GridSetting();

    }

    void GridSetting()
    {
        for (int i = 0; i < TableDataManager.Instance.m_ShopList.Count - 3; i++)
        {
            Transform gridchild = m_Grid.GetChild(i);

            gridchild.GetComponent<ShopItem>().Init(i);

            if (TimeManager.Instance.supersaletimechk)
            {
                gridchild.GetComponent<ShopItem>().SaleGoldSetting(TableDataManager.Instance.m_ShopList[i].gold, TableDataManager.Instance.m_ShopList[i].salegold);
            }
            else
            {
                gridchild.GetComponent<ShopItem>().GoldSetting(TableDataManager.Instance.m_ShopList[i].gold);
            }

            
            if (i == 1)
            {
                gridchild.GetComponent<ShopItem>().displaymostpopular();
            }
            if (i == 3)
            {
                gridchild.GetComponent<ShopItem>().displaybestvalue();
            }

            gridchild.GetComponent<ShopItem>().PaySetting(TableDataManager.Instance.m_ShopList[i].price);

            #if !UNITY_EDITOR
            if (i != 0)
            {
                //금액을 받아온다.
                string price = InAppPurchaser.Instance.LocalePrice((ProductKind)(Enum.GetValues(typeof(ProductKind))).GetValue(i - 1));

                if (price.Equals("null") == false)
                    gridchild.GetComponent<ShopItem>().PaySetting(price);
            }
#endif

        }
        // Transform gridchild = m_Grid.GetChild(0);
        Transform adfree = m_Grid.GetChild(7);

        adfree.GetComponent<Restore>().Init(7);
        adfree.GetComponent<Restore>().PaySetting(TableDataManager.Instance.m_ShopList[6].price);

#if !UNITY_EDITOR
        //Ads_Free 금액을 받아온다.
        string price_free = InAppPurchaser.Instance.LocalePrice(ProductKind.free_ads);

		if(price_free.Equals("null") == false)
			adfree.GetComponent<Restore>().PaySetting(price_free);
#endif

        if (GameManager.Instance.m_GameData.AdsFreeChk)
        {
            Transform adfree1 = m_Grid.GetChild(7);
            adfree1.GetComponent<Restore>().m_restorebg.gameObject.SetActive(true);
        }
    }

    public void RestoreSetting()
    {
        if (GameManager.Instance.m_GameData.AdsFreeChk)
        {
            Transform adfree = m_Grid.GetChild(7);
            adfree.GetComponent<Restore>().m_restorebg.gameObject.SetActive(true);
        }

        //SoundManager.Instance.PlayEffect("button_click");
       
    }

    public void RestoreAction()
    {
#if UNITY_IOS
        InAppPurchaser.Instance.RestorePurchases();
#endif
    }

    public void saletimedisplay(string time)
    {
        saletime.text = time;
    }


    public override void BackButtonClick()
    {
        ExitClick();
    }

    public void ExitClick()
    {
        if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
        {
            GameManager.Instance.resumechk = true;

            GetComponent<Parent_Pop>().ComeBackGame();

        }
        else
        {
            GetComponent<Parent_Pop>().MoveToBottom();
        }
    }
}
