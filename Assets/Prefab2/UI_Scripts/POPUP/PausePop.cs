﻿using UnityEngine;
using System.Collections;

public class PausePop : BackClickPop
{

    public UILabel title;

    public UILabel resume;

    public UILabel gameinfo;

    public UILabel quit;

    public UISprite effect_off;

    public UISprite effect_on;

    public UISprite bgsound_off;

    public UISprite bgsound_on;

    public void Init()
    {
        title.text = TableDataManager.Instance.GetLocalizeText(54);
        resume.text = TableDataManager.Instance.GetLocalizeText(55);
        gameinfo.text = TableDataManager.Instance.GetLocalizeText(204);
        quit.text = TableDataManager.Instance.GetLocalizeText(37);
        
        optionchk();
    }

    void OnEnable()
    {
        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            AdMob_Manager.Instance.AdMob_Banner_Hide();
            AdMob_Manager.Instance.AdMob_Medium_Show();
        }
    }

    void OnDisable()
    {
        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            AdMob_Manager.Instance.AdMob_Medium_Hide();
            AdMob_Manager.Instance.AdMob_Banner_Show();
        }
    }

    public void optionchk()
    {
        bool effect_snd = SecurityPlayerPrefs.GetInt("isEffect", 1) == 1 ? true : false;
        bool bg_snd = SecurityPlayerPrefs.GetInt("isBGM", 1) == 1 ? true : false;

        if (effect_snd == false)
        {
            // effect_on.gameObject.SetActive(false);
            effect_off.gameObject.SetActive(true);
        }
        else
        {
            effect_off.gameObject.SetActive(false);
        }

        if (bg_snd == false)
        {
            // bgsound_on.gameObject.SetActive(false);
            bgsound_off.gameObject.SetActive(true);
        }
        else
        {
            bgsound_off.gameObject.SetActive(false);
        }

       
    }

    public void EffectSoundSetting()
    {


        if (GameManager.Instance.m_GameData.Effect_Snd == false)
        {
            SoundManager.Instance.isEffect = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SecurityPlayerPrefs.SetInt("isEffect", 1);
            effect_off.gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Effect_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Effect_Snd == true)
        {
            SoundManager.Instance.isEffect = false;
            SecurityPlayerPrefs.SetInt("isEffect", 0);
            effect_off.gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Effect_Snd = false;
        }

    }

    public void BGSoundSetting()
    {


        if (GameManager.Instance.m_GameData.Bg_Snd == false)
        {
            SoundManager.Instance.isBGM = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                SoundManager.Instance.PlayBGM("play_bgm1");
            else
                SoundManager.Instance.PlayBGM("play_bgm2");
            SecurityPlayerPrefs.SetInt("isBGM", 1);
            bgsound_off.gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Bg_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Bg_Snd == true)
        {
            SoundManager.Instance.isBGM = false;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SoundManager.Instance.StopBGM();
            SecurityPlayerPrefs.SetInt("isBGM", 0);
            bgsound_off.gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Bg_Snd = false;
        }
    }


    public void moveToGameinfo()
    {

        GameManager.Instance.m_replaychk = true;

        GetComponent<Parent_Pop>().ComeBackGame();

        SoundManager.Instance.PlayEffect("play_click");
        // Popup_Manager.INSTANCE.popup_index = 5;
        // Popup_Manager.INSTANCE.Popup_Push();

        if (MatchManager.Instance.m_CSD.limit_Move <= 5)
            MatchManager.Instance.StageFailCount(true);

        MatchManager.Instance.Analytics_GameEnd(false);

        if (!GameManager.Instance.m_GameData.AdsFreeChk)
            AdMob_Manager.Instance.AdMob_FullAd_Show();
    }

    public void QuitClick()
    {
        if (Popup_Manager.INSTANCE.addisplaychk())
        {
            
            Popup_Manager.INSTANCE.adddisplaysetting();
            GetComponent<Parent_Pop>().MoveToBottom();
            //Popup_Manager.INSTANCE.Popup_Pop();
        }
        else
        {
            GetComponent<Parent_Pop>().MoveToBottom();
        }

        MatchManager.Instance.Analytics_GameEnd(false);
    }

    public void resumeClick()
    {

        GetComponent<Parent_Pop>().ComeBackGame();
        GameManager.Instance.resumechk = true;


    }

    public override void BackButtonClick()
    {
        resumeClick();
    }
   
}
