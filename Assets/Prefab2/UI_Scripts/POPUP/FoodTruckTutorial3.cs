﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FoodTruckTutorial3 : BackClickPop {

    public GameObject TalkBox;

    public UISprite donebutton;

    public void Init()
    {
        //사운드
        SoundManager.Instance.PlayEffect("popup");

        //TextBox 세팅
        TalkBox.transform.localPosition = new Vector3(-700, -332, 0);
        TalkBox.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.OutSine);

        donebutton.gameObject.SetActive(true);

        UILabel label = TalkBox.GetComponentInChildren<UILabel>();
        label.text = TableDataManager.Instance.GetLocalizeText(149);

        FirebaseManager.Instance.LogScreen("food tutorial3");
    }


    public void DonebtnClick()
    {
       // SoundManager.Instance.PlayEffect("button_chk");

        StartCoroutine(tutorialend());

        
    }

    IEnumerator tutorialend()
    {

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);

        donebutton.gameObject.SetActive(false);

        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        yield return new WaitForSeconds(0.2f);
        //푸드트럭 보상 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();
    }

    public override void BackButtonClick()
    {
        DonebtnClick();
    }
        
}
