﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Specialitem : MonoBehaviour {

    public List<UILabel> itemcntlist= new List<UILabel>();

    public UISprite closebg;

    public UISprite specialkind;

    public UILabel oktxt;

    public UILabel price;

    public UILabel closebutton;

    public int itemindex;

    public UISprite goldcoin;

    public void Init(int index)
    {


        itemindex = index;
        //아이템 갯수 세팅
        for (int i = 0; i < itemcntlist.Count - 1; i++)
        {

            itemcntlist[i].text = TableDataManager.Instance.m_packageitem[index].itemcnt.ToString();
        }
        //골드량 세팅
        itemcntlist[itemcntlist.Count - 1].text = TableDataManager.Instance.m_packageitem[index].glodcnt.ToString();

        oktxt.text = TableDataManager.Instance.GetLocalizeText(201);

        closebutton.text = TableDataManager.Instance.GetLocalizeText(222);

        price.text = "$"+TableDataManager.Instance.m_packageitem[index].price;
#if !UNITY_EDITOR
        //금액을 받아온다.
        string locale_price = InAppPurchaser.Instance.LocalePrice((ProductKind)((int)ProductKind.package_1 + index));

        if (locale_price.Equals("null") == false)
            price.text = locale_price;
#endif

        specialkind.spriteName = "step" + (index + 1).ToString();

        goldcoin.spriteName = "gold_0" + (3 + index);

        if ((GameManager.Instance.m_GameData.Packageitem1buychk && itemindex == 0) || (GameManager.Instance.m_GameData.Packageitem2buychk && itemindex == 1)
            || (GameManager.Instance.m_GameData.Packageitem3buychk && itemindex == 2) || (GameManager.Instance.m_GameData.Packageitem4buychk && itemindex == 3))
        {
            closebg.gameObject.SetActive(true);

            closebutton.gameObject.SetActive(true);

            oktxt.gameObject.SetActive(false);

            price.gameObject.SetActive(false);
        }
        else
        {
            closebg.gameObject.SetActive(false);

            closebutton.gameObject.SetActive(false);

            oktxt.gameObject.SetActive(true);

            price.gameObject.SetActive(true);
        }
    }


    public void SpecialbuyClick()
    {

        SoundManager.Instance.PlayEffect("button_click");
        

        switch (itemindex)
        {
            case 0:
                InAppPurchaser.Instance.BuyProduct(ProductKind.package_1);
                break;
            case 1:
                InAppPurchaser.Instance.BuyProduct(ProductKind.package_2);
                break;
            case 2:
                InAppPurchaser.Instance.BuyProduct(ProductKind.package_3);
                break;
            case 3:
                InAppPurchaser.Instance.BuyProduct(ProductKind.package_4);
                break;
           
        }

    }
    

}
