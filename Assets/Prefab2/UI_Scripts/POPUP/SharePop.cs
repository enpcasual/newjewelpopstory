﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SharePop : BackClickPop
{

    public UILabel notice_txt;

    public UILabel title_txt;

  

    public void Init()
    {
        title_txt.text = TableDataManager.Instance.GetLocalizeText(190);
        notice_txt.text = TableDataManager.Instance.GetLocalizeText(223);
    }

    
    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public override void BackButtonClick()
    {
        ExitClick();
    }

    public void ButtonClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();

        WorldMapUI.instance.Share_icon.gameObject.SetActive(false);

        WorldMapUI.instance.BottomUIGrid.Reposition();

        GameManager.Instance.m_GameData.sharechk = true;

        SecurityPlayerPrefs.SetInt("sharechk", 1);


        FaceBook_Manager.Instance.ShareLink();
    }
}
