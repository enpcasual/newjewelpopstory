﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//public class MisssionInfo
//{
//    public int missionkind;
//    public string itemstr;
//    public int itemcnt;
//}

public class MissionPop : BackClickPop {

    public UILabel title;

    public UILabel missiontxt;

    //public List<MisssionInfo> missioninfo = new List<MisssionInfo>();

    public List<MissionMenu> missionmenu = new List<MissionMenu>();

    public GameObject MovePulsAdObj = null;

    public void Init()
    {
        title.text = TableDataManager.Instance.GetLocalizeText(42);
        missiontxt.text = TableDataManager.Instance.GetLocalizeText(43);

        MovePulsAdObj.SetActive(AdManager.Instance.IsAdReady && StageItemManager.Instance.TotalnowStage > 20);
    }

    public void MissioninfoSetting(Stage stage)
    {
        for (int i = 0; i < missionmenu.Count; i++)
        {
            missionmenu[i].gameObject.SetActive(false);
        }

        //미션 세팅
        int index = stage.missionInfo.Count;
        if(index == 0)
        {
            Debug.LogError("미션이 세팅되어 있지 않습니다.!");
            return;
        }

        if (index > 4)
            index = 4;

        missionmenu[index -1].gameObject.SetActive(true);
        missionmenu[index - 1].MissionListSetting(stage.missionInfo);

    }

    public override void BackButtonClick()
    {
        Click_MissionPop();
    }

    public void Click_MissionPop()
    {
        GetComponent<Banner_Pop>().MoveToBottom();
    }

    public void Mission_Pop()
    {
        GetComponent<Banner_Pop>().Popup_pop();

        if(MatchManager.Instance.TutorialCheck())
        {
            MatchManager.Instance.SetMatchState(MatchState.Playing);
        }
        else
        {
            //Food Car Ablility
            MatchManager.Instance.SetMatchState(MatchState.Playing);
            MatchManager.Instance.StartFoodCarAbility();
        }

        if (m_MovePlus)
        {
            m_MovePlus = false;
            Invoke("AddMove", 0.1f);
            Invoke("AddMove", 0.4f);
            Invoke("AddMove", 0.7f);
        }
    }

    /// <summary>
    /// 귀찮았어요. 죄송해요 ...
    /// </summary>
    private void AddMove()
    {
        MissionManager.Instance.MoveAddCnt(MissionManager.MoveAddType.Post, 1);
    }

    private bool m_MovePlus = false;

    public void ADtoMovePlus()
    {
        if (AdManager.Instance.IsAdReady)
        {
            AdManager.AdsRewardResult = () =>
            {
                m_MovePlus = true;
                MovePulsAdObj.SetActive(false);
            };

            AdManager.Instance.Ad_Show("PostMovePlus");
        }
    }
}