﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameinfoPop : BackClickPop
{

    public List<GameObject> infopage = new List<GameObject>();
    public List<int> infoTextIndexs = new List<int>();

    public int nowpage;

    public Child_Pop m_childPop;

    public UILabel nowpagelabel;

    public UILabel title;

    public UILabel nextlabel;

    public void Init()
    {

        

        for (int i = 0; i < infopage.Count; i++)
        {
            infopage[i].SetActive(false);
        }
        
        nowpage = 0;

        infopage[nowpage].SetActive(true);

        nowpageSetting();

        //FontManager.instance.FontSetting(title);
       // FontManager.instance.FontSetting(nextlabel);

        title.text = TableDataManager.Instance.GetLocalizeText(21);
        nextlabel.text = TableDataManager.Instance.GetLocalizeText(23);

        for (int i = 0; i < infopage.Count; i++)
        {
           // FontManager.instance.FontSetting(infopage[i].GetComponentInChildren<UILabel>());
            infopage[i].GetComponentInChildren<UILabel>().text = TableDataManager.Instance.GetLocalizeText(infoTextIndexs[i]);
        }
    }

    public void NextPage()
    {



        if ((nowpage + 1) >= infopage.Count)
        {
            m_childPop.MoveToTop();
        }
        else
        {
            for (int i = 0; i < infopage.Count; i++)
            {
                infopage[i].SetActive(false);
            }

            nowpage++;

            infopage[nowpage].SetActive(true);

          

            nowpageSetting();

            SoundManager.Instance.PlayEffect("button_click");

        }


    }

    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();

    }
    void nowpageSetting()
    {
        nowpagelabel.text = (nowpage + 1).ToString() + "/" + infopage.Count;
    }
}
