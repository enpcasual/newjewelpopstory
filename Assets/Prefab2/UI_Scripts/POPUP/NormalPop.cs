﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalPop : BackClickPop
{
    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public override void BackButtonClick()
    {
        ExitClick();
    }
}
