﻿using UnityEngine;
using System.Collections;

public class FoodTruckSalePop : BackClickPop {

    public CarPortTruck saletruck;

    public UILabel titletxt;

    public UILabel buytxt;

    public UILabel salegold;

    int truckgrade;

     public void Init()
    {

        titletxt.text = TableDataManager.Instance.GetLocalizeText(126);

        buytxt.text = TableDataManager.Instance.GetLocalizeText(127);
#if !UNITY_EDITOR
        //금액을 받아온다.
        string locale_price = InAppPurchaser.Instance.LocalePrice(ProductKind.gold_21);

        if (locale_price.Equals("null") == false)
            buytxt.text = locale_price;
#endif

        truckgrade = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade-1;

   

        if (truckgrade < 0)
        {
            truckgrade = 0;
        }

        FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[truckgrade];

        saletruck.SetInfo(tempitem);

        saletruck.gradetxt.text = (truckgrade+1).ToString() +" Grade";

        salegold.text = TableDataManager.Instance.m_buyprice[11].buyprice.ToString();
    }

    public void SaleResult()
    {
        CarPortManager.Instance.AddFoodTruck(truckgrade);

        TimeManager.Instance.trucksaletimechk = false;

        TimeManager.Instance.m_trucksaleremaintime = 0;

        SecurityPlayerPrefs.SetLong("TruckSaleTime", 0);

        CarPortManager.Instance.SuperSaleIcon.gameObject.SetActive(false);

        
        TimeManager.Instance.FoodTruckSaleSetting();

        GetComponent<Child_Pop>().MoveToTop();

        WorldMapUI.instance.GoldSetting(TableDataManager.Instance.m_buyprice[11].buyprice);

    }



    public void BuyClick()
    {

        //inapp
        SoundManager.Instance.PlayEffect("button_click");

        InAppPurchaser.Instance.BuyProduct(ProductKind.gold_21);

       

        

    }

    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
