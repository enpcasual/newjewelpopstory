﻿using UnityEngine;
using System.Collections;

public class GateWayPop : BackClickPop {


    public UILabel title;

    public UILabel info1;

    public UILabel info2;

    public UILabel btn;

    public UILabel NeedStar_label;

    public int unlockgold;

    public UILabel mygold;

    public UILabel needgold;

    public void Init()
    {
       

        title.text = TableDataManager.Instance.GetLocalizeText(11);
        info1.text = TableDataManager.Instance.GetLocalizeText(12);
        info2.text = TableDataManager.Instance.GetLocalizeText(13);
        btn.text = TableDataManager.Instance.GetLocalizeText(14);

        unlockgold = TableDataManager.Instance.m_buyprice[13].buyprice;

        needgold.text = unlockgold.ToString();

        MyGoldSetting();

        NeedStar();

       
    }
    public void NeedStar()
    {
        //NeedStar_label.text = (StageItemManager.Instance.NeedStar - GameManager.Instance.m_GameData.MyStar).ToString();
    }


    public void MyGoldSetting()
    {
        mygold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
    }

    public void UnlockClick()
    {
        if (GameManager.Instance.m_GameData.MyGold >= unlockgold)
        {

            SoundManager.Instance.PlayEffect("buy");

            GameManager.Instance.m_GameData.MyGold -= unlockgold;

            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            WorldMapUI.instance.GoldRefresh();
            /*

            GameManager.Instance.m_MyStar = StageItemManager.Instance.NeedStar;

            SecurityPlayerPrefs.SetInt("MyStar", GameManager.Instance.m_MyStar);

            WorldMapUI.instance.StarReFresh();
            */

            GameManager.Instance.unclokgoldchk = true;

            GetComponent<Child_Pop>().MoveToTop();
        }
        else
        {
            //Popup_Manager.INSTANCE.Popup_Pop();

            Popup_Manager.INSTANCE.popup_index = 2;

            Popup_Manager.INSTANCE.Popup_Push();
        }
    }


    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }

}
