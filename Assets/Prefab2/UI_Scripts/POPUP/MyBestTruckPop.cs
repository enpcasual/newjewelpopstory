﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyBestTruckPop : BackClickPop {


    public List<ParticleSystem> effectstar = new List<ParticleSystem>();

    int effect_index;

    public UISprite mybesttruck;

    int truckGrade;

    public CarPortTruck MyTruck;

    public List<string> infotxtlist;

    //public List<GameObject> infolist = new List<GameObject>();
    public GameObject info;

    public UILabel titletxt;

    //public UISprite truckinfobg;

    public GameObject okbtn;

   
    public void Init()
    {

        for (int i = infotxtlist.Count - 1; i >= 0; i--)
        {
            infotxtlist.RemoveAt(i);
        }


        //for (int i = 0; i < infolist.Count; i++)
        //{
        //    infolist[i].gameObject.SetActive(false);

        //}

        okbtn.SetActive(false);

        //truckinfobg.gameObject.SetActive(false);

        effect_index = 0;
        //for (int i = 0; i < effectstar.Count; i++)
        //{
        //    effectstar[i].gameObject.SetActive(false);
        //    effectstar[i].Stop();
        //}

        truckGrade = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade;

        FoodTruckinfo temptruck = TableDataManager.Instance.m_Foodtruck[truckGrade];

        MyTruck.SetInfo(temptruck);

        effect_index = 0;

        mybesttruck.gameObject.SetActive(true);


        if (CarPortManager.Instance.m_mybesttruckclickchk)
        {
            mybesttruckinfosetting();
        }
        else
        {
            StartCoroutine(EffectStart());
        }

        CarPortManager.Instance.m_mybesttruckclickchk = false;

        titletxt.text = (truckGrade + 1).ToString() + ". " + TableDataManager.Instance.GetLocalizeText(58 + truckGrade);
        

        
    }

    IEnumerator EffectStart()
    {
        //truckinfobg.gameObject.SetActive(true);
        TruckinfoSetting();

        yield return new WaitForSeconds(0.5f);

        //while (effect_index < effectstar.Count)
        //{

        //    effectstar[effect_index].gameObject.SetActive(true);

        //    effectstar[effect_index].Play();
        //    effect_index++;
        //    yield return new WaitForSeconds(0.1f);
        //}


        yield return new WaitForSeconds(1f);


        if (!GameManager.Instance.m_GameData.TruckTutorialChk)
        {
            Popup_Manager.INSTANCE.popup_index = 30;

            Popup_Manager.INSTANCE.Popup_Push();

            yield return new WaitForSeconds(0.2f);

            okbtn.SetActive(true);
        }
        else
        {
            okbtn.SetActive(true);
        }


        
    }

    public void mybesttruckinfosetting()
    {
        //truckinfobg.gameObject.SetActive(true);

        TruckinfoSetting();


       

        if (!GameManager.Instance.m_GameData.TruckTutorialChk)
        {
            Popup_Manager.INSTANCE.popup_index = 30;

            Popup_Manager.INSTANCE.Popup_Push();

           

            okbtn.SetActive(true);
        }
        else
        {
            okbtn.SetActive(true);
        }
    }

    public void TruckinfoSetting()
    {
        string tempstring = TableDataManager.Instance.GetLocalizeText(120) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].moveplus.ToString();
        infotxtlist.Add(tempstring);


        tempstring = TableDataManager.Instance.GetLocalizeText(121) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].butterflyplus.ToString();
        infotxtlist.Add(tempstring);


        tempstring = TableDataManager.Instance.GetLocalizeText(122) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].goldplus.ToString();
        infotxtlist.Add(tempstring);

        tempstring = TableDataManager.Instance.GetLocalizeText(123) + ":" + TableDataManager.Instance.m_Foodtruck[truckGrade].bonusplus.ToString();
        infotxtlist.Add(tempstring);



        for (int i = 0; i < infotxtlist.Count; i++)
        {
            info.GetComponent<truckinfotxt>().infotxt[i].text = infotxtlist[i];
        }

        //if (abilitycnt >= 0)
        //{
        //    infolist[abilitycnt].SetActive(true);

        //    for (int i = 0; i < infotxtlist.Count; i++)
        //    {
        //        infolist[abilitycnt].GetComponent<truckinfotxt>().infotxt[i].text = infotxtlist[i];
        //    }


        //}
    }


    public void Okclick()
    {
        Popup_Manager.INSTANCE.Popup_Pop();

        CarPortManager.Instance.m_myBestFoodTruck.gameObject.SetActive(true);

        CarPortManager.Instance.m_myBestFoodTruck.GetComponent<Animator>().SetBool("TRUCK_CHANGE", true);

        CarPortManager.Instance.MyfoodTruckNameChange();

        if (!GameManager.Instance.m_GameData.TruckTutorialChk)
        {
            Popup_Manager.INSTANCE.popup_index = 31;

            Popup_Manager.INSTANCE.Popup_Push();
        }


    }

    public override void BackButtonClick()
    {
        Okclick();
    }
}
