﻿using UnityEngine;
using System.Collections;

public class InappPop : MonoBehaviour {

    public UILabel titletxt;

    public UILabel resulttxt;

    public void Init()
    {
        titletxt.text = TableDataManager.Instance.GetLocalizeText(130);

        resulttxt.text = TableDataManager.Instance.GetLocalizeText(131);
    }
}
