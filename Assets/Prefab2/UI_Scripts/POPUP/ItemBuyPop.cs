﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class ItemBuyPop : BackClickPop {

    public int itemkind;

    public UILabel titletxt;

    public UILabel itemname;

    //public UISprite itemspr;

    public UILabel iteminfo;

    public UILabel buylabel;

    public int itemprice;

    public UILabel itempricelabel;

    public Child_Pop childpop;

    public List<GameObject> itemeffectlist = new List<GameObject>();

    public void Init()
    {

        for (int i = 0; i < itemeffectlist.Count; i++)
        {
            itemeffectlist[i].SetActive(false);
        }

        itemeffectlist[itemkind].SetActive(true);

        //itemspr.spriteName = "item_0"+(itemkind+1);

        titletxt.text = TableDataManager.Instance.GetLocalizeText(185);
        buylabel.text = TableDataManager.Instance.GetLocalizeText(186);
        itemname.text = TableDataManager.Instance.GetLocalizeText(48+itemkind);
        iteminfo.text = TableDataManager.Instance.GetLocalizeText(51+itemkind);

        itemprice = TableDataManager.Instance.m_buyprice[4 + itemkind].buyprice;

        itempricelabel.text = itemprice.ToString();
    }
    public void itembuy()
    {
        int tmepgold = GameManager.Instance.m_GameData.MyGold - itemprice;

        

        if (tmepgold >= 0)
        {
            SoundManager.Instance.PlayEffect("buy");

            GameManager.Instance.m_GameData.MyGold = tmepgold;

            GameManager.Instance.m_GameData.MyItemCnt[itemkind] += 4;

            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            SecurityPlayerPrefs.SetInt("MyItem" + itemkind, GameManager.Instance.m_GameData.MyItemCnt[itemkind]);

            FirebaseManager.Instance.LogEvent_UseGold(MatchManager.Instance.m_StageIndex, itemname.text, itemprice);

            //이 팝업이 애초에 인게임중에만 나오는데..
            //WorldMapUI.instance.MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");//신규유저가 월드맵을 가지않고 구매할수도 있다.

            childpop.MoveToTop();
        }
        else if (tmepgold < 0)
        {
            Popup_Manager.INSTANCE.popup_index = 2;

            Popup_Manager.INSTANCE.Popup_Push();
        }

        //UI갱신
        UI_ThreeMatch.Instance.CashItemUpdate(itemkind);
    }


    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
