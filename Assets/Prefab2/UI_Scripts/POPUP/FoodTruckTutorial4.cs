﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FoodTruckTutorial4 : BackClickPop
{

    public UISprite Finger;

    Sequence Finger_Seq;


    public void Init()
    {
        FirebaseManager.Instance.LogScreen("food tutorial4");

        FingerMove();
    }

    public void FingerMove()
    {
        Finger.gameObject.SetActive(true);
        Finger.transform.localPosition = new Vector3(-160,487,0);
        //Finger.transform.DOLocalMove(List_Touch[1], 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);

        Finger_Seq = DOTween.Sequence();
        Finger_Seq.AppendCallback(() =>
        {
            Finger.transform.localPosition = new Vector3(-160, 487, 0);
            Finger.color = Color.white;
        });
        Finger_Seq.Append(Finger.transform.DOLocalMove(new Vector3(-180, 487, 0), 1f).SetEase(Ease.Linear));
        Finger_Seq.Insert(0.2f, DOTween.ToAlpha(() => Finger.color, _color => Finger.color = _color, 0f, 0.2F));
        Finger_Seq.SetLoops(-1, LoopType.Restart);
    }


    public void trucktutorialend()
    {
        Finger_Seq.Kill();

        Finger.gameObject.SetActive(false);


        Popup_Manager.INSTANCE.Popup_Pop();

        GameManager.Instance.m_GameData.TruckTutorialChk = true;

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);

        UI_CarPort.Instance.MoveToWorldMap();
       

    }

   

    public override void BackButtonClick()
    {
        trucktutorialend();

    }
	
}
