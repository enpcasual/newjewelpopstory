﻿using UnityEngine;
using System.Collections;

public class GameQuitPop : BackClickPop {

    public UILabel title;

    public UILabel info;

    public UILabel quit;

    public UILabel keepgame;

    public void Init()
    {
        

        title.text = TableDataManager.Instance.GetLocalizeText(35);
        info.text = TableDataManager.Instance.GetLocalizeText(36);
        quit.text = TableDataManager.Instance.GetLocalizeText(37);
        keepgame.text = TableDataManager.Instance.GetLocalizeText(38);

        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            AdMob_Manager.Instance.AdMob_Banner_Hide();
            AdMob_Manager.Instance.AdMob_Medium_Show();
        }
    }

    public void GameQuit()
    {
        //IGAWorks_Manager.instance.OnApplicationQuit();
        SecurityPlayerPrefs.Save();
        Application.Quit();
        //System.Diagnostics.Process.GetCurrentProcess().Kill();
        //Native_Brige.Application_Quit();
    }

    public void KeepPlay()
    {
        GetComponent<Parent_Pop>().MoveToBottom();

        if (!GameManager.Instance.m_GameData.AdsFreeChk)
        {
            AdMob_Manager.Instance.AdMob_Medium_Hide();
            if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
            {
                AdMob_Manager.Instance.AdMob_Banner_Show();
            }
        }
    }

    public override void BackButtonClick()
    {
        KeepPlay();
    }

    
}
