﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TruckInfoPop : BackClickPop {

    public List<GameObject> infolist = new List<GameObject>();

    public UILabel titletxt;

    public UILabel sharetxt;


    public UILabel oktxt;

    public CarPortTruck MyTruck;

    public List<string> infotxtlist;

    public int truckGrade;


    public GameObject okbtn;

    public UILabel firsttrucklabel;

    public UISprite truckbg;

    public void Init()
    {
     

        MyTruck.ani.SetBool("STOP_CHK", true);

        for (int i = infotxtlist.Count-1; i >= 0; i--)
        {
            infotxtlist.RemoveAt(i);
        }
       

        for (int i = 0; i < infolist.Count; i++)
        {
            infolist[i].gameObject.SetActive(false);

        }
       

        TruckBGSetting();


        firsttrucklabel.gameObject.SetActive(false);
        

       // garagetxt.text = TableDataManager.Instance.GetLocalizeText(124);

     
          sharetxt.text = TableDataManager.Instance.GetLocalizeText(125);

          if (CarPortManager.Instance.m_mybesttruckclickchk)
          {
              truckGrade = GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade;
          }
          else
          {
              truckGrade = GameManager.Instance.m_FoodTruckAlbumIndex;
          }

          CarPortManager.Instance.m_mybesttruckclickchk = false;

            sharetxt.gameObject.SetActive(true);

          titletxt.text = (truckGrade + 1).ToString() + ". " + TableDataManager.Instance.GetLocalizeText(58 + truckGrade);


        oktxt.text = TableDataManager.Instance.GetLocalizeText(10);

        FoodTruckinfo temptruck = TableDataManager.Instance.m_Foodtruck[truckGrade];

        MyTruck.SetInfo(temptruck);

         TruckinfoSetting();



    }

   
    public void TruckinfoSetting()
    {
        int abilitycnt = -1;

        if (TableDataManager.Instance.m_Foodtruck[truckGrade].moveplus > 0)
        {
            string tempstring = TableDataManager.Instance.GetLocalizeText(120) + TableDataManager.Instance.m_Foodtruck[truckGrade].moveplus.ToString();

            infotxtlist.Add(tempstring);

            abilitycnt++;

        } if (TableDataManager.Instance.m_Foodtruck[truckGrade].scoreplus > 0)
        {
            string tempstring = TableDataManager.Instance.GetLocalizeText(121) + TableDataManager.Instance.m_Foodtruck[truckGrade].butterflyplus.ToString();

            infotxtlist.Add(tempstring);

            abilitycnt++;

        }
        if (TableDataManager.Instance.m_Foodtruck[truckGrade].goldplus > 0)
        {
            string tempstring = TableDataManager.Instance.GetLocalizeText(122) + TableDataManager.Instance.m_Foodtruck[truckGrade].goldplus.ToString();

            infotxtlist.Add(tempstring);

            abilitycnt++;

        }
        if (TableDataManager.Instance.m_Foodtruck[truckGrade].bonusplus > 0)
        {
            string tempstring = TableDataManager.Instance.GetLocalizeText(123) + TableDataManager.Instance.m_Foodtruck[truckGrade].bonusplus.ToString();

            infotxtlist.Add(tempstring);

            abilitycnt++;

        }

        if (abilitycnt >= 0)
        {
            infolist[abilitycnt].SetActive(true);

            for (int i = 0; i < infotxtlist.Count; i++)
            {
                infolist[abilitycnt].GetComponent<truckinfotxt>().infotxt[i].text = infotxtlist[i];
            }
            

        }

        if (infotxtlist.Count == 0)
        {
            firsttrucklabel.gameObject.SetActive(true);

            firsttrucklabel.text = TableDataManager.Instance.GetLocalizeText(180);
        }


        
        

    }


    public void TruckBGSetting()
    {
        truckbg.spriteName = "Theme" + (StageItemManager.Instance.CurrentPart + 1).ToString();
    }
    public void buttonclick()
    {
       
            OnShareButton();
       

       

    }
   //Texture2 _myScreenShot;
 
    public void OnShareButton ()
    {
        // OS별 일반적 공유
        GeneralShare generalShare = new GeneralShare ();        
        generalShare.shareText ("MyGameApp", "Message");
 
        // 이미지 공유시
        //generalShare.shareImageWithText ("MyGameApp", "Message", _myScreenShot);
    }

    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
