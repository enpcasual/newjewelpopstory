﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FoodTruckTutorial1 : BackClickPop {

    public GameObject gotocarport;

    public GameObject TalkBox;

    public UISprite Finger;

    Sequence Finger_Seq;

    public GameObject skipbutton;

    public void Init()
    {
        //사운드
        SoundManager.Instance.PlayEffect("popup");

        //TextBox 세팅
        TalkBox.transform.localPosition = new Vector3(-700, -123, 0);
        TalkBox.transform.DOLocalMoveX(0, 0.15f).SetEase(Ease.OutSine);

        UILabel label = TalkBox.GetComponentInChildren<UILabel>();
        label.text = TableDataManager.Instance.GetLocalizeText(147);

        skipbutton.SetActive(true);


        FingerMove();

        FirebaseManager.Instance.LogScreen("food tutorial1");
    }
    public void FingerMove()
    {
        Finger.gameObject.SetActive(true);
        Finger.transform.localPosition = new Vector3(-23,-317,0);
        //Finger.transform.DOLocalMove(List_Touch[1], 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);

        Finger_Seq = DOTween.Sequence();
        Finger_Seq.AppendCallback(() =>
        {
            Finger.transform.localPosition = new Vector3(-23, -317, 0);
            Finger.color = Color.white;
        });
        Finger_Seq.Append(Finger.transform.DOLocalMove(new Vector3(-52, -317, 0), 0.3f).SetEase(Ease.Linear));
        Finger_Seq.Insert(0.3f, DOTween.ToAlpha(() => Finger.color, _color => Finger.color = _color, 0f, 0.2F));
        Finger_Seq.SetLoops(-1, LoopType.Restart);
    }
    public void TruckIconClick()
    {

        StartCoroutine(tutoriaend());
       

        
    }

    IEnumerator tutoriaend()
    {
        GameManager.Instance.gameresultchk = true;

        skipbutton.SetActive(false);

        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        Finger_Seq.Kill();
        Finger.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.2f);
        //푸드트럭 보상 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();

        //튜토리얼 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();

        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        CarPortManager.Instance.tutorialturcksetting();

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);
       
    }


    public void SkipClick()
    {
        GameManager.Instance.gameresultchk = false;

        GameManager.Instance.m_GameData.TruckTutorialChk = true;

        skipbutton.SetActive(false);

        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        Finger_Seq.Kill();
        Finger.gameObject.SetActive(false);

        
        //튜토리얼 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);
    }
    public override void BackButtonClick()
    {
        GameManager.Instance.gameresultchk = false;

        GameManager.Instance.m_GameData.TruckTutorialChk = true;

        skipbutton.SetActive(false);

        TalkBox.transform.DOLocalMoveY(-800, 0.1f).SetEase(Ease.Linear);

        Finger_Seq.Kill();
        Finger.gameObject.SetActive(false);

         //푸드트럭 보상 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();

        //튜토리얼 팝업 제거
        Popup_Manager.INSTANCE.Popup_Pop();

        SecurityPlayerPrefs.SetInt("trucktutorialchk", 1);


    }
}
