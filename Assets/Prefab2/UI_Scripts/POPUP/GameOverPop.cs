﻿using UnityEngine;
using System.Collections;

public class GameOverPop : BackClickPop {

   

    public UILabel title;

    public UILabel infotxt;

    public UILabel replay;

    //public UILabel playcnt;

    int gradecnt = 0;

    public UILabel warnninglabel;

    public GameObject gotocarport;

    public UISprite carporticon;

    public GameObject replayobj;

    public GameObject TruckShop;

    public UILabel infotext1;

    public UILabel infotext2;

    public UISprite randombox;

    int randomboxindex;

    public void Init()
    {
        title.text = TableDataManager.Instance.GetLocalizeText(39) + " " +(StageItemManager.Instance.nowplaystage+1).ToString();
        infotxt.text = TableDataManager.Instance.GetLocalizeText(40);
        replay.text = TableDataManager.Instance.GetLocalizeText(41);

        foodtruckdisplayChk();

        //replayobj.SetActive(false);

        gotocarport.gameObject.SetActive(false);

        TruckShop.gameObject.SetActive(false);

        //if (StageItemManager.Instance.TotalnowStage >= 100)
        //{

        //    TruckShop.gameObject.SetActive(true);

        //    TruckShopSetting();
        //}
        //else
        //{
        //    if (gradecnt > 0)
        //    {
        //        warnninglabel.text = TableDataManager.Instance.GetLocalizeText(140);

        //        gotocarport.gameObject.SetActive(true);
        //    }
       
        //}
        
        //Analytics
        MatchManager.Instance.Analytics_GameEnd(false);

        //시간 체크
        MatchManager.Instance.PlayTimeCheck();

        playbuttondisplay();


       
      
    }

    public void playbuttondisplay()
    {
        StartCoroutine(ButtonDiaplay());
    }


    IEnumerator ButtonDiaplay()
    {


      //  yield return new WaitForSeconds(0.5f);

  
      //  replayobj.SetActive(true);

        yield return new WaitForSeconds(0.4f);

       Popup_Manager.INSTANCE.adddisplaysetting();

    }

    public override void BackButtonClick()
    {
        GameoverExit();
    }
    public void GameoverExit()
    {
        if (!GameManager.Instance.newstageclear)
        {
            GameManager.Instance.playquitchk = true;
            
        }


        if (Popup_Manager.INSTANCE.addisplaychk())
        {


            
            Popup_Manager.INSTANCE.adddisplaysetting();
            GetComponent<Parent_Pop>().MoveToBottom();
            //Popup_Manager.INSTANCE.Popup_Pop();
        }
        else
        {
            GetComponent<Parent_Pop>().MoveToBottom();
        }

        

        
    }

    public void replayclick()
    {

        GameManager.Instance.m_replaychk = true;

        GetComponent<Parent_Pop>().ComeBackGame();

        SoundManager.Instance.PlayEffect("play_click");
        
    }

    public void carportmoveclick()
    {
       // GameManager.Instance.carportmovechk = true;

       // GetComponent<Parent_Pop>().MoveToBottom();

        return;

        Popup_Manager.INSTANCE.Popup_Pop();

        carporticon.GetComponent<Animator>().SetBool("BUTTON_PRESS", false);

        GameManager.Instance.playstatechk = true;

        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        

    }

    public void foodtruckdisplayChk()
    {

         gradecnt = 0; 

        for (int i = 0; i < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; i++)
        {
            int grade1 = GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[i].truckgrade;

            for (int j = 0; j < GameManager.Instance.m_GameData.MyFoodTruckData.gradelist.Count; j++)
            {
                if (j != i)
                {
                    if (grade1 == GameManager.Instance.m_GameData.MyFoodTruckData.gradelist[j].truckgrade)
                    {
                        gradecnt++;
                        break;
                    }
                }
            }
        }


        Debug.Log("gradecnt:::::::::::::" + gradecnt);

    }

    public void TruckShopSetting()
    {
        return;

        if (StageItemManager.Instance.TotalnowStage >= 100 && StageItemManager.Instance.TotalnowStage < 250)
        {
            randomboxindex = 0;
        }
        else if (StageItemManager.Instance.TotalnowStage >= 250 && StageItemManager.Instance.TotalnowStage < 450)
        {
            randomboxindex = 1;
        }
        else if (StageItemManager.Instance.TotalnowStage >= 450)
        {
            randomboxindex = 2;
        }

        randombox.spriteName = "randombox" + (randomboxindex + 1).ToString();

        infotext1.text = TableDataManager.Instance.GetLocalizeText(211 + randomboxindex);

        infotext2.text = TableDataManager.Instance.GetLocalizeText(214);
    }

    public void TruckShopClick()
    {
        return;

        carporticon.GetComponent<Animator>().SetBool("BUTTON_PRESS", false);

        GameManager.Instance.playstatechk = true;

        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        Popup_Manager.INSTANCE.Popup_Pop();

        Popup_Manager.INSTANCE.popup_index = 36;

        Popup_Manager.INSTANCE.Popup_Push();

    }
}
