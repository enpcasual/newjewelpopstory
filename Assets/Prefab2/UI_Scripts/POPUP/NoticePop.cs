﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoticePop : BackClickPop
{

    public UILabel notice_txt;

    public UILabel title_txt;

    public Action button_action;
    public void Init()
    {
        title_txt.text = TableDataManager.Instance.GetLocalizeText(190);
    }

    public void notice_setting(string notice, Action action = null)
    {
        notice_txt.text = notice;

        button_action = action;
    }

    public void ExitClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();
    }

    public override void BackButtonClick()
    {
        ExitClick(); 
    }

    public void ButtonClick()
    {
        GetComponent<Parent_Pop>().ComeBackGame();

        if (button_action != null)
            button_action();
    }
}
