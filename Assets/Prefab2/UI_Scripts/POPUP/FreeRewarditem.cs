﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRewarditem : MonoBehaviour {

    public UILabel listindex;

    public UISprite rewarditem;

    public UILabel rewarditemcount;

    public UILabel cooltimelabel;

    public int itemindex;

    public UISprite nowreward;

    [SerializeField]
    private int overrideHeight = 60;

    public void Init(int index)
    {

        nowreward.gameObject.SetActive(false);

        itemindex = index;

        listindex.text = (itemindex+1).ToString();

        if (itemindex == GameManager.Instance.m_GameData.FreeRewardnowindex)
        {
            nowreward.gameObject.SetActive(true);

            listindex.fontSize = 30;
        }

        rewardSetting(itemindex);

    }

    public void rewardSetting(int index)
    {
        switch (TableDataManager.Instance.m_FreeRewardlist[index].type)
        {
            case 0://골드
                {
                    rewarditem.spriteName = "gold_0" + TableDataManager.Instance.m_FreeRewardlist[index].kind;
                }
                break;
            case 1://아이템
                rewarditem.spriteName = "item_0" + TableDataManager.Instance.m_FreeRewardlist[index].kind;
                break;
        }

        rewarditem.keepAspectRatio = UIWidget.AspectRatioSource.Free;
        rewarditem.MakePixelPerfect();
        rewarditem.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnHeight;
        rewarditem.height = overrideHeight;

        rewarditemcount.text = TableDataManager.Instance.m_FreeRewardlist[index].reward.ToString();

    }


   
}
