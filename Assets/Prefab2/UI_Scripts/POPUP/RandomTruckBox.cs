﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTruckBox : MonoBehaviour {

    public int randomboxindex;

    public UILabel infotext1;

    public UILabel infotext2;

    public UILabel pricetext;

    public UILabel opentext;

    public UISprite closebg;

    int[] openlevel =  { 100, 250, 450 };

    int[] price = { 5000, 12000, 25000 };

    int[] randomgrade = { 5, 9, 13 };

    public void Init(int index)
    {
        randomboxindex = index;


        closebg.gameObject.SetActive(true);

        opentext.gameObject.SetActive(true);

        switch (randomboxindex)
        {
            case 0:
                {
                    infotext1.text = TableDataManager.Instance.GetLocalizeText(211);

                    if (StageItemManager.Instance.TotalnowStage >= 100)
                    {
                        closebg.gameObject.SetActive(false);
                        opentext.gameObject.SetActive(false);
                    }
                    else
                    {
                        opentext.gameObject.SetActive(true);
                        opentext.text = TableDataManager.Instance.GetLocalizeText(215);
                    }
                    
                }
                break;
            case 1:
                {
                    infotext1.text = TableDataManager.Instance.GetLocalizeText(212);

                    if (StageItemManager.Instance.TotalnowStage >= 250)
                    {
                        closebg.gameObject.SetActive(false);
                        opentext.gameObject.SetActive(false);
                    }
                    else
                    {
                         opentext.gameObject.SetActive(true);
                        opentext.text = TableDataManager.Instance.GetLocalizeText(216);
                    }
                    
                }
                break;
            case 2:
                {
                    infotext1.text = TableDataManager.Instance.GetLocalizeText(213);

                    if (StageItemManager.Instance.TotalnowStage >= 250)
                    {
                        closebg.gameObject.SetActive(false);
                        opentext.gameObject.SetActive(false);
                    }
                    else
                    {
                        opentext.gameObject.SetActive(true);
                        opentext.text = TableDataManager.Instance.GetLocalizeText(217);
                    }
                    
                }
                break;

        }

        pricetext.text = price[randomboxindex].ToString();

        infotext2.text = TableDataManager.Instance.GetLocalizeText(214);
    }

    public void RandomBoxClick()
    {
        GameManager.Instance.m_randombestgrade = false;

        if (GameManager.Instance.m_GameData.MyGold >= price[randomboxindex])
        {
            GameManager.Instance.m_GameData.MyGold -= price[randomboxindex];

            SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

            Popup_Manager.INSTANCE.Popup_Pop();

            GameManager.Instance.m_randomGrade = randomgrade[randomboxindex] + Random.Range(0, 4);

            Popup_Manager.INSTANCE.popup_index = 37;

            Popup_Manager.INSTANCE.Popup_Push();

            if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < GameManager.Instance.m_randomGrade)
            {
                GameManager.Instance.m_randombestgrade = true;
            }

            CarPortManager.Instance.AddFoodTruck(GameManager.Instance.m_randomGrade);

           
        }
        else if (GameManager.Instance.m_GameData.MyGold < price[randomboxindex])
        {
            Popup_Manager.INSTANCE.popup_index = 2;

            Popup_Manager.INSTANCE.Popup_Push();
        }
       
        
    }

  
}
