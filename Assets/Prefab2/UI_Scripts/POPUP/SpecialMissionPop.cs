﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialMissionPop : BackClickPop {


    public UILabel titletxt;

    public UILabel subtitletxt;

    public UILabel buttontxt;

    public List<UISprite> missionlevel = new List<UISprite>();

    public UISprite Missionbg;

    public CarPortTruck missiontruck;

    public UISprite lastmission;

    int tempcnt;

    public void Init()
    {
        tempcnt = 0;

        
        
        missiontruck.gameObject.SetActive(false);

        lastmission.gameObject.SetActive(false);

        titletxt.text = TableDataManager.Instance.GetLocalizeText(141);

        subtitletxt.text = TableDataManager.Instance.GetLocalizeText(142);

        buttontxt.text = TableDataManager.Instance.GetLocalizeText(143);

        Missionbg.spriteName = "Theme" + (StageItemManager.Instance.CurrentPart + 1).ToString();

        MissionTruckSetting();

      

        MissionLevelSetting();

        StartCoroutine(NowMissionNotice());

       
        
    }

    IEnumerator NowMissionNotice()
    {
        while (true)
        {

            yield return new WaitForSeconds(0.5f);

            tempcnt++;

            if (tempcnt % 2 == 0)
            {
                missionlevel[GameManager.Instance.m_GameData.NowMissionLevel].gameObject.SetActive(true);

            }
            else
            {
                missionlevel[GameManager.Instance.m_GameData.NowMissionLevel].gameObject.SetActive(false);
            }

        }
       
    }


    public void MissionTruckSetting()
    {

        if (GameManager.Instance.m_GameData.NowMissionLevel == 4)
        {
            missiontruck.gameObject.SetActive(false);

            lastmission.gameObject.SetActive(true);
        }
        else
        {
            FoodTruckinfo tempitem = TableDataManager.Instance.m_Foodtruck[GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade];

            missiontruck.SetInfo(tempitem);

            missiontruck.gameObject.SetActive(true);
        }
        
    }
    public void MissionLevelSetting()
    {
        for (int i = 0; i < missionlevel.Count; i++)
        {
            missionlevel[i].gameObject.SetActive(false);
        }

        for (int i = 0; i <GameManager.Instance.m_GameData.NowMissionLevel; i++)
        {
            missionlevel[i].gameObject.SetActive(true);
        }

        
    }

    public void PlayButtonClick()
    {
        GameManager.Instance.m_specialmissionchk = true;
        GameManager.Instance.m_specialmissionstart = true;

        SoundManager.Instance.PlayEffect("play_click");

        GetComponent<Parent_Pop>().ComeBackGame();
        
    }

    public void ExitButtonClick()
    {
        GameManager.Instance.m_specialmissionchk = false;
        GameManager.Instance.m_specialmissionstart = false;

        GetComponent<Parent_Pop>().MoveToBottom();
    }

    public override void BackButtonClick()
    {
        ExitButtonClick();
    }
}
