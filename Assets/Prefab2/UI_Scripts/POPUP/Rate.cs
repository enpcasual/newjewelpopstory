﻿using UnityEngine;
using System.Collections;

public class Rate : BackClickPop {

    public UILabel title;

    public UILabel info;

    public UILabel Btn;

    public void Init()
    {
        title.text = TableDataManager.Instance.GetLocalizeText(5);
        info.text = TableDataManager.Instance.GetLocalizeText(6);
        Btn.text = TableDataManager.Instance.GetLocalizeText(7);
    }

    public void moveToRate()
    {

       GameManager.Instance.m_ratechk = true;
       GameManager.Instance.m_ratepopchk = false;
        //더이상 레이트 유도 하지 않는다
        SecurityPlayerPrefs.SetInt("raterecodechk", 1);
        SecurityPlayerPrefs.SetInt("ratestageindex", -1);
        GameManager.Instance.m_GameData.RateRecodeChk = true;
        GameManager.Instance.m_GameData.RateStageIndex = -1;

#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.BRAEVE.newjewelpopstory.google");
#elif UNITY_IOS
        //Application.OpenURL("http://itunes.apple.com/app/id1255767173");
#endif
        
        GetComponent<Child_Pop>().MoveToTop();
        //StageItemManager.Instance.


    }
    public void ExitClick()
    {
        GameManager.Instance.m_ratepopchk = false;
        GetComponent<Child_Pop>().MoveToTop();
    }
    public override void BackButtonClick()
    {
        ExitClick();
    }
}
