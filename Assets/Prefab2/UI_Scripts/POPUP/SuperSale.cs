﻿using UnityEngine;
using System.Collections;

public class SuperSale : BackClickPop {

    public UILabel title;

    public UILabel beforeprice;

    public UILabel saleprice;

    public UILabel goldlabel;

    public UILabel info;

    public UILabel Button;

    public void Init()
    {
     

        title.text = TableDataManager.Instance.GetLocalizeText(15);
        saleprice.text = TableDataManager.Instance.GetLocalizeText(133);
        beforeprice.text = TableDataManager.Instance.GetLocalizeText(134);
        info.text = TableDataManager.Instance.GetLocalizeText(17);
        Button.text = TableDataManager.Instance.GetLocalizeText(146);
       
    }

    public void buybuttonclick()
    {
        //inapp
        //InAppPurchaser.Instance.BuyProduct(ProductKind.gold_11);
        GetComponent<Child_Pop>().MoveToTop();


    }

    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
