﻿using UnityEngine;
using System.Collections;

public class TruckAlbumPop : BackClickPop {

    public UIScrollView m_scrollView;

    public UIGrid m_grid;

    public UILabel titletxt;

   public void Init()
    {
        titletxt.text = TableDataManager.Instance.GetLocalizeText(128);

        GridSetting();

        m_grid.Reposition();

        m_scrollView.ResetPosition();
    }

    void GridSetting()
    {
        for (int i = 0; i < TableDataManager.Instance.m_Foodtruck.Count; i++)
        {
            Transform gridchild = m_grid.GetChild(i);

            
            if (GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < i)
            {

                

                gridchild.GetComponentInChildren<AlbumTruck>().bg.color = new Color(0, 0, 0);

                                
                gridchild.GetComponentInChildren<AlbumTruck>().truckicon.enabled = false;


                gridchild.GetComponentInChildren<AlbumTruck>().Grade = i;

                gridchild.GetComponentInChildren<AlbumTruck>().gradelabel.text = (i + 1).ToString() + ". ???? ";

               

                //  gradelabel.text = (grade + 1).ToString() + "???? ";
            }
            else
            {
                gridchild.GetComponentInChildren<AlbumTruck>().setinfo(i);


            }
            
        }
    }

    public override void BackButtonClick()
    {
        GetComponent<Parent_Pop>().MoveToBottom();
    }
}
