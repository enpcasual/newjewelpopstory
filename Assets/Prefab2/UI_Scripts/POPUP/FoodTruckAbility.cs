﻿using UnityEngine;
using System.Collections;

public class FoodTruckAbility : MonoBehaviour {

    public UISprite goldplus;

    public GameResultPop m_gameresultpop;

    public void GoldPlusStart()
    {
        goldplus.gameObject.SetActive(true);
    }

    public void GoldPlusenEnd()
    {
        goldplus.gameObject.SetActive(false);

        m_gameresultpop.truckgoldplusani();

    }

    public void AbilityEnd()
    {
        GetComponent<Animator>().SetBool("ABILITY_CHK", false);

        m_gameresultpop.foodtruckabilitychk = false;

       // m_gameresultpop.RewardFoodTruckPop();
    }
}
