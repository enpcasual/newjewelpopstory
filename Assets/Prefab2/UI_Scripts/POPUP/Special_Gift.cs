﻿using UnityEngine;
using System.Collections;

public class Special_Gift : BackClickPop {

    public static int Gold = 0;

    public UILabel gold_label;

    public UILabel title;

    public UILabel info;

    public UILabel btn;

    public void Init()
    {
       

        title.text = TableDataManager.Instance.GetLocalizeText(8);
        info.text = TableDataManager.Instance.GetLocalizeText(9);
        btn.text = TableDataManager.Instance.GetLocalizeText(10);

        GiftGodeSetting();
    }
    public void GiftGodeSetting()
    {
        gold_label.text = "+" + Gold.ToString();


        GameManager.Instance.m_GameData.MyGold += Gold;

        WorldMapUI.instance.addmoney = Gold; 

        
        SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

       
    }

    public override void BackButtonClick()
    {
        GetComponent<Child_Pop>().MoveToTop();
    }
}
