﻿using UnityEngine;
using System.Collections;

public class OpenGoldBox : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(popupdisplay());

        SoundManager.Instance.PlayEffect("buy");
    }

    IEnumerator popupdisplay()
    {
        GameManager.Instance.m_isTouch = false;

        transform.SetParent(StageItemManager.Instance.transform.parent, true);

        var sprite = GetComponent<UISprite>();
        if (sprite != null)
        {
            //TODO: 패널옮김. 더좋은방법이있으면 수정바람
            sprite.RemoveFromPanel();
            sprite.CreatePanel();
            sprite.depth = 5; // 트럭의 depth가 4.
        }
        yield return new WaitForSeconds(1f);

        //gameObject.SetActive(false);

        Popup_Manager.INSTANCE.popup_index = 9;//spacial_gift

        Popup_Manager.INSTANCE.Popup_Push();

        GameManager.Instance.m_isTouch = true;

        Destroy(gameObject);
    }
}