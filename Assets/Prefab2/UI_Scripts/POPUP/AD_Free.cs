﻿using UnityEngine;
using System.Collections;

public class AD_Free : BackClickPop {

    public UILabel title;

    public UILabel Ad_Free_info;

    public UILabel btn_label;



    //첫날은 나오지 않는다
    //둘째날 부터 광고 2회,4회에 나온다
    //총 8회까지만 나온다

    public void Init()
    {
        title.text = TableDataManager.Instance.GetLocalizeText(2);
        Ad_Free_info.text = TableDataManager.Instance.GetLocalizeText(3);
        btn_label.text = TableDataManager.Instance.GetLocalizeText(4);

#if !UNITY_EDITOR
            //금액을 받아온다.
            string price = InAppPurchaser.Instance.LocalePrice(ProductKind.free_ads);

            if(price.Equals("null") == false)
            btn_label.text = price;
#endif
    }
    
    public void EXitClick()
    {
        GetComponent<Child_Pop>().MoveToTop();

        if (GameManager.Instance.playquitchk)//StageItemManager.Instance.playExitchk || 
        {
            if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.ThreeMatch)
                MatchManager.Instance.EndGame();
        }
    }

    public override void BackButtonClick()
    {
        EXitClick();
    }

    public void okbtnclick()
    {

       
        //inapp
        InAppPurchaser.Instance.BuyProduct(ProductKind.free_ads);
        
    }
    public void Adfreeinapp()
    {
        GameManager.Instance.m_add_displaychk = true;

        SecurityPlayerPrefs.SetInt("addfreechk", 1);
    }
}
