using UnityEngine;
using System.Collections;

public class FollowMotionPath : MonoBehaviour
{
	public MotionPath motionPath;
	public float startPosition;
	public float speed;										// Realworld units per second you want your object to travel
	public bool loop;
	float uv;
    public float frequency;

    public Transform StageItem;

	void Start()
	{
		uv = startPosition;
		if (motionPath == null)
			enabled = false;

        StageSetting();
	}
	
	
	void FixedUpdate()
	{
      
		uv += ((speed / motionPath.length) * Time.fixedDeltaTime);			// This gets you uv amount per second so speed is in realworld units
		if (loop)
			uv = (uv<0?1+uv:uv) %1;
		else if (uv > 1)
			enabled = false;
		Vector3 pos = motionPath.PointOnNormalizedPath(uv);
		Vector3 norm = motionPath.NormalOnNormalizedPath(uv);
		
		transform.position = pos;
		transform.forward = speed >0?norm:-norm;
        
       
	}

    void StageSetting()
    {
        /*
        if (frequency <= 0) return;

        float stepSize = 1f / (frequency - 1f);

        float distanceTraveled = 0;

        for (int p = 0, f = 0; f < motionPath.controlPoints.Length; f++)
        {
            // for (int i = 0; i < items.Length; i++, p++)
            {
                Transform item = Instantiate(StageItem) as Transform;

                 distanceTraveled += Vector3.Distance(motionPath.controlPoints[f + 1], motionPath.controlPoints[f]);
                 Vector3 pos = motionPath.PointOnNormalizedPath(distanceTraveled);
                item.transform.localPosition = pos;
                //item.name = items[i].name + "_" + p.ToString();
                // Vector3 position = spline.GetPointByDistance(p * stepSize) + offset;
                //float scale = Mathf.Min(spline.GetVelocity(p * stepSize).magnitude/10f, 1f);

                //item.transform.localScale = Vector3.one * scale;
                // if (lookForward)
                //  {
                //     item.transform.LookAt(position + spline.GetDirection(p * stepSize));
                //  }
                item.transform.parent = transform;
            }
        }
         * */

        /*
        if (frequency <= 0) return;

        float stepSize = 1f / (frequency - 1f);

        for (int p = 0, f = 0; f < frequency; f++)
        {
           // for (int i = 0; i < items.Length; i++, p++)
            {
                Transform item = Instantiate(StageItem) as Transform;
                Vector3 pos = motionPath.PointOnNormalizedPath(stepSize*f);
                item.transform.localPosition = pos;
                //item.name = items[i].name + "_" + p.ToString();
               // Vector3 position = spline.GetPointByDistance(p * stepSize) + offset;
                //float scale = Mathf.Min(spline.GetVelocity(p * stepSize).magnitude/10f, 1f);
                
                //item.transform.localScale = Vector3.one * scale;
               // if (lookForward)
              //  {
               //     item.transform.LookAt(position + spline.GetDirection(p * stepSize));
              //  }
                item.transform.parent = transform;
            }
        }
         * */
    }
	
}
