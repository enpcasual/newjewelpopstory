﻿using UnityEngine;
using System.Collections;
using System;

public class WorldMapUI : MonoBehaviour {

    public static WorldMapUI instance;

    public UILabel MyGold;

    public UILabel MyStar;

    public int addmoney;

    public Color orgColor;

    public int nowMoney;


    public UISprite dailyrewardgold;

    public UISprite goldplus;

    public ButtonAniManager supersaleAnimation;

    public UISprite supersaleicon;

    public UILabel supersaletime;

    public UILabel missiontime;

    public UISprite missionicon;

    public UISprite freecoin;

    public UILabel freecointime;

    int movegab;
    

    public UISprite freecoineffect;

    public UISprite mytruckpostion;

    public UISprite package1;

    public UISprite package2;

    public UISprite Share_icon;

    public UIGrid BottomUIGrid;

    void Awake()
    {
        instance = this;

    }
    public void WorldmapStart()
    {
        StageItemManager.Instance.StageSartSetting(StageItemManager.Instance.rewardstar);
    }


    public void SetMoveButton(bool toFirst)
    {
        if (toFirst)
        {
            isMoveToFirst = true;
            mytruckpostion.spriteName = "icon_position";
        }
        else
        {
            isMoveToFirst = false;
            mytruckpostion.spriteName = "icon_position2";
        }

        mytruckpostion.gameObject.SetActive(true);
        WorldMapUI.instance.BottomUIGrid.Reposition();
    }

    private bool isMoveToFirst = false;

    public void FreeCoinSetting()
    {
        if (AdManager.Instance.IsAdReady)
        {
            if (TimeManager.Instance.freecoinchk)
            {
                freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

                freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

                freecoineffect.gameObject.SetActive(false);

                freecointime.gameObject.SetActive(true);
            }
            else
            {
                freecoindisplay();
            }
        }
        else
        {
            freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

            freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

            freecoineffect.gameObject.SetActive(false);
        }
       
    }

    public void VideoRewardResultSetting()
    {
        TimeManager.Instance.freecoinchk = true;

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", false);

        freecoineffect.gameObject.SetActive(false);

        freecointime.gameObject.SetActive(true);
    }
    public void UnityAdsRewardResult()
    {


        GameManager.Instance.m_adsRewardchk = false;

        long cooltime = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].cooltime;
#if ENABLE_CHEAT
        //cooltime = 2;
#endif
        long time = DateTime.Now.AddSeconds(cooltime).Ticks;


        TimeManager.Instance.m_freecointime = time;


        string cooltimestr = TimeManager.Instance.RefreshTimer(TimeManager.Instance.m_freecointime, 3);

        //텍스트갱신 1.월드맵
        if (WorldMapUI.instance != null)
            WorldMapUI.instance.freecointimedisplay(cooltimestr);
        //텍스트갱신 2.샵
        Popup_Manager.INSTANCE.popup_list[2].GetComponent<ShopPop>().freecointimedisplay(cooltimestr);
        //텍스트갱신 3.인게임버튼
        if (UI_ThreeMatch.Instance != null)
            UI_ThreeMatch.Instance.freecointimedisplay(cooltimestr);

        switch (TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].type)
        {
            case 0://골드
                {

                    GoldSetting(TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].reward);


                }
                break;
            case 1://아이템
                {
                    GameManager.Instance.m_adsRewardchk = true;

                    Popup_Manager.INSTANCE.popup_index = 0;

                    Popup_Manager.INSTANCE.Popup_Push();

                    /*
                    int itemindex = TableDataManager.Instance.m_FreeRewardlist[GameManager.Instance.m_GameData.FreeRewardnowindex].kind - 1;
                    GameManager.Instance.m_GameData.MyItemCnt[itemindex]++;
                    SecurityPlayerPrefs.SetInt("MyItem" + itemindex, GameManager.Instance.m_GameData.MyItemCnt[itemindex]);
                     * */
                }
                break;
        }


        SecurityPlayerPrefs.SetLong("FreeCoinTime", TimeManager.Instance.m_freecointime);


    }

    public void freecoindisplay()
    {
        freecoin.GetComponent<Animator>().SetBool("IDLE_ANI", true);

        freecoin.GetComponent<Animator>().SetBool("IDLE_STOP", false);

        freecoineffect.gameObject.SetActive(true);

        freecointime.gameObject.SetActive(false);

    }


    //보상형 광고 클릭
    public void freecoinclick()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        if (AdManager.Instance.IsAdReady)
        {
            if (!TimeManager.Instance.freecoinchk)
            {
                AdManager.AdsRewardResult = null;
                AdManager.Instance.Ad_Show("WorldMap");
            }
        }
        else
        {
            Popup_Manager.INSTANCE.popup_index = 33;
            Popup_Manager.INSTANCE.popup_list[33].GetComponent<NoticePop>().notice_setting(TableDataManager.Instance.GetLocalizeText(199));
            Popup_Manager.INSTANCE.Popup_Push();
        }
    }

    public void freecoinlistclick()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Popup_Manager.INSTANCE.popup_index = 34;

        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void freecointimedisplay(string time)
    {
        freecointime.text = time;
    }
    public void saletimedisplay(string time)
    {
        supersaletime.text = time;
    }

    public void missiontimedisplay(string time)
    {
        missiontime.text = time;
    }


    public void AddMyGold()
    {

        dailyrewardgold.gameObject.SetActive(false);

        orgColor = MyGold.color;
        MyGold.fontSize = 40;

        nowMoney = GameManager.Instance.m_GameData.MyGold - addmoney;

        SoundManager.Instance.PlayEffect("coin");

        movegab = 1;

        if (addmoney < 100)
        {
            movegab = 20;
        }
        else if (addmoney >= 100 && addmoney < 500)
        {
            movegab = 50;
        }
        else if (addmoney >= 500 && addmoney <= 5000)
        {
            movegab = 200;
        }
        else if (addmoney > 5000 && addmoney < 50000)
        {
            movegab = 1000;
        }
        else if (addmoney >= 50000 && addmoney < 500000)
        {
            movegab = 10000;
        }
        else
        {
            movegab = 50000;
        }
        
        StartCoroutine(moneyadd());
        
    }

    IEnumerator moneyadd()
    {
        while (addmoney > 0)
        {
            nowMoney += movegab;
            addmoney -= movegab;

            MyGold.text = nowMoney.ToString();

            yield return null;
        }
        
        yield return null;
        MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
        MyGold.fontSize = 30;
        MyGold.color = orgColor;

        GameManager.Instance.m_isTouch = true;

        
        

    }
    
    public void goldplusclick()
    {
        if(GameManager.Instance.m_isTouch == false)
            return;

        SoundManager.Instance.PlayEffect("button_click");

        Popup_Manager.INSTANCE.popup_index = 2;

        Popup_Manager.INSTANCE.Popup_Push();

        goldplus.GetComponent<ButtonAniManager>().Init();
    }

    public void MissionIconClick()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        SoundManager.Instance.PlayEffect("button_click");

        if (GameManager.Instance.m_GameData.NowMissionLevel < 5)
        {
            Popup_Manager.INSTANCE.popup_index = 26;

            Popup_Manager.INSTANCE.Popup_Push();
        }
        else if (GameManager.Instance.m_GameData.NowMissionLevel == 5)
        {
            Popup_Manager.INSTANCE.popup_index = 27;

            Popup_Manager.INSTANCE.Popup_Push();
        }
       

        missionicon.GetComponent<ButtonAniManager>().Init();
    }


    public void GoldRefresh()
    {
        MyGold.text = GameManager.Instance.m_GameData.MyGold.ToString("N0");
    }

    public void StarReFresh()
    {
        MyStar.text = GameManager.Instance.m_GameData.MyStar.ToString("N0");
    }
    public void GoldSetting(int rewardmoney)
    {
       nowMoney = GameManager.Instance.m_GameData.MyGold;

       GameManager.Instance.m_GameData.MyGold += rewardmoney;

       addmoney = rewardmoney;

       SecurityPlayerPrefs.SetInt("MyGold", GameManager.Instance.m_GameData.MyGold);

       if (GameManager.Instance.m_CrruntUI.m_GameState == GAMESTATE.WorldMap)
       {
           GameManager.Instance.m_isTouch = false; //moneyadd()코루틴에서 true
            dailyrewardgold.gameObject.SetActive(true);
       }
       
    }

    public void openCarPort()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Debug.Log("openCarPort:::::::::::::::::");

        SoundManager.Instance.PlayEffect("button_click");

        GameManager.Instance.SetGameState(GAMESTATE.CarPort);

        GameManager.Instance.newstageclear = false;
        
        CarPortManager.Instance.SuperSaleIcon.gameObject.SetActive(false);

        if (TimeManager.Instance.trucksaletimechk && GameManager.Instance.m_GameData.MyFoodTruckData.m_MyFoodTruckGrade < 29)
        {
            CarPortManager.Instance.SuperSaleIcon.gameObject.SetActive(true);
        }

    }

    public void PackageiconClick()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Popup_Manager.INSTANCE.popup_index = 35;
        Popup_Manager.INSTANCE.Popup_Push();
    }

    public void ShareClick()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Popup_Manager.INSTANCE.popup_index = 38;
        Popup_Manager.INSTANCE.Popup_Push();
    }
}
