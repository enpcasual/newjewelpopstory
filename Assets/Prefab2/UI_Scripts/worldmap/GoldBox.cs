﻿using UnityEngine;
using System.Collections;

public class GoldBox : MonoBehaviour {
    public GameObject OpenGoldBox;
    private Animator m_Animator = null;
    private void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    public void ChangeGoldBox()
    {
        m_Animator.SetBool("BOX_OPEN", false);
        gameObject.SetActive(false);
        OpenGoldBox.SetActive(true);

        Debug.Log("ChangeGoldBox::::::::::::::::::");

        GameManager.Instance.m_isTouch = true;

        //Destroy(gameObject);
    }

    public void openAniSetting()
    {
        m_Animator.SetBool("BOX_OPEN", true);

        GameManager.Instance.m_isTouch = false;

        //transform.SetParent(StageItemManager.Instance.transform.parent, true);

        var sprite = GetComponent<UISprite>();
        if (sprite != null)
        {
            //TODO: 패널옮김. 더좋은방법이있으면 수정바람
            sprite.RemoveFromPanel();
            sprite.CreatePanel();
            sprite.depth = 5; // 트럭의 depth가 4.
        }
    }
}
