﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cloud : MonoBehaviour {

    public List<UISprite> cloudlist = new List<UISprite>();

    int dir;

    Vector3 startpos;

    float nowpos = 0;

    float despos = 0;

    float movegab = 0.2f;

    public void Awake()
    {
        int type = Random.Range(0, 4);

        dir = Random.Range(0, 2);

        nowpos = 0;

        despos = 0;

        for (int i = 0; i < cloudlist.Count; i++)
        {
            cloudlist[i].gameObject.SetActive(false);
        }

        switch (type)
        {
            case 0:
                cloudlist[0].gameObject.SetActive(true);
                cloudlist[1].gameObject.SetActive(true);
                cloudlist[2].gameObject.SetActive(true);
                break;
            case 1:
                cloudlist[0].gameObject.SetActive(true);
              //  cloudlist[1].gameObject.SetActive(true);
                cloudlist[2].gameObject.SetActive(true);
                break;
            case 2:
                //cloudlist[0].gameObject.SetActive(true);
                cloudlist[1].gameObject.SetActive(true);
                cloudlist[2].gameObject.SetActive(true);
                break;
            case 3:
                cloudlist[0].gameObject.SetActive(true);
                cloudlist[1].gameObject.SetActive(true);
                //cloudlist[2].gameObject.SetActive(true);
                break;

        }

        
        
        Vector3 addtpos = new Vector3(0, 0, 0);
             


        //gameObject.transform.position = new Vector3(0,pos.y,0);

        if (dir == 0)
        {
           

            addtpos = new Vector3(300, Random.Range(200, 300), 0);

            movegab = -0.2f;
        }
        else if (dir == 1)
        {
            

            addtpos = new Vector3(0, Random.Range(200, 300), 0);

            movegab = 0.2f;
        }

        despos = 700;

        gameObject.transform.localPosition += addtpos;


        startpos = gameObject.transform.localPosition;   

      }

   
    void OnEnable()
    {
        StartCoroutine(moveCloud());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

   
    IEnumerator moveCloud()
    {
        
        while (despos > nowpos)
        {
            nowpos += 0.2f;

            gameObject.transform.localPosition += new Vector3(movegab, 0, 0);

            yield return null;
        }
        
    }




}
