﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MenuManager : MonoBehaviour {

    public UISprite gamecenter_btn;

    [SerializeField]
    private List<GameObject> m_OptionObjs = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_OptionCoverObjs = new List<GameObject>();

    private bool isOpen = false;

    public void Awake()
    {
#if UNITY_ANDROID
        gamecenter_btn.spriteName = "icon_gamecenter";
#elif UNITY_IOS
        gamecenter_btn.spriteName = "icon_gamecenter_ios";
#endif
        optionchk();
    }

    public void OnEnable()
    {
        CloseMenu();
    }

    public void moveToGameCenter()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

#if UNITY_ANDROID
        Popup_Manager.INSTANCE.popup_index = 10;
        Popup_Manager.INSTANCE.Popup_Push();
#elif UNITY_IOS
		Debug.Log("BJS:GameCenterClick");
		if (GCIC_Manager.Instance.isLogin())
		{
			Debug.Log("BJS:GameCenterClick_Show");
			GCIC_Manager.Instance.ShowAchievementsUI();
		}
		else
		{
			Debug.Log("BJS:GameCenterClick_Login");
			GameManager.Instance.m_Achivementloginchk = true;

			GCIC_Manager.Instance.LoginGC();
		}
#endif
        CloseMenu();
    }

    public void moveToShop()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Popup_Manager.INSTANCE.popup_index = 2;
        Popup_Manager.INSTANCE.Popup_Push();
        CloseMenu();
    }

    public void moveToOption()
    {
        optionchk();

        if (isOpen)
        {
            for (int i = m_OptionObjs.Count - 1; i >= 0; i--)
            {
                m_OptionObjs[i].transform.DOLocalMoveY(0, 0.1f * i);
            }
        }
        else
        {
            for (int i = 0; i < m_OptionObjs.Count; i++)
            {
                m_OptionObjs[i].transform.DOLocalMoveY(i * 100 + 140, 0.1f * i);
            }
        }

        isOpen = !isOpen;
    }

    public void CloseMenu()
    {
        if (isOpen != false)
        {
            isOpen = false;

            for (int i = m_OptionObjs.Count - 1; i >= 0; i--)
            {
                m_OptionObjs[i].transform.DOLocalMoveY(0, 0.1f * i);
            }
        }
    }
    
    public void OpenOptionPopup()
    {
        if (GameManager.Instance.m_isTouch == false)
            return;

        Popup_Manager.INSTANCE.popup_index = 1;
        Popup_Manager.INSTANCE.Popup_Push();
        CloseMenu();
    }

    public void moregameclick()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://search?q=pub:ENP+Games");
#elif UNITY_IOS
        Application.OpenURL("https://itunes.apple.com/kr/developer/enp-games/id829463188?mt=8");
#endif
    }

    public void optionchk()
    {
        
            m_OptionCoverObjs[1].gameObject.SetActive(SoundManager.Instance.isEffect == false);
        
            m_OptionCoverObjs[0].gameObject.SetActive(SoundManager.Instance.isBGM == false);
            m_OptionCoverObjs[2].gameObject.SetActive(TimeManager.Instance.notification != 1);


    }

    public void EffectSoundSetting()
    {
        //bool effect_snd = SecurityPlayerPrefs.GetInt("isEffect", 1) == 1 ? true : false;

        if (GameManager.Instance.m_GameData.Effect_Snd == false)
        {
            SoundManager.Instance.isEffect = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SecurityPlayerPrefs.SetInt("isEffect", 1);
            m_OptionCoverObjs[1].gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Effect_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Effect_Snd == true)
        {
            SoundManager.Instance.isEffect = false;
            SecurityPlayerPrefs.SetInt("isEffect", 0);
            m_OptionCoverObjs[1].gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Effect_Snd = false;
        }
    }

    public void BGSoundSetting()
    {


        // bool bg_snd = SecurityPlayerPrefs.GetInt("isBGM", 1) == 1 ? true : false;

        if (GameManager.Instance.m_GameData.Bg_Snd == false)
        {
            SoundManager.Instance.isBGM = true;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SoundManager.Instance.PlayBGM("intro_bgm");
            SecurityPlayerPrefs.SetInt("isBGM", 1);
            m_OptionCoverObjs[0].gameObject.SetActive(false);

            GameManager.Instance.m_GameData.Bg_Snd = true;
        }
        else if (GameManager.Instance.m_GameData.Bg_Snd == true)
        {
            SoundManager.Instance.isBGM = false;
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            SoundManager.Instance.StopBGM();
            SecurityPlayerPrefs.SetInt("isBGM", 0);
            m_OptionCoverObjs[0].gameObject.SetActive(true);

            GameManager.Instance.m_GameData.Bg_Snd = false;
        }
    }

    public void NotificationSetting()
    {

        //GameManager.Instance.m_notification = SecurityPlayerPrefs.GetInt("notification", 1);

        m_OptionCoverObjs[2].gameObject.SetActive(GameManager.Instance.m_GameData.Notification != 1);

        if (GameManager.Instance.m_GameData.Notification == 0)
        {
            GameManager.Instance.m_GameData.Notification = 1;
            SecurityPlayerPrefs.SetInt("notification", GameManager.Instance.m_GameData.Notification);
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            m_OptionCoverObjs[2].gameObject.SetActive(false);
            //TimeManager.Instance.AlarmSetting("alarmSetting");

            //GCM 푸시 설정
            //IGAWorks_Manager.instance.SetNotificationIcon(true);
        }
        else if (GameManager.Instance.m_GameData.Notification == 1)
        {
            GameManager.Instance.m_GameData.Notification = 0;
            SecurityPlayerPrefs.SetInt("notification", GameManager.Instance.m_GameData.Notification);
            SoundManager.Instance.PlayEffect("button_click", false, 1f);
            m_OptionCoverObjs[2].gameObject.SetActive(true);
            //TimeManager.Instance.releaseAlarm("releasealarm");

            //GCM 푸시 설정
            //IGAWorks_Manager.instance.SetNotificationIcon(false);
        }
    }

}
