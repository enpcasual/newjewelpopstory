using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HUDFPS : MonoBehaviour
{

    // Attach this to a GUIText to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // correct overall FPS even if the interval renders something like
    // 5.5 frames.

    public float updateInterval = 0.5F;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval

    private Text text;
    void Start()
    {
        text = GetComponent<Text>();
        if (!text)
        {
            Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
            enabled = false;
            return;
        }
        timeleft = updateInterval;
    }

    void Update()
    {
        //기존에 있던 방법..
        //delta타임을 이용해서 프레임횟수/delta시간 ::타임 스케일에 영향을 받는다.
        /*
        timeleft -= Time.deltaTime;
        accum += Time.timeScale/Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if( timeleft <= 0.0 )
        {
            // display two fractional digits (f2 format)
        float fps = accum/frames;
        string format = System.String.Format("{0:F2} FPS",fps);
        guiText.text = format;

        if(fps < 30)
            guiText.material.color = Color.yellow;
        else 
            if(fps < 10)
                guiText.material.color = Color.red;
            else
                guiText.material.color = Color.green;
        //  DebugConsole.Log(format,level);
        //Debug.Log(format);
            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
        }
        */

        //타임 스케일에 영향을 받지 않는realtimeSinceStartup를 이용한방법
        //realtimeSinceStartup은 게임이 시작한 순간부터 시간을 체크하므로 timescale에 영향을 받지 않는다.
        m_nFrame += 1;
        float fTimeNow = Time.realtimeSinceStartup;
        if (fTimeNow >= m_fLastFrameTime + m_fPrecision)
        {
            m_fFPS = m_nFrame / (fTimeNow - m_fLastFrameTime);
            m_nFrame = 0;
            m_fLastFrameTime = fTimeNow;

            string format = System.String.Format("{0:F2} FPS", m_fFPS);
            text.text = format;
        }
    }
    long m_nFrame = 0;
    float m_fLastFrameTime = 0f;
    float m_fPrecision = 0.5f;

    float m_fFPS = 0f;
}