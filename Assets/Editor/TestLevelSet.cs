﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TestLevelSet : Editor {

    [MenuItem("ENP/LevelSet")]
    public static void LevelSet()
    {
        using (System.IO.StreamReader file =
            new System.IO.StreamReader(@"E:\TestFolder\TestLog.txt", true))
        {
            string levelText =  file.ReadToEnd();

            string[] levelTexts = levelText.Split('\n');
            int index = 0;
            foreach(string level in levelTexts)
            {
                if ((!string.IsNullOrEmpty(level) && level.Length > 10))
                {
                    string[] texts = level.Split('(');

                    string[] ts = texts[0].Split(' ');

                    string stageText = ts[0].Remove(0, 6);
                    Debug.Log(stageText);
                    int stageindex = int.Parse(stageText);

                    index = stageindex;
                        string[] successrate = texts[1].Split(':');

                        int success = int.Parse(successrate[0]);
                        int failure = int.Parse(successrate[1].Replace(") /", ""));
                        int playCount = success + failure;

                        if (success == 0)
                        {
                            Debug.Log("is Not success stage : " + stageindex);
                            continue;
                        }

                        string[] scores = texts[2].Split('_');

                        int average = int.Parse(scores[0]);
                        Debug.Log(scores[0] + " " + average);
                        int max = int.Parse(scores[1]);
                        Debug.Log(scores[1] + " " + max);
                        Debug.Log(scores[2].Replace(")", ""));
                        int min = int.Parse(scores[2].Replace(")", ""));
                        Debug.Log(scores[2].Replace(")", "") + " " + min);

                        int move = int.Parse(texts[3].Replace(")", ""));

                        Stage stage = DataManager.LoadStage(stageindex);
                        stage.scoreStar1 = (long)(average * 0.3f);
                        stage.scoreStar2 = (long)(average * 0.7f);
                        stage.scoreStar3 = (long)(average * 0.9f);
                        //stage.limit_Move = 50;

                        //int standard = 

                        if (((float)success / (float)playCount > 1f - stageindex * 0.001) && stageindex > 50)
                        {
                            //난이도 조절 성공률이 높을때 Move 마이너스
                            stage.limit_Move -= (int)(((float)move * 0.5f) + (((float)stageindex * 0.01f)) * ((float)success / (float)playCount));
                        }
                        else if (stageindex > 50)
                        {
                            //성공률이 낮을때 Move 플러스
                            //float step1 = 1f - (success / playCount);
                            //float step2 = (10f * step1);
                            //int plusMove = (int)(step2 - (stageindex * 0.01f));

                            int plusMove = (int)((10f * (1f - ((float)success / (float)playCount))) - (stageindex * 0.01f));
                            if (plusMove > 0)
                            {
                                stage.limit_Move += plusMove;
                            }

                        }

                        //Debug.Log("난이도 스테이지 : " + stageindex + " _ " +  (((float)stageindex * 0.01f)) * ((float)success / (float)playCount));

                        DataManager.SaveStage(stageindex, stage);
                    }
                else
                {
                    Debug.Log("Not Stage Data index: " + index);

                }

                //Debug.Log("");
            }
        }
    }
}
