﻿{
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 1\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 0\r\n}"
						}
					]
			}
		],
	"items": 
		[
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal"
		],
	"colors": 
		[
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 10000,
	"scoreStar2": 15000,
	"scoreStar3": 20000,
	"limit_Move": 40,
	"missionType": "OrderS",
	"isOrderNMission": true,
	"isOrderSMission": true,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Bread",
				"count": 0
			},
			{
				"type": "OrderS",
				"kind": "Line",
				"count": 50
			}
		],
	"food_MaxExist": 7,
	"food_Interval": 1,
	"food_SpawnCnt": 3,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 0,
	"Donut_MaxExist": 0,
	"Donut_Interval": 0,
	"Donut_SpawnCnt": 0,
	"TimeBomb_MinExist": 20,
	"TimeBomb_MaxExist": 50,
	"TimeBomb_Interval": 0,
	"TimeBomb_SpawnCnt": 20,
	"TimeBomb_FirstCount": 10,
	"Bear_MaxExist": 8,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 0,
	"Mystery_Interval": 0,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 0,
	"Chameleon_Interval": 0,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 1,
	"Key_MaxExist": 1,
	"Key_Interval": 3,
	"Key_SpawnCnt": 1,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Line_X",
			"Line_X",
			"Line_Y",
			"Line_Y"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}