﻿{
	"DropDirs": 
		[],
	"isUseGravity": false,
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "S_Tree",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Creator_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Creator_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "MagicColor",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			}
		],
	"items": 
		[
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Donut",
			"Ghost",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Ghost"
		],
	"colors": 
		[
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			true,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"defaultSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"SpiralSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"DonutSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"TimeBombSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"MysterySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"ChameleonSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"KeySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 3316,
	"scoreStar2": 7737,
	"scoreStar3": 9948,
	"limit_Move": 30,
	"missionType": "OrderN",
	"isOrderNMission": true,
	"isOrderSMission": false,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Donut",
				"count": 32
			}
		],
	"food_MaxExist": 0,
	"food_Interval": 0,
	"food_SpawnCnt": 0,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 14,
	"Donut_MaxExist": 14,
	"Donut_Interval": 1,
	"Donut_SpawnCnt": 14,
	"TimeBomb_MinExist": 0,
	"TimeBomb_MaxExist": 0,
	"TimeBomb_Interval": 0,
	"TimeBomb_SpawnCnt": 0,
	"TimeBomb_FirstCount": 15,
	"Bear_MaxExist": 0,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 0,
	"Mystery_Interval": 0,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 0,
	"Chameleon_Interval": 0,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 0,
	"Key_MaxExist": 0,
	"Key_Interval": 0,
	"Key_SpawnCnt": 0,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Line_X",
			"Line_X",
			"Line_X",
			"Line_X"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}