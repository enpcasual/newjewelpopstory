﻿{
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			}
		],
	"items": 
		[
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"colors": 
		[
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 3000,
	"scoreStar2": 4500,
	"scoreStar3": 5800,
	"limit_Move": 28,
	"missionType": "OrderN",
	"isOrderNMission": true,
	"isOrderSMission": false,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Bread",
				"count": 0
			},
			{
				"type": "OrderN",
				"kind": "Orange",
				"count": 24
			}
		],
	"food_MaxExist": 7,
	"food_Interval": 1,
	"food_SpawnCnt": 3,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 0,
	"Donut_MaxExist": 0,
	"Donut_Interval": 0,
	"Donut_SpawnCnt": 0,
	"TimeBomb_MinExist": 20,
	"TimeBomb_MaxExist": 50,
	"TimeBomb_Interval": 0,
	"TimeBomb_SpawnCnt": 20,
	"TimeBomb_FirstCount": 10,
	"Bear_MaxExist": 8,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 0,
	"Mystery_Interval": 0,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 0,
	"Chameleon_Interval": 0,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 3,
	"Key_MaxExist": 3,
	"Key_Interval": 3,
	"Key_SpawnCnt": 1,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Normal",
			"Normal",
			"Normal",
			"Normal"
		],
	"JewelTreeItemColor": 
		[
			"ORANGE",
			"ORANGE",
			"ORANGE",
			"ORANGE"
		]
}