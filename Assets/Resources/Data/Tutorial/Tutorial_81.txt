﻿{
	"Stage": 91,
	"PanelY": 3,
	"PlayTimeSpeed": 1,
	"PaseInfos": 
		[
			{
				"PaseIndex": 0,
				"Points": 
					[
						{
							"x": 1,
							"y": 1
						},
						{
							"x": 1,
							"y": 2
						},
						{
							"x": 1,
							"y": 3
						},
						{
							"x": 2,
							"y": 1
						},
						{
							"x": 2,
							"y": 2
						},
						{
							"x": 2,
							"y": 3
						},
						{
							"x": 3,
							"y": 1
						},
						{
							"x": 3,
							"y": 2
						},
						{
							"x": 3,
							"y": 3
						}
					],
				"ItemInfos": 
					[
						{
							"ID": 0,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 1,
							"Type": "Item",
							"ItemIndex": 22,
							"X": 1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 0,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 1,
										"Order": 4,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 1
						},
						{
							"ID": 2,
							"Type": "Item",
							"ItemIndex": 21,
							"X": 1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 2,
										"Order": 4,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialProduce",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 3,
										"Order": 4,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 4,
										"Order": 6,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialMove",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"X\": 0,\r\n\t\"Y\": 0,\r\n\t\"TargetID\": -1,\r\n\t\"UseSmalling\": false\r\n}"
									},
									{
										"ID": 5,
										"Order": 6,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 2.5,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 6,
										"Order": 7,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 0,\r\n\t\"RotateZ_End\": 10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 7,
										"Order": 8,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 10,\r\n\t\"RotateZ_End\": -10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 8,
										"Order": 9,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": -10,\r\n\t\"RotateZ_End\": 10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 9,
										"Order": 10,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 10,\r\n\t\"RotateZ_End\": -10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 10,
										"Order": 11,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": -10,\r\n\t\"RotateZ_End\": 10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 11,
										"Order": 12,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 10,\r\n\t\"RotateZ_End\": -10,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 12,
										"Order": 13,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialRotation",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": -10,\r\n\t\"RotateZ_End\": 0,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 13,
										"Order": 14,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 2
									},
									{
										"ID": 14,
										"Order": 15,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 3,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 0,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 15,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 16,
										"Order": 2,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialSwap",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"TargetID\": 8\r\n}"
									},
									{
										"ID": 17,
										"Order": 3,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 4,
							"Type": "Item",
							"ItemIndex": 0,
							"X": -1,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 18,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 19,
										"Order": 3,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 5,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 1,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 20,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 21,
										"Order": 3,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 6,
							"Type": "Item",
							"ItemIndex": 0,
							"X": -1,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 22,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 23,
										"Order": 15,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "RED",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 7,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 1,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 24,
										"Order": 1,
										"Time": 0.3,
										"Color": "YELLOW",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 25,
										"Order": 15,
										"Time": 0.3,
										"Color": "YELLOW",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "YELLOW",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 8,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 26,
										"Order": 1,
										"Time": 0.3,
										"Color": "BLUE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 27,
										"Order": 15,
										"Time": 0.3,
										"Color": "BLUE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "BLUE",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 9,
							"Type": "Item",
							"ItemIndex": 0,
							"X": -1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 28,
										"Order": 1,
										"Time": 0.3,
										"Color": "PURPLE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 29,
										"Order": 15,
										"Time": 0.3,
										"Color": "PURPLE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "PURPLE",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 10,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 0,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 30,
										"Order": 1,
										"Time": 0.3,
										"Color": "ORANGE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 31,
										"Order": 15,
										"Time": 0.3,
										"Color": "ORANGE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "ORANGE",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 11,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 12,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 13,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 14,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 15,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 16,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 17,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 18,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 19,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 20,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 21,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						}
					],
				"ItemParent": 
					{
						"ID": 0,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"PanelParent": 
					{
						"ID": 11,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"PanelDefaultParent": 
					{
						"ID": 12,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"X": -0.4,
				"Y": -0.5,
				"PanelY": 0,
				"TextTableIndex": 262
			}
		],
	"List_Seed": 
		[]
}