﻿{
	"Stage": 401,
	"PanelY": 3,
	"PlayTimeSpeed": 0.8,
	"PaseInfos": 
		[
			{
				"PaseIndex": 0,
				"Points": 
					[
						{
							"x": 1,
							"y": 1
						},
						{
							"x": 1,
							"y": 2
						},
						{
							"x": 1,
							"y": 3
						},
						{
							"x": 2,
							"y": 1
						},
						{
							"x": 2,
							"y": 2
						},
						{
							"x": 2,
							"y": 3
						},
						{
							"x": 3,
							"y": 1
						},
						{
							"x": 3,
							"y": 2
						},
						{
							"x": 3,
							"y": 3
						}
					],
				"ItemInfos": 
					[
						{
							"ID": 0,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 1,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 0,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 0,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 1,
										"Order": 2,
										"Time": 0.5,
										"Color": "GREEN",
										"Type": "TutorialSwap",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"TargetID\": 4\r\n}"
									},
									{
										"ID": 2,
										"Order": 3,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 3,
										"Order": 4,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 4,
										"Order": 5,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 5,
										"Order": 6,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.39,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 6,
										"Order": 7,
										"Time": 0.2,
										"Color": "GREEN",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 7,
										"Order": 8,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 2,
							"Type": "Item",
							"ItemIndex": 16,
							"X": -1,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 8,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 9,
										"Order": 3,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 10,
										"Order": 4,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 11,
										"Order": 5,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 12,
										"Order": 6,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.39,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 13,
										"Order": 7,
										"Time": 0.2,
										"Color": "GREEN",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 14,
										"Order": 8,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 29
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 3,
							"Type": "Item",
							"ItemIndex": 16,
							"X": 1,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 15,
										"Order": 1,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 16,
										"Order": 3,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 17,
										"Order": 4,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 18,
										"Order": 5,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": true,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.7,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 19,
										"Order": 6,
										"Time": 0.3,
										"Color": "GREEN",
										"Type": "TutorialScale",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 1.39,\r\n\t\"EndScale\": 1.39,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 20,
										"Order": 7,
										"Time": 0.2,
										"Color": "GREEN",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 21,
										"Order": 8,
										"Time": 1,
										"Color": "GREEN",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 29
									}
								],
							"Color": "GREEN",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 4,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[
									{
										"ID": 22,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 23,
										"Order": 8,
										"Time": 1,
										"Color": "RED",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "RED",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 5,
							"Type": "Item",
							"ItemIndex": 0,
							"X": -1,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 24,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 25,
										"Order": 8,
										"Time": 1,
										"Color": "RED",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "RED",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 6,
							"Type": "Item",
							"ItemIndex": 0,
							"X": 1,
							"Y": 1,
							"TutorialOrders": 
								[
									{
										"ID": 26,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 27,
										"Order": 8,
										"Time": 1,
										"Color": "RED",
										"Type": "TutorialDelete",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 27
									}
								],
							"Color": "RED",
							"ParticlesIndex": 
								[
									0,
									1,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 0
						},
						{
							"ID": 7,
							"Type": "Item",
							"ItemIndex": 18,
							"X": 1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 28,
										"Order": 8,
										"Time": 0.25,
										"Color": "None",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 31,
										"ParticleIndex": 0
									},
									{
										"ID": 29,
										"Order": 8,
										"Time": 0.25,
										"Color": "None",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 32,
										"ParticleIndex": 0
									},
									{
										"ID": 30,
										"Order": 8,
										"Time": 0.25,
										"Color": "None",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 33,
										"ParticleIndex": 0
									},
									{
										"ID": 31,
										"Order": 11,
										"Time": 1,
										"Color": "None",
										"Type": "TutorialProduce",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 32,
										"Order": 14,
										"Time": 1,
										"Color": "None",
										"Type": "TutorialScale",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 0,\r\n\t\"EndScale\": 1.39,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 33,
										"Order": 15,
										"Time": 1,
										"Color": "None",
										"Type": "TutorialRotation",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 90,\r\n\t\"RotateZ_End\": 0,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 34,
										"Order": 12,
										"Time": 0.3,
										"Color": "None",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 35,
										"Order": 14,
										"Time": 0.3,
										"Color": "None",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "None",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 1
						},
						{
							"ID": 8,
							"Type": "Item",
							"ItemIndex": 15,
							"X": -1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 36,
										"Order": 8,
										"Time": 0.25,
										"Color": "PURPLE",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 39,
										"ParticleIndex": 0
									},
									{
										"ID": 37,
										"Order": 8,
										"Time": 0.25,
										"Color": "PURPLE",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 40,
										"ParticleIndex": 0
									},
									{
										"ID": 38,
										"Order": 8,
										"Time": 0.25,
										"Color": "PURPLE",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 41,
										"ParticleIndex": 0
									},
									{
										"ID": 39,
										"Order": 11,
										"Time": 1,
										"Color": "PURPLE",
										"Type": "TutorialProduce",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 40,
										"Order": 18,
										"Time": 1,
										"Color": "PURPLE",
										"Type": "TutorialScale",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"isPingPong\": false,\r\n\t\"StartScale\": 0,\r\n\t\"EndScale\": 1.39,\r\n\t\"X\": true,\r\n\t\"Y\": true\r\n}"
									},
									{
										"ID": 41,
										"Order": 19,
										"Time": 1,
										"Color": "PURPLE",
										"Type": "TutorialRotation",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"RotateZ_Start\": 90,\r\n\t\"RotateZ_End\": 0,\r\n\t\"isPingPong\": false\r\n}"
									},
									{
										"ID": 42,
										"Order": 12,
										"Time": 0.3,
										"Color": "PURPLE",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 43,
										"Order": 13,
										"Time": 0.5,
										"Color": "PURPLE",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0
									},
									{
										"ID": 44,
										"Order": 14,
										"Time": 0.3,
										"Color": "PURPLE",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "PURPLE",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 1
						},
						{
							"ID": 9,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 10,
							"Type": "Panel",
							"ItemIndex": 18,
							"X": -1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 45,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 46,
										"Order": 8,
										"Time": 0.1,
										"Color": "RED",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 47,
										"ParticleIndex": 0
									},
									{
										"ID": 47,
										"Order": 9,
										"Time": 1,
										"Color": "RED",
										"Type": "TutorialDelete",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 21
									}
								],
							"Color": "None",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 2
						},
						{
							"ID": 11,
							"Type": "Panel",
							"ItemIndex": 18,
							"X": 0,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 48,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 49,
										"Order": 14,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 100,\r\n\t\"EndAlpha\": 0\r\n}"
									}
								],
							"Color": "None",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 2
						},
						{
							"ID": 12,
							"Type": "Panel",
							"ItemIndex": 18,
							"X": 1,
							"Y": -1,
							"TutorialOrders": 
								[
									{
										"ID": 50,
										"Order": 1,
										"Time": 0.3,
										"Color": "RED",
										"Type": "TutorialAlpha",
										"isNotUseOrder": false,
										"NextOrderID": -1,
										"ParticleIndex": 0,
										"addData": "{\r\n\t\"StartAlpha\": 0,\r\n\t\"EndAlpha\": 100\r\n}"
									},
									{
										"ID": 51,
										"Order": 8,
										"Time": 0.1,
										"Color": "RED",
										"Type": "TutorialEmphasis",
										"isNotUseOrder": false,
										"NextOrderID": 52,
										"ParticleIndex": 0
									},
									{
										"ID": 52,
										"Order": 9,
										"Time": 1,
										"Color": "RED",
										"Type": "TutorialDelete",
										"isNotUseOrder": true,
										"NextOrderID": -1,
										"ParticleIndex": 21
									}
								],
							"Color": "None",
							"ParticlesIndex": 
								[
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0,
									0
								],
							"SpriteOrder": 2
						},
						{
							"ID": 13,
							"Type": "Parent",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": 0
						},
						{
							"ID": 14,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 15,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 16,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": -1.00000012,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 17,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 18,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 19,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 0,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 20,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": -0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 21,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": 0,
							"TutorialOrders": 
								[],
							"Color": "RED",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						},
						{
							"ID": 22,
							"Type": "Panel",
							"ItemIndex": 0,
							"X": 1,
							"Y": 0.99999994,
							"TutorialOrders": 
								[],
							"Color": "None",
							"ParticlesIndex": 
								[],
							"SpriteOrder": -1
						}
					],
				"ItemParent": 
					{
						"ID": 0,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"PanelParent": 
					{
						"ID": 9,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"PanelDefaultParent": 
					{
						"ID": 13,
						"Type": "Parent",
						"ItemIndex": 0,
						"X": 0,
						"Y": 0,
						"TutorialOrders": 
							[],
						"Color": "RED",
						"ParticlesIndex": 
							[],
						"SpriteOrder": 0
					},
				"X": -0.4,
				"Y": -0.5,
				"PanelY": 0,
				"TextTableIndex": 278
			}
		],
	"List_Seed": 
		[]
}