{
	"Index": 21,
	"panels": 
		[
			1,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			0,
			1,
			0,
			0,
			0,
			1,
			0,
			1,
			23,
			1,
			1,
			0,
			0,
			0,
			1,
			1,
			23,
			0,
			23,
			0,
			0,
			0,
			0,
			0,
			23,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			17,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			17,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			4,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			4,
			4,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			4
		],
	"strengths": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			1,
			1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			1
		],
	"shaded": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"pieces": 
		[
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			15,
			0,
			0,
			0,
			0,
			0,
			15,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			6,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0
		],
	"colors": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			-1,
			0,
			0,
			2,
			0,
			0,
			0,
			0,
			-1,
			0,
			2,
			2,
			3,
			0,
			3,
			1,
			1,
			0,
			0,
			3,
			3,
			0,
			0,
			1,
			3,
			3,
			0
		],
	"panelInfos": 
		[
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
		],
	"startPieces": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"isTimerGame": false,
	"isMaxMovesGame": true,
	"isClearShadedGame": false,
	"isClearChocolateGame": false,
	"isGetTypesGame": false,
	"isTreasureGame": true,
	"isSpecialJewelGame": false,
	"isPenguinGame": false,
	"isYetiGame": false,
	"givenTime": 0,
	"allowedMoves": 46,
	"scoreRequiredWin": true,
	"scoreEndsGame": false,
	"scoreToReach": 
		[
			4000,
			9500,
			13500
		],
	"shadesRequiredWin": false,
	"shadesEndsGame": false,
	"chocolateRequiredWin": false,
	"chocolateEndsGame": false,
	"typesRequiredWin": false,
	"typesEndsGame": false,
	"numToGet": 
		[
			0,
			0,
			0,
			0,
			0,
			0
		],
	"treasureRequiredWin": true,
	"treasureEndsGame": true,
	"enableTreasure": true,
	"icePop": 0,
	"icecreamCone": 0,
	"maxOnScreen": 2,
	"chanceToSpawn": 8,
	"treasureGoal": 
		[
			0,
			0,
			1,
			0,
			7,
			0,
			8,
			0
		],
	"easyProbability": 
		[
			23,
			23,
			23,
			23,
			0,
			8
		],
	"normalProbability": 
		[
			22,
			22,
			22,
			22,
			0,
			11
		],
	"enableTimeBomb": false,
	"movePerTimeBomb": 0,
	"minTimeBombCount": 1,
	"maxTimeBombCount": 1,
	"defaultFallBackTime": 0,
	"fallBackTime": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"enableSpiralSnow": false,
	"movePerSpiralSnow": 5,
	"minSpiralSnowCount": 1,
	"maxSpiralSnowCount": 1,
	"specialJewels": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"maxPenguinOnScreen": 0,
	"numberOfPenguin": 0,
	"penguinSpawn": 
		[],
	"movePerPenguin": 0,
	"enableMysteryPiece": false,
	"movePerMysteryPiece": 0,
	"minMysteryPieceCount": 1,
	"maxMysteryPieceCount": 1,
	"mysteryPieceConvertProbability": 
		[
			100,
			100,
			30,
			80,
			70,
			29,
			80,
			80,
			50,
			30,
			20,
			100,
			1,
			80,
			50,
			50,
			20,
			30,
			0
		],
	"enableChameleon": false,
	"movePerChameleon": 0,
	"minChameleonPieceCount": 0,
	"maxChameleonPieceCount": 0,
	"chameleonSpawnColumn": 
		[],
	"yetiHealth": 0,
	"attackPerMove": 0,
	"yetiBallDropProbability": 0,
	"minYetiBallPieceCount": 0,
	"maxYetiBallPieceCount": 0,
	"minYetiAttackCount": 0,
	"maxYetiAttackCount": 0,
	"attackConvertProbability": 
		[
			25,
			20,
			10,
			5,
			5,
			5,
			5,
			20,
			5
		],
	"yetiBallSpawnColumn": 
		[],
	"episodeIdx": 0,
	"portalTypes": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"portalIndices": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"defaultSpawnColumn": 
		[
			1,
			2,
			3,
			4,
			5,
			6
		],
	"movePerTreasure": 2,
	"treasureSpawnCountPerMove": 1,
	"spiralSnowSpawnCountPerMove": 1,
	"timeBombSpawnCountPerMove": 0,
	"mysteryPieceSpawnCountPerMove": 0,
	"chameleonSpawnCountPerMove": 0,
	"isClearBlockerGame": false,
	"isClearCageGame": false,
	"isClearSnowGame": false,
	"isClearIcingGame": false,
	"isClearTimeBombGame": false,
	"isClearMysteryGame": false,
	"treasures": 
		[
			2,
			2,
			2,
			2,
			0
		],
	"blockers": 
		[
			0,
			0,
			0
		],
	"blockerInfos": 
		[
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			}
		],
	"treasureSpawnColumn": 
		[
			0,
			8
		],
	"isClearCakeGame": false,
	"isClearChameleonGame": false,
	"numberOfChameleon": 0,
	"isClearPieGame": false,
	"numberOfPie": 0,
	"pieceNumberForLavaCake": 0,
	"pieceColorForLavaCake": 0,
	"blockerSpawnColumn": 
		[]
}