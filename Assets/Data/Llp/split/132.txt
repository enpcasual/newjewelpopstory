{
	"Index": 26,
	"panels": 
		[
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1,
			1,
			1,
			0,
			0,
			0,
			0,
			0,
			1,
			1
		],
	"strengths": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"shaded": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"pieces": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			8,
			8,
			0,
			0,
			0,
			0,
			0,
			8,
			8,
			8,
			8,
			8,
			0,
			0,
			0,
			8,
			8,
			8,
			8,
			8,
			8,
			0,
			0,
			0,
			8,
			8,
			8,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			9,
			0,
			0,
			0,
			0
		],
	"colors": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			3,
			4,
			0,
			0,
			5,
			6,
			6,
			0,
			0,
			0,
			3,
			0,
			0,
			0,
			5,
			0,
			0,
			0,
			0,
			3,
			0,
			0,
			0,
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			6,
			0,
			0,
			0,
			0,
			0,
			2,
			3,
			6,
			4,
			6,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"panelInfos": 
		[
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
		],
	"startPieces": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"isTimerGame": false,
	"isMaxMovesGame": true,
	"isClearShadedGame": false,
	"isClearChocolateGame": false,
	"isGetTypesGame": true,
	"isTreasureGame": false,
	"isSpecialJewelGame": false,
	"isPenguinGame": true,
	"isYetiGame": false,
	"givenTime": 0,
	"allowedMoves": 32,
	"scoreRequiredWin": true,
	"scoreEndsGame": false,
	"scoreToReach": 
		[
			3400,
			6000,
			8600
		],
	"shadesRequiredWin": false,
	"shadesEndsGame": false,
	"chocolateRequiredWin": false,
	"chocolateEndsGame": false,
	"typesRequiredWin": false,
	"typesEndsGame": false,
	"numToGet": 
		[
			0,
			0,
			41,
			0,
			0,
			0
		],
	"treasureRequiredWin": false,
	"treasureEndsGame": false,
	"enableTreasure": false,
	"icePop": 0,
	"icecreamCone": 0,
	"maxOnScreen": 3,
	"chanceToSpawn": 0,
	"treasureGoal": 
		[],
	"easyProbability": 
		[
			20,
			20,
			20,
			20,
			0,
			20
		],
	"normalProbability": 
		[
			17,
			17,
			22,
			17,
			11,
			17
		],
	"enableTimeBomb": false,
	"movePerTimeBomb": 0,
	"minTimeBombCount": 1,
	"maxTimeBombCount": 1,
	"defaultFallBackTime": 10,
	"fallBackTime": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"enableSpiralSnow": false,
	"movePerSpiralSnow": 5,
	"minSpiralSnowCount": 1,
	"maxSpiralSnowCount": 1,
	"specialJewels": 
		[
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"maxPenguinOnScreen": 3,
	"numberOfPenguin": 13,
	"penguinSpawn": 
		[
			2,
			0,
			4,
			0,
			6,
			0
		],
	"movePerPenguin": 2,
	"enableMysteryPiece": false,
	"movePerMysteryPiece": 0,
	"minMysteryPieceCount": 1,
	"maxMysteryPieceCount": 1,
	"mysteryPieceConvertProbability": 
		[
			100,
			100,
			30,
			80,
			70,
			29,
			80,
			80,
			50,
			30,
			20,
			100,
			1,
			80,
			50,
			50,
			20,
			30,
			0
		],
	"enableChameleon": false,
	"movePerChameleon": 0,
	"minChameleonPieceCount": 0,
	"maxChameleonPieceCount": 0,
	"chameleonSpawnColumn": 
		[],
	"yetiHealth": 0,
	"attackPerMove": 0,
	"yetiBallDropProbability": 0,
	"minYetiBallPieceCount": 0,
	"maxYetiBallPieceCount": 0,
	"minYetiAttackCount": 0,
	"maxYetiAttackCount": 0,
	"attackConvertProbability": 
		[
			25,
			20,
			10,
			5,
			5,
			5,
			5,
			20,
			5
		],
	"yetiBallSpawnColumn": 
		[],
	"episodeIdx": 2,
	"portalTypes": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"portalIndices": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"defaultSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		],
	"movePerTreasure": 7,
	"treasureSpawnCountPerMove": 1,
	"spiralSnowSpawnCountPerMove": 1,
	"timeBombSpawnCountPerMove": 0,
	"mysteryPieceSpawnCountPerMove": 0,
	"chameleonSpawnCountPerMove": 0,
	"isClearBlockerGame": true,
	"isClearCageGame": false,
	"isClearSnowGame": false,
	"isClearIcingGame": false,
	"isClearTimeBombGame": false,
	"isClearMysteryGame": false,
	"treasures": 
		[
			0,
			0,
			0,
			0,
			0
		],
	"blockers": 
		[
			28,
			0,
			0
		],
	"blockerInfos": 
		[
			{
				"enable": true,
				"interval": 2,
				"minCount": 10,
				"maxCount": 15,
				"spawnCount": 1
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			}
		],
	"treasureSpawnColumn": 
		[],
	"isClearCakeGame": false,
	"isClearChameleonGame": false,
	"numberOfChameleon": 0,
	"isClearPieGame": false,
	"numberOfPie": 0,
	"pieceNumberForLavaCake": 0,
	"pieceColorForLavaCake": 0,
	"blockerSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		]
}