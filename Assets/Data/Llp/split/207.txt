{
	"Index": 31,
	"panels": 
		[
			0,
			1,
			1,
			1,
			23,
			1,
			1,
			1,
			0,
			0,
			1,
			0,
			1,
			0,
			1,
			0,
			1,
			0,
			24,
			1,
			24,
			1,
			0,
			1,
			24,
			1,
			24,
			1,
			0,
			23,
			1,
			17,
			1,
			23,
			0,
			1,
			0,
			17,
			0,
			1,
			0,
			1,
			0,
			17,
			0,
			0,
			1,
			17,
			1,
			0,
			1,
			17,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			22,
			22,
			22,
			22,
			0,
			22,
			22,
			22,
			22,
			22,
			22,
			22,
			22,
			0,
			22,
			22,
			22,
			22
		],
	"strengths": 
		[
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"shaded": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"pieces": 
		[
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			20,
			0,
			0,
			6,
			0,
			0,
			0,
			16,
			0,
			0,
			0,
			0,
			0,
			0,
			3,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			15,
			0,
			0,
			0,
			15,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"colors": 
		[
			-1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			-1,
			0,
			0,
			-1,
			0,
			0,
			0,
			-1,
			0,
			0,
			5,
			0,
			5,
			0,
			0,
			0,
			5,
			0,
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"panelInfos": 
		[
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"RIGHT\",\r\n\t\"Out\": \"DOWN\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"DOWN\",\r\n\t\"Out\": \"LEFT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"RIGHT\",\r\n\t\"Out\": \"DOWN\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"DOWN\",\r\n\t\"Out\": \"LEFT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"RIGHT\",\r\n\t\"Out\": \"DOWN\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"DOWN\",\r\n\t\"Out\": \"LEFT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"DOWN\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"PLATE_CLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"DOWN\",\r\n\t\"ImageType\": \"PLATE_CLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"UP\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"UP\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"UP\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"UP\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"UP\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"UP\",\r\n\t\"ImageType\": \"PLATE_COUNTERCLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"RIGHT\",\r\n\t\"Out\": \"UP\",\r\n\t\"ImageType\": \"PLATE_CLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"UP\",\r\n\t\"Out\": \"LEFT\",\r\n\t\"ImageType\": \"PLATE_CLOCKWISE\",\r\n\t\"PortalIndex\": 0\r\n}"
		],
	"startPieces": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"isTimerGame": false,
	"isMaxMovesGame": true,
	"isClearShadedGame": false,
	"isClearChocolateGame": false,
	"isGetTypesGame": false,
	"isTreasureGame": true,
	"isSpecialJewelGame": false,
	"isPenguinGame": false,
	"isYetiGame": false,
	"givenTime": 0,
	"allowedMoves": 27,
	"scoreRequiredWin": true,
	"scoreEndsGame": false,
	"scoreToReach": 
		[
			2400,
			6350,
			8000
		],
	"shadesRequiredWin": false,
	"shadesEndsGame": false,
	"chocolateRequiredWin": false,
	"chocolateEndsGame": false,
	"typesRequiredWin": false,
	"typesEndsGame": false,
	"numToGet": 
		[
			0,
			0,
			0,
			0,
			0,
			0
		],
	"treasureRequiredWin": false,
	"treasureEndsGame": false,
	"enableTreasure": false,
	"icePop": 0,
	"icecreamCone": 0,
	"maxOnScreen": 4,
	"chanceToSpawn": 0,
	"treasureGoal": 
		[
			1,
			0,
			2,
			0,
			4,
			0,
			6,
			0,
			7,
			0
		],
	"easyProbability": 
		[
			28,
			22,
			18,
			6,
			0,
			26
		],
	"normalProbability": 
		[
			27,
			22,
			17,
			9,
			0,
			25
		],
	"enableTimeBomb": false,
	"movePerTimeBomb": 0,
	"minTimeBombCount": 1,
	"maxTimeBombCount": 1,
	"defaultFallBackTime": 0,
	"fallBackTime": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"enableSpiralSnow": false,
	"movePerSpiralSnow": 5,
	"minSpiralSnowCount": 1,
	"maxSpiralSnowCount": 1,
	"specialJewels": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"maxPenguinOnScreen": 0,
	"numberOfPenguin": 0,
	"penguinSpawn": 
		[],
	"movePerPenguin": 0,
	"enableMysteryPiece": false,
	"movePerMysteryPiece": 0,
	"minMysteryPieceCount": 1,
	"maxMysteryPieceCount": 1,
	"mysteryPieceConvertProbability": 
		[
			100,
			100,
			30,
			80,
			70,
			29,
			80,
			80,
			50,
			30,
			20,
			100,
			1,
			80,
			50,
			50,
			20,
			30,
			0
		],
	"enableChameleon": false,
	"movePerChameleon": 0,
	"minChameleonPieceCount": 0,
	"maxChameleonPieceCount": 0,
	"chameleonSpawnColumn": 
		[],
	"yetiHealth": 0,
	"attackPerMove": 0,
	"yetiBallDropProbability": 0,
	"minYetiBallPieceCount": 0,
	"maxYetiBallPieceCount": 0,
	"minYetiAttackCount": 0,
	"maxYetiAttackCount": 0,
	"attackConvertProbability": 
		[
			25,
			20,
			10,
			5,
			5,
			5,
			5,
			20,
			5
		],
	"yetiBallSpawnColumn": 
		[],
	"episodeIdx": 0,
	"portalTypes": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"portalIndices": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"defaultSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		],
	"movePerTreasure": 1,
	"treasureSpawnCountPerMove": 1,
	"spiralSnowSpawnCountPerMove": 1,
	"timeBombSpawnCountPerMove": 0,
	"mysteryPieceSpawnCountPerMove": 0,
	"chameleonSpawnCountPerMove": 0,
	"isClearBlockerGame": false,
	"isClearCageGame": false,
	"isClearSnowGame": false,
	"isClearIcingGame": false,
	"isClearTimeBombGame": false,
	"isClearMysteryGame": false,
	"treasures": 
		[
			1,
			1,
			1,
			1,
			0
		],
	"blockers": 
		[
			0,
			0,
			0
		],
	"blockerInfos": 
		[
			{
				"enable": false,
				"interval": 3,
				"minCount": 14,
				"maxCount": 81,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			}
		],
	"treasureSpawnColumn": 
		[],
	"isClearCakeGame": false,
	"isClearChameleonGame": false,
	"numberOfChameleon": 0,
	"isClearPieGame": false,
	"numberOfPie": 0,
	"pieceNumberForLavaCake": 0,
	"pieceColorForLavaCake": 0,
	"blockerSpawnColumn": 
		[]
}