{
	"Index": 317,
	"panels": 
		[
			22,
			22,
			22,
			22,
			22,
			22,
			22,
			22,
			22,
			1,
			17,
			17,
			17,
			17,
			17,
			1,
			1,
			0,
			23,
			0,
			0,
			0,
			0,
			0,
			23,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			8,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			1,
			0
		],
	"strengths": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			0,
			0,
			0,
			0,
			0,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			2,
			-1,
			-1,
			-1,
			-1,
			-1,
			2,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			2,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"shaded": 
		[
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		],
	"pieces": 
		[
			5,
			8,
			8,
			8,
			5,
			8,
			8,
			8,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			10,
			0,
			0,
			0,
			0,
			0,
			10,
			0,
			8,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			8,
			0,
			0,
			8,
			8,
			8,
			0,
			0,
			0,
			8,
			0,
			0,
			0,
			10,
			0,
			0,
			0,
			0,
			8
		],
	"colors": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			-1,
			-1,
			-1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"panelInfos": 
		[
			"{\r\n\t\"Type\": \"BEGIN\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"MIDDLE\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"{\r\n\t\"Type\": \"END\",\r\n\t\"In\": \"LEFT\",\r\n\t\"Out\": \"RIGHT\",\r\n\t\"ImageType\": \"DEFAULT\",\r\n\t\"PortalIndex\": 0\r\n}",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			""
		],
	"startPieces": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"isTimerGame": false,
	"isMaxMovesGame": true,
	"isClearShadedGame": false,
	"isClearChocolateGame": false,
	"isGetTypesGame": false,
	"isTreasureGame": true,
	"isSpecialJewelGame": false,
	"isPenguinGame": false,
	"isYetiGame": false,
	"givenTime": 0,
	"allowedMoves": 39,
	"scoreRequiredWin": true,
	"scoreEndsGame": false,
	"scoreToReach": 
		[
			25000,
			362033,
			586721
		],
	"shadesRequiredWin": false,
	"shadesEndsGame": false,
	"chocolateRequiredWin": false,
	"chocolateEndsGame": false,
	"typesRequiredWin": false,
	"typesEndsGame": false,
	"numToGet": 
		[
			0,
			0,
			0,
			0,
			0,
			0
		],
	"treasureRequiredWin": false,
	"treasureEndsGame": false,
	"enableTreasure": true,
	"icePop": 0,
	"icecreamCone": 0,
	"maxOnScreen": 2,
	"chanceToSpawn": 100,
	"treasureGoal": 
		[
			8,
			0
		],
	"easyProbability": 
		[
			20,
			20,
			20,
			20,
			0,
			20
		],
	"normalProbability": 
		[
			14,
			14,
			27,
			27,
			5,
			14
		],
	"enableTimeBomb": false,
	"movePerTimeBomb": 0,
	"minTimeBombCount": 1,
	"maxTimeBombCount": 1,
	"defaultFallBackTime": 0,
	"fallBackTime": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"enableSpiralSnow": false,
	"movePerSpiralSnow": 5,
	"minSpiralSnowCount": 1,
	"maxSpiralSnowCount": 1,
	"specialJewels": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"maxPenguinOnScreen": 0,
	"numberOfPenguin": 0,
	"penguinSpawn": 
		[],
	"movePerPenguin": 0,
	"enableMysteryPiece": false,
	"movePerMysteryPiece": 0,
	"minMysteryPieceCount": 1,
	"maxMysteryPieceCount": 1,
	"mysteryPieceConvertProbability": 
		[
			60,
			0,
			30,
			150,
			50,
			29,
			30,
			0,
			0,
			0,
			0,
			30,
			0,
			0,
			0,
			0,
			20,
			0,
			0
		],
	"enableChameleon": false,
	"movePerChameleon": 0,
	"minChameleonPieceCount": 1,
	"maxChameleonPieceCount": 1,
	"chameleonSpawnColumn": 
		[],
	"yetiHealth": 0,
	"attackPerMove": 0,
	"yetiBallDropProbability": 0,
	"minYetiBallPieceCount": 0,
	"maxYetiBallPieceCount": 0,
	"minYetiAttackCount": 0,
	"maxYetiAttackCount": 0,
	"attackConvertProbability": 
		[
			25,
			20,
			10,
			5,
			5,
			5,
			5,
			20,
			5
		],
	"yetiBallSpawnColumn": 
		[],
	"episodeIdx": 1,
	"portalTypes": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"portalIndices": 
		[
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		],
	"defaultSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		],
	"movePerTreasure": 1,
	"treasureSpawnCountPerMove": 0,
	"spiralSnowSpawnCountPerMove": 1,
	"timeBombSpawnCountPerMove": 0,
	"mysteryPieceSpawnCountPerMove": 0,
	"chameleonSpawnCountPerMove": 0,
	"isClearBlockerGame": false,
	"isClearCageGame": false,
	"isClearSnowGame": false,
	"isClearIcingGame": false,
	"isClearTimeBombGame": false,
	"isClearMysteryGame": false,
	"treasures": 
		[
			2,
			0,
			0,
			0,
			0
		],
	"blockers": 
		[
			0,
			0,
			0
		],
	"blockerInfos": 
		[
			{
				"enable": true,
				"interval": 3,
				"minCount": 1,
				"maxCount": 81,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			},
			{
				"enable": false,
				"interval": 0,
				"minCount": 1,
				"maxCount": 1,
				"spawnCount": 0
			}
		],
	"treasureSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		],
	"isClearCakeGame": false,
	"isClearChameleonGame": false,
	"numberOfChameleon": 0,
	"isClearPieGame": false,
	"numberOfPie": 0,
	"pieceNumberForLavaCake": 0,
	"pieceColorForLavaCake": 0,
	"blockerSpawnColumn": 
		[
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8
		]
}