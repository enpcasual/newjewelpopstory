﻿#region 어셈블리 lollipopmatch3, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// D:\FoodCar\ThreeMatchs\Lollipop\Lollipop_4\Assets\Data\lib\lollipopmatch3.dll
#endregion

using JsonFx.Json;
public enum LEVEL_TYPE
{
    RED = 0,
    YELLOW = 1,
    GREEN = 2,
    BLUE = 3,
    PURPLE = 4,
    ORANGE = 5,
    PENGUIN = 6,
    PURPLE2 = 7,
    YETI = 8,
    CAKE = 9,
    CHAMELEON = 10
}
public class Level
{
    public int allowedMoves;                //이동 제한 수
    public int[] attackConvertProbability;  //16번 미션에서 사용됨.
    public int attackPerMove;               //16번 미션에서 사용됨
    public ImpressionInfo[] blockerInfos;
    public int[] blockers;                  //3번 미션이랑 같이 사용됨.
    public int[] blockerSpawnColumn;
    public int[] chameleonSpawnColumn;
    public int chameleonSpawnCountPerMove;
    public int chanceToSpawn;
    public bool chocolateEndsGame;
    public bool chocolateRequiredWin;
    public int[] colors;                    //컬러
    public int defaultFallBackTime;
    public int[] defaultSpawnColumn;        //아이템소환되는 라인
    public int[] easyProbability;
    public bool enableChameleon;
    public bool enableMysteryPiece;
    public bool enableSpiralSnow;
    public bool enableTimeBomb;
    public bool enableTreasure;                 //13번 미션에서 사용. 생성기를 사용한다면 false
    public int episodeIdx;
    public int[] fallBackTime;
    public float givenTime;
    public int icecreamCone;
    public int icePop;
    public bool isClearBlockerGame;                     //3.Mission : Eliminate Around Blocker
    public bool isClearCageGame;                        //4.Mission : Eliminate Cage
    public bool isClearCakeGame;                        //9.Mission : Eliminate Cake
    public bool isClearChameleonGame;                   //10.Mission : Eliminate Chameleon
    public bool isClearChocolateGame;                   //2.Mission : Eliminate Snowman
    public bool isClearIcingGame;                       //6.Mission : liminate Icing
    public bool isClearMysteryGame;                     //8.Mission : Eliminate Mystery
    public bool isClearPieGame;                         //11.Mission : Eliminate Pie
    public bool isClearShadedGame;                      //1.Mission : Eliminate Shaded
    public bool isClearSnowGame;                        //5.Mission : Eliminate Snow
    public bool isClearTimeBombGame;                    //7.Mission : Eliminate TimeBomb
    public bool isGetTypesGame;                         //12.Mission : Collect Jewel
    public bool isMaxMovesGame;
    public bool isPenguinGame;                          //15.Mission : Eliminate Penguin
    public bool isSpecialJewelGame;                     //14.Mission : Collect Special Jewel
    public bool isTimerGame;
    public bool isTreasureGame;                         //13.Mission : Collect Ingredient
    public bool isYetiGame;                             //16.Mission : Eliminate Yeti
    public string key;
    public int maxChameleonPieceCount;
    public int maxMysteryPieceCount;
    public int maxOnScreen;                             //13번 미션에서 사용
    public int maxPenguinOnScreen;
    public int maxSpiralSnowCount;
    public int maxTimeBombCount;
    public int maxYetiAttackCount;              //16번 미션에서 사용됨.
    public int maxYetiBallPieceCount;           //16번 미션에서 사용됨.
    public int minChameleonPieceCount;
    public int minMysteryPieceCount;
    public int minSpiralSnowCount;
    public int minTimeBombCount;
    public int minYetiAttackCount;              //16번 미션에서 사용됨
    public int minYetiBallPieceCount;           //16번 미션에서 사용됨.
    public int movePerChameleon;
    public int movePerMysteryPiece;
    public int movePerPenguin;
    public int movePerSpiralSnow;
    public int movePerTimeBomb;
    public int movePerTreasure;                 //13번 미션에서 사용
    public int[] mysteryPieceConvertProbability;
    public int mysteryPieceSpawnCountPerMove;
    public int[] normalProbability;             //출현 컬러값 확률.
    public int numberOfChameleon;               //10번 미션에서 같이 사용됨.
    public int numberOfPenguin;                 //15번 미션에서 같이 사용됨.
    public int numberOfPie;                     //11번 미션에서 같이 사용됨.
    public int[] numToGet;                      //12번 미션에서 같이 사용됨.
    public string[] panelInfos;
    public int[] panels;                        //패널정보
    public int[] penguinSpawn;
    public int pieceColorForLavaCake;
    public int pieceNumberForLavaCake;
    public int[] pieces;                        //아이템정보
    public int[] portalIndices;                 //워프의 매칭 인덱스
    public int[] portalTypes;
    public bool scoreEndsGame;
    public bool scoreRequiredWin;
    public int[] scoreToReach;
    public int[] shaded;                        //아이스 미션
    public bool shadesEndsGame;
    public bool shadesRequiredWin;
    public int[] specialJewels;                 //14번 미션에서 사용
    public int spiralSnowSpawnCountPerMove;
    public bool[] startPieces;                  //아이템이 없는곳을 이걸로 처리함. == ItemType.None
    public int[] strengths;                     //패널에 효과능력치.
    public Theme theme;
    public int timeBombSpawnCountPerMove;
    public bool treasureEndsGame;
    public int[] treasureGoal;
    public bool treasureRequiredWin;
    public int[] treasures;                     //13번 미션  재료모으기 미션5개종류
    public int[] treasureSpawnColumn;           //13번 미션  재료 스폰 라인..
    public int treasureSpawnCountPerMove;       //13번 미션에서 사용
    public bool typesEndsGame;
    public bool typesRequiredWin;
    public int yetiBallDropProbability;         //16번 미션에서 사용.
    public int[] yetiBallSpawnColumn;
    public int yetiHealth;                      //16번미션에서 사용.

    //public Level();

    public int CageCount { get; set; }
    public int CakeCount { get; set; }
    public int ChameleonCount { get; set; }
    public int GlobalIndex { get; set; }
    public int IcingCount { get; set; }
    public int Index { get; set; }
    public int MysteryCount { get; set; }
    public Level Next { get; set; }
    public bool Opened { get; set; }
    public int ShadeCount { get; set; }
    public int SnowCount { get; set; }
    public int SnowmanCount { get; set; }
    public int SpiralSnowCount { get; set; }
    [JsonIgnore]
    public int StraightFailCount { get; set; }
    public int TimeBombCount { get; set; }
    public LEVEL_TYPE Type { get; set; }

    //public void Initialize(int index, Theme _theme);

    public class Record
    {
        //public Record();

        public bool Cleared { get; set; }
        public int Grade { get; set; }
        public int Score { get; set; }
        public int StraightFailCount { get; set; }
    }
}
