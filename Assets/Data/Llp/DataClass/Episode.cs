﻿#region 어셈블리 lollipopmatch3, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// D:\FoodCar\ThreeMatchs\Lollipop\Lollipop_4\Assets\Data\lib\lollipopmatch3.dll
#endregion

using System.Collections.Generic;
using JsonFx.Json;

public class Episode
{
    public string count;
    public int Index;
    public string key;
    [JsonIgnore]
    public List<Level> levels;
    public int requireStarCount;
    public int rewardCoin;

    //public Episode();

    public class Record
    {
        //public Record();

        public bool GainedReward { get; set; }
        public bool IsOpened { get; set; }
    }
}
