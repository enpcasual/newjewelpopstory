﻿#region 어셈블리 lollipopmatch3, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// D:\FoodCar\ThreeMatchs\Lollipop\Lollipop_4\Assets\Data\lib\lollipopmatch3.dll
#endregion

using JsonFx.Json;


    public class Theme1
    {
        public int baseIndex;
        public Episode[] episodes;
        public string id;
        public bool isHardcore;
        public string key;
        //[JsonFileLink("lv_$id$")]
        public Level[] levels;
        public string name;
        public int requireCoinCount;
        public int requireStarCount;
        public int rewardCoin;

        //public Theme();

        public bool Cleared { get; set; }
        public int ClearedLevelCount { get; set; }
        public int Index { get; set; }
        public int LevelCount { get; set; }
        public Theme Next { get; set; }
        public int StarCount { get; set; }
        public int TotalScore { get; set; }

        //public void Initialize(int index, int _baseIndex, Root _root);
        //public bool IsOpendLevel(Level level);

        public class Record
        {
            //public Record();

            public bool GainedReward { get; set; }
            public bool IsOpened { get; set; }
        }
    }
