{
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 31\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 19\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 19\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 31\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 26\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 26\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 19\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 19\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 31\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 26\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 26\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 31\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 36\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 36\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 36\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 36\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 44\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 44\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 44\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 44\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Start\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 49\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 66\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 66\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 49\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 69\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 69\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 66\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 66\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 49\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 69\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 69\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"End\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 49\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Ring",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			}
		],
	"items": 
		[
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"Donut",
			"Donut",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"Donut",
			"Donut",
			"Donut",
			"Donut",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"Donut",
			"Donut",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"Donut",
			"Donut",
			"Normal",
			"Donut",
			"Donut",
			"None",
			"None",
			"None",
			"None",
			"Donut",
			"Donut",
			"Normal",
			"Donut",
			"Donut",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None"
		],
	"colors": 
		[
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"YELLOW",
			"YELLOW",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"BLUE",
			"None",
			"YELLOW",
			"Rnd",
			"YELLOW",
			"Rnd",
			"None",
			"BLUE",
			"BLUE",
			"Rnd",
			"BLUE",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None"
		],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 10000,
	"scoreStar2": 13000,
	"scoreStar3": 23386,
	"limit_Move": 29,
	"missionType": "OrderN",
	"isOrderNMission": true,
	"isOrderSMission": true,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Spiral",
				"count": 40
			},
			{
				"type": "OrderS",
				"kind": "Line",
				"count": 30
			}
		],
	"food_MaxExist": 0,
	"food_Interval": 0,
	"food_SpawnCnt": 0,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 1,
	"Donut_MaxExist": 8,
	"Donut_Interval": 1,
	"Donut_SpawnCnt": 2,
	"TimeBomb_MinExist": 1,
	"TimeBomb_MaxExist": 81,
	"TimeBomb_Interval": 5,
	"TimeBomb_SpawnCnt": 0,
	"TimeBomb_FirstCount": 9,
	"Bear_MaxExist": 0,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 1,
	"Mystery_MaxExist": 81,
	"Mystery_Interval": 5,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 10,
	"Chameleon_MaxExist": 81,
	"Chameleon_Interval": 1,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 0,
	"Key_MaxExist": 0,
	"Key_Interval": 0,
	"Key_SpawnCnt": 0,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Spiral",
			"Spiral",
			"TimeBomb",
			"Line_X"
		],
	"JewelTreeItemColor": 
		[
			"None",
			"None",
			"Rnd",
			"Rnd"
		]
}