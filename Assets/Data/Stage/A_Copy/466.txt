﻿{
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 30\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 30\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Cracker",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 33\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 33\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 30\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 30\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Cracker",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 33\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 33\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Cracker",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 47\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 47\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Cracker",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 60\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 60\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 47\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 47\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Cracker",
							"defence": 7,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 60\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 60\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_A",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_B",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_C",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "JewelTree_D",
							"defence": 1,
							"value": 0
						}
					]
			}
		],
	"items": 
		[
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"colors": 
		[
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 4000,
	"scoreStar2": 17420,
	"scoreStar3": 21775,
	"limit_Move": 33,
	"missionType": "OrderS",
	"isOrderNMission": true,
	"isOrderSMission": true,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Red",
				"count": 130
			},
			{
				"type": "OrderN",
				"kind": "Yellow",
				"count": 130
			},
			{
				"type": "OrderS",
				"kind": "Cross",
				"count": 50
			}
		],
	"food_MaxExist": 0,
	"food_Interval": 0,
	"food_SpawnCnt": 0,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 0,
	"Donut_MaxExist": 0,
	"Donut_Interval": 0,
	"Donut_SpawnCnt": 0,
	"TimeBomb_MinExist": 1,
	"TimeBomb_MaxExist": 81,
	"TimeBomb_Interval": 5,
	"TimeBomb_SpawnCnt": 0,
	"TimeBomb_FirstCount": 9,
	"Bear_MaxExist": 0,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 1,
	"Mystery_MaxExist": 81,
	"Mystery_Interval": 5,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 81,
	"Chameleon_Interval": 5,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 0,
	"Key_MaxExist": 0,
	"Key_Interval": 0,
	"Key_SpawnCnt": 0,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Line_C",
			"Normal",
			"Line_C",
			"Normal"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}