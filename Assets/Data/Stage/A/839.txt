﻿{
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Right\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Left\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 38\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Bottom\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Bottom\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Top\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Right\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "ConveyerBelt",
							"defence": -1,
							"value": 0,
							"addData": "{\r\n\t\"Type\": \"Mid\",\r\n\t\"Dir_In\": \"Top\",\r\n\t\"Dir_Out\": \"Left\",\r\n\t\"WaropIndex\": 10\r\n}"
						}
					]
			}
		],
	"items": 
		[
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"Normal",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal"
		],
	"colors": 
		[
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"Rnd",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 4500,
	"scoreStar2": 8000,
	"scoreStar3": 10000,
	"limit_Move": 30,
	"missionType": "OrderS",
	"isOrderNMission": true,
	"isOrderSMission": true,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "OrderN",
				"kind": "Red",
				"count": 80
			},
			{
				"type": "OrderS",
				"kind": "Line",
				"count": 15
			}
		],
	"food_MaxExist": 7,
	"food_Interval": 1,
	"food_SpawnCnt": 3,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 0,
	"Donut_MaxExist": 0,
	"Donut_Interval": 0,
	"Donut_SpawnCnt": 0,
	"TimeBomb_MinExist": 20,
	"TimeBomb_MaxExist": 50,
	"TimeBomb_Interval": 0,
	"TimeBomb_SpawnCnt": 20,
	"TimeBomb_FirstCount": 10,
	"Bear_MaxExist": 8,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 0,
	"Mystery_Interval": 0,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 0,
	"Chameleon_Interval": 0,
	"Chameleon_SpawnCnt": 0,
	"Key_MinExist": 3,
	"Key_MaxExist": 3,
	"Key_Interval": 3,
	"Key_SpawnCnt": 1,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Rainbow",
			"Line_X",
			"Line_Y",
			"Donut"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}