﻿{
	"DropDirs": 
		[
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"D"
			],
			[
				"R"
			],
			[
				"R"
			],
			[
				"R"
			],
			[
				"R"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"D"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"D"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"D"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"D"
			],
			[
				"U"
			],
			[
				"U",
				"L"
			],
			[
				"U",
				"L"
			],
			[
				"U",
				"L"
			]
		],
	"isUseGravity": true,
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 14
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 1
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 2
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 3
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 1
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 2
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 3
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 15
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 4
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 5
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 6
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 7
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 16
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 17
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "FoodArrive",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 16
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 15
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 4
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 5
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 6
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 7
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 8
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 9
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 10
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 11
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 8
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 9
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 10
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 11
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_Out",
							"defence": -1,
							"value": 17
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Wafer_floor",
							"defence": 1,
							"value": 0
						},
						{
							"paneltype": "Warp_In",
							"defence": -1,
							"value": 14
						}
					]
			}
		],
	"items": 
		[
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal"
		],
	"colors": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"DonutSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"ChameleonSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"KeySpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"defaultSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"SpiralSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"DonutSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"TimeBombSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"MysterySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"ChameleonSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"KeySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			true
		],
	"scoreStar1": 70000,
	"scoreStar2": 90000,
	"scoreStar3": 100000,
	"limit_Move": 28,
	"missionType": "Wafer",
	"isOrderNMission": false,
	"isOrderSMission": false,
	"isWaferMission": true,
	"isFoodMission": true,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[
			{
				"type": "Wafer",
				"kind": "Wafer",
				"count": 0
			},
			{
				"type": "Food",
				"kind": "ChocolatePiece",
				"count": 1
			},
			{
				"type": "Food",
				"kind": "Parfait",
				"count": 1
			},
			{
				"type": "Food",
				"kind": "WhiteCake",
				"count": 1
			}
		],
	"food_MaxExist": 1,
	"food_Interval": 1,
	"food_SpawnCnt": 1,
	"Spiral_MinExist": 2,
	"Spiral_MaxExist": 4,
	"Spiral_Interval": 1,
	"Spiral_SpawnCnt": 2,
	"Donut_MinExist": 5,
	"Donut_MaxExist": 8,
	"Donut_Interval": 1,
	"Donut_SpawnCnt": 4,
	"TimeBomb_MinExist": 1,
	"TimeBomb_MaxExist": 81,
	"TimeBomb_Interval": 5,
	"TimeBomb_SpawnCnt": 0,
	"TimeBomb_FirstCount": 20,
	"Bear_MaxExist": 0,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Basic",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 0,
	"Mystery_Interval": 0,
	"Mystery_SpawnCnt": 0,
	"Chameleon_MinExist": 8,
	"Chameleon_MaxExist": 20,
	"Chameleon_Interval": 2,
	"Chameleon_SpawnCnt": 3,
	"Key_MinExist": 0,
	"Key_MaxExist": 0,
	"Key_Interval": 0,
	"Key_SpawnCnt": 0,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Line_X",
			"Line_X",
			"Line_X",
			"Line_X"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}