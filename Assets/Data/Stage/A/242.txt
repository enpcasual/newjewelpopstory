﻿{
	"DropDirs": 
		[
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"U"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"U"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			],
			[
				"L"
			]
		],
	"isUseGravity": true,
	"panels": 
		[
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 9
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 9
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 9
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 9
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 27
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 27
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Empty",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 27
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 27
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 21
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 45
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 45
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 42
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 45
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 3,
							"value": 45
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 2,
							"value": 64
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 5,
							"value": 48
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 2,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 3,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 4,
							"value": 60
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 2,
							"value": 64
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 4,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 1,
							"value": 75
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele",
							"defence": 1,
							"value": 75
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 2,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Stele_Hide",
							"defence": 3,
							"value": 0
						},
						{
							"paneltype": "Bread_Block",
							"defence": 5,
							"value": 0
						}
					]
			},
			{
				"listinfo": 
					[
						{
							"paneltype": "Default_Full",
							"defence": -1,
							"value": 0
						},
						{
							"paneltype": "Fixed_Block",
							"defence": -1,
							"value": 0
						}
					]
			}
		],
	"items": 
		[
			"None",
			"Mystery",
			"Chameleon",
			"Mystery",
			"Chameleon",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"Normal",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Normal",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"colors": 
		[
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"Rnd",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"Rnd",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None",
			"None"
		],
	"focus": 
		[],
	"defaultSpawnLine": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"SpiralSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"DonutSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"TimeBombSpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"MysterySpawnLine": 
		[
			false,
			true,
			false,
			true,
			false,
			true,
			false,
			true,
			false
		],
	"ChameleonSpawnLine": 
		[
			true,
			false,
			true,
			false,
			true,
			false,
			true,
			false,
			true
		],
	"KeySpawnLine": 
		[
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
	"defaultSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"foodSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"SpiralSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"DonutSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"TimeBombSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"MysterySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"ChameleonSpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"KeySpawnLineY": 
		[
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		],
	"appearColor": 
		[
			true,
			true,
			true,
			true,
			true,
			false
		],
	"scoreStar1": 11521,
	"scoreStar2": 26884,
	"scoreStar3": 34565,
	"limit_Move": 32,
	"missionType": "Stele",
	"isOrderNMission": false,
	"isOrderSMission": false,
	"isWaferMission": false,
	"isFoodMission": false,
	"isBearMission": false,
	"isIceCreamMission": false,
	"isJamMission": false,
	"missionInfo": 
		[],
	"food_MaxExist": 0,
	"food_Interval": 0,
	"food_SpawnCnt": 0,
	"Spiral_MinExist": 0,
	"Spiral_MaxExist": 0,
	"Spiral_Interval": 0,
	"Spiral_SpawnCnt": 0,
	"Donut_MinExist": 0,
	"Donut_MaxExist": 0,
	"Donut_Interval": 0,
	"Donut_SpawnCnt": 0,
	"TimeBomb_MinExist": 0,
	"TimeBomb_MaxExist": 0,
	"TimeBomb_Interval": 0,
	"TimeBomb_SpawnCnt": 0,
	"TimeBomb_FirstCount": 20,
	"Bear_MaxExist": 0,
	"Bear_Interval": 0,
	"IceCream_Interval": 1,
	"IceCreamCreator_Interval": 1,
	"Mystery_SettingType": "Mystery_Easy",
	"Mystery_MinExist": 0,
	"Mystery_MaxExist": 8,
	"Mystery_Interval": 1,
	"Mystery_SpawnCnt": 1,
	"Chameleon_MinExist": 0,
	"Chameleon_MaxExist": 10,
	"Chameleon_Interval": 1,
	"Chameleon_SpawnCnt": 2,
	"Key_MinExist": 0,
	"Key_MaxExist": 0,
	"Key_Interval": 0,
	"Key_SpawnCnt": 0,
	"S_Tree_SettingType": "Basic",
	"JewelTreeItem": 
		[
			"Line_X",
			"Line_X",
			"Line_X",
			"Line_X"
		],
	"JewelTreeItemColor": 
		[
			"Rnd",
			"Rnd",
			"Rnd",
			"Rnd"
		]
}