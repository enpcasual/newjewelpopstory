#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("7xxQClbrsxVYBJuN1YU/uSpWdfU0Td3UXrBwRHH8Kss23pZbJSVOHlgD7W8lXNpkMc0ECQCcr6YB4gAVauSsuCNQ82svwQ6ABOMMjzvK0YkJuzgbCTQ/MBO/cb/ONDg4ODw5OsZ3vWhEDV+MrbF3uwn1bmhlHQNKuzg2OQm7ODM7uzg4Ob8c1ie37A/69WYD+ipCd0aAVIoiaHrNsyYUMlMI+3Mnwzaxc1SA89IMhhax0YKFO/H4imX9mcVyYbyKC8Wz4soiny7FruzF59cWZESuVYh1aWzy5aF9deMx5U3tcpzrYSzYJC13wJrMgyIzStF7X6pR3Qc3gUciAK8jRHieHxr2ESHcnqqIU38gII4OsCyDp5s7zJsYEKfIYDd5Zjs6ODk4");
        private static int[] order = new int[] { 11,13,8,12,11,5,13,10,11,13,10,13,13,13,14 };
        private static int key = 57;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
