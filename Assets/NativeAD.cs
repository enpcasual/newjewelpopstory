﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

public class NativeAD : MonoBehaviour {
    
    private void OnEnable()
    {
        Init();
    }

    private void OnDisable()
    {
        AdMob_Manager.Instance.RequestNativeAd();
    }

    [SerializeField]
    private UITexture m_Icon = null;
    [SerializeField]
    private UITexture m_Logo = null;
    [SerializeField]
    private UITexture m_Image = null;
    [SerializeField]
    private UILabel m_Headline = null;
    [SerializeField]
    private UILabel m_Body = null;
    [SerializeField]
    private UILabel m_CallAction = null;
    [SerializeField]
    List<GameObject> m_Images = null;

    public void Init()
    {
        if (AdMob_Manager.Instance.nativeAd != null)
        {
            // Get Texture2D for icon asset of native ad.
            m_Icon.mainTexture = AdMob_Manager.Instance.nativeAd.GetIconTexture();
            m_Logo.mainTexture = AdMob_Manager.Instance.nativeAd.GetAdChoicesLogoTexture();
            m_Image.mainTexture = AdMob_Manager.Instance.nativeAd.GetImageTextures()[0];
            // Get string for headline asset of native ad.
            m_Headline.text = AdMob_Manager.Instance.nativeAd.GetHeadlineText();
            m_Body.text = AdMob_Manager.Instance.nativeAd.GetBodyText();
            m_CallAction.text = AdMob_Manager.Instance.nativeAd.GetCallToActionText();

            if (!AdMob_Manager.Instance.nativeAd.RegisterIconImageGameObject(m_Icon.gameObject))
            {
                Debug.Log("failure : RegisterIconImageGameObject");
            }

            if (!AdMob_Manager.Instance.nativeAd.RegisterHeadlineTextGameObject(m_Headline.gameObject))
            {
                Debug.Log("failure : RegisterHeadlineTextGameObject");
            }

            if (!AdMob_Manager.Instance.nativeAd.RegisterBodyTextGameObject(m_Body.gameObject))
            {
                Debug.Log("failure : RegisterBodyTextGameObject");
            }

            if (!AdMob_Manager.Instance.nativeAd.RegisterCallToActionGameObject(m_CallAction.gameObject))
            {
                Debug.Log("failure : RegisterCallToActionGameObject");
            }
            m_Images.Clear();
            m_Images = new List<GameObject>();
            m_Images.Add(m_Image.gameObject);
            AdMob_Manager.Instance.nativeAd.RegisterImageGameObjects(m_Images);

            if (!AdMob_Manager.Instance.nativeAd.RegisterAdChoicesLogoGameObject(m_Logo.gameObject))
            {
                Debug.Log("failure : RegisterAdChoicesLogoGameObject");
            }
        }
    }
}
