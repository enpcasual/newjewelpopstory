//
//  V2R_Plugin_iOS.h
//  V2R_Plugin_iOS
//
//  Created by Baek JiSeong on 2017. 7. 13..
//  Copyright © 2017년 Baek JiSeong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2R_Plugin_iOS : NSObject

- (NSString*)GetLanguage_iOS;
- (NSString*)GetCountry_iOS_ISO_3166_1_alpha_2;

@end
