//
//  V2R_Plugin_iOS.m
//  V2R_Plugin_iOS
//
//  Created by Baek JiSeong on 2017. 7. 13..
//  Copyright © 2017년 Baek JiSeong. All rights reserved.
//

#import "ENP_Plugin_iOS.h"
#import <CloudKit/CloudKit.h>
#import "UnityAppController.h"

/*
@implementation V2R_Plugin_iOS

@end
*/

const char* GetLanguage_iOS() {
    //languages구하는 다른방법.
    //NSUserDefaults *defaluts = [NSUserDefaults standardUserDefaults];
    //NSArray *arry_language = [defaluts objectForKey:@"AppleLanguages"];
    //NSString *language = [arry_languages objectAtIndex:0];
    
    //iOS 9 이후부터 "en-US", "ko-KR" 처럼 얻어진다. 한국에서 중국어로 바꾸면 "zh-Hans-KR" 처럼 나온다.
    //그래서 NSDictionary를 추가로 얻어야 한다.
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    NSString *scriptCode = [languageDic objectForKey:@"kCFLocaleScriptCodeKey"];
    //안드로이드랑 다르게 현재 위치가 나오기 때문에 사용하지 않는다.
    //NSString *countryCode = [languageDic objectForKey:@"kCFLocaleCountryCodeKey"];
    if([scriptCode isEqualToString:@"Hans"]) //중국, 간체
    {
        languageCode = [languageCode stringByAppendingString:@"_CN"];
    }
    else if([scriptCode isEqualToString:@"Hant"]) //대만, 번체
    {
        languageCode = [languageCode stringByAppendingString:@"_TW"];
    }
    
    
    NSLog(@"언어 출력!! %@ :", languageCode);
    
    //UnitySendMessage("Native_Brige", "PluginLog", "aa");
    //[language cStringUsingEncoding:NSUTF8StringEncoding]);//MakeStringCopy([language UTF8String]));
    return [languageCode cStringUsingEncoding:NSUTF8StringEncoding];
}

const char* GetCountry_iOS_ISO_3166_1_alpha_2()
{
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    
    return [countryCode cStringUsingEncoding:NSUTF8StringEncoding];
}

//ios에서 unity로 string을 넘길때 문제가 발생한다.
//1.첫번째 방법은 아래 함수를 Plugin쪽에서 사용해서 넘기는 방법
//2.두번째 방법은 unity c#쪽에서 intptr로 받는 방법
char * MakeStringCopy(const char * str)
{
    
    if( str == NULL )
        
        return NULL;
    
    char * res = (char *)malloc( strlen( str ) + 1 );
    
    strcpy( res, str );
    
    return res;
    
}

const char* GetCloudData_iOS(const char* key) {
    
    NSString *data;
    
    //*******************************************************
    //CloudKit은 개별 클라우드 공간이 아닌 앱의 공용 클라우드를 사용한다.
    //*******************************************************
    //    //컨테이너 이름을 잘못만들어서 디폴트컨테이너로 사용못함.
    //    //CKContainer *defaultContainer = [CKContainer defaultContainer];
    //    CKContainer *defaultContainer = [CKContainer containerWithIdentifier:@"iCloud.daerisoft.foodcrush"];
    //    CKDatabase *database = [defaultContainer privateCloudDatabase];
    //
    //    NSPredicate *predicate =[NSPredicate predicateWithValue:YES]; //항상 true
    //    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"SaveData" predicate:predicate];
    //
    //
    //    [database performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error) {
    //        if (!error) {
    //            //[results objectAtIndex:0];
    //            NSLog(@"BJS : 로드 성공 %@", results);
    //        } else {
    //            NSLog(@"BJS : 로드 실패 %@", error);
    //        }
    //    }
    //     ];
    
    
    //ios8.0 이상
//    [[CKContainer defaultContainer] accountStatusWithCompletionHandler:^(CKAccountStatus accountStatus, NSError * _Nullable error){
//        
//        switch (accountStatus) {
//                //계정 상태를 가져올 때 오류가 발생했습니다. 해당 NSError를 참조하십시오.
//            case CKAccountStatusCouldNotDetermine:
//                break;
//                //이 응용 프로그램에서 iCloud 계정 자격 증명을 사용할 수 있습니다.
//            case CKAccountStatusAvailable:
//                break;
//                //자녀 보호 / 장치 관리가 iCloud 계정 자격증 명에 대한 액세스를 거부했습니다
//            case CKAccountStatusRestricted:
//                break;
//                //이 장치에 iCloud 계정이 로그인되어 있지 않습니다.
//            case CKAccountStatusNoAccount:
//                break;
//            default:
//                break;
//        }
//    }];

    
    NSUbiquitousKeyValueStore *cloudStore = [NSUbiquitousKeyValueStore defaultStore];
    data = [cloudStore stringForKey:[NSString stringWithUTF8String:key]];
    
    if(data == NULL)
    {
        NSLog(@"BJS : 로드 실패");
        data = @"None";
    }
    else
    {
        NSLog(@"BJS : 로드 성공 %s : %@", key, data);
    }
    
    
    return [data cStringUsingEncoding:NSUTF8StringEncoding];
}

bool SetCloudData_iOS(const char* key, const char* uuid) {
    
    //   NSString *res;
    
    //    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:@"001"];
    //    CKRecord *postRecrod = [[CKRecord alloc] initWithRecordType:@"SaveData" recordID:recordID];
    //    postRecrod[@"uuid"] = [NSString stringWithUTF8String:uuid];
    //
    //    //CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    //    CKContainer *defaultContainer = [CKContainer containerWithIdentifier:@"iCloud.daerisoft.foodcrush"];
    //    CKDatabase *database = [defaultContainer privateCloudDatabase];
    //    [database saveRecord:postRecrod completionHandler:^(CKRecord *record, NSError *error) {
    //        if(error) {
    //            NSLog(@"BJS : 세이브 실패 %@", error);
    //        } else {
    //            NSLog(@"BJS : 세이브 성공");
    //        }
    //    }];
    //     return [res cStringUsingEncoding:NSUTF8StringEncoding];
    
    NSUbiquitousKeyValueStore *cloudStore = [NSUbiquitousKeyValueStore defaultStore];
    [cloudStore setString : [NSString stringWithUTF8String:uuid] forKey:[NSString stringWithUTF8String:key]];
    
    bool ok = [cloudStore synchronize];
    if(ok)
    {
        NSLog(@"BJS : 세이브 성공");
    }else{
        NSLog(@"BJS : 세이브 실패");
    }
    
    return ok;
}

 void NSLog_iOS(const char* log) {
       
    NSLog(@"%@", [NSString stringWithUTF8String:log]);

}
