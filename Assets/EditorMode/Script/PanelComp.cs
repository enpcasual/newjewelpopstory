﻿using UnityEngine;
using System.Collections;

public enum PanelSort
{
    B_Default,  //패널 기본

    B_Bottom,   //패널 바닥(와퍼)

    I_Back,     //아이템보다 뒤쪽

    I_Front,    //아이템보다 앞쪽

    B_Upper,    //패널 맨위
}

public class PanelComp : Comp
{
    public PanelType m_PanelType;
    public PanelSort m_PanelSort;
    public int Defence;

    //1. CompManager에서 Comp를 찾을때 PanelType과 Defence값으로만 찾으므로 
    //   패널을 구분하는 용도로 사용 불가.
    //2. Editor상에서 추가되는 값을 저장하기 위한 용도로 구현되어있으므로 
    //   Comp의 Inspector에 값을 입력해서 사용할 수 없음.
    //   예) 로드할때) 데이타의 Value -> Comp의 Value -> EBoard의 m_WPanelValue
    //       저장할때) EBoard의 m_WPanelValue -> 데이타의 Value
    //결국 요약하면 이 변수는 Defence처럼 정해진 값이 아니라 
    //     워프의 숫자처럼  Editor상에서 추가변경되는 가변적인 값을 저장할때 사용한다.
    [HideInInspector] public int Value;

}
