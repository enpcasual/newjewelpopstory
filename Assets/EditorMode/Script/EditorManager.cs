﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using JsonFx.Json;
using UnityEngine.UI;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class EditorManager : MonoBehaviour
{
    public static EditorManager Instance;

    //******************
    //인스펙터 세팅
    //*****************
    public GameObject EBoard_Prefab;

    //******************
    //자동 세팅
    //******************
    public Text TextInfo;
    public Transform EditorField;
    float CellWidth = 80;
    float CellHight = 80;
    public List<Toggle> List_Toggle_Default;
    public List<Toggle> List_Toggle_Food;
    public List<Toggle> List_Toggle_Spiral;
    public List<Toggle> List_Toggle_Donut;
    public List<Toggle> List_Toggle_TimeBomb;
    public List<Toggle> List_Toggle_Mystery;
    public List<Toggle> List_Toggle_Chameleon;
    public List<Toggle> List_Toggle_Key;

    public Toggle ToggleSwitch;
    public Text ToggleSwitchText;

    //******************
    //Variable
    //*******************
    public List<EBoard> m_ListEBoard;
    public Stage CSD;
    private Comp m_SelectComp;
    public Comp SelectComp
    {
        get
        {
            return m_SelectComp;
        }
        set
        {
            if (value as DropComp)
            {
                foreach (var eBoard in m_ListEBoard)
                {
                    eBoard.Drop_EBoard.gameObject.SetActive(true);
                }
            }
            else
            {
                foreach (var eBoard in m_ListEBoard)
                {
                    eBoard.Drop_EBoard.gameObject.SetActive(false);
                }
            }

            m_SelectComp = value;
        }
    }


    /// <summary>
    /// 세이브 관련 데이타
    /// </summary>
    public int m_Set;
    public int m_Stage;

    //Editor 관련 데이타
    public static string DataSetKey = "SelectDataSet";
    public static string SaveStageKey = "EditingStage";
    public static string EditorToPlayKey = "EditorToPlay";

    void Awake()
    {
        Instance = this;

        TextInfo = transform.Find("TextInfo").GetComponent<Text>();

        EditorField = transform.Find("EditorField");
        //상단 체크박스
        Transform CheckGroups = transform.Find("CheckGroups"); //체크 그룹의 Root

        ToggleSwitch = CheckGroups.Find("ToggleSwitch").GetComponent<Toggle>();
        ToggleSwitchText = ToggleSwitch.transform.Find("XroY").GetComponent<Text>();
        Transform DefaultDrop = CheckGroups.Find("DefaultDropSpawn");
        for (int i = 0, len = DefaultDrop.childCount; i < len; i++)
        {
            List_Toggle_Default.Add(DefaultDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform FoodDrop = CheckGroups.Find("FoodDropSpawn");
        for (int i = 0, len = FoodDrop.childCount; i < len; i++)
        {
            List_Toggle_Food.Add(FoodDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform SpiralDrop = CheckGroups.Find("SpiralDropSpawn");
        for (int i = 0, len = SpiralDrop.childCount; i < len; i++)
        {
            List_Toggle_Spiral.Add(SpiralDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform DonutDrop = CheckGroups.Find("DonutDropSpawn");
        for (int i = 0, len = DonutDrop.childCount; i < len; i++)
        {
            List_Toggle_Donut.Add(DonutDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform TimeBombDrop = CheckGroups.Find("TimeBombDropSpawn");
        for (int i = 0, len = TimeBombDrop.childCount; i < len; i++)
        {
            List_Toggle_TimeBomb.Add(TimeBombDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform MysteryDrop = CheckGroups.Find("MysterytDropSpawn");
        for (int i = 0, len = MysteryDrop.childCount; i < len; i++)
        {
            List_Toggle_Mystery.Add(MysteryDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform ChameleonDrop = CheckGroups.Find("ChameleonDropSpawn");
        for (int i = 0, len = ChameleonDrop.childCount; i < len; i++)
        {
            List_Toggle_Chameleon.Add(ChameleonDrop.GetChild(i).GetComponent<Toggle>());
        }

        Transform KeyDrop = CheckGroups.Find("KeyDropSpawn");
        for (int i = 0, len = KeyDrop.childCount; i < len; i++)
        {
            List_Toggle_Key.Add(KeyDrop.GetChild(i).GetComponent<Toggle>());
        }
    }
    // Use this for initialization
    void Start()
    {

        //Default 세팅
        CreateCells();

#if UNITY_EDITOR
        m_Set = UnityEditor.EditorPrefs.GetInt(DataSetKey, 0);
        //Stage 세팅
        m_Stage = UnityEditor.EditorPrefs.GetInt(SaveStageKey, 1);
#endif
        //*************************************************************
        //원본을 수정하면 바로 수정되는 문제가 있으므로
        //클론을 만들어서 수정하고 작업이 끝난 후 저장할지 물어봐야한다.
        //*************************************************************
        //CCS = Data.Instance.GetStageCopy(1,1,stage);

        LoadStage();
#if UNITY_EDITOR
        Selection.activeGameObject = this.gameObject;
        //내부적으로 null에러가 뜨네..로드가 다 안된듯.
        //그래서 Start함수를 끝내고 실행하도록.
        Invoke("YellowPoint", 0.2f);
#endif
    }

    public void YellowPoint()
    {
#if UNITY_EDITOR
        EditorGUIUtility.PingObject(this);
#endif
    }

    public bool isControl = false;
    private bool ToggleOn = true;
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl))
            isControl = true;
        else
            isControl = false;

        if (ToggleSwitch.isOn != ToggleOn)
        {
            ToggleBoxSave();
            ToggleOn = ToggleSwitch.isOn;
            ToggleBoxLoad();

            if (ToggleOn)
                ToggleSwitchText.text = "X";
            else
                ToggleSwitchText.text = "Y";

        }
    }

    void CreateCells()
    {
        m_ListEBoard.Clear();
        for (int y = 0; y < MatchDefine.MaxY; y++)
        {
            for (int x = 0; x < MatchDefine.MaxX; x++)
            {
                GameObject obj = Instantiate(EBoard_Prefab);
                obj.transform.parent = EditorField;
                obj.transform.localPosition = new Vector2(x * CellWidth, -y * CellHight);
                obj.transform.localScale = Vector3.one;
                EBoard _ebd = obj.GetComponent<EBoard>();
                _ebd.Init(x, y);
                m_ListEBoard.Add(_ebd);
            }
        }
    }

    public EBoard GetEBoard(int index)
    {
        if (index < 0 || index > 81)
            return null;
        else
            return m_ListEBoard[index];
    }

    public void SelectDataSet()
    {
#if UNITY_EDITOR
        UnityEditor.EditorPrefs.SetInt(DataSetKey, m_Set);
#endif
    }

    public void LoadStage()
    {
        CSD = DataManager.LoadStage(m_Stage, m_Set);

        foreach (EBoard _ebd in m_ListEBoard)
            _ebd.Reset();


        int INDEX = 0;
        foreach (EBoard _ebd in m_ListEBoard)
        {
            //드랍세팅
            //아이템세팅
            if (CSD.DropDirs.Count != 0)
            {
                _ebd.IninDropDir(CSD.DropDirs[INDEX]);
            }
            else
            {
                for (int i = 0; i < 81; i++)
                {
                    CSD.DropDirs.Add(new DROP_DIR[1]);
                    CSD.DropDirs[i][0] = DROP_DIR.U;
                }

                _ebd.IninDropDir(CSD.DropDirs[INDEX]);
            }

            //패널세팅
            foreach (PanelData info in CSD.panels[INDEX].listinfo)
            {
                switch (info.paneltype)
                {
                    case PanelType.Stele:
                        {
                            _ebd.PanelB_Value = info.value;
                        }
                        break;
                    case PanelType.ConveyerBelt:
                        {
                            _ebd.m_ConveyerBeltInfo = JsonReader.Deserialize<ConveyerBeltInfo>(info.addData);
                        }
                        break;
                    case PanelType.Warp_In:
                    case PanelType.Warp_Out:
                        {
                            _ebd.PanelT_Value = info.value;
                        }
                        break;
                }


                _ebd.SetPanel(CompManager.GetPanelComp(info.paneltype, info.defence), false);

            }

            //디폴트 패널을 넣을때 기본노말아이템을 넣어주기때문에 한번 초기화 필요함!
            _ebd.Item_Comp = null;
            _ebd.Item_Img.enabled = false;

            //아이템세팅
            _ebd.SetItem(CompManager.GetItemComp(CSD.items[INDEX], CSD.colors[INDEX]), false);

            //ETC:Focus 초기화
            _ebd.Etc_Img.enabled = false;

            //ETC:Focus 세팅
            if (CSD.focus.Contains(INDEX))
            {
                _ebd.SetEtc(CompManager.GetEtcComp(EtcType.Focus));
            }

            INDEX++;
        }
        ToggleBoxLoad();

        TextInfoUpdate();

#if UNITY_EDITOR
        UnityEditor.EditorPrefs.SetInt(SaveStageKey, m_Stage);
#endif
    }

    public void SaveStage()
    {
        ToggleBoxSave();
        CSD.isUseGravity = false;
        for (int i = 0; i < CSD.DropDirs.Count; i++)
        {
            for (int j = 0; j < CSD.DropDirs[i].Length; j++)
            {
                if (CSD.DropDirs[i][j] != DROP_DIR.U)
                {
                    CSD.isUseGravity = true;
                    break;
                }
            }
        }

        if (!CSD.isUseGravity)
        {
            CSD.DropDirs = null;
            CSD.defaultSpawnLineY = null;
            CSD.foodSpawnLineY = null;
            CSD.SpiralSpawnLineY = null;
            CSD.DonutSpawnLineY = null;
            CSD.TimeBombSpawnLineY = null;
            CSD.MysterySpawnLineY = null;
            CSD.ChameleonSpawnLineY = null;
            CSD.KeySpawnLineY = null;
        }

        //****************
        //Json Serialized
        //*****************
        DataManager.SaveStage(m_Stage, CSD, m_Set);
        //CSD.DropDirs = new List<DROP_DIR[]>();
        //***************
        //Mission Save..
        //월드맵에서 사용할 정보 따로 저장
        //***************
        //1.중간에 stage보다 다음 stage를 먼저 생성하면 미션순서에 문제가 생김.
        //2.테스트용인 B세트를 수정하는중에도 A세트미션정보를 수정해버림.(이건 처리하면 되지만.)
        //위와 같은 문제로 인해. 미션정보 생성 버튼을 따로 둔다.
        //DataManger.SaveGame_Mission(m_Stage, CSD.missionType);
    }

    private void ToggleBoxLoad()
    {
        //*************************
        //스테이지 상단 체크박스.
        //*************************
        for (int i = 0; i < CSD.defaultSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Default[i].isOn = CSD.defaultSpawnLine[i];
            else
                List_Toggle_Default[i].isOn = CSD.defaultSpawnLineY[i];
        }
        for (int i = 0; i < CSD.foodSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Food[i].isOn = CSD.foodSpawnLine[i];
            else
                List_Toggle_Food[i].isOn = CSD.foodSpawnLineY[i];
        }
        for (int i = 0; i < CSD.SpiralSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Spiral[i].isOn = CSD.SpiralSpawnLine[i];
            else
                List_Toggle_Spiral[i].isOn = CSD.SpiralSpawnLineY[i];
        }
        for (int i = 0; i < CSD.DonutSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Donut[i].isOn = CSD.DonutSpawnLine[i];
            else
                List_Toggle_Donut[i].isOn = CSD.DonutSpawnLineY[i];
        }
        for (int i = 0; i < CSD.TimeBombSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_TimeBomb[i].isOn = CSD.TimeBombSpawnLine[i];
            else
                List_Toggle_TimeBomb[i].isOn = CSD.TimeBombSpawnLineY[i];
        }
        for (int i = 0; i < CSD.MysterySpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Mystery[i].isOn = CSD.MysterySpawnLine[i];
            else
                List_Toggle_Mystery[i].isOn = CSD.MysterySpawnLineY[i];
        }
        for (int i = 0; i < CSD.ChameleonSpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Chameleon[i].isOn = CSD.ChameleonSpawnLine[i];
            else
                List_Toggle_Chameleon[i].isOn = CSD.ChameleonSpawnLineY[i];
        }
        for (int i = 0; i < CSD.KeySpawnLine.Length; i++)
        {
            if (ToggleOn)
                List_Toggle_Key[i].isOn = CSD.KeySpawnLine[i];
            else
                List_Toggle_Key[i].isOn = CSD.KeySpawnLineY[i];
        }
    }

    private void ToggleBoxSave()
    {
        //*************************
        //스테이지 상단 체크박스.
        //*************************
        for (int i = 0, len = List_Toggle_Default.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Default[i].isOn)
                    CSD.defaultSpawnLine[i] = true;
                else
                    CSD.defaultSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Default[i].isOn)
                    CSD.defaultSpawnLineY[i] = true;
                else
                    CSD.defaultSpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Food.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Food[i].isOn)
                    CSD.foodSpawnLine[i] = true;
                else
                    CSD.foodSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Food[i].isOn)
                    CSD.foodSpawnLineY[i] = true;
                else
                    CSD.foodSpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Spiral.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Spiral[i].isOn)
                    CSD.SpiralSpawnLine[i] = true;
                else
                    CSD.SpiralSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Spiral[i].isOn)
                    CSD.SpiralSpawnLineY[i] = true;
                else
                    CSD.SpiralSpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Donut.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Donut[i].isOn)
                    CSD.DonutSpawnLine[i] = true;
                else
                    CSD.DonutSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Donut[i].isOn)
                    CSD.DonutSpawnLineY[i] = true;
                else
                    CSD.DonutSpawnLineY[i] = false;
            }
        }

        for (int i = 0, len = List_Toggle_TimeBomb.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_TimeBomb[i].isOn)
                    CSD.TimeBombSpawnLine[i] = true;
                else
                    CSD.TimeBombSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_TimeBomb[i].isOn)
                    CSD.TimeBombSpawnLineY[i] = true;
                else
                    CSD.TimeBombSpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Mystery.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Mystery[i].isOn)
                    CSD.MysterySpawnLine[i] = true;
                else
                    CSD.MysterySpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Mystery[i].isOn)
                    CSD.MysterySpawnLineY[i] = true;
                else
                    CSD.MysterySpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Chameleon.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Chameleon[i].isOn)
                    CSD.ChameleonSpawnLine[i] = true;
                else
                    CSD.ChameleonSpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Chameleon[i].isOn)
                    CSD.ChameleonSpawnLineY[i] = true;
                else
                    CSD.ChameleonSpawnLineY[i] = false;
            }
        }
        for (int i = 0, len = List_Toggle_Key.Count; i < len; i++)
        {
            if (ToggleOn)
            {
                if (List_Toggle_Key[i].isOn)
                    CSD.KeySpawnLine[i] = true;
                else
                    CSD.KeySpawnLine[i] = false;
            }
            else
            {
                if (List_Toggle_Key[i].isOn)
                    CSD.KeySpawnLineY[i] = true;
                else
                    CSD.KeySpawnLineY[i] = false;
            }
        }
    }

    public void NewStage()
    {
        Stage SD = new Stage();

        //기본 판넬 세팅
        for (int i = 0; i < SD.panels.Length; i++)
        {
            PanelData info = new PanelData();
            info.paneltype = PanelType.Default_Full;
            info.defence = -1;

            SD.panels[i].listinfo.Add(info);
        }

        //노말 아이템 세팅
        for (int i = 0; i < SD.items.Length; i++)
        {
            //기본으로 normail
            SD.items[i] = ItemType.Normal;
        }

        //아이템 랜덤 컬러 세팅
        for (int i = 0; i < SD.colors.Length; i++)
        {
            SD.colors[i] = ColorType.Rnd;
        }

        //스테이지 기본세팅
        for (int i = 0; i < SD.defaultSpawnLine.Length; i++)
        {
            SD.defaultSpawnLine[i] = true;
        }
        for (int i = 0; i < SD.foodSpawnLine.Length; i++)
        {
            SD.foodSpawnLine[i] = true;
        }
        for (int i = 0; i < SD.SpiralSpawnLine.Length; i++)
        {
            SD.SpiralSpawnLine[i] = true;
        }
        for (int i = 0; i < SD.DonutSpawnLine.Length; i++)
        {
            SD.DonutSpawnLine[i] = true;
        }
        for (int i = 0; i < SD.TimeBombSpawnLine.Length; i++)
        {
            SD.TimeBombSpawnLine[i] = true;
        }


        for (int i = 0; i < SD.appearColor.Length; i++)
        {
            SD.appearColor[i] = true;
        }

        //미션관련 세팅
        SD.missionInfo = new List<MissionInfo>();


        //**********
        //Selialize
        //**********
        DataManager.SaveStage(m_Stage, SD, m_Set);
    }

    public void PlayGame()
    {
        SaveStage();
        Destory_Scene();
#if UNITY_EDITOR
        UnityEditor.EditorPrefs.SetBool(EditorToPlayKey, true);
#endif
        SceneManager.LoadScene("Threematch");   //Application.LoadLevel("Threematch")

    }

    public void Destory_Scene()
    {
        //아항 씬을 넘어가도 Static인 놈들은 남아있어서 다음 로드할때 문제를 일으킨다.!!!
        CompManager.Destory_Comp();

    }

    public enum TOGGLE_TYPE
    {
        DEFAULT = 0, FOOD, SPIRAL, DONUT, TIMEBOMB, MYSTERY, CHAMELEON, KEY, NUM
    }
    public void clickAllCheckToggle(int idxType)
    {
        // 에디터에서 사용, 드랍아이템 전체 on/off
        Toggle all_check_toggle = transform.Find("CheckGroups/AllCheckGroups/" + ((TOGGLE_TYPE)idxType).ToString()).GetComponent<Toggle>();

        List<Toggle> temp_list = new List<Toggle>();
        switch ((TOGGLE_TYPE)idxType)
        {
            case TOGGLE_TYPE.DEFAULT:
                temp_list = List_Toggle_Default;
                break;
            case TOGGLE_TYPE.FOOD:
                temp_list = List_Toggle_Food;
                break;
            case TOGGLE_TYPE.SPIRAL:
                temp_list = List_Toggle_Spiral;
                break;
            case TOGGLE_TYPE.DONUT:
                temp_list = List_Toggle_Donut;
                break;
            case TOGGLE_TYPE.TIMEBOMB:
                temp_list = List_Toggle_TimeBomb;
                break;
            case TOGGLE_TYPE.MYSTERY:
                temp_list = List_Toggle_Mystery;
                break;
            case TOGGLE_TYPE.CHAMELEON:
                temp_list = List_Toggle_Chameleon;
                break;
            case TOGGLE_TYPE.KEY:
                temp_list = List_Toggle_Key;
                break;
            default:
                break;
        }

        foreach (Toggle one in temp_list)
        {
            one.isOn = all_check_toggle.isOn;
        }
    }
    #region 추가 정보 표시
    public Dictionary<ItemType, int> Dic_TextInfo_Item = new Dictionary<ItemType, int>();
    public Dictionary<PanelType, int> Dic_TextInfo_Panel = new Dictionary<PanelType, int>();
    public void TextInfoInit()
    {
        Dic_TextInfo_Item.Clear();
        Dic_TextInfo_Panel.Clear();
    }
    public void SetTextInfo(ItemType type, int Plus)
    {
        if (type == ItemType.Normal)
            return;

        if (Dic_TextInfo_Item.ContainsKey(type))
        {
            Dic_TextInfo_Item[type] += Plus;

            if (Dic_TextInfo_Item[type] <= 0)
                Dic_TextInfo_Item.Remove(type);
        }
        else
        {
            if (Plus > 0)
                Dic_TextInfo_Item[type] = Plus;
        }

        //  DrawTextInfo();
    }
    public void SetTextInfo(PanelType type, int Plus)
    {
        if (type == PanelType.Default_Full)
            return;

        if (Dic_TextInfo_Panel.ContainsKey(type))
        {
            Dic_TextInfo_Panel[type] += Plus;

            if (Dic_TextInfo_Panel[type] <= 0)
                Dic_TextInfo_Panel.Remove(type);
        }
        else
        {
            if (Plus > 0)
                Dic_TextInfo_Panel[type] = Plus;
        }

        // DrawTextInfo();
    }

    public void TextInfoUpdate()
    {
        TextInfoInit();
        for (int i = 0; i < EditorManager.Instance.m_ListEBoard.Count; i++)
        {
            if (EditorManager.Instance.m_ListEBoard[i].Item_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].Item_Comp.m_ItemType, 1);
            if (EditorManager.Instance.m_ListEBoard[i].PanelB_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].PanelB_Comp.m_PanelType, 1);
            if (EditorManager.Instance.m_ListEBoard[i].PanelF_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].PanelF_Comp.m_PanelType, 1);
            if (EditorManager.Instance.m_ListEBoard[i].PanelUn_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].PanelUn_Comp.m_PanelType, 1);
            if (EditorManager.Instance.m_ListEBoard[i].PanelUp_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].PanelUp_Comp.m_PanelType, 1);
            if (EditorManager.Instance.m_ListEBoard[i].PanelT_Comp != null)
                SetTextInfo(EditorManager.Instance.m_ListEBoard[i].PanelT_Comp.m_PanelType, 1);
        }

        DrawTextInfo();
    }

    public void DrawTextInfo()
    {
        EditorManager.Instance.TextInfo.text = "";

        foreach (KeyValuePair<ItemType, int> dic in Dic_TextInfo_Item)
        {
            EditorManager.Instance.TextInfo.text += dic.Key.ToString() + Environment.NewLine + "<color=yellow>" + dic.Value + "</color>" + Environment.NewLine;
        }
        foreach (KeyValuePair<PanelType, int> dic in Dic_TextInfo_Panel)
        {
            EditorManager.Instance.TextInfo.text += dic.Key.ToString() + Environment.NewLine + "<color=yellow>" + dic.Value + "</color>" + Environment.NewLine;
        }
    }
    #endregion
}
