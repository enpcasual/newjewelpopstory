﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public enum CompType
{
    Item = 0,
    Panel,
    Drop,
    Etc,    //블럭세팅과는 다른세팅
}

public abstract class Comp : MonoBehaviour {

    //보드를 세팅하기 위해 필요한 정보
    public CompType m_CompType = CompType.Item;

    public Image m_Image;

    public void Init()
    {
        if (m_Image == null)
        {
            m_Image = transform.GetComponent<Image>();
        }
    }

}
