﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropEBoard : MonoBehaviour {

    [SerializeField]
    private Image Drop_U = null;
    [SerializeField]
    private Image Drop_D = null;
    [SerializeField]
    private Image Drop_L = null;
    [SerializeField]
    private Image Drop_R = null;
    [SerializeField]
    private Image Drop_Last = null;

    public List<DROP_DIR> DropComps
    {
        get
        {
            List<DROP_DIR> _dropComp = new List<DROP_DIR>();
            if(Drop_U.enabled)
            {
                _dropComp.Add(DROP_DIR.D);
            }
            if (Drop_D.enabled)
            {
                _dropComp.Add(DROP_DIR.U);
            }
            if (Drop_L.enabled)
            {
                _dropComp.Add(DROP_DIR.R);
            }
            if (Drop_R.enabled)
            {
                _dropComp.Add(DROP_DIR.L);
            }
            if (Drop_Last.enabled)
            {
                _dropComp.Add(DROP_DIR.List);
            }

            if (_dropComp.Count == 0)
                _dropComp.Add(DROP_DIR.U);

            return _dropComp;
        }
    }

    //가져오는 방향이기 때문에 화살표가 반대로 그려져야 함.
    public void SetDropImage(DropComp dropComps)
    {
        switch (dropComps.m_DropDir)
        {
            case DROP_DIR.D:
                Drop_U.enabled = true;
                break;
            case DROP_DIR.U:
                Drop_D.enabled = true;
                break;
            case DROP_DIR.R:
                Drop_L.enabled = true;
                break;
            case DROP_DIR.L:
                Drop_R.enabled = true;
                break;
            case DROP_DIR.List:
                Drop_Last.enabled = true;
                break;
        }
    }

    public void DeleteDropImage()
    {
        Drop_U.enabled = false;
        Drop_D.enabled = false;
        Drop_L.enabled = false;
        Drop_R.enabled = false;
        Drop_Last.enabled = false;
    }
}
