﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using JsonFx.Json;
using System;

public static class Extensions
{
    public static T Before<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) - 1;
        return (j < 0) ? Arr[Arr.Length - 1] : Arr[j];
    }

    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }
}

public class EBoard : MonoBehaviour
{
    //석판레이어(Panel_Bottom)가 추가 되면서 전체 명칭 변경!!
    //Inspecter
    public Image PanelD_Img;
    public Image PanelB_Img;
    public Image PanelF_Img;
    public Image PanelUn_Img;
    public Image Item_Img;
    public Image PanelUp_Img;
    public Image PanelT_Img; //warp

    //Sub, Text
    public Image PanelUn_Img_Sub;
    public Text PanelB_Text;
    public Text PanelT_Text;

    public Sprite PanelD_Spr1;
    public Sprite PanelD_Spr2;

    //ETC
    public Image Etc_Img;

    //Drop
    public DropEBoard Drop_EBoard = null;

    [HideInInspector]
    public PanelComp PanelD_Comp;
    [HideInInspector]
    public PanelComp PanelB_Comp;
    [HideInInspector]
    public PanelComp PanelF_Comp;
    [HideInInspector]
    public PanelComp PanelUn_Comp;
    [HideInInspector]
    public ItemComp Item_Comp;
    [HideInInspector]
    public PanelComp PanelUp_Comp;
    [HideInInspector]
    public PanelComp PanelT_Comp;

    //[HideInInspector]
    //public List<DropComp> Drop_Comps = new List<DropComp>();
    //*****************
    //추가 정보값 변수
    //*****************
    //골때리네. comp들이 참조라서value값을 각각 들고 있지 않아서 문제가 생긴다.
    //즉, 로드할때 각 Eboard마다 해당 값을 가지고 있고 수정하거나 변경 되면 다시 저장한다.
    //컴포넌트는 단일객체이고 로드할때 해당 객체로 연결되서 이미지등을 가져오기 때문
    //패널들의 Value값을 로드할 곳이 없다.
    //그래서 EBoard에 변수를 만들어서 각각 들고 있는거다.
    public int PanelB_Value = 0;
    public int PanelT_Value = 0;

    //*****************
    //컨베이어벨트 추가
    //*****************
    public Sprite ConveyerBelt1;
    public Sprite ConveyerBelt2;
    public Sprite ConveyerBelt3; //포탈이미지
    public ConveyerBeltInfo m_ConveyerBeltInfo = new ConveyerBeltInfo();
    static public List<EBoard> List_EBoard = null;


    [HideInInspector]
    public int X = 0;
    [HideInInspector]
    public int Y = 0;
    [HideInInspector]
    public int INDEX = 0;

    [HideInInspector]
    public Item m_Item;

    [HideInInspector]
    public EditorManager Edit_Mgr;
    [HideInInspector]
    public Stage CCS; //CurrnetCopyStage


    float AxiScrollWheel = 0f;

    public void Awake()
    {
        Edit_Mgr = EditorManager.Instance;
    }

    public void Start()
    {

    }

    void Update()
    {
        if (MouseOver)
        {
            AxiScrollWheel = Input.GetAxisRaw("Mouse ScrollWheel");

            if (AxiScrollWheel == 0)
                return;

            if (EditorManager.Instance.isControl)
            {
                if (PanelT_Comp != null)
                {
                    switch (PanelT_Comp.m_PanelType)
                    {
                        case PanelType.Warp_In:
                        case PanelType.Warp_Out:
                            {
                                if (AxiScrollWheel < 0)
                                {
                                    if (PanelT_Value > 0)
                                        PanelT_Value--;
                                }
                                else if (AxiScrollWheel > 0)
                                {
                                    PanelT_Value++;
                                }

                                PanelT_Text.text = PanelT_Value.ToString();
                                SaveCopyData();
                            }
                            break;
                    }
                }
            }
            else
            {
                if (PanelUn_Comp != null)
                {
                    switch (PanelUn_Comp.m_PanelType)
                    {
                        case PanelType.ConveyerBelt:
                            {
                                if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
                                {
                                    if (AxiScrollWheel < 0)
                                        m_ConveyerBeltInfo.Dir_In = m_ConveyerBeltInfo.Dir_In.Next();
                                    else if (AxiScrollWheel > 0)
                                        m_ConveyerBeltInfo.Dir_In = m_ConveyerBeltInfo.Dir_In.Before();

                                    ConveyerBeltUpdate();
                                }
                                else if (m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
                                {
                                    if (AxiScrollWheel < 0)
                                        m_ConveyerBeltInfo.Dir_Out = m_ConveyerBeltInfo.Dir_Out.Next();
                                    else if (AxiScrollWheel > 0)
                                        m_ConveyerBeltInfo.Dir_Out = m_ConveyerBeltInfo.Dir_Out.Before();

                                    ConveyerBeltUpdate();
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    public void Init(int x, int y)
    {
        X = x;
        Y = y;

        INDEX = X + Y * MatchDefine.MaxX;
    }

    public void Reset()
    {
        CCS = EditorManager.Instance.CSD;

        //아이템 초기화
        Item_Comp = null;
        Item_Img.enabled = false;

        //패널 초기화
        PanelD_Comp = null;
        PanelD_Img.enabled = false;

        PanelB_Comp = null;
        PanelB_Img.enabled = false;
        PanelB_Text.enabled = false;

        PanelF_Comp = null;
        PanelF_Img.enabled = false;

        PanelUn_Comp = null;
        PanelUn_Img.enabled = false;
        PanelUn_Img_Sub.enabled = false;

        PanelUp_Comp = null;
        PanelUp_Img.enabled = false;

        PanelT_Comp = null;
        PanelT_Img.enabled = false;
        PanelT_Text.enabled = false;

        //Value, AddData 추가 정보 로드(Comp에서 가져오는 객체는 단일 객채를 참조하는 형식이므로 
        //각각 다른 값인 Value, AddData는 EBoard 변수에 로드함.
        PanelB_Value = 0;
        PanelT_Value = 0;
    }

    #region 보드 미션패널가능 체크
    public bool CheckBoardMission()
    {
        int index = 0;
        if (PanelD_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo[PanelD_Comp.m_PanelType].TogetherMission == false)
                return false;
        }

        if (PanelB_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelB_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelB_Comp.m_PanelType].TogetherMission == false)
                    return false;
        }

        if (PanelF_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelF_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelF_Comp.m_PanelType].TogetherMission == false)
                    return false;
        }

        if (PanelUn_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelUn_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelUn_Comp.m_PanelType].TogetherMission == false)
                    return false;
        }
        if (PanelUp_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelUp_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelUp_Comp.m_PanelType].TogetherMission == false)
                    return false;
        }
        if (PanelT_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelT_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelT_Comp.m_PanelType].TogetherMission == false)
                    return false;
        }

        if (index == 0) //패널이 하나도 없으면
            return false;

        return true;
    }
    #endregion

    #region 아이템과 패널조건 체크
    public bool CheckPossbleItem(PanelComp _comp)
    {
        if (_comp != null)
        {
            if (PanelManager.Instance.Dic_PanelInfo[_comp.m_PanelType].ItemExist == false)
                return false;
        }
        return true;
    }
    public bool CheckPossbleItem()
    {
        int index = 0;
        if (PanelD_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo[PanelD_Comp.m_PanelType].ItemExist == false)
                return false;
        }
        if (PanelB_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelB_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelB_Comp.m_PanelType].ItemExist == false)
                    return false;
        }
        if (PanelF_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelF_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelF_Comp.m_PanelType].ItemExist == false)
                    return false;
        }
        if (PanelUn_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelUn_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelUn_Comp.m_PanelType].ItemExist == false)
                    return false;
        }
        if (PanelUp_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelUp_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelUp_Comp.m_PanelType].ItemExist == false)
                    return false;
        }
        if (PanelT_Comp != null)
        {
            index++;
            if (PanelManager.Instance.Dic_PanelInfo.ContainsKey(PanelT_Comp.m_PanelType) == true)     //패널이 구현되어 있고
                if (PanelManager.Instance.Dic_PanelInfo[PanelT_Comp.m_PanelType].ItemExist == false)
                    return false;
        }

        if (index == 0) //패널이 하나도 없으면
            return false;

        return true;
    }

    public bool CheckPossbleTogetherMission(PanelComp _comp)
    {
        if (_comp != null)
        {
            if (PanelManager.Instance.Dic_PanelInfo[_comp.m_PanelType].TogetherMission == false)
                return false;
        }
        return true;
    }
    #endregion

    #region Panel 추가
    public void SetPanel_Click(PanelComp pcomp)
    {
        //SetPanel는 터치로 인한 세팅뿐만 아니라 데이타로드할때도 사용한다.
        //그런데 SetPanel안에서 다른보드를 건드는 작업은 데이타로드할때 문제를 일으킨다.
        //뒤에쪽 보드는 아직 로드가 되지 않았기 때문이다.
        //결국 SetPanel함수는 한개의 보드를 세팅하는것으로 국한되어야한다.
        //여러개를 작업할려면 해당보드마다 호출해주도록 하자.
        switch (((PanelComp)EditorManager.Instance.SelectComp).m_PanelType)
        {
            case PanelType.Stele:
                {
                    //첫번째 보드에서 석판 보드를 다 세팅한다.
                    //단, 로드일때 문제가 생기지 않는 이유는 함수 내부에서 
                    //석판이 포함되어있으면 List를 클리어 해버리기 때문이다.
                    List<EBoard> list_Eboard = GetSteleBoads(pcomp.Defence);
                    for (int i = 0; i < list_Eboard.Count; i++)
                    {
                        list_Eboard[i].PanelB_Value = INDEX;

                        list_Eboard[i].SetPanel(pcomp);
                    }
                }
                break;
            case PanelType.Cake_A:
                {
                    //현재 보드세팅
                    SetPanel(pcomp);

                    //오른쪽 보드 세팅
                    EBoard _ebd = EditorManager.Instance.GetEBoard(INDEX + 1);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.Cake_B, pcomp.Defence));

                    //아래쪽 보드 세팅
                    _ebd = EditorManager.Instance.GetEBoard(INDEX + 9);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.Cake_C, pcomp.Defence));

                    //대각선 아래쪽 보드 세팅
                    _ebd = EditorManager.Instance.GetEBoard(INDEX + 10);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.Cake_D, pcomp.Defence));
                }
                break;
            case PanelType.JewelTree_A:
                {
                    //현재 보드세팅
                    SetPanel(pcomp);

                    //오른쪽 보드 세팅
                    EBoard _ebd = EditorManager.Instance.GetEBoard(INDEX + 1);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.JewelTree_B, pcomp.Defence));

                    //아래쪽 보드 세팅
                    _ebd = EditorManager.Instance.GetEBoard(INDEX + 9);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.JewelTree_C, pcomp.Defence));

                    //대각선 아래쪽 보드 세팅
                    _ebd = EditorManager.Instance.GetEBoard(INDEX + 10);
                    if (_ebd != null)
                        _ebd.SetPanel(CompManager.GetPanelComp(PanelType.JewelTree_D, pcomp.Defence));
                }
                break;
            default:
                SetPanel((PanelComp)EditorManager.Instance.SelectComp);
                break;
        }
    }

    public void SetPanel(PanelComp pcomp, bool save = true)
    {
        //만약 아이템이 있을수 없는 패널을 찍으면 아이템 지운다.  
        if (CheckPossbleItem(pcomp) == false)
        {
            Item_Comp = null;
            Item_Img.enabled = false;
        }

        //미션패널(와퍼,딸기잼)과 공존할수 없다면
        if (CheckPossbleTogetherMission(pcomp) == false)
        {
            PanelF_Comp = null;
            PanelF_Img.enabled = false;
        }


        if (pcomp.m_PanelType != PanelType.Default_Full && pcomp.m_PanelType != PanelType.Default_Empty && pcomp.m_PanelType != PanelType.Creator_Empty)
        {
            if (PanelD_Comp == null || PanelD_Comp.m_PanelType != PanelType.Default_Full)
            {
                //기본 패널 넣어준다.
                PanelD_Comp = CompManager.GetPanelComp(PanelType.Default_Full, -1);
                //바닥이미지는 보드 세팅할때 격자로 세팅해준다.
                //DPanel_Img.sprite = pcomp.m_Image.sprite;
                if (INDEX % 2 == 0)
                    PanelD_Img.sprite = PanelD_Spr1;
                else
                    PanelD_Img.sprite = PanelD_Spr2;

                PanelD_Img.color = pcomp.m_Image.color;
                PanelD_Img.enabled = true;
            }
        }

        //화면세팅
        switch (pcomp.m_PanelType)
        {
            //디폴트 패널
            case PanelType.Default_Full:
                {
                    PanelD_Comp = pcomp;
                    //바닥이미지는 보드 세팅할때 격자로 세팅해준다.
                    //DPanel_Img.sprite = pcomp.m_Image.sprite;
                    if (INDEX % 2 == 0)
                        PanelD_Img.sprite = PanelD_Spr1;
                    else
                        PanelD_Img.sprite = PanelD_Spr2;

                    PanelD_Img.color = pcomp.m_Image.color;
                    PanelD_Img.enabled = true;

                    //기본 아이템 추가
                    SetItem(CompManager.GetItemComp(ItemType.Normal, ColorType.Rnd), false);
                }
                break;
            case PanelType.Default_Empty:
                {
                    //모든 세팅 Delete
                    PanelD_Comp = null; PanelD_Img.enabled = false;
                    PanelB_Comp = null; PanelB_Img.enabled = false;
                    PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;

                    PanelD_Comp = pcomp;
                    //DPanel_Img.sprite = pcomp.m_Image.sprite;
                    //DPanel_Img.color = pcomp.m_Image.color;
                    PanelD_Img.enabled = false;
                }
                break;
            case PanelType.Creator_Empty:
                {
                    //모든 세팅 Delete
                    PanelD_Comp = null; PanelD_Img.enabled = false;
                    PanelB_Comp = null; PanelB_Img.enabled = false;
                    PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;


                    PanelD_Comp = pcomp;
                    PanelD_Img.sprite = pcomp.m_Image.sprite;
                    PanelD_Img.color = pcomp.m_Image.color;
                    PanelD_Img.enabled = true;
                }
                break;
            //석판 패널(Bottom)
            case PanelType.Stele:
                {
                    PanelB_Comp = pcomp;
                    PanelB_Img.sprite = pcomp.m_Image.sprite;
                    PanelB_Img.color = pcomp.m_Image.color;
                    PanelB_Img.enabled = true;

                    //Value 추가 정보
                    PanelB_Text.enabled = true;
                    PanelB_Text.text = PanelB_Value.ToString();
                }
                break;
            //미션 패널(Floor)
            case PanelType.Wafer_floor:
            case PanelType.Jam:
            case PanelType.Stele_Hide:
                {
                    if (CheckBoardMission())
                    {
                        PanelF_Comp = pcomp;
                        PanelF_Img.sprite = pcomp.m_Image.sprite;
                        PanelF_Img.color = pcomp.m_Image.color;
                        PanelF_Img.enabled = true;
                    }
                }
                break;

            //아이템뒤패널
            case PanelType.ConveyerBelt:
            case PanelType.IceCream_Block:
            case PanelType.IceCream_Creator:
            case PanelType.Bread_Block:
            case PanelType.Fixed_Block:
            case PanelType.Cracker:
            case PanelType.S_Tree:
            case PanelType.MagicColor:
            case PanelType.Ring:
                {
                    PanelUn_Comp = pcomp;
                    PanelUn_Img.sprite = pcomp.m_Image.sprite;
                    PanelUn_Img.color = pcomp.m_Image.color;
                    PanelUn_Img.enabled = true;
                    PanelUn_Img.transform.localEulerAngles = Vector3.zero;

                    switch (PanelUn_Comp.m_PanelType)
                    {
                        case PanelType.ConveyerBelt:
                            {
                                //항상 여기는 Load와 클릭(세팅) 둘다 사용하는 함수라는걸 유념해야한다.
                                //그래서 정보가 있던 없던 무조껀 세팅을 하자.
                                //클릭(세팅)중이라면 클릭을 땔때 List_EBoard를 돌면서 재대로 세팅해줄것이다.
                                ConveyerBeltUpdate(save);
                            }
                            break;
                        default:
                            break;
                    }
                }
                break;
            case PanelType.JewelTree_A:
            case PanelType.JewelTree_B:
            case PanelType.JewelTree_C:
            case PanelType.JewelTree_D:
                {
                    //기본패널 빼고 전부 초기화.
                    //m_DpanelComp = null; DPanel_Img.enabled = false;
                    PanelB_Comp = null; PanelB_Img.enabled = false;
                    PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;


                    PanelUn_Comp = pcomp;
                    PanelUn_Img.sprite = pcomp.m_Image.sprite;
                    PanelUn_Img.color = pcomp.m_Image.color;
                    PanelUn_Img.enabled = true;
                    PanelUn_Img.transform.localEulerAngles = Vector3.zero;
                }
                break;
            case PanelType.Cake_A:
            case PanelType.Cake_B:
            case PanelType.Cake_C:
            case PanelType.Cake_D:
                {
                    //기본, 미션 패널 빼고 전부 초기화.
                    //m_DpanelComp = null; DPanel_Img.enabled = false;
                    //PanelB_Comp = null; PanelB_Img.enabled = false;
                    //PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;


                    PanelUn_Comp = pcomp;
                    PanelUn_Img.sprite = pcomp.m_Image.sprite;
                    PanelUn_Img.color = pcomp.m_Image.color;
                    PanelUn_Img.enabled = true;
                    PanelUn_Img.transform.localEulerAngles = Vector3.zero;
                }
                break;
            //아이템앞패널
            case PanelType.Lolly_Cage:
            case PanelType.Ice_Cage:
            case PanelType.Bottle_Cage:
                {
                    PanelUp_Comp = pcomp;
                    PanelUp_Img.sprite = pcomp.m_Image.sprite;
                    PanelUp_Img.color = pcomp.m_Image.color;
                    PanelUp_Img.enabled = true;
                }
                break;
            case PanelType.Creator_Food:
            case PanelType.Creator_Sprial:
            case PanelType.Creator_TimeBomb:
            case PanelType.Creator_Food_Sprial:
            case PanelType.Creator_Food_TimeBomb:
            case PanelType.Creator_Sprial_TimeBomb:
            case PanelType.Creator_Key:
            case PanelType.Creator_Key_Food:
            case PanelType.Creator_Key_TimeBomb:
                {
                    //모든 세팅 Delete
                    //m_DpanelComp = null; DPanel_Img.enabled = false;
                    PanelB_Comp = null; PanelB_Img.enabled = false;
                    PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;

                    PanelUp_Comp = pcomp;
                    PanelUp_Img.sprite = pcomp.m_Image.sprite;
                    PanelUp_Img.color = pcomp.m_Image.color;
                    PanelUp_Img.enabled = true;
                }
                break;
            //워프 아이템
            case PanelType.Warp_In:
            case PanelType.Warp_Out:
                {
                    PanelT_Comp = pcomp;
                    PanelT_Img.sprite = pcomp.m_Image.sprite;
                    PanelT_Img.color = pcomp.m_Image.color;
                    PanelT_Img.enabled = true;

                    //WPanel_Img.rectTransform.anchoredPosition = Vector3.zero;
                    if (pcomp.m_PanelType == PanelType.Warp_In)
                        PanelT_Img.rectTransform.localPosition = new Vector3(40, -70f, 0);
                    else if (pcomp.m_PanelType == PanelType.Warp_Out)
                        PanelT_Img.rectTransform.localPosition = new Vector3(40, -10, 0);
                    PanelT_Img.SetNativeSize();

                    //Value 텍스트 수정!!m_WPanel_Value값이 로드할때 있음.                 
                    PanelT_Text.enabled = true;
                    PanelT_Text.text = PanelT_Value.ToString();
                }
                break;
            case PanelType.FoodArrive:
            case PanelType.JellyBearStart:
                {
                    PanelT_Comp = pcomp;
                    PanelT_Img.sprite = pcomp.m_Image.sprite;
                    PanelT_Img.color = pcomp.m_Image.color;
                    PanelT_Img.enabled = true;

                    //WPanel_Img.rectTransform.anchoredPosition = Vector3.zero;
                    PanelT_Img.rectTransform.localPosition = new Vector3(40, -70f, 0);
                    PanelT_Img.SetNativeSize();

                    //혹시Value 텍스트 켜져있으면 종료!
                    PanelT_Text.enabled = false;
                    PanelT_Value = 0;
                }
                break;

        }

        //SetTextInfo(pcomp.m_PanelType, 1);

        //데이타세팅
        if (save) SaveCopyData();

    }

    public void DeletePanel(PanelComp pcomp)
    {
        switch (pcomp.m_PanelType)
        {
            //디폴트 패널
            case PanelType.Default_Full:
                {
                    //모든 세팅 Delete
                    PanelD_Comp = null; PanelD_Img.enabled = false;
                    PanelB_Comp = null; PanelB_Img.enabled = false;
                    PanelF_Comp = null; PanelF_Img.enabled = false;
                    PanelUn_Comp = null; PanelUn_Img.enabled = false;
                    PanelUp_Comp = null; PanelUp_Img.enabled = false;
                    PanelT_Comp = null; PanelT_Img.enabled = false;
                    Item_Comp = null; Item_Img.enabled = false;
                    Drop_EBoard.DeleteDropImage();

                    //SetTextInfo(pcomp.m_PanelType, -1);
                    SetPanel(CompManager.GetPanelComp(PanelType.Default_Empty, -1));
                }
                break;
            case PanelType.Default_Empty:
            case PanelType.Creator_Empty:
                {
                    SetPanel(CompManager.GetPanelComp(PanelType.Default_Full, -1));
                }
                break;
            //석판 패널
            case PanelType.Stele:
                {
                    if (PanelB_Comp == null)
                        break;

                    PanelB_Comp = null;
                    PanelB_Img.enabled = false;

                    //Value 초기화
                    PanelB_Value = 0;
                    PanelB_Text.enabled = false;
                }
                break;
            //미션 패널
            case PanelType.Wafer_floor:
            case PanelType.Jam:
            case PanelType.Stele_Hide:
                {
                    if (PanelF_Comp == null)
                        break;

                    PanelF_Comp = null;
                    PanelF_Img.enabled = false;

                }
                break;
            //아이템뒤패널
            case PanelType.ConveyerBelt:
            case PanelType.IceCream_Block:
            case PanelType.IceCream_Creator:
            case PanelType.Bread_Block:
            case PanelType.Fixed_Block:
            case PanelType.Cracker:
            case PanelType.S_Tree:
            case PanelType.MagicColor:
            case PanelType.Ring:
            case PanelType.JewelTree_A:
            case PanelType.JewelTree_B:
            case PanelType.JewelTree_C:
            case PanelType.JewelTree_D:
                {
                    if (PanelUn_Comp == null)
                        break;
                    PanelUn_Comp = null;
                    PanelUn_Img.enabled = false;
                    PanelUn_Img_Sub.enabled = false;
                }
                break;
            case PanelType.Cake_A:
            case PanelType.Cake_B:
            case PanelType.Cake_C:
            case PanelType.Cake_D:
                {
                    if (PanelUn_Comp == null)
                        break;
                    PanelUn_Comp = null;
                    PanelUn_Img.enabled = false;
                    PanelUn_Img_Sub.enabled = false;
                }
                break;
            //아이템앞패널
            case PanelType.Lolly_Cage:
            case PanelType.Ice_Cage:
            case PanelType.Bottle_Cage:
                {
                    if (PanelUp_Comp == null)
                        break;

                    PanelUp_Comp = null;
                    PanelUp_Img.enabled = false;
                }
                break;
            case PanelType.Creator_Food:
            case PanelType.Creator_Sprial:
            case PanelType.Creator_TimeBomb:
            case PanelType.Creator_Food_Sprial:
            case PanelType.Creator_Food_TimeBomb:
            case PanelType.Creator_Sprial_TimeBomb:
            case PanelType.Creator_Key:
            case PanelType.Creator_Key_Food:
            case PanelType.Creator_Key_TimeBomb:
                {
                    if (PanelUp_Comp == null)
                        break;

                    PanelUp_Comp = null;
                    PanelUp_Img.enabled = false;
                }
                break;
            //워프 아이템
            case PanelType.Warp_In:
            case PanelType.Warp_Out:
            case PanelType.FoodArrive:
            case PanelType.JellyBearStart:
                {
                    if (PanelT_Comp == null)
                        break;

                    PanelT_Comp = null;
                    PanelT_Img.enabled = false;

                    //Value 초기화
                    PanelT_Value = 0;
                    PanelT_Text.enabled = false;
                }
                break;
        }

        //만약 아이템이 있을수 없는 패널을 지웠으면 기본 아이템을 넣어준다.
        //아이템을 넣을때 패널에 아이템이 있을수 있는지 체크하기 때문에 패널은 지운 다음 넣어주어야 한다.
        if (CheckPossbleItem(pcomp) == false)
        {
            //기본 아이템 추가
            SetItem(CompManager.GetItemComp(ItemType.Normal, ColorType.Rnd), false);
        }

        //데이타저장
        SaveCopyData();
    }

    #endregion

    #region 아이템 세팅
    public void SetItem(ItemComp icomp, bool save = true)
    {
        //bool _ok = false;
        if (icomp != null)
        {
            if (CheckPossbleItem())
            {
                Item_Comp = icomp;
                Item_Img.sprite = icomp.m_Image.sprite;
                Item_Img.color = icomp.m_Image.color;
                Item_Img.enabled = true;

                //SetTextInfo(icomp.m_ItemType, 1);

                //데이타세팅
                if (save) SaveCopyData();
            }
        }
    }

    public void DeleteItem(ItemComp icomp)
    {
        if (Item_Comp == null)
            return;

        //아이템은 같은 아이템이 아니여도 보드에서 무조껀 삭제.
        //if (m_ItemComp.m_ItemType == icomp.m_ItemType)
        {
            //SetTextInfo(m_ItemComp.m_ItemType, -1);

            Item_Comp = null;
            Item_Img.enabled = false;

            if (EditorManager.Instance.isControl)
                SaveCopyData();
            else //기본적으로 지우면 노말아이템이 들어간다.
                SetItem(CompManager.GetItemComp(ItemType.Normal, ColorType.Rnd));
        }
    }

    #endregion

    #region 세이브 관련
    public void SaveCopyData()
    {
        //********
        //드랍방향
        //********
        CCS.DropDirs[INDEX] = Drop_EBoard.DropComps.ToArray();

        //********
        //패널
        //********
        CCS.panels[INDEX].listinfo.Clear();

        PanelData _info = new PanelData();
        if (PanelD_Comp != null)
        {
            _info.paneltype = PanelD_Comp.m_PanelType;
            _info.defence = PanelD_Comp.Defence;
            CCS.panels[INDEX].listinfo.Add(_info);
        }

        if (PanelB_Comp != null)
        {
            _info = new PanelData();
            _info.paneltype = PanelB_Comp.m_PanelType;
            _info.defence = PanelB_Comp.Defence;
            _info.value = PanelB_Value;    //Value 저장

            CCS.panels[INDEX].listinfo.Add(_info);
        }

        if (PanelF_Comp != null)
        {
            _info = new PanelData();
            _info.paneltype = PanelF_Comp.m_PanelType;
            _info.defence = PanelF_Comp.Defence;

            CCS.panels[INDEX].listinfo.Add(_info);
        }

        if (PanelUn_Comp != null)
        {
            _info = new PanelData();
            _info.paneltype = PanelUn_Comp.m_PanelType;
            _info.defence = PanelUn_Comp.Defence;
            switch (PanelUn_Comp.m_PanelType)
            {
                case PanelType.ConveyerBelt:
                    _info.addData = JsonWriter.Serialize(m_ConveyerBeltInfo);
                    break;
                default:
                    break;
            }

            CCS.panels[INDEX].listinfo.Add(_info);
        }

        if (PanelUp_Comp != null)
        {
            _info = new PanelData();
            _info.paneltype = PanelUp_Comp.m_PanelType;
            _info.defence = PanelUp_Comp.Defence;
            CCS.panels[INDEX].listinfo.Add(_info);
        }

        if (PanelT_Comp != null)
        {
            _info = new PanelData();
            _info.paneltype = PanelT_Comp.m_PanelType;
            _info.defence = PanelT_Comp.Defence;
            _info.value = PanelT_Value;    //Value 저장

            CCS.panels[INDEX].listinfo.Add(_info);
        }


        //********
        //아이템
        //********
        CCS.items[INDEX] = ItemType.None;
        CCS.colors[INDEX] = ColorType.None;

        if (Item_Comp != null)
        {
            CCS.items[INDEX] = Item_Comp.m_ItemType;
            CCS.colors[INDEX] = Item_Comp.m_ColorType;
        }

        Edit_Mgr.TextInfoUpdate();
    }

    #endregion

    #region ListEboard관리
    public void ListEboard_Add()
    {
        if (List_EBoard == null)
            List_EBoard = new List<EBoard>();

        if (List_EBoard.Contains(this) == false)
            List_EBoard.Add(this);
    }
    public void ListEboard_Clear()
    {
        if (List_EBoard != null)
            List_EBoard.Clear();
    }
    #endregion

    #region 컨베이어벨트
    public void ConveyerBeltSetting()
    {
        if (EditorManager.Instance.SelectComp == null) return;

        if (EditorManager.Instance.SelectComp.m_CompType != CompType.Panel) return;

        if (((PanelComp)EditorManager.Instance.SelectComp).m_PanelType != PanelType.ConveyerBelt) return;

        if (List_EBoard.Count > 1)
        {
            for (int i = 0; i < List_EBoard.Count; i++)
            {
                if (i == 0)
                {
                    List_EBoard[0].m_ConveyerBeltInfo.Type = ConveyerBeltType.Start;
                    List_EBoard[0].m_ConveyerBeltInfo.Dir_In = ConveyerBeltDir.Left;
                    List_EBoard[0].m_ConveyerBeltInfo.Dir_Out = GeDirOtherBoard(List_EBoard[0].INDEX, List_EBoard[1].INDEX);
                    List_EBoard[0].m_ConveyerBeltInfo.WaropIndex = List_EBoard[0].INDEX;
                }
                else if (i == List_EBoard.Count - 1)
                {
                    List_EBoard[i].m_ConveyerBeltInfo.Type = ConveyerBeltType.End;
                    List_EBoard[i].m_ConveyerBeltInfo.Dir_In = GeDirOtherBoard(List_EBoard[i].INDEX, List_EBoard[i - 1].INDEX);
                    List_EBoard[i].m_ConveyerBeltInfo.Dir_Out = ConveyerBeltDir.Right;
                    List_EBoard[i].m_ConveyerBeltInfo.WaropIndex = List_EBoard[0].INDEX;
                }
                else
                {
                    List_EBoard[i].m_ConveyerBeltInfo.Type = ConveyerBeltType.Mid;
                    List_EBoard[i].m_ConveyerBeltInfo.Dir_In = GeDirOtherBoard(List_EBoard[i].INDEX, List_EBoard[i - 1].INDEX);
                    List_EBoard[i].m_ConveyerBeltInfo.Dir_Out = GeDirOtherBoard(List_EBoard[i].INDEX, List_EBoard[i + 1].INDEX);
                    List_EBoard[i].m_ConveyerBeltInfo.WaropIndex = List_EBoard[0].INDEX;
                }

                List_EBoard[i].ConveyerBeltUpdate();
            }
        }
    }

    public ConveyerBeltDir GeDirOtherBoard(int c_index, int o_index)
    {
        ConveyerBeltDir dir = ConveyerBeltDir.Left;
        int c_x = c_index % MatchDefine.MaxY;
        int c_y = c_index / MatchDefine.MaxY;
        int o_x = o_index % MatchDefine.MaxY;
        int o_y = o_index / MatchDefine.MaxY;

        if (c_x > o_x)
            dir = ConveyerBeltDir.Left;
        else if (c_x < o_x)
            dir = ConveyerBeltDir.Right;
        else if (c_y > o_y)
            dir = ConveyerBeltDir.Top;
        else
            dir = ConveyerBeltDir.Bottom;

        return dir;
    }

    public void ConveyerBeltUpdate(bool save = true)
    {
        //**************************
        //컨베이어벨트 이미지 세팅
        //**************************
        int gap = Mathf.Abs(m_ConveyerBeltInfo.Dir_In - m_ConveyerBeltInfo.Dir_Out);
        if (gap == 2)
        {
            //직선일때
            PanelUn_Img.sprite = ConveyerBelt1;
            PanelUn_Img.transform.localEulerAngles = new Vector3(0, 0, (int)m_ConveyerBeltInfo.Dir_In * -90);
        }
        else
        {
            //곡선일때
            PanelUn_Img.sprite = ConveyerBelt2;
            if (m_ConveyerBeltInfo.Dir_Out.Next() == m_ConveyerBeltInfo.Dir_In)
                PanelUn_Img.transform.localEulerAngles = new Vector3(0, 0, (int)m_ConveyerBeltInfo.Dir_In * -90);
            else
                PanelUn_Img.transform.localEulerAngles = new Vector3(180, 0, (int)m_ConveyerBeltInfo.Dir_In * 90);
        }

        //*********************
        //Start, End 연결 체크
        //*********************
        if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
        {
            EBoard end_board = EditorManager.Instance.m_ListEBoard.Find(find => find.m_ConveyerBeltInfo.Type == ConveyerBeltType.End
            && find.m_ConveyerBeltInfo.WaropIndex == m_ConveyerBeltInfo.WaropIndex);
            if (end_board != null)
            {
                if (IsArroundBoard(INDEX, end_board.INDEX))
                {
                    if (m_ConveyerBeltInfo.Dir_In == GetArroundDir(INDEX, end_board.INDEX)
                        && end_board.m_ConveyerBeltInfo.Dir_Out == GetArroundDir(end_board.INDEX, INDEX))
                    {
                        m_ConveyerBeltInfo.Type = ConveyerBeltType.Mid;
                        end_board.m_ConveyerBeltInfo.Type = ConveyerBeltType.Mid;
                        end_board.ConveyerBeltUpdate();

                    }
                }
            }
        }
        else if (m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
        {
            EBoard start_board = EditorManager.Instance.m_ListEBoard.Find(find => find.m_ConveyerBeltInfo.Type == ConveyerBeltType.Start
            && find.m_ConveyerBeltInfo.WaropIndex == m_ConveyerBeltInfo.WaropIndex);
            if (start_board != null)
            {
                if (IsArroundBoard(start_board.INDEX, INDEX))
                {
                    if (start_board.m_ConveyerBeltInfo.Dir_In == GetArroundDir(start_board.INDEX, INDEX)
                       && m_ConveyerBeltInfo.Dir_Out == GetArroundDir(INDEX, start_board.INDEX))
                    {
                        start_board.m_ConveyerBeltInfo.Type = ConveyerBeltType.Mid;
                        start_board.ConveyerBeltUpdate();
                        m_ConveyerBeltInfo.Type = ConveyerBeltType.Mid;
                    }
                }
            }
        }

        //********
        //포탈
        //********
        if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start || m_ConveyerBeltInfo.Type == ConveyerBeltType.End)
        {
            PanelUn_Img_Sub.sprite = ConveyerBelt3;
            PanelUn_Img_Sub.enabled = true;
            PanelUn_Img_Sub.SetNativeSize();

            ConveyerBeltDir chekc_dir;
            if (m_ConveyerBeltInfo.Type == ConveyerBeltType.Start)
                chekc_dir = m_ConveyerBeltInfo.Dir_In;
            else
                chekc_dir = m_ConveyerBeltInfo.Dir_Out;

            if (chekc_dir == ConveyerBeltDir.Left)
            {
                PanelUn_Img_Sub.transform.localEulerAngles = new Vector3(0, 0, 90);
                PanelUn_Img_Sub.transform.localPosition = new Vector3(10, -40, 0);
            }
            else if (chekc_dir == ConveyerBeltDir.Right)
            {
                PanelUn_Img_Sub.transform.localEulerAngles = new Vector3(0, 0, 90);
                PanelUn_Img_Sub.transform.localPosition = new Vector3(70, -40, 0);
            }
            else if (chekc_dir == ConveyerBeltDir.Top)
            {
                PanelUn_Img_Sub.transform.localEulerAngles = new Vector3(0, 0, 0);
                PanelUn_Img_Sub.transform.localPosition = new Vector3(40, -10, 0);
            }
            else if (chekc_dir == ConveyerBeltDir.Bottom)
            {
                PanelUn_Img_Sub.transform.localEulerAngles = new Vector3(0, 0, 0);
                PanelUn_Img_Sub.transform.localPosition = new Vector3(40, -70, 0);
            }
        }
        else
        {
            PanelUn_Img_Sub.enabled = false;
        }


        if (save)
            SaveCopyData();
    }

    public bool IsArroundBoard(int c_index, int o_index)
    {
        int c_x = c_index % MatchDefine.MaxY;
        int c_y = c_index / MatchDefine.MaxY;
        int o_x = o_index % MatchDefine.MaxY;
        int o_y = o_index / MatchDefine.MaxY;

        int gap_x = Mathf.Abs(c_x - o_x);
        int gap_y = Mathf.Abs(c_y - o_y);

        if (gap_x == 1 && gap_y == 0)
            return true;
        if (gap_x == 0 && gap_y == 1)
            return true;

        return false;
    }

    public ConveyerBeltDir GetArroundDir(int c_index, int o_index)
    {
        int c_x = c_index % MatchDefine.MaxY;
        int c_y = c_index / MatchDefine.MaxY;
        int o_x = o_index % MatchDefine.MaxY;
        int o_y = o_index / MatchDefine.MaxY;

        int gap_x = c_x - o_x;
        int gap_y = c_y - o_y;

        if (gap_x == 1)
        {
            return ConveyerBeltDir.Left;
        }
        else if (gap_x == -1)
        {
            return ConveyerBeltDir.Right;
        }
        else if (gap_y == 1)
        {
            return ConveyerBeltDir.Top;
        }
        else
        {
            return ConveyerBeltDir.Bottom;
        }
    }
    #endregion

    #region DropDir 세팅
    public void IninDropDir(DROP_DIR[] drop_dirs = null)
    {
        Drop_EBoard.DeleteDropImage();
        foreach (var drop in drop_dirs)
        {
            SetDropDir(CompManager.GetDropComp(drop), false);
        }
    }

    public void SetDropDir(DropComp dcomp, bool save = true)
    {
        //bool _ok = false;
        if (dcomp != null)
        {
            Drop_EBoard.SetDropImage(dcomp);

            //SetTextInfo(icomp.m_ItemType, 1);

            //데이타세팅
            if (save) SaveCopyData();
        }
    }

    public void DeleteDropDir(DropComp dcomp)
    {
        if (dcomp == null)
            return;

        Drop_EBoard.DeleteDropImage();
    }
    #endregion

    #region 터치
    //**********************************************
    //마우스의 터치를 구분하기 위해서 추가되었다.
    //EventTrigger에서 호출하면 된다.
    //**********************************************
    public static bool isDown = false;
    public static bool isLeftDown = true;

    public bool MouseOver = false;
    public void EventPointerDown(BaseEventData data)
    {
        isDown = true;

        PointerEventData pointerEventData = data as PointerEventData;
        GameObject pressed = pointerEventData.pointerPress;

        if (pointerEventData.button == PointerEventData.InputButton.Left)
        {
            //참고:Down은 click과 다르게 카운터를 세는 변수는 없다.
            isLeftDown = true;
            ListEboard_Add(); //Enter에서만 하면 처음클릭한 보드가 추가가 안됨.
            OnLeftSingleDown(pressed);

        }
        else if (pointerEventData.button == PointerEventData.InputButton.Right)
        {
            isLeftDown = false;
            OnRightSingleDown(pressed);
        }
        else if (pointerEventData.button == PointerEventData.InputButton.Middle)
        {
            //OnMiddleSingleDown(pressed);
        }
    }

    public void OnLeftSingleDown(GameObject obj)
    {

        if (EditorManager.Instance.SelectComp != null)
        {
            switch (EditorManager.Instance.SelectComp.m_CompType)
            {
                case CompType.Item:
                    SetItem((ItemComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Panel:
                    SetPanel_Click((PanelComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Drop:
                    SetDropDir((DropComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Etc:
                    SetEtc((EtcComp)EditorManager.Instance.SelectComp);
                    break;
            }
        }
    }

    public void OnRightSingleDown(GameObject obj)
    {
        if (EditorManager.Instance.SelectComp != null)
        {
            switch (EditorManager.Instance.SelectComp.m_CompType)
            {
                case CompType.Item:
                    DeleteItem((ItemComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Panel:
                    DeletePanel((PanelComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Drop:
                    DeleteDropDir((DropComp)EditorManager.Instance.SelectComp);
                    break;
                case CompType.Etc:
                    DeleteEtc((EtcComp)EditorManager.Instance.SelectComp);
                    break;
            }

        }
    }

    public void EventPointerUp(BaseEventData data)
    {
        isDown = false;

        //나중에 이런 패널이 더 추가되면 타입변수 하나 추가해서 switch분기처리할것!
        ConveyerBeltSetting();


        //맨 마지막에 처리
        ListEboard_Clear();
    }

    public void EventPointerEnter(BaseEventData data)
    {
        MouseOver = true;
        //Enter는 좌,우버튼 개념이 없나보다. 마우스가 올라오면 바로 호출됨..

        if (EditorManager.Instance.SelectComp != null && isDown)
        {
            if (isLeftDown)
            {
                ListEboard_Add();
                OnLeftSingleDown(gameObject);
            }
            else
            {
                OnRightSingleDown(gameObject);
            }
        }

    }

    public void EventPointerExit(BaseEventData data)
    {
        MouseOver = false;
    }
    #endregion

    #region 석판 추가 함수
    public List<EBoard> GetSteleBoads(int type)
    {
        List<EBoard> list_SteleBoard = new List<EBoard>();

        //타입별 세팅할 Eboard 가져오기  
        switch (type)
        {
            case 1:
                {
                    if (INDEX + 1 < 81 && INDEX % 9 != 8) //맨 마지막줄이 아니면
                    {
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX]);  //==this
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 1]);
                    }
                }
                break;
            case 2:
                {
                    if (INDEX + 9 < 81)
                    {
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX]);  //==this
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 9]);
                    }
                }
                break;
            case 3:
                {
                    if (INDEX + 9 + 1 < 81 && INDEX % 9 != 8) //맨 마지막줄이 아니면
                    {
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX]);  //==this
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 1]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 9]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 10]);
                    }
                }
                break;
            case 4:
                {
                    if (INDEX + 9 + 2 < 81 && INDEX % 9 != 8 && INDEX % 9 != 7) //맨 마지막줄이 아니면
                    {
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX]);  //==this
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 1]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 2]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 9]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 10]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 11]);
                    }
                }
                break;
            case 5:
                {
                    if (INDEX + 9 + 9 + 1 < 81 && INDEX % 9 != 8) //맨 마지막줄이 아니면
                    {
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX]);  //==this
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 1]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 9]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 10]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 18]);
                        list_SteleBoard.Add(Edit_Mgr.m_ListEBoard[INDEX + 19]);
                    }
                }
                break;
        }

        //예외처리!!
        for (int i = 0; i < list_SteleBoard.Count; i++)
        {
            //이미 석판인 곳이 포함되어 있으면..
            if (list_SteleBoard[i].PanelB_Comp != null && list_SteleBoard[i].PanelB_Comp.m_PanelType == PanelType.Stele)
            {
                list_SteleBoard.Clear();
                break;
            }
        }

        return list_SteleBoard;
    }
    #endregion
    #region ETC 추가
    public void SetEtc(EtcComp ecomp)
    {
        switch (ecomp.m_EtcType)
        {
            case EtcType.Focus:
                Etc_Img.sprite = ecomp.m_Image.sprite;
                Etc_Img.enabled = true;
                if (CCS.focus.Contains(INDEX) == false)
                {
                    CCS.focus.Add(INDEX);
                }
                break;
        }
        ////bool _ok = false;
        //if (icomp != null)
        //{
        //    if (CheckPossbleItem())
        //    {
        //        Item_Comp = icomp;
        //        Item_Img.sprite = icomp.m_Image.sprite;
        //        Item_Img.color = icomp.m_Image.color;
        //        Item_Img.enabled = true;

        //        //SetTextInfo(icomp.m_ItemType, 1);

        //        //데이타세팅
        //        if (save) SaveCopyData();
        //    }
        //}
    }

    public void DeleteEtc(EtcComp ecomp)
    {
        switch (ecomp.m_EtcType)
        {
            case EtcType.Focus:
                //Etc_Img.sprite = ecomp.m_Image.sprite;
                Etc_Img.enabled = false;
                if (CCS.focus.Contains(INDEX) == true)
                {
                    CCS.focus.Remove(INDEX);
                }
                break;
        }

        //if (Item_Comp == null)
        //    return;

        ////아이템은 같은 아이템이 아니여도 보드에서 무조껀 삭제.
        ////if (m_ItemComp.m_ItemType == icomp.m_ItemType)
        //{
        //    //SetTextInfo(m_ItemComp.m_ItemType, -1);

        //    Item_Comp = null;
        //    Item_Img.enabled = false;

        //    if (EditorManager.Instance.isControl)
        //        SaveCopyData();
        //    else //기본적으로 지우면 노말아이템이 들어간다.
        //        SetItem(CompManager.GetItemComp(ItemType.Normal, ColorType.Rnd));
        //}
    }
    #endregion
}
