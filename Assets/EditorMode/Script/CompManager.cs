﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CompManager : MonoBehaviour
{

    GameObject m_Extension;
    Toggle m_Toggle;
    Image m_ToggleImage;

    public Comp ShowComp;

    public int Ext_Cnt = 0;

    //모든 아이템들을 관리하고 싶은..

    public static List<ItemComp> m_ListItemComp = new List<ItemComp>();
    public static List<PanelComp> m_ListPanelComp = new List<PanelComp>();
    public static List<EtcComp> m_ListEtcComp = new List<EtcComp>();
    public static List<DropComp> m_ListDropComp = new List<DropComp>();

    public static ItemComp GetItemComp(ItemType _type, ColorType _color)
    {
        return m_ListItemComp.Find(find => find.m_ItemType == _type && find.m_ColorType == _color);
    }

    public static PanelComp GetPanelComp(PanelType _type, int _def)
    {
        PanelComp _comp = m_ListPanelComp.Find(find => find.m_PanelType == _type && find.Defence == _def);
        return _comp;
    }

    public static EtcComp GetEtcComp(EtcType _type)
    {
        return m_ListEtcComp.Find(find => find.m_EtcType == _type);
    }


    public static DropComp GetDropComp(DROP_DIR _dir)
    {
        return m_ListDropComp.Find(find => find.m_DropDir == _dir);
    }

    public static void Destory_Comp()
    {
        m_ListItemComp.Clear();
        m_ListPanelComp.Clear();
        m_ListDropComp.Clear();
        m_ListEtcComp.Clear();
    }

    //현재 클래스만으로 세팅하는 부분은 AWake()에서 한다.
    void Awake()
    {
        //기본 세팅
        SetExtension();
        SetToggleImage();

        //들고 있는 Comp를 EditorManger에게 전달.
        SetTotalComp();

    }

    //다른 클래스에 접근해서 무언가를 하는행동은 Start()에서 하자.
    void Start()
    {
        //첫번째 Comp를 기본으로 세팅.
        SetFirstComp();
    }

    public void SetExtension()
    {
        m_Extension = transform.Find("Extension").gameObject;

        Image _BgImage = m_Extension.GetComponent<Image>();

        int childcnt = m_Extension.transform.childCount;

        _BgImage.rectTransform.sizeDelta = new Vector2(60, 60 * childcnt);

    }

    public void SetToggleImage()
    {
        m_Toggle = GetComponent<Toggle>();

        m_ToggleImage = (Image)m_Toggle.targetGraphic;
    }

    public void SetTotalComp()
    {
        Comp[] listComp = m_Extension.GetComponentsInChildren<Comp>();
        foreach (Comp _comp in listComp)
        {
            _comp.Init();

            if (_comp.GetType() == typeof(ItemComp))
                m_ListItemComp.Add((ItemComp)_comp);
            else if (_comp.GetType() == typeof(PanelComp))
                m_ListPanelComp.Add((PanelComp)_comp);
            else if (_comp.GetType() == typeof(DropComp))
                m_ListDropComp.Add((DropComp)_comp);
            else
                m_ListEtcComp.Add((EtcComp)_comp);
        }
    }

    public void SetFirstComp()
    {
        Comp[] listComp = m_Extension.GetComponentsInChildren<Comp>();

        SetShowComp(listComp[0]);

        Ext_Cnt = listComp.Length;

    }


    public void SetShowComp(Comp _comp)
    {
        //보이는 이미지 변경
        m_ToggleImage.sprite = _comp.m_Image.sprite;
        m_ToggleImage.color = _comp.m_Image.color;

        //선택한 컴포넌트 세팅
        ShowComp = _comp;
        EditorManager.Instance.SelectComp = ShowComp;
    }

    public void SetSelectComp(Comp _comp)
    {
        EditorManager.Instance.SelectComp = _comp;

        //선택했다고 토글배경출력
        m_Toggle.isOn = true;
        SetShowExtension(false);
    }


    //확장 보여주기
    bool m_ShowExtention = false;
    public void SetShowExtension(bool var)
    {
        m_ShowExtention = var;
        m_Extension.SetActive(var);
    }

    #region 터치
    //**********************************************
    //마우스의 터치를 구분하기 위해서 추가되었다.
    //EventTrigger에서 호출하면 된다.
    //**********************************************
    public void EventPointerClick(BaseEventData data)
    {
        PointerEventData pointerEventData = data as PointerEventData;
        GameObject pressed = pointerEventData.pointerPress;

        //Left click
        if (pointerEventData.button == PointerEventData.InputButton.Left)
        {
            if (pointerEventData.clickCount == 1)
            {
                OnLeftSingleClick(pressed);
            }
            else if (pointerEventData.clickCount == 2)
            {
                OnLeftDoubleClick(pressed);
            }
        }
        //Right click
        else if (pointerEventData.button == PointerEventData.InputButton.Right)
        {
            if (pointerEventData.clickCount == 1)
            {
                OnRightSingleClick(pressed);
            }
            else if (pointerEventData.clickCount == 2)
            {
                OnRightDoubleClick(pressed);
            }
        }
        //Middle click
        else if (pointerEventData.button == PointerEventData.InputButton.Middle)
        {
            if (pointerEventData.clickCount == 1)
            {
                OnMiddleSingleClick(pressed);
            }
            else if (pointerEventData.clickCount == 2)
            {
                OnMiddleDoubleClick(pressed);
            }
        }
    }

    public void OnLeftSingleClick(GameObject obj)
    {
        if (obj.Equals(this.gameObject)) //토글을 선택했을때
        {
            SetSelectComp(ShowComp);
        }

        else //확장하여 보이는 아이템들을 선택했을때
        {
            Comp _comp = obj.GetComponent<Comp>();
            SetShowComp(_comp);
            SetSelectComp(_comp);
        }
    }
    public void OnLeftDoubleClick(GameObject obj) { }

    public void OnRightSingleClick(GameObject obj)
    {
        if (Ext_Cnt > 1) //1개만 있으면 구지 열리지 않는다.
        {
            SetShowExtension(!m_ShowExtention);
        }
    }
    public void OnRightDoubleClick(GameObject obj) { }

    public void OnMiddleSingleClick(GameObject obj) { }
    public void OnMiddleDoubleClick(GameObject obj) { }
    #endregion
}
