﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(EditorManager))]
public class EditorManagerEditor : Editor
{
    EditorManager EditorMgr;

    string[] setname = {"A Set", "B Set"};

    public void OnEnable()
    {
        EditorMgr = target as EditorManager;    
    }

    public void MissionInfo_Add(MissionType type, MissionKind kind, int cnt)
    {
        MissionInfo info = EditorMgr.CSD.missionInfo.Find(find => find.type == type && find.kind == kind);
        if (info == null)
        {
            info = new MissionInfo();
            info.type = type;
            info.kind = kind;
            info.count = cnt;
            EditorMgr.CSD.missionInfo.Add(info);
        }
        else
        {
            info.count = cnt;
        }

        if (EditorMgr.CSD.missionInfo.Count > 4)
            Debug.LogError("미션이 4개를 초과하였습니다.!!");
    }
    public void MissionInfo_RemoveType(MissionType type)
    {
        EditorMgr.CSD.missionInfo.RemoveAll(find => find.type == type);
    }
    public void MissionInfo_RemoveKind(MissionType type, MissionKind kind)
    {
        EditorMgr.CSD.missionInfo.RemoveAll(find => find.type == type && find.kind == kind);
    }

    bool Stage_go = true;
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        serializedObject.Update();

        GUILayout.Space(10f);

        EditorMgr.m_Set = GUILayout.Toolbar(EditorMgr.m_Set, setname, new GUILayoutOption[] { GUILayout.Width(150) });

        GUILayout.Space(10f);

        EditorGUILayout.BeginHorizontal();


        // JEditorUtil.SetLabelWidth(40f);
        JEditorUtil.DrawLabel("STAGE", false, GUILayout.Width(45f));
        if (JEditorUtil.DrawButton_Click("<<", GUILayout.Width(50f)))
        {
            EditorMgr.m_Stage--;

            if (Stage_go)
            {
                EditorMgr.SelectDataSet();
                EditorMgr.LoadStage();
            }
        }
        JEditorUtil.DrawProperty("", serializedObject, "m_Stage", false, GUILayout.Width(50f));
        if (JEditorUtil.DrawButton_Click(">>", GUILayout.Width(50f)))
        {
            EditorMgr.m_Stage++;

            if (Stage_go)
            {
                EditorMgr.SelectDataSet();
                EditorMgr.LoadStage();
            }
        }
        GUILayout.Space(20f);

        Stage_go = JEditorUtil.DrawVariable_Bool("",Stage_go, false, null);
       

        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10f);

        EditorGUILayout.BeginHorizontal();
        if (JEditorUtil.DrawButton_Click("Load Stage", GUILayout.Height(30f)))
        {
            EditorMgr.SelectDataSet();
            EditorMgr.LoadStage();
        }

        if (JEditorUtil.DrawButton_Click("Save Stage", GUILayout.Height(30f)))
        {
            EditorMgr.SelectDataSet();
            EditorMgr.SaveStage();
        }

        if (JEditorUtil.DrawButton_Click("New Stage", GUILayout.Height(30f)))
        {
            EditorMgr.SelectDataSet();
            EditorMgr.NewStage();
        }

        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10f);

        serializedObject.ApplyModifiedProperties();

        //*****************
        //Stage Clear Info
        //*****************

        if (EditorMgr.CSD != null)
        {
            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            JEditorUtil.DrawVariable_Field<Stage>("StarScore 1", EditorMgr.CSD, "scoreStar1", true);
            JEditorUtil.DrawVariable_Field<Stage>("StarScore 2", EditorMgr.CSD, "scoreStar2", true);
            JEditorUtil.DrawVariable_Field<Stage>("StarScore 3", EditorMgr.CSD, "scoreStar3", true);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            JEditorUtil.DrawVariable_Field<Stage>("MoveLimit", EditorMgr.CSD, "limit_Move", true);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            JEditorUtil.DrawVariable_Field<Stage>("TitleMission", EditorMgr.CSD, "missionType", true);


            EditorMgr.CSD.isOrderNMission = JEditorUtil.DrawVariable_Bool("OrderN", EditorMgr.CSD.isOrderNMission, false);
            if (EditorMgr.CSD.isOrderNMission)
            {
                EditorGUI.indentLevel++;

                //**********************************************
                //OderN미션중에 숫자를 적는건 여기서 처리한다.
                //Enum값이므로 중간에 추가해도 된다.
                //**********************************************
                for (int i = (int)MissionKind.Red; i <= (int)MissionKind.Spiral; i++)
                {
                    MissionInfo _info = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.OrderN && find.kind == (MissionKind)i);
                    if (_info == null)
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), 0, false);
                        if (num > 0)
                            MissionInfo_Add(MissionType.OrderN, (MissionKind)i, num);
                    }
                    else
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), _info.count, false);
                        if (num > 0 && _info.count != num)
                            MissionInfo_Add(MissionType.OrderN, (MissionKind)i, num);
                        else if (num == 0)
                            MissionInfo_RemoveKind(MissionType.OrderN, (MissionKind)i);
                    }
                }

                //*********************************************
                //OderN미션중에 체크만 하는건 여기서 처리한다.
                //OderN미션중에 맨밑에 추가한다.
                //**********************************************
                for (int i = (int)MissionKind.Bread; i <= (int)MissionKind.S_Tree; i++)
                {
                    MissionInfo _info2 = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.OrderN && find.kind == (MissionKind)i);
                    if (_info2 == null)
                    {
                        bool var = JEditorUtil.DrawVariable_Bool(((MissionKind)i).ToString(), false, false);
                        if (var)
                            MissionInfo_Add(MissionType.OrderN, (MissionKind)i, 0);
                    }
                    else
                    {
                        bool var = JEditorUtil.DrawVariable_Bool(((MissionKind)i).ToString(), true, false);
                        if (var == false)
                            MissionInfo_RemoveKind(MissionType.OrderN, (MissionKind)i);
                    }
                }

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.OrderN);
            }

            EditorMgr.CSD.isOrderSMission = JEditorUtil.DrawVariable_Bool("OrderS", EditorMgr.CSD.isOrderSMission, false);
            if (EditorMgr.CSD.isOrderSMission)
            {
                EditorGUI.indentLevel++;

                //**********************************************
                //OderS미션중에 숫자를 적는건 여기서 처리한다.
                //Enum값이므로 중간에 추가해도 된다.
                //**********************************************
                for (int i = (int)MissionKind.Line; i <= (int)MissionKind.Rainbow_Rainbow; i++)
                {
                    MissionInfo _info = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.OrderS && find.kind == (MissionKind)i);
                    if (_info == null)
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), 0, false);
                        if (num > 0)
                            MissionInfo_Add(MissionType.OrderS, (MissionKind)i, num);
                    }
                    else
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), _info.count, false);
                        if (num > 0 && _info.count != num)
                            MissionInfo_Add(MissionType.OrderS, (MissionKind)i, num);
                        else if (num == 0)
                            MissionInfo_RemoveKind(MissionType.OrderS, (MissionKind)i);
                    }
                }

                //*********************************************
                //OderS미션중에 체크만 하는건 여기서 처리한다.
                //OderS미션중에 맨밑에 추가한다.
                //**********************************************
                for (int i = (int)MissionKind.TimeBomb; i <= (int)MissionKind.TimeBomb; i++)
                {
                    MissionInfo _info2 = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.OrderS && find.kind == (MissionKind)i);
                    if (_info2 == null)
                    {
                        bool var = JEditorUtil.DrawVariable_Bool(((MissionKind)i).ToString(), false, false);
                        if (var)
                            MissionInfo_Add(MissionType.OrderS, (MissionKind)i, 0);
                    }
                    else
                    {
                        bool var = JEditorUtil.DrawVariable_Bool(((MissionKind)i).ToString(), true, false);
                        if (var == false)
                            MissionInfo_RemoveKind(MissionType.OrderS, (MissionKind)i);
                    }
                }

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.OrderS);
            }

            EditorMgr.CSD.isWaferMission = JEditorUtil.DrawVariable_Bool("Wafer", EditorMgr.CSD.isWaferMission, false);
            if (EditorMgr.CSD.isWaferMission)
            {
                EditorGUI.indentLevel++;

                JEditorUtil.DrawLabel("세팅된 웨하스가 전부깨지면 클리어!", false);
                MissionInfo_Add(MissionType.Wafer, MissionKind.Wafer, 0);

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.Wafer);
            }

            EditorMgr.CSD.isFoodMission = JEditorUtil.DrawVariable_Bool("Food", EditorMgr.CSD.isFoodMission, false);
            if (EditorMgr.CSD.isFoodMission)
            {
                EditorGUI.indentLevel++;

                for (int i = (int)MissionKind.StrawberryCake; i <= (int)MissionKind.Hamburger; i++)
                {
                    MissionInfo _info = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.Food && find.kind == (MissionKind)i);
                    if (_info == null)
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), 0, false);
                        if (num > 0)
                            MissionInfo_Add(MissionType.Food, (MissionKind)i, num);
                    }
                    else
                    {
                        int num = JEditorUtil.DrawVariable_Int(((MissionKind)i).ToString(), _info.count, false);
                        if (num > 0 && _info.count != num)
                            MissionInfo_Add(MissionType.Food, (MissionKind)i, num);
                        else if (num == 0)
                            MissionInfo_RemoveKind(MissionType.Food, (MissionKind)i);
                    }
                }

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.Food);
            }

            EditorMgr.CSD.isBearMission = JEditorUtil.DrawVariable_Bool("Bear", EditorMgr.CSD.isBearMission, false);
            if (EditorMgr.CSD.isBearMission)
            {
                EditorGUI.indentLevel++;

                MissionInfo _info = EditorMgr.CSD.missionInfo.Find(find => find.type == MissionType.Bear && find.kind == MissionKind.Bear);
                if (_info == null)
                {
                    int num = JEditorUtil.DrawVariable_Int("Bear_Count", 0, false);
                    if (num > 0)
                        MissionInfo_Add(MissionType.Bear, MissionKind.Bear, num);
                }
                else
                {
                    int num = JEditorUtil.DrawVariable_Int("Bear_Count", _info.count, false);
                    if (num > 0 && _info.count != num)
                        MissionInfo_Add(MissionType.Bear, MissionKind.Bear, num);
                    else if (num == 0)
                        MissionInfo_RemoveKind(MissionType.Bear, MissionKind.Bear);
                }

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.Bear);
            }
            EditorMgr.CSD.isIceCreamMission = JEditorUtil.DrawVariable_Bool("IceCream", EditorMgr.CSD.isIceCreamMission, false);
            if (EditorMgr.CSD.isIceCreamMission)
            {
                EditorGUI.indentLevel++;

                JEditorUtil.DrawLabel("아이스크림이 전부깨지면 클리어!", false);
                MissionInfo_Add(MissionType.IceCream, MissionKind.IceCream, 0);

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.IceCream);
            }

            EditorMgr.CSD.isJamMission = JEditorUtil.DrawVariable_Bool("Jam", EditorMgr.CSD.isJamMission, false);
            if (EditorMgr.CSD.isJamMission)
            {
                EditorGUI.indentLevel++;

                JEditorUtil.DrawLabel("쨈으로 모든 바닥을 덮으면 클리어!", false);
                MissionInfo_Add(MissionType.Jam, MissionKind.Jam, 0);

                EditorGUI.indentLevel--;
            }
            else
            {
                MissionInfo_RemoveType(MissionType.Jam);
            }


            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            EditorGUILayout.BeginHorizontal(); 
            EditorGUILayout.PrefixLabel("Appear Color"); //LabelField와 차이점이?? 일단 이런것도 있다는 차원에서 사용해봄.
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            JEditorUtil.SetLabelWidth(80f);
            if (EditorMgr.CSD.appearColor == null || EditorMgr.CSD.appearColor.Length == 0)
                EditorMgr.CSD.appearColor = new bool[6];
            for (int i = 0, len = EditorMgr.CSD.appearColor.Length; i < len; i++)
            {
                EditorMgr.CSD.appearColor[i] = JEditorUtil.DrawVariable_Bool(((ColorType)i + 1).ToString(), EditorMgr.CSD.appearColor[i], false, GUILayout.Width(130));
                if (i == 1 || i == 3)
                {
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                }
            }

            JEditorUtil.SetLabelWidth(350f);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            EditorGUILayout.PrefixLabel("특수아이템세팅(Top과 Empty생성기만 라인체크박스로 끌수 있음,");

            JEditorUtil.SetLabelWidth(150f);
            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            //EditorMgr.CSD.isTopCreateFood = JEditorUtil.DrawVariable_Bool("Food Top 생성", EditorMgr.CSD.isTopCreateFood, false);
            {
                EditorGUILayout.LabelField("Food 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("food_MaxExist", EditorMgr.CSD, "food_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("food_Interval", EditorMgr.CSD, "food_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("food_SpawnCnt", EditorMgr.CSD, "food_SpawnCnt", false);

                EditorGUI.indentLevel--;
            }

            //EditorMgr.CSD.isTopCreateSpiral = JEditorUtil.DrawVariable_Bool("Spiral Top 생성", EditorMgr.CSD.isTopCreateSpiral, false);
            {
                EditorGUILayout.LabelField("Spiral 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("Spiral_MinExist", EditorMgr.CSD, "Spiral_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Spiral_MaxExist", EditorMgr.CSD, "Spiral_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Spiral_Interval", EditorMgr.CSD, "Spiral_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("Spiral_SpawnCnt", EditorMgr.CSD, "Spiral_SpawnCnt", false);

                EditorGUI.indentLevel--;
            }

            //EditorMgr.CSD.isTopCreateDonut = JEditorUtil.DrawVariable_Bool("Donut Top 생성", EditorMgr.CSD.isTopCreateDonut, false);
            {
                EditorGUILayout.LabelField("Donut 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("Donut_MinExist", EditorMgr.CSD, "Donut_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Donut_MaxExist", EditorMgr.CSD, "Donut_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Donut_Interval", EditorMgr.CSD, "Donut_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("Donut_SpawnCnt", EditorMgr.CSD, "Donut_SpawnCnt", false);

                EditorGUI.indentLevel--;
            }

            //EditorMgr.CSD.isTopCreateTimeBomb = JEditorUtil.DrawVariable_Bool("TimeBomb Top 생성", EditorMgr.CSD.isTopCreateTimeBomb, false);
            {
                EditorGUILayout.LabelField("TimeBomb 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("TimeBomb_MinExist", EditorMgr.CSD, "TimeBomb_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("TimeBomb_MaxExist", EditorMgr.CSD, "TimeBomb_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("TimeBomb_Interval", EditorMgr.CSD, "TimeBomb_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("TimeBomb_SpawnCnt", EditorMgr.CSD, "TimeBomb_SpawnCnt", false);
                JEditorUtil.DrawVariable_Field<Stage>("TimeBomb_FirstCount", EditorMgr.CSD, "TimeBomb_FirstCount", false);

                EditorGUI.indentLevel--;
            }

            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Mystery 생성조건. ", GUILayout.Width(150));
                //GUILayout.Space(20f);
                if (JEditorUtil.DrawButton_Click("Probility_Modify", GUILayout.Width(120f)))
                {
                    MysterySetting.Init();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("Mystery_SettingType", EditorMgr.CSD, "Mystery_SettingType", false, new GUILayoutOption[] { GUILayout.Width(290) });
                JEditorUtil.DrawVariable_Field<Stage>("Mystery_MinExist", EditorMgr.CSD, "Mystery_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Mystery_MaxExist", EditorMgr.CSD, "Mystery_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Mystery_Interval", EditorMgr.CSD, "Mystery_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("Mystery_SpawnCnt", EditorMgr.CSD, "Mystery_SpawnCnt", false);

               
                EditorGUI.indentLevel--;
            }

            {
                EditorGUILayout.LabelField("Chameleon 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("Chameleon_MinExist", EditorMgr.CSD, "Chameleon_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Chameleon_MaxExist", EditorMgr.CSD, "Chameleon_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Chameleon_Interval", EditorMgr.CSD, "Chameleon_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("Chameleon_SpawnCnt", EditorMgr.CSD, "Chameleon_SpawnCnt", false);

                EditorGUI.indentLevel--;
            }

            {
                EditorGUILayout.LabelField("Key 생성조건");
                EditorGUI.indentLevel++;

                JEditorUtil.DrawVariable_Field<Stage>("Key_MinExist", EditorMgr.CSD, "Key_MinExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Key_MaxExist", EditorMgr.CSD, "Key_MaxExist", false);
                JEditorUtil.DrawVariable_Field<Stage>("Key_Interval", EditorMgr.CSD, "Key_Interval", false);
                JEditorUtil.DrawVariable_Field<Stage>("Key_SpawnCnt", EditorMgr.CSD, "Key_SpawnCnt", false);

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.BeginVertical(EditorStyles.textArea);
        EditorGUILayout.PrefixLabel("특수 아이템&패널 생성 조건");

        {//젤리 곰
            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            EditorGUILayout.PrefixLabel("JellyBear");

            EditorGUI.indentLevel++;

            JEditorUtil.DrawVariable_Field<Stage>("Bear_MaxExist", EditorMgr.CSD, "Bear_MaxExist", false);
            JEditorUtil.DrawVariable_Field<Stage>("Bear_Interval", EditorMgr.CSD, "Bear_Interval", false);

            EditorGUI.indentLevel--;

            EditorGUILayout.EndVertical();
        }

        {//아이스크림
            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            EditorGUILayout.PrefixLabel("아이스크림");

            EditorGUI.indentLevel++;

            JEditorUtil.DrawVariable_Field<Stage>("IceCream_Interval", EditorMgr.CSD, "IceCream_Interval", false);
            JEditorUtil.DrawVariable_Field<Stage>("IceCreamCreator_Interval", EditorMgr.CSD, "IceCreamCreator_Interval", false);

            EditorGUI.indentLevel--;

            EditorGUILayout.EndVertical();
        }

        {//스페셜트리
            EditorGUILayout.BeginVertical(EditorStyles.textArea);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("스페셜트리", GUILayout.Width(150));


            if (JEditorUtil.DrawButton_Click("Probility_Modify", GUILayout.Width(120f)))
            {
                S_TreeSetting.Init();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel++;
            JEditorUtil.DrawVariable_Field<Stage>("S_Tree_SettingType", EditorMgr.CSD, "S_Tree_SettingType", false, new GUILayoutOption[] { GUILayout.Width(290) });

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
        }

        {//보석트리
            EditorGUILayout.BeginVertical(EditorStyles.textArea);
            EditorGUILayout.PrefixLabel("보석트리");

            EditorGUI.indentLevel++;
            ////////////////////////////////////////
            if (EditorMgr.CSD.JewelTreeItem == null || EditorMgr.CSD.JewelTreeItem.Length == 0)
            {
                //실행되지 않은 상태로 여기를 타면 여기에서 할당한다.
                EditorMgr.CSD.JewelTreeItem = new ItemType[4];
                EditorMgr.CSD.JewelTreeItemColor = new ColorType[4];
            }

           
            for (int i = 0, len = EditorMgr.CSD.JewelTreeItem.Length; i < len; i++)
            {
                EditorGUILayout.BeginHorizontal();

                JEditorUtil.SetLabelWidth(60f);
                EditorMgr.CSD.JewelTreeItem[i] = (ItemType)JEditorUtil.DrawVariable_Enum("Item" + i, EditorMgr.CSD.JewelTreeItem[i], false, new GUILayoutOption[] { GUILayout.Width(160) });

                JEditorUtil.SetLabelWidth(90f);
                EditorMgr.CSD.JewelTreeItemColor[i] = (ColorType)JEditorUtil.DrawVariable_Enum("ItemColor" + i, EditorMgr.CSD.JewelTreeItemColor[i], false, new GUILayoutOption[] { GUILayout.Width(160) });

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.LabelField("*주의*컬러블럭과 None블럭의 차이는 매칭가능 유무입니다.");
            EditorGUILayout.LabelField("*주의*컬러블럭을 None으로 세팅하면 매칭이 되지 않습니다.!");

            EditorGUI.indentLevel--;

            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndVertical();

    }

    #region 미스테리 아이템 세팅
    public class MysterySetting : EditorWindow
    {
        public static MysteryData Mystery_Data = null; //new MysteryInfo();
        public MysterySettingType Mystery_SettingType = MysterySettingType.Mystery_Basic;
        int[] Crunt_prob;

        public static void Init()
        {
            Mystery_Data = DataManager.LoadMysteryData();

            // Get existing open window or if none, make a new one:
            MysterySetting window = (MysterySetting)EditorWindow.GetWindow(typeof(MysterySetting));
            window.Show();
        }

        void OnGUI()
        {
            //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
            //myBool = EditorGUILayout.Toggle("Toggle", myBool);
            //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
            //EditorGUILayout.EndToggleGroup();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(80f);
            GUILayout.Label("Mystery 확률 세팅", EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10f);

            JEditorUtil.SetLabelWidth(125f);
            //일단 높이가 작은건 나중에 찾으면 수정.
            //Mystery_SettingType = (MysterySettingType)EditorGUILayout.EnumPopup("MysterySettingType", Mystery_SettingType, new GUILayoutOption[] { GUILayout.Width(300)});
            JEditorUtil.DrawVariable_Field<MysterySetting>("MysterySettingType", this, "Mystery_SettingType", false, new GUILayoutOption[] { GUILayout.Width(290) });

            GUILayout.Space(10f);

            if(Mystery_Data.List_Prob.Count <= (int)Mystery_SettingType)
            {
                Debug.LogError("미스테리 세팅타입을 추가 하실려면 세이브를 누르세요.");
            }
            else
            {
                Crunt_prob = Mystery_Data.List_Prob[(int)Mystery_SettingType];
                for (int i = (int)MysteryItemType.Normal; i < (int)MysteryItemType.Last; i++)
                {
                    if (Crunt_prob.Length <= i)
                    {
                        Debug.LogError("미스테리용 아이템이 추가 되었나요? 로드파일의 아이템 갯수를 넘어갔습니다.");
                        Debug.LogError("세이브를 누르면 추가된 아이템까지 저장합니다.");
                        break;
                    }
                    Crunt_prob[i] = JEditorUtil.DrawVariable_Int(((MysteryItemType)i).ToString(), Crunt_prob[i], false, GUILayout.Width(290));
                }
            }
           
            GUILayout.Space(15f);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(90f);
            if (JEditorUtil.DrawButton_Click("Save", new GUILayoutOption[] { GUILayout.Width(100f), GUILayout.Height(20f) }))
            {
                Save_MysteryData();
            }
            EditorGUILayout.EndHorizontal();
        }

        public void Save_MysteryData()
        {

            if (Mystery_Data.List_Prob.Count <= (int)MysterySettingType.Mystery_Last_Test || Crunt_prob.Length < (int)MysteryItemType.Last)
            {
                MysteryData ninfo = new MysteryData();
                for(int i = 0; i < Mystery_Data.List_Prob.Count; i++)
                {
                    int[] _prob = Mystery_Data.List_Prob[i];
                    for (int k = 0; k < _prob.Length; k++)
                    {
                        ninfo.List_Prob[i][k] = _prob[k];
                    }
                }

                Mystery_Data = ninfo;
                Debug.LogError("정상적으로 아이템정보가 추가 되었습니다.!!");
            }

            DataManager.SaveMysteryData(Mystery_Data);

            AssetDatabase.Refresh();

            EditorUtility.DisplayDialog(
                    "Save Complete",
                    "Assets/Data/Stage/A/Item/MysteryData.txt",
                    "OK"
                );
        }
    }

    #endregion

    #region 스페셜트리 아이템 세팅
    public class S_TreeSetting : EditorWindow
    {
        public static S_TreeData m_S_TreeData = null;
        public S_TreeSettingType m_S_TreeSettingType = S_TreeSettingType.Basic;
        int[] Crunt_prob;

        public static void Init()
        {
            m_S_TreeData = DataManager.LoadSTreeData();
            if(m_S_TreeData == null)
                m_S_TreeData = new S_TreeData();

            // Get existing open window or if none, make a new one:
            S_TreeSetting window = (S_TreeSetting)EditorWindow.GetWindow(typeof(S_TreeSetting));
            window.Show();
        }

        void OnGUI()
        {
            //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
            //myBool = EditorGUILayout.Toggle("Toggle", myBool);
            //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
            //EditorGUILayout.EndToggleGroup();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(80f);
            GUILayout.Label("스페셜트리 생성아이템 확률 세팅", EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10f);

            JEditorUtil.SetLabelWidth(125f);
            //일단 높이가 작은건 나중에 찾으면 수정.
            //Mystery_SettingType = (MysterySettingType)EditorGUILayout.EnumPopup("MysterySettingType", Mystery_SettingType, new GUILayoutOption[] { GUILayout.Width(300)});
            JEditorUtil.DrawVariable_Field<S_TreeSetting>("S_TreeSettingType", this, "m_S_TreeSettingType", false, new GUILayoutOption[] { GUILayout.Width(290) });

            GUILayout.Space(10f);

            if (m_S_TreeData.List_Prob.Count <= (int)m_S_TreeSettingType)
            {
                Debug.LogError("스페셜트리 세팅타입을 추가 하실려면 세이브를 누르세요.");
            }
            else
            {
                Crunt_prob = m_S_TreeData.List_Prob[(int)m_S_TreeSettingType];
                for (int i = (int)S_TreeItemType.Normal; i < (int)S_TreeItemType.Last; i++)
                {
                    if (Crunt_prob.Length <= i)
                    {
                        Debug.LogError("스페셜트리 아이템이 추가 되었나요? 로드파일의 아이템 갯수를 넘어갔습니다.");
                        Debug.LogError("세이브를 누르면 추가된 아이템까지 저장합니다.");
                        break;
                    }
                    Crunt_prob[i] = JEditorUtil.DrawVariable_Int(((S_TreeItemType)i).ToString(), Crunt_prob[i], false, GUILayout.Width(290));
                }
            }

            GUILayout.Space(15f);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(90f);
            if (JEditorUtil.DrawButton_Click("Save", new GUILayoutOption[] { GUILayout.Width(100f), GUILayout.Height(20f) }))
            {
                Save_S_TreeData();
            }
            EditorGUILayout.EndHorizontal();
        }

        public void Save_S_TreeData()
        {
            //**********************************************************
            //세팅타입이나 아이템타입이 추가되면 확장하기 위해 타는 부분
            //**********************************************************
            if (m_S_TreeData.List_Prob.Count <= (int)S_TreeSettingType.Last_Test || Crunt_prob.Length < (int)S_TreeItemType.Last)
            {
                S_TreeData ninfo = new S_TreeData();
                for (int i = 0; i < m_S_TreeData.List_Prob.Count; i++)
                {
                    int[] _prob = m_S_TreeData.List_Prob[i];
                    for (int k = 0; k < _prob.Length; k++)
                    {
                        ninfo.List_Prob[i][k] = _prob[k];
                    }
                }

                m_S_TreeData = ninfo;
                Debug.LogError("정상적으로 아이템정보가 추가 되었습니다.!!");
            }
            //**********************************************************

            DataManager.SaveSTreeData(m_S_TreeData);

            AssetDatabase.Refresh();

            EditorUtility.DisplayDialog(
                    "Save Complete",
                    "Assets/Data/Stage/A/Item/S_TreeData.txt",
                    "OK"
                );
        }
    }

    #endregion
}
